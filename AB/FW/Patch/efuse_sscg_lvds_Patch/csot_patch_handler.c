#include <intrins.h>
sfr WDTREL   = 0x86;
sbit WDT  = 0xAE;
sbit SWDT  = 0xBE;
// SSCG
unsigned char xdata g_rw_dbg_mux_002Bh_mcu_top_dbgr_2 _at_ 0xF82B;
unsigned char xdata r_sscgpll_en_sd _at_ 0x1806;
unsigned char xdata r_sscgpll_en_pll _at_ 0x180A;


// efuse key
unsigned char xdata tcon_reg_efuse_key _at_ 0xF81C;
// efuse trim
unsigned char xdata tcon_reg_1_0 _at_ 0x4803;
unsigned char xdata tcon_reg_2_0 _at_ 0x4804;
unsigned char xdata tcon_reg_1_1 _at_ 0x700E;
unsigned char xdata tcon_reg_2_1 _at_ 0x700F;
// efuse trim para
unsigned char xdata trim_flg _at_ 0xF600;
unsigned char xdata delay_cnt_1 _at_ 0xF601;

// lvds
unsigned char xdata r_lvds_unlock_irq_en _at_ 0x4813; // 0x4813[0]
unsigned char xdata lvds_unlock_irq_ev _at_ 0x4902; // 0x4902[0]
unsigned char xdata r_aip_tx_reg_sw_en _at_ 0x1001; // 0x1001[4]
unsigned char xdata r_sscgpll_reg_sw_en _at_ 0x1875; // 0x1875[2]

// IC Ver Check
//volatile unsigned char xdata g_unlock _at_ 0xF81C;
volatile unsigned char xdata g_Ver_Cfg _at_ 0x7000;
// 0x7000[6] = 1
// read 0x7000 == 1 -->AA Ver

// ------------- PDF hit Frame Patch ----------------------- //
volatile unsigned char xdata reg_0x3816 _at_ 0x3816; // monitor 0x3816[0]
volatile unsigned char xdata reg_0x3817 _at_ 0x3817;
volatile unsigned char xdata reg_0x3818 _at_ 0x3818;
volatile unsigned char xdata reg_0x3819 _at_ 0x3819;
volatile unsigned char xdata reg_0xF80A _at_ 0xF80A;
volatile unsigned char xdata reg_0xF80E _at_ 0xF80E;
volatile unsigned char xdata reg_0xF85C _at_ 0xF85C; // vsync interrupt monitor [5]
volatile unsigned char xdata reg_0x3801 _at_ 0x3801; 

void double_patch_pdf_wait_reg_finish_handler(void); // C: 0x2C26
// --------------------------------------------------------- //

void patch_delay_100us(void);

void mcu_dma_i2c_slv_set_ch1(void) // C: 0xF8C
{
}

void mcu_dl_check(void)  // C : 0x1BA7
{
}

void mcu_init(void)	// C:0x1AEB
{
}

void tcon_det_lut_i2c_adr(void) // C: 1A66
{
}

void mcu_entry_dl_hdr(void) // C: 0473
{
}
	
void mcu_int_vsync(void)
{
	
}
#if 0
void Patch_TxSlew_SSCG(void)
{
	mcu_dma_i2c_slv_set_ch1();
	while (1)
	{
		if(g_rw_dbg_mux_002Bh_mcu_top_dbgr_2 & 0x02)		// aip finish
		{
			trim_flg = 0;
			r_sscgpll_en_pll |= 0x80;
			return;
		}
	}
}

void Patch_TxSlew_SSCG_16Kb(void)
{
	mcu_dma_i2c_slv_set_ch1();
	//trim_flg = 0;
	#if 0
	while (1)
	{
		if(g_rw_dbg_mux_002Bh_mcu_top_dbgr_2 & 0x02)		// aip finish
		{
			trim_flg = 0;
			r_sscgpll_en_pll |= 0x80;
			return;
		}
	}
	#endif
}
#endif
void patch_monitor_lvds_unlock_irq_event(void)
{
	if(lvds_unlock_irq_ev & 0x01)
	{
		r_sscgpll_reg_sw_en |= 0x04;
		_nop_();
		r_sscgpll_reg_sw_en &= 0xFB;
		
		r_aip_tx_reg_sw_en |= 0x10;
		_nop_();
		r_aip_tx_reg_sw_en &= 0xEF;
		lvds_unlock_irq_ev &= 0xFE;
	}
}

void Patch_efuse_trim(void)
{
	if(g_rw_dbg_mux_002Bh_mcu_top_dbgr_2 & 0x02)		// aip finish
	{
		if((r_sscgpll_en_pll & 0x80) == 0)
		{
			trim_flg = 0;
			r_sscgpll_en_pll |= 0x80;
			// if ver AA
			tcon_reg_efuse_key = 0xA0;		//ulock
			g_Ver_Cfg |= 0x40; 	// 0x7000[6] = 1
			if(g_Ver_Cfg == 0x01)	// AA Ver
			{
				patch_delay_100us();
				r_sscgpll_en_pll &= 0x7F;
				_nop_();
				_nop_();
				_nop_();
				_nop_();
				_nop_();
				r_sscgpll_en_pll |= 0x80;
			}
			g_Ver_Cfg &= 0xBF;
			tcon_reg_efuse_key = 0x00;
		}
	}
	if(g_rw_dbg_mux_002Bh_mcu_top_dbgr_2 & 0x04)		// reg finish
	{
		if(trim_flg == 0)
		{
			trim_flg = 1;
			tcon_reg_efuse_key |= 0xA0;
			tcon_reg_1_1 = tcon_reg_1_0;
			tcon_reg_2_1 = tcon_reg_2_0;
			tcon_reg_efuse_key &= 0x5F;
		}
		r_lvds_unlock_irq_en |= 0x01;
		patch_monitor_lvds_unlock_irq_event();		
	}
	mcu_dl_check();	// modify 12 xx xx
	// 22
}

void patch_delay_100us(void)
{
	for(delay_cnt_1 = 0; delay_cnt_1 < 50 ; delay_cnt_1++)
	{
		_nop_();
	}
}

void patch_mcu_wdt_init(void)	
{
    WDTREL = 0x7B; // 
//
    //mcu_wdt_reset();
}

void patch_mcu_wdt_reset(void)
{
    WDT = 1;
    SWDT = 1;
}

void patch_watdog_init(void)	//C: 2BF8
{
	patch_mcu_wdt_init();
	mcu_init();
}

void patch_main_loop_clr_wdog(void)	//C: 2C1A
{
	tcon_det_lut_i2c_adr();
	patch_mcu_wdt_reset();
}

void patch_mcu_entry_dl_hdr(void) // C: 2C20
{
	mcu_entry_dl_hdr();
	patch_mcu_wdt_reset();
}

// ------------- PDF hit Frame Patch ----------------------- //
void patch_pdf_hit_frame_wait_reg_finish(void)		// C: 0x2C60
{
	if(reg_0x3816 & 0x01)	//
	{
		reg_0xF80A |= 0x40;
		reg_0xF80E = 0x08;
		reg_0x3817 |= 0x04;
		reg_0x3819 |= 0x08;
	}
	if(reg_0x3816 & 0x02)	// lxl od 
	{
		reg_0xF80A |= 0x40;
		reg_0xF80E = 0x08;
	}
}

void patch_pdf_hit_frame_monitor_hit_interrutp(void) // C: 0x2C97
{
	if(reg_0x3816 & 0x01)
	{
		if(reg_0xF85C & 0x20) // [5]
		{
			reg_0x3817 = 0x84;
			reg_0x3818 = 0x25;
		}
		else
		{
			reg_0x3817 = 0x64;
			reg_0x3818 = 0x22;
		}
		reg_0x3819 = 0x08;
	}
	
	if(reg_0x3816 & 0x02)
	{
		if(reg_0xF85C & 0x20) // [5]
		{
			reg_0x3801 = 0x22;	// enable lxl od
		}
		else
		{
			reg_0x3801 = 0x20;	// disable lxl od
		}
	}
}

void double_patch_pdf_wait_reg_finish_handler(void) // C: 0x2C26
{
	patch_monitor_lvds_unlock_irq_event();
	patch_pdf_hit_frame_wait_reg_finish();
}

void double_patch_iex4_interrupt(void)	// C:0x2C2C
{
	mcu_int_vsync();
	patch_monitor_lvds_unlock_irq_event();
	patch_pdf_hit_frame_monitor_hit_interrutp();
}
// --------------------------------------------------------- //