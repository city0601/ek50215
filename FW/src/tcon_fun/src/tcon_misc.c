#include "mcu_fun_def.h"
#include "tcon_global_include.h"
#include "reg_include.h"
#include "tcon_misc.h"
#include "mcu_dl.h"
#include "mcu_int.h"
#include "mcu_uart.h"
#include "mcu_misc.h"
// -------------------------------------------------------------------------------//
bool g_vsync_irq_flg;
//uint8_t xdata g_hdr_ofst = 0;
//uint8_t xdata g_dl_entry_cnt = 0;
//volatile uint8_t xdata g_irq_reg_tcon_isr_event_0   _at_ (0xD00 + 0x00); 
//volatile uint8_t xdata g_irq_reg_tcon_isr_event_1   _at_ (0xD00 + 0x01);
//volatile uint8_t xdata g_irq_reg_tcon_isr_event_2   _at_ (0xD00 + 0x02);
//volatile uint8_t xdata g_irq_reg_tcon_isr_event_3   _at_ (0xD00 + 0x03); 
// -------------------------------------------------------------------------------//
//extern unsigned char code eeprom_entry_hdr_var;     // C : 0x2E02
// -------------------------------------------------------------------------------//
#ifdef DEMURA_CHK_FUN
void tcon_wait_dmr_finish(void)
{
    while(!g_rw_dbg_mux_011Eh.bits.r_demura_dl_finish);
}
#endif
#if 0
void tcon_wait_sys_finish(void)
{
	  //while((g_rw_dbg_mux_0081h_mcu_top_dbgr_1 & 0x02) != 0x02);     // 0xF881[1]
     // while((g_rw_dbg_mux_0054h_mcu_top_dbgr_1 & 0x02) != 0x02);     // 0xF854[1]
		while((g_rw_dbg_mux_0018h.byte & 0x02) != 0x02); // 0xF818[1]
	//g_rw_dbg_mux_0018h
	
}
#endif

static void chip_rst(void)
{
    g_rw_dbg_mux_0000h_sw_rst = 0xAA;
}

void tcon_wait_sys_reg_lut_finish(void)
{
    uint8_t l_dl_sta = 0;
    //uint8_t l_sta;
    /* option */
    
    while(1)
    {
        mcu_entry_dl_hdr();
        mcu_dl_handler();
        if(g_rw_dbg_mux_002Bh_mcu_top_dbgr_2 & 0x02)
            l_dl_sta |= 0x01;
        if(g_rw_dbg_mux_002Bh_mcu_top_dbgr_2 & 0x04)
            l_dl_sta |= 0x02;
        if(g_rw_dbg_mux_002Bh_mcu_top_dbgr_2 & 0x08)
            l_dl_sta |= 0x04;
        if(l_dl_sta == 0x07)
        {
            if(g_rw_sys_0003h.bits.r_cfg_end_sel)     // sys 003[5]
            {
                if(g_r_sys_data_20_u.bits.hw_init_finish)
            break;
            }
            else
            {
               break; 
            }
            //break;
        }
		//if ((g_rw_dbg_mux_0081h_mcu_top_dbgr_1 & 0x0C) == 0x0C)  // ---> 0xF881[3][2]
        //if ((g_rw_dbg_mux_0054h_mcu_top_dbgr_1 & 0x0C) == 0x0C)  // ---> 0xF854[3][2]
			//if ((g_rw_dbg_mux_0018h.byte & 0x0C) == 0x0C) // F818[3][2][1]
            //break;            
    }
}
#if 0
void tcon_wait_reg_finish(void)
{
    while(1)
    {
        mcu_dl_handler();
		//if ((g_rw_dbg_mux_0081h_mcu_top_dbgr_1 & 0x04) == 0x04)  // ---> 0xF881[2]
        //if ((g_rw_dbg_mux_0054h_mcu_top_dbgr_1 & 0x04) == 0x04)  // ---> 0xF854[2]
			if ((g_rw_dbg_mux_0018h.byte & 0x04) == 0x04)  // ---> 0xF818[2]
            break;            
    }
}

void tcon_wait_lut_finish(void)
{
    while(1)
    {
        mcu_dl_handler();
		//if ((g_rw_dbg_mux_0081h_mcu_top_dbgr_1 & 0x08) == 0x08)  // ---> 0xF881[3]
        //if ((g_rw_dbg_mux_0054h_mcu_top_dbgr_1 & 0x08) == 0x08)  // ---> 0xF854[3]
			if ((g_rw_dbg_mux_0018h.byte & 0x08) == 0x08)  // ---> 0xF818[3]
            break;            
    }
}
#endif
bool tcon_check_dl_sta(void)
{
    if(g_rw_mcu_top_0036h.bits.r_hdfailbypass == 1 || g_rw_mcu_top_0036h.bits.r_datafailbypass == 1)
        return HW_DL_FAIL;
    else
        return HW_DL_PASS;
}

void tcon_error_handler(void)
{
    if(!g_rw_mcu_top_0036h.bits.restart_dl_flag)
    {
        if(EXTDAT_SFR_2BH & 0x10)
        {
        g_rw_mcu_top_0035h.bits.r_init_restart = 1;
        chip_rst();
    }
    else
    {
		mcu_uart_puts("EK50215_DBG_Mode\n");
        while(1);
    }
    }
    else
    {
		mcu_uart_puts("EK50215_DBG_Mode\n");
        while(1);
    }
}
