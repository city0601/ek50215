/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: EK50208
		Platform: 8051
		Author: Eric Lee, 2021/03/22
		Description: SEC Transfer
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"
#include "mcu_spi.h"
#include "tcon_lut_transfer.h"
// #include "mcu_dmr_dl.h"
#include "tcon_lut_cal_cks.h"
#include "demura_load.h"
#include "demura.h"
// *******************************************************************
// *******************************************************************
// define
#define SEC_OFFSET	130352      // 0x1FD30
// *******************************************************************
struct r_sec_strength
{
    uint8_t fixed                       : 7;
    uint8_t plane3_0                    : 1;

    uint8_t plane3_1                    : 1;
    uint8_t plane4                      : 2;
    uint8_t plane5                      : 2;
    uint8_t plane6                      : 2;
    uint8_t none_0                      : 1;

    uint8_t none_1                      : 1;
    uint8_t plane0                      : 2;
    uint8_t plane1                      : 2;
    uint8_t plane2                      : 2;
    uint8_t reserved                    : 1;
};

// uint8_t xdata g_sec_dl_buf[8];
uint32_t xdata g_sec_spi_addr = 0;
uint8_t xdata g_fiti_reg;
volatile struct r_sec_strength xdata g_r_sec_strength_u;


static void tcon_lut_transfer_SEC_Fill_to_FitiReg(uint8_t val , uint8_t last_flg)
{
    uint8_t l_cnt;
    uint8_t l_fill_cnt;
    //set plane 
    Lut_Set_Plane_Num(val);
    // fill data
    if(last_flg)
        l_fill_cnt = 7;
    else
        l_fill_cnt = 8;    
    for(l_cnt = 0 ; l_cnt < l_fill_cnt ; l_cnt++)
    {
        // g_fiti_reg = g_sec_dl_buf[l_cnt];
        // Lut_Transfer_Fiti_Cal_Cks(g_sec_dl_buf[l_cnt]);
        g_fiti_reg = g_dmr_trans_cfg_buf[l_cnt];
        Lut_Transfer_Fiti_Cal_Cks(g_dmr_trans_cfg_buf[l_cnt]);
    }
    // fill data
    /*if(last_flg)
    {
        g_fiti_reg = g_sec_dl_buf[0];
        g_fiti_reg = g_sec_dl_buf[1];
        g_fiti_reg = g_sec_dl_buf[2];
        g_fiti_reg = g_sec_dl_buf[3];
        g_fiti_reg = g_sec_dl_buf[4];
        g_fiti_reg = g_sec_dl_buf[5];
        g_fiti_reg = g_sec_dl_buf[6];
    }
    if(last_flg)
        return;
    g_fiti_reg = g_sec_dl_buf[7];  */  
    
}

static void tcon_lut_transfer_fill_Fiti_strength(void)
{
    uint32_t l_sec_hdr_addr;
    
    l_sec_hdr_addr = 0x00;
    mcu_spi_flash_fast_read((uint8_t *)&l_sec_hdr_addr + 1,(uint8_t *)&g_r_sec_strength_u , 3 , 1);


    g_demura_reg_002Fh.bits.strength_m = (g_r_sec_strength_u.plane0 + 2);
    g_demura_reg_0033h.bits.strength_m = g_demura_reg_002Fh.bits.strength_m;
    g_demura_reg_0037h.bits.strength_m = g_demura_reg_002Fh.bits.strength_m;
    // g_rw_fiti_r_885_strength_1_2u.bits.strength_m = (g_r_sec_strength_u.plane0 + 2);
    // g_rw_fiti_g_889_strength_1_2u.bits.strength_m = g_rw_fiti_r_885_strength_1_2u.bits.strength_m;
    // g_rw_fiti_b_88D_strength_1_2u.bits.strength_m = g_rw_fiti_r_885_strength_1_2u.bits.strength_m;

    g_demura_reg_002Fh.bits.strength_l = (g_r_sec_strength_u.plane1 + 2);
    g_demura_reg_0033h.bits.strength_l = g_demura_reg_002Fh.bits.strength_l;
    g_demura_reg_0037h.bits.strength_l = g_demura_reg_002Fh.bits.strength_l;
    // g_rw_fiti_r_885_strength_1_2u.bits.strength_l = (g_r_sec_strength_u.plane1 + 2);
    // g_rw_fiti_g_889_strength_1_2u.bits.strength_l = g_rw_fiti_r_885_strength_1_2u.bits.strength_l;
    // g_rw_fiti_b_88D_strength_1_2u.bits.strength_l = g_rw_fiti_r_885_strength_1_2u.bits.strength_l;

    g_demura_reg_0030h.bits.strength_m = (g_r_sec_strength_u.plane2 + 2);
    g_demura_reg_0034h.bits.strength_m = g_demura_reg_0030h.bits.strength_m;
    g_demura_reg_0038h.bits.strength_m = g_demura_reg_0030h.bits.strength_m;
    // g_rw_fiti_r_886_strength_3_4u.bits.strength_m = (g_r_sec_strength_u.plane2 + 2);
    // g_rw_fiti_g_88A_strength_3_4u.bits.strength_m = g_rw_fiti_r_886_strength_3_4u.bits.strength_m;
    // g_rw_fiti_b_88E_strength_3_4u.bits.strength_m = g_rw_fiti_r_886_strength_3_4u.bits.strength_m;

    g_demura_reg_0030h.bits.strength_l = (g_r_sec_strength_u.plane3_1 << 1 | g_r_sec_strength_u.plane3_0) + 2;
    g_demura_reg_0034h.bits.strength_l = g_demura_reg_0030h.bits.strength_l;
    g_demura_reg_0038h.bits.strength_l = g_demura_reg_0030h.bits.strength_l;
    // g_rw_fiti_r_886_strength_3_4u.bits.strength_l = (g_r_sec_strength_u.plane3_1 << 1 | g_r_sec_strength_u.plane3_0) +2;
    // g_rw_fiti_g_88A_strength_3_4u.bits.strength_l = g_rw_fiti_r_886_strength_3_4u.bits.strength_l;
    // g_rw_fiti_b_88E_strength_3_4u.bits.strength_l = g_rw_fiti_r_886_strength_3_4u.bits.strength_l;

    g_demura_reg_0031h.bits.strength_m = (g_r_sec_strength_u.plane4 + 2);
    g_demura_reg_0035h.bits.strength_m = g_demura_reg_0031h.bits.strength_m;
    g_demura_reg_0039h.bits.strength_m = g_demura_reg_0031h.bits.strength_m;
    // g_rw_fiti_r_887_strength_5_6u.bits.strength_m = (g_r_sec_strength_u.plane4 + 2);
    // g_rw_fiti_g_88B_strength_5_6u.bits.strength_m = g_rw_fiti_r_887_strength_5_6u.bits.strength_m;
    // g_rw_fiti_b_88F_strength_5_6u.bits.strength_m = g_rw_fiti_r_887_strength_5_6u.bits.strength_m;

    g_demura_reg_0031h.bits.strength_l = (g_r_sec_strength_u.plane5 + 2);
    g_demura_reg_0035h.bits.strength_l = g_demura_reg_0031h.bits.strength_l;
    g_demura_reg_0039h.bits.strength_l = g_demura_reg_0031h.bits.strength_l;
    // g_rw_fiti_r_887_strength_5_6u.bits.strength_l = (g_r_sec_strength_u.plane5 + 2);
    // g_rw_fiti_g_88B_strength_5_6u.bits.strength_l = g_rw_fiti_r_887_strength_5_6u.bits.strength_l;
    // g_rw_fiti_b_88F_strength_5_6u.bits.strength_l = g_rw_fiti_r_887_strength_5_6u.bits.strength_l;

    g_demura_reg_0032h.bits.strength_m = (g_r_sec_strength_u.plane6 + 2);
    g_demura_reg_0036h.bits.strength_m = g_demura_reg_0032h.bits.strength_m;
    g_demura_reg_003Ah.bits.strength_m = g_demura_reg_0032h.bits.strength_m;
    // g_rw_fiti_r_888_strength_7_8u.bits.strength_m = (g_r_sec_strength_u.plane6 + 2);
    // g_rw_fiti_g_88C_strength_7_8u.bits.strength_m = g_rw_fiti_r_888_strength_7_8u.bits.strength_m;
    // g_rw_fiti_b_890_strength_7_8u.bits.strength_m = g_rw_fiti_r_888_strength_7_8u.bits.strength_m;

}

void tcon_lut_transfer_SECtoFiti(void)
{
    uint32_t l_sec_spi_addr;
    uint32_t l_spi_dl_len;
    uint32_t l_dl_cnt;
    uint8_t l_sec_offset;
    uint8_t l_last_flg;
    //uint8_t l_demura_cal_cks;
    uint8_t l_read_len;

    P1_0 = 0;

    tcon_lut_transfer_fill_Fiti_strength();

    //---------- Read SEC Checksum ----------//
    //Sec_chks_mode_sel();
	//---------------------------------------//
    l_last_flg = 0;
    //g_sec_spi_addr = 0x1000;
    g_sec_spi_addr = Sec_spi_addr_cal();
    // -------------- Get  read  len  ------------------//
    l_spi_dl_len = Lut_spi_dl_len_cal();
    // -------------- Get  read  len for every read  ------------------//
    l_read_len = g_demura_load_reg_0016h.bits.r_que_rd_len;
    // ------------------------------------------------------------------//

    //Lut_Transfer_Hw_Cal_Cks_En(0);
    //Lut_Transfer_Hw_Cal_Cks_Mode(CKS_SEC_MODE);
    //Lut_Transfer_Hw_Cal_Cks_En(1);
    Lut_Fill_Hw_Reg_En();
    
    //for(l_dl_cnt = 0 ; l_dl_cnt < 130351 ; l_dl_cnt += 8)
    for(l_dl_cnt = 0 ; l_dl_cnt < (l_spi_dl_len - 1) ; l_dl_cnt += l_read_len)
    {
        l_sec_spi_addr = g_sec_spi_addr;
        //if((l_dl_cnt + 8) >= 130351) 
        if((l_dl_cnt + l_read_len) >= (l_spi_dl_len - 1)) 
            l_last_flg = 1;
        for(l_sec_offset = 0; l_sec_offset < 7 ; l_sec_offset++)
        {
            // mcu_spi_flash_fast_read((uint8_t *)&l_sec_spi_addr + 1, (uint8_t *)&g_sec_dl_buf[0] , l_read_len , 1);
            mcu_spi_flash_fast_read((uint8_t *)&l_sec_spi_addr + 1, (uint8_t *)&g_dmr_trans_cfg_buf[0] , l_read_len , 1);  
            tcon_lut_transfer_SEC_Fill_to_FitiReg(7 - l_sec_offset , l_last_flg);
            l_sec_spi_addr += l_spi_dl_len;
        }

        g_sec_spi_addr += l_read_len;
    }

    /*l_demura_cal_cks = 0xFC - (g_r_sys_data_10_u.byte & 0xFF);

    if(!Lut_Transfer_Cmp_Cks(l_demura_cal_cks))
    {
        mcu_dmr_dl_reg_lut_dl_fail_set(1);
		P1_0 = 1;
    }*/
    /*l_demura_cal_cks = 0xFC - (g_demura_cal_cks & 0xFF);
    if(l_demura_cal_cks != g_org_cks)
        P1_0 = 1;*/
    P1_1 = 0;    
}
