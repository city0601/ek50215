/*
		Copyright 2023 FITIPOWER., Ltd.
		Module: N/A
		Project: EK50309
		Platform: 8051
		Author: Eric Lee, 2023/10/04
		Description: spi to led driver
*/
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"

#ifdef LD_LED_DRV_FUN
#include "reg_include.h"
#include "mcu_spi.h"
#include "mcu_i2c.h"
#include "mcu_reg51.h"
#include "tcon_spi_leddrv.h"
#include "tcon_i2c_proc.h"
#include "edp_misc.h"
#include "mcu_timer.h"

//---------------------------------------------------------------------
#define LED_BC_INIT_FIXED

#ifdef LED_BC_INIT_FIXED
    #define LED_BC_INIT_FIXED_VALUE_H    0xFF
    #define LED_BC_INIT_FIXED_VALUE_L    0xFF
#endif
//---------------------------------------------------------------------
#define NO_DAISY_CHAIN                 1
#define CS_ALL_LED                     0xF0
#define LED_FLASH_STRT_ADDR            0x004000     // initial data for led from flash / 16k
#define LED_INIT_FLASH_CS              0

// MBI6334 Flash Definition
#define MBI6334_INIT_FLASH_SIZE        0x0A00

#define MBI6334_MASK_FLASH_ADDR        0x0620     // 0x4620 - LED_FLASH_STRT_ADDR
#define MBI6334_CFG_FLASH_ADDR         0x0000
#define MBI6334_DOT_FLASH_ADDR         0x0420     //
#define MBI6334_BC_FLASH_ADDR          0x0020
#define MBI6334_IDAC_FLASH_ADDR        0x00             // NON USE

#define MBI6334_MASK_DATA_NUM          0x0004           // led is 16bit unit
#define MBI6334_CFG_DATA_NUM           0x0010
#define MBI6334_DOT_DATA_NUM           0X0020
#define MBI6334_BC_DATA_NUM            0x0040
#define MBI6334_IDAC_DATA_NUM          0x00             // NON USE

// MBI6322 Flash Definition / for INX3p 14x14 7 scan / 20230721 add 
#define MBI6322_INIT_FLASH_SIZE        0x0A00             // based on INX 14x14
#define MBI6322_MASK_FLASH_ADDR        0x0620
#define MBI6322_CFG_FLASH_ADDR         0x0000
#define MBI6322_DOT_FLASH_ADDR         0x0420
#define MBI6322_BC_FLASH_ADDR          0x0020
#define MBI6322_IDAC_FLASH_ADDR        0x00             // NON USE

#define MBI6322_MASK_DATA_NUM          0x0002           // led is 16 bit unit
#define MBI6322_CFG_DATA_NUM           0x000E
#define MBI6322_DOT_DATA_NUM           0x0006
#define MBI6322_BC_DATA_NUM            0x0020
#define MBI6322_IDAC_DATA_NUM          0x00             // NON USE

// NT50589 Flash Definition
#define NT50589_INIT_FLASH_SIZE        0x0A00

#define NT50589_MASK_FLASH_ADDR        0x00             // NON USE
#define NT50589_CFG_FLASH_ADDR         0x0000
#define NT50589_DOT_FLASH_ADDR         0x0440           //
#define NT50589_BC_FLASH_ADDR          0x0040
#define NT50589_IDAC_FLASH_ADDR        0x0640

#define NT50589_MASK_DATA_NUM          0x00            // NON USE
#define NT50589_CFG_DATA_NUM           0x0011
#define NT50589_DOT_DATA_NUM           0X0020
#define NT50589_BC_DATA_NUM            0x0040
#define NT50589_IDAC_DATA_NUM          0x0001

#define SRAM_SCAN_SIZE                 64
#define SRAM_SCAN_SHIFT                6
#define RSVD                           0x00
//-----------------------------------------------------------------
//#define BC_SRAM_BASE                    (0x2800)                        
//volatile uint8_t xdata bc_data_sram     _at_ (BC_SRAM_BASE + 0x00);     

volatile uint8_t xdata bc_data_dram[2048]     _at_ (0xE800);

uint8_t xdata g_led_idx = 0;
uint8_t xdata g_led_driver_mode = 0;    // 0: 6334 / 1 : 6332
uint8_t xdata g_block_step = 0;         // add how much sram addr after every bc update / 1 : 2 
uint16_t xdata g_led_strt_addr = 0;     // led bc data register start address
uint8_t xdata g_led_drv_num = 0;        // how much led driver to be controled
uint8_t xdata g_led_addr_step = 0;      // led driver bc_data register step
uint8_t xdata g_led_tx_length1 = 0;
uint8_t xdata g_led_tx_length2 = 0;
uint8_t xdata g_block_map2_sel = 0;     // 0 : map1=others, map2=end(use length2) / 1 : map1=first(use lejgth1), map2=others
uint8_t xdata g_scan_direction = 0;
uint8_t xdata g_led_spi_chksm_en = 0;
uint8_t xdata g_led_vsync_hwpin_en = 0;
uint8_t xdata g_led_vsync_cmd_en = 0;
uint8_t xdata g_led_last_scan = 0;
uint8_t xdata g_led_spcl_scan_offset = 0;
uint8_t xdata g_led_scan_num = 0;
uint8_t xdata g_led_dchain_num = 0;

uint8_t xdata *g_led_sram_ptr;
uint16_t xdata g_led_reg_realtime = 0;   // the real time led register for update bc data 

uint32_t xdata g_spi_dl_ofst_val = 0;   // need check flash size 

bool g_proc_led_last_scan = 0;

led_init_cmd_t xdata led_mask_cmd[3] = 
{
    {0x80, 0x00, 0x00, 0x04, 0x04, 0x00, RSVD, RSVD, 6},    // mbi6334
    {0x04, 0xE0, RSVD, RSVD, RSVD, RSVD, RSVD, RSVD, 2},    // mbi6322 write
    {0xC0, 0x00, 0x01, 0x0F, RSVD, RSVD, RSVD, RSVD, 4},    // NT50589 -> IDAC
};

led_init_cmd_t xdata led_cfg_cmd[3] = 
{
    {0x80, 0x00, 0x00, 0x10, 0x00, 0x00, RSVD, RSVD, 6},    // mbi6334
    {0x00, 0x00, RSVD, RSVD, RSVD, RSVD, RSVD, RSVD, 2},    // mbi6322
    {0x80, 0x00, 0x00, 0x11, 0x00, 0x01, RSVD, RSVD, 6},    // NT50589
};

led_init_cmd_t xdata led_dot_cmd[3] = 
{
    {0x80, 0x00, 0x00, 0x20, 0x02, 0x30, RSVD, RSVD, 6},    // mbi6334
    {0x04, 0x20, RSVD, RSVD, RSVD, RSVD, RSVD, RSVD, 2},    // mbi6322 write
    {0},                                                    // NT50589 
};

led_init_cmd_t xdata led_bc_cmd[3] =  
{
    {0x80, 0x00, 0x00, 0x40, 0x00, 0x20, RSVD, RSVD, 6},    // mbi6334
    {0x00, 0x20, RSVD, RSVD, RSVD, RSVD, RSVD, RSVD, 2},    // mbi6322 write
    {0xBF, 0xFF, 0x01, 0x80, 0x01, 0x10, RSVD, RSVD, 6},    // NT50589 / 20220209
};

led_init_cmd_t xdata led_frame_end_cmd[3] = 
{
    {0xC0, 0x00, 0x00, 0x1F, 0x00, 0x01, RSVD, RSVD, 6},    // mbi6334
    {0x00, 0x1E, 0x00, 0x01, RSVD, RSVD, RSVD, RSVD, 4},    // mbi6322
    {0xC0, 0x00, 0x02, 0x90, 0x00, 0x01, RSVD, RSVD, 6},    // NT50589  The last word is data 0x00, 0x01 
};

led_drv_flash_t xdata g_led_flash[3] =  
{
    {MBI6334_INIT_FLASH_SIZE, 
     MBI6334_MASK_FLASH_ADDR, MBI6334_MASK_DATA_NUM,
     MBI6334_CFG_FLASH_ADDR,  MBI6334_CFG_DATA_NUM,
     MBI6334_DOT_FLASH_ADDR,  MBI6334_DOT_DATA_NUM,
     MBI6334_BC_FLASH_ADDR,   MBI6334_BC_DATA_NUM,
     MBI6334_IDAC_FLASH_ADDR, MBI6334_IDAC_DATA_NUM},

    {MBI6322_INIT_FLASH_SIZE, 
     MBI6322_MASK_FLASH_ADDR, MBI6322_MASK_DATA_NUM,
     MBI6322_CFG_FLASH_ADDR,  MBI6322_CFG_DATA_NUM,
     MBI6322_DOT_FLASH_ADDR,  MBI6322_DOT_DATA_NUM,
     MBI6322_BC_FLASH_ADDR,   MBI6322_BC_DATA_NUM,
     MBI6322_IDAC_FLASH_ADDR, MBI6322_IDAC_DATA_NUM},

    {NT50589_INIT_FLASH_SIZE, 
     NT50589_MASK_FLASH_ADDR, NT50589_MASK_DATA_NUM,
     NT50589_CFG_FLASH_ADDR,  NT50589_CFG_DATA_NUM,
     NT50589_DOT_FLASH_ADDR,  NT50589_DOT_DATA_NUM,
     NT50589_BC_FLASH_ADDR,   NT50589_BC_DATA_NUM,
     NT50589_IDAC_FLASH_ADDR, NT50589_IDAC_DATA_NUM},
};

uint8_t xdata led_unlock_Command[4] ={0xC0, 0x00, 0x00, 0x15};
uint8_t xdata led_unlock_data[2] ={0xAC, 0xCA};
uint8_t xdata led_data_tmp[MBI6334_INIT_FLASH_SIZE];   // initial code size
uint8_t xdata flash_addr_tmp[3];
uint8_t xdata led_cmd_buf[6];

bool g_wait_vsync_flag;
bool g_proc_led_vsync_flg;
bool g_mcu_led_spi_read_done_flg;
bool g_proc_i2c2led_vsync_flg;

bool g_led_update_flag = 0;
bool g_proc_led_on_flg = 0;

uint8_t xdata g_led_update_int_cntr = 0;
uint8_t xdata g_led_last_scan_cntr = 0;

static void tcon_spi_proc_mcu_vsync_pin(void)
{
    if(g_led_vsync_hwpin_en)
    {
        VSYNC_SET(1);
        //mcu_delay(20);         // delay 20us for all type led now / 20220314 
        mcu_timer_wait_us(20);
        VSYNC_SET(0);
    }
}

static uint8_t tcon_config_led_driver(void)
{
    // judge led driver
    g_led_idx = MBI6334_INDEX;
    //g_led_idx = MBI6322_INDEX;

    return g_led_idx;
}

static void tcon_leddrv_wr_frame_end(uint8_t led_idx) 
{
    uint8_t dchain_num;
    uint8_t cs_idx;

    led_init_cmd_t *cmd_ptr = &led_frame_end_cmd[led_idx];

    if(led_idx == MBI6334_INDEX){
        cs_idx = CS_ALL_LED;                // all led's cs pull low
        dchain_num = g_led_drv_num;         // daisy chain 
    }
    else if(led_idx == MBI6322_INDEX){
        cs_idx = CS_ALL_LED;                // all led's cs pull low
        dchain_num = NO_DAISY_CHAIN;        // no daisy chain / all led's cs pull low
    }
    else if(led_idx == NT50589_INDEX){
        cs_idx = CS_ALL_LED;                // all led's cs pull low
        dchain_num = g_led_drv_num;         // no daisy chain / all led's cs pull low
    }

    mcu_spi_write_leddrv_frameend((uint8_t *)cmd_ptr, cmd_ptr->cmd_byte, dchain_num, cs_idx, g_led_spi_chksm_en);
}

void tcon_set_led_bc_config(void)
{
    led_init_cmd_t *cmd_ptr = &led_bc_cmd[g_led_driver_mode];
   
   // this function must ba called after tcon_spi_leddrv_init_handler, 
   // and before update bc data
   if(g_led_driver_mode == MBI6334_INDEX)
   {
        cmd_ptr->cmd_buf0 = 0xBF;              // broadcast cmd 
        cmd_ptr->cmd_buf1 = 0x00;
        cmd_ptr->cmd_buf4 = g_led_strt_addr >> 8;
        cmd_ptr->cmd_buf5 = g_led_strt_addr & 0xFF;
   }
   else if(g_led_driver_mode == MBI6322_INDEX)
   {
        cmd_ptr->cmd_buf0 = (uint8_t)(g_led_strt_addr >> 8);
        cmd_ptr->cmd_buf1 = (uint8_t)(g_led_strt_addr & 0xFF);
   }
   else if(g_led_driver_mode == NT50589_INDEX)
   {
        cmd_ptr->cmd_buf0 = 0xBF;              // broadcast cmd 
        cmd_ptr->cmd_buf1 = 0xFF;
        cmd_ptr->cmd_buf4 = g_led_strt_addr >> 8;
        cmd_ptr->cmd_buf5 = g_led_strt_addr & 0xFF;
   }
}

void tcon_led_proc_display_onoff(uint8_t dp_on)
{
    uint16_t reg_tmp_16 = 0;
    uint8_t l_led_init_val[2];
    uint8_t i = 0;
    led_init_cmd_t *cmd_ptr = &led_bc_cmd[0];

    mcu_spi_set_clk(g_rw_mcu_top_0052h.bits.r_spi_led_clk);

    //if(g_pwm2spi_reg_13_u.bits.pwm2spi_test_mode)
    if(g_rw_ld_0151h.bits.r_pwm2spi_test_mode)
    {
        //l_led_init_val[0] =  g_pwm2spi_reg_14_u.byte;   
        l_led_init_val[0] =  g_rw_ld_0153h_pwm2spi_test_dt1;
        //l_led_init_val[1] = g_pwm2spi_reg_13_u.byte;
        l_led_init_val[1] = g_rw_ld_0152h_pwm2spi_test_dt1;
    }
    else
    {
        if(dp_on)
        {        
            l_led_init_val[0] = 0x0F;
            l_led_init_val[1] = 0xFF;
        }
        else
        {
            l_led_init_val[0] = 0x00;
            l_led_init_val[1] = 0x00;
        }
    }

    if(g_led_driver_mode == MBI6334_INDEX)
    {
        reg_tmp_16 = 0x0020;
        cmd_ptr->cmd_buf0 = 0x80;
        cmd_ptr->cmd_buf1 = 0x00;
        cmd_ptr->cmd_buf2 = 0x00;
        cmd_ptr->cmd_buf3 = 0x40;
        cmd_ptr->cmd_buf4 = 0x00;
        cmd_ptr->cmd_buf5 = 0x20;

        for(i = 0; i < g_led_scan_num; i++)
        {
            mcu_spi_write_leddrv_fixed((uint8_t *)cmd_ptr, cmd_ptr->cmd_byte, &l_led_init_val[0], 128, g_led_drv_num, CS_ALL_LED, g_led_spi_chksm_en);
            reg_tmp_16 += 0x40;
            cmd_ptr->cmd_buf4 = reg_tmp_16 >> 8;
            cmd_ptr->cmd_buf5 = reg_tmp_16 & 0xFF;
        }
    } 
    else if(g_led_driver_mode == MBI6322_INDEX)     
    {
        reg_tmp_16 = 0x0010;
        cmd_ptr->cmd_buf0 = 0x00;
        cmd_ptr->cmd_buf1 = 0x10;

        for(i = 0; i < g_led_scan_num; i++)
        {
            mcu_spi_write_leddrv_fixed((uint8_t *)cmd_ptr, cmd_ptr->cmd_byte, &l_led_init_val[0], 64, NO_DAISY_CHAIN, CS_ALL_LED, g_led_spi_chksm_en);
            reg_tmp_16 += 0x20;
            cmd_ptr->cmd_buf0 = reg_tmp_16 >> 8;
            cmd_ptr->cmd_buf1 = reg_tmp_16 & 0xFF;
        }    
    }

    else if(g_led_driver_mode == NT50589_INDEX)
    {
        reg_tmp_16 = 0x0020;
        cmd_ptr->cmd_buf0 = 0x80;
        cmd_ptr->cmd_buf1 = 0x00;
        cmd_ptr->cmd_buf2 = 0x01;
        cmd_ptr->cmd_buf3 = 0x80;
        cmd_ptr->cmd_buf4 = 0x01;
        cmd_ptr->cmd_buf5 = 0x10;

        for(i = 0; i < g_led_scan_num; i++)
        {
            mcu_spi_write_leddrv_fixed((uint8_t *)cmd_ptr, cmd_ptr->cmd_byte, &l_led_init_val[0], 128, g_led_drv_num, CS_ALL_LED, g_led_spi_chksm_en);
            reg_tmp_16 += 0x40;
            cmd_ptr->cmd_buf4 = reg_tmp_16 >> 8;
            cmd_ptr->cmd_buf5 = reg_tmp_16 & 0xFF;
        }
    }      

    if(g_led_vsync_cmd_en)
        tcon_leddrv_wr_frame_end(g_led_driver_mode);
        
    //mcu_spi_chg_clk(g_mcu_reg_7_u.bits.spi_clk);
    mcu_spi_set_clk(g_rw_mcu_top_001Fh.bits.r_mcu_spi_clk);
    
    // reconfig start address
    g_led_reg_realtime = g_led_strt_addr;
    tcon_set_led_bc_config();
}

void tcon_led_modify_current_gain(uint8_t burst_on)
{
    uint8_t reg_val[2];
    uint8_t cmd_buf[6] = 0;

    if(burst_on)
    {
        //reg_val[0] = g_pwm2spi_reg_rsrvd6;
        reg_val[0] = g_rw_ld_0167h_pwm2spi_mcu_bst_on_dt_msb;
        //reg_val[1] = g_pwm2spi_reg_rsrvd7;
        reg_val[1] = g_rw_ld_0168h_pwm2spi_mcu_bst_on_dt_lsb;
    }
    else
    {
        if(g_led_driver_mode == MBI6334_INDEX)
        {
            reg_val[0] = led_data_tmp[0x08];
            reg_val[1] = led_data_tmp[0x09];
        }
        else if(g_led_driver_mode == MBI6322_INDEX)
        {
            reg_val[0] = led_data_tmp[0x0E];
            reg_val[1] = led_data_tmp[0x0F];
        }
        else if(g_led_driver_mode == NT50589_INDEX)     // IDAC in initial data
        {
            reg_val[0] = led_data_tmp[0x640];
            reg_val[1] = led_data_tmp[0x641];
        }
    }

    if(g_led_driver_mode == MBI6334_INDEX)              // IDAC Register for 6334
    {
        cmd_buf[0] = 0xC0;
        cmd_buf[1] = 0x00;
        cmd_buf[2] = 0x00;
        cmd_buf[3] = 0x04;

        mcu_spi_write_leddrv_fixed((uint8_t *)&cmd_buf, 4, &reg_val[0], 2, g_led_drv_num, CS_ALL_LED, g_led_spi_chksm_en);
    } 
    else if(g_led_driver_mode == MBI6322_INDEX)     
    {
        //reg_tmp_16 = 0x0010;
        cmd_buf[0] = 0x00;
        cmd_buf[1] = 0x07;

        mcu_spi_write_leddrv_fixed((uint8_t *)&cmd_buf, 2, &reg_val[0], 2, NO_DAISY_CHAIN, CS_ALL_LED, g_led_spi_chksm_en);
    }
    else if(g_led_driver_mode == NT50589_INDEX)
    {
        cmd_buf[0] = 0xC0;
        cmd_buf[1] = 0x00;
        cmd_buf[2] = 0x01;
        cmd_buf[3] = 0x0F;

        mcu_spi_write_leddrv_fixed((uint8_t *)&cmd_buf, 4, &reg_val[0], 2, g_led_drv_num, CS_ALL_LED, g_led_spi_chksm_en);
    }       
}


#if 1
void tcon_update_led_config(void)
{
    //g_led_driver_mode = (uint8_t) g_pwm2spi_reg_12_u.bits.mcu_driver_mode;  
    g_led_driver_mode = (uint8_t)g_rw_ld_0150h.bits.r_pwm2spi_mcu_driver_mode;  // 0: 6334 / 1 : 6332
    //g_block_step = (uint8_t)g_pwm2spi_reg_4_u.bits.block_step;               
    g_block_step = (uint8_t)g_rw_ld_0148h.bits.r_pwm2spi_block_step;            // add how much sram addr after every bc update / 1 : 2
    g_block_step = 1 << (g_block_step + 1);                                     // +1 : word->byte
    //g_block_map2_sel = (uint8_t)g_pwm2spi_reg_4_u.bits.map2_sel;            
    g_block_map2_sel = (uint8_t)g_rw_ld_0148h.bits.r_pwm2spi_map2_sel;          // 0 : map1=others, map2=end(use length2) / 1 : map1=first(use lejgth1), map2=others
    //g_led_drv_num = (uint8_t)g_pwm2spi_reg_6_u.bits.mcu_driver_num;         
    g_led_drv_num = (uint8_t)g_rw_ld_014Ah.bits.r_pwm2spi_mcu_driver_num;       // how much led driver to be controled
    //g_led_scan_num = (uint8_t)g_pwm2spi_reg_6_u.bits.mcu_scan_num;
    g_led_scan_num = (uint8_t)g_rw_ld_014Ah.bits.r_pwm2spi_mcu_scan_num;
    //g_led_strt_addr = (uint16_t)((g_pwm2spi_reg_8_u.bits.mcu_rdaddr_st_h << 8) | g_pwm2spi_reg_7_u.bits.mcu_rdaddr_st_l);      // led bc data register start address
    g_led_strt_addr = (uint16_t)((g_rw_ld_014Ch_pwm2spi_mcu_rdaddr_st << 8) | g_rw_ld_014Bh_pwm2spi_mcu_rdaddr_st);
    //g_led_addr_step = (uint8_t)g_pwm2spi_reg_9_u.bits.mcu_rdaddr_step; 
    g_led_addr_step = g_rw_ld_014Dh_pwm2spi_mcu_rdaddr_step;                    // led driver bc_data register step
    //g_led_tx_length1 = (uint8_t)g_pwm2spi_reg_10_u.bits.mcu_dtlen1;
    g_led_tx_length1 = (uint8_t)g_rw_ld_014Eh_pwm2spi_mcu_dtlen1;
    //g_led_tx_length2 = (uint8_t)g_pwm2spi_reg_11_u.bits.mcu_dtlen2;
    g_led_tx_length2 = (uint8_t)g_rw_ld_014Fh_pwm2spi_mcu_dtlen2;

    g_scan_direction = (uint8_t)g_rw_ld_0150h.bits.r_pwm2spi_mcu_rdaddr_dec;              // positive or negative
    g_led_spi_chksm_en = (uint8_t)g_rw_ld_0150h.bits.r_pwm2spi_mcu_chksum_en;
    g_led_vsync_hwpin_en = (uint8_t)g_rw_ld_0150h.bits.r_pwm2spi_mcu_vsync_hwpin_en;
    g_led_vsync_cmd_en = (uint8_t)g_rw_ld_0150h.bits.r_pwm2spi_mcu_vsync_command_en;
    g_led_spcl_scan_offset = (uint8_t)g_rw_ld_0162h_pwm2spi_mcu_reserve1;

    /*g_scan_direction = 0;     // positive or negative
    g_led_spi_chksm_en = 0;
    g_led_vsync_hwpin_en = 0;
    g_led_vsync_cmd_en = 0;
    g_led_spcl_scan_offset = 0;*/

    g_led_reg_realtime = g_led_strt_addr;
}
#endif

void tcon_get_led_config(void)
{
    g_led_driver_mode = (uint8_t)g_rw_ld_0150h.bits.r_pwm2spi_mcu_driver_mode;      // 0: 6334 / 1 : 6332
    g_block_step = (uint8_t)g_rw_ld_0148h.bits.r_pwm2spi_block_step;                // add how much sram addr after every bc update / 1 : 2 
    g_block_step = 1 << (g_block_step + 1);                                         // +1 : word->byte
    g_block_map2_sel = (uint8_t)g_rw_ld_0148h.bits.r_pwm2spi_map2_sel;              // 0 : map1=others, map2=end(use length2) / 1 : map1=first(use lejgth1), map2=others
    g_led_drv_num = (uint8_t)g_rw_ld_014Ah.bits.r_pwm2spi_mcu_driver_num;           // how much led driver to be controled
    g_led_scan_num = (uint8_t)g_rw_ld_014Ah.bits.r_pwm2spi_mcu_scan_num;
    g_led_strt_addr = (uint16_t)((g_rw_ld_014Ch_pwm2spi_mcu_rdaddr_st << 8) | g_rw_ld_014Bh_pwm2spi_mcu_rdaddr_st);      // led bc data register start address
    g_led_addr_step = (uint8_t)g_rw_ld_014Dh_pwm2spi_mcu_rdaddr_step;               // led driver bc_data register step
    g_led_tx_length1 = (uint8_t)g_rw_ld_014Eh_pwm2spi_mcu_dtlen1;
    g_led_tx_length2 = (uint8_t)g_rw_ld_014Fh_pwm2spi_mcu_dtlen2;
    g_scan_direction = (uint8_t)g_rw_ld_0150h.bits.r_pwm2spi_mcu_rdaddr_dec;                  // positive or negative
    g_led_spi_chksm_en = (uint8_t)g_rw_ld_0150h.bits.r_pwm2spi_mcu_chksum_en;
    g_led_vsync_hwpin_en = (uint8_t)g_rw_ld_0150h.bits.r_pwm2spi_mcu_vsync_hwpin_en;
    g_led_vsync_cmd_en = (uint8_t)g_rw_ld_0150h.bits.r_pwm2spi_mcu_vsync_command_en;
    g_led_spcl_scan_offset = (uint8_t)g_rw_ld_0162h_pwm2spi_mcu_reserve1;
    g_led_last_scan = 0;

    // test value / need modify
    /*g_led_driver_mode = 0;     // 0: 6334 / 1 : 6332
    g_block_step = 1 << 6;     // 64 sram 
    g_led_strt_addr = 0x0020;  // led bc data register start address
    g_led_drv_num = 4;         // how much led driver to be controled
    g_led_scan_num = 6;
    g_led_addr_step = 0x40;      // led driver bc_data register step
    g_led_tx_length1 = 60;
    g_led_tx_length2 = 60;
    g_block_map2_sel = 0;      // 0 : map1=others, map2=end(use length2) / 1 : map1=first(use lejgth1), map2=others
    g_scan_direction = 0;      // negative 
    g_led_spi_chksm_en = 1;
    g_led_vsync_hwpin_en = 1;
    g_led_vsync_cmd_en = 1;
    g_led_last_scan = 0;    
    g_led_spcl_scan_offset = 0;*/
    //----------------------

    //g_led_sram_ptr = &bc_data_sram;
    g_led_sram_ptr = &bc_data_dram;

    g_led_reg_realtime = g_led_strt_addr;

    if(g_led_driver_mode == MBI6334_INDEX)
        g_led_dchain_num = g_led_drv_num;
    else if(g_led_driver_mode == MBI6322_INDEX)
        g_led_dchain_num = NO_DAISY_CHAIN;
    else if(g_led_driver_mode == NT50589_INDEX)
        g_led_dchain_num = g_led_drv_num;
}

void tcon_spi_leddrv_init_handler(void) 
{
    uint16_t data_tmp_16, data_tmp1_16 = 0;
    uint32_t l_led_str_addr;
    uint8_t i = 0;
    uint8_t cmda_ori, cmdb_ori;
    uint8_t dchain_num, cs_idx;
    uint8_t idx = 0;
    uint8_t chksm_en = 0;

    led_drv_flash_t *ptr;
    led_init_cmd_t *cmd_ptr;

    //write dram address
    //g_rw_ld_01C5h_pwm_abt_offset = 0x00;    // led pwm data stored in dram with address 0xE800
    //g_rw_ld_01C6h_pwm_abt_offset = 0xE8;

    tcon_get_led_config();
    idx = g_led_driver_mode;
    ptr = &g_led_flash[idx];
    chksm_en = g_led_spi_chksm_en;

    l_led_str_addr = LED_FLASH_STRT_ADDR  + g_spi_dl_ofst_val;

    // read init data from flash
    flash_addr_tmp[0] = (uint8_t)(l_led_str_addr>> 16);
    flash_addr_tmp[1] = (uint8_t)(l_led_str_addr >> 8);
    flash_addr_tmp[2] = (uint8_t)l_led_str_addr;
    mcu_spi_flash_fast_read(flash_addr_tmp, &led_data_tmp[0], ptr->flash_size, LED_INIT_FLASH_CS);

    mcu_spi_set_clk(g_rw_mcu_top_0052h.bits.r_spi_led_clk);

    if(idx == MBI6334_INDEX){
        cs_idx = CS_ALL_LED;              // all led's cs pull low
        dchain_num = g_led_drv_num;       // daisy chain 
    }
    else if(idx == MBI6322_INDEX){
        cs_idx = CS_ALL_LED;              // all led's cs pull low
        dchain_num = NO_DAISY_CHAIN;      // no daisy chain / all led's cs pull low
    }
    else if(idx == NT50589_INDEX){
        cs_idx = CS_ALL_LED;               // all led's cs pull low
        dchain_num = g_led_drv_num;        // no daisy chain / all led's cs pull low
    }
#if 0
    //---------------------------
    // write IDAC data to led driver
    //---------------------------
    if(idx == NT50589_INDEX){             //NT50589 can not get the unclock data from HW
        //cmd_ptr = &led_unlock_Command;
        //mcu_spi_write_leddrv((uint8_t *)cmd_ptr, 0x04, &led_unlock_data, 0x02, dchain_num, cs_idx, chksm_en);
        mcu_spi_write_leddrv((uint8_t *)(&led_unlock_Command), 0x04, &led_unlock_data, 0x02, dchain_num, cs_idx, chksm_en);
		}

    //---------------------------
    // write mask data to led driver
    //---------------------------
    if(idx != NT50589_INDEX){
        cmd_ptr = &led_mask_cmd[idx];
        mcu_spi_write_leddrv((uint8_t *)cmd_ptr, cmd_ptr->cmd_byte, &led_data_tmp[ptr->mask_flash_addr], ptr->mask_data_size << 1, dchain_num, cs_idx, chksm_en);
    }

    //---------------------------
    // write cfg data to led driver
    //---------------------------
    cmd_ptr = &led_cfg_cmd[idx];
    mcu_spi_write_leddrv((uint8_t *)cmd_ptr, cmd_ptr->cmd_byte, &led_data_tmp[ptr->cfg_flash_addr], ptr->cfg_data_size << 1, dchain_num, cs_idx, chksm_en);
    
    //---------------------------
    // DOT
    //---------------------------
    cmd_ptr = &led_dot_cmd[idx];
    if(idx == MBI6334_INDEX)   //6334
    {
        cmda_ori = cmd_ptr->cmd_buf4;
        cmdb_ori = cmd_ptr->cmd_buf5;
        data_tmp_16 = (cmda_ori << 8) | cmdb_ori;                       // led reg addr
        data_tmp1_16 = ptr->dot_flash_addr;                             // led mapping flash addr
        for(i = 0; i < g_led_scan_num; i++) {
            // write mask data to led driver
            (cmd_ptr->cmd_buf4) = (uint8_t)(data_tmp_16 >> 8);
            (cmd_ptr->cmd_buf5) = (uint8_t)(data_tmp_16);
            mcu_spi_write_leddrv((uint8_t *)cmd_ptr, cmd_ptr->cmd_byte, &led_data_tmp[data_tmp1_16], ptr->dot_data_size << 1, dchain_num, cs_idx, chksm_en);
            
            data_tmp_16 += ptr->dot_data_size;    
            data_tmp1_16 += (ptr->dot_data_size << 1); 
        }  
        cmd_ptr->cmd_buf4 = cmda_ori;
        cmd_ptr->cmd_buf5 = cmdb_ori;
				
    }
    else if(idx == MBI6322_INDEX)
    {
        cmda_ori = cmd_ptr->cmd_buf0;
        cmdb_ori = cmd_ptr->cmd_buf1;
        data_tmp_16 = (cmda_ori << 8) | cmdb_ori;                       // led reg addr
        data_tmp1_16 = ptr->dot_flash_addr;                             // led mapping flash addr
        for(i = 0; i < g_led_scan_num; i++) {
            // write mask data to led driver
            (cmd_ptr->cmd_buf0) = (uint8_t)(data_tmp_16 >> 8);
            (cmd_ptr->cmd_buf1) = (uint8_t)(data_tmp_16);
            mcu_spi_write_leddrv((uint8_t *)cmd_ptr, cmd_ptr->cmd_byte, &led_data_tmp[data_tmp1_16], ptr->dot_data_size << 1, dchain_num, cs_idx, chksm_en);
            
            data_tmp_16 = ((data_tmp_16 >> 1) + (ptr->dot_data_size)) << 1;
            //data_tmp_16 += ptr->dot_data_size;    
            data_tmp1_16 += (ptr->dot_data_size << 1); 
        }  
        cmd_ptr->cmd_buf0 = cmda_ori;
        cmd_ptr->cmd_buf1 = cmdb_ori;
    }
    else if(idx == NT50589_INDEX)
    {
        //RES
    }

    //---------------------------
    // IDAC only for NT50589
    //---------------------------    
    if(idx == NT50589_INDEX){
        cmd_ptr = &led_mask_cmd[idx];
        mcu_spi_write_leddrv((uint8_t *)cmd_ptr, cmd_ptr->cmd_byte, &led_data_tmp[ptr->IDAC_flash_addr], ptr->IDAC_data_size << 1, dchain_num, cs_idx, chksm_en);
    }
    //---------------------------
    // BC scan
    //---------------------------
    cmd_ptr = &led_bc_cmd[idx];
/*
#ifdef LED_BC_INIT_FIXED
    for(i = 0; i < 64; i++)
    {
        led_data_tmp[LED_BC_FLASH_ADDR + (i << 1)] = LED_BC_INIT_FIXED_VALUE_H;
        led_data_tmp[LED_BC_FLASH_ADDR + 1 + (i << 1)] = LED_BC_INIT_FIXED_VALUE_L;
    }
#endif
*/
    if(idx == MBI6334_INDEX)   //6334
    {        
        cmda_ori = cmd_ptr->cmd_buf4;
        cmdb_ori = cmd_ptr->cmd_buf5;
        data_tmp_16 = (cmda_ori) << 8 | (cmdb_ori);
        data_tmp1_16 = ptr->bc_flash_addr;
        for(i = 0; i < g_led_scan_num; i++)
        {
            if(i){
                (cmd_ptr->cmd_buf4) = (uint8_t)(data_tmp_16 >> 8);
                (cmd_ptr->cmd_buf5) = (uint8_t)(data_tmp_16);
            }
            mcu_spi_write_leddrv((uint8_t *)cmd_ptr, cmd_ptr->cmd_byte, &led_data_tmp[data_tmp1_16], ptr->bc_data_size << 1, dchain_num, cs_idx, chksm_en);
            data_tmp_16 += ptr->bc_data_size;
            data_tmp1_16 += (ptr->bc_data_size << 1);
        }
        cmd_ptr->cmd_buf4 = cmda_ori;
        cmd_ptr->cmd_buf5 = cmdb_ori;
    }
    else if(idx == MBI6322_INDEX)
    {
        cmda_ori = cmd_ptr->cmd_buf0;
        cmdb_ori = cmd_ptr->cmd_buf1;
        data_tmp_16 = (cmda_ori) << 8 | (cmdb_ori);
        data_tmp1_16 = ptr->bc_flash_addr;
        for(i = 0; i < g_led_scan_num; i++)
        {
            if(i){
                (cmd_ptr->cmd_buf0) = (uint8_t)(data_tmp_16 >> 8);
                (cmd_ptr->cmd_buf1) = (uint8_t)(data_tmp_16);
            }
            mcu_spi_write_leddrv((uint8_t *)cmd_ptr, cmd_ptr->cmd_byte, &led_data_tmp[data_tmp1_16], ptr->bc_data_size << 1, dchain_num, cs_idx, chksm_en);
            
            data_tmp_16 = ((data_tmp_16 >> 1) + (ptr->bc_data_size)) << 1;
            data_tmp1_16 += (ptr->bc_data_size << 1);
        }
        cmd_ptr->cmd_buf0 = cmda_ori;
        cmd_ptr->cmd_buf1 = cmdb_ori;
    }
    
    else if(idx == NT50589_INDEX)
    {        
        cmda_ori = cmd_ptr->cmd_buf4;
        cmdb_ori = cmd_ptr->cmd_buf5;
        data_tmp_16 = (cmda_ori) << 8 | (cmdb_ori);
        data_tmp1_16 = ptr->bc_flash_addr;

        mcu_spi_write_leddrv((uint8_t *)cmd_ptr, cmd_ptr->cmd_byte, &led_data_tmp[data_tmp1_16], 0x300, dchain_num, cs_idx, chksm_en);
        cmd_ptr->cmd_buf4 = cmda_ori;
        cmd_ptr->cmd_buf5 = cmdb_ori;
    }
    // frame end
    if(g_led_vsync_cmd_en)
        tcon_leddrv_wr_frame_end(idx);
    
    //mcu_delay(5);     // 20220216 delay 5us
    mcu_timer_wait_us(5);

    tcon_spi_proc_mcu_vsync_pin();

    tcon_set_led_bc_config();
#endif
    mcu_spi_set_clk(g_rw_mcu_top_001Fh.bits.r_mcu_spi_clk);
}

static void tcon_leddrv_update_bc(uint8_t idx, uint8_t scan_idx, uint16_t len)
{
    uint16_t led_reg_16 = 0;
    uint8_t chksm_en = g_led_spi_chksm_en;

    uint8_t cs_idx = 0;
    uint8_t i = 0;

    led_init_cmd_t *cmd_ptr = &led_bc_cmd[idx];

    if(idx == MBI6334_INDEX)   // 210604 modify
    {
        cs_idx = CS_ALL_LED;
        //len = len * g_led_drv_num;
        cmd_ptr->cmd_buf3 = len >> 1;

        // check special scan offset
        if((g_proc_led_last_scan && g_block_map2_sel == 0) || (scan_idx == 0 && g_block_map2_sel == 1 ))
        {
            g_led_reg_realtime += g_led_spcl_scan_offset;
            cmd_ptr->cmd_buf4 = g_led_reg_realtime >> 8;
            cmd_ptr->cmd_buf5 = g_led_reg_realtime & 0xFF;
        }

        mcu_spi_write_leddrv_bc((uint8_t *)cmd_ptr, cmd_ptr->cmd_byte, g_led_sram_ptr, len, g_led_drv_num, cs_idx, chksm_en);
        
        if(g_proc_led_last_scan){
            //g_led_sram_ptr = &bc_data_sram;
            g_led_sram_ptr = &bc_data_dram;
            g_led_reg_realtime = g_led_strt_addr;
            cmd_ptr->cmd_buf4 = g_led_reg_realtime >> 8;
            cmd_ptr->cmd_buf5 = g_led_reg_realtime & 0xFF;
        }
        else{

            //if((g_led_last_scan & g_block_map2_sel == 0) || (scan_idx == 0 & g_block_map2_sel == 1 ))
               //g_led_sram_ptr -= (g_led_spcl_scan_offset << 1);

            g_led_sram_ptr += (g_block_step * g_led_drv_num);

            if(g_scan_direction == 0)
            {
                if(scan_idx == 0 & g_block_map2_sel == 1 )
                    g_led_reg_realtime += (g_led_addr_step - g_led_spcl_scan_offset);
                else
                    g_led_reg_realtime += g_led_addr_step;
            }
            else
            {
                if(scan_idx == 0 & g_block_map2_sel == 1 )
                    g_led_reg_realtime -= (g_led_addr_step + g_led_spcl_scan_offset);
                else 
                    g_led_reg_realtime -= g_led_addr_step;
            }
            cmd_ptr->cmd_buf4 = g_led_reg_realtime >> 8;
            cmd_ptr->cmd_buf5 = g_led_reg_realtime & 0xFF;
        }
    }
    else if(idx == MBI6322_INDEX)
    {
        // check special scan offset
        if((g_proc_led_last_scan && g_block_map2_sel == 0) || (scan_idx == 0 && g_block_map2_sel == 1 ))
        {
            g_led_reg_realtime += g_led_spcl_scan_offset;
            cmd_ptr->cmd_buf0 = g_led_reg_realtime >> 8;
            cmd_ptr->cmd_buf1 = g_led_reg_realtime & 0xFF;

            g_led_sram_ptr += (g_led_spcl_scan_offset << 1);
        }

        for(i = 0; i < g_led_drv_num; i++)
        {
			cs_idx = (uint8_t)~(1 << i);     // 230720 peter modified 
            mcu_spi_write_leddrv((uint8_t *)cmd_ptr, cmd_ptr->cmd_byte, g_led_sram_ptr, len, NO_DAISY_CHAIN, cs_idx, chksm_en);
            if(g_led_drv_num - (i+1))        // 230724 peter add / add block step only for 2 more led_drv_num
			    g_led_sram_ptr += g_block_step;
        }

        if(g_proc_led_last_scan){
            //g_led_sram_ptr = &bc_data_sram;
            g_led_sram_ptr = &bc_data_dram;
            g_led_reg_realtime = g_led_strt_addr;
            cmd_ptr->cmd_buf0 = (uint8_t)(g_led_reg_realtime >> 8);
            cmd_ptr->cmd_buf1 = (uint8_t)(g_led_reg_realtime);
        }
        else{
            if((g_proc_led_last_scan & g_block_map2_sel == 0) || (scan_idx == 0 & g_block_map2_sel == 1 ))
               g_led_sram_ptr -= (g_led_spcl_scan_offset << 1);

            g_led_sram_ptr += g_block_step;

            if(g_scan_direction == 0)
            {
                if(scan_idx == 0 & g_block_map2_sel == 1 )
                    g_led_reg_realtime += (g_led_addr_step - g_led_spcl_scan_offset);
                else
                    g_led_reg_realtime += g_led_addr_step;
            }
            else
            {
                if(scan_idx == 0 & g_block_map2_sel == 1 )
                    g_led_reg_realtime -= (g_led_addr_step + g_led_spcl_scan_offset);
                else 
                    g_led_reg_realtime -= g_led_addr_step;
            }

            cmd_ptr->cmd_buf0 = (uint8_t)(g_led_reg_realtime >> 8);
            cmd_ptr->cmd_buf1 = (uint8_t)(g_led_reg_realtime);
        }
    }

    else if(idx == NT50589_INDEX)   // 
    {
        cs_idx = CS_ALL_LED;
        //len = len * g_led_drv_num;
        cmd_ptr->cmd_buf3 = len >> 1;

        // check special scan offset
        if((g_proc_led_last_scan && g_block_map2_sel == 0) || (scan_idx == 0 && g_block_map2_sel == 1 ))
        {
            g_led_reg_realtime += g_led_spcl_scan_offset;
            cmd_ptr->cmd_buf4 = g_led_reg_realtime >> 8;
            cmd_ptr->cmd_buf5 = g_led_reg_realtime & 0xFF;
        }

        mcu_spi_write_leddrv_bc((uint8_t *)cmd_ptr, cmd_ptr->cmd_byte, g_led_sram_ptr, len, g_led_drv_num, cs_idx, chksm_en);
        
        if(g_proc_led_last_scan){
            //g_led_sram_ptr = &bc_data_sram;
            g_led_sram_ptr = &bc_data_dram;
            g_led_reg_realtime = g_led_strt_addr;
            cmd_ptr->cmd_buf4 = g_led_reg_realtime >> 8;
            cmd_ptr->cmd_buf5 = g_led_reg_realtime & 0xFF;
        }
        else
        {
            //if((g_led_last_scan & g_block_map2_sel == 0) || (scan_idx == 0 & g_block_map2_sel == 1 ))
               //g_led_sram_ptr -= (g_led_spcl_scan_offset << 1);

            g_led_sram_ptr += (g_block_step * g_led_drv_num);

            if(g_scan_direction == 0)
            {
                if(scan_idx == 0 & g_block_map2_sel == 1 )
                    g_led_reg_realtime += (g_led_addr_step - g_led_spcl_scan_offset);
                else
                    g_led_reg_realtime += g_led_addr_step;
            }
            else
            {
                if(scan_idx == 0 & g_block_map2_sel == 1 )
                    g_led_reg_realtime -= (g_led_addr_step + g_led_spcl_scan_offset);
                else 
                    g_led_reg_realtime -= g_led_addr_step;
            }
            cmd_ptr->cmd_buf4 = g_led_reg_realtime >> 8;
            cmd_ptr->cmd_buf5 = g_led_reg_realtime & 0xFF;
        }
    }
}

void tcon_spi_leddrv_update_handler(void)
{
    static uint8_t scan_idx = 0;
    uint16_t len = 0;
    uint8_t idx = g_led_driver_mode;

    if(g_proc_i2c2led_vsync_flg)    // i2c over led vsync cmd
    {
        g_proc_i2c2led_vsync_flg = 0;
        if(g_led_vsync_cmd_en)
            tcon_leddrv_wr_frame_end(idx);  
        //mcu_delay(5);               // 20220225 delay 5us for 50589
        mcu_timer_wait_us(5);
        tcon_spi_proc_mcu_vsync_pin();
    }

    if(g_proc_led_vsync_flg)
    {
        g_proc_led_vsync_flg = 0;
        tcon_spi_proc_mcu_vsync_pin();
    }

    if(g_proc_led_on_flg)
    {
        g_proc_led_on_flg = 0;
        tcon_led_proc_display_onoff(1);
    }

	if(!g_led_update_flag)
        return;
	  
	// Peter add / for short time last scan int / 230512
	if(g_led_update_int_cntr)
	    g_led_update_int_cntr --;
		
    if(g_led_update_int_cntr == 0)
	    g_led_update_flag = 0;      // clear when no pending interrupt

	if(g_led_last_scan_cntr)
    {
        g_led_last_scan_cntr --;
        if(g_led_last_scan_cntr == 0)
        {
            if(scan_idx < (g_led_scan_num - 1)) 
            {
                g_proc_led_last_scan = 0;
                g_led_update_flag = 1;
            }
            else if(scan_idx == (g_led_scan_num - 1))
            {
                g_proc_led_last_scan = 1;
                g_led_last_scan = 0;
                g_led_update_flag = 0;					 
            }
        }
        else
            g_proc_led_last_scan = 0;
    }
    else
    {
        if(g_led_last_scan)
        {
            g_proc_led_last_scan = 1;
            g_led_last_scan = 0;
        }
        else
            g_proc_led_last_scan = 0;
    }

    // decide block map2
    if(g_block_map2_sel)
    {                                           // g_block_map2_sel = 1 / first scan is special, use map1
        if(scan_idx == 0)
            len = g_led_tx_length1 << 1;
        else
            len = g_led_tx_length2 << 1;
    }
    else
    {
        if(g_proc_led_last_scan)               // g_block_map2_sel = 0 / last scan is special, use map2
            len = g_led_tx_length2 << 1;
        else
            len = g_led_tx_length1 << 1;
    }

    //mcu_spi_led_chg_clk(g_mcu_reg_7_u.bits.spi_led_clk);
    mcu_spi_set_clk(g_rw_mcu_top_0052h.bits.r_spi_led_clk);

    // update BC
    tcon_leddrv_update_bc(idx, scan_idx, len);      // direct write from sram pointer / len is byte unit
    scan_idx ++;
		
    if(g_proc_led_last_scan)                        // this variable must be set in interrupt
    {
        if(!g_rw_ld_0164h.bits.r_pwm2spi_mcu_vsync_sel)   // need check
        {
						// need modify / JD1730 in irq
            /*while(!g_r_corestate_u.bits.vblkrdy);
            g_r_corestate_u.bits.vblkrdy = 0;*/
        }
        
        // check burst on/off
				// need modify / JD1730 in irq
        /*if(g_pwm2_led_reg_0x1E5_u.bits.bst_on)
            tcon_led_modify_current_gain(1);
        else if(g_pwm2_led_hw_sta_u.bits.bst_off)
            tcon_led_modify_current_gain(0);*/
        
        //mcu_delay(5);     // 20220225 update to 5us
        mcu_timer_wait_us(5);

        // Frame end
        if(g_led_vsync_cmd_en)
            tcon_leddrv_wr_frame_end(idx);

        //mcu_delay(5);     // 20220216 update to 5us
        mcu_timer_wait_us(5);
        tcon_spi_proc_mcu_vsync_pin();
        scan_idx = 0;
    }  

    //mcu_spi_chg_clk(g_mcu_reg_7_u.bits.spi_clk);
    mcu_spi_set_clk(g_rw_mcu_top_001Fh.bits.r_mcu_spi_clk);
}

void tcon_check_ld_event(void)              // call by ISR4
{
    static uint8_t l_double_ck_cnt = 0;
    // get event
	//----------------------------
	union tcon_isr_1 tcon_isr_event_1_temp;
	while(tcon_isr_event_1.byte)
	{
	    tcon_isr_event_1_temp.byte = tcon_isr_event_1.byte;
	    tcon_clr_irq_event();
	    //----------------------------

        //if(tcon_isr_event_1.bits.vsync_isr)
		if(tcon_isr_event_1_temp.bits.vsync_isr)   // modify
        {
            //P2_1 = 1;
            if(g_r_fiti_localdimmig_cfg.bits.ip_en == 0 & g_r_pwm2spi_ldoff.bits.ledoff_on_flg)
                g_proc_led_on_flg = 0;       // Jim ask to disable led all bright function
            mipi_check_corestate();
            if(g_r_corestate_u.bits.state == CORE_FREE_RUN_MODE)
            {
                g_glitch_flg = 1;
                l_double_ck_cnt = 0;
            }
            else if(g_glitch_flg)
            {
                l_double_ck_cnt ++;
                if(l_double_ck_cnt > 1)
                {
                    l_double_ck_cnt = 0;
                    g_glitch_flg = 0;
                }
            }
        }

        if(g_wait_vsync_flag)
        {
            g_wait_vsync_flag = 0;
            g_proc_led_vsync_flg = 1;
        }

    //if(tcon_isr_event_1.bits.pwm2spi_isr & g_r_corestate_u.bits.state != CORE_FREE_RUN_MODE)       // update LED
        if(tcon_isr_event_1_temp.bits.pwm2spi_isr & g_r_corestate_u.bits.state != CORE_FREE_RUN_MODE)
		{
            if(g_glitch_flg || mcu_i2c2led_mode || g_proc_aux_over_led_flg)
            {
                g_led_update_flag = 0;
            }
            else
            {
                g_led_update_flag = 1;       // set update led bc data flag
				g_led_update_int_cntr ++;    // Peter add / for short time last scan int / 230512
				//P0_1 = ~P0_1;    // test gpio
        
                if(g_pwm2_led_hw_sta_u.bits.pwm2led_last_flg)
                {
                    // last led flow ... gpio2[0]: Gpio vsync
                    g_led_last_scan = 1;
					g_led_last_scan_cntr = g_led_update_int_cntr;   // Peter add / for short time last scan int / 230512
                    // hw auto clear bit  
                }
                else
                    g_led_last_scan = 0;
            }
        }
	}
}
#endif