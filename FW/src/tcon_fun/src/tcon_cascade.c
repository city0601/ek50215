/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: 
		Platform: 8051
		Author: Eric Lee, 2023/09/28
		Description: Mcu Download 
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"
#include "tcon_cascade.h"
#include "mcu_int.h"
#include "reg_include.h"
#include "edp_global_vars.h"
#define VX1_TYPE 0
// *******************************************************************
// *******************************************************************
bool g_master_flg;
bool g_cascade_tx_flg;

rx_cscd_struct xdata g_rx_cscd;
uint8_t xdata g_cascade_flag_rcv_rx_int = 0;
// *******************************************************************
static void tcon_cascade_detect_ms_slv(void)
{
	g_master_flg = g_rw_mcu_top_0051h.bits.ms_chip_sel;
}

void tcon_tx_cascade(uint8_t *buf)
{
    g_rw_mcu_top_0045h_cscd_tx_data = *buf;
    g_rw_mcu_top_0046h_cscd_tx_data = *(buf + 1);
    g_rw_mcu_top_0047h_cscd_tx_data = *(buf + 2);
    g_rw_mcu_top_0048h_cscd_tx_data = *(buf + 3);
    g_cascade_tx_flg = 1;
    // trigger en
    g_rw_mcu_top_0044h.bits.r_cscd_tx_en = 1;
}

void tcon_rx_cascade(void)      // decode recv data / call in main loop
{
    if(g_rw_mcu_top_004Dh.bits.r_cscd_rx_valid)
    {
        g_rw_mcu_top_004Fh.bits.r_cscd_rx_clr = 1;
        g_rx_cscd.id = g_rw_mcu_top_0049h_cscd_rx_data;
        g_rx_cscd.val[0] = g_rw_mcu_top_004Ah_cscd_rx_data;
        g_rx_cscd.val[1] = g_rw_mcu_top_004Bh_cscd_rx_data;
        g_rx_cscd.val[2] = g_rw_mcu_top_004Ch_cscd_rx_data;
        g_cascade_tx_flg = 0;
    }

    if(g_rw_mcu_top_004Dh.bits.r_rx_fifo_ovr)
        g_rw_mcu_top_004Fh.bits.r_rx_ovr_clr = 1;
}

bool tcon_src_type(void)    //  0 :VX1 , 1 : eDP , default : 0
{
	return g_rw_sys_00DBh.bits.r_rx_src_sel;
}

void tcon_ctrl_sync(bool en) // from master to slave
{
    g_rw_inproc_0054h.bits.r_edp_vx1_ready_sync_flag = en;
}

void tcon_lock_status(void)
{
    if(tcon_src_type() == VX1_TYPE)
    {
        tcon_ctrl_sync(g_rw_vx1_0024h.bits.vx1_ok_o);
    }
    else  // eDP
    {
		// sync link rate / lane count / link status
        tcon_ctrl_sync(0);
    }
}

void tcon_enable_cascade_interrupt(void)
{
    //g_cascade_flag_rcv_rx_int = 0;
    tcon_cascade_detect_ms_slv();
    mcu_int_int8_en();
    g_rw_mcu_top_004Eh.bits.r_cscd_rx_int_en = 1;
}


