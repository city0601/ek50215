/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: EK50208
		Platform: 8051
		Author: Eric Lee, 2021/03/19
		Description: i2c over spi
*/
// *******************************************************************
// Include Path
#include <string.h>
#include "mcu_fun_def.h"
#include "reg_include.h"
#include "mcu_spi.h"
#include "mcu_i2c.h"
//#include "mcu_reg.h"
#include "mcu_top.h"
#include "mcu_dma.h"
#include <intrins.h>          //for _nop_();
#include "tcon_i2c_proc.h"
#include "CSOT_I2c_Slv.h"
#include "tcon_lut_i2c_slv.h"
#include "sys.h"
#include "CSOT_RTPM.h"
#include "mcu_int.h"
// *******************************************************************

bool g_P0_0_dlg_pre;
bool g_P0_1_dlg_pre;

//#ifdef I2C_OVER_SPI_FUN
#if 1
//#define I2C_OVER_SPI_CS_SEL     (g_mcu_reg_005h.bits.r_i2c_over_spi_sel)
#define I2C_OVER_SPI_CS_SEL     g_spi_path_flg

//#define NULL ((char *)0)

typedef struct {
    uint8_t i2c2spi_id;
    uint16_t cmd_len;
    mcu_i2c2spi_func_ptr cmd_func;
} mcu_i2c2spi_cmd_struct;
static void mcu_i2c2spi_func_write_status(void);
static void mcu_i2c2spi_func_page_program(void);
static void mcu_i2c2spi_func_read_data(void);
static void mcu_i2c2spi_func_read_status(void);
static void mcu_i2c2spi_func_flash_erase(void);
static void mcu_i2c2spi_func_chip_erase(void);
static void mcu_i2c2spi_func_read_tcon_id(void);
static void mcu_i2c2spi_func_sw_path(void);
static void mcu_i2c2spi_func_read_mcu_status(void);
static void i2c_cmd_02_proc(void);
static void i2c_cmd_05_proc(void);
//static void OD_Fun(void);
static void i2c_cmd_fe_proc(void);
// static void mcu_i2c2spi_func_debug_f2h(void);
//void test_fun_1(void);
//static void test_fun_2(void);
//static void test_fun_3(void);

//#ifdef FLASH_CODE
#if 1
mcu_i2ccmd_struct xdata mcu_i2c_cmd_table[] = {
/* 0x00 - 0x0F */    
NULL,NULL,NULL,NULL,mcu_i2c2spi_func_read_tcon_id,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,CSOT_I2c_Slv_Exit_Bypass_Mode,
/* 0x10 - 0x1F */	
NULL,CSOT_I2c_Slv_Read_Tcon_Vendor_Tcon_ID,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
/* 0x20 - 0x2F */
NULL,NULL,CSOT_Read_GammaCodeToTconReg,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
/* 0x30 - 0x3F */
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
/* 0x40 - 0x4F */
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
/* 0x50 - 0x5F */
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
/* 0x60 - 0x6F */
NULL,NULL,NULL,NULL,NULL,NULL,NULL,CSOT_I2c_Slv_I2c_Mst_Read_PMIC,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
/* 0x70 - 0x7F */
// CSOT_Spi_Read_Flash_Jedec_ID,CSOT_I2C_Slv_Read_t10VCOM_from_Flash,CSOT_I2C_Slv_Write_t10VCOM_to_Flash,CSOT_I2C_Slv_Read_t10VCOM_from_Flash,CSOT_I2C_Slv_Write_t10VCOM_to_Flash,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
/* 0x80 - 0x8F */
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
/* 0x90 - 0x9F */
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,CSOT_I2c_Slv_I2c_Mst_Write_PMIC,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
/* 0xA0 - 0xAF */
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
/* 0xB0 - 0xBF */
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
/* 0xC0 - 0xCF */
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,CSOT_Switch_Mode_Cmd,NULL,NULL,
/* 0xD0 - 0xDF */
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,CSOT_Write_GammaCodeToTconReg,NULL,NULL,
/* 0xE0 - 0xEF */
NULL,NULL,NULL,NULL,NULL,NULL,NULL,CSOT_DGCOff_Cmd,CSOT_DGCOn_Cmd,CSOT_DemuraOff_Cmd,CSOT_DemuraOn_Cmd,CSOT_Ctrl_TconReset_Cmd,CSOT_Read_TconResetStaFormTconReg_Cmd,CSOT_Read_TconDemuraStaFormTconReg_Cmd,NULL,NULL,
/* 0xF0 - 0xFF */
CSOT_I2c_Slv_Enter_Bypass_Mode,NULL,CSOT_I2c_Slv_Det_I2ctoSPI_Mode,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,i2c_cmd_fe_proc,NULL
};
#endif
#if 0
uint8_t xdata mcu_spicmd_list[] = {
    0xFF,0x02,0x03,0x00,0x09,0x01,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 0x00 - 0x0F 
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 0x10 - 0x1F
    0x04,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 0x20 - 0x2F
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 0x30 - 0x3F
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 0x40 - 0x4F
    0xFF,0xFF,0x05,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 0x50 - 0x5F
    0x08,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 0x60 - 0x6F
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 0x70 - 0x7F
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 0x80 - 0x8F
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 0x90 - 0x9F
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 0xA0 - 0xAF
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 0xB0 - 0xBF
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x07,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 0xC0 - 0xCF
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x06,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 0xD0 - 0xDF
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 0xE0 - 0xEF
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF  // 0xF0 - 0xFF
};

mcu_i2c2spi_cmd_struct xdata mcu_i2c2spi_cmd_table[] = {
    {4,      mcu_i2c2spi_func_read_data},
    {1,      mcu_i2c2spi_func_read_status},
    {2,      mcu_i2c2spi_func_write_status},
    {260,    mcu_i2c2spi_func_page_program},
    {4,      mcu_i2c2spi_func_flash_erase},
    {4,      mcu_i2c2spi_func_flash_erase},
    {4,      mcu_i2c2spi_func_flash_erase},
    {1,      mcu_i2c2spi_func_chip_erase},
    {1,      mcu_i2c2spi_func_chip_erase},
    {1,      mcu_i2c2spi_func_read_tcon_id}, // Read TCON Chip ID
    // DEBUG
    // {SPI_FLASH_CMD_DEBUG_F2H,           1,      mcu_i2c2spi_func_debug_f2h}
};
#endif
#if 0
mcu_i2c2spi_cmd_struct xdata mcu_i2c2spi_cmd_table[] = {
    {SPI_FLASH_CMD_READ_DATA,           4,      mcu_i2c2spi_func_read_data},
    {SPI_FLASH_CMD_READ_STATUS1,        1,      mcu_i2c2spi_func_read_status},
    {SPI_FLASH_CMD_READ_MCU_STATUS,     1,      mcu_i2c2spi_func_read_mcu_status},
    {SPI_FLASH_CMD_WRITE_STATUS,        2,      mcu_i2c2spi_func_write_status},
    {SPI_FLASH_CMD_PAGE_PROGRAM,        260,    mcu_i2c2spi_func_page_program},
    {SPI_FLASH_CMD_SECTOR_ERASE,        4,      mcu_i2c2spi_func_flash_erase},
    {SPI_FLASH_CMD_BLOCK_ERASE_32KB,    4,      mcu_i2c2spi_func_flash_erase},
    {SPI_FLASH_CMD_BLOCK_ERASE_64KB,    4,      mcu_i2c2spi_func_flash_erase},
    {SPI_FLASH_CMD_CHIP_ERASE,          1,      mcu_i2c2spi_func_chip_erase},
    {SPI_FLASH_CMD_CHIP_ERASE2,         1,      mcu_i2c2spi_func_chip_erase},
    {SPI_FLASH_CMD_READ_TCON_ID,        1,      mcu_i2c2spi_func_read_tcon_id}, // Read TCON Chip ID
    {SPI_FLASH_CMD_SW_PATH,             2,      mcu_i2c2spi_func_sw_path},
    // DEBUG
    // {SPI_FLASH_CMD_DEBUG_F2H,           1,      mcu_i2c2spi_func_debug_f2h}
};
#endif
static uint8_t xdata mcu_i2c2spi_cmd_idx;
static uint32_t mcu_i2c2spi_read_data_addr;
static uint8_t xdata mcu_i2c2spi_tcon_id[] = "EK58216AA_App";
static bool g_vsync_irq_ex3_dlg_flg;
static bool g_update_fifo_flag;
//static uint8_t g_update_fifo_adr;

bool g_dlg_update_flg = 0;
bool g_vrr_update_flg = 0;
bool g_clear_od_chg_lut_en = 0;
uint8_t xdata g_dlg_i2c_data_buf = 0x00;
uint8_t xdata g_vrr_i2c_data_buf = 0x00;

//extern void FITI_Mmecpy_fun(uint8_t* src,uint8_t* dst ,uint8_t len);

void tcon_i2c_proc_init(void)
{
    mcu_i2c2spi_cmd_init = FALSE;
    mcu_i2c2spi_cmd_doing = FALSE;
    mcu_i2c2spi_cmd_buf_len = 0;
    mcu_i2c2spi_reply_data = 0;
	mcu_i2c2spi_cmd_idx = 0xFF;
	mcu_i2c2spi_cmd_buf_idx = 0;
    mcu_i2c2spi_read_data_addr = 0;

}
#ifdef OD_FUN
static void OD_Fun(void)
{
    if (mcu_i2c2spi_cmd_init) 
    {
        if(g_mcu_i2c2spi_dev_adr != TCON_OD_SLV_ADR) 
            return;
        if(mcu_i2c2spi_cmd_buf[1] != 0x09)
            return;
        
        g_rw_os_00D4h.bits.r_temp_chg_lut_en = 1;
        //g_rw_os_00D5h.bits.r_lut_sel = mcu_i2c2spi_cmd_buf[2];
        g_rw_os_00D4h.bits.r_lut_sel_bits_1_0 = (mcu_i2c2spi_cmd_buf[2] & 0x03);
        g_rw_os_00D5h.bits.r_lut_sel_bits_2 = (mcu_i2c2spi_cmd_buf[2] & 0x04) >> 2;

        g_clear_od_chg_lut_en = 1;
        //_nop_();
        //_nop_();
        //g_os_reg_00D5h.bits.r_temp_chg_lut_en = 0;
    }
}
#endif
#if 0
static void mcu_i2c2spi_func_prepare_reply_data(void)
{
    if (mcu_i2c2spi_cmd_buf_idx < mcu_i2c2spi_cmd_buf_len) {
        mcu_i2c2spi_reply_data = mcu_i2c2spi_cmd_buf[mcu_i2c2spi_cmd_buf_idx];
        mcu_i2c2spi_cmd_buf_idx++;
    }
    else {
        mcu_i2c2spi_reply_data = 0;
    }
}
#endif
#ifdef FLASH_CODE
static void mcu_i2c2spi_func_write_status(void)
{
    if (mcu_i2c2spi_cmd_init) {
        if((g_mcu_i2c2spi_dev_adr !=  MCU_I2C_SLA_W_I2C2SPI)|| (mcu_i2c2spi_recv_len != 2))
            return;
        mcu_spi_flash_write_status(mcu_i2c2spi_cmd_buf[1], I2C_OVER_SPI_CS_SEL);
    }
}

static void mcu_i2c2spi_func_page_program(void)
{
    if (mcu_i2c2spi_cmd_init) {
        if((g_mcu_i2c2spi_dev_adr !=  MCU_I2C_SLA_W_I2C2SPI) || (mcu_i2c2spi_recv_len != 260)) return;
        mcu_spi_flash_page_program(mcu_i2c2spi_cmd_buf + 1, mcu_i2c2spi_cmd_buf + 4, 0x01 , 0x00, I2C_OVER_SPI_CS_SEL);
    }
}

void update_fifo_buf(void)
{
    if(mcu_i2c2spi_cmd_buf_idx > 63)   // 127
    {
        mcu_i2c2spi_cmd_buf_idx = 0;
        if(g_update_fifo_flag == 0)
        {
            //mcu_i2c2spi_cmd_buf_idx = 0;
            mcu_spi_flash_fast_read((uint8_t *)&mcu_i2c2spi_read_data_addr+1, &g_i2c2spi_read_buf[0], 64 ,I2C_OVER_SPI_CS_SEL); // 64
            g_update_fifo_flag = 1;
        }
        else
        {
            mcu_dma_ch0_rst();
            mcu_spi_flash_fast_read((uint8_t *)&mcu_i2c2spi_read_data_addr+1, &g_i2c2spi_read_buf[64], 64 ,I2C_OVER_SPI_CS_SEL); // 64
			g_update_fifo_flag = 0;
        }
        mcu_i2c2spi_read_data_addr += 64;
    }
}

static void mcu_i2c2spi_func_read_data(void)
{
    if((g_mcu_i2c2spi_dev_adr !=  MCU_I2C_SLA_W_I2C2SPI) || (mcu_i2c2spi_recv_len != 4)) return;
    if (mcu_i2c2spi_cmd_init) 
    {
        VAR_MERGER_32(mcu_i2c2spi_read_data_addr, mcu_i2c2spi_cmd_buf[1]);
        mcu_i2c2spi_read_data_addr >>= 8;
        mcu_spi_flash_fast_read((uint8_t *)&mcu_i2c2spi_read_data_addr+1, &g_i2c2spi_read_buf[0], 128 ,I2C_OVER_SPI_CS_SEL);    // 128

        g_update_fifo_flag = 0;
        mcu_i2c2spi_read_data_addr += 128;// 128

    }
    else if(mcu_i2c2spi_cmd_doing)
    {
        update_fifo_buf();
    }

}

static void mcu_i2c2spi_func_read_status(void)
{
    if((g_mcu_i2c2spi_dev_adr !=  MCU_I2C_SLA_W_I2C2SPI) || (mcu_i2c2spi_recv_len != 1)) 
        return;
    if (mcu_i2c2spi_cmd_init) {
        g_i2c2spi_read_buf[0] = mcu_spi_flash_read_status1(I2C_OVER_SPI_CS_SEL);
        //mcu_i2c2spi_cmd_buf = &g_i2c2spi_read_buf[0];
        //mcu_i2c2spi_reply_data = g_i2c2spi_read_buf[0];
        //mcu_i2c2spi_cmd_buf_idx = 0;
        //mcu_i2c2spi_cmd_buf_len = 1;
    } else if (mcu_i2c2spi_cmd_doing) {
        //mcu_i2c2spi_reply_data = 0;
    }
}
#endif
static void mcu_i2c2spi_func_read_mcu_status(void)
{
    if((g_mcu_i2c2spi_dev_adr !=  MCU_I2C_SLA_W_I2C2SPI) || (mcu_i2c2spi_recv_len != 1))
        return;
    if (mcu_i2c2spi_cmd_init) {
        g_i2c2spi_read_buf[0] = g_i2c0_spi1_sel;
    } else if (mcu_i2c2spi_cmd_doing) {
        //mcu_i2c2spi_reply_data = 0;
    }
}
#ifdef FLASH_CODE
static void mcu_i2c2spi_func_flash_erase(void)
{
    if((g_mcu_i2c2spi_dev_adr !=  MCU_I2C_SLA_W_I2C2SPI) || (mcu_i2c2spi_recv_len != 4)) 
        return;
    if (mcu_i2c2spi_cmd_init) {
        mcu_spi_flash_erase(&mcu_i2c2spi_cmd_buf[0],I2C_OVER_SPI_CS_SEL);
    }
}


static void mcu_i2c2spi_func_chip_erase(void)
{
    if((g_mcu_i2c2spi_dev_adr !=  MCU_I2C_SLA_W_I2C2SPI) || (mcu_i2c2spi_recv_len != 1))
        return;
    if (mcu_i2c2spi_cmd_init) {
        mcu_spi_flash_chip_erase(I2C_OVER_SPI_CS_SEL);
    }
}
#endif
static void mcu_i2c2spi_func_read_tcon_id(void)
{
    if((g_mcu_i2c2spi_dev_adr !=  MCU_I2C_SLA_W_I2C2SPI) || (mcu_i2c2spi_recv_len != 1)) 
        return;
    if (mcu_i2c2spi_cmd_init) {

        //FITI_Mmecpy_fun(&g_i2c2spi_read_buf[0] ,&mcu_i2c2spi_tcon_id[0],sizeof(mcu_i2c2spi_tcon_id));
		memcpy(&g_i2c2spi_read_buf[0] ,&mcu_i2c2spi_tcon_id[0],sizeof(mcu_i2c2spi_tcon_id));	  

    //} else if (mcu_i2c2spi_cmd_doing) {
        //mcu_i2c2spi_func_prepare_reply_data();
    }
}

static void mcu_i2c2spi_func_sw_path(void)
{
    if((g_mcu_i2c2spi_dev_adr !=  MCU_I2C_SLA_W_I2C2SPI) || (mcu_i2c2spi_recv_len != 3)) 
        return;
    if (mcu_i2c2spi_cmd_init) 
    {
        g_spi_path_flg = mcu_i2c2spi_cmd_buf[1];
        g_flash_type = mcu_i2c2spi_cmd_buf[2];
    }
}
#ifdef FLASH_CODE
static void i2c_cmd_02_proc(void)
{
    if(g_i2crecv_csot_flg)
        CSOT_I2c_Slv_Spi_Mst_Write_PMICCodeToFlash();
    else
        mcu_i2c2spi_func_page_program();
}
static void i2c_cmd_05_proc(void)
{
    if(g_i2crecv_csot_flg)
        CSOT_I2c_Slv_Read_FW_Status();
    else
        mcu_i2c2spi_func_read_status();
}
#endif

static void i2c_cmd_fe_proc(void)
{
#ifdef OD_FUN
    if(mcu_i2c2spi_recv_len == 3)   // OD
        OD_Fun();
#endif
#ifdef DLG_FUN
    else if(mcu_i2c2spi_recv_len == 4) // DLG
        tcon_i2c_dlg_proc();
#endif
}
static void mcu_i2c2spat_reg(void)
{
    uint16_t ptr;
    uint8_t i;
    
    ptr = (uint16_t)&g_rw_spat_0000h_aging0;
    if (mcu_i2c2spi_cmd_init)
    {
        mcu_i2c_slv_cur_rw_ptr = ptr + (mcu_i2c2spi_cmd_buf[0] - 0x23);
        if(mcu_i2c2spi_recv_len > 1)
        {
            for(i = 0 ; i < mcu_i2c2spi_recv_len ; i++)
			{
                tcon_i2c_write_reg(mcu_i2c_slv_cur_rw_ptr, mcu_i2c2spi_cmd_buf[1+i]);
				mcu_i2c_slv_cur_rw_ptr++;
			}

        }
        else if(mcu_i2c2spi_recv_len == 1)
        {
            mcu_dma_set_ch0_adr(mcu_i2c_slv_cur_rw_ptr);
            mcu_i2c_slv_cur_rw_ptr++;
        }
    }
    else if(mcu_i2c2spi_cmd_doing)
    {
        if(mcu_i2c2spi_recv_len == 1)
        {
            mcu_i2c_slv_cur_rw_ptr ++;
        }
    }
}

void tcon_i2c_over_spi_start(uint8_t *cmd_buf, uint16_t buf_len)
{
    //uint8_t i;
    g_mcu_i2c2spi_dev_adr = cmd_buf[0];
    mcu_i2c2spi_cmd_idx = cmd_buf[1];
    mcu_i2c2spi_cmd_buf = &cmd_buf[1];
    mcu_i2c2spi_recv_len = buf_len;
    mcu_i2c2spi_cmd_init = TRUE;

}

void tcon_i2c_over_spi_done(void)
{
    tcon_i2c_proc_init();
}

void tcon_i2c_over_spi_proc(void)
{
    mcu_i2c_proc_slv_cmd();
    if (mcu_i2c2spi_cmd_init || mcu_i2c2spi_cmd_doing) {
        #ifdef FLASH_CODE
        mcu_spi_acc_bus(1);
        #endif
        switch (g_i2c_proc_dev_fun)
        {  
            case 0:     // CSOT CMD
            //#ifdef FLASH_CODE
                if(mcu_i2c_cmd_table[mcu_i2c2spi_cmd_idx].cmd_func != NULL)
                    (mcu_i2c_cmd_table[mcu_i2c2spi_cmd_idx].cmd_func)();
            //#endif
                break;
            case 1:     // spattern
                mcu_i2c2spat_reg();
                break;

            case 2:     // lut srw 
                tcon_i2c_over_lut_start(mcu_i2c2spi_cmd_buf, mcu_i2c2spi_recv_len);
                break;

            default:
                break;
        }
        #ifdef FLASH_CODE
        mcu_spi_acc_bus(0);
        #endif
        mcu_i2c2spi_cmd_init = FALSE;
        mcu_i2c2spi_cmd_doing = FALSE;

    }
}
#endif

void tcon_i2c_write_reg(uint16_t reg_ptr, uint8_t reg_val)
{
    *(volatile uint8_t xdata *)reg_ptr = reg_val;
}

uint8_t tcon_i2c_read_reg(uint16_t reg_ptr)
{
    return  *(volatile uint8_t xdata *)reg_ptr;
}

#ifdef DLG_FUN
void tcon_i2c_dlg_isr(void)
{
    g_vsync_irq_ex3_dlg_flg = 1;
}

void tcon_gpio_dlg_onoff_func(void)
{
    uint8_t i2c_dlg_cmd;
    bool dlg_en_flg;

    if((g_P0_0_dlg_cur && g_P0_1_dlg_cur) == 1) // P0_0 = 1 and P0_1 = 1
        return;
    else
		{
			i2c_dlg_cmd = (uint8_t)g_P0_1_dlg_cur;
			i2c_dlg_cmd = (uint8_t)((i2c_dlg_cmd << 1) | g_P0_1_dlg_cur);
		}

    if(i2c_dlg_cmd == 0) // i2c_dlg_cmd == 0
        dlg_en_flg = 0;
    else
        dlg_en_flg = 1;
    
    tcon_dlg_onoff_core_func(i2c_dlg_cmd, dlg_en_flg);
}
#endif
#ifdef VRR_FUN
void tcon_vrr_onoff_func(uint8_t i2c_vrr_cmd)
{
    if(i2c_vrr_cmd){
        // P0_6 = 1;
        if(g_rw_daf_0000h.bits.r_gam_en)
            g_rw_daf_0019h.bits.r_gam_vrr_lut_en = 0x01;

        //if(g_rw_daf_0003h.bits.r_dwo_gam_en)
        //    g_rw_dwo_001Ah.bits.r_gam_vrr_lut_en = 0x01;

        if(g_rw_daf_0001h.bits.r_lxl_od_en)
            g_rw_lxlod_004Fh.bits.r_lxl_gain2_en = 0x01;

        /*if(g_rw_daf_0002h.bits.r_os_en)
        {
            g_rw_os_00C8h.bits.r_vrr_chg_lut_en = 0x01;
            g_rw_os_00D4h.bits.r_temp_chg_lut_en = 0x01;
        }*/
        // P0_6 = 0;
    }else{
        // P0_7 = 1;
        //g_rw_dwo_0011h.bits.r_dwo_vrr_en = 0x00;
        g_rw_daf_0019h.bits.r_gam_vrr_lut_en = 0x00;
        g_rw_lxlod_004Fh.bits.r_lxl_gain2_en = 0x00;
        //g_rw_os_00C8h.bits.r_vrr_chg_lut_en = 0x00;
        //g_rw_os_00D4h.bits.r_temp_chg_lut_en = 0x00;
        // P0_7 = 0;
    }
}
#endif
#ifdef DLG_FUN
void tcon_i2c_set_dlg_val(bool val)
{
    if(g_rw_sys_087h.bits.r_dlg_sw_frun_en){

        g_in_proc_reg_003Ch.bits.r_mcu_de_stop_or = val;
    }else{

        g_rw_sys_0047h.bits.r_mcu_mask_en = val;
        g_rw_p2p_0000h.bits.r_unlock_cdr = val;
    }
}

void tcon_dlg_reg_preset(uint8_t i2c_dlg_cmd)
{
    // Register Set Part 1 (0x01 / 0x00 or 0x02)
    // r_sr_en = 1/0, r_hdisp_0 = 240/480, r_htotal_min_lvdk = 246/492, 
    // r_front_end_hact_min = 120/240, r_front_end_hact_max = 250/500,
    if(i2c_dlg_cmd == 0x01)
    {
        // r_sr_en = 1, r_hdisp_0 = 240, r_htotal_min_lvdk = 246, 
        g_rw_daf_0004h.bits.r_sr_en = 1;
        g_in_proc_reg_0000h.bits.r_hdisp_6_0 = 0x70;
        g_in_proc_reg_0001h.bits.r_hdisp_11_7 = 0x01;
        g_in_proc_reg_0001h.bits.r_htotal_min_lvclk_2_0 = 0x06;
        g_in_proc_reg_0002h.bits.r_htotal_min_lvclk_10_3 = 0x1E;
        g_in_proc_reg_0003h.bits.r_htotal_min_lvclk_12_11 = 0x00;

        // r_front_end_hact_min = 120, r_front_end_hact_max = 250,
        g_vx1_reg_0019h.bits.r_front_end_hact_min_7_0 = 0x78;
        g_vx1_reg_001Ah.bits.r_front_end_hact_min_13_8 = 0x00;
        g_vx1_reg_001Dh.bits.r_front_end_hact_max_5_0 = 0x3A;
        g_vx1_reg_001Eh.bits.r_front_end_hact_max_13_6 = 0x03;
    }else
    {
        // r_sr_en = 0, r_hdisp_0 = 480, r_htotal_min_lvdk = 492, 
        g_rw_daf_0004h.bits.r_sr_en = 0;
        g_in_proc_reg_0000h.bits.r_hdisp_6_0 = 0x60;
        g_in_proc_reg_0001h.bits.r_hdisp_11_7 = 0x03;
        g_in_proc_reg_0001h.bits.r_htotal_min_lvclk_2_0 = 0x04;
        g_in_proc_reg_0002h.bits.r_htotal_min_lvclk_10_3 = 0x3D;
        g_in_proc_reg_0003h.bits.r_htotal_min_lvclk_12_11 = 0x00;

        // r_front_end_hact_min = 240, r_front_end_hact_max = 500,
        g_vx1_reg_0019h.bits.r_front_end_hact_min_7_0 = 0xF0;
        g_vx1_reg_001Ah.bits.r_front_end_hact_min_13_8 = 0x00;    
        g_vx1_reg_001Dh.bits.r_front_end_hact_max_5_0 = 0x34;
        g_vx1_reg_001Eh.bits.r_front_end_hact_max_13_6 = 0x07;
    }

    // Register Set Part 2 (0x00 / 0x01 or 0x02)
    if(i2c_dlg_cmd == 0x00)
    {
        // r_front_end_vact_min = 1080, r_front_end_vact_max = 2180,
        g_vx1_reg_001Bh.bits.r_front_end_vact_min_7_0 = 0x38;
        g_vx1_reg_001Ch.bits.r_front_end_vact_min_13_8 = 0x04;
        g_vx1_reg_0038h.bits.r_front_end_vact_max_7_0 = 0x84;
        g_vx1_reg_0039h.bits.r_front_end_vact_max_13_8 = 0x08;

        // r_vdisp_0 = 2160, r_vdisp_pg_0 = 2160,
        g_in_proc_reg_0004h.bits.r_vdisp_0_0 = 0x00;
        g_in_proc_reg_0005h.bits.r_vdisp_0_8_1 = 0x38;
        g_in_proc_reg_0006h.bits.r_vdisp_0_12_9 = 0x04;
        g_in_proc_reg_0015h.bits.r_vdisp_pg_0_3_0 = 0x00;
        g_in_proc_reg_0016h.bits.r_vdisp_pg_0_11_4 = 0x87;

        // r_disp_vtot_0 = 2280, r_disp_vtot_1 = 2280,
        g_in_proc_reg_0018h.bits.r_disp_vtot_0_3_0 = 0x08;
        g_in_proc_reg_0019h.bits.r_disp_vtot_0_11_4 = 0x8E;
        g_in_proc_reg_001Ah.bits.r_disp_vtot_0_12 = 0x00;
        g_in_proc_reg_001Eh.bits.r_disp_vtot_1_0 = 0x00;
        g_in_proc_reg_001Fh.bits.r_disp_vtot_1_8_1 = 0x74;
        g_in_proc_reg_0020h.bits.r_disp_vtot_1_12_9 = 0x04;

        // r_disp_vtot_tp_0 = 2280, r_disp_vtot_bus_en_0 = 2280,
        g_in_proc_reg_001Ah.bits.r_disp_vtot_tp_0_6_0 = 0x68;
        g_in_proc_reg_001Bh.bits.r_disp_vtot_tp_0_12_7 = 0x11;
        g_in_proc_reg_001Bh.bits.r_disp_vtot_bus_en_1_0 = 0x00;
        g_in_proc_reg_001Ch.bits.r_disp_vtot_bus_en_9_2 = 0x3A;
        g_in_proc_reg_001Dh.bits.r_disp_vtot_bus_en_12_10 = 0x02;

        // r_vtotal_min_0 = 2166, r_vtotal_max_0 = 8192
        g_in_proc_reg_0006h.bits.r_vtotal_min_0_3_0 = 0x06;
        g_in_proc_reg_0007h.bits.r_vtotal_min_0_11_4 = 0x87;
        g_in_proc_reg_0008h.bits.r_vtotal_min_0_12 = 0x00;
        g_in_proc_reg_0008h.bits.r_vtotal_max_0_6_0 = 0x00;
        g_in_proc_reg_0009h.bits.r_vtotal_max_0_14_7 = 0x40;
        g_in_proc_reg_000Ah.bits.r_vtotal_max_0_15 = 0x00;

    }else
    {
        // r_front_end_vact_min = 540, r_front_end_vact_max = 1090,
        g_vx1_reg_001Bh.bits.r_front_end_vact_min_7_0 = 0x1C;
        g_vx1_reg_001Ch.bits.r_front_end_vact_min_13_8 = 0x02;
        g_vx1_reg_0038h.bits.r_front_end_vact_max_7_0 = 0x42;
        g_vx1_reg_0039h.bits.r_front_end_vact_max_13_8 = 0x04;

        // r_vdisp_0 = 1080, r_vdisp_pg_0 = 1080,
        g_in_proc_reg_0004h.bits.r_vdisp_0_0 = 0x00;
        g_in_proc_reg_0005h.bits.r_vdisp_0_8_1 = 0x1C;
        g_in_proc_reg_0006h.bits.r_vdisp_0_12_9 = 0x02;
        g_in_proc_reg_0015h.bits.r_vdisp_pg_0_3_0 = 0x08;
        g_in_proc_reg_0016h.bits.r_vdisp_pg_0_11_4 = 0x43;

        // r_disp_vtot_0 = 1140, r_disp_vtot_1 = 1140,
        g_in_proc_reg_0018h.bits.r_disp_vtot_0_3_0 = 0x04;
        g_in_proc_reg_0019h.bits.r_disp_vtot_0_11_4 = 0x47;
        g_in_proc_reg_001Ah.bits.r_disp_vtot_0_12 = 0x00;
        g_in_proc_reg_001Eh.bits.r_disp_vtot_1_0 = 0x00;
        g_in_proc_reg_001Fh.bits.r_disp_vtot_1_8_1 = 0x3A;
        g_in_proc_reg_0020h.bits.r_disp_vtot_1_12_9 = 0x02;

        // r_disp_vtot_tp_0 = 1140, r_disp_vtot_bus_en_0 = 1140,
        g_in_proc_reg_001Ah.bits.r_disp_vtot_tp_0_6_0 = 0x74;
        g_in_proc_reg_001Bh.bits.r_disp_vtot_tp_0_12_7 = 0x08;
        g_in_proc_reg_001Bh.bits.r_disp_vtot_bus_en_1_0 = 0x00;
        g_in_proc_reg_001Ch.bits.r_disp_vtot_bus_en_9_2 = 0x1D;
        g_in_proc_reg_001Dh.bits.r_disp_vtot_bus_en_12_10 = 0x01;

        // r_vtotal_min_0 = 1086, r_vtotal_max_0 = 4096
        g_in_proc_reg_0006h.bits.r_vtotal_min_0_3_0 = 0x0E;
        g_in_proc_reg_0007h.bits.r_vtotal_min_0_11_4 = 0x43;
        g_in_proc_reg_0008h.bits.r_vtotal_min_0_12 = 0x00;
        g_in_proc_reg_0008h.bits.r_vtotal_max_0_6_0 = 0x00;
        g_in_proc_reg_0009h.bits.r_vtotal_max_0_14_7 = 0x20;
        g_in_proc_reg_000Ah.bits.r_vtotal_max_0_15 = 0x00;
    }
}

void tcon_dlg_set_vact_num(bool dlg_en_flg)
{
    uint16_t l_vact_num;
    if(dlg_en_flg){
        if(g_data_mapping_reg_0000h.bits.r_hsd_en)
            l_vact_num = 2160;
        else
        {
            if(g_data_mapping_reg_0000h.bits.r_tri_gate_en)
                l_vact_num = 3240;
            else
                l_vact_num = 1080;
        }
    }else{
        if(g_data_mapping_reg_0000h.bits.r_hsd_en)
            l_vact_num = 4320;
        else
        {
            if(g_data_mapping_reg_0000h.bits.r_tri_gate_en)
                l_vact_num = 6480;
            else
                l_vact_num = 2160;
        }
    }

    g_rw_p2p_0002h.bits.r_vact_num_3_0   =  (uint8_t)(l_vact_num & 0x0F);
    g_rw_p2p_0003h.bits.r_vact_num_11_4  = (uint8_t)(l_vact_num >> 4);
    g_rw_p2p_0004h.bits.r_vact_num_13_12 = (uint8_t)(l_vact_num >> 12);
}


void tcon_dlg_send_i2c_cmd_sub(bool dlg_en_flg, uint8_t dev_addr, bool snd_data_flg)
{
    uint8_t l_mem_addr;
    uint8_t l_i2d_data[3];
   
    // Cmd: S dev_addr 00 5A AD P
    l_mem_addr = 0x00;
    l_i2d_data[0] = 0x5A;
    l_i2d_data[1] = 0xAD;
    mcu_i2c_mst_write(dev_addr, &l_mem_addr, 1, &l_i2d_data[0], 2, 0, g_i2c_wr_dly_time);

    // Cmd: S dev_addr 11 00 00 00 P / S dev_addr 11 01 (00 or 01) 02 P
    l_mem_addr = 0x13;
    if(dlg_en_flg){
        l_i2d_data[0] = 0x0A;

    }else{
        l_i2d_data[0] = 0x02;
 
    }
    mcu_i2c_mst_write(dev_addr, &l_mem_addr, 1, &l_i2d_data[0], 1, 0, g_i2c_wr_dly_time);
    // Cmd: S dev_addr 1A 80 P
    l_mem_addr = 0x1A;
    l_i2d_data[0] = 0x80;
    mcu_i2c_mst_write(dev_addr, &l_mem_addr, 1, &l_i2d_data[0], 1, 0, g_i2c_wr_dly_time);
}

void tcon_dlg_send_i2c_cmd(bool dlg_en_flg)
{
    uint8_t l_two_pin_sel;

    if(!g_rw_sys_0072h.bits.r_dlg_i2c_en)
        return;

    g_i2c_wr_dly_time = array_i2c_wr_dly_time[g_rw_mcu_top_0002h.bits.r_i2c_wr_dly_tm];

    l_two_pin_sel = g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl;
    mcu_i2c_set_two_pin_sel();
    tcon_dlg_send_i2c_cmd_sub(dlg_en_flg, 0x28, 0);
    tcon_dlg_send_i2c_cmd_sub(dlg_en_flg, 0x29, 1);
    g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = l_two_pin_sel;
}

void tcon_dlg_onoff_core_func(uint8_t i2c_dlg_cmd, bool dlg_en_flg)
{
    uint16_t vdisp_val;

    tcon_dlg_reg_preset(i2c_dlg_cmd);

    tcon_dlg_set_vact_num(dlg_en_flg);

    if(dlg_en_flg)
        vdisp_val = 1080;
    else
        vdisp_val = 2160;
        
    tcon_i2c_set_dlg_val(1);

    _nop_();
    g_in_proc_reg_0004h.bits.r_vdisp_0_0 = (vdisp_val & 0x01);
    g_in_proc_reg_0005h.bits.r_vdisp_0_8_1 = (vdisp_val >> 1);
    g_in_proc_reg_0006h.bits.r_vdisp_0_12_9 = (vdisp_val >> 9);
    // g_r_vdisp_msb = vdisp_val >> 8;
    // g_r_vdisp_lsb = vdisp_val & 0xFF;  

    _nop_();
    tcon_i2c_set_dlg_val(0);

    g_system_reg_087h.bits.r_dlg_en = dlg_en_flg;

    // set r_v_block_size & r_y_div
    tcon_dlg_set_vblock_y_div(dlg_en_flg);

    tcon_dlg_send_i2c_cmd(dlg_en_flg);   
    mcu_int_clear_iex4_isr_event();
}

void tcon_i2c_cmd_dlg_onoff_func(uint8_t i2c_dlg_cmd)
{
    bool dlg_en_flg;
   
    switch (i2c_dlg_cmd)
    {
        case 0x01:
            dlg_en_flg = 1;
            break;

        case 0x02:
            dlg_en_flg = 1;
            break;
        
        default: // case 0x00
            dlg_en_flg = 0;
            break;
    }

    tcon_dlg_onoff_core_func(i2c_dlg_cmd, dlg_en_flg);
}


void tcon_i2c_dlg_proc(void)
{   
    if(mcu_i2c2spi_cmd_buf[1] != 0xEA || mcu_i2c2spi_cmd_buf[2] != 0x0C)
        return;
    if (mcu_i2c2spi_cmd_init)    
    {
        // Check write data
        g_dlg_update_flg = 1;
        g_dlg_i2c_data_buf = mcu_i2c2spi_cmd_buf[3];
    }
}
#endif

void tcon_vsync_update_vrr_dlg_proc(void)
{
    if(g_vrr_update_flg)
    {
        #ifdef VRR_FUN
        tcon_vrr_onoff_func(g_vrr_i2c_data_buf);
        #endif
        g_vrr_update_flg = 0;
    }
#ifdef DLG_FUN
    if(g_dlg_update_flg)
    {
        tcon_i2c_cmd_dlg_onoff_func(g_dlg_i2c_data_buf);
        g_dlg_update_flg = 0;
    }

    if((g_P0_0_dlg_cur != g_P0_0_dlg_pre) || \ 
       (g_P0_1_dlg_cur != g_P0_1_dlg_pre))
    {
        g_P0_0_dlg_pre = g_P0_0_dlg_cur;
        g_P0_1_dlg_pre = g_P0_1_dlg_cur;
        tcon_gpio_dlg_onoff_func();
    }
#endif
}

#if 0
void tcon_clear_hw_od_en(void)
{
    if(g_clear_od_chg_lut_en)
    {
        g_clear_od_chg_lut_en = 0;
        g_rw_os_00D4h.bits.r_temp_chg_lut_en = 0;
    }
}
#endif