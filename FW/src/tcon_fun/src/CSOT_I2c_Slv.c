/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: CSOT
		Platform: 8051
		Author: Eric Lee, 2021/09/10
		Description: CSOT I2c Slave
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"

#ifdef CSOT_FUN
#include <string.h>
#include "mcu_global_vars.h"
#include "mcu_type.h"
#include "CSOT_I2C_SLV.h"
#include "mcu_i2c.h"
#include "CSOT_RTPM.h"
#include "mcu_spi.h"
//#include "mcu_reg.h"
#include "mcu_top.h"
#include "mcu_timer.h"
#include "reg_include.h"
#include "CSOT_Crc.h"
#include "tcon_i2c_proc.h"

// *******************************************************************
//------------------------------------------
// I2C to CSOT Variable Declaration
//------------------------------------------
//uint16_t xdata g_csot_read_reg_ptr;
//uint16_t xdata g_csot_reply_data_idx = 0x0000;
//uint32_t xdata g_Spi_PMIC_StartAddr = 0x00001000;
//uint8_t xdata g_csot_i2ccmd_num = 0;
uint16_t l_pmic_i2c_rw_dly_time = 0;
uint16_t xdata array_pmic_i2c_rw_dly_time[4] = {0, 2000, 4000, 6000};
uint8_t xdata g_csot_spirw_target = 0;
static uint8_t xdata g_current_vcm_i2c_en = 0;
#ifdef CSOT_SPI_CMD_SUP
volatile mcu_spi_rw_sta xdata g_spi_rw_sta;		//JC add 220315
volatile mcu_spi_rw_sta xdata g_csot_flash_rw_ptr[4];

static uint32_t xdata g_pmic_temp_addr[4] = {0,0,0,0} ;

#endif
// *******************************************************************
//------------------------------------------
// Function Declaration
//------------------------------------------
void CSOT_I2c_Slv_Read_FW_Status(void);                      // CMD : 0x05
void CSOT_I2c_Slv_Enter_Bypass_Mode(void);                   // CMD : 0xF0
void CSOT_I2c_Slv_Exit_Bypass_Mode(void);                    // CMD : 0x0F
void CSOT_I2c_Slv_I2c_Mst_Read_PMIC(void);                   // CMD : 0x67
void CSOT_I2c_Slv_I2c_Mst_Write_PMIC(void);                  // CMD : 0x98
void CSOT_I2c_Slv_Det_I2ctoSPI_Mode(void);                   // CMD : 0xF2
void CSOT_I2c_Slv_Read_Tcon_Vendor_Tcon_ID(void);            // CMD : 0x11
//void CSOT_Spi_Read_Flash_Jedec_ID(void);                     // CMD : 0x70 / 0x8F
//void CSOT_I2c_Slv_Spi_Mst_Read_PMICFromFlash(void);          // CMD : 0x0B / 0xF4
//void CSOT_I2c_Slv_Spi_Mst_Write_PMICCodeToFlash(void);       // CMD : 0x02 / 0xFD
//void CSOT_I2c_Slv_Spi_Mst_Read_PMICCodeFromFlash(void);      // CMD : 0x99
//void CSOT_I2c_Slv_I2c_Mst_Read_Spi_Write(void);              // CMD : 0x66
void CSOT_Write_GammaCodeToTconReg(void);		            // CMD : 0xDD
void CSOT_Read_GammaCodeToTconReg(void);		                // CMD : 0x22
void CSOT_DemuraOff_Cmd(void);				                // CMD : 0xE9
void CSOT_DemuraOn_Cmd(void);					            // CMD : 0xEA
void CSOT_Ctrl_TconReset_Cmd(void);			                // CMD : 0xEB
void CSOT_Read_TconResetStaFormTconReg_Cmd(void);			// CMD : 0xEC
void CSOT_Read_TconDemuraStaFormTconReg_Cmd(void);			// CMD : 0xED
void CSOT_Switch_Mode_Cmd(void);			                    // CMD : 0xCD
void CSOT_Switch_WhiteTrackingPattern_Cmd(void);			    // CMD : 0xE3
void CSOT_TurnOff_WhiteTracking_Cmd(void);				    // CMD : 0xE4
void CSOT_TurnOn_WhiteTracking_Cmd(void);					// CMD : 0xE5
void CSOT_RealTime_TuningWhiteTracking_Cmd(void);			// CMD : 0xE6
void CSOT_DGCOff_Cmd(void);									// CMD : 0xE7
void CSOT_DGCOn_Cmd(void);									// CMD : 0xE8



#if 0
mcu_i2c2csot_cmd_struct xdata mcu_i2c2csot_cmd_table[] = {
    {CSOT_CMD1_READ_MCU_FW_STATUS,        1,    CSOT_I2c_Slv_Read_FW_Status},
	{CSOT_CMD7_READ_PMIC_CODE,            3,    CSOT_I2c_Slv_I2c_Mst_Read_PMIC},
	{CSOT_CMD4_READ_TCONID,               1,    CSOT_I2c_Slv_Read_Tcon_Vendor_Tcon_ID},   
#ifdef CSOT_SPI_CMD_SUP
	{CSOT_CMD10_READ_CFLASH_JEDECID,      1,    CSOT_Spi_Read_Flash_Jedec_ID},
	{CSOT_CMD10_READ_XFLASH_JEDECID,      1,    CSOT_Spi_Read_Flash_Jedec_ID},
	{CSOT_CMD12_READ_FROM_CFLASH,         3,    CSOT_I2c_Slv_Spi_Mst_Read_PMICFromFlash},
	{CSOT_CMD12_READ_FROM_XFLASH,         3,    CSOT_I2c_Slv_Spi_Mst_Read_PMICFromFlash},
	{CSOT_CMD15_READ_FROM_FLASH,          3,    CSOT_I2c_Slv_Spi_Mst_Read_PMICCodeFromFlash},
	{CSOT_CMD16_WRITE_FROM_PMIC_TO_FLASH, 1,    CSOT_I2c_Slv_I2c_Mst_Read_Spi_Write},
#endif
#ifdef CSOT_BYPASS_MODE_SUP
    {CSOT_CMD5_ENTER_I2C_BYPASS_MODE,     1,    CSOT_I2c_Slv_Enter_Bypass_Mode},
	{CSOT_CMD6_EXIT_I2C_BYPASS_MODE,      1,    CSOT_I2c_Slv_Exit_Bypass_Mode},
#endif
    {CSOT_CMD8_WRITE_PMIC_CODE,          48,    CSOT_I2c_Slv_I2c_Mst_Write_PMIC},   // 48byte per bank
	{CSOT_CMD_Enter_EXIT_DBG_MODE,        6,    CSOT_I2c_Slv_Det_I2ctoSPI_Mode},
#ifdef CSOT_SPI_CMD_SUP
	{CSOT_CMD14_WRITE_TO_CFLASH,         48,    CSOT_I2c_Slv_Spi_Mst_Write_PMICCodeToFlash}
#endif
};
#endif
void CSOT_Decide_PMIC_I2C_Path(bool path)
{
	g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = path;
}

void CSOT_I2c_Slv_Read_FW_Status(void)		// CMD = 0x05
{
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x01)
		return;
		g_i2c2spi_read_buf[0] = g_rw_csot_fw_sta.byte;

	}
}
/*
static void CSOT_I2C_Write_PMIC_Dbg_Reg(bool dbg_en)
{
	uint8_t data_tmp = 0x00;

	//g_rtpm_i2c_write_buf[0] = g_mcu_reg_008h.bits.pmic_adr;
	g_rtpm_i2c_write_buf[0] = g_comm_hdr_buf.pmic_dev_addr;
	g_rtpm_i2c_write_buf[1] = 0x64;

	CSOT_Decide_PMIC_I2C_Path(g_rw_mcu_top_0027h.bits.r_pmic_i2c_path);   // change to master for pmic
    
	mcu_timer_wait_us(l_pmic_i2c_rw_dly_time);   // 2205025 add delay 1ms
	mcu_i2c_mst_read(&g_rtpm_i2c_write_buf[0], 1, &data_tmp, 1 , 0,0);
	if(dbg_en)
	    data_tmp |= 0x80;
	else
	    data_tmp &= 0x7F;

    mcu_timer_wait_us(l_pmic_i2c_rw_dly_time);   // 2205025 add delay 1ms
	mcu_i2c_mst_write(g_rtpm_i2c_write_buf[0] ,&g_rtpm_i2c_write_buf[1], 1, &data_tmp , 1, 0); 

	g_mcu_reg_004h.bits.mcu_i2c_ctrl = 0;   // back to slave

}*/

void CSOT_I2c_Slv_Det_I2ctoSPI_Mode(void)	// a.k.a Enter/Exit_Debug_Mode / CMD = 0xF2
{
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x06)
		return;
		if(mcu_i2c2spi_cmd_buf[1] == 0x70 && \
		mcu_i2c2spi_cmd_buf[2] == 0x55 && \
		mcu_i2c2spi_cmd_buf[3] == 0xAA && \
		mcu_i2c2spi_cmd_buf[4] == 0x5A && \
		mcu_i2c2spi_cmd_buf[5] == 0xA5)	{
		g_rw_csot_fw_sta.bits.i2c_to_spi_mode_valid = 1;
			//CSOT_I2C_Write_PMIC_Dbg_Reg(1);
	}
		else if(mcu_i2c2spi_cmd_buf[1] == 0x70 && \
		mcu_i2c2spi_cmd_buf[2] == 0x5A && \
		mcu_i2c2spi_cmd_buf[3] == 0xA5 && \
		mcu_i2c2spi_cmd_buf[4] == 0xAA && \
		mcu_i2c2spi_cmd_buf[5] == 0x55)	{
		g_rw_csot_fw_sta.bits.i2c_to_spi_mode_valid = 0;
			//CSOT_I2C_Write_PMIC_Dbg_Reg(0);
		}
	}
}

void CSOT_I2c_Slv_Read_Tcon_Vendor_Tcon_ID(void)		// CMD = 0x11  
{
	uint8_t *tconid_ptr = &g_rw_mcu_top_0038h_vendor_id;
	
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x01)
		return;
		g_i2c2spi_read_buf[0] = *tconid_ptr;
		g_i2c2spi_read_buf[1] = *(tconid_ptr + 1);
		g_i2c2spi_read_buf[2] = *(tconid_ptr + 2);

	}
	else if(mcu_i2c2spi_cmd_doing)
	{
		//csot_access_i2cslvbuf();
	}
}
#ifdef CSOT_BYPASS_MODE_SUP
void CSOT_I2c_Slv_Enter_Bypass_Mode(void)		// CMD = 0xF0
{
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x01)
		return;
	g_rw_csot_fw_sta.bits.i2c_bypass_mode_sta = 1;
	// hardware doesnt have through mode for EK50305
		g_current_vcm_i2c_en = g_rw_mcu_top_0003h.bits.vcm_i2cen;
		g_rw_mcu_top_0003h.bits.through_mode = 1;

		//if(g_rw_dbg_mux_0028h_mcu_top_dbgr_17 & 0x80)   // check i2c two pin sel
		if(g_rw_dbg_mux_003Bh_mcu_top_dbgr_18 & 0x80)   // check i2c two pin sel
		{
			g_rw_mcu_top_0003h.bits.vcm_i2cen = 1;
		}
	}
}

void CSOT_I2c_Slv_Exit_Bypass_Mode(void)		// CMD = 0x0F
{
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x01)
		return;
	g_rw_csot_fw_sta.bits.i2c_bypass_mode_sta = 0;
	// hardware doesnt have through mode for EK50305
		g_rw_mcu_top_0003h.bits.through_mode = 0;
		
		//if(g_rw_dbg_mux_0028h_mcu_top_dbgr_17 & 0x80)   // check i2c two pin sel
		if(g_rw_dbg_mux_003Bh_mcu_top_dbgr_18 & 0x80)   // check i2c two pin sel
			g_rw_mcu_top_0003h.bits.vcm_i2cen = g_current_vcm_i2c_en;
	}
}
#endif
void CSOT_I2c_Slv_I2c_Mst_Read_PMIC(void)	// CMD = 0x67   // in Patch RAM
{
	if(mcu_i2c2spi_cmd_init)
	{
	if(!g_rw_csot_fw_sta.bits.i2c_to_spi_mode_valid)
		return;
		
		if(mcu_i2c2spi_recv_len != 0x03)
		return;

		//g_rtpm_i2c_write_buf[0] = g_comm_hdr_buf.pmic_dev_addr;
		CSOT_ReadPMIC_Code_From_PMIC_Handler(mcu_i2c2spi_cmd_buf[1],mcu_i2c2spi_cmd_buf[2]);
	}
}

void CSOT_I2c_Slv_I2c_Mst_Write_PMIC(void)	// CMD = 0x98   // in Patch RAM
{
	if(!g_rw_csot_fw_sta.bits.i2c_to_spi_mode_valid)
		return;
	if(mcu_i2c2spi_recv_len < 0x03)
		return;
	if(mcu_i2c2spi_cmd_init)
	{
		g_rw_rtpm_i2c_write_len = (mcu_i2c2spi_recv_len - 1);
		//g_rtpm_i2c_write_buf[0] = g_comm_hdr_buf.pmic_dev_addr;
		CSOT_Decide_PMIC_I2C_Path(g_rw_mcu_top_0027h.bits.r_pmic_i2c_path);
		// mcu_timer_wait_us(l_pmic_i2c_rw_dly_time);   // 2205025 add delay 1ms
		mcu_i2c_mst_write(g_comm_hdr_buf.pmic_dev_addr ,&mcu_i2c2spi_cmd_buf[1], 1, &mcu_i2c2spi_cmd_buf[2] , g_rw_rtpm_i2c_write_len - 1, 0, l_pmic_i2c_rw_dly_time); 	
		
		g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;
	}
}

#ifdef CSOT_SPI_CMD_SUP
void CSOT_Spi_Read_Flash_Jedec_ID(void)	// CMD : 0x70 (X BOARD) / 0x8F (C BOARD)
{
	bool l_cs;

	if(mcu_i2c2spi_recv_len != 0x01)
		return;
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_cmd_buf[0] == 0x70)
		l_cs = FLASH_CS_XBOARD;	// x board
	else
			l_cs = FLASH_CS_CBOARD;	// c board

    if(mcu_spi_flash_read_id(SPI_FLASH_CMD_READ_ID_3BYTE,&g_spi_sta_buf[0], l_cs) == TRUE)	
    {	
			/*g_i2c2spi_read_buf[0] = mcu_global_buf[0];
			g_i2c2spi_read_buf[1] = mcu_global_buf[1];	
			g_i2c2spi_read_buf[2] = mcu_global_buf[2];	*/
			g_i2c2spi_read_buf[0] = g_spi_sta_buf[0];
			g_i2c2spi_read_buf[1] = g_spi_sta_buf[1];	
			g_i2c2spi_read_buf[2] = g_spi_sta_buf[2];
	}
	else	
	{			
			g_i2c2spi_read_buf[0] = 0x00;
			g_i2c2spi_read_buf[1] = 0x00;	
			g_i2c2spi_read_buf[2] = 0x00;	
	}
		//mcu_i2c2spi_cmd_buf = &g_i2c2spi_read_buf[0];
		//mcu_i2c2spi_cmd_buf_idx = 1;
        //mcu_i2c2spi_cmd_buf_len = 3;
		//mcu_i2c2spi_reply_data = mcu_i2c2spi_cmd_buf[0];
	}
	else if(mcu_i2c2spi_cmd_doing)
	{
		//csot_access_i2cslvbuf();
	}
}

void CSOT_I2c_Slv_Spi_Mst_Read_PMICFromFlash(void)		// CMD : 0x0B (XFLASH) / 0xF4 (CFLASH)   // in Patch RAM
{
	bool l_cs;
	uint16_t l_rlen;
	uint32_t l_pmic_addr;
	
	if(!g_rw_csot_fw_sta.bits.i2c_to_spi_mode_valid)
		return;
	//P0_6 = 1;
	if(mcu_i2c2spi_recv_len != 0x03)
		return;
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_cmd_buf[0] == CSOT_CMD12_READ_FROM_XFLASH)
		l_cs = FLASH_CS_XBOARD;	// x board
	else
			l_cs = FLASH_CS_CBOARD;	// c board
		
		//l_pmic_addr = CSOT_Get_Flash_Adr(g_rtpm_i2c_write_buf[1],l_cs);		// CSOT_Get_BANK
		l_pmic_addr = CSOT_Get_Flash_Adr(CSOT_Get_BANK(mcu_i2c2spi_cmd_buf[1]),l_cs);
		//l_pmic_addr = l_pmic_addr + CSOT_Get_Offset(mcu_i2c2spi_cmd_buf[1]);
		l_rlen = (uint16_t)(mcu_i2c2spi_cmd_buf[2] & 0x00FF);
		mcu_spi_flash_fast_read((uint8_t *)&l_pmic_addr + 1, &g_i2c2spi_read_buf[0] ,l_rlen ,l_cs);

	}
	else if(mcu_i2c2spi_cmd_doing)
	{
		//csot_access_i2cslvbuf();
	}
	// P0_6 = 0;
}

bool CSOT_Spi_Check_Flash_ID(uint8_t *rd_ptr)     //  in ROM
{
	//uint8_t *flashid_ptr = &g_flash_id;
    if((rd_ptr[0] == 0x00) && (rd_ptr[1] == 0x00) && (rd_ptr[2] == 0x00))
		return FALSE;
	else if((rd_ptr[0] == 0xFF) && (rd_ptr[1] == 0xFF) && (rd_ptr[2] == 0xFF))
		return FALSE;
	else
	    return TRUE;
}

static bool CSOT_Spi_Pmic_Read_And_Verify(uint8_t *sec_adr, uint8_t *ref_ptr, uint8_t len, bool cs) 
{
	uint8_t *rddata_ptr;
	//uint8_t i;
	//static uint8_t l_write_cnt = 0;
	
	rddata_ptr = &g_i2c2spi_read_buf;
	
	if(mcu_spi_flash_fast_read(sec_adr, rddata_ptr ,len ,cs) == FALSE)
	   return FALSE;

	if(memcmp(rddata_ptr,ref_ptr,len))
		return FALSE;

	/*for(i = 0; i < len; i++)
	{
		if(*(rddata_ptr + i) != *(ref_ptr + i))
		    return FALSE;
	}*/
	return TRUE;
}

static void CSOT_SetFlashPtr_Config(uint8_t wr_cnt,uint8_t *sec_ptr, uint8_t *wr_ptr, uint8_t w_len, bool cs)
{
	g_csot_flash_rw_ptr[wr_cnt].sec_ptr = sec_ptr; 
	g_csot_flash_rw_ptr[wr_cnt].wr_ptr = wr_ptr;
	g_csot_flash_rw_ptr[wr_cnt].w_len = w_len;
	g_csot_flash_rw_ptr[wr_cnt].cs = cs;
	g_csot_flash_rw_ptr[wr_cnt].g_mcu_spi_rw_en = 1;
	g_csot_flash_rw_ptr[wr_cnt].g_CSOT_spi_idx = 1;
	g_csot_spirw_target ++;
	if(g_csot_spirw_target > 3)
		g_csot_spirw_target = 0;
}

static void CSOT_GetFlashPtr_Config(uint8_t wr_cnt)
{
	g_spi_rw_sta.sec_ptr = g_csot_flash_rw_ptr[wr_cnt].sec_ptr;
	g_spi_rw_sta.wr_ptr = g_csot_flash_rw_ptr[wr_cnt].wr_ptr;
	g_spi_rw_sta.w_len = g_csot_flash_rw_ptr[wr_cnt].w_len;
	g_spi_rw_sta.cs = g_csot_flash_rw_ptr[wr_cnt].cs;
	g_spi_rw_sta.g_mcu_spi_rw_en = 1;
	g_spi_rw_sta.g_CSOT_spi_idx = 1;
}

static bool CSOT_Write_PMIC_Code_To_Flash(bool cs)     // in Patch RAM
{
	// reset status and set busy bit
	g_rw_csot_fw_sta.byte = 0x21;   //0x01;

	// check flash ID
	if(mcu_spi_flash_read_id(SPI_FLASH_CMD_READ_ID_3BYTE,&g_spi_sta_buf[0], cs) == FALSE) 	
	{
	    // set flash connection status bit, and clear busy bit
		g_rw_csot_fw_sta.byte = 0x24;   //0x04;
		return FALSE;
	}
	if(CSOT_Spi_Check_Flash_ID(&g_spi_sta_buf) == FALSE)
	{	
		g_rw_csot_fw_sta.byte = 0x24;   //0x04;
		return FALSE;
	}
	return TRUE;
}

void CSOT_I2c_Slv_Spi_Mst_Write_PMICCodeToFlash(void)		// CMD : 0x02 / 0xFD  // in Patch RAM
{
	bool l_cs;
	uint8_t l_bank;
	uint8_t l_wlen;
	//uint32_t l_pmic_addr;

	if(!g_rw_csot_fw_sta.bits.i2c_to_spi_mode_valid)
		return;
	if(mcu_i2c2spi_recv_len < 0x03)
		return;
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_cmd_buf[0] == CSOT_CMD13_WRITE_FROM_XFLASH)
			l_cs = FLASH_CS_XBOARD;	// x board
		else
			l_cs = FLASH_CS_CBOARD;	// c board

		l_bank = CSOT_Get_BANK(mcu_i2c2spi_cmd_buf[1]);

		l_wlen = CSOT_Get_Len(l_bank);

		if(CSOT_Crc_FW_Cal(&mcu_i2c2spi_cmd_buf[2],l_wlen) == 0x00)
		{
			if(CSOT_Write_PMIC_Code_To_Flash(l_cs) == TRUE)
			{
				//g_pmic_addr = CSOT_Get_Flash_Adr(l_bank,l_cs);
				g_pmic_temp_addr[0] = CSOT_Get_Flash_Adr(l_bank,l_cs);
				g_pmic_rw_ptr = CSOT_Get_PMIC_DataBuf(l_bank);
				memcpy(g_pmic_rw_ptr,&mcu_i2c2spi_cmd_buf[2],l_wlen);
				if(l_cs == FLASH_CS_CBOARD)
				{
					if(l_bank == GMA || l_bank == PMIC_HDR)
					{
						g_pmic_temp_addr[0] = CSOT_Get_Flash_Adr(PMIC_HDR,FLASH_CS_CBOARD);
						g_pmic_rw_ptr = CSOT_Get_PMIC_DataBuf(PMIC_HDR);
						if(g_rw_mcu_top_0011h.bits.r_pmic_gma_en)
						l_wlen = 0x40;		// Hdr + GMA
						else
							l_wlen = CSOT_Get_Len(PMIC_HDR);
						//l_wlen = 0x40;		// Hdr + GMA
					}
				}
				CSOT_SetFlashPtr_Config(0,(uint8_t *)&g_pmic_temp_addr[0]+1, g_pmic_rw_ptr, l_wlen, l_cs);
				CSOT_GetFlashPtr_Config(0);
			}
		}
		else
		{
			CSOT_RTPM_Set_Fw_Sta_Busy(0);
			CSOT_RTPM_Set_Fw_Sta_Crc_Fail_Tcon_Pmic_Flash(1);
		}
	}
}

void CSOT_I2c_Slv_Spi_Mst_Read_PMICCodeFromFlash(void)	// CMD : 0x99   // in Patch RAM
{
	bool l_cs;
	uint8_t l_bank;
	uint16_t l_rlen;
	uint32_t l_pmic_addr;

	if(!g_rw_csot_fw_sta.bits.i2c_to_spi_mode_valid)
		return;
	if(mcu_i2c2spi_recv_len != 0x03)
		return;
	if(mcu_i2c2spi_cmd_init)
	{
		l_cs = CSOT_Decide_Flash_Path();
		l_bank = CSOT_Get_BANK(mcu_i2c2spi_cmd_buf[1]);
		l_pmic_addr = CSOT_Get_Flash_Adr(l_bank,l_cs);
		//l_pmic_addr = l_pmic_addr + CSOT_Get_Offset(mcu_i2c2spi_cmd_buf[1]);
		l_rlen = (uint16_t)(mcu_i2c2spi_cmd_buf[2] & 0x00FF);
		mcu_spi_flash_fast_read((uint8_t *)&l_pmic_addr + 1, &g_i2c2spi_read_buf[0], l_rlen, l_cs);
	}
}

void CSOT_I2c_Slv_I2c_Mst_Read_Spi_Write(void)		// CMD : 0x66   // in Patch RAM
{
	bool l_cs;
	uint8_t l_wlen;
	uint8_t l_rw_bank;
	
	if(!g_rw_csot_fw_sta.bits.i2c_to_spi_mode_valid)
		return;
	if(mcu_i2c2spi_recv_len != 0x01)
		return;
    if(mcu_i2c2spi_cmd_init)
	{
    // decide i2c pmic dev adr & bank strt addr
		//g_rtpm_i2c_write_buf[0] = g_comm_hdr_buf.pmic_dev_addr;
    // i2c read PMIC code from PMIC
		if(CSOT_ReadPMIC_Code_From_PMIC_Handler(PMIC_READ_ALL_BANK,PMIC_READ_ALL_LEN) == FALSE)
		{
			CSOT_RTPM_Set_Fw_Sta_Crc_Fail_Tcon_Pmic_Flash(1);
			CSOT_RTPM_Set_Fw_Sta_Busy(0);
			return;
		}
	
		l_cs = CSOT_Decide_Flash_Path();
		if(CSOT_Write_PMIC_Code_To_Flash(l_cs) == TRUE)
		{
			for(l_rw_bank = 0 ; l_rw_bank < 2 ; l_rw_bank ++)		// write bankA and B
			{
				//g_pmic_addr = CSOT_Get_Flash_Adr(l_rw_bank,l_cs);		// First DL BankB  -> A 
				g_pmic_temp_addr[l_rw_bank] = CSOT_Get_Flash_Adr(l_rw_bank,l_cs);		// First DL BankB  -> A
				//g_pmic_flash_adr = CSOT_Get_PMIC_Adr(l_rw_bank,l_cs);
				g_pmic_rw_ptr = CSOT_Get_PMIC_DataBuf(l_rw_bank);		
				l_wlen = CSOT_Get_Len(l_rw_bank);
				//CSOT_SetFlashPtr_Config(l_rw_bank,(uint8_t*)&g_pmic_addr+1, g_pmic_rw_ptr, l_wlen, l_cs);
				CSOT_SetFlashPtr_Config(l_rw_bank,(uint8_t*)&g_pmic_temp_addr[l_rw_bank]+1, g_pmic_rw_ptr, l_wlen, l_cs);
			}
			// Write C Board Header
			//g_pmic_addr = CSOT_Get_Flash_Adr(PMIC_HDR,FLASH_CS_CBOARD);
			g_pmic_temp_addr[l_rw_bank] = CSOT_Get_Flash_Adr(PMIC_HDR,FLASH_CS_CBOARD);
			//g_pmic_flash_adr = CSOT_Get_PMIC_Adr(PMIC_HDR,FLASH_CS_CBOARD);
			g_pmic_rw_ptr = CSOT_Get_PMIC_DataBuf(PMIC_HDR);
			if(g_rw_mcu_top_0011h.bits.r_pmic_gma_en)
			l_wlen = 0x40;		// Hdr + GMA
			else
				l_wlen = CSOT_Get_Len(PMIC_HDR);
			//CSOT_SetFlashPtr_Config(l_rw_bank,(uint8_t*)&g_pmic_addr+1, g_pmic_rw_ptr, l_wlen, FLASH_CS_CBOARD);
			CSOT_SetFlashPtr_Config(l_rw_bank,(uint8_t*)&g_pmic_temp_addr[l_rw_bank]+1, g_pmic_rw_ptr, l_wlen, FLASH_CS_CBOARD);
			CSOT_GetFlashPtr_Config(0);
		}
	}
}
#endif

// -------------------------------------------------------------------------------------------------------------
void CSOT_update_GammaCodeToTconReg(uint8_t wlen)
{
	uint8_t *gama_xmem_ptr;
	uint8_t i;

	gama_xmem_ptr = &(g_rw_p2p_0011h_gam1);
	
	for(i = 0; i < wlen; i++)
	{	
		*(gama_xmem_ptr + i) = g_csot_gma_buf[i];
	}
#ifdef SUP_USI_T_GAM
	// ISP_Gamma & CSPI_Gamma
	if(g_rw_p2p_0007h.bits.r_isp_en){
		
		for(i = 0; i < wlen; i++)
		{	
			*(gama_xmem_ptr + i) = g_csot_gma_buf[i];
		}

	// USI-T_Gamma
	}else if(g_rw_p2p_0008h.bits.r_usit_en){
		
		for(i = 0; i < wlen; i++)
		{		
			csot_usit_gamma_hw_1to3_csot = g_csot_gma_buf[i];

			csot_usit_gamma_hw_cal_en |= 0x01;		// Enable 1to3 hw calcation
			
			// Wait HW calculation finish
			while(1)
			{
				if((csot_usit_gamma_hw_cal_en & 0x01) == 0x00)
					break;
				if(mcu_timer_dl_timeout())
					break;
			}

			*(gama_xmem_ptr    ) = csot_usit_gamma_hw_1to3_fiti.r_gam_dt_7_0;
			*(gama_xmem_ptr + 1) = csot_usit_gamma_hw_1to3_fiti.r_gam_dt_15_8;
			*(gama_xmem_ptr + 2) = csot_usit_gamma_hw_1to3_fiti.r_gam_dt_23_16;
			gama_xmem_ptr += 3;

		}
	}
	#endif

}

void CSOT_Write_GammaCodeToTconReg(void)		// CMD : 0xDD
{
	uint8_t l_wlen;
	uint16_t l_gamma_crc; //, l_gamma_crc_ans;
	
	if(mcu_i2c2spi_cmd_init)
	{
		// CMD + Offset + 16 Data = 18 bytes
		if(mcu_i2c2spi_recv_len < 0x04)
			return;
		
		// Check I2C offset cmd is 0xFE 
		if(mcu_i2c2spi_cmd_buf[1] != 0xFE)
			return;

		//P1_1 = 1;
		// Write Tcon Reg 
		memcpy(&g_csot_gma_buf[0], &mcu_i2c2spi_cmd_buf[2], mcu_i2c2spi_recv_len - 2); // 14

		// Check Gamma CRC
		//P1_2 = 1;
		// l_gamma_crc_ans = ((g_csot_gma_buf[mcu_i2c2spi_recv_len - 4] << 8) | (g_csot_gma_buf[mcu_i2c2spi_recv_len - 3]));
		l_gamma_crc = CSOT_Crc_FW_Cal(&g_csot_gma_buf[0], mcu_i2c2spi_recv_len - 4);
		
		if(((uint8_t)(l_gamma_crc >> 8) != g_csot_gma_buf[mcu_i2c2spi_recv_len - 4]) ||
		    ((uint8_t)(l_gamma_crc & 0xFF) != g_csot_gma_buf[mcu_i2c2spi_recv_len - 3]))
		{
			//P1_2 = 0;
			return;
		}	

		l_wlen = mcu_i2c2spi_recv_len - 4;
		CSOT_update_GammaCodeToTconReg(l_wlen);

		//P1_2 = 0;
		//P1_1 = 0;

	}
}

void CSOT_update_GammaCodeFromTconReg(void)
{
	uint8_t *gama_xmem_ptr;
	uint8_t i, l_rlen;
	uint16_t l_gamma_crc;

	// For Test
	//P1_1 = 1;
	l_gamma_crc = 0x00;

	l_rlen = 0x10;
	gama_xmem_ptr = &(g_rw_p2p_0011h_gam1);
	
	for(i = 0; i < l_rlen; i++)
		{	
			g_csot_gma_buf[i] = *(gama_xmem_ptr + i);
		}
#ifdef SUP_USI_T_GAM	
	// ISP_Gamma & CSPI_Gamma
	if(g_rw_p2p_0007h.bits.r_isp_en){
		
		for(i = 0; i < l_rlen; i++)
		{	
			g_csot_gma_buf[i] = *(gama_xmem_ptr + i);
		}

	// USI-T_Gamma
	}else if(g_rw_p2p_0008h.bits.r_usit_en){

		for(i = 0; i < (l_rlen-2); i++)
		{

			csot_usit_gamma_hw_3to1_fiti.r_gam_dt_7_0   = *(gama_xmem_ptr    );
			csot_usit_gamma_hw_3to1_fiti.r_gam_dt_15_8  = *(gama_xmem_ptr + 1);
			csot_usit_gamma_hw_3to1_fiti.r_gam_dt_23_16 = *(gama_xmem_ptr + 2);
			gama_xmem_ptr += 3;

			csot_usit_gamma_hw_cal_en |= 0x02;		// Enable 3to1 hw calcation
			
			// Wait HW calculation finish
			while(1)
			{
				if((csot_usit_gamma_hw_cal_en & 0x02) == 0x00)
					break;
				if(mcu_timer_dl_timeout())
					break;
			}

			g_csot_gma_buf[i] = csot_usit_gamma_hw_3to1_csot;

		}
	}else{
		
		for(i = 0; i < l_rlen; i++)
		{
			g_csot_gma_buf[i] = 0x00;
		}
		P1_1 = 0;
		return;
	}		
#endif
	// Calculate Gamma CRC
	l_gamma_crc = CSOT_Crc_FW_Cal(&g_csot_gma_buf[0], l_rlen-2);
	g_csot_gma_buf[14] = (uint8_t)(l_gamma_crc >> 8);
	g_csot_gma_buf[15] = (uint8_t)(l_gamma_crc & 0xFF);
	
	//P1_1 = 0;
}


void CSOT_Read_GammaCodeToTconReg(void)		// CMD : 0x22
{
	uint8_t l_rlen;

	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x02)
			return;
		
		//P1_1 = 1;
		l_rlen = 0x10;
		memcpy(&g_i2c2spi_read_buf[0],&g_csot_gma_buf[0], l_rlen);
		//mcu_i2c2spi_cmd_buf = &g_i2c2spi_read_buf[0];
		//mcu_i2c2spi_cmd_buf_idx = 1;
        //mcu_i2c2spi_cmd_buf_len = l_rlen;
		//mcu_i2c2spi_reply_data = mcu_i2c2spi_cmd_buf[0];
		//P1_1 = 0;
	}
	else if(mcu_i2c2spi_cmd_doing)
	{
		//csot_access_i2cslvbuf();
	}
}

void CSOT_DemuraOff_Cmd(void)				// CMD : 0xE9
{
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x01)
			return;
		// set demura off
		//g_rw_daf_0003h.bits.r_dmr_en = 0;
	}
}

void CSOT_DemuraOn_Cmd(void)					// CMD : 0xEA
{
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x01)
			return;
		// set demura on
		//g_rw_daf_0003h.bits.r_dmr_en = 1;
	}
}

void CSOT_Ctrl_TconReset_Cmd(void)			// CMD : 0xEB
{
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x01)
			return;
	    // set tcon reset
		//tcon_r_sw_rstn = 0xAA;
		g_rw_dbg_mux_0000h_sw_rst = 0xAA;
	}
}

void CSOT_Read_TconResetStaFormTconReg_Cmd(void)			// CMD : 0xEC
{
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x02)
			return;
	    // read tcon status
		//memcpy(&g_i2c2spi_read_buf[0],&tcon_inpron_dbgr[0],0x10);
		//memcpy(&g_i2c2spi_read_buf[0],&g_rw_dbg_mux_0100h_indbgr_0,0x10);
		memcpy(&g_i2c2spi_read_buf[0],&g_rw_dbg_mux_0000h_sw_rst,0x8);
		
		//mcu_i2c2spi_cmd_buf = &g_i2c2spi_read_buf[0];
		//mcu_i2c2spi_cmd_buf_idx = 1;
        //mcu_i2c2spi_cmd_buf_len = 0x2;
		//mcu_i2c2spi_reply_data = mcu_i2c2spi_cmd_buf[0];
	}
	else if(mcu_i2c2spi_cmd_doing)
	{
		//csot_access_i2cslvbuf();
	}
}

void CSOT_Read_TconDemuraStaFormTconReg_Cmd(void)			// CMD : 0xED
{
#ifdef DEMURA_TRANS_FUN
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x02)
			return;

		// read demura status
		g_i2c2spi_read_buf[0] = g_demura_load_reg_0050h.byte;
		g_i2c2spi_read_buf[1] = g_demura_load_reg_0052h.byte;
	}
	else if(mcu_i2c2spi_cmd_doing)
	{
		//csot_access_i2cslvbuf();
	}
#endif
}

void CSOT_Switch_Mode_Cmd(void)			// CMD : 0xCD
{
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x07)
			return;

		switch(mcu_i2c2spi_cmd_buf[1])
		{	
			case 0x02: // DLG CMD /* EK50215 Not Support*/
			#ifdef DLG_FUN
				g_dlg_update_flg = 1;
				g_vrr_update_flg = 1;
				if(mcu_i2c2spi_cmd_buf[2] == 0x01){
					// Normal mode -> DLG off, VRR off
        			g_dlg_i2c_data_buf = 0x00;
					g_vrr_i2c_data_buf = 0x00;
				}else{
					// DLG mode -> DLG on
        			g_dlg_i2c_data_buf = 0x02;
					// VRR mode -> VRR on/off check
					g_vrr_i2c_data_buf = mcu_i2c2spi_cmd_buf[3];
				}
			#endif
			break;
			case 0x03: // MLED CMD	
				// EK50606 / EK50215 Not Support
			break;
			case 0x04: // 120 hz ~ 50 / 60hz
				// EK50606 / EK50215 Not Support
			break;
			case 0xFF:	// VRR cmd 
			#ifdef VRR_FUN
				g_vrr_update_flg = 1;
				g_vrr_i2c_data_buf = mcu_i2c2spi_cmd_buf[2];
			#endif
				// tcon_vrr_onoff_func(mcu_i2c2spi_cmd_buf[2]);
			break;
		}
	}
}

//--------------------------------------DGC---------------------------------------------------------------------
void CSOT_DGCOff_Cmd(void)								// CMD : 0xE7
{
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x01)
			return;
	}
}

void CSOT_DGCOn_Cmd(void)								// CMD : 0xE8
{
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x01)
			return;
	}
}
#if 0
// ------------------------------------Auto White---------------------------------------------------------------
void CSOT_Switch_WhiteTrackingPattern_Cmd(void)			// CMD : 0xE3
{
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x04)
			return;
	// RGB
	}
}

void CSOT_TurnOff_WhiteTracking_Cmd(void)				// CMD : 0xE4
{
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x01)
			return;
		// Turn off white tracking
	}
}

void CSOT_TurnOn_WhiteTracking_Cmd(void)					// CMD : 0xE5
{
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x01)
			return;
	// Turn on white tracking
	}
}

void CSOT_RealTime_TuningWhiteTracking_Cmd(void)			// CMD : 0xE6
{
	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_recv_len != 0x09)
			return;
		// Real Time white tracking
	}
}

// --------------------------------- t10 Tcon VCOM -------------------------------------------------------
void CSOT_I2C_Slv_Read_t10VCOM_from_Flash(void)				// CMD : 0x71 / 0x73
{

}

void CSOT_I2C_Slv_Write_t10VCOM_to_Flash(void)				// CMD : 0x72 / 0x74
{
	bool l_cs;
	uint8_t offset, l_wlen;

	if(mcu_i2c2spi_recv_len < 0x03)
		return;
	if(r_comm_hdr_buf.vcm_gma_sel != 0x01)
		return;

	if(mcu_i2c2spi_cmd_init)
	{
		if(mcu_i2c2spi_cmd_buf[0] == CSOT_CMD18_WRITE_VCOM_FROM_XFLASH)
			l_cs = FLASH_CS_XBOARD;	// x board
		else
			l_cs = FLASH_CS_CBOARD;	// c board

		offset = mcu_i2c2spi_cmd_buf[1];
		l_wlen = mcu_i2c2spi_recv_len - 2;

		if(CSOT_Write_PMIC_Code_To_Flash(l_cs) == TRUE)
		{
			// N4 = CSOT_Get_Flash_Adr(l_bank,l_cs);
			// g_pmic_rw_ptr = CSOT_Get_PMIC_DataBuf(l_bank);
			// memcpy(g_pmic_rw_ptr,&mcu_i2c2spi_cmd_buf[2],l_wlen);

			// if(l_cs == FLASH_CS_CBOARD)
			// {
			// 	if(l_bank == GMA || l_bank == PMIC_HDR)
			// 	{
			// 		g_pmic_addr = CSOT_Get_Flash_Adr(PMIC_HDR,FLASH_CS_CBOARD);
			// 		g_pmic_rw_ptr = CSOT_Get_PMIC_DataBuf(PMIC_HDR);
			// 		l_wlen = 0x40;		// Hdr + GMA
			// 	}
			// }
			CSOT_SetFlashPtr_Config(0,(uint8_t *)&g_pmic_addr+1, g_pmic_rw_ptr, l_wlen, l_cs);
			CSOT_GetFlashPtr_Config(0);
		}

	}
}
#endif

// -------------------------------------------------------------------------------------------------------------
/*void CSOT_I2c_Slv_Init(void)
{
	//g_csot_reply_data_idx = 0;
	//mcu_i2c2spi_recv_len = 0;
}*/

void tcon_CSOT_I2c_Slv_Init(void)   // called by main() for initial
{
	//g_csot_reply_data_idx = 0;
    //g_csot_read_reg_ptr = 0;
	//g_csot_i2ccmd_num = (sizeof(mcu_i2c2csot_cmd_table) / sizeof(mcu_i2c2csot_cmd_struct));
	l_pmic_i2c_rw_dly_time = array_pmic_i2c_rw_dly_time[g_rw_mcu_top_0013h.bits.r_pmic_i2c_dly_rim];

#ifdef CSOT_SPI_CMD_SUP
	g_spi_rw_sta.w_len = 0x00;
	g_spi_rw_sta.cs = 0x00;
	g_spi_rw_sta.g_mcu_spi_rw_en = 0x00;
	g_spi_rw_sta.g_CSOT_spi_idx = 0x00;
#endif
}

/*void CSOT_I2c_Slv_Set_I2c_Slv_Adr(void)
{
     mcu_i2c_sec_addr_2 = (MCU_I2C_SLV_ADDR_CSOT << 1);
} */

/*void CSOT_I2c_Slv_Rx_Fill_RTPM_Buf(void)
{
    if (mcu_i2c2spi_recv_len < MCU_GLOBAL_BUF_SZ) 
    {
		//g_rtpm_i2c_write_buf[mcu_i2c2spi_recv_len++] = I2C2DAT;
		g_rtpm_i2c_write_buf[mcu_i2c2spi_recv_len++] = g_i2cslvdat;
    }
}*/

/*uint8_t CSOT_I2c_Slv_Read_Data(void)
{
	return (g_i2c2spi_read_buf[g_csot_reply_data_idx++]);
}*/
#ifdef CSOT_SPI_CMD_SUP
void CSOT_Write_PMIC_Code(void)
{
	uint8_t data_tmp, spi_sta;
	static uint8_t l_rw_cnt = 0;

	if(l_rw_cnt == g_csot_spirw_target)	return;
	//CSOT_GetFlashPtr_Config(l_rw_cnt);
	if(g_spi_rw_sta.g_mcu_spi_rw_en ==  0) return;
	g_mcu_int_flg = 1;
	// check flash busy
	spi_sta = mcu_spi_flash_read_status1(g_spi_rw_sta.cs) & 0x01;
	if(spi_sta != 0x00) return;

	switch (g_spi_rw_sta.g_CSOT_spi_idx) 
	{
		case 1: // read flash status and clear block protection bit
			data_tmp = (mcu_spi_flash_read_status1(g_spi_rw_sta.cs) & 0xE3);
			mcu_spi_flash_write_status(data_tmp, g_spi_rw_sta.cs);
			g_spi_rw_sta.g_CSOT_spi_idx = 2;
			break;
		
		case 2: // send flash write enable and erase 4k /
			//mcu_spi_flash_sector_erase(g_spi_rw_sta.sec_ptr[0], g_spi_rw_sta.sec_ptr[1], g_spi_rw_sta.sec_ptr[2], g_spi_rw_sta.cs);
			mcu_spi_flash_merge_erase(SPI_FLASH_CMD_SECTOR_ERASE,&g_spi_rw_sta.sec_ptr[0], g_spi_rw_sta.cs);
			g_spi_rw_sta.g_CSOT_spi_idx = 3;
			break;
		
		case 3: // send flash write enable and
			mcu_spi_flash_page_program(g_spi_rw_sta.sec_ptr, g_spi_rw_sta.wr_ptr, 0x00, g_spi_rw_sta.w_len, g_spi_rw_sta.cs);
			g_spi_rw_sta.g_CSOT_spi_idx = 4;
			break;
   
		case 4: // read back PMIC Code and Verification
			if(CSOT_Spi_Pmic_Read_And_Verify(g_spi_rw_sta.sec_ptr, g_spi_rw_sta.wr_ptr, g_spi_rw_sta.w_len, g_spi_rw_sta.cs) == FALSE)
			{
				g_rw_csot_fw_sta.byte = 0x22;   //0x02;							// set crc_fail_tcon_pmic_flash and clear busy bit / debug mode bit still set
				g_spi_rw_sta.g_mcu_spi_rw_en = 0;						// finish spi rw process
				g_spi_rw_sta.g_CSOT_spi_idx = 0;
				g_csot_spirw_target = 0;
				l_rw_cnt = 0;
			}
			else{
				l_rw_cnt ++;
				if(l_rw_cnt == g_csot_spirw_target)
				{
					g_csot_spirw_target = 0;
					l_rw_cnt = 0;
					data_tmp = (mcu_spi_flash_read_status1(g_spi_rw_sta.cs) | 0x1C);
					mcu_spi_flash_write_status(data_tmp, g_spi_rw_sta.cs);
					g_rw_csot_fw_sta.byte &= 0xFE;									// clear busy bit
					g_spi_rw_sta.g_mcu_spi_rw_en = 0;							// finish spi rw process
					g_spi_rw_sta.g_CSOT_spi_idx = 0;
				}
				else
					CSOT_GetFlashPtr_Config(l_rw_cnt);
			}
			break;

		default:
			break;
    }
}
#endif

#endif