/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: EK50508
		Platform: 8051
		Author: Johnny Lee, 2021/09/07
		Description: CSOT RTPM
*/
// *******************************************************************
// *******************************************************************
#include "mcu_fun_def.h"

#ifdef CSOT_FUN
// Include Path
#include "mcu_reg51.h"
#include "mcu_global_vars.h"
#include "mcu_type.h"
#include "mcu_const.h"
#include "mcu_i2c.h"
#include "csot_global_include.h"
//#include "mcu_reg.h"
#include "mcu_top.h"
#include "mcu_spi.h"
#include <string.h>
//#include "CSOT_RTPM.h"
//#include "CSOT_Crc.h"
//#include "mcu_entry_dl.h"

// *******************************************************************
// *******************************************************************
// Define
//#define RETRY_TIMES				3
/*#define BANK_A				0x00
#define BANK_B					0x01
#define GMA						0x02
#define PMIC_HDR				0x03*/
//#define SPI_C_FLASH_PATH    	0
//#define SPI_X_FLASH_PATH    	1

#define RTPM_DL_FAIL			0
#define RTPM_DL_PASS			1
// *******************************************************************
// *******************************************************************
static uint8_t xdata g_rtpm_pmic_status_reg[4];
static uint8_t xdata g_rtpm_pmic_status_adr[4] = {0,0,0,0};
static uint32_t xdata g_rtpm_common_hdr = 0xFF;
//static uint8_t g_rtpm_debug_flag = 0;
uint8_t xdata g_rtpm_monitor_status = 0;
static bool g_csot_pmic_pass_cfg_flg = 0;
static bool g_csot_rtpm_proc_flg = 0;
static bool g_csot_hdr_crc_pass_flg;
static bool g_bank_A_crc_pass_flg;
static bool g_bank_B_crc_pass_flg;
static bool g_csot_gma_crc_pass_flg;

uint8_t g_csot_dl_len;
uint8_t *g_csot_bank_buf;
uint8_t *g_bank_adr;
// *******************************************************************
// *******************************************************************
volatile union rw_csot_fw_sta xdata g_rw_csot_fw_sta;
// *******************************************************************
// *******************************************************************
//uint16_t l_i2c_wr_dly_time = 0;
uint16_t xdata array_i2c_wr_dly_time[4] = {5, 50, 500, 6000};
uint16_t xdata array_vcom_wr_dly_time[4] = {5, 50, 500, 6000};
// *******************************************************************
// *******************************************************************
void CSOT_RTPM_DL_BANK_GMA(uint8_t l_mem_type, uint8_t mem_len , uint8_t bank ,uint8_t l_flash_path);
// *******************************************************************
// *******************************************************************
void CSOT_RTPM_Init(void)
{
	g_rw_csot_fw_sta.byte = 0x00;
	g_rtpm_pmic_status_reg[0] = 0x00;
	g_rtpm_pmic_status_reg[1] = 0x00;
	g_rtpm_pmic_status_reg[2] = 0x00;
	g_rtpm_pmic_status_reg[3] = 0x00;

	g_i2c_wr_dly_time = array_i2c_wr_dly_time[g_rw_mcu_top_0002h.bits.r_i2c_wr_dly_tm];
	g_vcom_wr_dly_time = array_i2c_wr_dly_time[g_rw_mcu_top_0002h.bits.r_vcom_delay_sel];
}

void CSOT_RTPM_Set_Fw_Sta_Busy(bool en)
{
	g_rw_csot_fw_sta.bits.busy_bit = en;
}

#if 0
void CSOT_RTPM_Set_Crc_Fail_Tcon_Exi2c(bool en)
{
	g_rw_csot_fw_sta.bits.crc_fail_tcon_exi2c = en;
}

void CSOT_RTPM_Set_Flash_Conn_Sta(bool en)
{
	g_rw_csot_fw_sta.bits.flash_connection_status = en;
}
#endif

void CSOT_RTPM_Set_Fw_Sta_Crc_Fail_Tcon_Pmic_Flash(bool en)
{
	g_rw_csot_fw_sta.bits.crc_fail_tcon_pmic_flash = en;
}

uint8_t CSOT_RTPM_Ret_Fw_Sta_Crc_Fail_Tcon_Pmic_Flash(void)
{
	return g_rw_csot_fw_sta.bits.crc_fail_tcon_pmic_flash;
}

#if 0
void CSOT_RTPM_Set_I2c_Bypass_Sta(bool en)
{
	g_rw_csot_fw_sta.bits.i2c_bypass_mode_sta = en;
}

void CSOT_RTPM_Set_I2c_To_Spi_Valid(bool en)
{
	g_rw_csot_fw_sta.bits.i2c_to_spi_mode_valid = en;
}

void CSOT_RTPM_Clr_Pmic_En(void)
{
	//g_csot_rtpm_proc_flg = 1;
	g_mcu_entry_cfg_4_u.bits.rtpm_en = 0;
}
#endif

bool CSOT_Decide_Flash_Path(void)
{
	if(g_comm_hdr_buf.driving_struct)
		return FLASH_CS_CBOARD;
	else
		return FLASH_CS_XBOARD;
}

void CSOT_RTPM_Storage_Crc(void)
{
	uint8_t l_bank_A_crc_start_adr;
	uint8_t l_bank_B_crc_start_adr;

	l_bank_A_crc_start_adr = (g_comm_hdr_buf.bankA_len - 2);

	l_bank_B_crc_start_adr = (g_comm_hdr_buf.bankB_len - 2);

	g_rw_mcu_pmic_bankA_crc.bits.crc_msb = g_pmic_bank_a_buf[l_bank_A_crc_start_adr];
	g_rw_mcu_pmic_bankA_crc.bits.crc_lsb = g_pmic_bank_a_buf[(l_bank_A_crc_start_adr + 1)];

	g_rw_mcu_pmic_bankB_crc.bits.crc_msb = g_pmic_bank_b_buf[l_bank_B_crc_start_adr];
	g_rw_mcu_pmic_bankB_crc.bits.crc_lsb = g_pmic_bank_b_buf[(l_bank_B_crc_start_adr + 1)];

}

void CSOT_RTPM_Set_PMIC_Cfg_Pass(bool en)
{
	g_csot_pmic_pass_cfg_flg = en;
}

#if 0
bool CSOT_RTPM_Check_Dl_En(void)
{
	return g_mcu_entry_cfg_4_u.bits.rtpm_en;
}

void CSOT_RTPM_Set_PMIC_GPIO(uint8_t pin)
{
	g_system_reg_032h.bits.r_oen_mux_sel23 = pin;
}
#endif 

//void CSOT_RTPM_Dl_PMIC_Finish(bool en)

bool COST_RTPM_Write_PMIC_Code_To_PMIC(void)
{
	uint8_t l_pmic_dev;
	uint8_t l_reg_reg[4];
	uint8_t l_reg_len;
	if(g_comm_hdr_buf.pmic_total_bank_num == 0)
		return RTPM_DL_PASS;
	//P0_7 = 0;	
	//l_pmic_dev = g_mcu_reg_008h.bits.pmic_adr;
	l_pmic_dev = g_comm_hdr_buf.pmic_dev_addr;

	// get one or two byte mode
	if(!g_rw_mcu_top_0037h.bits.r_csot_pmic_reg_mode == 0)
	{
		l_reg_reg[0] = g_rw_mcu_top_000Bh_pmic_banka_start_adr;
		l_reg_reg[1] = 0x00; 
		l_reg_reg[2] = g_rw_mcu_top_000Ch_pmic_bankb_start_adr;
		l_reg_reg[3] = 0x00; 
		l_reg_len = 1;
	}
	else
	{
		l_reg_reg[0] = 0x00;
		l_reg_reg[1] = g_rw_mcu_top_000Bh_pmic_banka_start_adr;
		l_reg_reg[2] = 0x00;
		l_reg_reg[3] = g_rw_mcu_top_000Ch_pmic_bankb_start_adr;
		l_reg_len = 2;
	}

    // switch to master
	CSOT_Decide_PMIC_I2C_Path(g_rw_mcu_top_0027h.bits.r_pmic_i2c_path);

    // write bankB    
	//if(g_mcu_reg_00Eh.bits.total_bank_num > 1)
	if(g_comm_hdr_buf.pmic_total_bank_num > 1)
	{
		if(mcu_i2c_mst_write(l_pmic_dev, &l_reg_reg[2] , l_reg_len ,&g_pmic_bank_b_buf[0], g_comm_hdr_buf.bankB_len, 0, g_vcom_wr_dly_time) == I2C_FAIL)
			goto RTPM_FAIL;
		
		// mcu_timer_wait_us(l_i2c_wr_dly_time);		// delay 24c02. JC add 220315 , 6ms / RTL sim mask
	}
	
	// write bankA
	if(mcu_i2c_mst_write(l_pmic_dev, &l_reg_reg[0] , l_reg_len , &g_pmic_bank_a_buf[0], g_comm_hdr_buf.bankA_len, 0, g_vcom_wr_dly_time) == I2C_FAIL)
		goto RTPM_FAIL;
	else
		goto RTPM_PASS;
	
	RTPM_FAIL:
	    // switch to slave
		g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;
	    // mcu_timer_wait_us(l_i2c_wr_dly_time);
		return RTPM_DL_FAIL; 
	
	RTPM_PASS:
		g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;
		//P0_7 = 1;
	    // mcu_timer_wait_us(l_i2c_wr_dly_time);
		return RTPM_DL_PASS; 
}

static bool CSOT_RTPM_IC_TYPE(void)
{
	if(g_rw_mcu_top_0011h.bits.r_pmic_type == 0 || g_rw_mcu_top_0011h.bits.r_pmic_type == 1)	// cs601 / 602
	{
		return 0;
	}
	else
	{
		//if(!g_system_reg_032h.bits.r_oen_mux_sel23)
		if(!CSOT_BANK_SEL)
			return 0;   // low : choose bankA
		else
			return 1;   // high : choose bankB
	}
}


#if 0
static bool CSOT_RTPM_Tcon_Dl_PMIC_Common_From_CB_Flash(void)
{
	uint8_t i;
	uint8_t l_i2c_wr_buf[4];

	//mcu_i2c_mst_read((uint8_t *)&g_test_addr,2,&g_pmic_data_buf[0],PMIC_DATA_BUF_SZ,0,0);
	
    for(i = 0 ; i < RETRY_TIMES ; i++)
    {
		// Download PMIC from CB flash 
		//mcu_spi_flash_fast_read((uint8_t *)&g_rtpm_bank_a+1,(uint8_t *)&g_pmic_data_buf[0], PMIC_DATA_BUF_SZ, SPI_C_FLASH_PATH);
		// Download Header from CB flash 
		//mcu_spi_flash_fast_read((uint8_t *)&g_rtpm_common_hdr+1,(uint8_t *)&g_comm_hdr_buf, COMM_HDR_BUF_SZ, SPI_C_FLASH_PATH);
		// Download PMIC  
		mcu_i2c_mst_read(&l_i2c_wr_buf[0],2,&g_pmic_data_buf[0],PMIC_DATA_BUF_SZ,0,0,0);
		// Download Header 
		mcu_i2c_mst_read(&l_i2c_wr_buf[0],2,(uint8_t *)&g_comm_hdr_buf,COMM_HDR_BUF_SZ,0,0,0);
		// PMIC code CRC check
		if(CSOT_Crc_Cal(&g_pmic_data_buf, PMIC_DATA_BUF_SZ) == 0x0000)
			return TRUE;
	}
	return FALSE;

}

static void CSOT_RTPM_Tcon_Dl_Bank_Code_To_RAM(uint8_t bank, uint8_t mem_type)
{
	uint8_t i;
	uint8_t start_addr = 0;
	uint8_t l_reg_len = 0;

	if(bank == BANK_A)
		start_addr = 0;			
	else if(bank == BANK_B)
		start_addr = 48;

	if(BUS_ENI)
        l_reg_len = 1;
    else
        l_reg_len = 2;

	for(i = 0 ; i < RETRY_TIMES ; i++)
	{
		// Download bank from X board
		// xb_mem_type => 1:EEPROM(I2C), 0:Flash(SPI)
		/*if(mem_type)
		{
			mcu_i2c_mst_read(&g_pmic_data_buf[start_addr], l_reg_len, (uint8_t *)&bank, PMIC_BANK_BUF_SZ, 0,0,0);
		}else
		{
			mcu_spi_flash_fast_read((uint8_t *)&bank+1,(uint8_t *)&g_pmic_data_buf[start_addr], PMIC_BANK_BUF_SZ, SPI_X_FLASH_PATH);
		}*/
		mcu_i2c_mst_read(&g_pmic_data_buf[start_addr], l_reg_len, (uint8_t *)&bank, PMIC_BANK_BUF_SZ, 0,0,0);
		// X board bank PMIC code CRC check
		if(CSOT_Crc_Cal(&g_pmic_data_buf, PMIC_BANK_BUF_SZ) == 0x0000)
		{
			return;
		}	
	}
	// Download bank from C board
	/*if(!mem_type)
	{
		mcu_spi_flash_fast_read((uint8_t *)&bank+1,(uint8_t *)&g_pmic_data_buf[start_addr], PMIC_BANK_BUF_SZ, SPI_C_FLASH_PATH);
	}*/
}

#endif
static bool CSOT_RTPM_Cks_Crc_Flg(uint8_t l_bank)
{
	if(l_bank == BANK_A)
		return g_bank_A_crc_pass_flg;
	else
		return g_bank_B_crc_pass_flg;
}


bool CSOT_RTPM_Tcon_Check_Common_Cfg(void)
{
	if(g_comm_hdr_buf.driving_struct != 0)
	{
		return 0;
	}
	return 1;
}

static bool CSOT_RTPM_Read_PMIC_Status(void)
{
	bool l_res;
	uint8_t l_reg_len;

	//g_rtpm_pmic_status_adr[0] = g_rw_mcu_top_0008h.byte;
	g_rtpm_pmic_status_adr[0] = g_comm_hdr_buf.pmic_dev_addr;
	
	if(!g_rw_mcu_top_0037h.bits.r_csot_pmic_reg_mode)
	{
		g_rtpm_pmic_status_adr[1] = g_rw_mcu_top_0012h_pmic_status_adr;
		l_reg_len = 1;
	}
	else
	{
		g_rtpm_pmic_status_adr[1] = 0x00;
		g_rtpm_pmic_status_adr[2] = g_rw_mcu_top_0012h_pmic_status_adr;
		l_reg_len = 2;
	}
	
    // switch to master 
	CSOT_Decide_PMIC_I2C_Path(g_rw_mcu_top_0027h.bits.r_pmic_i2c_path);
	l_res = mcu_i2c_mst_read(&g_rtpm_pmic_status_adr[0], l_reg_len, &g_rtpm_pmic_status_reg[0], 4, 0, g_vcom_wr_dly_time,0);
	// switch to slave 
	g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;

	return l_res;
}

bool CSOT_RTPM_Cal_PMIC_Status_For_Mcu(uint16_t CRC_Code_H, uint16_t CRC_Code_L)
{
	bool l_pimic_status;

	l_pimic_status = ((g_rtpm_pmic_status_reg[0]&0xAA) == (((CRC_Code_L<<7)&0x80)|((CRC_Code_H<<3)&0x20)|((CRC_Code_L>>2)&0x08)|((CRC_Code_H>>3)&0x02))) && \
					 ((g_rtpm_pmic_status_reg[1]&0x2B) == (((CRC_Code_L>>1)&0x20)|((CRC_Code_L<<1)&0x08)|((CRC_Code_H<<1)&0x02)|((CRC_Code_H>>6)&0x01))) && \
					 ((g_rtpm_pmic_status_reg[2]&0xA9) == (((CRC_Code_H<<4)&0x80)|((CRC_Code_L<<4)&0x20)|((CRC_Code_L>>4)&0x08)|((CRC_Code_L>>3)&0x01))) && \
					 ((g_rtpm_pmic_status_reg[3]&0x36) == (((CRC_Code_H<<4)&0x20)|((CRC_Code_H>>3)&0x10)|((CRC_Code_L>>2)&0x04)|((CRC_Code_H>>4)&0x02)));

	return l_pimic_status;
}

bool CSOT_RTPM_Cal_PMIC_Status(uint16_t CRC_Code_H, uint16_t CRC_Code_L)
{
	#if 0
	bool l_pimic_status;

	l_pimic_status = ((g_rtpm_pmic_status_reg[0]&0xAA) == (((CRC_Code_L<<7)&0x80)|((CRC_Code_H<<3)&0x20)|((CRC_Code_L>>2)&0x08)|((CRC_Code_H>>3)&0x02))) && \
					 ((g_rtpm_pmic_status_reg[1]&0x2B) == (((CRC_Code_L>>1)&0x20)|((CRC_Code_L<<1)&0x08)|((CRC_Code_H<<1)&0x02)|((CRC_Code_H>>6)&0x01))) && \
					 ((g_rtpm_pmic_status_reg[2]&0xA9) == (((CRC_Code_H<<4)&0x80)|((CRC_Code_L<<4)&0x20)|((CRC_Code_L>>4)&0x08)|((CRC_Code_L>>3)&0x01))) && \
					 ((g_rtpm_pmic_status_reg[3]&0x36) == (((CRC_Code_H<<4)&0x20)|((CRC_Code_H>>3)&0x10)|((CRC_Code_L>>2)&0x04)|((CRC_Code_H>>4)&0x02)));

	return l_pimic_status;
	#endif
	bool l_pimic_status;
	#if 1
	if(g_rw_mcu_top_0007h.bits.r_csot_crc_mcu == 0)
	{
		/*g_rtpm_pmic_status_reg[0] = 0xAA;
	g_rtpm_pmic_status_reg[1] = 0x2B;
	g_rtpm_pmic_status_reg[2] = 0xA9;
		g_rtpm_pmic_status_reg[3] = 0x36;*/
	g_rw_hw_reg_FFF1h_pmic_reg_0 = g_rtpm_pmic_status_reg[0];
	g_rw_hw_reg_FFF2h_pmic_reg_1 = g_rtpm_pmic_status_reg[1];
	g_rw_hw_reg_FFF3h_pmic_reg_2 = g_rtpm_pmic_status_reg[2];
	g_rw_hw_reg_FFF4h_pmic_reg_3 = g_rtpm_pmic_status_reg[3];
	(uint16_t)g_rw_hw_reg_FFF5h_pmic_crc_h_7_0 = CRC_Code_H;
	(uint16_t)g_rw_hw_reg_FFF7h_pmic_crc_l_7_0 = CRC_Code_L;
	g_rw_hw_reg_FFF0h.bits.start = 1;
	while(g_rw_hw_reg_FFF0h.bits.start != 0);
	return g_r_hw_reg_FFF9h.bits.pmic_sta;
	}
	else
	{
		l_pimic_status = CSOT_RTPM_Cal_PMIC_Status_For_Mcu(CRC_Code_H,CRC_Code_L);

	return l_pimic_status;
	}
#endif
}


static bool CSOT_RTPM_PMIC_Status_Check(uint8_t bank)
{
	uint16_t CRC_Code_H = 0x0000;
	uint16_t CRC_Code_L = 0x0000;
	//bool l_pimic_status;

	if(!g_rw_mcu_top_0011h.bits.r_crc_en)
		return TRUE;

	if(bank == BANK_A)
	{
		CRC_Code_H = g_rw_mcu_pmic_bankA_crc.bits.crc_msb;
		CRC_Code_L = g_rw_mcu_pmic_bankA_crc.bits.crc_lsb;

	}else if(bank == BANK_B)
	{
		CRC_Code_H = g_rw_mcu_pmic_bankB_crc.bits.crc_msb;
		CRC_Code_L = g_rw_mcu_pmic_bankB_crc.bits.crc_lsb;
	}
	
	//l_pimic_status = CSOT_RTPM_Cal_PMIC_Status(CRC_Code_H, CRC_Code_L);

	if(CSOT_RTPM_Cal_PMIC_Status(CRC_Code_H, CRC_Code_L))
		return TRUE;
	else
		return FALSE;
	
}


static bool CSOT_RTPM_Tcon_Upload_PMIC_Code_To_PMIC(void)
{
	uint8_t i;
	bool l_sel_bank;
	
	for(i = 0 ; i < RETRY_TIMES ; i++)
    {
		//P0_6 = 0;
		// Update PMIC code to PMIC Bank A&B Reg By I2c Write
		if(COST_RTPM_Write_PMIC_Code_To_PMIC() == RTPM_DL_FAIL)
			continue;
		// Read PMIC Status By I2C Master
		CSOT_RTPM_Read_PMIC_Status();
		//P0_6 = 1;

		l_sel_bank = CSOT_RTPM_IC_TYPE();

		if(CSOT_RTPM_PMIC_Status_Check(l_sel_bank))
			return TRUE;

		mcu_timer_wait_us(g_i2c_wr_dly_time);		// delay 24c02. Peter add 220426 , 6ms / RTL sim mask	
	}
	return FALSE;
}

static void CSOT_RTPM_DL_HDR(uint8_t l_mem_type, uint8_t mem_len)
{
	//bool l_cs;
	//uint16_t *l_mem_reg = &g_rom_entry_addr[1];
	uint8_t l_header_len = 0;
	uint16_t l_hdr_crc;


	g_csot_hdr_crc_pass_flg = 0;
	l_header_len = g_rw_mcu_top_000Fh_pmic_header_len;
	
	if(l_mem_type == 0) 
		mcu_i2c_mst_read(&g_rom_cost_hdr_entry_addr[0], mem_len, (uint8_t*)&g_comm_hdr_buf, l_header_len, 1, g_i2c_wr_dly_time,0);
	else 
	{			
	#ifdef FLASH_CODE																				// JC add 220308
		mcu_spi_flash_fast_read(&g_rom_cost_hdr_entry_addr[0],(uint8_t *)&g_comm_hdr_buf,(uint16_t)l_header_len , FLASH_CS_CBOARD);
	#endif
	}
	// Eric Add RTPM Flow - 20220808
	l_hdr_crc = CSOT_Crc_FW_Cal(&g_comm_hdr_buf.common_hdr_len,38);

	if(l_hdr_crc == g_comm_hdr_buf.common_hdr_chks)
	{
		g_csot_hdr_crc_pass_flg = 1;
	}
}

bool Check_CSOT_Bank_Data(uint8_t* buf ,uint8_t len )
{
	uint8_t i;
	uint8_t l_fail_cnt = 0;
	for(i == 0; i < len ; i++)
	{
		if(buf[i] == 0 || buf[i] == 0xFF)
			l_fail_cnt ++;
		//return 1;
	}
	if(l_fail_cnt == len)
		return FALSE;
	else 
		return TRUE;
}

static void CSOT_RTPM_Access_Ptr(uint8_t bank,uint8_t l_flash_path)
{
	if(bank == BANK_A)
	{
		g_csot_bank_buf = &g_pmic_bank_a_buf[0];
		g_csot_dl_len = g_comm_hdr_buf.bankA_len;
		g_bank_A_crc_pass_flg = 0;
		if(l_flash_path == FLASH_CS_CBOARD)
			//l_bank_adr = &g_mcu_reg_r_c_board_pmic_bankA_adr1; 
			g_bank_adr = &g_rom_cost_bankA_entry_addr[0];
		else
			g_bank_adr = &g_rw_mcu_top_001Ah_x_board_pmic_bankA_adr1; 
	}
	else if(bank == BANK_B) // bank B
	{
		g_csot_bank_buf = &g_pmic_bank_b_buf[0];
		g_csot_dl_len = g_comm_hdr_buf.bankB_len;
		g_bank_B_crc_pass_flg = 0;
		if(l_flash_path == FLASH_CS_CBOARD)
			g_bank_adr = &g_rom_cost_bankB_entry_addr[0];
			//l_bank_adr = &g_mcu_reg_r_c_board_pmic_bankB_adr1; 
		else
			g_bank_adr = &g_rw_mcu_top_001Dh_x_board_pmic_bankB_adr1; 
	}
	else
	{
		g_csot_bank_buf = &g_csot_gma_buf[0];
		g_csot_dl_len = g_rw_mcu_top_0010h_pmic_gma_len;
		g_csot_gma_crc_pass_flg = 0;
		if(l_flash_path == FLASH_CS_CBOARD)
			//l_bank_adr = &g_mcu_reg_r_c_board_pmic_bankA_adr1; 
			g_bank_adr = &g_rom_cost_gma_entry_addr[0];
		else
			g_bank_adr = &g_rw_mcu_top_0017h_x_board_pmic_gma_adr1;
	}
}

void CSOT_RTPM_DL_BANK_GMA(uint8_t l_mem_type, uint8_t mem_len , uint8_t bank ,uint8_t l_flash_path)
{
	//uint8_t l_cs;
	//uint8_t l_dl_len;
	//uint8_t *l_bank_buf;
	//uint8_t *l_bank_adr;
#if 0
	if(bank == BANK_A)
	{
		l_bank_buf = &g_pmic_bank_a_buf[0];
		l_dl_len = g_comm_hdr_buf.bankA_len;
		g_bank_A_crc_pass_flg = 0;
		if(l_flash_path == FLASH_CS_CBOARD)
			//l_bank_adr = &g_mcu_reg_r_c_board_pmic_bankA_adr1; 
			l_bank_adr = &g_rom_cost_bankA_entry_addr[0];
		else
			l_bank_adr = &g_rw_mcu_top_001Ah_x_board_pmic_bankA_adr1; 
	}
	else if(bank == BANK_B) // bank B
	{
		l_bank_buf = &g_pmic_bank_b_buf[0];
		l_dl_len = g_comm_hdr_buf.bankB_len;
		g_bank_B_crc_pass_flg = 0;
		if(l_flash_path == FLASH_CS_CBOARD)
			l_bank_adr = &g_rom_cost_bankB_entry_addr[0];
			//l_bank_adr = &g_mcu_reg_r_c_board_pmic_bankB_adr1; 
		else
			l_bank_adr = &g_rw_mcu_top_001Dh_x_board_pmic_bankB_adr1; 
	}
	else
	{
		l_bank_buf = &g_csot_gma_buf[0];
		l_dl_len = g_rw_mcu_top_0010h_pmic_gma_len;
		g_csot_gma_crc_pass_flg = 0;
		if(l_flash_path == FLASH_CS_CBOARD)
			//l_bank_adr = &g_mcu_reg_r_c_board_pmic_bankA_adr1; 
			l_bank_adr = &g_rom_cost_gma_entry_addr[0];
		else
			l_bank_adr = &g_rw_mcu_top_0017h_x_board_pmic_gma_adr1;
	}
#endif
	CSOT_RTPM_Access_Ptr(bank,l_flash_path);
	HW_CRC_CAL_ON;
	if(l_mem_type == 0) // 0: i2c dl   1: spi dl
	{
		mcu_i2c_mst_read(g_bank_adr, mem_len, g_csot_bank_buf, g_csot_dl_len, 1, g_i2c_wr_dly_time,0);
	}
	else 
	{			
	#ifdef FLASH_CODE																				
		mcu_spi_flash_fast_read(l_bank_adr,l_bank_buf,(uint16_t)l_dl_len , l_flash_path);
	#endif
	}
	// ----- 00 or FF -----------//
	if(Check_CSOT_Bank_Data(g_csot_bank_buf,g_csot_dl_len) == FALSE)	
	{
		if(bank == BANK_A)
			g_bank_A_crc_pass_flg = 0;
		else if(bank == BANK_B)
			g_bank_B_crc_pass_flg = 0;
		else
			g_csot_gma_crc_pass_flg = 0;
		HW_CRC_CAL_OFF;
		return;
	}	
	// ----- 00 or FF -----------//
	if(CSOT_Crc_HW_Cal() == 0x0000)
	{
		if(bank == BANK_A)
			g_bank_A_crc_pass_flg = 1;
		else if(bank == BANK_B)
			g_bank_B_crc_pass_flg = 1;
		else
			g_csot_gma_crc_pass_flg = 1;
	}
	HW_CRC_CAL_OFF;
}

bool CSOT_RTPM_Cal_Dl_Crc_Pass(void)	//  l_check_type : 0x00
{
	if(!g_rw_mcu_top_0011h.bits.r_crc_en)
		return TRUE;

	// delay 400us
	mcu_timer_wait_us(400);
	//if(g_csot_hdr_crc_pass_flg & g_bank_A_crc_pass_flg & g_bank_B_crc_pass_flg & g_csot_gma_crc_pass_flg) 
	if(g_csot_hdr_crc_pass_flg & g_bank_A_crc_pass_flg & g_bank_B_crc_pass_flg/* & g_csot_gma_crc_pass_flg*/) 
	{
		if(g_rw_mcu_top_0011h.bits.r_pmic_gma_en)
		{
			if(!g_csot_gma_crc_pass_flg)
				return FALSE;
		}
		CSOT_RTPM_Storage_Crc();
		return TRUE;
	}
	return FALSE;
}

void CSOT_RTPM_Tcon_DL_CBoard(uint8_t mem_len)
{
	uint8_t i;
	for(i = 0 ; i < RETRY_TIMES ; i++)
	{
		// dl hdr
		CSOT_RTPM_DL_HDR(g_i2c0_spi1_sel, mem_len);	
		// dl gma
		if(g_rw_mcu_top_0011h.bits.r_pmic_gma_en)
			CSOT_RTPM_DL_BANK_GMA(g_i2c0_spi1_sel, mem_len , GMA , FLASH_CS_CBOARD);
		// dl bank A
		CSOT_RTPM_DL_BANK_GMA(g_i2c0_spi1_sel, mem_len , BANK_A , FLASH_CS_CBOARD);
		// dl bank B
		if(g_comm_hdr_buf.pmic_total_bank_num > 1)
			CSOT_RTPM_DL_BANK_GMA(g_i2c0_spi1_sel, mem_len , BANK_B , FLASH_CS_CBOARD);
		// check crc
		if(CSOT_RTPM_Cal_Dl_Crc_Pass())
			return;
	}
	CSOT_RTPM_Set_Fw_Sta_Crc_Fail_Tcon_Pmic_Flash(1);
}


void CSOT_RTPM_DL_BANK_GMA_To_RAM_Handler(uint8_t l_mem_type,uint8_t l_mem_len,uint8_t l_bank)
{
	uint8_t l_retry_cnt;

	//CSOT_RTPM_Clr_Crc_Flg(l_bank);
	// DL x board
	for(l_retry_cnt = 0; l_retry_cnt < RETRY_TIMES ; l_retry_cnt++)
	{
		CSOT_RTPM_DL_BANK_GMA(l_mem_type , l_mem_len , l_bank ,FLASH_CS_XBOARD);
		if(CSOT_RTPM_Cks_Crc_Flg(l_bank))
			return ;
	}
	// if X board Download Fail , download C Board
	CSOT_RTPM_DL_BANK_GMA(l_mem_type , l_mem_len , l_bank ,FLASH_CS_CBOARD);
}

void CSOT_RTPM_Tcon_DL_XBoard(uint8_t mem_len)
{
	uint8_t l_xb_memory_code_disable;
	uint8_t l_mem_type;

	l_mem_type = ~g_comm_hdr_buf.xb_mem_type;
	l_xb_memory_code_disable = g_comm_hdr_buf.xb_flash_code_disable;
#ifdef FLASH_CODE
	if(l_mem_type) // Flash
	{
		// Check Auto Cal Flash Size En
        if(g_rw_mcu_top_0024h.bits.r_auto_cal_flash_size_en)
        {
            mcu_spi_get_flash_size(FLASH_CS_XBOARD);
            CSOT_RTPM_Update_Addr();
        }
	}
#endif
	switch(l_xb_memory_code_disable)
	{
		case 0x00:
			CSOT_RTPM_DL_BANK_GMA_To_RAM_Handler(l_mem_type , mem_len , BANK_B);
			CSOT_RTPM_DL_BANK_GMA_To_RAM_Handler(l_mem_type , mem_len , BANK_A);
			CSOT_RTPM_DL_BANK_GMA_To_RAM_Handler(l_mem_type , mem_len , GMA);
			break;
		case 0x01:
			CSOT_RTPM_DL_BANK_GMA_To_RAM_Handler(l_mem_type , mem_len , BANK_B);
			break;
		case 0x02:
			CSOT_RTPM_DL_BANK_GMA_To_RAM_Handler(l_mem_type , mem_len , BANK_A);
			break;
		case 0x03:
			break;
		default:
			break;			
	}	
}
#ifdef FLASH_CODE
void CSOT_RTPM_Update_Addr(void)
{
	uint32_t l_start_adr;
	if(g_xboard_flash_size == 0 )
		return;
	// X Board gma Start Addr
	l_start_adr = (g_xboard_flash_size - FLASH_SECTOR_SZ);
	g_rw_mcu_top_0017h_x_board_pmic_gma_adr1 = (l_start_adr >> 16) & 0xFF;
	g_rw_mcu_top_0029h_x_board_pmic_gma_adr2 = (l_start_adr >> 8) & 0xFF;
	g_rw_mcu_top_002Ah_x_board_pmic_gma_adr3 = (l_start_adr + 0x30) & 0xFF;

	// X Board BankA Start Addr
	l_start_adr = (l_start_adr - FLASH_SECTOR_SZ);
	g_rw_mcu_top_001Ah_x_board_pmic_bankA_adr1 = (l_start_adr >> 16) & 0xFF;
	g_rw_mcu_top_002Ch_x_board_pmic_bankA_adr2 = (l_start_adr >> 8) & 0xFF;
	g_rw_mcu_top_002Dh_x_board_pmic_bankA_adr3 = l_start_adr & 0xFF; 

	// X Board BankB Start Addr
	l_start_adr = (l_start_adr - FLASH_SECTOR_SZ);
	g_rw_mcu_top_001Dh_x_board_pmic_bankB_adr1 = (l_start_adr >> 16) & 0xFF;
	g_rw_mcu_top_002Fh_x_board_pmic_bankB_adr2 = (l_start_adr >> 8) & 0xFF;
	g_rw_mcu_top_0030h_x_board_pmic_bankB_adr3 = l_start_adr & 0xFF;  
}
#endif
//---------------------------------------------------//
//---------------------------------------------------//
//---------------------------------------------------//
uint8_t xdata g_crc_msb = 0;
uint8_t xdata g_crc_lsb = 0;

bool CSOT_ReadPMIC_Code_From_PMIC(uint8_t ofset , uint8_t len , bool all_read_flg, bool cks_flg)
{
	//bool l_bank;
	uint8_t l_buf_ofset;
	uint8_t l_i2c_write_buf[2];
	if(all_read_flg)
	{
		l_buf_ofset = ofset;
		//g_rtpm_i2c_write_buf[1] = ofset;
		l_i2c_write_buf[1] = ofset;
	}
	else
	{
		l_buf_ofset = 0;
		l_i2c_write_buf[1] = mcu_i2c2spi_cmd_buf[1];
	}

	l_i2c_write_buf[0] = g_comm_hdr_buf.pmic_dev_addr;
	HW_CRC_CAL_ON;
	//mcu_i2c_mst_read(&g_rtpm_i2c_write_buf[0], 1, &g_i2c2spi_read_buf[l_buf_ofset], len , 0);
	mcu_i2c_mst_read(&l_i2c_write_buf[0], 1, &g_i2c2spi_read_buf[l_buf_ofset], len , 0, g_i2c_wr_dly_time,0);
	if(ofset == 0)
	{
	g_crc_msb = g_r_sys_data_13_u.bits.crc_msb;
	g_crc_lsb = g_r_sys_data_13_u.bits.crc_lsb;
	}else{
	g_crc_msb = g_r_sys_data_13_u.bits.crc_msb;
	g_crc_lsb = g_r_sys_data_13_u.bits.crc_lsb;
	}
	HW_CRC_CAL_OFF;
	if(cks_flg)
	{
		//if(CSOT_Crc_HW_Cal() == 0x0000)
		if(g_crc_lsb == 0x00 && g_crc_msb == 0x00)
			return TRUE;
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}

#if 0
static bool CSOT_RTPM_Read_Flash_Id(void)
{
	bool l_cs;
	l_cs = CSOT_Decide_Flash_Path();
	if(mcu_spi_flash_read_id(SPI_FLASH_CMD_READ_ID_3BYTE, l_cs) == FALSE) 
	{
	    return FALSE; 
	}
	if(CSOT_Spi_Check_Flash_ID(&mcu_global_buf) == FALSE)
	{	
		return FALSE;
	}

	return TRUE;
}

static void CSOT_RTPM_Get_Flash_Addr(uint8_t *ptr, bool cs, uint8_t bank)
{ 
	uint8_t *l_rom_adr;
	if(cs == FLASH_CS_CBOARD)
	{
		if(bank == BANK_A)
			l_rom_adr = &g_rom_cost_bankA_entry_addr[0];
		else if(bank == BANK_B)
			l_rom_adr = &g_rom_cost_bankB_entry_addr[0];
		else if(bank == PMIC_HDR)
			l_rom_adr = &g_rom_cost_hdr_entry_addr[0];
	}
	else if(cs == FLASH_CS_XBOARD)
	{
		if(bank == BANK_A)
			l_rom_adr = &g_rw_mcu_top_001Ah_x_board_pmic_bankA_adr1;
		else if(bank == BANK_B)
			l_rom_adr = &g_rw_mcu_top_001Dh_x_board_pmic_bankB_adr1;
	}
	
	ptr[0] = *(l_rom_adr+0);
	ptr[1] = *(l_rom_adr+1);
	ptr[2] = *(l_rom_adr+2);
}

static void CSOT_RTPM_Read_Flash_and_Set_Status(void)
{
	uint8_t l_data_tmp;
	bool l_cs;
	l_cs = CSOT_Decide_Flash_Path();
	l_data_tmp = (mcu_spi_flash_read_status1(l_cs) & 0xE3);
	mcu_spi_flash_write_status(l_data_tmp, l_cs);
}
#endif

uint8_t CSOT_Get_BANK(uint8_t offset)
{
	if(offset == 0xFF)
		return PMIC_HDR;
	if(offset == 0xFE)
		return GMA;
	else if(offset < 0x30)
		return BANK_A;
	else	
		return BANK_B;
}

uint8_t CSOT_Get_Len(uint8_t bank)
{
	if(bank == BANK_A)
		return g_comm_hdr_buf.bankA_len;
	else if(bank == BANK_B)
		return g_comm_hdr_buf.bankB_len;
	else if(bank == GMA)
		return 0x10;
	else
		return g_comm_hdr_buf.common_hdr_len;
}

uint32_t CSOT_Get_Flash_Adr(uint8_t bank,bool cs)
{
	uint32_t l_pmic_addr;
	if(cs == FLASH_CS_XBOARD)
	{
		if(bank == BANK_A)
			VAR_MERGER_32(l_pmic_addr, g_rw_mcu_top_001Ah_x_board_pmic_bankA_adr1);
		else if(bank == BANK_B)
			VAR_MERGER_32(l_pmic_addr, g_rw_mcu_top_001Dh_x_board_pmic_bankB_adr1);
		else if(bank == GMA)
			VAR_MERGER_32(l_pmic_addr, g_rw_mcu_top_0017h_x_board_pmic_gma_adr1);
		else if(bank == PMIC_HDR)
			VAR_MERGER_32(l_pmic_addr, g_rw_mcu_top_0014h_x_board_pmic_hdr_adr1);
	}
	else
	{
		if(bank == BANK_A)
			VAR_MERGER_32(l_pmic_addr, g_rom_cost_bankA_entry_addr[0]);
		else if(bank == BANK_B)
			VAR_MERGER_32(l_pmic_addr, g_rom_cost_bankB_entry_addr[0]);
		else if(bank == GMA)
			VAR_MERGER_32(l_pmic_addr, g_rom_cost_gma_entry_addr[0]);
		else if(bank == PMIC_HDR)
			VAR_MERGER_32(l_pmic_addr, g_rom_cost_hdr_entry_addr[0]);
	}

	l_pmic_addr >>=8;
	
	return l_pmic_addr;
}

#if 0
uint8_t *CSOT_Get_PMIC_Adr(uint8_t bank,bool cs)
{
	if(cs == FLASH_CS_XBOARD)
	{
		if(bank == BANK_A)
			return &g_rw_mcu_top_001Ah_x_board_pmic_bankA_adr1;
			//VAR_MERGER_32(l_pmic_addr, g_rw_mcu_top_001Ah_x_board_pmic_bankA_adr1);
		else if(bank == BANK_B)
			return &g_rw_mcu_top_001Dh_x_board_pmic_bankB_adr1;
			//VAR_MERGER_32(l_pmic_addr, g_rw_mcu_top_001Dh_x_board_pmic_bankB_adr1);
		else if(bank == GMA)
			return &g_rw_mcu_top_0017h_x_board_pmic_gma_adr1;
			//VAR_MERGER_32(l_pmic_addr, g_rw_mcu_top_0017h_x_board_pmic_gma_adr1);
		else 
			return &g_rw_mcu_top_0014h_x_board_pmic_hdr_adr1;
			//VAR_MERGER_32(l_pmic_addr, g_rw_mcu_top_0014h_x_board_pmic_hdr_adr1);
	}
	else
	{
		if(bank == BANK_A)
			return &g_rom_cost_bankA_entry_addr[0];
			//VAR_MERGER_32(l_pmic_addr, g_rom_cost_bankA_entry_addr[0]);
		else if(bank == BANK_B)
			return &g_rom_cost_bankB_entry_addr[0];
			//VAR_MERGER_32(l_pmic_addr, g_rom_cost_bankB_entry_addr[0]);
		else if(bank == GMA)	
			return g_rom_cost_gma_entry_addr[0];
			//VAR_MERGER_32(l_pmic_addr, g_rom_cost_gma_entry_addr[0]);
		else 
			return &g_rom_cost_hdr_entry_addr[0];
			//VAR_MERGER_32(l_pmic_addr, g_rom_cost_hdr_entry_addr[0]);
	}
}
#endif
uint8_t *CSOT_Get_PMIC_DataBuf(uint8_t bank)
{
	if(bank == BANK_A)
		return &g_pmic_bank_a_buf[0];
	if(bank == BANK_B)
		return &g_pmic_bank_b_buf[0];
	if(bank == GMA)
		return &g_csot_gma_buf[0];
	else
		return &g_comm_hdr_buf;
	/*else 
	{
		if(cs == FLASH_CS_XBOARD)
			return &g_csot_gma_buf[0];
		else
			return &g_comm_hdr_buf;
	}	*/
}

uint8_t CSOT_Get_Offset(uint8_t offset)
{
	if(offset == 0xFF ||offset == 0xFE)
		return 0;
	if(offset < 0x30)
		return (0x30 - offset);
	else
		return (0x60 - offset);
	
}

#if 0
static void CSOT_RTPM_Sector_Erase(uint8_t bank)
{
	bool l_cs;
	uint32_t l_ptr;
	//uint8_t *l_ptr;
	l_cs = CSOT_Decide_Flash_Path();
	//CSOT_RTPM_Get_Flash_Addr(&l_ptr,l_cs,bank);
	l_ptr = CSOT_Get_Flash_Adr(bank,l_cs);
	mcu_spi_flash_merge_erase(SPI_FLASH_CMD_SECTOR_ERASE,(uint8_t *)&l_ptr + 1, l_cs);
}

void CSOT_RTPM_Program_PMIC_Code_To_Flash(uint8_t bank)
{
	bool l_cs;
	uint32_t l_ptr;
	uint8_t *l_bank_buf;
	uint8_t l_dl_len = g_comm_hdr_buf.bankB_len;

	l_cs = CSOT_Decide_Flash_Path();
	l_ptr = CSOT_Get_Flash_Adr(bank,l_cs);
	
	if(bank == BANK_A)
	{
		l_bank_buf = &g_pmic_bank_a_buf[0];
		l_dl_len = g_comm_hdr_buf.bankA_len;
	}
	else if(bank == BANK_B)
	{
		l_bank_buf = &g_pmic_bank_b_buf[0];
		l_dl_len = g_comm_hdr_buf.bankB_len;
	}
	else if(bank == PMIC_HDR)
	{
		l_bank_buf = &g_rom_cost_hdr_entry_addr[0];
		l_dl_len = g_rw_mcu_top_000Dh_pmic_headelen;
	}
	mcu_spi_flash_page_program((uint8_t *)&l_ptr + 1,l_bank_buf,0x00,l_dl_len, l_cs);
}
#endif

static void CSOT_Cal_Hdr_Pmic_Binfile_Crc(void)
{
	uint8_t l_crc_buf[6];
	uint16_t l_crc_val;

	//l_crc_buf[0] = g_comm_hdr_buf.pmic_binfile_cks[0];		// msb
	//l_crc_buf[1] = g_comm_hdr_buf.pmic_binfile_cks[1];		// lsb
	l_crc_buf[0] = (g_comm_hdr_buf.common_hdr_chks) >> 8;		// msb
	l_crc_buf[1] = (g_comm_hdr_buf.common_hdr_chks) & 0xff;		// lsb
	l_crc_buf[2] = g_rw_mcu_pmic_bankA_crc.bits.crc_msb;	// msb
	l_crc_buf[3] = g_rw_mcu_pmic_bankA_crc.bits.crc_lsb;	// lsb
	l_crc_buf[4] = g_rw_mcu_pmic_bankB_crc.bits.crc_msb;	// msb
	l_crc_buf[5] = g_rw_mcu_pmic_bankB_crc.bits.crc_lsb;	// lsb

	l_crc_val = CSOT_Crc_FW_Cal(&l_crc_buf[0] , 6);
	g_comm_hdr_buf.pmic_binfile_cks[0] = l_crc_val >> 8;
	g_comm_hdr_buf.pmic_binfile_cks[1] = l_crc_val & 0xFF;
}

bool CSOT_ReadPMIC_Code_From_PMIC_Handler(uint8_t offset , uint8_t len)
{
	uint8_t l_retry;
	bool l_res;

	CSOT_Decide_PMIC_I2C_Path(g_rw_mcu_top_0027h.bits.r_pmic_i2c_path);
	if(offset == PMIC_READ_ALL_BANK)		// only for cmd 0x66
	{
		for(l_retry = 0 ; l_retry < RETRY_TIMES ; l_retry++)
		{
			//if(CSOT_ReadPMIC_Code_From_PMIC(0x0 , g_mcu_reg_00Bh.bits.pmic_bankA_len , 1 ,1) & CSOT_ReadPMIC_Code_From_PMIC(0x30,g_mcu_reg_00Ch.bits.pmic_bankB_len , 1 ,1))
			if(CSOT_ReadPMIC_Code_From_PMIC(0x30 , g_rw_mcu_top_000Eh_pmic_bankb_len , 1 ,1) & CSOT_ReadPMIC_Code_From_PMIC(0x00,g_rw_mcu_top_000Dh_pmic_banka_len , 1 ,1))
			{
				l_res = TRUE;
				break;
			}
			else
				l_res = FALSE;
		}
		g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;
		if(l_res == FALSE)
			return FALSE;
		memcpy(&g_pmic_bank_a_buf[0],&g_i2c2spi_read_buf[0],0x60);		// bank a and b 
		CSOT_RTPM_Storage_Crc();
		// cal hdr pmic bin file crc
		CSOT_Cal_Hdr_Pmic_Binfile_Crc();
		//l_buf[0] = 
		return TRUE;
	}
	else	// for cmd 0x67
	{
		CSOT_ReadPMIC_Code_From_PMIC(offset , len , 0 ,1);
		g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;
	}
	return TRUE;
}

static void CSOT_ReUpload_RTPM(void)
{
	g_rtpm_monitor_status = FALSE;		
	// Re-upload data from TCON to PMIC
	COST_RTPM_Write_PMIC_Code_To_PMIC();
}

void CSOT_RTPM_Tcon_Monitor_PMIC_CRC(void)
{
	uint8_t l_sel_bank = 0;

	// Chekck Enter Debug mode & Frame change
	if(g_rw_csot_fw_sta.bits.i2c_to_spi_mode_valid)
	return;

	// Read PMIC Status By I2C Master
	if(CSOT_RTPM_Read_PMIC_Status() == I2C_FAIL)
	{
		//g_rtpm_monitor_status = FALSE;		
		// Re-upload data from TCON to PMIC
		//COST_RTPM_Write_PMIC_Code_To_PMIC();
		CSOT_ReUpload_RTPM();
		return;
	}

	l_sel_bank = CSOT_RTPM_IC_TYPE();
	// Compare PMIC Status Reg CRC with TCON RAM CRC
	//if((g_mcu_reg_00Eh.bits.total_bank_num == 1 && CSOT_RTPM_PMIC_Status_Check(BANK_A)) || 
	//(g_mcu_reg_00Eh.bits.total_bank_num == 2 && CSOT_RTPM_PMIC_Status_Check(BANK_A) && CSOT_RTPM_PMIC_Status_Check(BANK_B)))
	if(CSOT_RTPM_PMIC_Status_Check(l_sel_bank))
	{	
		g_rtpm_monitor_status = TRUE;		
	}
	else
	{
		CSOT_ReUpload_RTPM();
		//g_rtpm_monitor_status = FALSE;		
		// Re-upload data from TCON to PMIC
		//COST_RTPM_Write_PMIC_Code_To_PMIC();
	}

}

void CSOT_RTPM_Upload_PMIC_Code_To_PMIC(void)
{
	//if(!g_csot_rtpm_proc_flg)
		//return;
	uint8_t l_i2c_path;

	l_i2c_path = g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl;
	CSOT_Decide_PMIC_I2C_Path(g_rw_mcu_top_0027h.bits.r_pmic_i2c_path);
	if(!CSOT_RTPM_Tcon_Upload_PMIC_Code_To_PMIC())
	{
		// Tcon set => CRC fail (Reg set)
		CSOT_RTPM_Set_Fw_Sta_Crc_Fail_Tcon_Pmic_Flash(1);
		// Set PMIC Fail (Reg set)
		CSOT_RTPM_Set_PMIC_Cfg_Pass(0);
	}
	else
	{
		// Set PMIC Pass (Reg set)
		CSOT_RTPM_Set_PMIC_Cfg_Pass(1);
	}
	g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = l_i2c_path;
}

static bool CSOT_Check_WP_Pin(void)
{
	if(CSOT_WP_PIN)
	    return 1;
	else
		return 0;
}

static bool CSOT_Check_Pmic_Cfg_Flg(void)
{
	return g_csot_pmic_pass_cfg_flg;
}

void CSOT_RTPM_Handler(void)
{

	//if(CSOT_WP_PIN)
	    //return;
	if(CSOT_Check_WP_Pin())
		return;

	if(!g_csot_pmic_pass_cfg_flg)
		return;
    //P1_0 = 1; 
	//****** for CSOT 3 in 1 test****************/
	/*if(!CSOT_RTPM_Check_Dl_En())
	    return;*/
	//*******************************************/
	CSOT_RTPM_Tcon_Monitor_PMIC_CRC();
//	if(g_vsync_flg)
		//{
		//mcu_timer_delay_us(6000);
		//CSOT_RTPM_Tcon_Monitor_PMIC_CRC();
        
		//****** for CSOT 3 in 1 test****************/
		 /*P1_0 = 0;  P1_1 = 0;  P1_2 = 0;
        g_hw_dbg_cfg_x_u.bits.bit4_0 = 11;
		g_hw_dbg_cfg_2_u.bits.r_p2p_dbgo_sel = 5;

		if(g_hw_hit_cfg_r_u.bits.sig_1)
            P1_0 = 1;
		else if(g_hw_hit_cfg_r_u.bits.sig_2)
            P1_1 = 1;
		else if(g_hw_hit_cfg_r_u.bits.sig_3)
            P1_2 = 1;*/
	    //*******************************************/
		
		//g_vsync_flg = 0;
			//}
	//P1_0 = 0; 
}
#endif

