/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: EK50208
		Platform: 8051
		Author: Eric Lee, 2021/03/19
		Description: Demura Transfer
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"
#include "reg_include.h"

#ifdef DEMURA_CHK_FUN
#include <stdio.h>
#include <string.h>
#include "tcon_misc.h"
#include "mcu_spi.h"

#ifdef CASCADE_DEMURA_FUN
#include "tcon_cascade.h"
#endif

// ******************************************************************* 
// Demura Truth Table Check
#define DEMURA_SDP                      g_rw_demura_load_0047h.bits.r_demura_sdp
#define BIST_CHK_EN                     g_rw_demura_load_0051h.bits.r_demura_bist_chk 
#define FITI_MODE_EN                    g_rw_demura_load_003Dh.bits.r_fiti_mode_en
#define DEMURA_DL_EN                    g_rw_demura_load_003Dh.bits.r_demura_dl_en
#define DEMURA_DL_FINISH                g_rw_dbg_mux_011Eh.bits.r_demura_dl_finish
#define DEMURA_CHKSUM_EN                g_rw_demura_load_0047h.bits.r_demura_chksum_en

static uint8_t storage_demura_en_sta;
static bool g_fw_chk_finish_flg;
bool dmr_flash_reload_flag;

#ifdef CASCADE_DEMURA_FUN
uint8_t xdata g_dmr_cascade_mstr_data_buf[4] = 0;
bool g_dmr_en_ready_flag = 0;
bool g_dmr_cascade_slv_waiting_rx_flag = 0;
bool g_dmr_cascade_slv_rcv_src_rdy_flag = 0;
bool g_dmr_cascade_slv_rcv_src_pass_flag = 0;

void demura_cascade_master_tx_handler(uint8_t dl_pass)
{
    uint8_t *buf_ptr = g_dmr_cascade_mstr_data_buf;
    if(g_master_flg == MASTER_TYPE)
    {
        if(g_cascade_tx_flg == 0)       
        {
            buf_ptr[0] = CASCADE_DATA_ID_MSTR_DEMURA_STATUS;          // demura cascade master id 
            buf_ptr[1] = (uint8_t)((dl_pass << 1) | 0x01);
            tcon_tx_cascade(buf_ptr);
        }
    }
}
void demura_cascade_slave_rx_handler(void)
{
    if(g_master_flg == MASTER_TYPE)
        return;
    if((g_rx_cscd.id != CASCADE_DATA_ID_MSTR_DEMURA_STATUS) && (g_master_flg == SLAVE_TYPE)
        && (g_rw_demura_load_0011h.bits.r_non_fiti_sel != 7))
        return;
    g_dmr_cascade_slv_rcv_src_rdy_flag = 1;

    if(g_rx_cscd.val[0] & 0x03 == 0x03)
    {
        g_dmr_cascade_slv_rcv_src_pass_flag = 1;
        if(g_dmr_cascade_slv_waiting_rx_flag)
            g_rw_daf_0003h.bits.r_dmr_en = 1;
    }
    else
    {
        g_dmr_cascade_slv_rcv_src_pass_flag = 0;
        g_rw_daf_0003h.bits.r_dmr_en = 0;
    }
    g_dmr_cascade_slv_waiting_rx_flag = 0;

    storage_demura_en_sta = g_rw_daf_0003h.bits.r_dmr_en;
}
#endif

void mcu_dmr_check_dl_csot_mode(void)
{
    uint8_t dmr_csot_chksm_addr[3] = 0;
    uint8_t dmr_csot_chksm_data[4] = 0;
    uint32_t chksm_data_tmp = 0;

    if(((g_rw_sys_00E3h.bits.r_cscd_en) && (g_master_flg == MASTER_TYPE)) ||    // Master action
       (!g_rw_sys_00E3h.bits.r_cscd_en))
    {
        dmr_csot_chksm_addr[0] = (uint8_t)(g_rw_demura_load_0062h_temp2);       // MSB
        dmr_csot_chksm_addr[1] = (uint8_t)(g_rw_demura_load_0061h_temp2);
        dmr_csot_chksm_addr[2] = (uint8_t)(g_rw_demura_load_0060h_temp2);       // LSB

        mcu_spi_acc_bus(1); 
        mcu_spi_flash_fast_read(dmr_csot_chksm_addr, &dmr_csot_chksm_data[0], 4, 1);
        mcu_spi_acc_bus(0);

        chksm_data_tmp = (dmr_csot_chksm_data[3] << 24) | (dmr_csot_chksm_data[2] << 16) | (dmr_csot_chksm_data[1] << 8) | dmr_csot_chksm_data[0];
        
        if((chksm_data_tmp == 0x00000000) || (chksm_data_tmp == 0xFFFFFFFF))
        {
            if(g_rw_sys_00E3h.bits.r_cscd_en)
                demura_cascade_master_tx_handler(1);                            // master send pass message
            g_rw_daf_0003h.bits.r_dmr_en = 1;
        }    
        else
        {
            if(g_rw_sys_00E3h.bits.r_cscd_en)
                demura_cascade_master_tx_handler(0);                            // master send fail message
            g_rw_daf_0003h.bits.r_dmr_en = 0;
        }
    }
    else if((g_rw_sys_00E3h.bits.r_cscd_en) && (g_master_flg == SLAVE_TYPE))    // Slave action
    {
        if(g_dmr_cascade_slv_rcv_src_rdy_flag)
        {
            g_rw_daf_0003h.bits.r_dmr_en = (g_dmr_cascade_slv_rcv_src_pass_flag) ? 1 : 0;
        }
        else
            g_dmr_cascade_slv_waiting_rx_flag = 1;
    }
    storage_demura_en_sta = g_rw_daf_0003h.bits.r_dmr_en;
}
// *******************************************************************
// Demura Bist Set
static void mcu_dmr_dl_reg_force_demura_bist_set(void)
{
    if((BIST_CHK_EN)&&(g_rw_dbg_mux_011Eh.bits.r_demura_header_dl_fail) && \
        (!g_rw_dbg_mux_011Eh.bits.r_demura_header_ff_chk) && storage_demura_en_sta)
    {
        g_rw_demura_load_0051h.bits.r_force_dmura_bist = 1;
    }
}

// *******************************************************************
// Demura Download Enable Set
static void mcu_dmr_dl_demura_enable_set(void)
{
    // FW Demura transfer check
    if(DEMURA_DL_EN && (!FITI_MODE_EN) /*&& (g_r_smooth_855_reg_cfg.bits.smooth_en)*/)
    {
        if((!g_rw_dbg_mux_011Eh.bits.r_demura_header_dl_fail) && (!g_rw_dbg_mux_011Eh.bits.r_demura_lut_dl_fail))
        {
            if(g_rw_demura_load_0011h.bits.r_non_fiti_sel == 7)         // CSOT demura mode
                mcu_dmr_check_dl_csot_mode();
            else
            g_rw_daf_0003h.bits.r_dmr_en = 1;
        }
        else
            g_rw_daf_0003h.bits.r_dmr_en = 0;
    }else
    {
        if(((DEMURA_SDP)&(!g_rw_dbg_mux_011Eh.bits.r_demura_header_dl_fail)&(!g_rw_dbg_mux_011Eh.bits.r_demura_header_ff_chk)|(!DEMURA_SDP))& \
            (storage_demura_en_sta)&((!g_rw_dbg_mux_011Eh.bits.r_demura_no_flash) & \
            ((DEMURA_CHKSUM_EN)&(!g_rw_dbg_mux_011Eh.bits.r_demura_reg_dl_fail)&(!g_rw_dbg_mux_011Eh.bits.r_demura_lut_dl_fail)&(DEMURA_DL_FINISH)| \
            (!DEMURA_CHKSUM_EN)&(DEMURA_DL_FINISH))))
        {
            g_rw_daf_0003h.bits.r_dmr_en = 1;
        }else{
            g_rw_daf_0003h.bits.r_dmr_en = 0;
        }
    }
    storage_demura_en_sta = g_rw_daf_0003h.bits.r_dmr_en;
    
}

/*void demrua_chk_handler(void)
{
    if(DEMURA_DL_EN == 0)
        return;
    // Init flg and store demura_en value
    dmr_flash_reload_flag = 0;
    storage_demura_en_sta = g_rw_daf_0003h.bits.r_dmr_en;

    tcon_wait_dmr_finish();

    mcu_dmr_dl_reg_force_demura_bist_set();

    mcu_dmr_dl_demura_enable_set();

    g_fw_chk_finish_flg = 1;
}*/

void demrua_chk_handler(void)
{
    if(DEMURA_DL_EN == 0)
        return;

    //if((!g_rw_dbg_mux_011Eh.bits.r_demura_dl_finish) || (g_fw_chk_finish_flg))
    if((!g_rw_dbg_mux_011Eh.bits.r_demura_dl_finish) || (g_rw_demura_load_0057h.bits.r_fw_chk_finish_en))
        return;
    
    // Init flg and store demura_en value
    dmr_flash_reload_flag = 0;
    storage_demura_en_sta = g_rw_daf_0003h.bits.r_dmr_en;

    //tcon_wait_dmr_finish();

    mcu_dmr_dl_reg_force_demura_bist_set();

    mcu_dmr_dl_demura_enable_set();

    g_fw_chk_finish_flg = 1;
}

void demrua_vsync_chk_reload_handler(void)
{
    // Set r_fw_chk_finish_en when first enter vsync
    if(g_fw_chk_finish_flg)
    {
        g_rw_demura_load_0057h.bits.r_fw_chk_finish_en = 1;
        g_fw_chk_finish_flg = 0;
    }
    // check reload dmr transfer or not
    if(dmr_flash_reload_flag)
    {
#ifdef DEMURA_TRANS_FUN
        tcon_lut_transfer_handler();
#endif
        demrua_chk_handler();
    }
}

#endif

#ifdef DEMURA_TRANS_FUN
#include <stdio.h>
#include <string.h>
#include "mcu_spi.h"
#include "tcon_lut_transfer.h"
#include "demura.h"
#include "demura_load.h"
#include "tcon_misc.h"

#ifdef TANSFER_INX
    #include "tcon_lut_transfer_inx.h"
#endif
#ifdef TANSFER_NVT
    #include "tcon_lut_transfer_nvt.h"   
#endif 
#ifdef TANSFER_HIX
    #include "tcon_lut_transfer_hix.h"   
#endif 
#ifdef TANSFER_SEC
    #include "tcon_lut_transfer_sec.h"   
#endif 
#ifdef VD_TRANS_FUN
    #include "tcon_lut_transfer_vd.h"	
#endif

#define HW_SPI_CKS_CAL_ON               (SPISET |= 0x20)       // SPI chksum calculation on
#define HW_SPI_CKS_CAL_OFF              (SPISET &= ~0x20)      // SPI chksum calculation off


bool g_hw_trans_fw_cks_flg;
bool g_lut_compress_flg;

uint8_t xdata g_set_plane_Num;
uint8_t xdata g_transfer_mode;
uint8_t xdata g_user_mode_flg;

uint16_t xdata g_abs_lut_dt_max_lx[8];
uint16_t xdata g_demura_cal_cks ;
uint16_t xdata g_org_cks;
uint16_t xdata g_r_hw_align_len;

volatile uint8_t xdata g_dmr_trans_cfg_buf[722]     _at_    0xE800;  // inx:155*12 = 1860, hix:722, 
volatile uint8_t xdata g_dmr_fw_state               _at_    0xF8D0;

void tcon_lut_transfer_set_spi_shift(uint8_t bytes_num , uint8_t bit_shift , uint8_t shift_mode)
{
    g_rw_sys_data_0F_u.byte = bytes_num;
    g_rw_sys_data_0D_u.bits.spi_dma_shift = bit_shift;
    g_rw_sys_data_0D_u.bits.format_shift_mode = shift_mode;
}

static void Fiti_Cks_Init(void)
{
    g_demura_cal_cks = 0;
    g_org_cks = 0;
}

uint8_t get_transfer_plane_num(void)
{
    return g_demura_reg_0002h.bits.r_plane_num;
}

uint16_t get_line_num(void)
{
    return ((g_demura_load_reg_0020h.bits.r_ver_rd_num_8 << 8) | g_demura_load_reg_001Fh.byte);
    // return (g_r_demura_trans_ver_num.bits.bit8 << 8 | g_r_demura_trans_ver_num_1.byte);
}

void Lut_Fill_Hw_Reg_En(void)
{
    g_demura_load_reg_0055h.bits.r_demura_fw_lut_en = 1;
    g_demura_load_reg_0059h.bits.r_fw_lut_plane = 0;
}

void Lut_Fill_Hw_Reg_Dis(void)
{
    g_demura_load_reg_0055h.bits.r_demura_fw_lut_en = 0;
}

void Lut_Set_Plane_Num(uint8_t val)
{
    g_demura_load_reg_0059h.bits.r_fw_lut_plane = val;
}

void Lut_Transfer_Fiti_Cal_Cks(uint8_t val)
{
    P2_1 = 1;
    g_demura_load_reg_0056h.bits.r_demura_fw_lut_data = val;
    P2_1 = 0;
    g_demura_cal_cks += val;
}

bool tcon_lut_transfer_chk_hw_stop(void)
{
    while(1)
    {
        // If demura hw buffer is not full (trans not stop) => return true
        if(!g_demura_load_reg_005Ah.bits.hw_dmr_trans_stop)
            return TRUE;
        if(mcu_timer_dl_timeout())
            return FALSE;
    }
}

#ifdef FLASH_CODE
void tcon_lut_transfer_wait_fw_state(void)
{
    while(1)
    {
        if(g_dmr_fw_state)
            return;
        if(mcu_timer_dl_timeout())
            return;
    }
}

void tcon_lut_transfer_handler(void)
{
   if(!g_demura_load_reg_0050h.bits.r_mcu_demura_trans_en)
       return;
   
   // Wait for HW Demura state change to FW trans state
   mcu_timer_wait_dl_init();
   tcon_lut_transfer_wait_fw_state();

    g_rw_daf_0003h.bits.r_dmr_clk_en = 1;
    // g_r_dmr_dl_824_reg_cfg.bits.clk_en = 1;
    
    mcu_spi_sw_path(SPI_DMR_PATH); // demura flash path    
    Fiti_Cks_Init();
    
    mcu_spi_set_clk(g_rw_mcu_top_0032h.bits.r_c_board_spi_freq_sel);
    
    HW_SPI_CKS_CAL_ON;

    g_transfer_mode = g_demura_load_reg_0011h.bits.r_non_fiti_sel;
    g_hw_trans_fw_cks_flg = 1;
    if(g_transfer_mode == INX_FORMAT)
    {
        tcon_lut_transfer_set_spi_shift(11,2,0);    // shift : 2 , Mode : 0
        tcon_lut_transfer_InxtoFiti();
        tcon_lut_transfer_set_spi_shift(11,0,0);    // Shift OFF
    }
    else if(g_transfer_mode == NVT_FORMAT)
    {
        // tcon_lut_transfer_set_spi_shift(1,4,1);     // shift : 4 , Mode : 1
        tcon_lut_transfer_NVTtoFiti();
        // tcon_lut_transfer_set_spi_shift(1,0,0);     // Shift OFF
    }
    else if(g_transfer_mode == HIX_FORMAT)
    {
        // tcon_lut_transfer_set_spi_shift(1,4,0);     // shift : 4 , Mode : 0
        tcon_lut_transfer_HIXtoFiti();
        // tcon_lut_transfer_set_spi_shift(1,0,0);     // Shift OFF
    }
    else if(g_transfer_mode == SEC_FORMAT)
    {
        tcon_lut_transfer_SECtoFiti();
    }else if(g_transfer_mode == VD_FORMAT)
    {
        tcon_lut_transfer_VDtoFiti();
    }

    HW_SPI_CKS_CAL_OFF;

    Lut_Fill_Hw_Reg_Dis();
    mcu_spi_sw_path(SPI_CFG_PATH);
    mcu_spi_set_clk(g_rw_mcu_top_0032h.bits.r_c_board_spi_freq_sel);

    g_demura_load_reg_0050h.bits.r_mcu_demura_trans_finish = 1;
}

void demura_id_floating_set(void)
{
    g_rw_sys_0090h.bits.r_spi_sdi_mux_pd = 0;
}

#endif
#endif     // for DEMURA_TRANS_FUN definition
