/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: 
		Platform: 8051
		Author: Eric Lee, 2021/03/22
		Description: Mcu Download 
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"
#include "mcu_global_vars.h"
#include "mcu_spi.h"
#include "mcu_i2c.h"
#include "mcu_timer.h"
#include "mcu_uart.h"
#include "CSOT_I2C_SLV.h"
#include "mcu_misc.h"

#ifdef CSOT_FUN
    #include "csot_global_include.h"
#endif
#include "mcu_reg51.h"
#include "reg_include.h"

#define DL_PAUSE                (0x80)
#define READ_HW_DL_PAUSE        (FITI_REG_SET & DL_PAUSE)
#define CLEAR_HW_DL_PAUSE       (FITI_REG_SET |= DL_PAUSE)

#define DL_ENTRY_LEN            10
#define MEM_BLANK_SZ            (0xB)       // 2K

#define CHECK_FAIL              (0)
#define CHECK_PASS              (1)

#define DL_RETRY_CNT            (3)

#define DL_SPI_PATH             (1)
#define DL_I2C_PATH             (0)

#define HW_CKS_CAL_ON           DL_FINISH |= 0x10
#define HW_CKS_CAL_OFF          DL_FINISH &= ~0x10
#define HW_CHKS_RESET           DL_FINISH |= 0x40
#define MCU_DL_LAST_FINISH      DL_FINISH |= 0x08

#define DCG_IDX         0
#define CSOT_HDR_IDX    1
#define CSOT_GMA_IDX    2
#define CSOT_BANK_A_IDX 3
#define CSOT_BANK_B_IDX 4
#define PDF_IDX         6

extern unsigned char code eeprom_entry_data_cks_adr; // C: 0x2E00
extern unsigned char code eeprom_entry_data_cks_len; // c: 0x2E02
extern unsigned char code eeprom_entry_hdr_var;     // C : 0x2E03


extern void MERGE_ADDR(void);

uint32_t xdata g_mcu_entry_addr;
static uint16_t xdata g_mcu_slv_addr;
static uint8_t xdata g_mcu_dl_i2c_adr;
static uint8_t xdata g_check_flash_empty_buf[10] = {0,0,0,0,0,0,0,0,0,0};

//uint8_t xdata g_mcu_old_ver;         // use for EK50508AD

uint8_t xdata g_i2c_hw_cks_res_u8;
uint16_t xdata g_i2c_fw_cks_res_u16;
uint16_t xdata g_i2c_read_len;

uint8_t xdata g_hdr_ofst = 0;
uint8_t xdata g_cks_ofst = 0;
uint8_t xdata g_dl_entry_cnt = 0;

uint8_t xdata g_2K_entry_num = 0;
//uint8_t xdata g_csot_retry_dl_cnt;
static void mcu_dl_csot_save_dl_addr(uint8_t idx,uint8_t l_reg_len);
static void mcu_dl_csot_pmic_handler(uint8_t l_reg_len);
static void mcu_dl_csot_dgc_handler(uint8_t l_reg_len);

static bool mcu_dl_check(void)
{
    if(READ_HW_DL_PAUSE)
        return TRUE;
    return FALSE;    
}

static void mcu_dl_done(void)
{
    CLEAR_HW_DL_PAUSE;
}

static uint8_t mcu_dl_hrd_chk(void)
{
    uint16_t val = 0;
    uint8_t val_1 = 0;
    uint8_t cal_cnt;
    uint8_t *ptr = &g_mcu_entry_cfg_0_u;

    for(cal_cnt = 0; cal_cnt < (DL_ENTRY_LEN-1);cal_cnt++)
    {
        val_1 = * ( volatile char xdata *) (ptr + cal_cnt);
        val += val_1;
        if(val >= 0x100)
           val = (val & 0xFF) + 1; 

        DBG_LOG("hrd_cks = 0x%x\n",val);
    }
    return val;    
}

static void mcu_dl_get_sfr_entry_addr(void)
{
	//MERGE_ADDR();
    g_mcu_entry_addr = MCU_ROM_OFST_23_16;
    g_mcu_entry_addr <<= 8;
    g_mcu_entry_addr += MCU_ROM_OFST_15_8;
    g_mcu_entry_addr <<= 8;
    g_mcu_entry_addr += MCU_ROM_OFST_7_0;   

    DBG_LOG("dl_entry_adr = 0x%lx\n",g_mcu_entry_addr);
}

static void mcu_dl_i2c_access_mcu_entry_addr(uint8_t* ptr,uint8_t l_reg_length)
{
    ptr[0] = g_mcu_dl_i2c_adr;  // device
    if(l_reg_length == 1)
    {
        ptr[1] = g_mcu_entry_addr & 0xff;
    }
    else
    {
    ptr[1] = (g_mcu_entry_addr >> 8) & 0xff;
    ptr[2] = g_mcu_entry_addr & 0xff;
    }
}

static void mcu_dl_access_rom_addr(uint8_t* ptr, bool path,uint8_t l_reg_length)
{
    uint16_t l_reg_adr;
    uint8_t l_i2c_adr,l_bank;
    if(path == DL_I2C_PATH)
    {
        //ptr[0] = g_mcu_dl_i2c_adr;
        if(l_reg_length == 1)
        {
            l_reg_adr = g_mcu_entry_cfg_0_u.bits.rom_addr_15_8;
            l_reg_adr <<= 8;
            l_reg_adr += g_mcu_entry_cfg_0_u.bits.rom_addr_7_0;
            l_i2c_adr = (HW_I2C_ADR_AND_CRC_CKS & 0x70);
            l_bank = (l_reg_adr / 256);
            g_mcu_dl_i2c_adr = (l_i2c_adr + l_bank);

            ptr[1] = g_mcu_entry_cfg_0_u.bits.rom_addr_7_0;
        }
        else
        {
        ptr[1] = g_mcu_entry_cfg_0_u.bits.rom_addr_15_8;
        ptr[2] = g_mcu_entry_cfg_0_u.bits.rom_addr_7_0;
    }
        ptr[0] = g_mcu_dl_i2c_adr;
    }
    else
    {
        ptr[0] = g_mcu_entry_cfg_1_u.bits.rom_addr_23_16;
        ptr[1] = g_mcu_entry_cfg_0_u.bits.rom_addr_15_8;
        ptr[2] = g_mcu_entry_cfg_0_u.bits.rom_addr_7_0;    
    }
}

static void mcu_dl_cal_slv_adr(void)
{
    uint16_t l_bank_ofset;

    l_bank_ofset =  g_mcu_entry_cfg_2_u.bits.bank_addr_10_8 << 8 | g_mcu_entry_cfg_1_u.bits.bank_addr_7_0;
    g_mcu_slv_addr = (uint16_t)(g_mcu_entry_cfg_2_u.bits.bank_sel_4_0 << MEM_BLANK_SZ) + l_bank_ofset;      // MEM_BLANK_SZ : 2K
}

static uint16_t mcu_dl_cal_dl_len(void)
{
    return (g_mcu_entry_cfg_3_u.bits.len_15_8 << 8 | g_mcu_entry_cfg_2_u.bits.len_7_0);
}

static uint8_t mcu_dl_hrd_cks(void)
{
    if(g_rw_mcu_top_0003h.bits.reserved2 == 1)
        return 1;
    if(mcu_dl_hrd_chk() + g_mcu_entry_cfg_4_u.bits.hdr_checksum != 0xFF)
    {
        DBG_LOG("hrd_cks_1 = 0x%x\n",g_mcu_entry_cfg_4_u.bits.hdr_checksum);
        return 0;
    }
    return 1;
}  

static uint8_t mcu_dl_cmp_data_cks(uint16_t val)
{
    uint8_t l_msb;
    uint8_t l_lsb;

    l_msb = val >> 8;
    l_lsb = val & 0xFF;
    // g_test_dl_val = g_mcu_entry_cfg_3_u.bits.checksum;
    if((l_msb + l_lsb + g_mcu_entry_cfg_3_u.bits.checksum) != 0xFF)
    {
        return 0;
    }
    return 1;    
    //if((g_rw_sys_data_0D_u.bits.hw_cks_msb + g_r_sys_data_0E_u.byte + g_mcu_entry_cfg_3_u.bits.checksum) != 0xFF)
        //return 0;
    //return 1;    
}

static void mcu_dl_slv_finish(void)
{
    if(!g_mcu_entry_cfg_4_u.bits.finfail_en)
        return;
    switch(g_mcu_entry_cfg_4_u.bits.finfail_idx_2_0)
    {
        case 0:     // slv0 finish
            DL_SLV_0_FINISH = 0;
        break;

        case 1:     // slv1 finish
            DL_SLV_1_FINISH = 0;
        break;

        case 2:     // slv2 finish
            DL_SLV_2_FINISH = 0;
        break;

        case 3:     // slv3 finish
            DL_SLV_3_FINISH = 0;
        break;
    }

    if(g_r_sys_data_20_u.bits.hw_init_finish)
        MCU_DL_LAST_FINISH;
    
}

void mcu_dl_i2c_hw_cal_cks_en(void)
{   
    HW_CHKS_RESET;
    HW_CKS_CAL_ON;
}

void mcu_dl_i2c_cal_data_cks(uint8_t val)
{
    g_i2c_fw_cks_res_u16 += val;
  
}

void mcu_dl_onebyte_i2cadr(uint8_t l_reg_length)
{
    uint8_t l_bank = 0;
    uint8_t l_i2c_adr;

    if(l_reg_length == 1)
    {
        l_i2c_adr = (HW_I2C_ADR_AND_CRC_CKS & 0x70);
        l_bank = (g_mcu_entry_addr / 256);
        g_mcu_dl_i2c_adr = (l_i2c_adr + l_bank);
    }
}

//uint8_t xdata g_i2c_mode_test = 0;
// step 0 : initial hw config
uint8_t l_hw_config(void)
{
    //volatile uint8_t l_length;

    if(g_i2c0_spi1_sel == 0)
    {
        //mcu_i2c1_init();
        g_i2c_fw_cks_res_u16 = 0;
        g_rw_mcu_pmic_bankA_crc.uint = 0;
        g_rw_mcu_pmic_bankB_crc.uint = 0;
        //if(I2C_REG_MODE == 1)
        //g_mcu_dl_i2c_adr = HW_I2C_ADR_AND_CRC_CKS & 0x70;
        
		//g_i2c_mode_test = I2C_REG_MODE & 0x01;
        if((I2C_REG_MODE & 0x01) == 0)
        {
            g_mcu_dl_i2c_adr = HW_I2C_ADR_AND_CRC_CKS & 0x70;
            return 1;
        }
        else 
        {
            g_mcu_dl_i2c_adr = HW_I2C_ADR_AND_CRC_CKS & 0x7F;
            return 2;
        }
    
        //return l_length;
    }

    //else if(g_spi_i2c_path == 1)
    else
    {
        return 0;
    }

    //else return 0xFF;    

}

// step 2 : Read Mcu Entry Cfg
void mcu_read_cfg(uint8_t l_reg_length)
{
#ifdef FLASH_CODE
    bool l_cs;  
#endif
	
    uint8_t l_i2c_w_buf[3] = {0,0,0};
    //uint8_t l_bank = 0;
    //uint8_t l_i2c_adr;
    if(g_i2c0_spi1_sel == 0)
    {
        g_i2c_fw_cks_res_u16 = 0;
        mcu_dl_onebyte_i2cadr(l_reg_length);
        /*if(l_reg_length == 1)
        {
            l_i2c_adr = (HW_I2C_ADR_AND_CRC_CKS & 0x70);
            l_bank = (g_mcu_entry_addr / 256);
            g_mcu_dl_i2c_adr = l_i2c_adr + l_bank;
        }*/
        mcu_dl_i2c_access_mcu_entry_addr(&l_i2c_w_buf[0],l_reg_length);
        //mcu_timer_delay_us(6000);
        mcu_i2c_mst_read(&l_i2c_w_buf[0],l_reg_length,(uint8_t *)&g_mcu_entry_cfg_0_u,DL_ENTRY_LEN,0, g_i2c_wr_dly_time,0);
    }
#ifdef FLASH_CODE
    //else if(g_spi_i2c_path == 1)
    else
    {
        l_cs = FLASH_CS_CBOARD;
        mcu_spi_flash_fast_read((uint8_t *)&g_mcu_entry_addr+1,(uint8_t *)&g_mcu_entry_cfg_0_u,DL_ENTRY_LEN , l_cs);
    }
#endif
}

void mcu_cfg_dl_info(uint8_t l_reg_length)
{
    if(g_i2c0_spi1_sel == 0)       // i2c 
    {
        mcu_dl_access_rom_addr(&g_rom_entry_addr[0],DL_I2C_PATH,l_reg_length);
        mcu_dl_cal_slv_adr();
        //mcu_timer_delay_us(6000);   
        g_i2c_read_len = mcu_dl_cal_dl_len();
    }
    else
    {
        mcu_dl_access_rom_addr(&g_rom_entry_addr[0],DL_SPI_PATH,l_reg_length);
        mcu_dl_cal_slv_adr();
    }
}

// step 4 : update slv data
void mcu_update_data(uint8_t l_reg_length)
{
    if(g_i2c0_spi1_sel == 0)       // i2c 
    {
        /*if(g_mcu_entry_cfg_4_u.bits.vcom_en)
        {
           mcu_i2c_mst_read(&g_rom_entry_addr[0],l_reg_length,&g_i2c_rd_buf[0],g_i2c_read_len,1, g_i2c_wr_dly_time);     
        }
        else
        {
            mcu_i2c_mst_read(&g_rom_entry_addr[0],l_reg_length,(uint8_t *)g_mcu_slv_addr,g_i2c_read_len,1, g_i2c_wr_dly_time); 
        }*/
        mcu_i2c_mst_read(&g_rom_entry_addr[0],l_reg_length,(uint8_t *)g_mcu_slv_addr,g_i2c_read_len,1, g_i2c_wr_dly_time,0);
    }
#ifdef FLASH_CODE
    else    // spi
    {
        mcu_spi_flash_fast_read(&g_rom_entry_addr[0],(uint8_t *)g_mcu_slv_addr,(g_mcu_entry_cfg_3_u.bits.len_15_8 << 8 | g_mcu_entry_cfg_2_u.bits.len_7_0) , FLASH_CS_CBOARD);
    }
#endif
}

// step 5 : check data checksum
#if 0
uint8_t CSOT_RTPM_Cal_Dl_Crc(void)
{
    g_csot_retry_dl_cnt++;
    return 0x00;
    #if 0
    if(CSOT_RTPM_Cal_Dl_Crc_Pass())     // check HW CRC
    {
        //CSOT_RTPM_Set_PMIC_GPIO(0);
        CSOT_RTPM_Storage_Crc();  
        CSOT_RTPM_Upload_PMIC_Code_To_PMIC();
        CSOT_RTPM_Clr_Pmic_En();
        mcu_dl_slv_finish();
        g_csot_retry_dl_cnt = 0;
        P0_2 = 0;
        return 0x01;
    }
    else if(g_csot_retry_dl_cnt == DL_RETRY_CNT)
    {
        CSOT_RTPM_Set_Fw_Sta_Crc_Fail_Tcon_Pmic_Flash(1);
        g_csot_retry_dl_cnt = 0;
        return 0x00;
    }
    else 
        return 0x00;
    #endif
}
#endif
uint8_t mcu_data_chk(void)
{
    //uint8_t l_CSOT_RTPM_Crc_Det = 0x00;
    //uint8_t l_RetVal;
    uint16_t l_mcu_dl_i2c_data_cks;
    if(g_rw_mcu_top_0003h.bits.reserved2 == 1)
       return 1;
    if(g_i2c0_spi1_sel == 0)
    {
        l_mcu_dl_i2c_data_cks = (g_i2c_fw_cks_res_u16 >> 8)  + (g_i2c_fw_cks_res_u16 & 0xFF);
    }
    else
    {
		l_mcu_dl_i2c_data_cks = (g_rw_sys_data_0D_u.bits.hw_cks_msb >> 8) + g_r_sys_data_0E_u.byte;
        //l_mcu_dl_i2c_data_cks = (g_rw_sys_data_0D_u.bits.hw_cks_msb >> 8) + g_r_sys_data_0E_u.byte;
    }
    return mcu_dl_cmp_data_cks(l_mcu_dl_i2c_data_cks);
}
#if 0
void mcu_write_vcom_action(void)
{
    uint8_t l_mem_reg_len;
    uint8_t l_wr_len;
    uint8_t l_total_len;

    l_mem_reg_len = g_mcu_dl_vcom_cfg.setting;
    l_wr_len = g_mcu_dl_vcom_cfg.rw_len;
    l_total_len = g_i2c_read_len;
    while(l_total_len != 0)
    {
        l_total_len -= (4 + l_wr_len);
        if(g_mcu_dl_vcom_cfg.write_flg == 0)
        {
            mcu_i2c_mst_write(g_mcu_dl_vcom_cfg.slv_adr,&g_mcu_dl_vcom_cfg.reg_ofset[0],l_mem_reg_len,&g_mcu_dl_vcom_cfg.data_byte[0],l_wr_len, 0 , 20);
        }


    }
}
#endif
static uint8_t mcu_dl_vcom_len(void)
{
    return (g_mcu_dl_vcom_cfg.rw_len + 1);
}

static uint8_t mcu_dl_vcom_reg_mode(void)
{
    return g_mcu_dl_vcom_cfg.setting;
}

uint8_t xdata g_test1;
uint8_t xdata g_test2;
uint8_t xdata g_test3;
void mcu_read_entry_vcom(uint8_t reg_len)
{
    uint8_t l_rw_len;
    if(g_i2c0_spi1_sel == 0)       // i2c 
    {
        /*Read Vcom Header*/
			g_test1 = g_rom_entry_addr[0];
			g_test2 = g_rom_entry_addr[1];
			g_test3 = g_rom_entry_addr[2];
        mcu_i2c_mst_read(&g_rom_entry_addr[0],reg_len,(uint8_t *)&g_mcu_dl_vcom_cfg,4,1, g_i2c_wr_dly_time,0);
        l_rw_len = mcu_dl_vcom_len();
        //l_rw_len = (g_mcu_dl_vcom_cfg.rw_len + 1);
        /*Read Vcom Header + Data*/
        mcu_i2c_mst_read(&g_rom_entry_addr[0],reg_len,(uint8_t *)&g_mcu_dl_vcom_cfg,(l_rw_len+4),0, g_i2c_wr_dly_time,0);
    }
    #ifdef FLASH_CODE
    else                            // spi
    {
        mcu_spi_flash_fast_read(&g_rom_entry_addr[0],(uint8_t *)&g_mcu_dl_vcom_cfg,4 , FLASH_CS_CBOARD);
        l_rw_len = g_mcu_dl_vcom_cfg.rw_len + 1;
        mcu_spi_flash_fast_read(&g_rom_entry_addr[0],(uint8_t *)&g_mcu_dl_vcom_cfg,l_rw_len , FLASH_CS_CBOARD);
    }
    #endif
}

void mcu_read_entry_vcom_offset(void)
{
    uint32_t adr;
    if(g_i2c0_spi1_sel == 0)       // i2c
    {
        VAR_MERGER_16(adr,g_rom_entry_addr[1]);
        adr += (4+g_mcu_dl_vcom_cfg.rw_len + 1);
        g_rom_entry_addr[1] = (adr >> 8) & 0xff;
        g_rom_entry_addr[2] = adr & 0xff;
    }
    #ifdef FLASH_CODE
    else
    {
        VAR_MERGER_32(adr,g_rom_entry_addr[1]);
        adr >>= 8;
        adr += (4+g_mcu_dl_vcom_cfg.rw_len + 1);
        g_rom_entry_addr[0] = (adr >> 16) & 0xff;
        g_rom_entry_addr[1] = (adr >> 8) & 0xff;
        g_rom_entry_addr[2] = adr & 0xff;
    }
    #endif
}

bool mcu_compare_vcom_data(void)
{
    uint8_t i;
    uint8_t l_rw_len;
    //l_rw_len = g_mcu_dl_vcom_cfg.rw_len + 1;
    l_rw_len = mcu_dl_vcom_len();
    for(i = 0;i < l_rw_len ; i++)
    {
        if(g_mcu_dl_vcom_cfg.read_byte[i] != g_mcu_dl_vcom_cfg.write_byte[i])
        {
            return FALSE;
        }
    }
    return TRUE;
}

bool mcu_write_vcom(void)
{
    uint8_t l_mem_reg_len;
    uint8_t l_rw_len;

    //l_rw_len = g_mcu_dl_vcom_cfg.rw_len + 1;
    l_rw_len = mcu_dl_vcom_len();
    l_mem_reg_len = mcu_dl_vcom_reg_mode();
    return mcu_i2c_mst_write(g_mcu_dl_vcom_cfg.slv_adr,&g_mcu_dl_vcom_cfg.reg_ofset[0],l_mem_reg_len,&g_mcu_dl_vcom_cfg.write_byte[0],l_rw_len, 0 , 20);
}

bool mcu_read_vcom(void)
{
    uint8_t l_mem_reg_len;
    uint8_t l_rw_len;

    //l_rw_len = g_mcu_dl_vcom_cfg.rw_len + 1;
    l_rw_len = mcu_dl_vcom_len();
    l_mem_reg_len = mcu_dl_vcom_reg_mode();

    g_mcu_dl_vcom_cfg.read_cfg[0] = g_mcu_dl_vcom_cfg.slv_adr;
    g_mcu_dl_vcom_cfg.read_cfg[1] = g_mcu_dl_vcom_cfg.reg_ofset[0];
    g_mcu_dl_vcom_cfg.read_cfg[2] = g_mcu_dl_vcom_cfg.reg_ofset[1];
    return mcu_i2c_mst_read(&g_mcu_dl_vcom_cfg.read_cfg[0],l_mem_reg_len,&g_mcu_dl_vcom_cfg.read_byte[0],l_rw_len,0,g_i2c_wr_dly_time,0);
}

void mcu_write_vcom_handler(uint8_t reg_len)
{
    bool l_pass_flg = 1;
    uint8_t l_total_len;
    uint8_t l_rw_len;
    uint8_t l_retry;
    //uint32_t adr;

    l_total_len = g_i2c_read_len;

    while(l_total_len!= 0)
    {
        for(l_retry = 0 ; l_retry < RETRY_TIMES ; l_retry ++)
        {
            mcu_read_entry_vcom(reg_len);
            l_rw_len = mcu_dl_vcom_len();
            // mcu write vcom
            if(mcu_write_vcom() == I2C_FAIL)
                continue;
            if(g_mcu_dl_vcom_cfg.write_flg == 1) // check
            {
                mcu_timer_wait_us(g_i2c_wr_dly_time);
                if(mcu_read_vcom() == I2C_FAIL)
                  continue;  
                l_pass_flg = mcu_compare_vcom_data();
            }
            if(l_pass_flg == 1)
                break;    
        }
        l_pass_flg = 1;
        l_total_len -= (4+l_rw_len);
        mcu_read_entry_vcom_offset();
    }
}

void mcu_dl_pmic_en_handler(uint8_t reg_len)
{
    uint8_t idx;
    uint32_t adr;
    idx = g_mcu_entry_cfg_4_u.bits.finfail_idx_2_0;
	//#ifdef FLASH_CODE
    mcu_dl_csot_save_dl_addr(idx,reg_len);      
//#endif	
    switch(idx)
    {
        case DCG_IDX:
            mcu_dl_csot_dgc_handler(reg_len);
        break;

        case CSOT_HDR_IDX:
        case CSOT_GMA_IDX:
        case CSOT_BANK_A_IDX:
        case CSOT_BANK_B_IDX:
            if(g_mcu_entry_cfg_4_u.bits.pmic_action)                // pmic, gma dl
                mcu_dl_csot_pmic_handler(reg_len);
        break;

        case PDF_IDX:
        adr = g_rom_entry_addr[0];
        adr <<= 8;
        adr += g_rom_entry_addr[1];
        adr <<= 8;
        adr += g_rom_entry_addr[2];
        if(PDF_DL_PIN == 1)
        {
            adr += 450;
            g_rom_entry_addr[0] = (adr >> 16) & 0xff;  
            g_rom_entry_addr[1] = (adr >> 8) & 0xff;
            g_rom_entry_addr[2] = adr & 0xff;
        }

        //update slv data
        mcu_update_data(reg_len);
        break;
         
    }
}

uint8_t mcu_dl_check_pmic_result(void)
{
    //uint8_t l_entry_ofst;

    //l_entry_ofst = g_mcu_entry_cfg_2_u.bits.bank_sel_4_0;

    if(g_mcu_entry_cfg_4_u.bits.pmic_action)
    {
        if(CSOT_RTPM_Ret_Fw_Sta_Crc_Fail_Tcon_Pmic_Flash() == 0) // dl pass
        {
            return 1;
        }
        return 2;
    }

    return 0;
}

void mcu_dl_proc_handler(void)
{
    uint8_t l_entry_cnt;
    uint8_t l_reg_len;
    uint8_t l_retry_cnt;
    //uint8_t l_one_byte;
    uint8_t l_pmic_res;
    //g_csot_retry_dl_cnt = 0;
    // step 0 : initial hw config
    l_reg_len = l_hw_config();

    if(g_i2c0_spi1_sel == 0) 
        g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = g_cfg_rom_sel_path;
    if(l_reg_len == 1)
    {
        //l_one_byte = *(volatile uint8_t code *)(&eeprom_entry_data_cks_adr);
        g_mcu_dl_i2c_adr &= 0xF0;
    }
    // step 1 : Read SFR MCU Entry Data
    mcu_dl_get_sfr_entry_addr();

    for(l_entry_cnt = 0 ; l_entry_cnt < MCU_ENTRY_CNT ; l_entry_cnt++ , g_mcu_entry_addr += DL_ENTRY_LEN)
    {
        // Retry 
        for(l_retry_cnt = 0 ; l_retry_cnt < DL_RETRY_CNT ; l_retry_cnt++)
        {
            // step 2 : Read Mcu Entry Cfg
            mcu_read_cfg(l_reg_len);

            //if(g_rw_mcu_top_0003h.bits.reserved2 == 0)
            //{
            // step 3 : cal hdr checksum
            if(mcu_dl_hrd_cks() == CHECK_FAIL) 
                continue;
            //}

            mcu_cfg_dl_info(l_reg_len);
        // ---------  pmic download --------//
            //#ifdef FLASH_CODE
            if(g_mcu_entry_cfg_4_u.bits.rtpm_en)
            {
                mcu_dl_pmic_en_handler(l_reg_len);
                #if 1
                l_pmic_res = mcu_dl_check_pmic_result(); 
                if(l_pmic_res == 1) // pass
                {
                    l_entry_cnt += g_mcu_entry_cfg_2_u.bits.bank_sel_4_0;
                    g_mcu_entry_addr += (DL_ENTRY_LEN * g_mcu_entry_cfg_2_u.bits.bank_sel_4_0);
                }
                #endif
                #if 1
                else if(l_pmic_res == 2) // fail
                {
                    if(g_mcu_entry_cfg_2_u.bits.bank_sel_4_0 > 0)
                    {
                        CSOT_RTPM_Set_Fw_Sta_Crc_Fail_Tcon_Pmic_Flash(0); // clear fail status
                    }
                }
                #endif
                    break;
                }
        // ---------  pmic download end--------// 
                else
                {
            //#endif
                // step 4 : update slv data
            // ---------  vcom download --------//
                if(g_mcu_entry_cfg_4_u.bits.vcom_en)
                {
                    mcu_write_vcom_handler(l_reg_len);
                break;
            }
            // ---------  vcom download end--------//
            else
            {
                mcu_update_data(l_reg_len);
                }

                // step 5 : check data checksum
                if(mcu_data_chk() == 0x01) 
                {
                    mcu_dl_slv_finish();
                    break;
                }
            }
        }
    }

    //if(g_i2c0_spi1_sel == 0)
        g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;   // slave connect to i2c sec path

    mcu_dl_done();
}


//#ifdef FLASH_CODE
#if 1
static void mcu_dl_csot_save_dl_addr(uint8_t idx,uint8_t l_reg_len)
{
    //uint8_t i;
    //i = g_mcu_entry_cfg_4_u.bits.finfail_idx_2_0;
    if(idx == 0)              // DGC
    {
        mcu_dl_access_rom_addr(&g_rom_cost_dgc_entry_addr[0],g_i2c0_spi1_sel,l_reg_len);
    }
    else if(idx == 1)       // hdr
    {
        mcu_dl_access_rom_addr(&g_rom_cost_hdr_entry_addr[0], g_i2c0_spi1_sel,l_reg_len);
    }
    else if(idx == 2)       // gma
    {
        mcu_dl_access_rom_addr(&g_rom_cost_gma_entry_addr[0], g_i2c0_spi1_sel,l_reg_len);
    }
    else if(idx == 3)       // bank A
    {
        mcu_dl_access_rom_addr(&g_rom_cost_bankA_entry_addr[0], g_i2c0_spi1_sel,l_reg_len);            
    }
    else if(idx == 4)       // bank B
    {
        mcu_dl_access_rom_addr(&g_rom_cost_bankB_entry_addr[0], g_i2c0_spi1_sel,l_reg_len);  
    }
    //g_csot_sup_fun[i] = 1;
}
#ifdef FLASH_CODE
static bool mcu_dl_checkXBflash_empty(void) // 0 : empty   /  1 : no empty
{

    uint8_t i = 0;
    mcu_spi_flash_fast_read(&g_rom_cost_dgc_entry_addr[0],&g_check_flash_empty_buf[0],10 , FLASH_CS_XBOARD);
    for(i = 0 ; i < 10 ; i++)
    {
        if(g_check_flash_empty_buf[i] != 0x00 || g_check_flash_empty_buf[i] != 0xFF)
            return 1;
    }
    return 0;
}
#endif
static void mcu_dl_csot_pmic_handler(uint8_t l_reg_len)
{
    CSOT_RTPM_Tcon_DL_CBoard(l_reg_len);
    if(CSOT_RTPM_Ret_Fw_Sta_Crc_Fail_Tcon_Pmic_Flash()) // fail
        return;

    if(CSOT_RTPM_Tcon_Check_Common_Cfg())   // check driving structure X/C Board
    {
        CSOT_RTPM_Tcon_DL_XBoard(l_reg_len);
    }
    CSOT_RTPM_Upload_PMIC_Code_To_PMIC();
}

static void mcu_dl_csot_dgc_handler(uint8_t l_reg_len)
{
    if(g_i2c0_spi1_sel == 0)       // i2c 
        return;
#ifdef FLASH_CODE
    if(!mcu_dl_checkXBflash_empty())
    {
        // dl xb board flash
        g_rom_entry_addr[0] = g_rom_cost_dgc_entry_addr[0];
        g_rom_entry_addr[1] = g_rom_cost_dgc_entry_addr[1];
        g_rom_entry_addr[2] = g_rom_cost_dgc_entry_addr[2];
    }
    mcu_update_data(l_reg_len);
#endif
}
#endif
void mcu_dl_handler(void)
{
    if(mcu_dl_check())
    {
        mcu_dl_get_ee_path();
#ifdef CSOT_FUN
        CSOT_RTPM_Init();
#endif
        mcu_dl_proc_handler();  //JC
    }
    
}

void mcu_dl_entry_cks(void)
{
    uint8_t l_i2c_dat[3];
    volatile uint8_t l_dl_len;
    volatile uint8_t l_reg_len;
    volatile uint8_t l_one_byte;

    if((EXTDAT_SFR_2BH & 0x08) != 0x08) // check 2k eeprom
        return;

    l_reg_len = l_hw_config();
    
    if(l_reg_len == 2)
    {
        l_i2c_dat[0] = (g_mcu_dl_i2c_adr);
        l_i2c_dat[1] = *(volatile uint8_t code *)(&eeprom_entry_data_cks_adr);
        l_i2c_dat[2] = *(volatile uint8_t code *)(&eeprom_entry_data_cks_adr + 1);
    }
    else
    {
        l_one_byte = *(volatile uint8_t code *)(&eeprom_entry_data_cks_adr);
        g_mcu_dl_i2c_adr |= l_one_byte;
        l_i2c_dat[0] = (g_mcu_dl_i2c_adr);    
        l_i2c_dat[1] = *(volatile uint8_t code *)(&eeprom_entry_data_cks_adr+ 1);
    }

    l_dl_len = *(volatile uint8_t code *)(&eeprom_entry_data_cks_len);

    mcu_i2c_mst_read(&l_i2c_dat[0],l_reg_len,&g_entry_cks_tmp[0],l_dl_len,0,0x100,0);
}

bool mcu_entry_dl_check_128Bytes(void)
{
    if(g_dl_entry_cnt == g_2K_entry_num)
        return 1;
    else
        return 0;
}

void mcu_entry_dl_hdr(void) 
{
    uint8_t i  = 0;
    //uint16_t ptr;
    //uint8_t l_ofst = 0;
    //ptr = (uint16_t)g_rw_cfghdr_reg_0000h_cfg_reserved0;
	g_2K_entry_num = 13;
    if((EXTDAT_SFR_2BH & 0x08) != 0x08) // check 2k eeprom
        return;

    if(EXTDAT_SFR_2BH & 0x40)
        return;

    //mcu_entry_dl_check_128Bytes();	
    if(mcu_entry_dl_check_128Bytes())
    {
        if(EXTDAT_SFR_2BH & 0x20) // 128 bytes mode
        {
            //l_ofst = 10;
			g_hdr_ofst += 10;
        }
    }

    for(i = 0 ; i < 10 ; i ++)
	{
        if(i == 7)
        {
            *(volatile uint8_t xdata *)(&g_rw_cfghdr_reg_0000h_cfg_reserved0+i) = g_entry_cks_tmp[g_cks_ofst];
            g_cks_ofst ++;
        }
        else
            *(volatile uint8_t xdata *)(&g_rw_cfghdr_reg_0000h_cfg_reserved0+i) = *(volatile uint8_t code *)(&eeprom_entry_hdr_var+g_hdr_ofst);
        
        g_hdr_ofst ++;
	}
    g_dl_entry_cnt ++;
	EXTDAT_SFR_2BH |= 0x40; // fw cfg hdr done
}
