/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: EK50208
		Platform: 8051
		Author: Eric Lee, 2021/03/19 
		Description: fw cal org checsum
*/
// *******************************************************************
// *******************************************************************
#include "mcu_fun_def.h"
#ifdef DEMURA_TRANS_FUN
// Include Path
#include <stdio.h>
#include <string.h>
#include "mcu_spi.h"
#include "tcon_lut_transfer.h"
#include "demura_load.h"
#include "demura.h"
// *******************************************************************
// *******************************************************************
// define
// #define DL_NVT_DMA_BASE	0x5500
// #define DL_HIX_DMA_BASE	0x4C00
#define DL_DATA_MAX     256
// *******************************************************************
// *******************************************************************
extern uint32_t xdata g_sec_spi_addr;
// *******************************************************************
// *******************************************************************

uint16_t xdata g_ori_cks_val _at_ 0x06B6;
uint16_t xdata g_hw_cks_val _at_ 0x06B8;

static uint32_t Tcon_lut_trans_spi_cal(struct demura_load_reg_cfg_3byte dmr_trans_struct)
{
    uint32_t l_spi_res;
    l_spi_res = dmr_trans_struct.adr_23_16;
    l_spi_res <<= 8;
    l_spi_res |= dmr_trans_struct.adr_15_8;
    l_spi_res <<= 8;
    l_spi_res |= dmr_trans_struct.adr_7_0;
    return l_spi_res;
}

uint32_t Inx_spi_start_addr_cal(void)
{
    uint32_t l_inx_spi_addr;
    // l_inx_spi_addr = Tcon_lut_trans_spi_cal(g_r_demura_inx_cfg);
    l_inx_spi_addr = Tcon_lut_trans_spi_cal(g_demura_load_reg_0013h);
    return l_inx_spi_addr;
}

#if 0
uint32_t lut_spi_chks_addr_cal(void)
{
    uint32_t l_inx_spi_addr;
    // l_inx_spi_addr = Tcon_lut_trans_spi_cal(g_r_demura_inx_cks);
    l_inx_spi_addr = Tcon_lut_trans_spi_cal(g_demura_load_reg_0037h);
    return l_inx_spi_addr;
}
#endif 

uint32_t Lut_spi_dl_len_cal(void)
{
    uint32_t l_spi_dl_len;
    // l_spi_dl_len = Tcon_lut_trans_spi_cal(g_r_demura_que_rd_ofst_cfg);
    l_spi_dl_len = Tcon_lut_trans_spi_cal(g_demura_load_reg_0018h);
    return l_spi_dl_len;
}

uint32_t Inx_spi_dl_len_cal(void)
{
    uint32_t l_spi_dl_len;
    // l_spi_dl_len = Tcon_lut_trans_spi_cal(g_r_rd_offset_cfg);
    l_spi_dl_len = Tcon_lut_trans_spi_cal(g_demura_load_reg_0021h);
    return l_spi_dl_len;
}

#if 0
uint32_t lut_cks_total_dl_len_cal(void)
{
    uint32_t l_cks_dl_len;
    // l_cks_dl_len = Tcon_lut_trans_spi_cal(g_r_demura_145_147_cks_total_len_cfg);
    l_cks_dl_len = Tcon_lut_trans_spi_cal(g_demura_load_reg_003Ah);
    return l_cks_dl_len;
}

static void Inx_cks_fun(void)
{
    uint32_t l_inx_spi_addr;
    uint16_t l_spi_dl_len;
    uint16_t l_line_max;
    uint16_t l_line;

    P1_0 = 0;

    l_inx_spi_addr = lut_spi_chks_addr_cal();
    //mcu_spi_flash_fast_read((uint8_t *)&l_inx_spi_addr + 1, (uint8_t *)&g_org_cks , 2 , 1);  //g_org_cks
    Lut_Transfer_Read_Org_Cks(l_inx_spi_addr, (uint8_t *)&g_org_cks , 2);

    // -------------- Get Inx Cks ------------------//
    l_inx_spi_addr = Inx_spi_start_addr_cal();

    // -------------- Get Inx read addr offset for every read  ------------------//
    l_spi_dl_len = Inx_spi_dl_len_cal();
    // -------------- Get line num - -------------- -------------- ------------------//
    l_line_max = get_line_num();
    // Enable HW Cal Cks
    Lut_Transfer_Hw_Cal_Cks_En(0);
    Lut_Transfer_Hw_Cal_Cks_Mode(CKS_INX_MODE);
    Lut_Transfer_Hw_Cal_Cks_En(1);

    for(l_line = 0 ; l_line < l_line_max ; l_line++)
    {
        // mcu_spi_flash_fast_read((uint8_t *)&l_inx_spi_addr + 1, (uint8_t *)&g_r_spi_inx_demura_cfg_u[0] , l_spi_dl_len , 1);  
        mcu_spi_flash_fast_read((uint8_t *)&l_inx_spi_addr + 1, (uint8_t *)&g_dmr_trans_cfg_buf[0] , l_spi_dl_len , 1);  
        l_inx_spi_addr += l_spi_dl_len;
    }
    g_hw_cks_val = g_r_sys_data_10_u.byte;
    g_ori_cks_val = g_org_cks;
    if(!Lut_Transfer_Cmp_Cks(g_r_sys_data_10_u.byte))
    {
        // mcu_dmr_dl_reg_lut_dl_fail_set(1);
		P1_0 = 1;
    }
}
#endif

uint32_t Nvt_dl_total_len_cal(void)
{
    uint32_t l_line_max;
    uint32_t l_spi_dl_len;
    uint32_t l_nvt_dl_total_len;
    // -------------- Get Inx read addr offset for every read  ------------------// l_spi_dl_len = 481
    l_spi_dl_len = (g_demura_reg_0004h.bits.r_h_lut_num_10_4 << 4) + g_demura_reg_0005h.bits.r_h_lut_num_3_0;
    // l_spi_dl_len = (g_r_dmr_dl_85A_reg_cfg.bits.h_lut_num_10_4 << 4) + g_r_dmr_dl_85B_reg_cfg.bits.h_lut_num_3_0;        
    // -------------- Get line num - -------------- -------------- ------------------// l_line_max = 271
    l_line_max = (g_demura_reg_0005h.bits.r_v_lut_num_10_8 << 8) + g_demura_reg_0006h.bits.r_v_lut_num_7_0;
    // l_line_max = (g_r_dmr_dl_85B_reg_cfg.bits.v_lut_num_9_8 << 8) + g_r_dmr_dl_85C_reg_cfg.bits.v_lut_num_7_0;                                       
	// -------------- Get transfer plane num ------ -------------- ------------------//
	g_set_plane_Num = get_transfer_plane_num(); // plane_num = 3

    l_nvt_dl_total_len = l_spi_dl_len * l_line_max * g_set_plane_Num * 12;

    if((l_nvt_dl_total_len & 0xFF) == 0x00)
    {
        l_nvt_dl_total_len = l_nvt_dl_total_len >> 8;
    }else
    {
        l_nvt_dl_total_len = (l_nvt_dl_total_len >> 8) + 1;
    }
    l_nvt_dl_total_len *= 32;

    return l_nvt_dl_total_len;
}

uint32_t Nvt_spi_addr_cal(void)
{
    uint32_t l_nvt_spi_addr;
    l_nvt_spi_addr = Tcon_lut_trans_spi_cal(g_demura_load_reg_0013h);
    // l_nvt_spi_addr = Tcon_lut_trans_spi_cal(g_r_demura_nvt_cfg);
    return l_nvt_spi_addr;
}

#if 0
static void Nvt_cks_fun(void)
{
    uint32_t l_nvt_spi_addr;
    uint32_t l_dl_total_len;
    uint32_t l_cks_len;
    uint8_t l_dl_len;
    uint32_t l_nvt_dl_total_len;
    P1_0 = 0;

    l_dl_total_len = 0;
    l_cks_len = 0;

    if(g_demura_load_reg_000Dh.bits.r_chksum_total_length_en)
        l_nvt_dl_total_len = Nvt_dl_total_len_cal(); // lut_cks_total_dl_len_cal();
    else
        l_nvt_dl_total_len = 0x8F360;

    if(g_set_plane_Num == 3)
		l_dl_len = 56;
	else if(g_set_plane_Num == 5)
		l_dl_len = 60;
    
    //---------- Read NVT Checksum ----------//
    l_nvt_spi_addr = lut_spi_chks_addr_cal();
	Lut_Transfer_Read_Org_Cks(l_nvt_spi_addr ,(uint8_t xdata *)&g_org_cks, 2);
	//---------------------------------------//

    l_nvt_spi_addr = Nvt_spi_addr_cal();

	Lut_Transfer_Hw_Cal_Cks_Mode(CKS_NVT_MODE);
	Lut_Transfer_Hw_Cal_Crc_Cks_Rst();

    while(l_cks_len < l_nvt_dl_total_len)
	{
		if(l_cks_len + l_dl_len > l_nvt_dl_total_len)
		{
			l_dl_len = l_nvt_dl_total_len - l_cks_len;
		}
		//l_hw_dma_len = l_dl_len;
		// mcu_spi_flash_fast_read((uint8_t *)&l_nvt_spi_addr + 1, (uint8_t xdata *)(DL_NVT_DMA_BASE) , l_dl_len , 1);
        mcu_spi_flash_fast_read((uint8_t *)&l_nvt_spi_addr + 1, (uint8_t xdata *)(&g_dmr_trans_cfg_buf[0]) , l_dl_len , 1);
		l_nvt_spi_addr += l_dl_len;
		l_cks_len += l_dl_len;
	}

    g_hw_cks_val = g_r_sys_data_13_u.byte;
    g_ori_cks_val = g_org_cks;

	if(!Lut_Transfer_Cmp_Cks(g_r_sys_data_13_u.byte))		// crc checksum fail
	{
        // mcu_dmr_dl_reg_lut_dl_fail_set(1);
		P1_0 = 1;
	}
}
#endif

uint32_t Hix_spi_addr_cal(void)
{
    uint32_t l_hix_spi_addr;
    l_hix_spi_addr = Tcon_lut_trans_spi_cal(g_demura_load_reg_0013h);
    // l_hix_spi_addr = Tcon_lut_trans_spi_cal(g_r_demura_hmx_cfg);
    return l_hix_spi_addr;
}

#if 0
static void Hix_cks_fun(void)
{
    uint32_t l_hix_spi_addr;
    uint16_t l_line_max;
    uint16_t l_line;
    uint16_t l_dl_len;
    uint8_t l_plane;
    P1_0 = 0;

    l_line = 0;
    l_plane = 0;
    //---------- Read Hix Checksum ----------//
    l_hix_spi_addr = lut_spi_chks_addr_cal();
    Lut_Transfer_Read_Org_Cks(l_hix_spi_addr ,(uint8_t xdata *)&g_org_cks, 2);
    // -------------- -------------- -------------- -------------- ------------------//
    l_hix_spi_addr = Hix_spi_addr_cal();
    // -------------- Get transfer plane num ------ -------------- ------------------//
    g_set_plane_Num = get_transfer_plane_num(); // plane_num = 3
    // -------------- -------------- -------------- -------------- ------------------//
    // -------------- Get line num - -------------- -------------- ------------------//
    l_line_max = get_line_num();
    // -------------- -------------- -------------- -------------- ------------------//

    // -------------- Get download  num - -------------- -------------- ------------------//
    l_dl_len = Lut_spi_dl_len_cal();
    // -------------- -------------- -------------- -------------- ------------------//

	Lut_Transfer_Hw_Cal_Cks_En(0);
    Lut_Transfer_Hw_Cal_Cks_Mode(CKS_HIX_MODE);
    Lut_Transfer_Hw_Cal_Cks_En(1);
    while(l_line < l_line_max) // 271
	{

		// mcu_spi_flash_fast_read((uint8_t *)&l_hix_spi_addr + 1, (uint8_t xdata*)(DL_HIX_DMA_BASE) , l_dl_len , 1);
        mcu_spi_flash_fast_read((uint8_t *)&l_hix_spi_addr + 1, (uint8_t xdata*)(&g_dmr_trans_cfg_buf[0]) , l_dl_len , 1);

		l_hix_spi_addr += l_dl_len;
        l_plane ++;
		if(l_plane == g_set_plane_Num)
		{
			l_plane = 0;
			l_line ++;
		}
    }
    g_hw_cks_val = g_r_sys_data_10_u.byte;
    g_ori_cks_val = g_org_cks;

    if(!Lut_Transfer_Cmp_Cks(g_r_sys_data_10_u.byte))
    {
        // mcu_dmr_dl_reg_lut_dl_fail_set(1);
		P1_0 = 1;
    }
}

void Sec_chks_mode_sel(void)
{
    uint32_t l_sec_cks_adr;
    l_sec_cks_adr = lut_spi_chks_addr_cal();
    Lut_Transfer_Read_Org_Cks(l_sec_cks_adr ,(uint8_t xdata *)&g_org_cks, 2);
    /*if(!g_r_system_17B_cfg.bits.r_sec_cksum_mode)       // Test Mode
        //tcon_lut_transfer_org_cks(0xDFC50);
	    Lut_Transfer_Read_Org_Cks(0xDFC50 ,(uint8_t xdata *)&g_org_cks, 2);
    else
        //tcon_lut_transfer_org_cks(0x12FC50);
        Lut_Transfer_Read_Org_Cks(0x12FC50 ,(uint8_t xdata *)&g_org_cks, 2);*/
}
#endif

uint32_t Sec_spi_addr_cal(void)
{
    uint32_t l_sec_spi_addr;
    l_sec_spi_addr = Tcon_lut_trans_spi_cal(g_demura_load_reg_0013h);
    // l_sec_spi_addr = Tcon_lut_trans_spi_cal(g_r_demura_sec_cfg);
    return l_sec_spi_addr;
}

#if 0
static void Sec_cks_fun(void)
{
    uint32_t l_sec_spi_addr;
    uint32_t l_spi_dl_len;
    uint32_t l_dl_cnt;
    uint8_t l_demura_cal_cks;
    uint16_t l_read_len;

    P1_0 = 0;

    //tcon_lut_transfer_fill_Fiti_strength();

    //---------- Read SEC Checksum ----------//
    Sec_chks_mode_sel();
	//---------------------------------------//
    //g_sec_spi_addr = 0x1000;
    l_sec_spi_addr = Sec_spi_addr_cal();
    // -------------- Get  read  len  ------------------//
    //l_spi_dl_len = Sec_spi_dl_len_cal();
    /*if(!g_r_system_17B_cfg.bits.r_sec_cksum_mode)
        l_spi_dl_len = 0xDEC50;
    else
        l_spi_dl_len = 0x12FC50;*/
    if(g_demura_load_reg_000Dh.bits.r_chksum_total_length_en)    
        l_spi_dl_len = lut_cks_total_dl_len_cal();
    else
    {
        if(!g_demura_load_reg_0050h.bits.r_sec_chksum_mode)
            l_spi_dl_len = 0xDEC50;
        else
            l_spi_dl_len = 0x12FC50;
    }
    // -------------- Get  read  len for every read  ------------------//
    l_read_len = DL_DATA_MAX;
    // ------------------------------------------------------------------//

    Lut_Transfer_Hw_Cal_Cks_En(0);
    Lut_Transfer_Hw_Cal_Cks_Mode(CKS_SEC_MODE);
    Lut_Transfer_Hw_Cal_Cks_En(1);
    //Lut_Fill_Hw_Reg_En();
    for(l_dl_cnt = 0 ; l_dl_cnt < l_spi_dl_len  ; l_dl_cnt += l_read_len)
    {
        if((l_dl_cnt + l_read_len) >= l_spi_dl_len) 
            l_read_len = l_spi_dl_len - l_dl_cnt;
        // mcu_spi_flash_fast_read((uint8_t *)&l_sec_spi_addr + 1, (uint8_t *)&g_org_cks_buf[0] , l_read_len , 1);
        mcu_spi_flash_fast_read((uint8_t *)&l_sec_spi_addr + 1, (uint8_t *)&g_dmr_trans_cfg_buf[8] , l_read_len , 1);
        l_sec_spi_addr += l_read_len;
    }
    g_hw_cks_val = g_r_sys_data_10_u.byte;
    g_ori_cks_val = g_org_cks>>8;

//    l_demura_cal_cks = 0xFC - (g_r_sys_data_10_u.byte & 0xFF);
	l_demura_cal_cks = 0xFC - (g_hw_cks_val & 0xFF);

    if((uint8_t)(g_ori_cks_val) != l_demura_cal_cks)
    {
        // mcu_dmr_dl_reg_lut_dl_fail_set(1);
		P1_0 = 1;
    }
    P1_1 = 0;    
}
#endif

uint32_t Vd_spi_addr_cal(void)
{
    uint32_t l_vd_spi_addr;
    l_vd_spi_addr = Tcon_lut_trans_spi_cal(g_demura_load_reg_0013h);
    return l_vd_spi_addr;
}

#if 0
void tcon_lut_cal_cks_FwHelpHWFun(void)
{
    //uint32_t l_inx_spi_addr = 0;
    if(!g_hw_trans_fw_cks_flg)
        return;
    //g_hw_trans_fw_cks_flg = 0;    
    g_transfer_mode = g_demura_load_reg_0011h.bits.r_non_fiti_sel;
    mcu_spi_sw_path(SPI_DMR_PATH); // demura flash path   
    if(g_transfer_mode == INX_FORMAT)
    {
        tcon_lut_transfer_set_spi_shift(11,2,0);    // shift : 2 , Mode : 0
        Inx_cks_fun();
        tcon_lut_transfer_set_spi_shift(11,0,0);    // Shift OFF
    }
    else if(g_transfer_mode == NVT_FORMAT)
    {
        tcon_lut_transfer_set_spi_shift(1,4,1);     // shift : 4 , Mode : 1
        Nvt_cks_fun();
        tcon_lut_transfer_set_spi_shift(1,0,0);     // Shift OFF           
    }
    else if(g_transfer_mode == HIX_FORMAT)
    {
        tcon_lut_transfer_set_spi_shift(1,4,0);     // shift : 4 , Mode : 0
        Hix_cks_fun();
        tcon_lut_transfer_set_spi_shift(1,0,0);     // Shift OFF
    }
    else if(g_transfer_mode == SEC_FORMAT)
    {
        Sec_cks_fun();
    }
    mcu_spi_sw_path(SPI_CFG_PATH);
}

#endif 

#endif