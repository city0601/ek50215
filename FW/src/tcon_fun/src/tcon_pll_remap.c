/*
        Copyright 2021 FITIPOWER., Ltd.
        Module: N/A
        Project: EK50305
        Platform: 8051
        Author: Johnny Lee, 2021/07/20
        Description: PLL Remap
*/
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"
#ifdef PLL_REMAP_FUN
#include <intrins.h>          //for _nop_();
#include "tcon_pll_remap.h"
#include "sys.h"
#include "aip_reg.h"
#include "mcu_top.h"

static bool g_vsync_irq_ex3_pllremap_flg;

uint8_t xdata tx_p_ary[28] = {61, 63, 81, 84, 61, 63, 73, 76, 81, 81,
                            84, 84, 84, 43, 87, 44, 49, 51, 51, 53,
                            54, 56, 56, 57, 58, 59, 65, 67};
uint8_t xdata tx_n_ps1[28] = {36, 36, 48, 48, 36, 36, 18, 18, 48, 18,
                            18, 48, 18, 12, 18, 12, 12, 6,  12, 6, 
                            12, 12, 12, 16, 12, 16, 8,  8};

void sys_pll_remap_wo_mrmf(uint8_t l_freq_sel)
{
    uint16_t l_r_syspll_p;

    if((l_freq_sel == 15) || (!g_rw_sys_0083h.bits.r_sysfreq_en))   // 0209 mask 
        return;

    if(l_freq_sel < 15)
    {   
        // sys_pll remap by mapping 
        //g_rw_aip_reg_0039h.bits.r_syspll_n_pre_bits_3_0 = 4;
			  g_rw_aip_reg_00A3h.bits.r_syspll_n_pre_bits_4_0 = 4;
        _nop_();_nop_();_nop_();
        //g_rw_aip_reg_003Ah.bits.r_syspll_n_pre_bits_5_4 = 0;
			  g_rw_aip_reg_00A4h.bits.r_syspll_n_pre_bits_5 = 0;
        _nop_();_nop_();_nop_();

        if(l_freq_sel < 8)
				{
            //g_rw_aip_reg_0040h.bits.r_syspll_sel_w_psclk = 0;
						g_rw_aip_reg_00AAh.bits.r_syspll_sel_w_psclk_bits_0 = 0;
					  _nop_();_nop_();_nop_();
				    g_rw_aip_reg_00ABh.bits.r_syspll_sel_w_psclk_bits_1 = 0;
				}
        else
				{
            //g_rw_aip_reg_0040h.bits.r_syspll_sel_w_psclk = 1;
					  g_rw_aip_reg_00AAh.bits.r_syspll_sel_w_psclk_bits_0 = 1;
					  _nop_();_nop_();_nop_();
				    g_rw_aip_reg_00ABh.bits.r_syspll_sel_w_psclk_bits_1 = 0;
				}
        _nop_();_nop_();

        switch(l_freq_sel)
        {
            case 0:
                l_r_syspll_p = 27;
                //g_rw_aip_reg_003Eh.bits.r_syspll_n_ps1 = 6;
						    g_rw_aip_reg_00A9h.bits.r_syspll_n_ps1 = 6;
                _nop_();_nop_();_nop_();
                //g_rw_aip_reg_003Fh.bits.r_syspll_n_ps2 = 9;
						    g_rw_aip_reg_00AAh.bits.r_syspll_n_ps2 = 9;
                break;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                l_r_syspll_p = l_freq_sel + 35;
                //g_rw_aip_reg_003Eh.bits.r_syspll_n_ps1 = 4;
						    g_rw_aip_reg_00A9h.bits.r_syspll_n_ps1 = 4;
                _nop_();_nop_();_nop_();
                //g_rw_aip_reg_003Fh.bits.r_syspll_n_ps2 = 6;
						    g_rw_aip_reg_00AAh.bits.r_syspll_n_ps2 = 6;
                break;
            case 6:
                l_r_syspll_p = 47;
                //g_rw_aip_reg_003Eh.bits.r_syspll_n_ps1 = 4;
						    g_rw_aip_reg_00A9h.bits.r_syspll_n_ps1 = 4;
                _nop_();_nop_();_nop_();
                //g_rw_aip_reg_003Fh.bits.r_syspll_n_ps2 = 6;
						    g_rw_aip_reg_00AAh.bits.r_syspll_n_ps2 = 6;
                break;
            case 7:
                l_r_syspll_p = 52;
                //g_rw_aip_reg_003Eh.bits.r_syspll_n_ps1 = 4;
						    g_rw_aip_reg_00A9h.bits.r_syspll_n_ps1 = 4;
                _nop_();_nop_();_nop_();
                //g_rw_aip_reg_003Fh.bits.r_syspll_n_ps2 = 6;
						    g_rw_aip_reg_00AAh.bits.r_syspll_n_ps2 = 6;
                break;
            case 8:
                l_r_syspll_p = 24;
                //g_rw_aip_reg_003Eh.bits.r_syspll_n_ps1 = 2;
						    g_rw_aip_reg_00A9h.bits.r_syspll_n_ps1 = 2;
                _nop_();_nop_();_nop_();
                //g_rw_aip_reg_003Fh.bits.r_syspll_n_ps2 = 3;
						    g_rw_aip_reg_00AAh.bits.r_syspll_n_ps2 = 3;
                break;   
            case 9:
            case 10:
            case 11:
            case 12:
                l_r_syspll_p = l_freq_sel + 28;
                //g_rw_aip_reg_003Eh.bits.r_syspll_n_ps1 = 2;
						    g_rw_aip_reg_00A9h.bits.r_syspll_n_ps1 = 2;
                _nop_();_nop_();_nop_();
                //g_rw_aip_reg_003Fh.bits.r_syspll_n_ps2 = 3;
						    g_rw_aip_reg_00AAh.bits.r_syspll_n_ps2 = 3;
                break; 
            case 13:
                l_r_syspll_p = 46;
                //g_rw_aip_reg_003Eh.bits.r_syspll_n_ps1 = 2;
                g_rw_aip_reg_00A9h.bits.r_syspll_n_ps1 = 2;
						    _nop_();_nop_();_nop_();
                //g_rw_aip_reg_003Fh.bits.r_syspll_n_ps2 = 3;
						    g_rw_aip_reg_00AAh.bits.r_syspll_n_ps2 = 3;
                break;
            case 14:
                l_r_syspll_p = 47;
                //g_rw_aip_reg_003Eh.bits.r_syspll_n_ps1 = 2;
						    g_rw_aip_reg_00A9h.bits.r_syspll_n_ps1 = 2;
                _nop_();_nop_();_nop_();
                //g_rw_aip_reg_003Fh.bits.r_syspll_n_ps2 = 3;
						    g_rw_aip_reg_00AAh.bits.r_syspll_n_ps2 = 3;
                break;
            default:
                break;
        }
        _nop_();_nop_();_nop_();
		    //g_rw_aip_reg_0045h_syspll_p = (uint8_t)(l_r_syspll_p & 0xFF);
				g_rw_aip_reg_00B1h.bits.r_syspll_p_bits_4_0 = (l_r_syspll_p & 0x1F);
        _nop_();_nop_();_nop_();
		    //g_rw_aip_reg_0046h.bits.r_syspll_p_bits_8 = (l_r_syspll_p >> 8) & 0x01;
				g_rw_aip_reg_00B2h.bits.r_syspll_p_bits_8_5 = (l_r_syspll_p >> 5) & 0x0F;
        _nop_();_nop_();_nop_();
    }
}

void int_pll_remap(uint8_t l_freq_sel)
{
    uint8_t  l_intpll_ps1;
    uint8_t  l_intpll_ps2;
    uint16_t l_intpll_p;
    
    if((l_freq_sel >= 15) || (!g_rw_sys_0083h.bits.r_ctrlfreq_en))   // 0209 mask
        return;

    if(l_freq_sel < 15)
    {   
        // int_pll remap by mapping 
        //g_rw_aip_reg_0052h.bits.r_intpll_n_pre_bits_4_0 = 4;
			  g_rw_aip_reg_00BEh.bits.r_ctrlpll_n_pre_bits_0 = 0;
        _nop_();_nop_();_nop_();
        //g_rw_aip_reg_0053h.bits.r_intpll_n_pre_bits_5 = 0;
			  g_rw_aip_reg_00BFh.bits.r_ctrlpll_n_pre_bits_5_1 = 2;
        _nop_();_nop_();_nop_();

        if(l_freq_sel < 8)
            //g_rw_aip_reg_0057h.bits.r_intpll_sel_w_psclk = 0;
				    g_rw_aip_reg_00C6h.bits.r_ctrlpll_sel_w_psclk = 0;
        else
            //g_rw_aip_reg_0057h.bits.r_intpll_sel_w_psclk = 1;
				    g_rw_aip_reg_00C6h.bits.r_ctrlpll_sel_w_psclk = 1;
        _nop_();_nop_();_nop_();
        
        switch(l_freq_sel)
        {
            case 0:
                l_intpll_p = 30;
                l_intpll_ps1 = 6;
                l_intpll_ps2 = 9;
                break;
            case 1: case 2: case 3: case 4: case 5:
                l_intpll_p = l_freq_sel + 38;
                l_intpll_ps1 = 4;
                l_intpll_ps2 = 6;
                break;
            case 6:
                l_intpll_p = 50;
                l_intpll_ps1 = 4;
                l_intpll_ps2 = 6;
                break;
            case 7:
                l_intpll_p = 55;
                l_intpll_ps1 = 4;
                l_intpll_ps2 = 6;
                break;
            case 8:
                l_intpll_p = 27;
                l_intpll_ps1 = 2;
                l_intpll_ps2 = 3;
                break;
            case 9:
            case 10:
            case 11:
            case 12:
                l_intpll_p = l_freq_sel + 31;
                l_intpll_ps1 = 2;
                l_intpll_ps2 = 3;
                break;
            case 13:
                l_intpll_p = 49;
                l_intpll_ps1 = 2;
                l_intpll_ps2 = 3;
                break;
            case 14:
                l_intpll_p = 50;
                l_intpll_ps1 = 2;
                l_intpll_ps2 = 3;
                break;
        }
    }

    //g_rw_aip_reg_0055h.bits.r_intpll_n_ps1_bits_3_0 = l_intpll_ps1 & 0x0F;
		g_rw_aip_reg_00C4h.bits.r_ctrlpll_n_ps1_bits_3_0 = l_intpll_ps1 & 0x0F;
    _nop_();_nop_();_nop_();
    //g_rw_aip_reg_0056h.bits.r_intpll_n_ps1_bits_5_4 = l_intpll_ps1 >> 4;
		g_rw_aip_reg_00C5h.bits.r_ctrlpll_n_ps1_bits_5_4 = l_intpll_ps1 >> 4;
    _nop_();_nop_();_nop_();            
    //g_rw_aip_reg_0056h.bits.r_intpll_n_ps2_bits_3_0 = l_intpll_ps2 & 0x0F;
		g_rw_aip_reg_00C5h.bits.r_ctrlpll_n_ps2_bits_3_0 = l_intpll_ps2 & 0x0F;
    _nop_();_nop_();_nop_();
    //g_rw_aip_reg_0057h.bits.r_intpll_n_ps2_bits_5_4 = l_intpll_ps2 >> 4;
		g_rw_aip_reg_00C6h.bits.r_ctrlpll_n_ps2_bits_5_4 = l_intpll_ps2 >> 4;
    _nop_();_nop_();_nop_();
    //g_rw_aip_reg_005Ch.bits.r_intpll_p_bits_4_0 = l_intpll_p & 0x001F;
		g_rw_aip_reg_00CCh.bits.r_ctrlpll_p_bits_0 = l_intpll_p & 0x0001;
    _nop_();_nop_();_nop_();
    //g_rw_aip_reg_005Dh.bits.r_intpll_p_bits_8_5 = l_intpll_p >> 5;
		g_rw_aip_reg_00CDh_ctrlpll_p = l_intpll_p >> 1;
    _nop_();_nop_();_nop_(); 
}

void tx_pll_remap(uint8_t l_freq_sel)
{
    if(!g_rw_sys_0083h.bits.r_txfreq_en)   // 0209 mask
        return;
    if(l_freq_sel > 27)
        return;
    
    //decide r_tx_sel_vcodiv
    if(l_freq_sel <= 3)
        //g_rw_aip_reg_0015h.bits.r_tx_sel_vcodiv = 2;
		    g_rw_aip_reg_0017h.bits.r_tx_sel_vcodiv = 2;
    else if((l_freq_sel <= 12) || (l_freq_sel == 14))
        //g_rw_aip_reg_0015h.bits.r_tx_sel_vcodiv = 1;
		    g_rw_aip_reg_0017h.bits.r_tx_sel_vcodiv = 1;
    else
        //g_rw_aip_reg_0015h.bits.r_tx_sel_vcodiv = 0;
		    g_rw_aip_reg_0017h.bits.r_tx_sel_vcodiv = 0;
    _nop_();_nop_();_nop_();
    // decide r_tx_sel_w_psclk
    if((l_freq_sel == 17) || (l_freq_sel == 19))
		{
        //g_rw_aip_reg_000Dh.bits.r_tx_sel_w_psclk = 0;
		    g_rw_aip_reg_000Dh.bits.r_tx_sel_w_psclk_bits_0 = 0;
			  _nop_();_nop_();_nop_();
			  g_rw_aip_reg_000Eh.bits.r_tx_sel_w_psclk_bits_1 = 0;
    }
		else
		{
        //g_rw_aip_reg_000Dh.bits.r_tx_sel_w_psclk = 1;
			  g_rw_aip_reg_000Dh.bits.r_tx_sel_w_psclk_bits_0 = 1;
			  _nop_();_nop_();_nop_();
			  g_rw_aip_reg_000Eh.bits.r_tx_sel_w_psclk_bits_1 = 0;
		}
    _nop_();_nop_();_nop_();

    g_rw_aip_reg_0002h.bits.r_tx_n_pre = 6;
    _nop_();_nop_();_nop_();

    //g_rw_aip_reg_000Ah.bits.r_tx_p_bits_0 = tx_p_ary[l_freq_sel] & 0x01;
		g_rw_aip_reg_000Bh.bits.r_tx_p_bits_3_0 = tx_p_ary[l_freq_sel] & 0x0F;
    _nop_();_nop_();_nop_();
    //g_rw_aip_reg_000Bh_tx_p = (tx_p_ary[l_freq_sel] >> 1);
		g_rw_aip_reg_000Ch.bits.r_tx_p_bits_8_4 = (tx_p_ary[l_freq_sel] >> 4) & 0x1F;
    _nop_();_nop_();_nop_();

    //g_rw_aip_reg_000Ch.bits.r_tx_n_ps1_bits_4_0 = tx_n_ps1[l_freq_sel] & 0x0F;
    //_nop_();_nop_();_nop_();
    //g_rw_aip_reg_000Dh.bits.r_tx_n_ps1_bits_5 = (tx_n_ps1[l_freq_sel] >> 4);
		g_rw_aip_reg_000Dh.bits.r_tx_n_ps1 = (tx_n_ps1[l_freq_sel] & 0x3F);
    _nop_();_nop_();_nop_();
}

void tcon_pll_remap_handler(void)
{
    sys_pll_remap_wo_mrmf(g_rw_sys_0082h.bits.r_sysfreq_sel);

    int_pll_remap(g_rw_sys_0082h.bits.r_ctrlfreq_sel);    // ctrl pll  

    tx_pll_remap(g_rw_sys_0083h.bits.r_txfreq_sel);
}

void tcon_pll_remap_isr(void)
{
    g_vsync_irq_ex3_pllremap_flg = 1;
}

void tcon_i2c_pll_remap_proc(void)
{    
    if(!g_vsync_irq_ex3_pllremap_flg)
        return;
    
    if(g_rw_mcu_top_0020h_i2c_over_fw_addr1 == 0x09)
    {
        switch (g_rw_mcu_top_0021h_i2c_over_fw_addr2)
        {
            case 0xE0:
                sys_pll_remap_wo_mrmf(g_rw_mcu_top_0039h_i2c_over_data1 & 0x0F);
                int_pll_remap(g_rw_mcu_top_0039h_i2c_over_data1 >> 4);
                break;

            case 0xE1:
                tx_pll_remap(g_rw_mcu_top_0039h_i2c_over_data1 & 0x0F);
                break;
        }
        
    }
    g_vsync_irq_ex3_pllremap_flg = 0;

}

#endif