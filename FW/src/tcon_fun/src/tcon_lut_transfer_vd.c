/*
		Copyright 2022 FITIPOWER., Ltd.
		Module: N/A
		Project: tcon
		Platform: 8051
		Author: Johnny Lee, 2022/08/31
		Description: 
*/
// *******************************************************************
// *******************************************************************
#include "mcu_fun_def.h"
#ifdef VD_TRANS_FUN
#include "tcon_lut_transfer.h"
#include "tcon_lut_cal_cks.h"
#include "tcon_lut_transfer_vd.h"
#include "mcu_spi.h"

#define DMU_FLASH_CS        1

// *******************************************************************
static uint32_t xdata g_vd_adr_ofset;

// static uint8_t xdata g_plane;

static uint32_t xdata g_vd_adr;
// *******************************************************************

#if 0
void vd_trans_line()
{
    uint8_t l_color_cnt, l_plane_cnt, l_byte_cnt;
    uint16_t l_read_cnt, l_buf_inx, l_dl_len;

    l_dl_len = 484;         // 481 data + 3 dummy

    // For each color (R/G/B)
    for (l_color_cnt = 0; l_color_cnt < 3; l_color_cnt++)
    {   
        // For each plane (Plane 1/2/3)
        for (l_plane_cnt = 0; l_plane_cnt < 3; l_plane_cnt++)
        {
            l_byte_cnt = 0;     // cnt for reverse byte position

            // Set Plane Number
            g_plane = l_plane_cnt;
            Lut_Set_Plane_Num(l_plane_cnt);

            // SPI read 481 data + 3 dummy
            mcu_spi_flash_fast_read((uint8_t*) &g_vd_adr, (uint8_t*) &g_dmr_trans_cfg_buf[0], l_dl_len, DMU_FLASH_CS);

            // For each data in a line (481 data)
            for (l_read_cnt = 0; l_read_cnt < 481; l_read_cnt++)
            {
                l_buf_inx = l_read_cnt + 3 - l_byte_cnt;

                // Store transfered data
                Lut_Transfer_Fiti_Cal_Cks(g_dmr_trans_cfg_buf[l_buf_inx]);

                if(l_byte_cnt == 6)
                    l_byte_cnt = 0;
                else
                    l_byte_cnt += 2;

            }
            g_vd_adr += l_dl_len;      // 481 data + 3 dummy
        }     
    }
}
#endif

void vd_trans_line()
{
    uint8_t l_color_cnt, l_plane_cnt, l_dl_len;
    uint16_t l_read_cnt;

    l_dl_len = 4;
    // For each 8 data in a line (484 data = 4 * 121)
    for (l_read_cnt = 0; l_read_cnt < 121; l_read_cnt++)
    {
        g_vd_adr = g_vd_adr_ofset;

        // For each color (R/G/B)
        for (l_color_cnt = 0; l_color_cnt < 3; l_color_cnt++)
        {   
            // For each plane (Plane 1/2/3)
            for (l_plane_cnt = 0; l_plane_cnt < 3; l_plane_cnt++)
            {
                if(tcon_lut_transfer_chk_hw_stop())
                {
                    Lut_Set_Plane_Num(l_plane_cnt + 1);

                    mcu_spi_flash_fast_read((uint8_t*) &g_vd_adr + 1, (uint8_t*) &g_dmr_trans_cfg_buf[0], l_dl_len, DMU_FLASH_CS);
                }
                
                g_vd_adr += 484;        // move to next plane
            }
        }
        g_vd_adr_ofset += l_dl_len;     // move to next 8 data
    }

    g_vd_adr_ofset += 3872;    // 3872 = 484 * (9-1);

}


// *******************************************************************
void tcon_lut_transfer_VDtoFiti(void)
{
    uint16_t l_line_cnt, l_line_max;

    Lut_Fill_Hw_Reg_En();
    
    g_vd_adr_ofset = Vd_spi_addr_cal();
    l_line_max = get_line_num();

    for (l_line_cnt = 0; l_line_cnt < l_line_max; l_line_cnt++)
    {
        vd_trans_line();
    }
}

#endif
