/*
		Copyright 2022 FITIPOWER., Ltd.
		Module: N/A
		Project: EK50503
		Platform: 8051
		Author: Eric Lee, 2022/03/01
		Description: 
*/
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"
#include "mcu_i2c.h"
#ifdef CSOT_FUN
    #include "csot_global_include.h"
#endif
// *******************************************************************
#if 0
static uint8_t xdata mcu_i2c2csot_cmd_idx;
static uint8_t *mcu_i2c2csot_cmd_buf;
static uint8_t xdata mcu_i2c2csot_cmd_buf_idx;
static uint8_t xdata mcu_i2c2csot_cmd_buf_len;
static uint8_t xdata mcu_i2c2csot_reply_data;
static bool mcu_i2c2csot_cmd_init;
//static bool mcu_i2c2csot_cmd_doing;

//********************************************************************/
// CSOT
//********************************************************************/
#ifdef CSOT_FUN
void tcon_i2c_over_csot_start(uint8_t *cmd_buf, uint16_t buf_len)
{
    uint8_t i;

    mcu_i2c2csot_cmd_idx = 0xFF;

    if (buf_len > 0) {
        for (i = 0; i < g_csot_i2ccmd_num; i++) {
            if (((cmd_buf[0] == mcu_i2c2csot_cmd_table[i].i2c2csot_id) && (buf_len == mcu_i2c2csot_cmd_table[i].cmd_len)) ||
                //((cmd_buf[0] == mcu_i2c2csot_cmd_table[i].i2c2csot_id) && (cmd_buf[0] == CSOT_CMD_WRITE_GAMMA_CODE) && (buf_len <= mcu_i2c2csot_cmd_table[i].cmd_len)) ||
                ((cmd_buf[0] == mcu_i2c2csot_cmd_table[i].i2c2csot_id) && (cmd_buf[0] == CSOT_CMD8_WRITE_PMIC_CODE) && (buf_len <= mcu_i2c2csot_cmd_table[i].cmd_len)) ||
                ((cmd_buf[0] == mcu_i2c2csot_cmd_table[i].i2c2csot_id) && (cmd_buf[0] == CSOT_CMD14_WRITE_TO_CFLASH) && (buf_len <= mcu_i2c2csot_cmd_table[i].cmd_len)) ||
                ((cmd_buf[0] == mcu_i2c2csot_cmd_table[i].i2c2csot_id) && (cmd_buf[0] == CSOT_CMD16_WRITE_FROM_PMIC_TO_FLASH) && (buf_len <= mcu_i2c2csot_cmd_table[i].cmd_len)))
            {
                mcu_i2c2csot_cmd_idx = i;
                mcu_i2c2csot_cmd_buf = cmd_buf;
                mcu_i2c2csot_cmd_init = TRUE;
                return;
            }
        }
    }
}


void tcon_i2c_over_csot_proc(void)
{
    if (mcu_i2c2csot_cmd_init && (mcu_i2c2csot_cmd_idx != 0xFF)) {
        (mcu_i2c2csot_cmd_table[mcu_i2c2csot_cmd_idx].cmd_func)();
        mcu_i2c2csot_cmd_init = FALSE;
    }
}
#endif
#endif