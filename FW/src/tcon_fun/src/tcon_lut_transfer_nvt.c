/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: EK50208
		Platform: 8051
		Author: Eric Lee, 2021/03/22
		Description: NVT Transfer
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include <stdio.h>
#include <string.h>
#include "mcu_fun_def.h"
#include "mcu_spi.h"
#include "tcon_lut_transfer.h"
#include "tcon_lut_transfer_nvt.h"
// #include "mcu_dmr_dl.h"
#include "tcon_lut_cal_cks.h"
// *******************************************************************
// *******************************************************************
// define
#define NVT_POINT_MAX	130350
// #define DL_NVT_DMA_BASE	0x5500
// *******************************************************************
// *******************************************************************
// Extern Func
#ifdef KEIL_SIM_NVT 
extern unsigned char xdata NvtData[110];
#endif
// *******************************************************************
//uint16_t xdata g_nvt_cal_crc_cks = 0;
//uint16_t xdata g_nvt_crc_cks = 0;
uint8_t xdata g_val = 0;
uint8_t xdata g_nvt_dl_len_set_1 = 0;
uint8_t xdata g_nvt_dl_len_set_2 = 0;
uint32_t xdata g_nvt_dl_total_len = 0;

#if 0
void tcon_lut_transfer_NVTtoFiti(void)
{
	uint32_t l_nvt_spi_addr;
	uint32_t l_dl_total_len;
	uint32_t l_cks_len;
	uint16_t l_val;
	uint8_t l_dl_len;
	uint8_t l_hw_dma_len;
	uint8_t l_plane;
	//uint8_t l_align_cnt;
	uint8_t l_out_tmp;
	uint8_t i;

	P1_0 = 0;
	l_plane = 0;
	l_dl_total_len = 0;
	l_cks_len = 0;
	g_demura_cal_cks = 0;
	g_lut_compress_flg = 0;
	//g_nvt_dl_len_set_1 = 54;
	g_nvt_dl_len_set_1 = 56;
	g_nvt_dl_len_set_2 = 60;
	//g_nvt_dl_total_len = 0x874;

	tcon_lut_transfer_nvt_hix_init();

	g_nvt_dl_total_len = Nvt_dl_total_len_cal(); // lut_cks_total_dl_len_cal();

	if(g_user_mode_flg)
	{
		l_dl_len = g_nvt_dl_len_set_1;
	}
	else
	{
		if(g_set_plane_Num == 3)
			l_dl_len = g_nvt_dl_len_set_1;
		else if(g_set_plane_Num == 5)
			l_dl_len = g_nvt_dl_len_set_2;
	}
	
	//---------- Read NVT Checksum ----------//
	//Lut_Transfer_Read_Org_Cks(0x40 ,(uint8_t xdata *)&g_org_cks, 2);
	//---------------------------------------//
#if 0
	l_nvt_spi_addr = Nvt_spi_addr_cal();
	P1_1 = 1;
	
	Lut_Transfer_Hw_Cal_Cks_Mode(CKS_NVT_MODE);
	Lut_Transfer_Hw_Cal_Crc_Cks_Rst();

	while(l_cks_len < g_nvt_dl_total_len)
	{
		if(l_dl_total_len + l_dl_len > g_nvt_dl_total_len)
		{
			l_dl_len = g_nvt_dl_total_len - l_dl_total_len;
		}
		//l_hw_dma_len = l_dl_len;
		mcu_spi_flash_fast_read((uint8_t *)&l_nvt_spi_addr + 1, (uint8_t xdata *)(DL_NVT_DMA_BASE) , l_dl_len , 1);
		l_nvt_spi_addr += l_dl_len;
		l_cks_len += l_dl_len;
	}
	if(!Lut_Transfer_Cmp_Cks(g_r_sys_data_13_u.byte))		// crc checksum fail
	{
		mcu_dmr_dl_reg_lut_dl_fail_set(1);
		P1_0 = 1;
		return;
	}

	Lut_Transfer_Hw_Cal_Cks_Mode(CKS_NVT_MODE);
	Lut_Transfer_Hw_Cal_Crc_Cks_Rst();
	//P2_0 = 0;
	if(g_lut_compress_flg)		// search max val
	{
		//while(l_point < 130351)
		while(l_dl_total_len < g_nvt_dl_total_len)
		{
			if(l_dl_total_len + l_dl_len > g_nvt_dl_total_len)
			{
				l_dl_len = g_nvt_dl_total_len - l_dl_total_len;
			}
			l_hw_dma_len = l_dl_len;
			mcu_spi_flash_fast_read((uint8_t *)&l_nvt_spi_addr + 1, (uint8_t xdata *)(DL_NVT_DMA_BASE) , l_hw_dma_len , 1);
			l_nvt_spi_addr += l_dl_len;
			l_dl_total_len += l_dl_len;
			for(i = 0; i < g_r_hw_align_len ; i+=2)
			{
				if((g_r_hw_align_len - i ) < 2)
				break;
				l_val = *(volatile uint8_t xdata *)(DL_NVT_DMA_BASE + i);
				l_val <<= 4;
				l_val |= *(volatile uint8_t xdata *)(DL_NVT_DMA_BASE + i + 1);
				tcon_lut_transfer_nvt_hix_get_max_val(l_plane , (l_val & 0x800)?((~l_val + 1) & 0x7FF):(l_val & 0x7FF));
				l_plane++;
				if(l_plane == g_set_plane_Num)
				{
					l_plane = 0;
					//l_point++;
				}
			}
		}
	}
	#endif
	l_nvt_spi_addr = Nvt_spi_addr_cal();

	l_dl_total_len = 0;
	l_plane = 0;
	Lut_Fill_Hw_Reg_En();
	while(l_dl_total_len < g_nvt_dl_total_len)
	{
		if(l_dl_total_len + l_dl_len > g_nvt_dl_total_len)
		{
			l_dl_len = g_nvt_dl_total_len - l_dl_total_len;
			l_dl_total_len = l_dl_total_len + l_dl_len;
		}
		else
		{
			l_dl_total_len = l_dl_total_len + (l_dl_len - 2);
		}
		l_hw_dma_len = l_dl_len;
    	// mcu_spi_flash_fast_read((uint8_t *)&l_nvt_spi_addr + 1, (uint8_t xdata *)(DL_NVT_DMA_BASE) , l_hw_dma_len , 1);
		mcu_spi_flash_fast_read((uint8_t *)&l_nvt_spi_addr + 1, (uint8_t xdata*)(&g_dmr_trans_cfg_buf[0]) ,l_hw_dma_len , 1);
		/*l_nvt_spi_addr += l_dl_len;
		l_dl_total_len += l_dl_len;*/
		l_nvt_spi_addr = l_nvt_spi_addr + (l_dl_len - 2);
		//l_dl_total_len = l_dl_total_len + (l_dl_len - 2);
		
		for(i = 0; i < (g_r_hw_align_len - 2) ; i += 2)
		{
			if((g_r_hw_align_len - i) < 2)
				break;
			// l_val = *(volatile uint8_t xdata *)(DL_NVT_DMA_BASE + i);
			l_val = *(volatile uint8_t xdata *)((&g_dmr_trans_cfg_buf[0]) + i);
			l_val <<= 4;
			// l_val |= *(volatile uint8_t xdata *)(DL_NVT_DMA_BASE + i + 1);
			l_val |= *(volatile uint8_t xdata *)((&g_dmr_trans_cfg_buf[0]) + i + 1);
			if(g_lut_compress_flg)
			{
				// compress
				l_out_tmp = tcon_lut_transfer_12b_to_8b(l_plane,l_val);
			}
			else
			{
				l_out_tmp = l_val >> 2;
			}
			g_val = l_out_tmp;
			Lut_Transfer_Fiti_Cal_Cks(l_out_tmp);
			l_plane++;
			if(l_plane == g_set_plane_Num)
			{
				l_plane = 0;
			}
			Lut_Set_Plane_Num(l_plane);
		}
	}
	
	//if(!Lut_Transfer_Cmp_Cks(g_r_sys_data_13_u.byte))
	//	P1_0 = 1;
	P1_1 = 0;
}

#endif

void tcon_lut_transfer_NVTtoFiti(void)
{
	uint32_t l_nvt_spi_addr;
	uint32_t l_dl_total_len;
	uint8_t l_dl_len;

	g_nvt_dl_total_len = Nvt_dl_total_len_cal(); // lut_cks_total_dl_len_cal();
	l_nvt_spi_addr = Nvt_spi_addr_cal();
	l_dl_len = 16;
	l_dl_total_len = 0;

	Lut_Fill_Hw_Reg_En();
	while(l_dl_total_len < g_nvt_dl_total_len)
	{
		// Last Download
		if(l_dl_total_len + l_dl_len > g_nvt_dl_total_len) 
		{
			l_dl_len = g_nvt_dl_total_len - l_dl_total_len;
			l_dl_total_len = l_dl_total_len + l_dl_len;
		}
		else
		{
			l_dl_total_len = l_dl_total_len + l_dl_len;
		}

		if(tcon_lut_transfer_chk_hw_stop())
        {
			mcu_spi_flash_fast_read((uint8_t *)&l_nvt_spi_addr + 1, (uint8_t xdata*)(&g_dmr_trans_cfg_buf[0]), l_dl_len , 1);
		}

		l_nvt_spi_addr = l_nvt_spi_addr + l_dl_len;
	}
}
