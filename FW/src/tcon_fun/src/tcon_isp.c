/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: EK50208
		Platform: 8051
		Author: Eric Lee, 2021/03/19
		Description: i2c over spi
*/
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"
#include "tcon_isp.h"
#include "reg_include.h"
// *******************************************************************
#ifdef ISP_FUN
// ISP
bool g_process_isp_flg = 0;
uint8_t xdata g_org_ip_dbgo_sel = 0;
uint8_t xdata g_org_p2p_dbgo_sel = 0;
// *******************************************************************
static void isp_pwrc_sel(uint8_t val)
{
	uint8_t l_mcu_isp_dyn_lcs_len = 0;
    uint16_t temp16 = 0;
	
    l_mcu_isp_dyn_lcs_len = g_rw_p2p_05C5h.bits.r_mcu_isp_dyn_lcs_len;
    switch(val)
    {
        case 0:
        case 1:
        temp16 = (g_rw_p2p_05C3h.bits.r_mcu_isp_dyn_pol_bits_8_2 << 2) + g_rw_p2p_05C2h.bits.r_mcu_isp_dyn_pol_bits_1_0;
        g_rw_p2p_0549h.bits.r_isp_polb_bits_2_0 = temp16 & 0x0007;
        g_rw_p2p_054Ah.bits.r_isp_polb_bits_8_3 = (temp16 >> 3) & 0x3F;
        if(val == 1)
        {
            g_rw_p2p_001Bh.bits.r_lcs_len = l_mcu_isp_dyn_lcs_len + 1;
            g_rw_p2p_0053h.bits.r_lcs_cmd0_idx = 6;
        }
        else
        {
            g_rw_p2p_001Bh.bits.r_lcs_len = l_mcu_isp_dyn_lcs_len;
            g_rw_p2p_0053h.bits.r_lcs_cmd0_idx = 1;
        }
        g_rw_p2p_0549h.bits.r_isp_polb_en = 0;
        break;

        case 2:
        case 3:
		temp16 = (g_rw_p2p_05C4h_mcu_isp_dyn_pol_bias << 1) + g_rw_p2p_05C3h.bits.r_mcu_isp_dyn_pol_bias_bits_0;
        g_rw_p2p_0549h.bits.r_isp_polb_bits_2_0 = (temp16 & 0x07);
        g_rw_p2p_054Ah.bits.r_isp_polb_bits_8_3 = (temp16 >> 3) & 0x3F;
        g_rw_p2p_001Bh.bits.r_lcs_len = l_mcu_isp_dyn_lcs_len + 1;
        g_rw_p2p_0053h.bits.r_lcs_cmd0_idx = 6;
        g_rw_p2p_0549h.bits.r_isp_polb_en = 1;
        break;
        default:
        break;
    }
}

void tcon_isp_proc(void)
{
    uint8_t sel = 0;
    if(!g_process_isp_flg)
        return;
    g_process_isp_flg = 0;
    if(g_rw_dbg_mux_0179h_outdbgr_7 & 0x20)      // 0xF979[5] / hit3
        sel = g_rw_p2p_05C1h.bits.r_mcu_isp_dyn_sel1;

    else if(g_rw_dbg_mux_0179h_outdbgr_7 & 0x40) // 0xF979[6] / hit2
        sel = g_rw_p2p_05C2h.bits.r_mcu_isp_dyn_sel2;

    else if(g_rw_dbg_mux_0179h_outdbgr_7 & 0x80) // 0xF979[7] / hit1
        sel = g_rw_p2p_05C2h.bits.r_mcu_isp_dyn_sel3;

    else if(g_rw_dbg_mux_0179h_outdbgr_7 & 0x10) // 0xF979[4] / smart cs
        sel = g_rw_p2p_05C2h.bits.r_mcu_isp_dyn_sel4;  
    else
        sel = 0x0;
    isp_pwrc_sel(sel);     
		
}

void tcon_isp_check_event(void)
{
    if(g_rw_p2p_05C1h.bits.r_mcu_isp_dyn_en)
    {
        g_process_isp_flg = 1;
    }
}

#endif

