/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: EK50208
		Platform: 8051
		Author: Eric Lee, 2021/03/22
		Description: Inx Transfer
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include <stdio.h>
#include <string.h>
#include "mcu_fun_def.h"
#include "mcu_spi.h"
#include "tcon_lut_transfer.h"
#include "tcon_lut_transfer_inx.h"
// #include "mcu_dmr_dl.h"
#include "tcon_lut_cal_cks.h"
//#include "test_code.h"
#include "demura_load.h"

// *******************************************************************
// *******************************************************************

#define MAX_INX_LINE_SIZE       1760
//#define MAX_FITI_FROMAT_SIZE    1877        // (155 * 12)
#define MAX_FITI_FROMAT_SIZE    1860        // (155 * 12)
#define MAX_FITI_INX_STRUCT_CNT 155         // (496/16) * 5

// *******************************************************************
// MDU define section
#define SHIFT_LEFT  0
#define SHIFT_RIGHT 1
// *******************************************************************

// *******************************************************************
// Keil Sim section
#ifdef KEIL_SIM_INX  
extern unsigned char xdata fitiData[0x1C2F];
//extern unsigned char code fiti_Row_Data[0x2EF9];
extern unsigned char xdata inxData[5580];
#endif
// *******************************************************************
struct r_inx_hdr
{
    uint8_t type_msb;
    uint8_t type_lsb;
    uint8_t cks_msb;
    uint8_t cks_lsb;
    uint8_t start_addr_h_msb;
    uint8_t start_addr_h_lsb;
    uint8_t start_addr_l_msb;
    uint8_t start_addr_l_lsb;
};
volatile struct r_inx_hdr xdata g_r_inx_hdr;


#ifdef TANSFER_INX  
#define  INX_SPI_DL_BASE         			0xE800		   
#define  INX_STRUCT_CFG_BASE     			0xEF44	  
#define  INX_HW_CAL_BASE					0xFFE0

// uint8_t g_inx_last_code_val;
uint8_t g_inx_last_Q_val ;
uint8_t g_inx_last_N_val;
uint8_t g_inx_last_mode;

volatile struct rw_inx_demura_cfg xdata g_rw_inx_demura_cfg_u           _at_    INX_STRUCT_CFG_BASE;
volatile struct rw_inx_decode_cfg xdata g_rw_inx_decode_cfg_u           _at_    (INX_STRUCT_CFG_BASE + 0x16);
volatile struct r_demura_cfg_0  xdata g_r_spi_inx_demura_cfg_u[155]     _at_    INX_SPI_DL_BASE;        // 155 = (496/16) * 5    HW SPI DMA --> INX FORMAT

uint8_t xdata g_rw_set_inx_hw_cal_en									_at_	INX_HW_CAL_BASE;
uint8_t xdata g_w_set_inx_numerator_1									_at_	(INX_HW_CAL_BASE + 1);
uint8_t xdata g_w_set_inx_numerator_2									_at_	(INX_HW_CAL_BASE + 2);
uint16_t xdata g_w_set_inx_numerator_3									_at_	(INX_HW_CAL_BASE + 3);
uint8_t xdata g_w_set_inx_add_num										_at_	(INX_HW_CAL_BASE + 5);
uint8_t xdata g_r_result_27_24											_at_	(INX_HW_CAL_BASE + 6);
uint8_t xdata g_r_result_23_16											_at_	(INX_HW_CAL_BASE + 7);
uint8_t xdata g_r_result_15_8											_at_	(INX_HW_CAL_BASE + 8);
uint8_t xdata g_r_result_7_0											_at_	(INX_HW_CAL_BASE + 9);
uint8_t xdata g_w_inx_set_offset										_at_	(INX_HW_CAL_BASE + 10);
uint16_t xdata g_r_hw_align_len_inx    									_at_	(INX_HW_CAL_BASE + 11);
uint8_t xdata g_r_result1_23_16											_at_	(INX_HW_CAL_BASE + 13);
uint8_t xdata g_r_result1_15_8											_at_	(INX_HW_CAL_BASE + 14);
uint8_t xdata g_r_result1_7_0											_at_	(INX_HW_CAL_BASE + 15);
#endif


uint16_t xdata g_plane = 0;
uint16_t xdata g_plane_cnt = 0;

void tcon_lut_transfer_Inx_Init(void)
{
    g_inx_last_Q_val = 0;
    // g_inx_last_code_val = 0;
    g_inx_last_N_val = 0;
    g_inx_last_mode = 0xff;
}


static void Lut_Transfer_Align_Inx_Format(struct r_inx_m0 *val)
{
    g_rw_inx_demura_cfg_u.code_data[0] =  val->m0_code_0;
    g_rw_inx_demura_cfg_u.code_data[1] =  val->m0_code_1;
    g_rw_inx_demura_cfg_u.code_data[2] =  val->m0_code_2;
    g_rw_inx_demura_cfg_u.code_data[3] =  val->m0_code_3;
    g_rw_inx_demura_cfg_u.code_data[4] =  val->m0_code_4;
    g_rw_inx_demura_cfg_u.code_data[5] =  val->m0_code_5;
    g_rw_inx_demura_cfg_u.code_data[6] =  val->m0_code_6;
    g_rw_inx_demura_cfg_u.code_data[7] =  val->m0_code_7;
    g_rw_inx_demura_cfg_u.code_data[8] =  val->m0_code_8;
    g_rw_inx_demura_cfg_u.code_data[9] =  val->m0_code_9;
    g_rw_inx_demura_cfg_u.code_data[10] =  val->m0_code_10;
    g_rw_inx_demura_cfg_u.code_data[11] =  val->m0_code_11;
    g_rw_inx_demura_cfg_u.code_data[12] =  val->m0_code_12;
    g_rw_inx_demura_cfg_u.code_data[13] =  val->m0_code_13;
    g_rw_inx_demura_cfg_u.code_data[14] =  val->m0_code_14;
    g_rw_inx_demura_cfg_u.code_data[15] =  val->m0_code_15;

    g_rw_inx_demura_cfg_u.mode[0] = val->m0_mode_0;
    g_rw_inx_demura_cfg_u.mode[1] = val->m0_mode_1;
    g_rw_inx_demura_cfg_u.mode[2] = (val->m0_mode_2_2_1 << 2 | val->m0_mode_2_1_0);
    g_rw_inx_demura_cfg_u.mode[3] = val->m0_mode_3;

    g_rw_inx_demura_cfg_u.range = (val->m0_range_6_4 << 4 | val->m0_range_3_0);
    g_rw_inx_demura_cfg_u.min = (val->m0_min_6_5 << 5 | val->m0_min_4_0);

}

static void Lut_Transfer_Inx_Cal_SubRange(uint8_t *dst_val,uint8_t val_offset)
{
    g_w_set_inx_numerator_2 = val_offset;
    g_rw_set_inx_hw_cal_en = 1;
    while(g_rw_set_inx_hw_cal_en);
    *dst_val = g_r_result_7_0;
}

static void Lut_Transfer_Inx_Cal_M_N_Q(uint8_t mode , struct rw_inx_decode_cfg *ptr , uint8_t seq)
{
    uint16_t l_m_val;

    if(g_inx_last_mode == mode)
    {
        *(&ptr->N[seq]) = g_inx_last_N_val;
        *(&ptr->Q[seq]) = g_inx_last_Q_val;
        return;
    }

    switch(mode)
    {
        case 0:
            l_m_val = g_rw_inx_demura_cfg_u.min + g_rw_inx_demura_cfg_u.range;
            *(&ptr->N[seq]) = g_rw_inx_demura_cfg_u.min;
        break;
        case 1:
            l_m_val = ptr->sub_range[2];
            *(&ptr->N[seq]) = g_rw_inx_demura_cfg_u.min;
        break;
        case 2:
            l_m_val = ptr->sub_range[1];
            *(&ptr->N[seq]) = g_rw_inx_demura_cfg_u.min;
        break;
        case 3:
            l_m_val = ptr->sub_range[0];
            *(&ptr->N[seq]) = g_rw_inx_demura_cfg_u.min;
        break;
        case 4:
            l_m_val = g_rw_inx_demura_cfg_u.min + g_rw_inx_demura_cfg_u.range;
            *(&ptr->N[seq]) = ptr->sub_range[0];
        break;
        case 5:
            l_m_val = g_rw_inx_demura_cfg_u.min + g_rw_inx_demura_cfg_u.range;
            *(&ptr->N[seq]) = ptr->sub_range[1];
        break;
        case 6:
            l_m_val = g_rw_inx_demura_cfg_u.min + g_rw_inx_demura_cfg_u.range;
            *(&ptr->N[seq]) = ptr->sub_range[2];
        break;
        case 7:
            l_m_val = ptr->sub_range[2];
            *(&ptr->N[seq]) = ptr->sub_range[0];
        break;
        default:
            l_m_val = g_rw_inx_demura_cfg_u.min + g_rw_inx_demura_cfg_u.range;
            *(&ptr->N[seq]) = g_rw_inx_demura_cfg_u.min;
        break;
    }
//*val->Q
    *(&ptr->Q[seq]) = l_m_val - ptr->N[seq];
    g_inx_last_N_val = ptr->N[seq];
    g_inx_last_Q_val = ptr->Q[seq];
    g_inx_last_mode = mode;
}
#if 0
bool cmp_val(uint8_t src,uint8_t dst)
{
    if(src == dst)
        return TRUE;
    return FALSE;    
}
#endif

#ifdef KEIL_SIM_INX 
static void tcon_lut_transfer_cmp_data(uint8_t i , uint8_t delut_val)
{
    uint16_t l_adr;
    l_adr = (i*5)+g_plane+(5*g_plane_cnt);

    if(delut_val != fitiData[l_adr])
    {
        // P0_7 = 1;
        // P0_7 = 0;
    }    
}
#endif 

/*static void Lut_Transfer_Fiti_Cal_Cks(uint8_t val)
{
    g_demura_cal_cks += val;
}*/

/*static void Lut_Transfer_Inx_cmp_cks(void)
{
    if(g_demura_cal_cks != g_inx_cks)
    //if(g_demura_cal_cks != 0xE6F4)
    {
        P1_2 = 0;
    }
}*/

static void Lut_Transfer_Inx_Cal_Delut(uint8_t cnt , uint8_t cal_cnt)
{
    uint8_t l_N;
    uint8_t l_Q;
    uint8_t l_delut;
    uint8_t i;
    uint8_t l_seq;
    uint8_t l_cur_val;
    uint8_t l_cal_cnt;
    
    l_seq = cnt >> 2;   
    l_cal_cnt = cnt + cal_cnt;
    l_N = g_rw_inx_decode_cfg_u.N[l_seq];
    l_Q = g_rw_inx_decode_cfg_u.Q[l_seq];

    g_w_set_inx_numerator_2 = l_Q;
    g_w_set_inx_add_num = l_N;
    
    for(i = cnt ; i < l_cal_cnt ; i++)
    {
        if(l_Q != 0)
        {
            l_cur_val = g_rw_inx_demura_cfg_u.code_data[i];
            g_w_set_inx_numerator_1 = l_cur_val;
            g_rw_set_inx_hw_cal_en = 1;
            while(g_rw_set_inx_hw_cal_en);
            l_delut = g_r_result1_7_0;
        }
        else
        {
            //l_val = l_N;
            if(l_N >= 64)
            {
                l_delut = l_N - 64;
            }
            else
            {
                l_delut = l_N + 192;
            }
        }

        Lut_Transfer_Fiti_Cal_Cks(l_delut); 
        #ifdef KEIL_SIM_INX 
        //tcon_lut_transfer_cmp_data(i,l_delut); 
        #endif
    }
}

/*static void Lut_Transfer_Delut_Row_Col_Sort(void)
{
    uint8_t l_row , l_col;
    //uint8_t l_val_tmp;
    uint8_t l_offset = 0;

    for(l_row = 0 ; l_row < 16 ; l_row++)
    {
        for(l_col = 0 ; l_col < g_set_plane_Num ; l_col++)
        {
            g_inxtofiti_ary[l_offset++] = g_rw_inx_decode_cfg_u.deLut[l_col][l_row];   
        }
    }
}*/

void tcon_lut_transfer_InxtoFiti(void)
{
    uint32_t l_inx_spi_addr;
    uint32_t l_spi_dl_len;
    uint8_t l_fill_inx_struct_cnt = 0;
    //uint16_t l_size_val = 0;
    uint8_t l_cnt = 0;
    uint8_t l_plane_num = 0;
    uint8_t l_fill_cnt = 0;
    uint16_t l_line = 0;
    uint16_t l_line_max;
    #ifdef KEIL_SIM_INX 
    uint16_t l_rw_offset = 0;
    bool l_last_flg = 0;
    #endif
    
    //l_inx_spi_addr = 0x2491;
    g_demura_cal_cks = 0;
    //l_size_val = sizeof(g_r_spi_inx_demura_cfg_u);
    P1_0 = 0;
    P1_1 = 1;
    P1_2 = 1;

    // l_inx_spi_addr = Inx_spi_chks_addr_cal();
    // //mcu_spi_flash_fast_read((uint8_t *)&l_inx_spi_addr + 1, (uint8_t *)&g_org_cks , 2 , 1);  //g_org_cks
    // Lut_Transfer_Read_Org_Cks(l_inx_spi_addr, (uint8_t *)&g_org_cks , 2);

    Lut_Fill_Hw_Reg_En();
    l_inx_spi_addr = Inx_spi_start_addr_cal();
    
    // -------------- Get Inx read addr offset for every read  ------------------//
    l_spi_dl_len = Inx_spi_dl_len_cal();
    // -------------- -------------- -------------- -------------- ------------------//
    // -------------- Get transfer plane num ------ -------------- ------------------//
    g_set_plane_Num = get_transfer_plane_num();	// plane_num = 5
    // -------------- -------------- -------------- -------------- ------------------//
    // -------------- Get line num - -------------- -------------- ------------------//
    l_line_max = get_line_num();
    // -------------- -------------- -------------- -------------- ------------------//
    // Get Inx Format P1 ~ P5 N1 - 481
    for(l_line = 0 ; l_line < l_line_max ; l_line++)
    {
        P1_1 = 0;
        mcu_spi_flash_fast_read((uint8_t *)&l_inx_spi_addr + 1, (uint8_t *)&g_r_spi_inx_demura_cfg_u[0] , l_spi_dl_len , 1);  
        l_inx_spi_addr += l_spi_dl_len;

#ifdef KEIL_SIM_INX        
        memcpy(&g_r_spi_inx_demura_cfg_u[0],&inxData[l_rw_offset],MAX_FITI_FROMAT_SIZE);      // (155 * 12)
        l_rw_offset += MAX_FITI_FROMAT_SIZE;
#endif

        for(l_fill_inx_struct_cnt = 0 ; l_fill_inx_struct_cnt < MAX_FITI_INX_STRUCT_CNT ; l_fill_inx_struct_cnt ++)     // (496/16) * 5
        {
            if((l_fill_inx_struct_cnt % 5) >= g_set_plane_Num)
            {
                continue;
            }

           // P1_1 = 0; 
            Lut_Transfer_Align_Inx_Format(&g_r_spi_inx_demura_cfg_u[l_fill_inx_struct_cnt].Pn_Nx_16);
            //P1_1 = 1;  
            g_w_set_inx_numerator_1 = g_rw_inx_demura_cfg_u.range;
            g_w_set_inx_numerator_3 = 1;
            g_w_set_inx_add_num = g_rw_inx_demura_cfg_u.min;
            g_w_inx_set_offset = 2;

            for(l_cnt = 0 ; l_cnt < 3 ; l_cnt++)
            {
                // Cal Subrange 1 2 3
                Lut_Transfer_Inx_Cal_SubRange(&g_rw_inx_decode_cfg_u.sub_range[l_cnt] , l_cnt+1);
            }
            // Cal M N Q
            for(l_cnt = 0 ; l_cnt < 4 ; l_cnt++)
            {
                Lut_Transfer_Inx_Cal_M_N_Q(g_rw_inx_demura_cfg_u.mode[l_cnt] , &g_rw_inx_decode_cfg_u,l_cnt);
			}
            // Cal Delut
            g_w_set_inx_numerator_3 = 546;
            g_w_inx_set_offset = 13;
            if(l_fill_inx_struct_cnt > 149)        // last 
            {
                Lut_Transfer_Inx_Cal_Delut(0,1);
                #ifdef KEIL_SIM_INX 
                l_last_flg = 1;
                #endif
            }
            else
            {
                for(l_cnt = 0 ; l_cnt < 16 ; l_cnt+=4)
                {
                    Lut_Transfer_Inx_Cal_Delut(l_cnt,4);   
                    #ifdef KEIL_SIM_INX
                    l_last_flg = 0;  
                    #endif          
                }
            }
            //Lut_Transfer_Cal_Delut(0, 0, l_plane_num); 
            l_plane_num++;
            g_plane++;
            //g_plane_cnt++;
            if(g_plane == g_set_plane_Num)
            {
                //g_plane_cnt++;
                g_plane = 0;
                #ifdef KEIL_SIM_INX 
                if(l_last_flg == 0)
                    g_plane_cnt+=16;
                else
                    g_plane_cnt+=1;
                #endif
            }
            Lut_Set_Plane_Num(g_plane);
            //g_rw_dmu_plane_cfg_u.bits.fw_lut_plane = g_plane;
            /*if(l_plane_num >= g_set_plane_Num)
            {
                l_plane_num = 0;
                Lut_Transfer_Delut_Row_Col_Sort();
            }*/
            tcon_lut_transfer_Inx_Init();
        }
        P1_1 = 1;
    }
    P1_0 = 0;
    // if(!Lut_Transfer_Cmp_Cks(g_r_sys_data_10_u.byte))
    // {
    //     mcu_dmr_dl_reg_lut_dl_fail_set(1);
    //     P1_0 = 1;
    // }
		
    //Lut_Transfer_Inx_cmp_cks();
}

