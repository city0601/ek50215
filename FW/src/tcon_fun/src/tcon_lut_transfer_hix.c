/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: EK50208
		Platform: 8051
		Author: Eric Lee, 2021/03/22
		Description: HIMAX Transfer
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include <stdio.h>
#include <string.h>
#include "mcu_fun_def.h"
#include "mcu_spi.h"
#include "tcon_lut_transfer.h"
// #include "mcu_dmr_dl.h"
#include "tcon_lut_cal_cks.h"
// *******************************************************************
// *******************************************************************
// define
#define HIX_POINT_MAX	130350
// #define DL_HIX_DMA_BASE	0x4C00
// *******************************************************************
// *******************************************************************
// Extern Func
#ifdef KEIL_SIM_HIX 
extern unsigned char xdata HixData[110];
#endif
// *******************************************************************
uint8_t xdata g_test_val = 0;
uint16_t xdata g_hix_dl_len_set_1 = 0;
uint16_t xdata g_hix_dl_len_set_2 = 0;
uint16_t xdata g_cal_cnt = 0;

#if 0
void tcon_lut_transfer_HIXtoFiti(void)
{
	uint32_t l_hix_spi_addr;
	//uint32_t l_point;
	uint16_t l_val;
	uint16_t l_dl_len;
	uint8_t l_plane;
	uint16_t l_align_cnt;
	uint16_t l_line;
	uint16_t l_line_max;
	uint8_t l_out_tmp;
	uint16_t i;

	P1_0 = 0;
	P2_0 = 0;
	P2_1 = 0;
	P2_2 = 0;

	g_demura_cal_cks = 0;
	l_line = 0;
	l_plane = 0;
	g_lut_compress_flg = 0;

	g_hix_dl_len_set_1 = 726;
	g_hix_dl_len_set_2 = 726;

	tcon_lut_transfer_nvt_hix_init();

	if(g_user_mode_flg)
	{
		l_dl_len = g_hix_dl_len_set_1;
	}
	else
	{
		l_dl_len = g_hix_dl_len_set_2;
	}
	//l_align_cnt = l_dl_len << 1;
	
	//g_r_hw_align_len = 72;

	l_align_cnt = g_r_hw_align_len;

	//Lut_Transfer_Read_Org_Cks(0x99 ,(uint8_t xdata *)&g_org_cks, 2);
	//Lut_Transfer_Read_Org_Cks(0x99 ,(uint8_t xdata *)&g_org_cks, 2);

	l_hix_spi_addr = Hix_spi_addr_cal();
	P1_1 = 1;
    // -------------- -------------- -------------- -------------- ------------------//
    // -------------- Get transfer plane num ------ -------------- ------------------//
    g_set_plane_Num = get_transfer_plane_num(); // plane_num = 3
    // -------------- -------------- -------------- -------------- ------------------//
    // -------------- Get line num - -------------- -------------- ------------------//
    l_line_max = get_line_num();
    // -------------- -------------- -------------- -------------- ------------------//
	// Lut_Transfer_Hw_Cal_Cks_En(0);
    // Lut_Transfer_Hw_Cal_Cks_Mode(CKS_HIX_MODE);
    // Lut_Transfer_Hw_Cal_Cks_En(1);
	// 	while(l_line < l_line_max) // 271
	// 		{
	// 		mcu_spi_flash_fast_read((uint8_t *)&l_hix_spi_addr + 1, (uint8_t xdata*)(DL_HIX_DMA_BASE) , l_dl_len, 1);
	// 			l_hix_spi_addr += l_dl_len;
	// 		l_plane ++;
	// 		if(l_plane == g_set_plane_Num)
	// 		{
	// 			l_plane = 0;
	// 			l_line ++;
	// 		}
	// 	}
    // if(!Lut_Transfer_Cmp_Cks(g_r_sys_data_10_u.byte))
	// {
	// 	mcu_dmr_dl_reg_lut_dl_fail_set(1);
	// 	P1_0 = 1;
	// 	return;
	// }

	Lut_Transfer_Hw_Cal_Cks_En(0);
	l_hix_spi_addr = Hix_spi_addr_cal();
	l_line = 0;
	l_plane = 0;

	Lut_Fill_Hw_Reg_En();
	while(l_line < l_line_max) // 271
	{
		// l_dl_len - 4;  484 only get 481
		// mcu_spi_flash_fast_read((uint8_t *)&l_hix_spi_addr + 1, (uint8_t xdata*)(DL_HIX_DMA_BASE) , l_dl_len-4 , 1);
		mcu_spi_flash_fast_read((uint8_t *)&l_hix_spi_addr + 1, (uint8_t xdata*)(&g_dmr_trans_cfg_buf[0]) , l_dl_len-4 , 1);

		l_hix_spi_addr += l_dl_len;
		if(l_plane == 0)
			P2_0 = 1;
		else if(l_plane == 1)
			P2_1 = 1;
		else 
			P2_2 = 1;	
		//l_point = 0;
		for(i = 0; i < g_r_hw_align_len ; i+=2)
		{
			// l_val = *(volatile uint8_t xdata *)(DL_HIX_DMA_BASE + i + 1);
			l_val = *(volatile uint8_t xdata *)((uint16_t)(&g_dmr_trans_cfg_buf[0]) + i + 1);
			l_val <<= 8;
			// l_val |= *(volatile uint8_t xdata *)(DL_HIX_DMA_BASE + i);
			l_val |= *(volatile uint8_t xdata *)((&g_dmr_trans_cfg_buf[0]) + i);

			if(g_lut_compress_flg)
			{
				// compress
				l_out_tmp = tcon_lut_transfer_12b_to_8b(l_plane,l_val);
			}
			else
			{
				l_out_tmp = l_val >> 2;
			}
			g_cal_cnt ++;
			g_test_val = l_out_tmp;
			Lut_Transfer_Fiti_Cal_Cks(l_out_tmp);
		}
		P2_0 = 0;
		P2_1 = 0;
		P2_2 = 0;
		l_plane ++;
		if(l_plane == g_set_plane_Num)
		{
			l_plane = 0;
			l_line ++;
		}
		Lut_Set_Plane_Num(l_plane);
	}
	//if(!Lut_Transfer_Cmp_Cks(g_r_sys_data_10_u.byte))
		//P1_0 = 1;
	P1_1 = 0;
}
#endif

void tcon_lut_transfer_HIXtoFiti(void)
{
	uint8_t l_plane, l_read_cnt;
	uint16_t l_line_len, l_dl_len, l_line, l_line_max;
	uint32_t l_hix_spi_addr_ofset, l_hix_spi_addr;

	l_line_len = 726;

    // -------------- -------------- -------------- -------------- ------------------//
    // -------------- Get transfer plane num ------ -------------- ------------------//
    g_set_plane_Num = get_transfer_plane_num(); // plane_num = 3
    // -------------- -------------- -------------- -------------- ------------------//
    // -------------- Get line num - -------------- -------------- ------------------//
    l_line_max = get_line_num();
    // -------------- -------------- -------------- -------------- ------------------//

	l_hix_spi_addr_ofset = Hix_spi_addr_cal();
	l_line = 0;

	Lut_Fill_Hw_Reg_En();
	while(l_line < l_line_max) // 271
	{
		l_dl_len = 18;
		// l_dl_len - 4;  484 only get 481 -> 726 only download 722 byte
		for (l_read_cnt = 0; l_read_cnt < 41 ; l_read_cnt++) // 722 = 18*40 + 2
		{
			l_hix_spi_addr = l_hix_spi_addr_ofset;

			if(l_read_cnt == 40)
				l_dl_len = 2;

			for(l_plane = 0; l_plane < g_set_plane_Num; l_plane++)
			{
				if(tcon_lut_transfer_chk_hw_stop())
                {
					Lut_Set_Plane_Num(l_plane + 1);

					mcu_spi_flash_fast_read((uint8_t *)&l_hix_spi_addr + 1, (uint8_t xdata*)(&g_dmr_trans_cfg_buf[0]) , l_dl_len , 1);
				}

				l_hix_spi_addr += l_line_len;
			}
			l_hix_spi_addr_ofset += l_dl_len;
		}

		l_line ++;
		l_hix_spi_addr_ofset += 1457; // 1457 = 726*3 - 721
	}
}