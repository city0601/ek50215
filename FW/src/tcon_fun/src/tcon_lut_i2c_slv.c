/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: tcon
		Platform: 8051
		Author: Eric Lee, 2021/03/19
		Description: 
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"

#ifdef LUT_SRW_FUN
#include "reg_include.h"
#include "mcu_dma.h"
#include "tcon_global_include.h"
// *******************************************************************
// *******************************************************************
uint16_t xdata g_lut_i2c_read_reg_ptr;   // store real address
volatile union lut_i2c_slv_flag_u xdata g_lut_i2c_slv_flag;

//********************************************************************/
// Function
//********************************************************************/ 
#if 0
static void tcon_lut_i2c_slv_lxl_read_reg(uint8_t offset)
{
	uint16_t i;
	for(i = 0; i < g_lxl_read_num; i++)
	{
        g_lxl_data[i + offset] = tcon_i2c_read_reg(0xA000 + i);
	}
}

void tcon_lut_i2c_slv_lxl_init(void)
{
	uint16_t l_lxl_addr = 0xA000;
	uint8_t l_lxl_gain_6b_val;
	uint8_t l_lxl_od_en_ori = g_r_dataflow_3801_cfg.bits.r_lxl_od_en;
	uint8_t l_lxl_x = g_r_lxlod_5067_cfg.bits.r_h_block_num;
	uint8_t l_lxl_y = g_r_lxlod_5068_cfg.bits.r_v_block_num;
	uint8_t l_offset = l_lxl_x << 1;

	P0_7 = 1;
	// For 6 bit lxl gain table
	l_lxl_gain_6b_val = g_r_lxlod_5067_cfg.bits.r_lxl_gain_6b_en;
	g_r_lxlod_5067_cfg.bits.r_lxl_gain_6b_en = 0;

	// Prevent enable r_lxl_od_en
	g_r_dataflow_3801_cfg.bits.r_lxl_od_en = 0;

	// Calculate LXL read range
	g_lxl_total_num = (l_lxl_x * l_lxl_y ) << 1 ;
	g_lxl_read_num = g_lxl_total_num - l_offset;

	tcon_lut_i2c_slv_lxl_read_reg(0);
	
	g_r_lxlod_5060_cfg.bits.lxl_block_shift_en = 1;

	tcon_lut_i2c_slv_lxl_read_reg(l_offset);

	g_r_dataflow_3801_cfg.bits.r_lxl_od_en = l_lxl_od_en_ori;
	g_r_lxlod_5060_cfg.bits.lxl_block_shift_en = 0;

	g_r_lxlod_5067_cfg.bits.r_lxl_gain_6b_en = l_lxl_gain_6b_val;
	P0_7 = 0;
}

static void tcon_lut_i2c_slv_lxl_write_sram(uint8_t index_l, uint8_t index_h, uint8_t lxl_data)
{
	uint16_t l_index;

	l_index = (uint16_t)((index_h << 8) | index_l);
	g_lxl_data[l_index] = lxl_data;
}

static void tcon_lut_i2c_slv_lxl_write_reg(void)
{
	uint16_t l_lxl_addr = 0xA000;
	uint8_t l_lxl_gain_6b_val;
    uint16_t i ;

	// For 6 bit lxl gain table
	l_lxl_gain_6b_val = g_r_lxlod_5067_cfg.bits.r_lxl_gain_6b_en;
	g_r_lxlod_5067_cfg.bits.r_lxl_gain_6b_en = 0;

	for(i = 0; i < g_lxl_total_num; i++)
	{
        tcon_i2c_write_reg((0xA000 + i), g_lxl_data[i]);
	}
	
	g_r_lxlod_5067_cfg.bits.r_lxl_gain_6b_en = l_lxl_gain_6b_val;
}
#endif

#ifdef LUT_SRW_FUN
static void LUT_Write_Wrdt(uint8_t *wr_buf, uint8_t wr_len)
{
   uint8_t i = 0;
	uint8_t *ptr = &g_rw_srw_0005h_lut_wrdt;
	for(i = 0; i < wr_len; i++)
	{
        *(ptr + i) = *(wr_buf + i);
	}
}


void Lut_Decide_IP(uint8_t *cmd_buf)
{
	g_rw_srw_0001h_lut_ip_sel = cmd_buf[0];
}

void Lut_Decide_Sram(uint8_t *cmd_buf)
{
	g_rw_srw_0002h_lut_sram_sel = cmd_buf[0];
}

void Lut_Sram_Byte_Mode(uint8_t *cmd_buf)
{
	g_rw_srw_0003h_lut_addr = cmd_buf[5];
	if(cmd_buf[3] == 2)
		g_rw_srw_0004h_lut_addr = cmd_buf[6];
	else if(cmd_buf[3] == 1)
		g_rw_srw_0004h_lut_addr = 0x00;
}

void Lut_Access_Reg_Ptr(void)
{
	//g_lut_i2c_read_reg_ptr = (uint16_t)&g_rw_srw_0003h_lut_addr;
	g_lut_i2c_read_reg_ptr = (uint16_t)&g_rw_srw_0009h_lut_rddt_lth;			// ptr access read data [7:0]
}

void Lut_Read_Handler(void)
{
	Lut_Access_Reg_Ptr();
	// check update mechanism
	mcu_dma_set_ch0_adr(g_lut_i2c_read_reg_ptr);

	if(!g_rw_srw_000Dh.bits.r_lut_srw_update_sel)
	{
	    g_rw_srw_000Eh.bits.r_lut_srw_rd_en = 1;
		g_lut_i2c_read_reg_ptr ++;
	}
	else   
	    g_lut_i2c_slv_flag.bits.rd_vsync_update = 1;
}

void Lut_Write_Handler(uint8_t *cmd_buf,bool sram_mode)
{
	uint8_t l_ofst_adr;

	if(sram_mode == 0)
		l_ofst_adr = 6;
	else
		l_ofst_adr = 7;	
	LUT_Write_Wrdt(&cmd_buf[l_ofst_adr], cmd_buf[4]);

	// check update mechanism
	if(!g_rw_srw_000Dh.bits.r_lut_srw_update_sel)
	    g_rw_srw_000Eh.bits.r_lut_srw_wren = 1;
	else   
	    g_lut_i2c_slv_flag.bits.wr_vsync_update = 1;
}

void tcon_i2c_over_lut_start(uint8_t *cmd_buf, uint16_t buf_len)
{
	bool l_sram_mode;
	if (mcu_i2c2spi_cmd_init)
	{
	    //if(buf_len > 11)
		if(buf_len > 23)     // write byte extend to 16byte
	        return;
		// decide IP
		Lut_Decide_IP(&cmd_buf[1]);
		// decide IP
		//g_rw_srw_0001h_lut_ip_sel = cmd_buf[1];
	    // decide which SRAM
		Lut_Decide_Sram(&cmd_buf[2]);
		//g_rw_srw_0002h_lut_sram_sel = cmd_buf[2];
		//if((Lut_Sram_Byte_Mode(&cmd_buf[3])
		if(cmd_buf[3] == 0)	
			return;
		if(cmd_buf[3] == 1)
			l_sram_mode = 0;
		else if(cmd_buf[3] == 2)
			l_sram_mode = 1;
			
		Lut_Sram_Byte_Mode(&cmd_buf[0]);
		#if 0
	    if(cmd_buf[3] == 1)        // sram addr one byte mode
		{
			Lut_Sram_Byte_Mode(&cmd_buf[5],0);
			//g_rw_srw_0003h_lut_addr = cmd_buf[5];
			//g_rw_srw_0004h_lut_addr = 0x00;
			l_sram_mode = 0;
		}
		else if(cmd_buf[3] == 2)
		{
	    	//g_rw_srw_0003h_lut_addr = cmd_buf[5];
			//g_rw_srw_0004h_lut_addr = cmd_buf[6];
			Lut_Sram_Byte_Mode(&cmd_buf[5],1);
			l_sram_mode = 1;
		}
		else
			return;
		#endif

    	if(cmd_buf[4] == 0)     // read lut
		{
			//g_lut_i2c_read_reg_ptr = (uint16_t)&g_rw_srw_0003h_lut_addr;
			Lut_Read_Handler();	
#if 0
			Lut_Access_Reg_Ptr();
		    // check update mechanism
		    mcu_dma_set_ch0_adr(g_lut_i2c_read_reg_ptr);


		    if(!g_rw_srw_000Dh.bits.r_lut_srw_update_sel)
		    {
	    	    g_rw_srw_000Eh.bits.r_lut_srw_rd_en = 1;

				//mcu_i2c2spi_reply_data = tcon_i2c_read_reg(g_lut_i2c_read_reg_ptr);
				//g_i2c2spi_read_buf[l_adr++] = tcon_i2c_read_reg(g_lut_i2c_read_reg_ptr);
        	    g_lut_i2c_read_reg_ptr ++;
		    }
		    else   
	    	    g_lut_i2c_slv_flag.bits.rd_vsync_update = 1;
#endif
	    }
	    else                    // write lut
	    {
			Lut_Write_Handler(&cmd_buf[0],l_sram_mode);
#if 0
		    if(l_sram_mode == 0)
		    {
				LUT_Write_Wrdt(&cmd_buf[6], cmd_buf[4]);
		    }
		    else
		    {
			    LUT_Write_Wrdt(&cmd_buf[7], cmd_buf[4]);
		    }
		    // check update mechanism
		    if(!g_rw_srw_000Dh.bits.r_lut_srw_update_sel)
	    	    g_rw_srw_000Eh.bits.r_lut_srw_wren = 1;
		    else   
	    	    g_lut_i2c_slv_flag.bits.wr_vsync_update = 1;
#endif
	    }
	}
	/*else if(mcu_i2c2spi_cmd_doing)
    {
    
	}*/
}

#if 0
void Lut_I2c_Slv_LxL_WriteReg_Handler(void)
{
	if(g_lxl_write_flag && (!g_lut_i2c_slv_flag.bits.wr_vsync_update))
	{
		//P1_1 = 1;
		tcon_lut_i2c_slv_lxl_write_reg();
		g_lxl_write_flag = 0;
		//P1_1 = 0;
	}
}
#endif

void Lut_I2c_Slv_RW_Vsync_Handler(void)
{
	if(g_lut_i2c_slv_flag.bits.wr_vsync_update)
	{
		g_rw_srw_000Eh.bits.r_lut_srw_wren = 1;
		g_lut_i2c_slv_flag.bits.wr_vsync_update = 0;
	}

	if(g_lut_i2c_slv_flag.bits.rd_vsync_update)
	{
		g_rw_srw_000Eh.bits.r_lut_srw_rd_en = 1;
        g_lut_i2c_slv_flag.bits.rd_vsync_update = 0;
	}
}
#endif

#endif
