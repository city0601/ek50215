#ifndef TCON_ISP_H_
#define TCON_ISP_H_

#include "mcu_global_vars.h"
// ISP
extern bool g_process_isp_flg;
extern uint8_t xdata g_org_ip_dbgo_sel;
extern uint8_t xdata g_org_p2p_dbgo_sel;

extern void tcon_isp_proc(void);
extern void tcon_isp_check_event(void);
#endif