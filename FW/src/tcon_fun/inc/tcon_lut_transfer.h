#ifndef TCON_LUT_TRANSFER_H_
#define TCON_LUT_TRANSFER_H_

#ifdef DEMURA_CHK_FUN
extern bool dmr_flash_reload_flag;

extern void demrua_chk_handler(void);
extern void demura_id_floating_set(void);
extern void demrua_vsync_chk_reload_handler(void);

#ifdef CASCADE_DEMURA_FUN
extern bool g_dmr_cascade_slv_waiting_rx_flag;
extern void demura_cascade_slave_rx_handler(void);
#endif

#endif

#ifdef DEMURA_TRANS_FUN
#include "demura_load.h"
#include "demura.h"

#define CKS_INX_MODE    0
#define CKS_SEC_MODE    0
#define CKS_HIX_MODE    1
#define CKS_NVT_MODE    2

// Transfer Format
#define AUTO_DETECT_MODE    0
#define FITI_FORMAT         1
#define INX_FORMAT          2
#define SEC_FORMAT          3
#define NVT_FORMAT          4
#define HIX_FORMAT          5
#define VD_FORMAT           6

extern bool g_hw_trans_fw_cks_flg;
extern bool g_lut_compress_flg;
extern bool dmr_flash_reload_flag;

extern uint8_t xdata g_set_plane_Num;
extern uint8_t xdata g_transfer_mode;
extern uint8_t xdata g_user_mode_flg;

extern uint16_t xdata g_abs_lut_dt_max_lx[8];
extern uint16_t xdata g_demura_cal_cks;
extern uint16_t xdata g_org_cks;
extern uint16_t xdata g_r_hw_align_len;

extern uint8_t xdata g_dmr_trans_cfg_buf[722];

//extern void tcon_lut_transfer_Inx_Init(void);
extern void tcon_lut_transfer_nvt_hix_init(void);
extern void tcon_lut_transfer_nvt_hix_get_max_val(uint8_t layer , uint16_t cur_val);
extern uint8_t tcon_lut_transfer_12b_to_8b(uint8_t layer,uint16_t lut_dt);
extern void tcon_lut_transfer_handler(void);
extern void Lut_Transfer_Fiti_Cal_Cks(uint8_t val);
extern void Lut_Transfer_Hw_Cal_Cks_Mode(uint8_t mode);
extern void Lut_Transfer_Hw_Cal_Cks_En(bool en);
extern void Lut_Transfer_Hw_Cal_Crc_Cks_Rst(void);
extern void Lut_Transfer_Read_Org_Cks(uint32_t addr , uint8_t* buf ,uint8_t len);
extern bool Lut_Transfer_Cmp_Cks(uint16_t hw_cal_org_cks);
extern void Lut_Fill_Hw_Reg_En(void);
extern void Lut_Set_Plane_Num(uint8_t val);
extern uint8_t get_transfer_plane_num(void);
extern uint16_t get_line_num(void);
extern void tcon_lut_transfer_set_spi_shift(uint8_t bytes_num , uint8_t bit_shift , uint8_t shift_mode);
extern bool tcon_lut_transfer_chk_hw_stop(void);
#endif
#endif
