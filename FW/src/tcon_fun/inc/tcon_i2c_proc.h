#ifndef __TCON_I2C_PROC_H__
#define __TCON_I2C_PROC_H__

typedef void (*mcu_i2c2spi_func_ptr)();

typedef struct {
    mcu_i2c2spi_func_ptr cmd_func;
} mcu_i2ccmd_struct;


uint8_t tcon_i2c_read_reg(uint16_t reg_ptr);
void tcon_i2c_proc_init(void);
void tcon_i2c_over_spi_start(uint8_t *cmd_buf, uint16_t buf_len);
//uint8_t tcon_i2c_over_spi_data(void);
void tcon_i2c_over_spi_done(void);
void tcon_i2c_over_spi_proc(void);
void tcon_i2c_write_reg(uint16_t reg_ptr, uint8_t reg_val);
// extern uint8_t mcu_fw_ver[];
// extern uint8_t mcu_fw_ver_len;
extern void tcon_i2c_dlg_isr(void);
extern void tcon_vrr_onoff_func(uint8_t i2c_vrr_cmd);
extern void tcon_i2c_cmd_dlg_onoff_func(uint8_t i2c_dlg_cmd);
extern void tcon_i2c_dlg_proc(void);
extern void tcon_vsync_update_vrr_dlg_proc(void);
//extern void tcon_clear_hw_od_en(void);

extern bool g_dlg_update_flg;
extern bool g_vrr_update_flg;
extern uint8_t xdata g_dlg_i2c_data_buf;
extern uint8_t xdata g_vrr_i2c_data_buf;
#endif
