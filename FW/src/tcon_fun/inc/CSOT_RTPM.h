#ifndef _CSOT_RTPM_H_
#define _CSOT_RTPM_H_
#ifdef CSOT_FUN
#define PMIC_READ_ALL_BANK 0xF0
#define PMIC_READ_ALL_LEN  0xFF

#define BANK_A					0x00
#define BANK_B					0x01
#define PMIC_HDR				0x02
#define GMA						0x03

union rw_csot_fw_sta
{
    uint8_t byte;
    struct
    {
        uint8_t busy_bit                            : 1;
        uint8_t crc_fail_tcon_exi2c                 : 1;
        uint8_t flash_connection_status             : 1;
        uint8_t crc_fail_tcon_pmic_flash            : 1;
        uint8_t i2c_bypass_mode_sta                 : 1;
        uint8_t i2c_to_spi_mode_valid               : 1;
        uint8_t reserved                            : 2; 
    }bits;
};

extern volatile union rw_csot_fw_sta xdata g_rw_csot_fw_sta;
//extern uint16_t l_i2c_wr_dly_time;
extern uint16_t xdata array_i2c_wr_dly_time[4];

bool CSOT_RTPM_Cal_Dl_Crc_Pass(void);
bool CSOT_RTPM_Check_Dl_En(void);
void CSOT_RTPM_Handler(void);
void CSOT_RTPM_Set_Fw_Sta_Busy(bool en);
void CSOT_RTPM_Set_Crc_Fail_Tcon_Exi2c(bool en);
void CSOT_RTPM_Set_Flash_Conn_Sta(bool en);
void CSOT_RTPM_Set_Fw_Sta_Crc_Fail_Tcon_Pmic_Flash(bool en);
void CSOT_RTPM_Set_I2c_Bypass_Sta(bool en);
void CSOT_RTPM_Set_I2c_To_Spi_Valid(bool en);
void CSOT_RTPM_Clr_Pmic_En(void);
//void CSOT_RTPM_Entry_Dl_Tcon_To_Sram(uint8_t mem_len,uint8_t l_flash_path , bool l_dl_dhr);
void CSOT_RTPM_Storage_Crc(void);
void CSOT_RTPM_Upload_PMIC_Code_To_PMIC(void);
void CSOT_RTPM_Set_PMIC_GPIO(uint8_t pin);
void CSOT_RTPM_Init(void);
uint8_t CSOT_RTPM_Ret_Fw_Sta_Crc_Fail_Tcon_Pmic_Flash(void);
bool CSOT_RTPM_Tcon_Check_Common_Cfg(void);
void CSOT_RTPM_Tcon_DL_CBoard(uint8_t mem_len);
void CSOT_RTPM_Tcon_DL_XBoard(uint8_t mem_len);
bool CSOT_ReadPMIC_Code_From_PMIC_Handler(uint8_t offset, uint8_t len);
bool CSOT_Decide_Flash_Path(void);
uint8_t CSOT_Get_BANK(uint8_t offset);

uint32_t CSOT_Get_Flash_Adr(uint8_t bank,bool cs);
uint8_t CSOT_Get_Offset(uint8_t offset);
uint8_t CSOT_Get_Len(uint8_t bank);
uint8_t *CSOT_Get_PMIC_DataBuf(uint8_t bank);
bool CSOT_RTPM_Cal_PMIC_Status(uint16_t CRC_Code_H, uint16_t CRC_Code_L);
#ifdef FLASH_CODE
void CSOT_RTPM_Update_Addr(void);
#endif
//uint8_t *CSOT_Get_PMIC_Adr(uint8_t bank,bool cs);
//void CSOT_RTPM_Tcon_Check_Common_Cfg(uint8_t mem_len);
#endif
#endif