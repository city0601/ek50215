#ifndef __TCON_CASCADE_H_
#define __TCON_CASCADE_H_
#include "mcu_type.h"

#define CASCADE_DATA_ID_MSTR_DL_STATUS      0x00
#define CASCADE_DATA_ID_MSTR_EDP_STATUS     0x02
#define CASCADE_DATA_ID_MSTR_MCU_STATUS     0x04
#define CASCADE_DATA_ID_MSTR_DEMURA_STATUS  0x06

#define CASCADE_DATA_ID_SLV_DL_STATUS       0x81
#define CASCADE_DATA_ID_SLV_SEND_EDP_STATUS 0x82
#define CASCADE_DATA_ID_SLV_EDP_STATUS      0x83
#define CASCADE_DATA_ID_SLV_MCU_STATUS      0x85

#define MASTER_TYPE 0
#define SLAVE_TYPE 1  

typedef struct {
    uint8_t id;
    uint8_t val[3];
} rx_cscd_struct;

extern bool g_master_flg;
extern bool g_cascade_tx_flg;

extern rx_cscd_struct xdata g_rx_cscd;
extern uint8_t xdata g_cascade_flag_rcv_rx_int;

extern void tcon_tx_cascade(uint8_t *buf);
extern void tcon_rx_cascade(void);
extern void tcon_enable_cascade_interrupt(void);
#endif