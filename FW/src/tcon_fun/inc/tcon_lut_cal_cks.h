#ifndef __TCON_LUT_CAL_CKS_H__
#define __TCON_LUT_CAL_CKS_H__

extern void tcon_lut_cal_cks_FwHelpHWFun(void);
extern uint32_t Inx_spi_start_addr_cal(void);
extern uint32_t lut_spi_chks_addr_cal(void);
extern uint32_t lut_cks_total_dl_len_cal(void);
extern uint32_t Inx_spi_dl_len_cal(void);
extern uint32_t Nvt_dl_total_len_cal(void);
extern uint32_t Nvt_spi_addr_cal(void);
extern uint32_t Hix_spi_addr_cal(void);
extern void Sec_chks_mode_sel(void);
extern uint32_t Sec_spi_addr_cal(void);
extern uint32_t Vd_spi_addr_cal(void);
extern uint32_t Lut_spi_dl_len_cal(void);
#endif