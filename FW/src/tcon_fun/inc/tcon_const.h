#ifndef __TCON_CONST_H__
#define __TCON_CONST_H__

#define TCON_DL_SZ_EDID (256)
#define TCON_DL_SZ_TCON (2000)
#define TCON_DL_SZ_SDRV (54)
#define TCON_DL_SZ_LUT  (900)

#endif
