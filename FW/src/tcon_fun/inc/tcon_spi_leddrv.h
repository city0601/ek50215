#ifndef __TCON_SPI_LEDDRV_H__
#define __TCON_SPI_LEDDRV_H__

//#include "tcon_global_vars.h"
#include "mcu_reg51.h"
//#define VSYNC_SET(val)                (val)?(LED_VSYNC = 1):(LED_VSYNC = 0)
#define VSYNC_SET(val)                (val)?(P0_7 = 1):(P0_7 = 0)

// LED INDEX
#define MBI6334_INDEX                  0
#define MBI6322_INDEX                  1
#define NT50589_INDEX                  2


typedef struct {
    uint8_t cmd_buf0; uint8_t cmd_buf1; 
    uint8_t cmd_buf2; uint8_t cmd_buf3;
    uint8_t cmd_buf4; uint8_t cmd_buf5;
    uint8_t cmd_buf6; uint8_t cmd_buf7;
    uint8_t cmd_byte;
}led_init_cmd_t;

typedef struct { 
    uint16_t flash_size;
    uint32_t mask_flash_addr;  uint16_t mask_data_size;
    uint32_t cfg_flash_addr;   uint16_t cfg_data_size;
    uint32_t dot_flash_addr;   uint16_t dot_data_size;
    uint32_t bc_flash_addr;    uint16_t bc_data_size;
    uint32_t IDAC_flash_addr;  uint16_t IDAC_data_size;
}led_drv_flash_t;

void tcon_spi_leddrv_init_handler(void);
extern void tcon_spi_leddrv_update_handler(void);
extern void tcon_led_proc_display_onoff(uint8_t dp_on);

extern uint8_t xdata g_led_last_scan;
extern uint8_t xdata g_led_vsync_hwpin_en;
extern uint8_t xdata g_led_drv_num;
extern uint8_t xdata g_block_step;
extern uint8_t xdata g_led_dchain_num;
extern uint8_t xdata g_led_spi_chksm_en;
extern uint8_t xdata g_led_driver_mode;

extern bool g_wait_vsync_flag;
extern bool g_proc_led_vsync_flg;
extern bool g_mcu_led_spi_read_done_flg;
extern bool g_proc_i2c2led_vsync_flg;

extern bool g_led_update_flag;
extern uint8_t xdata g_led_update_int_cntr;
extern uint8_t xdata g_led_last_scan_cntr;

extern bool g_proc_led_on_flg;

#endif
