#ifndef __TCON_LUT_I2C_SLV_H__
#define __TCON_LUT_I2C_SLV_H__

#define LUT_IP_GAMMA_IDX   0x01
#define LUT_IP_FRC_IDX     0x02
#define LUT_IP_OD_IDX      0x03
#define LUT_IP_LXL_IDX     0x04

union lut_i2c_slv_flag_u
{
    uint8_t byte;
    struct 
    {
        uint8_t   wr_vsync_update   : 1;
        uint8_t   rd_vsync_update   : 1;
        uint8_t   reserved          : 6;
    }bits;
};

extern volatile union lut_i2c_slv_flag_u xdata g_lut_i2c_slv_flag;

extern uint16_t xdata g_lut_i2c_read_reg_ptr;

extern void tcon_i2c_over_lut_start(uint8_t *cmd_buf, uint16_t buf_len);
extern void Lut_I2c_Slv_RW_Vsync_Handler(void);
//extern void Lut_I2c_Slv_LxL_WriteReg_Handler(void);
//extern void tcon_lut_i2c_slv_lxl_init(void);
#endif
