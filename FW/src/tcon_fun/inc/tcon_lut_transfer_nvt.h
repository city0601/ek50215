#ifndef TCON_LUT_TRANSFER_NVT_H_
#define TCON_LUT_TRANSFER_NVT_H_

#include "mcu_global_vars.h"

extern void tcon_lut_transfer_NVTtoFiti(void);

// NVT Transfer
// union r_nvt_format
// {
//     uint16_t u16;
//     struct 
//     {
//         uint16_t val_lsb                  : 4;
//         uint16_t reserved                 : 4;
//         uint16_t val_msb                  : 8;
//     }bits;    
// };

// extern volatile union r_nvt_format  xdata g_r_nvt_format_u;
//extern uint8_t xdata g_nvt_dl_buf;


#endif