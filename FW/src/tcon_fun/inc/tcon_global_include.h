#ifndef TCON_GLOBAL_INCLUDE_H_
#define TCON_GLOBAL_INCLUDE_H_

#include "mcu_spi.h"
#include "mcu_i2c.h"
#include "tcon_i2c_proc.h"


#include "tcon_misc.h"
#ifdef LUT_SRW_FUN
	#include "tcon_lut_i2c_slv.h"
#endif
#endif
