#ifndef _CSOT_I2C_SLV_H_
#define _CSOT_I2C_SLV_H_
#ifdef CSOT_FUN

//#define CSOT_SPI_CMD_SUP
#define CSOT_BYPASS_MODE_SUP

// CSOT EXT_I2C_MST to TCON_I2C_SLV CMD Definition
#define CSOT_CMD1_READ_MCU_FW_STATUS        0x05
#define CSOT_CMD5_ENTER_I2C_BYPASS_MODE     0XF0
#define CSOT_CMD6_EXIT_I2C_BYPASS_MODE      0X0F
#define CSOT_CMD7_READ_PMIC_CODE            0X67
#define CSOT_CMD8_WRITE_PMIC_CODE           0X98
#define CSOT_CMD_Enter_EXIT_DBG_MODE        0xF2

#define CSOT_CMD4_READ_TCONID               0x11
#define CSOT_CMD10_READ_CFLASH_JEDECID      0x8F
#define CSOT_CMD10_READ_XFLASH_JEDECID      0x70
#define CSOT_CMD12_READ_FROM_CFLASH         0xF4
#define CSOT_CMD12_READ_FROM_XFLASH         0x0B
#define CSOT_CMD13_WRITE_FROM_CFLASH        0xFD
#define CSOT_CMD13_WRITE_FROM_XFLASH        0x02
#define CSOT_CMD14_WRITE_TO_CFLASH          0xFD
#define CSOT_CMD15_READ_FROM_FLASH          0x99
#define CSOT_CMD16_WRITE_FROM_PMIC_TO_FLASH 0x66

#define CSOT_CMD_READ_GAMMA_CODE            0x22
#define CSOT_CMD_WRITE_GAMMA_CODE           0xDD
#define CSOT_CMD_RELOAD_EEPROM              0xCC

#define CSOT_CMD17_READ_VCOM_FROM_XFLASH    0x71
#define CSOT_CMD18_WRITE_VCOM_FROM_XFLASH   0x72
#define CSOT_CMD19_READ_VCOM_FROM_CFLASH    0x73
#define CSOT_CMD20_WRITE_VCOM_FROM_CFLASH   0x74


// Other Definition
//#define FLASH_CS_XBOARD                     1
//#define FLASH_CS_CBOARD                     1
//#define FLASH_CS_CBOARD                     0      // EK50303B has just one path for cs

//#ifdef CSOT_SPI_CMD_SUP
    //#define FLASH_PMIC_STRTADDR                 0x00001000
//#endif
typedef void (*mcu_i2c2csot_func_ptr)();

typedef struct
{
    uint8_t i2c2csot_id;
    uint16_t cmd_len;
    mcu_i2c2csot_func_ptr cmd_func;
} mcu_i2c2csot_cmd_struct;

#ifdef CSOT_SPI_CMD_SUP
typedef struct
{
    uint8_t *sec_ptr;
    uint8_t *wr_ptr;
    uint8_t w_len;
    uint8_t cs;
    uint8_t g_mcu_spi_rw_en;
    uint8_t g_CSOT_spi_idx;

}mcu_spi_rw_sta;
#endif

//extern mcu_i2c2csot_cmd_struct xdata mcu_i2c2csot_cmd_table[];

//extern bool g_csot_read_reg_flag;
//extern uint8_t xdata g_csot_i2ccmd_num;
//extern uint16_t xdata g_csot_read_reg_ptr;
//extern void CSOT_I2c_Slv_Init(void);
extern void tcon_CSOT_I2c_Slv_Init(void);
//extern void CSOT_I2c_Slv_Set_I2c_Slv_Adr(void);
//extern uint8_t CSOT_I2c_Slv_Read_Data(void);
//extern void CSOT_I2c_Slv_Rx_Fill_RTPM_Buf(void);
extern void CSOT_Write_PMIC_Code(void);
extern void CSOT_Decide_PMIC_I2C_Path(bool path);
extern bool CSOT_Spi_Check_Flash_ID(uint8_t *rd_ptr);

// CSOT CMD List
extern void CSOT_I2c_Slv_Read_FW_Status(void);                      // CMD : 0x05
extern void CSOT_I2c_Slv_Enter_Bypass_Mode(void);                   // CMD : 0xF0
extern void CSOT_I2c_Slv_Exit_Bypass_Mode(void);                    // CMD : 0x0F
extern void CSOT_I2c_Slv_I2c_Mst_Read_PMIC(void);                   // CMD : 0x67
extern void CSOT_I2c_Slv_I2c_Mst_Write_PMIC(void);                  // CMD : 0x98
extern void CSOT_I2c_Slv_Det_I2ctoSPI_Mode(void);                   // CMD : 0xF2
extern void CSOT_I2c_Slv_Read_Tcon_Vendor_Tcon_ID(void);            // CMD : 0x11
extern void CSOT_Spi_Read_Flash_Jedec_ID(void);                     // CMD : 0x70 / 0x8F
extern void CSOT_I2c_Slv_Spi_Mst_Read_PMICFromFlash(void);          // CMD : 0x0B / 0xF4
extern void CSOT_I2c_Slv_Spi_Mst_Write_PMICCodeToFlash(void);       // CMD : 0x02 / 0xFD
extern void CSOT_I2c_Slv_Spi_Mst_Read_PMICCodeFromFlash(void);      // CMD : 0x99
extern void CSOT_I2c_Slv_I2c_Mst_Read_Spi_Write(void);              // CMD : 0x66
extern void CSOT_Write_GammaCodeToTconReg(void);		            // CMD : 0xDD
extern void CSOT_Read_GammaCodeToTconReg(void);		                // CMD : 0x22
extern void CSOT_DemuraOff_Cmd(void);				                // CMD : 0xE9
extern void CSOT_DemuraOn_Cmd(void);					            // CMD : 0xEA
extern void CSOT_Ctrl_TconReset_Cmd(void);			                // CMD : 0xEB
extern void CSOT_Read_TconResetStaFormTconReg_Cmd(void);			// CMD : 0xEC
extern void CSOT_Read_TconDemuraStaFormTconReg_Cmd(void);			// CMD : 0xED
extern void CSOT_Switch_Mode_Cmd(void);			                    // CMD : 0xCD
extern void CSOT_Switch_WhiteTrackingPattern_Cmd(void);			    // CMD : 0xE3
extern void CSOT_TurnOff_WhiteTracking_Cmd(void);				    // CMD : 0xE4
extern void CSOT_TurnOn_WhiteTracking_Cmd(void);					// CMD : 0xE5
extern void CSOT_RealTime_TuningWhiteTracking_Cmd(void);			// CMD : 0xE6
extern void CSOT_DGCOff_Cmd(void);									// CMD : 0xE7
extern void CSOT_DGCOn_Cmd(void);									// CMD : 0xE8
// extern void CSOT_I2C_Slv_Read_t10VCOM_from_Flash(void);             // CMD : 0x71 / 0x73
// extern void CSOT_I2C_Slv_Write_t10VCOM_to_Flash(void);              // CMD : 0x72 / 0x74
extern void CSOT_update_GammaCodeFromTconReg(void);
extern void CSOT_update_GammaCodeToTconReg(uint8_t wlen);
#endif

#endif