#ifndef __MCU_DL_H_
#define __MCU_DL_H_

//extern volatile union mcu_entry_cfg_3 xdata g_mcu_entry_cfg_3_u;
//extern void mcu_dl_i2c_cal_data_cks_old_ver(void);
extern void mcu_dl_handler(void);
extern void mcu_dl_i2c_cal_data_cks(uint8_t val);
extern void mcu_dl_i2c_hw_cal_cks_en(void);
extern void mcu_entry_dl_hdr(void);
extern void mcu_dl_entry_cks(void);
//extern void mcu_dl_i2c_hw_read_cks(void);
// extern uint8_t mcu_dl_cks_cmp_old_ver(uint8_t val);
// extern uint8_t xdata g_mcu_old_ver; 
extern uint8_t xdata g_i2c_hw_cks_res_u8;

#endif
