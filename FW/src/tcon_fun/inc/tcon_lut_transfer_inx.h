#ifndef TCON_LUT_TRANSFER_INX_H_
#define TCON_LUT_TRANSFER_INX_H_

#include "mcu_global_vars.h"

extern void tcon_lut_transfer_Inx_Init(void);
extern void tcon_lut_transfer_InxtoFiti(void);

// Demura Inx Transfer
struct rw_inx_demura_cfg
{
    uint8_t code_data[16];
    uint8_t mode[4];
    uint8_t range;
    uint8_t min;
};

struct rw_inx_decode_cfg
{
    uint8_t sub_range[3];
    uint8_t N[4];              
    uint8_t Q[4];            
    uint8_t deLut[5][16];           
};

struct r_inx_m0                         // 12 bytes
{
    uint8_t m0_code_0                   : 4;
    uint8_t m0_code_1                   : 4;

    uint8_t m0_code_2                   : 4;
    uint8_t m0_code_3                   : 4;

    uint8_t m0_code_4                   : 4;
    uint8_t m0_code_5                   : 4;

    uint8_t m0_code_6                   : 4;
    uint8_t m0_code_7                   : 4;

    uint8_t m0_code_8                   : 4;
    uint8_t m0_code_9                   : 4;

    uint8_t m0_code_10                  : 4;
    uint8_t m0_code_11                  : 4;

    uint8_t m0_code_12                  : 4;
    uint8_t m0_code_13                  : 4;

    uint8_t m0_code_14                  : 4;
    uint8_t m0_code_15                  : 4;
        
    uint8_t m0_mode_0                   : 3;
    uint8_t m0_mode_1                   : 3;
    uint8_t m0_mode_2_1_0               : 2;

    uint8_t m0_mode_2_2_1               : 1;
    uint8_t m0_mode_3                   : 3;
    uint8_t m0_range_3_0                : 4;

    uint8_t m0_range_6_4                : 3;
    uint8_t m0_min_4_0                  : 5;

    uint8_t m0_min_6_5                  : 2;
    uint8_t reserved                    : 6;
};

struct r_demura_cfg_0  
{
    struct r_inx_m0 Pn_Nx_16;
};

extern volatile struct rw_inx_demura_cfg xdata g_rw_inx_demura_cfg_u;
extern volatile struct rw_inx_decode_cfg xdata g_rw_inx_decode_cfg_u;
extern volatile struct r_demura_cfg_0  xdata g_r_spi_inx_demura_cfg_u[155];        // 155 = (496/16) * 5    HW SPI DMA --> INX FORMAT

extern uint8_t g_inx_last_Q_val ;
// extern uint8_t g_inx_last_code_val;
extern uint8_t g_inx_last_N_val;
extern uint8_t g_inx_last_mode;

extern uint8_t xdata g_rw_set_inx_hw_cal_en;
extern uint8_t xdata g_w_set_inx_numerator_1;
extern uint8_t xdata g_w_set_inx_numerator_2;
extern uint16_t xdata g_w_set_inx_numerator_3;
extern uint8_t xdata g_w_set_inx_add_num;
extern uint8_t xdata g_r_result_27_24;
extern uint8_t xdata g_r_result_23_16;
extern uint8_t xdata g_r_result_15_8;
extern uint8_t xdata g_r_result_7_0;
extern uint8_t xdata g_w_inx_set_offset;
extern uint16_t xdata g_r_hw_align_len_inx;
extern uint8_t xdata g_r_result1_23_16;
extern uint8_t xdata g_r_result1_15_8;
extern uint8_t xdata g_r_result1_7_0;

#endif