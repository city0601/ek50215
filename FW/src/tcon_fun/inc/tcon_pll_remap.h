#ifndef TCON_PLL_REMAP_H_
#define TCON_PLL_REMAP_H_

#ifdef PLL_REMAP_FUN
//#include "system_reg.h"
//#include "sys_reg.h"
#include "sys.h"
#include "aip_reg.h"

void tcon_pll_remap_handler(void);
extern void tcon_pll_remap_isr(void);
void tcon_i2c_pll_remap_proc(void);
#endif

#endif
