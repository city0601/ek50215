#ifndef TCON_MISC_H_
#define TCON_MISC_H_
#include "irq.h"

#define HW_DL_FAIL 0
#define HW_DL_PASS 1

extern bool g_vsync_irq_flg;

extern void tcon_wait_dmr_finish(void);
//extern void tcon_wait_sys_finish(void);
extern void tcon_wait_sys_reg_lut_finish(void);
//extern void tcon_wait_reg_finish(void);
//extern void tcon_wait_lut_finish(void);
//extern void tcon_wait_lut_finish_with_esol(void);
extern bool tcon_check_dl_sta(void);
extern void tcon_error_handler(void);
//extern void mcu_entry_dl_hdr(void);
//extern void mcu_dl_entry_cks(void);
#endif
