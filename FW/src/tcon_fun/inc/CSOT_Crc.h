#ifndef _COST_CRC_H_
#define _COST_CRC_H_
#ifdef CSOT_FUN
#include "mcu_global_vars.h"

#define HW_CRC_CAL_ON       HW_I2C_ADR_AND_CRC_CKS |= 0x80
#define HW_CRC_CAL_OFF      HW_I2C_ADR_AND_CRC_CKS &= ~0x80

#define HW_CRC_CAL_RESET    DL_FINISH |= 0x20

extern uint16_t CSOT_Crc_FW_Cal(uint8_t* dataBuffer,uint8_t len);
extern uint16_t CSOT_Crc_HW_Cal(void);
// extern uint16_t CSOT_Crc_Cal(uint8_t* dataBuffer,uint16_t len);
#endif
#endif
