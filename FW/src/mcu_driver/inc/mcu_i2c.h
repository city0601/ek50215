#ifndef __MCU_I2C_H__
#define __MCU_I2C_H__

#include "mcu_global_vars.h"

#define I2C_FAIL                        0
#define I2C_PASS                        1
// define I2c Slave Addr
#define MCU_I2C_SLV_ADDR_TCON    (0x5B)
#define MCU_I2C_SLV_ADDR_I2C2SPI (0x61)
#define MCU_I2C_SLV_ADDR_CSOT    (0x70)

#define MCU_I2C_WAIT_SI_TIMEOUT         1000
/*
 * I2CSTA - I2C Status Code
 */
#define S_PAT_REG_MIN                   0x22
#define S_PAT_REG_MAX                   0x5A

#define I2CSTA_IDLE                     (0xF8)
#define I2CSTA_ERROR                    (0x00)

#define I2CSTA_MST_START                (0x08)
#define I2CSTA_MST_RPT_START            (0x10)
#define I2CSTA_MST_ARB_LOST             (0x38)

#define I2CSTA_MST_SLA_W_ACK            (0x18)
#define I2CSTA_MST_SLA_W_NACK           (0x20)
#define I2CSTA_MST_DATA_W_ACK           (0x28)
#define I2CSTA_MST_DATA_W_NACK          (0x30)

#define I2CSTA_MST_SLA_R_ACK            (0x40)
#define I2CSTA_MST_SLA_R_NACK           (0x48)
#define I2CSTA_MST_DATA_R_ACK           (0x50)
#define I2CSTA_MST_DATA_R_NACK          (0x58)

#define I2CSTA_SLV_SLA_W_ACK            (0x60)
#define I2CSTA_SLV_ARB_LOST_SLA_W_ACK   (0x68)
#define I2CSTA_SLV_DATA_W_ACK           (0x80)
#define I2CSTA_SLV_DATA_W_NACK          (0x88)
#define I2CSTA_SLV_DATA_W_ACK_STOP      (0xA0)

// GC
#define I2CSTA_SLV_GC_ACK               (0x70)
#define I2CSTA_SLV_GC_DATA_ACK          (0x90)

#define I2CSTA_SLV_SLA_R_ACK            (0xA8)
#define I2CSTA_SLV_ARB_LOST_SLA_R_ACK   (0xB0)
#define I2CSTA_SLV_DATA_R_ACK           (0xB8)
#define I2CSTA_SLV_DATA_R_NACK          (0xC0)
#define I2CSTA_SLV_LAST_DATA_R_ACK      (0xC8)

/*
 * I2CCON Definition & Function
 */
#define I2CCON_CR2      (0x01 << 7)
#define I2CCON_ENS1     (0x01 << 6)
#define I2CCON_STA      (0x01 << 5)
#define I2CCON_STO      (0x01 << 4)
#define I2CCON_SI       (0x01 << 3)
#define I2CCON_AA       (0x01 << 2)
#define I2CCON_CR1      (0x01 << 1)
#define I2CCON_CR0      (0x01)

//#define I2CCON_CR       (0x00) // 108MHz / 256 = 421kHz
//#define I2CCON_CR       (0x03) // 108MHz / 160 = 675kHz
//#define I2CCON_CR       (0x81) // 108MHz / 120 = 900kHz
//#define I2CCON_CR       (0x82) // 108MHz / 60 = 1.8MHz
#define I2CCON_CR       (0x83) // for time1

#define CLK_1MHZ					0x41
#define CLK_400KHZ       0x42
#define CLK_200KHZ       0x43
#define CLK_100KHZ       0xC0

#if 0
#define CLK_375KHZ       0xFD
#define CLK_281KHZ       0xFC
#define CLK_187KHZ       0xFA
#define CLK_100KHZ       0xF5
#endif

#define I2C1CON_EN   (I2CCON |= I2CCON_ENS1)
#define I2C1CON_DIS   (I2CCON &= ~I2CCON_ENS1)
#define I2CCON_START    (I2CCON_ENS1 | I2CCON_STA)
#define I2CCON_STOP     (I2CCON_ENS1 | I2CCON_STO)

#define I2C1CON_SI_IS_SET   (I2CCON & I2CCON_SI)
#define I2C1CON_SI_CLEAR    (I2CCON &= (~I2CCON_SI))
#define I2C1CON_CLEAR       (I2CCON = I2CCON & (~(I2CCON_STA | I2CCON_STO)) | (I2CCON_AA))
#define I2C1CON_AA_0        (I2CCON &= (~I2CCON_AA))
#define I2C1CON_AA_1        (I2CCON |= I2CCON_AA)
#define I2C1CON_SEND_START  (I2CCON |= I2CCON_START)
#define I2C1CON_SEND_STOP   (I2CCON |= I2CCON_STOP)

#define I2C1_SLV_STA        I2CSTA

#if 0
    #define I2C2CON_SI_IS_SET   (I2CCON & I2CCON_SI)
    #define I2C2CON_SI_CLEAR    (I2CCON &= (~I2CCON_SI))
    #define I2C2CON_CLEAR       (I2CCON = ((I2CCON & (~(I2CCON_STA | I2CCON_STO))) | (I2CCON_AA)))
    #define I2C2CON_AA_0        (I2CCON &= (~I2CCON_AA))
    #define I2C2CON_AA_1        (I2CCON |= I2CCON_AA)
    #define I2C2CON_SEND_START  (I2CCON |= I2CCON_START)
    #define I2C2CON_SEND_STOP   (I2CCON |= I2CCON_STOP)

    #define I2C_SLV_CON I2CCON
    #define I2C_SLV_ADR I2CADR
    #define I2C_SLV_STA I2CSTA
    #define I2C_SLV_DAT I2CDAT
#else

    #define I2C2CON_SI_IS_SET   (I2C2CON & I2CCON_SI)
    #define I2C2CON_SI_CLEAR    (I2C2CON &= (~I2CCON_SI))
    #define I2C2CON_CLEAR       (I2C2CON = ((I2C2CON & (~(I2CCON_STA | I2CCON_STO))) | (I2CCON_AA)))
    #define I2C2CON_AA_0        (I2C2CON &= (~I2CCON_AA))
    #define I2C2CON_AA_1        (I2C2CON |= I2CCON_AA)
    #define I2C2CON_SEND_START  (I2C2CON |= I2CCON_START)
    #define I2C2CON_SEND_STOP   (I2C2CON |= I2CCON_STOP)

    #define I2C2_SLV_CON I2C2CON
    #define I2C2_SLV_ADR I2C2ADR
    #define I2C2_SLV_STA I2C2STA
    #define I2C2_SLV_DAT I2C2DAT
#endif

#define I2CCON_WAIT_SI_TIMEOUT (500)

#define I2C_MASTER_PATH (0)
#define I2C_SLAVE_PATH (1)

#define MCU_I2C_SLA_W_TCON      (MCU_I2C_SLV_ADDR_TCON << 1)          // 5Ah W => B4h
#define MCU_I2C_SLA_R_TCON      ((MCU_I2C_SLV_ADDR_TCON << 1) + 1)    // 5Ah R => B5h
#define MCU_I2C_SLA_W_I2C2SPI   (MCU_I2C_SLV_ADDR_I2C2SPI << 1)       // 61h W => C2h
#define MCU_I2C_SLA_R_I2C2SPI   ((MCU_I2C_SLV_ADDR_I2C2SPI << 1) + 1) // 61h R => C3h

#define SAMSUNG_SLV_ADR  0xC2
#define TCON_OD_SLV_ADR  0xC2

#define I2C_GC              0x01

#define MCU_I2C_SLA_W_CSOT      (MCU_I2C_SLV_ADDR_CSOT << 1)          // 70h W => E0h
#define MCU_I2C_SLA_R_CSOT      ((MCU_I2C_SLV_ADDR_CSOT << 1) + 1)    // 70h R => E1h


void mcu_i2c_isr(void);
void mcu_i2c2_isr(void);
void mcu_i2c1_init(void);
void mcu_i2c2_init(void);
bool mcu_i2c_mst_byte_ready(void);
void mcu_i2c_mst_byte_read_req(void);
uint8_t mcu_i2c_mst_read_byte(bool last_byte);
void mcu_i2c_mst_write_byte(uint8_t wt_val);
bool mcu_i2c_mst_recv_ack(void);
void mcu_i2c_mst_write_stop(uint16_t wait_tm_us);
void mcu_i2c_mst_write_start_addr(uint8_t addr, uint8_t wt0_rd1, bool rpt);

void mcu_i2c_send_nack(void);   // Eric add at 20201212
void mcu_i2c_clk_init(void);
void mcu_get_i2cclk_set(void);

bool mcu_i2c_mst_write(uint8_t dev_adr ,uint8_t* mem_reg, uint8_t mem_len, uint8_t* data_buf , uint16_t data_len, bool cal_dl_cks_flg, uint16_t wait_tm_us);
bool mcu_i2c_mst_read(uint8_t* w_buf , uint8_t mem_len , uint8_t* r_buf , uint16_t r_len , bool cal_dl_cks_flg, uint16_t wait_tm_us,bool no_stop_flg);
bool mcu_i2c_mst_read_data(uint8_t* r_buf,uint16_t r_len,uint16_t wait_tm_us);

void mcu_i2c_mst_en(void);
void mcu_i2c_slv_en(void);

void mcu_i2c_proc_slv_cmd(void);
extern void mcu_i2c_set_two_pin_sel(void);

extern uint8_t g_i2csta_tmp;
extern uint8_t g_i2c1sta_tmp;
extern uint8_t g_i2c2sta_tmp;
extern uint8_t g_i2cslvdat;
extern uint16_t g_i2c_wr_dly_time;
extern uint16_t g_vcom_wr_dly_time;
#endif
