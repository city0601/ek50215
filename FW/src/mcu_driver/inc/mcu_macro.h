#ifndef __MCU_MACRO_H__
#define __MCU_MACRO_H__

//#define MERGE_32BIT(a,b,c,d) ((uint32_t)a << 24) | ((uint32_t)b << 16) | (c << 8) | (d)
#define VAR_MERGER_16(res,var) (res = *((uint16_t*)&var))
#define VAR_MERGER_32(res,var) (res = *((uint32_t*)&var))

//#define APPLICATION_MODE      0        // Application Mode (Signature verification passed)
#endif
