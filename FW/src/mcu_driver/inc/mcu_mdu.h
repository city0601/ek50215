
#if 0
//  Function define section
unsigned int MduDiv_32_16(unsigned long u32_dividend,
                          unsigned int u16_divisor,
                          unsigned long *u32_consult);
// -----------------------------------------------
unsigned int MduDiv_16_16(unsigned int u16_dividend,
                          unsigned int u16_divisor,
                          unsigned int *u16_consult);
// -----------------------------------------------
/*void MduMul_16x16(unsigned int u16_multiplicand,
                  unsigned int u16_multiplicator,
                  unsigned long *u32_consult);*/
unsigned long MduMul_16x16(unsigned int u16_multiplicand,
                  unsigned int u16_multiplicator);
// -----------------------------------------------
void MduShift(unsigned long u32_shift_data,
              unsigned char u8_shift_lr,
              unsigned char u8_shift_counter,
              unsigned long *u32_consult);
// -----------------------------------------------
void MduNormalizing(unsigned long u32_nor_data,
                    unsigned long *u32_consult);
// *******************************************************************
#endif
