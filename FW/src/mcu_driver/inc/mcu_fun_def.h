#ifndef __MCU_FUN_DEF_H__
#define __MCU_FUN_DEF_H__

//#define EDP_FUN
//#define EDP1_4_FUN                    // not support in EK50309
#define I2C_RW_SAMSUMG_PAT_FUN
#define MCU_SPI_FUN
//#define I2C_OVER_SPI_FUN
#define LUT_SRW_FUN
//#define PLL_REMAP_FUN         // mask now / need modify
#define CSOT_FUN
//#define FLASH_CODE
// #define DEMURA_TRANS_FUN             // not support in EK50309   
//#define DEMURA_CHK_FUN 
//#define CABC_FUN                      // not support in EK50309

//#define LD_LED_DRV_FUN        // mask for test
//#define CASCADE_EDP_FUN
//#define CASCADE_VX1_FUN
//#define CASCADE_DEMURA_FUN

//#define ISP_FUN               

#define VRR_FUN
//#define OD_FUN
//#define DLG_FUN
#endif
