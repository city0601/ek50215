#ifndef __WIN32_EMU_H__
#define __WIN32_EMU_H__

#ifdef __WIN32_EMULATION__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//typedef unsigned char   bool;
typedef unsigned char   bool;

typedef char            int8_t;
typedef unsigned char   uint8_t;

typedef short           int16_t;
typedef unsigned short  uint16_t;

typedef long            int32_t;
typedef unsigned long   uint32_t;

#define __WIN32_EMULATION_PRINTF(arg) printf(arg)
#define __WIN32_EMULATION_PRINTF2(arg1, arg2) printf(arg1, arg2)

void __WIN32_EMULATION_OPEN(void);
void __WIN32_EMULATION_CLOSE(void);
void __WIN32_EMULATION_PRE_PROC(void);
void __WIN32_EMULATION_POST_PROC(void);

void __WIN32_EMULATION_EDP_INIT(void);
bool __WIN32_EMULATION_MCU_TIMER_CHECK_300US_TIMEOUT_FUNC(void);

#define __WIN32_EMULATION_MCU_TIMER_CHECK_300US_TIMEOUT return __WIN32_EMULATION_MCU_TIMER_CHECK_300US_TIMEOUT_FUNC()

extern uint8_t vvv;

#define __REG51XC_H__
#define __MCU_TYPE_H__

#define xdata
#define idata
#define _at_(a)
#define __WIN32_EMULATION_MCU_ISR43_INT_8_USING_2

extern volatile unsigned char I2CDAT;
extern volatile unsigned char I2CADR;
extern volatile unsigned char I2CCON;
extern volatile unsigned char I2CSTA;

extern volatile unsigned char I2C2DAT;
extern volatile unsigned char I2C2ADR;
extern volatile unsigned char I2C2CON;
extern volatile unsigned char I2C2STA;

extern volatile unsigned char SPSTA;
extern volatile unsigned char SPCON;
extern volatile unsigned char SPDAT;
extern volatile unsigned char SPSSN;
extern volatile unsigned char SPISET;

extern volatile unsigned char P0_0;

#else

#define __WIN32_EMULATION_PRINTF(arg)
#define __WIN32_EMULATION_PRINTF2(arg1, arg2)

#define __WIN32_EMULATION_OPEN()
#define __WIN32_EMULATION_CLOSE()
#define __WIN32_EMULATION_PRE_PROC()
#define __WIN32_EMULATION_POST_PROC()

#define __WIN32_EMULATION_EDP_INIT()

#define __WIN32_EMULATION_MCU_TIMER_CHECK_300US_TIMEOUT

#define __WIN32_EMULATION_MCU_ISR43_INT_8_USING_2 interrupt 8 using 2


#endif


#endif
