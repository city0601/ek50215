#ifndef __MCU_UART_H__
#define __MCU_UART_H__

#include "mcu_global_vars.h"

void mcu_uart_init(void);
void mcu_uart_isr(void);

void mcu_uart_putchar(uint8_t ch);
void mcu_uart_puts(uint8_t *str);
void mcu_uart_put_nibble_hex(uint8_t val);
void mcu_uart_put_hex(uint8_t val);
void mcu_uart_put_hex2n(uint8_t val1, uint8_t val2);
void mcu_uart_put_hex3n(uint8_t val1, uint8_t val2, uint8_t val3);
void mcu_uart_put_dec(uint8_t val);

#define MCU_LOG mcu_uart_puts

#ifndef DBG_MSG
    #define DBG_LOG 
#else
#include <stdio.h>
    #define DBG_LOG printf
#endif

#endif
