#ifndef __MCU_INT_H__
#define __MCU_INT_H__

#include "mcu_global_vars.h"

#ifdef EDP_FUN
    #include "edp_link_proc.h"
#endif

void mcu_int_init(void);
void mcu_int_i2c_mask(bool en);
void mcu_int_i2c2_mask(bool en);
void mcu_int_int0_en(void);
void mcu_int_int1_en(void);
void mcu_int_int3_en(void);
void mcu_int_int4_en(void);
void mcu_int_int5_en(void);
void mcu_int_int6_en(void);
void mcu_int_int8_en(void);
void mcu_int_int9_en(void);
void mcu_int_int9_dis(void);
void mcu_int_tim2_en(void);
void mcu_int_tim2_dis(void);
extern void mcu_int_clear_iex4_isr_event(void);
extern void mcu_clear_iex4_isr_event(void);
#endif

