#ifndef __MCU_SPI_H__
#define __MCU_SPI_H__

#include "mcu_global_vars.h"
//-----------------------------------------------
//#define SPI_WP_HW_CTRL
//-----------------------------------------------
#define SPI_FLASH_CMD_WRITE_STATUS      (0x01)
#define SPI_FLASH_CMD_READ_STATUS1      (0x05)
#define SPI_FLASH_CMD_READ_STATUS2      (0x35)
#define SPI_FLASH_CMD_READ_ID_2BYTE     (0x90)  
#define SPI_FLASH_CMD_READ_ID_3BYTE     (0x9F)

#define SPI_FLASH_CMD_WRITE_ENABLE      (0x06)
#define SPI_FLASH_CMD_WRITE_DISABLE     (0x04)
#define SPI_FLASH_CMD_PAGE_PROGRAM      (0x02)

#define SPI_FLASH_CMD_READ_DATA         (0x03)
#define SPI_FLASH_CMD_FAST_READ         (0x0B)
#define SPI_FLASH_CMD_FAST_READ_DUAL    (0x3B)

#define SPI_FLASH_CMD_SECTOR_ERASE      (0x20)
#define SPI_FLASH_CMD_BLOCK_ERASE_32KB  (0x52)
#define SPI_FLASH_CMD_BLOCK_ERASE_64KB  (0xD8)
#define SPI_FLASH_CMD_CHIP_ERASE        (0xC7)
#define SPI_FLASH_CMD_CHIP_ERASE2       (0x60)

#define READ_DL_STATUS                  (0x88)

#define SPI_FLASH_CMD_READ_TCON_ID      (0x04)
#define SPI_FLASH_CMD_SW_PATH           (0x07)
#define SPI_FLASH_CMD_READ_MCU_STATUS   (0x08)
#define SPI_FLASH_CMD_DEBUG_F2H         (0xF2)

#define CLK_54M     0
#define CLK_27M     1
#define CLK_13_5M   2
#define CLK_6_75M   3
#define CLK_3_3M    4
#define CLK_1_68M   5

#define SPI_CFG_PATH    0
#define SPI_DMR_PATH    1

#define HW_SPI_ACCESS       1
#define HW_SPI_RELEASE      0

#define FLASH_SECTOR_SZ     0x1000
// Other Definition
#define FLASH_CS_XBOARD                     1
//#define FLASH_CS_CBOARD                     1
#define FLASH_CS_CBOARD                     0      // EK50303B has just one path for cs


void mcu_spi_set_clk(uint8_t clk);
void mcu_spi_acc_bus(bool en);
void mcu_spi_init(void);

bool mcu_spi_flash_read_id(uint8_t cmd,uint8_t *buf, uint8_t cs);
uint8_t mcu_spi_flash_read_status1(uint8_t cs);
//uint8_t mcu_spi_flash_read_status2(uint8_t cs);
bool mcu_spi_flash_write_status(uint8_t status, uint8_t cs);
bool mcu_spi_flash_chip_erase(uint8_t cs);
/*bool mcu_spi_flash_block_erase_32kb(uint8_t blk_adr_h, uint8_t blk_adr_m, uint8_t blk_adr_l, bool cs);
bool mcu_spi_flash_block_erase_64kb(uint8_t blk_adr_h, uint8_t blk_adr_m, uint8_t blk_adr_l, bool cs);
bool mcu_spi_flash_sector_erase(uint8_t sec_adr_h, uint8_t sec_adr_m, uint8_t sec_adr_l, bool cs);*/
bool mcu_spi_flash_merge_erase(uint8_t cmd, uint8_t* buf, uint8_t cs);
bool mcu_spi_flash_page_program(uint8_t *sec_adr, uint8_t *sec_buf, uint8_t len_h , uint8_t len_l, uint8_t cs);
bool mcu_spi_flash_fast_read(uint8_t *adr, uint8_t *buf, uint16_t len , uint8_t cs);
bool mcu_spi_flash_fast_read_dual(uint8_t *adr, uint8_t *buf, uint16_t len , uint8_t cs);
bool mcu_spi_flash_erase(uint8_t* recv_buf, uint8_t cs);
void mcu_spi_get_flash_size(uint8_t l_cs);
void mcu_spi_flash_protected(uint8_t l_cs);
void mcu_spi_sw_path(uint8_t path);

#ifdef LD_LED_DRV_FUN
bool mcu_spi_read_leddrv(uint8_t *cmd_ptr, uint8_t cmd_len, uint8_t *buf_adr, uint16_t rd_len, uint8_t cs_idx, uint8_t led_num);
bool mcu_spi_write_leddrv_fixed(uint8_t *cmd_ptr, uint8_t cmd_len, uint8_t* wr_val, uint16_t wr_len, uint8_t led_num, uint8_t cs_idx, uint8_t cksm_en);
bool mcu_spi_write_leddrv(uint8_t *cmd_ptr, uint8_t cmd_len, uint8_t *ptr, uint16_t wr_len, uint8_t led_num, uint8_t cs_idx, uint8_t cksm_en);
bool mcu_spi_write_leddrv_frameend(uint8_t *cmd_ptr, uint8_t cmd_len, uint8_t led_num, uint8_t cs_idx, uint8_t cksm_en);
bool mcu_spi_write_leddrv_bc(uint8_t *cmd_ptr, uint8_t cmd_len, uint8_t *ptr, uint16_t wr_len, uint8_t led_num, uint8_t cs_idx, uint8_t cksm_en);

bool mcu_spi_write_leddrv_bc_int(uint8_t *cmd_ptr, uint8_t cmd_len, uint8_t *ptr, uint16_t wr_len, uint8_t led_num, uint8_t cs_idx, uint8_t cksm_en);

void spi_leddrv_test_func(void);
void mcu_spi_leddrv_irq_handler(void);
#endif

#endif
