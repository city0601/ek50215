#ifndef __MCU_WDT_H__
#define __MCU_WDT_H__

#include "mcu_global_vars.h"

void mcu_wdt_init(void);
void mcu_wdt_reset(void);

#endif
