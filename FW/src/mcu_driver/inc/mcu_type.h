#ifndef __MCU_TYPE_H__
#define __MCU_TYPE_H__

//typedef unsigned char   bool;
typedef bit   bool;

typedef char            int8_t;
typedef unsigned char   uint8_t;

typedef short           int16_t;
typedef unsigned short  uint16_t;

typedef long            int32_t;
typedef unsigned long   uint32_t;

#endif
