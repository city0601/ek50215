#ifndef __MCU_DMA_H__
#define __MCU_DMA_H__

#include "mcu_global_vars.h"

//void mcu_dma_init(void);


//  dmasel.4 dmarol
#define DMA_ROT_PRIORITY_ENABLE  1    //DMA輪換優先級使能,設定後,DMA各通道
                                      //的優先級按通道號升序順序排列
#define DMA_ROT_PRIORITY_DISABLE 0    //DMA輪換優先級不使能,設定後,DMA各通道
                                      //的優先級先按優先級高低分2組,同組內按
                                      //升序順序排列

//  dmasel.3 dmasynch
#define DMA_SYNCH                1    //同步模式,激活一次只搬運1個字節
#define DMA_ASYNCH               0    //异步模式,激活一次搬運DMAC中設定的字節數

//  dmasel.2..0 dmasel
#define DMA_SEL_CH0              0    //選定的Channel號
#define DMA_SEL_CH1              1
#define DMA_SEL_CH2              2
#define DMA_SEL_CH3              3
#define DMA_SEL_CH4              4
#define DMA_SEL_CH5              5
#define DMA_SEL_CH6              6
#define DMA_SEL_CH7              7
// *******************************************************************
//  DMAM0 register

//  memory space select
#define DMA_CODE                 0    //程序存儲器
#define DMA_XDATA                1    //外部數據存儲器
#define DMA_DATA                 2    //片內數據存儲器
#define DMA_SFR                  3    //特殊功能寄存器

//  address control
#define DMA_HOLD                 0    //傳輸一個字節後源和目標地址保持不變
#define DMA_INC                  2    //傳輸一個字節後源和目標地址自動加1
#define DMA_DEC                  3    //傳輸一個字節後源和目標地址自動减1
// *******************************************************************
//  DMAM1 register
//  dmam1.7 dmairq                    //DMA中斷請求標識位
#define DMA_IRQ_FLAG_CLR         0    //DMA中斷請求標識,DMA完成後硬件置1,需軟件清0

//  dmam1.6 dmaie                     //DMA中斷使能標識位
#define DMA_IRQ_EN               1    //DMA中斷使能
#define DMA_IRQ_DIS              0    //DMA中斷屏蔽

//  dmam1.5 dmapri                    //DMA中斷優先級.8個通道分高低2組,
#define DMA_PRI_HIGHER           1    //DMA中斷優先級高
#define DMA_PRI_LOWER            0    //DMA中斷優先級低

//  dmam1.4..0                        //選擇觸發DMA啓動的中斷源
#define DMA_TRIGER_EXT0          0    //外中斷0
#define DMA_TRIGER_T0            1    //定時器0
#define DMA_TRIGER_EXT1          2    //外中斷1
#define DMA_TRIGER_T1            3    //定時器1
#define DMA_TRIGER_S0            4    //串口0發送或接收中斷
#define DMA_TRIGER_T2            5    //定時器2
#define DMA_TRIGER_EXT7          6    //外中斷7(I2C共用此中斷)
#define DMA_TRIGER_EXT2          7    //外中斷2(SPI共用此中斷)
#define DMA_TRIGER_EXT3          8    //外中斷3
#define DMA_TRIGER_EXT4          9    //外中斷4
#define DMA_TRIGER_EXT5          10   //外中斷5
#define DMA_TRIGER_EXT6          11   //外中斷6
#define DMA_TRIGER_S1            12   //串口1發送或接收中斷
#define DMA_TRIGER_EXT8          13   //外中斷8(DMA0和DMA4共用此中斷)
#define DMA_TRIGER_EXT9          14   //外中斷8(DMA1和DMA5共用此中斷)
#define DMA_TRIGER_EXT10         15   //外中斷8(DMA2和DMA6共用此中斷)
#define DMA_TRIGER_EXT11         16   //外中斷8(DMA3和DMA7共用此中斷)
#define DMA_TRIGER_EXT12         17   //外中斷12
#define DMA_TRIGER_OTHERS        18   //其它,此時將使用DMA軟啓動方式啓動DMA
//關于DMA啓動中斷源的一些說明:儘量選擇沒有使用的中斷作爲DMA啓動中斷源,否則
//可能引起誤解,分辯不出是要啓動DMA還是要觸發中斷。
// *******************************************************************

#define mcu_dma_start_soft()        DMASEL |= 0x80
#define mcu_dma_irq_flag_clr()      DMAM1 &= 0x7F
#define mcu_dma_irq_enable()        DMAM1 |= 0x40
#define mcu_dma_irq_disable()       DMAM1 &= 0xBF
#define mcu_dma_wait_until_done()   while(!(DMAM1&0x80))
#define mcu_dma0_int_enable()       IEN2 |= 0x02
#define mcu_dma1_int_enable()       IEN2 |= 0x04
#define mcu_dma2_int_enable()       IEN2 |= 0x08
#define mcu_dma3_int_enable()       IEN2 |= 0x10
#define mcu_dma0_int_disable()      IEN2 &= 0xFD
#define mcu_dma1_int_disable()      IEN2 &= 0xFB
#define mcu_dma2_int_disable()      IEN2 &= 0xF7
#define mcu_dma3_int_disable()      IEN2 &= 0xEF

//******************************************************************
//  Function name:  void DmaChannelSelRot(uint8_t dma_rot, uint8_t dma_synch,
//                                        uint8_t dma_channel);
//  Description:    Used to select the currently accessed set of
//                  registers assigned to one of the DMA channels.
//                  The other purpose of this register is to manually
//                  start the transmission using the actually selected
//                  DMA channel. Another feature controlled by the DMASEL
//                  register is the type of priority structure between
//                  channels (2-level programmable or rotating).
//  Parameters:
//    uint8_t dma_rot: DMA priority mode. When set to 1, the rotating
//                  priority is enabled, and after completing the transfer
//                  the highest priority is automatically given to the
//                  channel with the next following number (ascending).
//                  When set to 0, each DMA channel’s priority can be
//                  programmed to one of two levels, and within each
//                  level the natural priority is used (channel 0 has
//                  the highest priority, channel 1 has lower and so on
//                  till the last channel implemented, which has the
//                  lowest priority).
//    uint8_t dma_synch: Synchronous Mode flag. When set to 1, configures
//                  the selected DMA channel to work in the Synchronous
//                  Mode, where only a single byte is transferred at
//                  a single DMA activity (upon interrupt or software run);
//                  when 0, the selected channel works in the Asynchronous
//                  Mode, making the transmission of the whole amount of
//                  data specified in the DMAC register.
//    uint8_t dma_channel: Number of currently selected DMA channel. If the
//                  number written to this field exceeds the number of
//                  channels implemented, the Channel 0 is accessed.
//  Returns:        None.
//******************************************************************
void mcu_dma_channel_sel_rot(uint8_t dma_rot, uint8_t dma_synch, uint8_t dma_channel);
//******************************************************************


//******************************************************************
//  Function name:  void DmaSrcAddrSet(uint32_t dma_src_addr);
//  Description:    Set the selected channel(dmasel.0-2) dma source
//                  address.There are hav 8 group DMAS0..2.
//  Parameters:
//    uint32_t dma_src_addr: Only the lower 23 bits are implemented.
//  Returns:        None.
//******************************************************************
void mcu_dma_src_addr_set(uint32_t dma_src_addr);
//******************************************************************


//******************************************************************
//  Function name:  void DmaTarAddrSet(uint32_t dma_tar_addr);
//  Description:    Set the selected channel(dmasel.0-2) dma target
//                  address.There are hav 8 group DMAT0..2.
//  Parameters:
//    uint32_t dma_tar_addr: Only the lower 23 bits are implemented.
//  Returns:        None.
//******************************************************************
void mcu_dma_tar_addr_set(uint32_t dma_tar_addr);
//******************************************************************


//******************************************************************
//  Function name:  void DmaCntSet(uint32_t dma_len);
//  Description:    Set the selected channel(dmasel.0-2) dma target
//                  address.There are hav 8 group DMAC0..2.
//  Parameters:
//    uint32_t dma_tar_addr: Only the lower 23 bits are implemented.
//  Returns:        None.
//******************************************************************
void mcu_dma_cnt_set(uint32_t dma_len);
//******************************************************************


//******************************************************************
//  Function name:  void DmaMode0Set(uint8_t tar_space, uint8_t tar_addr_ctrl,
//                                   uint8_t src_space, uint8_t src_addr_ctrl);
//  Description:    The DMAM0 register is used to program the auto-modification
//                  of source address and target address independently.
//                  After transferring a byte, source/target address can
//                  remain the same, or be either incremented or decremented,
//                  depending on the settings in the DMAM0 register. The other
//                  purpose of the DMAM0 register is to specify the space accessed
//                  as source and as target. All the accessible spaces are:
//                  Program Memory, External Data Memory, On-Chip Memory (IRAM)
//                  and Special Function Registers. The source and target spaces
//                  are programmed independently, making it possible to transfer
//                  data from and to each of the available spaces.Only the number
//                  of bits which equal the size of the selected space’s address
//                  bus are relevant (e.g.when SFR space is selected, only the
//                  7 LSBs of the address are used, the others are “don’t care”).
//  Parameters:
//    uint8_t tar_space: Target space code.Only the lower 2 bits are implemented.
//    uint8_t tar_addr_ctrl: Target address control. Only the lower 2 bits are implemented.
//    uint8_t src_space: Source space code. Only the lower 2 bits are implemented.
//    uint8_t src_addr_ctrl: Source address control. Only the lower 2 bits are implemented.
//  Returns:        None.
//******************************************************************
void mcu_dma_mode0_set(uint8_t tar_space, uint8_t tar_addr_ctrl, uint8_t src_space, uint8_t src_addr_ctrl);
//******************************************************************


//******************************************************************
//  Function name:  void DmaMode1Set(uint8_t dma_ie, uint8_t dma_pri, uint8_t dma_intsel);
//  Description:    The DMAM1 register is used to assign one of the 18 possible 
//                  interrupt sources with the selected DMAchannel.
//                  When no valid source is specified, the selected channel can 
//                  only be triggered by software.
//                  After completing the transfer, the selected source flag is
//                  automatically cleared by hardware upon request from the DMA 
//                  controller. This enables to process interrupts without the 
//                  need to interrupt the CPU, but with the use of DMA only.

//                  The DMAM1 register contains also an interrupt enable flag and 
//                  interrupt request flag, assigned to the selected DMA channel. 
//                  The interrupt request flag is set by hardware each time the 
//                  selected DMA channel has completed its transmission (the DMA
//                  Byte Counter has reached 0). If the interrupt enable flag is set,
//                  the interrupt request flag is passed to the interrupt controller 
//                  of the R8051XC2. 
//                  The DMAM1 register contains the DMA channel priority flag, which
//                  sets one of two priority levels to the selected channel. At the 
//                  same level, the natural priority is considered when a new transfer
//                  is to be chosen from more than one sources (Channel 0 has the highest
//                  priority, Channel 1 has lower and so on). When there is a request 
//                  from another channel which has higher priority than the one which
//                  performs a transfer, the transfer is interrupted and the requesting 
//                  channel is given the control. The transmission cannot be interrupted
//                  by another channel with the same priority flag setting. After completion, 
//                  all pending requests from all channels are checked and the one which has 
//                  the highest natural priority is given control.
//  Parameters:
//    uint8_t dma_ie:  DMA Channel interrupt enable.
//    uint8_t dma_pri: DMA Channel priority bit.
//    uint8_t dma_intsel: DMA Channel trigger selection.
//  Returns:        None.
//******************************************************************
void mcu_dma_mode1_set(uint8_t dma_ie, uint8_t dma_pri, uint8_t dma_intsel);
//******************************************************************

void mcu_dma_demo_0(void);
void mcu_dma_demo_1(void);
void mcu_dma_i2c_slv_set_ch0(void);
void mcu_dma_i2c_slv_set_ch1(void);
void mcu_dma_ch0_rst(void);
void mcu_dma_ch1_rst(void);
void mcu_dma_set_ch0_adr(uint16_t l_start_adr);
#if 0
void mcu_dma_ch2_rst(void);
void mcu_dma_ch2_rst_cfg(void);
#endif
#endif
