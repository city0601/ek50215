#ifndef __MCU_TIMER_H__
#define __MCU_TIMER_H__

#include "mcu_global_vars.h"

void mcu_timer_init(void);
void mcu_timer2_isr(void);
void mcu_timer_delay_us(uint16_t us);
void mcu_timer_wait_us(uint16_t us);
void mcu_timer_set_timeout_us(uint16_t us);
bool mcu_timer_check_timeout_us(void);
void mcu_timer_stop_timeout_us(void);
void mcu_timer_wait_dl_init(void);
bool mcu_timer_dl_timeout(void);
//extern void mcu_delay(uint16_t us);
#endif
