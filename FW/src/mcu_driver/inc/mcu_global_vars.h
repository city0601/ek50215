#ifndef __MCU_GLOBAL_VARS_H__
#define __MCU_GLOBAL_VARS_H__

#include "win32_emu.h"

#include "mcu_reg51.h"
#include "mcu_type.h"
#include "mcu_const.h"
#include "mcu_macro.h"
#include "mcu_wdt.h"
#include "mcu_timer.h"


//#define MCU_GLOBAL_BUF_SZ       (270)
#define MCU_GLOBAL_BUF_SZ       (128)
// CSOT
#define PMIC_DATA_BUF_SZ        (128)
#define PMIC_BANK_BUF_SZ        (48)
#define COMM_HDR_BUF_SZ         (42)  
#define I2C_READ_BUF_SZ    (128)

#define RETRY_TIMES				(3)

// *******************************************************************
// HW Pause
union mcu_entry_cfg_0
{
    uint16_t uint;
    struct 
    {
        //uint16_t rom_addr_7_0              : 8;
        uint16_t rom_addr_15_8             : 8;
        uint16_t rom_addr_7_0              : 8;
    } bits;  
};

union mcu_entry_cfg_1
{
    uint16_t uint;
    struct 
    {
        //uint16_t rom_addr_23_16            : 8;
        uint16_t bank_addr_7_0             : 8;
        uint16_t rom_addr_23_16            : 8;
    } bits; 
};

union mcu_entry_cfg_2
{
    uint16_t uint;
    struct 
    {
        //uint16_t bank_addr_10_8            : 3;
        //uint16_t bank_sel_4_0              : 5;
        uint16_t len_7_0                   : 8;
        uint16_t bank_addr_10_8            : 3;
        uint16_t bank_sel_4_0              : 5;
    } bits;    
};

union mcu_entry_cfg_3
{
    uint16_t uint;
    struct 
    {
        //uint16_t len_15_8                  : 8;
        uint16_t checksum                  : 8;  
        uint16_t len_15_8                  : 8;       
    } bits;    
};

union mcu_entry_cfg_4
{
    uint16_t uint;
    struct 
    {
        uint16_t hdr_checksum               : 8;
        uint16_t vcom_en                    : 1;
        uint16_t finfail_en                 : 1;
        uint16_t patch_en                   : 1;
        uint16_t pmic_action                : 1;        // pause_en
        uint16_t finfail_idx_2_0            : 3;
        //uint16_t pmic_en                    : 1;
        uint16_t rtpm_en                    : 1;
    } bits;    
};
extern volatile union mcu_entry_cfg_0 xdata g_mcu_entry_cfg_0_u; 
extern volatile union mcu_entry_cfg_1 xdata g_mcu_entry_cfg_1_u; 
extern volatile union mcu_entry_cfg_2 xdata g_mcu_entry_cfg_2_u; 
extern volatile union mcu_entry_cfg_3 xdata g_mcu_entry_cfg_3_u; 
extern volatile union mcu_entry_cfg_4 xdata g_mcu_entry_cfg_4_u; 
//extern uint8_t xdata g_mcu_dl_base;
// *******************************************************************
typedef struct
{
    uint8_t write_flg               : 1;    // 0: write , 1:write & check
    uint8_t slv_adr                 : 7;
    uint8_t rw_len                  : 6;
    uint8_t setting                 : 2;    // 0 : no reg , 1: 1byte mode , 2: 2byte mode
    //uint8_t res                     : 1;
    uint8_t reg_ofset[2];
    uint8_t write_byte[64]; 
    uint8_t read_cfg[3]; 
    uint8_t read_byte[64];             

}mcu_dl_vcom_cfg;

extern mcu_dl_vcom_cfg xdata g_mcu_dl_vcom_cfg;
// *******************************************************************
// HW Spi Shift Cfg
union rw_sys_data_0D
{
    uint8_t byte;
    struct 
    {
        uint8_t spi_dma_shift       : 3;
        uint8_t reserved_0          : 1;
        uint8_t format_shift_mode   : 1;
        uint8_t reserved_1          : 2;
        uint8_t hw_cks_msb          : 1;
    }bits;    
};

union r_sys_data_0E
{
    uint8_t byte;
    struct 
    {
        uint8_t hw_cks_msb                  : 8;
    }bits;    
};

union rw_sys_data_0F
{
    uint8_t byte;
    struct
    {
        uint8_t spi_dma_shift_bytes_cnt          : 8;
    }bits;
};

union r_sys_data_10
{
    uint16_t byte;
    struct
    {
        uint8_t cks_msb                         : 8;
        uint8_t cks_lsb                         : 8;
    }bits;
};

union rw_sys_data_12
{
    uint8_t byte;
    struct
    {
        uint8_t demura_chksum_mode_sel          : 4;
        uint8_t demura_chksum_mode_start        : 1;
        uint8_t crc_rst                         : 1;
        uint8_t reserved                        : 2;
    }bits;
};

union r_sys_data_13
{
    uint16_t byte;
    struct
    {
        uint8_t crc_msb                         : 8;
        uint8_t crc_lsb                         : 8;
    }bits;
};

union r_sys_data_20
{
    uint8_t byte;
    struct
    {
        uint8_t through_mode_en                  : 1;
        uint8_t pmic_en                          : 1;
        uint8_t i2c_2pin_sel                     : 1;
        uint8_t vcm_i2c_en                       : 1;

        uint8_t reserved                         : 1; 
        uint8_t hw_init_finish                   : 1;
        uint8_t mcu_code_ver                     : 1;
        uint8_t eep_1_2_byte_sel                 : 1;
    }bits;
};

union r_sys_data_21
{
    uint8_t byte;
    struct
    {
        uint8_t cks                         : 8;
    }bits;
};

// *******************************************************************
// HW Pause
extern volatile union rw_sys_data_0D  xdata g_rw_sys_data_0D_u;
extern volatile union r_sys_data_0E  xdata g_r_sys_data_0E_u;
extern volatile union rw_sys_data_0F  xdata g_rw_sys_data_0F_u; 
//extern volatile uint16_t xdata g_rw_set_spi_en_byte_cnt;

// *******************************************************************
extern volatile union r_sys_data_10  xdata g_r_sys_data_10_u; 
extern volatile union rw_sys_data_12  xdata g_rw_sys_data_12_u; 
extern volatile union r_sys_data_13 xdata g_r_sys_data_13_u;
extern volatile union r_sys_data_20 xdata g_r_sys_data_20_u;
extern volatile union r_sys_data_21 xdata g_r_sys_data_21_u;

// *******************************************************************
extern uint16_t xdata mcu_global_buf_idx;
extern uint8_t xdata mcu_global_buf[];

#ifdef MCU_SPI_FUN
extern bool tcon_spi_dual_mode;
extern volatile uint8_t xdata mcu_spi_rw_addr_h;
extern volatile uint8_t xdata mcu_spi_rw_addr_l;
extern volatile uint8_t xdata mcu_spi_rw_cfg;
extern volatile uint8_t xdata mcu_spi_rw_len_h;
extern volatile uint8_t xdata mcu_spi_rw_len_l;
#endif

extern volatile uint8_t xdata mcu_i2c_pri_addr_2;
extern volatile uint8_t xdata mcu_i2c_pri_addr_3;
extern volatile uint8_t xdata mcu_i2c_pri_addr_4;
extern volatile uint8_t xdata mcu_i2c_pri_addr_5;

extern volatile uint8_t xdata mcu_i2c_sec_addr_2;
extern volatile uint8_t xdata mcu_i2c_sec_addr_3;
extern volatile uint8_t xdata mcu_i2c_sec_addr_4;
extern volatile uint8_t xdata mcu_i2c_sec_addr_5;

// *******************************************************************
// CSOT
union rw_mcu_pmic_crc
{
    uint16_t uint;
    struct
    {
        uint8_t crc_msb                         : 8;
        uint8_t crc_lsb                         : 8;
    }bits;
};

extern volatile union rw_mcu_pmic_crc xdata g_rw_mcu_pmic_bankA_crc;
extern volatile union rw_mcu_pmic_crc xdata g_rw_mcu_pmic_bankB_crc;

struct r_comm_hdr_buf
{
    uint8_t pmic_binfile_cks[2];
    uint8_t common_hdr_len;
    uint8_t pmic_i2c_slv_addr;
    uint8_t magic_num[4];
    uint8_t xb_mem_type;
    uint8_t pmic_dev_addr;
    //uint8_t reserved_0[2];
    uint8_t vcm_gma_sel;
    uint8_t reserved_0;
    uint8_t project_name[20];
    uint8_t driving_struct;
    uint8_t tcon_binfile_crc_msb;
    uint8_t pmic_total_bank_num;
    uint8_t i2c_mst_slv_seperate;
    uint8_t bankA_len;
    uint8_t bankB_len;
    uint8_t xb_flash_code_disable;
    uint8_t tcon_binfile_crc_lsb;
    uint16_t common_hdr_chks;       // 0x29
    uint8_t reserved_1[6];           // 0x2A ~ 0x2F
};

/*struct r_gma_buf
{
    uint8_t val[14];
    uint8_t crc_msb;
    uint8_t crc_lsb;
};*/

extern volatile struct r_comm_hdr_buf xdata g_comm_hdr_buf;
// *******************************************************************
union rw_hw_reg_FFF0h
{
    uint8_t byte;
    struct
    {
        uint8_t start                         : 1;
    }bits;
};

extern volatile union rw_hw_reg_FFF0h xdata g_rw_hw_reg_FFF0h;
extern volatile uint8_t xdata g_rw_hw_reg_FFF1h_pmic_reg_0;
extern volatile uint8_t xdata g_rw_hw_reg_FFF2h_pmic_reg_1;
extern volatile uint8_t xdata g_rw_hw_reg_FFF3h_pmic_reg_2;
extern volatile uint8_t xdata g_rw_hw_reg_FFF4h_pmic_reg_3;
extern volatile uint8_t xdata g_rw_hw_reg_FFF5h_pmic_crc_h_7_0;
extern volatile uint8_t xdata g_rw_hw_reg_FFF6h_pmic_crc_h_15_8;
extern volatile uint8_t xdata g_rw_hw_reg_FFF7h_pmic_crc_l_7_0;
extern volatile uint8_t xdata g_rw_hw_reg_FFF8h_pmic_crc_l_15_8;

union r_hw_reg_FFF9h
{
    uint8_t byte;
    struct
    {
        uint8_t pmic_sta                         : 1;
    }bits;
};
extern volatile union r_hw_reg_FFF9h xdata g_r_hw_reg_FFF9h;
// *******************************************************************
extern uint8_t xdata g_csot_gma_buf[0x10];
extern uint8_t xdata g_pmic_bank_a_buf[0x30];
extern uint8_t xdata g_pmic_bank_b_buf[0x30];
extern uint8_t xdata g_rw_rtpm_i2c_write_len;
extern uint8_t xdata g_rw_rtpm_i2c_read_len;

extern uint8_t xdata g_i2c2spi_read_buf[I2C_READ_BUF_SZ]; 
extern uint8_t* xdata g_pmic_rw_ptr;

extern uint8_t xdata g_fw_ver;
extern uint8_t xdata g_rom_entry_addr[3];
extern uint8_t xdata g_entry_cks_tmp[14];
extern uint8_t xdata mcu_wait_dl_tim_100us;
extern uint16_t xdata mcu_wait_dl_tim_1ms;

//extern bool edp_ioa_mode_en_flag; 
extern bool g_mcu_int_flg;

extern bool g_cfg_rom_sel_path; 
extern bool g_spi_path_flg;
extern bool g_i2c0_spi1_sel;

//extern bool g_mcu_dbg_mode_flg;

extern uint16_t xdata g_i2c_recv_len;

extern uint8_t xdata g_rom_cost_hdr_entry_addr[3];
extern uint8_t xdata g_rom_cost_gma_entry_addr[3];
extern uint8_t xdata g_rom_cost_bankA_entry_addr[3];
extern uint8_t xdata g_rom_cost_bankB_entry_addr[3];
extern uint8_t xdata g_rom_cost_dgc_entry_addr[3];
extern uint8_t xdata g_spi_sta_buf[3];
//extern uint8_t xdata g_csot_sup_fun[3];

// i2c slave
//extern bool g_i2c2_int_flg;
extern bool mcu_i2c2spi_cmd_init;
extern bool mcu_i2c2spi_cmd_doing;
extern bool g_i2crecv_csot_flg;
extern bool g_i2c_recv_done_flg;
extern uint8_t xdata g_i2c_proc_dev_fun;
extern uint8_t xdata g_mcu_i2c2spi_dev_adr;
extern uint8_t xdata mcu_i2c2spi_cmd_buf_idx;
extern uint8_t xdata mcu_i2c2spi_cmd_buf_len;
extern uint8_t xdata mcu_i2c2spi_reply_data;
extern uint8_t xdata *mcu_i2c2spi_cmd_buf;
extern uint16_t xdata mcu_i2c2spi_recv_len;
extern uint16_t xdata mcu_i2c_slv_cur_rw_ptr;
//extern uint32_t xdata g_cboard_flash_size;
extern uint32_t xdata g_xboard_flash_size;

// i2c GC
extern bool g_recv_i2c_gc_flg;

extern bool g_flash_type;
// TCON DL Status
//extern volatile uint8_t xdata tcon_reg_dl_finish;
// TCON Reset
//extern volatile uint8_t xdata tcon_r_sw_rstn; // 0xF800
// TCON Reset Status
//extern volatile uint8_t xdata tcon_inpron_dbgr[2]; // 0xF82F
// I2C Bypass Mode 
//extern volatile uint8_t xdata mcu_i2c_two_pin_sel;

// CSOT Gamma Register
struct r_gam_dt_comm_buf
{
    uint8_t r_gam_dt_7_0;
    uint8_t r_gam_dt_15_8;
    uint8_t r_gam_dt_23_16;
};

extern volatile struct r_gam_dt_comm_buf xdata g_p2p_reg_015Ah_r_gam_dt0;
extern volatile struct r_gam_dt_comm_buf xdata csot_usit_gamma_hw_1to3_fiti;
extern volatile struct r_gam_dt_comm_buf xdata csot_usit_gamma_hw_3to1_fiti;
extern volatile uint8_t xdata csot_usit_gamma_hw_cal_en;
extern volatile uint8_t xdata csot_usit_gamma_hw_1to3_csot;
extern volatile uint8_t xdata csot_usit_gamma_hw_3to1_csot;
#endif
