#ifndef __MCU_MISC_H__
#define __MCU_MISC_H__


void mcu_init(void);
void mcu_set_power_saving_mode(void);
void mcu_dl_get_ee_path(void);
void mcu_softrst(void);
#endif
