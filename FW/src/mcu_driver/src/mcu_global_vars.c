/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: MCU Global Var
		Platform: 8051
		Author: Eric Lee, 2021/04/06
		Description: MCU global var
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"
#include "mcu_global_vars.h"
// *******************************************************************
// *******************************************************************
// define
#define SPI_CFG_BASE                        0xFF00 
#define I2C_CFG_BASE                        0xFF60  
#define CSOT_USIT_GAMMA_HW_SPD_UP_BASE      0xFFD0
#define CSOT_CKS_PMIC_STATUS_BASE			0xFFF0

//#define HW_DL_RESULT_BASE 					0xFF20
// HW Pause
//#define MCU_DL_CFG_BASE 		0xF100
//#define MCU_DL_CFG_BASE 		0xD990
#define RECV_I2C_BUF_BASE		0xF000
#define MCU_GLOBAL_BASE			0xF080
#define MCU_DL_CFG_BASE 		0xF100
#define COMMON_HDR_BASE			0xF10A   // F10A ~ F1C9
#define DVCOM_DATA_BASE			0xF1CA
//#define MCU_DL_CFG_BASE 		0xF1C0 
//#define COMMON_HDR_BASE			0xF100
// *******************************************************************
// CSOT Base 
//#define COMMON_HDR_BASE						(0xF10A)				// pmic common header (00h ~ 2Ah)+ usi gamma (0x30 ~ 0x40)
//#define GMA_DATA_BASE          				(COMMON_HDR_BASE + 0x30)
//#define PMIC_DATA_BASE          			(COMMON_HDR_BASE + 0x40)
//#define PMIC_CRC_BASE						(PMIC_DATA_BASE + 0x60)
//#define RTPM_CFG_BASE						(PMIC_DATA_BASE + 0x80)
//#define RTPM_I2C_WRITE_BUF_BASE				(RTPM_CFG_BASE + 0x04)
//#define RTPM_I2C_READ_BUF_BASE				(RTPM_I2C_WRITE_BUF_BASE + 0x40)
// *******************************************************************

uint8_t xdata g_i2c2spi_read_buf[I2C_READ_BUF_SZ] 				_at_ (RECV_I2C_BUF_BASE + 0x00);

volatile union mcu_entry_cfg_0 xdata g_mcu_entry_cfg_0_u  _at_ (MCU_DL_CFG_BASE + 0x00); 
volatile union mcu_entry_cfg_1 xdata g_mcu_entry_cfg_1_u  _at_ (MCU_DL_CFG_BASE + 0x02); 
volatile union mcu_entry_cfg_2 xdata g_mcu_entry_cfg_2_u  _at_ (MCU_DL_CFG_BASE + 0x04); 
volatile union mcu_entry_cfg_3 xdata g_mcu_entry_cfg_3_u  _at_ (MCU_DL_CFG_BASE + 0x06); 
volatile union mcu_entry_cfg_4 xdata g_mcu_entry_cfg_4_u  _at_ (MCU_DL_CFG_BASE + 0x08); 

volatile mcu_dl_vcom_cfg xdata g_mcu_dl_vcom_cfg	_at_ (DVCOM_DATA_BASE + 0x00);
uint16_t xdata mcu_global_buf_idx;
uint16_t xdata g_i2c_recv_len;
bool g_spi_path_flg;
bool g_i2c0_spi1_sel;

volatile uint8_t xdata mcu_i2c_pri_addr_2               _at_ I2C_CFG_BASE;
volatile uint8_t xdata mcu_i2c_pri_addr_3               _at_ (I2C_CFG_BASE + 0x01);
volatile uint8_t xdata mcu_i2c_pri_addr_4               _at_ (I2C_CFG_BASE + 0x02);
volatile uint8_t xdata mcu_i2c_pri_addr_5               _at_ (I2C_CFG_BASE + 0x03);
volatile uint8_t xdata mcu_i2c_pri_addr_6               _at_ (I2C_CFG_BASE + 0x04);

volatile uint8_t xdata mcu_i2c_sec_addr_2               _at_ (I2C_CFG_BASE + 0x63);
volatile uint8_t xdata mcu_i2c_sec_addr_3               _at_ (I2C_CFG_BASE + 0x64);
volatile uint8_t xdata mcu_i2c_sec_addr_4               _at_ (I2C_CFG_BASE + 0x65);
volatile uint8_t xdata mcu_i2c_sec_addr_5               _at_ (I2C_CFG_BASE + 0x66);
volatile uint8_t xdata mcu_i2c_sec_addr_6               _at_ (I2C_CFG_BASE + 0x67);

#ifdef MCU_SPI_FUN
bool tcon_spi_dual_mode = 0;                            // single mode for default
volatile uint8_t xdata mcu_spi_rw_addr_h                _at_ (SPI_CFG_BASE + 0x00);
volatile uint8_t xdata mcu_spi_rw_addr_l                _at_ (SPI_CFG_BASE + 0x01);
volatile uint8_t xdata mcu_spi_rw_cfg                   _at_ (SPI_CFG_BASE + 0x02);
volatile uint8_t xdata mcu_spi_rw_len_h                 _at_ (SPI_CFG_BASE + 0x03);
volatile uint8_t xdata mcu_spi_rw_len_l                 _at_ (SPI_CFG_BASE + 0x04);
volatile uint8_t xdata mcu_spi_chksum_h                	_at_ (SPI_CFG_BASE + 0x05);
volatile uint8_t xdata mcu_spi_chksum_l                	_at_ (SPI_CFG_BASE + 0x06);
#endif

//volatile union rw_ff20_reg xdata g_hw_dl_result               _at_ (HW_DL_RESULT_BASE + 0x0);

uint8_t xdata mcu_global_buf[MCU_GLOBAL_BUF_SZ] _at_ MCU_GLOBAL_BASE;

// *******************************************************************
// *******************************************************************
// HW Pause
volatile union rw_sys_data_0D  xdata g_rw_sys_data_0D_u                 _at_    (SPI_CFG_BASE + 0x0D);
volatile union r_sys_data_0E  xdata g_r_sys_data_0E_u                   _at_    (SPI_CFG_BASE + 0x0E);
volatile union rw_sys_data_0F  xdata g_rw_sys_data_0F_u                 _at_    (SPI_CFG_BASE + 0x0F);
//volatile uint16_t xdata g_rw_set_spi_en_byte_cnt                        _at_    (SPI_CFG_BASE + 0x10);
// *******************************************************************
// *******************************************************************
volatile union r_sys_data_10  xdata g_r_sys_data_10_u                 _at_    (SPI_CFG_BASE + 0x10);
volatile union rw_sys_data_12  xdata g_rw_sys_data_12_u                 _at_    (SPI_CFG_BASE + 0x12);
volatile union r_sys_data_13 xdata g_r_sys_data_13_u                    _at_    (SPI_CFG_BASE + 0x13);
volatile union r_sys_data_20 xdata g_r_sys_data_20_u                    _at_    (SPI_CFG_BASE + 0x20);
volatile union r_sys_data_21 xdata g_r_sys_data_21_u                    _at_    (SPI_CFG_BASE + 0x21);
// *******************************************************************
// *******************************************************************
// EEPROM PATH
//bool g_vcom_sel_path;
//bool g_edid_sel_path;
/*bool g_spi_i2c_path;*/
bool g_cfg_rom_sel_path; 
// *******************************************************************
// *******************************************************************
// CSOT
//volatile struct r_comm_hdr_buf xdata g_comm_hdr_buf						_at_ (COMMON_HDR_BASE + 0x00);
volatile struct r_comm_hdr_buf xdata g_comm_hdr_buf						_at_ (COMMON_HDR_BASE);

//uint8_t xdata g_csot_gma_buf[0x10]										_at_ (GMA_DATA_BASE + 0x00);
uint8_t xdata g_csot_gma_buf[0x10]										_at_ (COMMON_HDR_BASE + 0x30);
//uint8_t xdata g_pmic_bank_a_buf[0x30] 									_at_ (PMIC_DATA_BASE + 0x00);
//uint8_t xdata g_pmic_bank_b_buf[0x30] 									_at_ (PMIC_DATA_BASE + 0x30);
uint8_t xdata g_pmic_bank_a_buf[0x30] 									_at_ (COMMON_HDR_BASE + 0x60);
uint8_t xdata g_pmic_bank_b_buf[0x30] 									_at_ (COMMON_HDR_BASE + 0x90);

uint8_t xdata g_rw_rtpm_i2c_write_len;
uint8_t xdata g_rw_rtpm_i2c_read_len;


uint8_t* xdata g_pmic_rw_ptr;

//uint8_t xdata g_i2c2spi_read_buf[I2C_READ_BUF_SZ] 				_at_ (0xE800 + 0x00);

volatile union rw_mcu_pmic_crc xdata g_rw_mcu_pmic_bankA_crc;
volatile union rw_mcu_pmic_crc xdata g_rw_mcu_pmic_bankB_crc; 
// *****************************************************************************************************************************
// *****************************************************************************************************************************
volatile union rw_hw_reg_FFF0h xdata g_rw_hw_reg_FFF0h       			_at_    (CSOT_CKS_PMIC_STATUS_BASE + 0x00);
volatile uint8_t xdata g_rw_hw_reg_FFF1h_pmic_reg_0       				_at_    (CSOT_CKS_PMIC_STATUS_BASE + 0x01);
volatile uint8_t xdata g_rw_hw_reg_FFF2h_pmic_reg_1       				_at_    (CSOT_CKS_PMIC_STATUS_BASE + 0x02);
volatile uint8_t xdata g_rw_hw_reg_FFF3h_pmic_reg_2       				_at_    (CSOT_CKS_PMIC_STATUS_BASE + 0x03);
volatile uint8_t xdata g_rw_hw_reg_FFF4h_pmic_reg_3       				_at_    (CSOT_CKS_PMIC_STATUS_BASE + 0x04);
volatile uint8_t xdata g_rw_hw_reg_FFF5h_pmic_crc_h_7_0       			_at_    (CSOT_CKS_PMIC_STATUS_BASE + 0x05);
volatile uint8_t xdata g_rw_hw_reg_FFF6h_pmic_crc_h_15_8       			_at_    (CSOT_CKS_PMIC_STATUS_BASE + 0x06);
volatile uint8_t xdata g_rw_hw_reg_FFF7h_pmic_crc_l_7_0       			_at_    (CSOT_CKS_PMIC_STATUS_BASE + 0x07);
volatile uint8_t xdata g_rw_hw_reg_FFF8h_pmic_crc_l_15_8       			_at_    (CSOT_CKS_PMIC_STATUS_BASE + 0x08);
volatile union r_hw_reg_FFF9h xdata g_r_hw_reg_FFF9h       				_at_    (CSOT_CKS_PMIC_STATUS_BASE + 0x09);

// *****************************************************************************************************************************
// *****************************************************************************************************************************
uint8_t xdata g_rom_entry_addr[3] = {0,0,0};
uint8_t xdata g_entry_cks_tmp[14]  _at_ 0xF780;

uint8_t xdata g_fw_ver _at_ 0xF7F0;
// *******************************************************************
// *******************************************************************
//bool g_hw_dl_finish_flg;
uint8_t xdata mcu_wait_dl_tim_100us;
uint16_t xdata mcu_wait_dl_tim_1ms;
// *******************************************************************
// *******************************************************************
//uint8_t xdata g_i2c_mode = 0;
//bool edp_ioa_mode_en_flag;
bool g_mcu_int_flg;
//bool g_mcu_dbg_mode_flg = 0;
uint8_t xdata g_rom_cost_hdr_entry_addr[3] = {0,0,0};
uint8_t xdata g_rom_cost_gma_entry_addr[3] = {0,0,0};
uint8_t xdata g_rom_cost_bankA_entry_addr[3] = {0,0,0};
uint8_t xdata g_rom_cost_bankB_entry_addr[3] = {0,0,0};
uint8_t xdata g_rom_cost_dgc_entry_addr[3] = {0,0,0};
uint8_t xdata g_spi_sta_buf[3];

// i2c slave
bool mcu_i2c2spi_cmd_init;
bool mcu_i2c2spi_cmd_doing;
bool g_i2crecv_csot_flg;
bool g_i2c_recv_done_flg;
uint8_t xdata g_i2c_proc_dev_fun;
uint8_t xdata g_mcu_i2c2spi_dev_adr;
uint8_t xdata mcu_i2c2spi_cmd_buf_idx = 0;
uint8_t xdata mcu_i2c2spi_cmd_buf_len;
uint8_t xdata mcu_i2c2spi_reply_data;
uint8_t xdata *mcu_i2c2spi_cmd_buf;
uint16_t xdata mcu_i2c2spi_recv_len;
uint16_t xdata mcu_i2c_slv_cur_rw_ptr;
uint32_t xdata g_xboard_flash_size = 0;

// i2c GC
bool g_recv_i2c_gc_flg = 0;

bool g_flash_type;
// DL Status
//volatile uint8_t xdata tcon_reg_dl_finish    _at_   (0xF818); // 0xF818

// TCON Reset 
//volatile uint8_t xdata tcon_r_sw_rstn        _at_   (0xF800); // 0xF800

// TCON Reset Status
//volatile uint8_t xdata tcon_inpron_dbgr[2]   _at_   (0xF82F); // 0xF82F

//volatile uint8_t xdata mcu_i2c_two_pin_sel   _at_   (0xF828); // mcu_i2c_two_pin_sel
// g_rw_dbg_mux_0028h_mcu_top_dbgr_17

// CSOT Gamma Register
volatile uint8_t xdata csot_usit_gamma_hw_cal_en                            _at_ (CSOT_USIT_GAMMA_HW_SPD_UP_BASE + 0x00);  // bit0 : 1to3 enable, bit1 : 3to1 enable.
volatile uint8_t xdata csot_usit_gamma_hw_1to3_csot                         _at_ (CSOT_USIT_GAMMA_HW_SPD_UP_BASE + 0x01);
volatile struct r_gam_dt_comm_buf xdata csot_usit_gamma_hw_1to3_fiti        _at_ (CSOT_USIT_GAMMA_HW_SPD_UP_BASE + 0x02);
volatile struct r_gam_dt_comm_buf xdata csot_usit_gamma_hw_3to1_fiti        _at_ (CSOT_USIT_GAMMA_HW_SPD_UP_BASE + 0x05);
volatile uint8_t xdata csot_usit_gamma_hw_3to1_csot                         _at_ (CSOT_USIT_GAMMA_HW_SPD_UP_BASE + 0x08);