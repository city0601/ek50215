#include "mcu_fun_def.h"
#include "mcu_dma.h"
#include "mcu_spi.h"
#include "mcu_i2c.h"

#if 0
void mcu_dma_init(void)
{
    __WIN32_EMULATION_PRINTF("[INFO] Doing DMA Initialization...\n");

}
#endif

// *******************************************************************
//  Function section

void mcu_dma_channel_sel_rot(uint8_t dma_rot, uint8_t dma_synch, uint8_t dma_channel)
{
    dma_rot &= 0x01;
    dma_synch &= 0x01;
    dma_channel &= 0x07;
    DMASEL = (dma_rot<<4) | (dma_synch<<3) | dma_channel;
}

void mcu_dma_src_addr_set(uint32_t dma_src_addr)
{
    DMAS0 = dma_src_addr & 0xff;
    DMAS1 = (dma_src_addr >> 8) & 0xff;
    DMAS2 = (dma_src_addr >> 16) & 0xff;
}

void mcu_dma_tar_addr_set(uint32_t dma_tar_addr)
{
    DMAT0 = dma_tar_addr & 0xff;
    DMAT1 = (dma_tar_addr >> 8) & 0xff;
    DMAT2 = (dma_tar_addr >> 16) & 0xff;
}

void mcu_dma_cnt_set(uint32_t dma_len)
{
    DMAC0 = dma_len & 0xff;
    DMAC1 = (dma_len >> 8) & 0xff;
    DMAC2 = (dma_len >> 16) & 0xff;
}

void mcu_dma_mode0_set(uint8_t tar_space, uint8_t tar_addr_ctrl, uint8_t src_space, uint8_t src_addr_ctrl)
{
    tar_addr_ctrl &= 0x03;
    src_space &= 0x03;
    src_addr_ctrl &= 0x03;
    DMAM0 = (tar_space<<6) | (tar_addr_ctrl<<4) | (src_space<<2) | src_addr_ctrl;
}

void mcu_dma_mode1_set(uint8_t dma_ie, uint8_t dma_pri, uint8_t dma_intsel)
{
    dma_ie &= 0x01;
    dma_pri &= 0x01;
    dma_intsel &= 0x1F;
    DMAM1 = (dma_ie << 6) | (dma_pri << 5) | dma_intsel;
}

#if 0
// *******************************************************************
void mcu_dma_demo_0(void)
{
    uint8_t i;
    uint8_t xdata *xptr;

//    D_PAGESEL = 0x01;
    xptr = 0x7000;
    for (i = 0; i < 250; i++) {
        *xptr = i;
        xptr++;
    }

    xptr = 0x7100;
    for (i = 0; i < 250; i++) {
        *xptr = 0;
        xptr++;
    }

    mcu_dma_channel_sel_rot(DMA_ROT_PRIORITY_DISABLE, DMA_ASYNCH, DMA_SEL_CH0);
    mcu_dma_src_addr_set(0x7000);
    mcu_dma_tar_addr_set(0x7100);
    mcu_dma_cnt_set(249);
    mcu_dma_mode0_set(DMA_XDATA, DMA_INC, DMA_XDATA, DMA_INC);
    mcu_dma_mode1_set(DMA_IRQ_DIS, DMA_PRI_LOWER, DMA_TRIGER_OTHERS);
    mcu_dma_start_soft();
    //mcu_dma_wait_until_done();
    xptr = 0x7200;
    i = 0;
    while (!(DMAM1 & 0x80)) {
        *xptr = i;
        xptr++;
        i++;
    }
    mcu_dma_irq_flag_clr();
}


// *******************************************************************
void mcu_dma_demo_1(void)
{
    uint8_t i;
    uint8_t xdata *xptr;
    //uint8_t sec_adr[3];

    xptr = 0x7000;
    for (i = 0; i < 250; i++) {
        *xptr = i;
        xptr++;
    }

    //xptr = 0x7000;
    //sec_adr[0] = 0x00;
    //sec_adr[1] = 0x01;
    //sec_adr[2] = 0x00;
    //mcu_spi_flash_page_program(sec_adr, xptr);
    mcu_dma_channel_sel_rot(DMA_ROT_PRIORITY_DISABLE, DMA_SYNCH, DMA_SEL_CH0);
    mcu_dma_src_addr_set(0x7000);
    mcu_dma_tar_addr_set(0xE3);
    mcu_dma_cnt_set(249);
    mcu_dma_mode0_set(DMA_SFR, DMA_HOLD, DMA_XDATA, DMA_INC);
    mcu_dma_mode1_set(DMA_IRQ_DIS, DMA_PRI_LOWER, DMA_TRIGER_EXT2);

    SPSSN = 0xFE;

    IRCON |= 0x02;
    mcu_dma_irq_flag_clr();
    
    //SPSSN_CHIP_SEL_TCON_FLASH;
}

void mcu_dma_i2c_slv_set_ch0(void)
{
    mcu_dma_channel_sel_rot(DMA_ROT_PRIORITY_ENABLE, DMA_SYNCH, DMA_SEL_CH0);
    mcu_dma_src_addr_set(0xDD);   //I2CSTA
    //mcu_dma_src_addr_set(0xD5);   //I2C2STA
    mcu_dma_tar_addr_set(0xF400);
    mcu_dma_cnt_set(0);
    mcu_dma_mode0_set(DMA_XDATA, DMA_HOLD, DMA_SFR, DMA_HOLD);
    mcu_dma_mode1_set(DMA_IRQ_EN, DMA_PRI_HIGHER, DMA_TRIGER_EXT7);
}

void mcu_dma_i2c_slv_set_ch1(void)
{
    //uint8_t xdata *xptr;

    //xptr = 0x1000;

    mcu_dma_channel_sel_rot(DMA_ROT_PRIORITY_ENABLE, DMA_SYNCH, DMA_SEL_CH1);
    //mcu_dma_src_addr_set(0xD2);   //I2C2DAT
    mcu_dma_src_addr_set(0xDA);   //I2CDAT
    mcu_dma_tar_addr_set(0xEFC3);
    mcu_dma_cnt_set(0);
    mcu_dma_mode0_set(DMA_XDATA, DMA_INC, DMA_SFR, DMA_HOLD);
    mcu_dma_mode1_set(DMA_IRQ_DIS, DMA_PRI_LOWER, DMA_TRIGER_EXT12);
}
#endif

void mcu_dma_i2c_slv_set_ch0(void)			// mem g_i2c2spi_read_buf --> I2C2DAT
{
    uint16_t src_ptr ;

    mcu_dma_channel_sel_rot(DMA_ROT_PRIORITY_ENABLE, DMA_SYNCH, DMA_SEL_CH0);
    //src_ptr = (uint16_t)&mcu_i2c2spi_reply_data;
    src_ptr = (uint16_t)&g_i2c2spi_read_buf[0];
    mcu_dma_src_addr_set(src_ptr); // mcu_i2c2spi_reply_data
    mcu_dma_tar_addr_set(0xD2); //I2C2DAT
    mcu_dma_cnt_set(0);
    //mcu_dma_mode0_set(DMA_SFR, DMA_HOLD, DMA_XDATA, DMA_HOLD);
    mcu_dma_mode0_set(DMA_SFR, DMA_HOLD, DMA_XDATA, DMA_INC);
    mcu_dma_mode1_set(DMA_IRQ_DIS, DMA_PRI_HIGHER, DMA_TRIGER_EXT11);
}

void mcu_dma_i2c_slv_set_ch1(void)		
{
    uint16_t tar_ptr;

    mcu_dma_channel_sel_rot(DMA_ROT_PRIORITY_ENABLE, DMA_SYNCH, DMA_SEL_CH1);
    tar_ptr = (uint16_t)&mcu_global_buf[0];
    mcu_dma_src_addr_set(0xD2);   // i2c2dat
	mcu_dma_tar_addr_set(tar_ptr);
    mcu_dma_cnt_set(0);
    mcu_dma_mode0_set(DMA_XDATA, DMA_INC, DMA_SFR, DMA_HOLD);
    mcu_dma_mode1_set(DMA_IRQ_DIS, DMA_PRI_LOWER, DMA_TRIGER_EXT12);
}

#if 0
void mcu_dma_i2c_slv_set_ch2(void)		
{
    uint16_t src_ptr;
    uint16_t tar_ptr;

    mcu_dma_channel_sel_rot(DMA_ROT_PRIORITY_ENABLE, DMA_SYNCH, DMA_SEL_CH2);
    //mcu_dma_src_addr_set(0x0B);   //I2C2DAT
	//mcu_dma_src_addr_set(0xE801);   //g_i2c2spi_read_buf
    //mcu_dma_src_addr_set(0xDA);   //I2CDAT
    //mcu_dma_tar_addr_set(0xEFC2);
    src_ptr = (uint16_t)&g_i2c2spi_read_buf[1];
    tar_ptr = (uint16_t)&mcu_i2c2spi_reply_data;
    
    mcu_dma_src_addr_set(src_ptr);   //g_i2c2spi_read_buf
    mcu_dma_tar_addr_set(tar_ptr);
    mcu_dma_cnt_set(0);
    mcu_dma_mode0_set(DMA_XDATA, DMA_HOLD, DMA_XDATA, DMA_INC);
    mcu_dma_mode1_set(DMA_IRQ_DIS, DMA_PRI_LOWER, DMA_TRIGER_EXT6);
}


void mcu_dma_i2c_slv_set_ch3(void)			// g_i2c2spi_read_buf -- > mem mcu_i2c2spi_reply_data(0xEFC2)
{
    uint16_t src_ptr ;
    uint16_t tar_ptr;

    mcu_dma_channel_sel_rot(DMA_ROT_PRIORITY_ENABLE, DMA_SYNCH, DMA_SEL_CH0);
    src_ptr = (uint16_t)&g_i2c2spi_read_buf[0];
    tar_ptr = (uint16_t)&mcu_i2c2spi_reply_data;
    mcu_dma_src_addr_set(src_ptr); // mcu_i2c2spi_reply_data
    mcu_dma_tar_addr_set(tar_ptr); 
    mcu_dma_cnt_set(0);
    mcu_dma_mode0_set(DMA_SFR, DMA_HOLD, DMA_XDATA, DMA_HOLD);
    mcu_dma_mode1_set(DMA_IRQ_DIS, DMA_PRI_HIGHER, DMA_TRIGER_EXT11);
}
#endif
void mcu_dma_ch0_rst(void)
{
    DMAS0 = 0x00;
	DMAS1 = 0xF0;
	DMAS2 = 0x00;
}

void mcu_dma_set_ch0_adr(uint16_t l_start_adr)
{
    DMAS0 = l_start_adr & 0xFF;
	DMAS1 = (l_start_adr >> 8);
	DMAS2 = 0;
}

void mcu_dma_ch1_rst(void)
{
    DMASEL |= 0x01;
	DMAT0 = 0x80;
	DMAT1 = 0xF0;
	DMAT2 = 0x00;
    //mcu_dma_tar_addr_set(0xEFC3);
    DMASEL &= 0xFE;
}
#if 0
void mcu_dma_ch2_rst(void)
{
    DMASEL |= 0x02;
	DMAS0 = 0x01;
	DMAS1 = 0xE8;
	DMAS2 = 0x00;
    //mcu_dma_tar_addr_set(0xEFC3);
    DMASEL &= 0xFD;
}

void mcu_dma_ch2_rst_cfg(void)
{
    DMASEL |= 0x02;
	DMAS0 = 0x00;
	DMAS1 = 0xE8;
	DMAS2 = 0x00;
    //mcu_dma_tar_addr_set(0xEFC3);
    DMASEL &= 0xFD;
}
#endif