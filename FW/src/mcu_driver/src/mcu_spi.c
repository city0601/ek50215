/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: MCU SPI
		Platform: 8051
		Author: Eric Lee, 2021/04/06
		Description: MCU SPI Driver
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"
#ifdef MCU_SPI_FUN
#include "mcu_spi.h"
#include "mcu_top.h"
#include "mcu_reg51.h"
#include "mcu_int.h"
#ifdef FLASH_CODE
#ifdef LD_LED_DRV_FUN
#include "tcon_spi_leddrv.h"
#endif
#define ONE_FLASH_TYPE  0
#define TCON_FLASH  0
#define DMR_FLASH   1
#define LD_FLASH    2
//#include <intrins.h>          //for _nop_();
#define SPI_CPHA_EN         DL_FINISH |= 0x80
#define SPI_CPHA_DIS        DL_FINISH &= ~0x80

#define SPCON_SPR2          (0x01 << 7)
#define SPCON_SPEN          (0x01 << 6)
#define SPCON_SSDIS         (0x01 << 5)
#define SPCON_MSTR          (0x01 << 4)
#define SPCON_CPOL          (0x01 << 3)
#define SPCON_CPHA          (0x01 << 2)
#define SPCON_SPR1          (0x01 << 1)
#define SPCON_SPR0          (0x01)

#define SPCON_MASTER_EN_DIV_2       (SPCON = (SPCON_SPEN | SPCON_MSTR ))                                    // system / 2
#define SPCON_MASTER_EN_DIV_4       (SPCON = (SPCON_SPEN | SPCON_MSTR | SPCON_SPR0))                        // system / 4
#define SPCON_MASTER_EN_DIV_8       (SPCON = (SPCON_SPEN | SPCON_MSTR | SPCON_SPR1))                        // system / 8
#define SPCON_MASTER_EN_DIV_16      (SPCON = (SPCON_SPEN | SPCON_MSTR | SPCON_SPR1 | SPCON_SPR0))           // system / 16
#define SPCON_MASTER_EN_DIV_32      (SPCON = (SPCON_SPEN | SPCON_MSTR | SPCON_SPR2 ))                       // system / 32
#define SPCON_MASTER_EN_DIV_64      (SPCON = (SPCON_SPEN | SPCON_MSTR | SPCON_SPR2 | SPCON_SPR0))           // system / 64
//#define SPCON_MASTER_EN     (SPCON = (SPCON_SPEN | SPCON_MSTR | SPCON_SPR1))        // 15M
//#define SPCON_MASTER_EN     (SPCON = (SPCON_SPEN | SPCON_MSTR))        // 60M

#define SPI_FLASH_STATUS1_SRP0          (0x01 << 7)   
#define SPI_FLASH_STATUS1_SEC           (0x01 << 6)
#define SPI_FLASH_STATUS1_TB            (0x01 << 5)
#define SPI_FLASH_STATUS1_BP2           (0x01 << 4)
#define SPI_FLASH_STATUS1_BP1           (0x01 << 3)
#define SPI_FLASH_STATUS1_BP0           (0x01 << 2)
#define SPI_FLASH_STATUS1_WEL           (0x01 << 1)
#define SPI_FLASH_STATUS1_BUSY          (0x01)

#define SPI_FLASH_STATUS2_SUS           (0x01 << 7)
#define SPI_FLASH_STATUS2_QE            (0x01 << 1)
#define SPI_FLASH_STATUS2_SRP1          (0x01)

#define SPSSN_CHIP_SEL_NONE           (SPSSN = 0xFF) 
#define SPSSN_CHIP_SEL_TCON_FLASH     (SPSSN = 0x7F)        // bit7
#define SPSSN_CHIP_SEL_DMU_FLASH      (SPSSN = 0xBF)        // bit6

#define SPSTA_SPIF      (0x01 << 7)
#define SPSTA_WCOL      (0x01 << 6)
#define SPSTA_SSERR     (0x01 << 5)
#define SPSTA_MODF      (0x01 << 4)

#define SPISET_AUTO_DMA_BY_HW   (0x01 << 7)
#define SPISET_CHKSUM_EN        (0x01 << 5)
#define SPISET_HW_WRITE_MODE    (0x01 << 4)
#define SPISET_HW_READ_MODE     (0x01 << 2)
#define SPISET_CPHA_H_MODE      (0x01 << 1)
#define SPISET_IS_DUAL_MODE     (0x01)

#define SPISET_CLEAR            (SPISET = 0x00 | SPISET_CHKSUM_EN)
#define SPISET_SET_WRITE_MODE   (SPISET = SPISET_HW_WRITE_MODE | SPISET_CHKSUM_EN)
#define SPISET_SET_READ_MODE    (SPISET = SPISET_HW_READ_MODE | SPISET_CHKSUM_EN)
#define SPISET_SET_HIGH_SPEED_READ_MODE     (SPISET = SPISET_CPHA_H_MODE | SPISET_HW_READ_MODE | SPISET_CHKSUM_EN)

#define SPISET_SET_READ_DUAL    (SPISET = SPISET_HW_READ_MODE | SPISET_IS_DUAL_MODE | SPISET_CHKSUM_EN)
#define SPISET_SET_HIGH_SPEED_READ_DUAL     (SPISET = SPISET_HW_READ_MODE | SPISET_CPHA_H_MODE | SPISET_IS_DUAL_MODE | SPISET_CHKSUM_EN)

#define MCU_SPI_WAIT_SPIF_TIMEOUT   (60000)
//#define MCU_SPI_WAIT_SPIF_TIMEOUT   (10000)

#define SPSTA_SPIF_IS_SET   (SPSTA & SPSTA_SPIF)

static uint8_t mcu_spi_read_data;

static uint8_t mcu_spi_flash_id[2];

static uint8_t g_spi_set_clk;

//------------------------------------
uint8_t leddrv_spi_state = 0;

void mcu_spi_set_clk(uint8_t clk)
{
    g_spi_set_clk = clk;
    if(clk == CLK_54M)                  // 60MHz
        SPCON_MASTER_EN_DIV_2;
    else if(clk == CLK_27M)             // 30MHz
        SPCON_MASTER_EN_DIV_4;
    else if(clk == CLK_13_5M)           // 15MHz
        SPCON_MASTER_EN_DIV_8;
    else if(clk == CLK_6_75M)           // 7.5MHz
        SPCON_MASTER_EN_DIV_16;
    else if(clk == CLK_3_3M)            // 3.75MHz
        SPCON_MASTER_EN_DIV_32;
    else if(clk == CLK_1_68M)           // 1.875MHz
        SPCON_MASTER_EN_DIV_64;
    else
        SPCON_MASTER_EN_DIV_8;          // 7.5MHz
}

static void mcu_spi_spssn_chip_sel(uint8_t cs)
{
#if 0
    if(~cs)
    {
        mcu_spi_set_clk(g_rw_mcu_top_0032h.bits.r_c_board_spi_freq_sel);
        SPSSN_CHIP_SEL_TCON_FLASH;
    }
    else
    {
        mcu_spi_set_clk(g_rw_mcu_top_0032h.bits.r_x_board_spi_freq_sel);
        SPSSN_CHIP_SEL_DMU_FLASH;   
    }
#endif
    if(g_flash_type == ONE_FLASH_TYPE)
    {
        if(cs == TCON_FLASH || cs == DMR_FLASH)  // tcon or demura flash
        {
            g_rw_mcu_top_0043h.bits.Mcu_spi_sel = 0;
            mcu_spi_set_clk(g_rw_mcu_top_0032h.bits.r_c_board_spi_freq_sel);
            SPSSN_CHIP_SEL_TCON_FLASH;
        }
        else if(cs == LD_FLASH)
        {
            g_rw_mcu_top_0043h.bits.Mcu_spi_sel = 0;
            mcu_spi_set_clk(g_rw_mcu_top_0032h.bits.r_c_board_spi_freq_sel);
        }
    }
    else
    {
        if(cs == TCON_FLASH)
        {
            g_rw_mcu_top_0043h.bits.Mcu_spi_sel = 0;
            mcu_spi_set_clk(g_rw_mcu_top_0032h.bits.r_c_board_spi_freq_sel);
            SPSSN_CHIP_SEL_TCON_FLASH;
        }
        else if(cs == DMR_FLASH)
        {
            g_rw_mcu_top_0043h.bits.Mcu_spi_sel = 1;
            mcu_spi_set_clk(g_rw_mcu_top_0032h.bits.r_x_board_spi_freq_sel);
            SPSSN_CHIP_SEL_DMU_FLASH;   
        }
        else if(cs == LD_FLASH)    // LD
        {
            g_rw_mcu_top_0043h.bits.Mcu_spi_sel = 0;
            mcu_spi_set_clk(g_rw_mcu_top_0032h.bits.r_c_board_spi_freq_sel);
        }
    }
		
}

void mcu_spi_acc_bus(bool en)
{
    g_rw_sys_00D4h.bits.r_rom_acc_fw_set = en;   
}

void mcu_spi_init(void)
{
    __WIN32_EMULATION_PRINTF("[INFO] Doing SPI Initialization...\n");

    SPSSN_CHIP_SEL_NONE;
    mcu_spi_set_clk(g_rw_mcu_top_0032h.bits.r_c_board_spi_freq_sel);
    //mcu_spi_set_clk(CLK_13_5M);   // test 
    tcon_spi_dual_mode = g_rw_mcu_top_0011h.bits.r_spi_read_mode;
    g_spi_path_flg = 0; // tcon flash
    SPI_WP_PIN = 0;     // write protected
    SPI_HOLD_PIN = 1;   // unhold
}
//#ifdef SPI_WP_HW_CTRL
#if 0
static void mcu_set_flash_wp_pin(bool pin)
{
	SPI_WP_PIN = pin;
    return;
}
#endif

static bool mcu_spi_wait_transfer_done(void)
{
    uint16_t timeout = MCU_SPI_WAIT_SPIF_TIMEOUT;

    while (timeout--) {
        if (SPSTA_SPIF_IS_SET) {
            mcu_spi_read_data = SPDAT;
            return TRUE;
        }
    }
    return FALSE;
}
#if 0
static bool mcu_spi_write_byte_dont_wait_done(uint8_t w_dat)
{
    SPDAT = w_dat;
    return TRUE;
}
#endif

static bool mcu_spi_write_byte_done(uint8_t w_dat)
{
    SPDAT = w_dat;
    return mcu_spi_wait_transfer_done();
}

static uint8_t mcu_spi_read_byte_done(void)
{
    SPDAT = 0x00; // dummy cycle
    if (mcu_spi_wait_transfer_done()) {
        return mcu_spi_read_data;
    }
    return 0;
}

static bool mcu_spi_flash_read_info(uint8_t cmd,uint8_t *buf, uint8_t w_dmy_cnt, uint8_t r_cnt, uint8_t cs)
{
    uint16_t ptr;
    uint8_t l_spi_clk;
	  uint8_t i = 0;

	SPISET_CLEAR;
    ptr = (uint16_t)buf;
    mcu_spi_rw_addr_h = (uint8_t)(ptr >> 8);
    mcu_spi_rw_addr_l = (uint8_t)(ptr);
    mcu_spi_rw_len_h = (uint8_t)(0);
    mcu_spi_rw_len_l = (uint8_t)(r_cnt);
		//SPI_SEL_MCU |= 0x80;
	//mcu_spi_sw_path(cs);
    mcu_spi_spssn_chip_sel(cs);
    //SPI_SEL_MCU |= 0x80;
    
   /* if(~cs)
        l_spi_clk = g_rw_mcu_top_0032h.bits.r_c_board_spi_freq_sel; 
    else
        l_spi_clk = g_rw_mcu_top_0032h.bits.r_x_board_spi_freq_sel; */
	l_spi_clk = g_spi_set_clk;	
	mcu_spi_write_byte_done(cmd);
		
		for (i = 0; i < w_dmy_cnt; i++) {
        mcu_spi_write_byte_done(0x00);
    }
		
	if(~l_spi_clk)  // 54MHZ
    {
        if(g_rw_mcu_top_0011h.bits.r_spi_cpha_mode)
            SPI_CPHA_EN;
        SPISET_SET_HIGH_SPEED_READ_MODE;
    }
    else
    {
        SPISET_SET_READ_MODE;
    }

    SPI_SEL_MCU |= 0x80;
	
	mcu_spi_write_byte_done(0x00); // dummy, trigger DMA and wait until done

    //mcu_spi_write_byte_dont_wait_done(0x00);

    SPSSN_CHIP_SEL_NONE;

    SPI_CPHA_DIS;

    SPISET_CLEAR;
		
	SPI_SEL_MCU &= 0x7F;

    return TRUE;
#if 0
    bool exec_ok = FALSE;
    uint8_t i;

    mcu_spi_sw_path(cs);
    mcu_spi_spssn_chip_sel(cs);

    if (mcu_spi_write_byte_done(cmd)) {
        for (i = 0; i < w_dmy_cnt; i++) {
            mcu_spi_write_byte_done(0x00);
        }
        for (i = 0; i < r_cnt; i++) {
            //mcu_global_buf[i] = mcu_spi_read_byte_done();
            g_spi_sta_buf[i] = mcu_spi_read_byte_done();
        }
        exec_ok = TRUE;
    }
    SPSSN_CHIP_SEL_NONE;

    return exec_ok;
    #endif
}

static bool mcu_spi_flash_write_cmd(uint8_t cmd, uint8_t cs)
{
    bool exec_ok = FALSE;

    /*if(~cs)
        SPSSN_CHIP_SEL_TCON_FLASH;
    else
        SPSSN_CHIP_SEL_DMU_FLASH;  */
    //mcu_spi_sw_path(cs);
    mcu_spi_spssn_chip_sel(cs);

    if (mcu_spi_write_byte_done(cmd)) {
        exec_ok = TRUE;
    }

    SPSSN_CHIP_SEL_NONE;

    return exec_ok;
}

static bool mcu_spi_flash_write_cmd_param(uint8_t cmd, uint8_t param_cnt, uint8_t cs)
{
    bool exec_ok = FALSE;
    uint8_t i;

    /*if(~cs)
        SPSSN_CHIP_SEL_TCON_FLASH;
    else
        SPSSN_CHIP_SEL_DMU_FLASH;  */
    //mcu_spi_sw_path(cs);
    mcu_spi_spssn_chip_sel(cs);

    if (mcu_spi_write_byte_done(cmd)) {
        for (i = 0; i < param_cnt; i++) {
            mcu_spi_write_byte_done(g_spi_sta_buf[i]);
        }
        exec_ok = TRUE;
    }
    SPSSN_CHIP_SEL_NONE;

    return exec_ok;
}

static bool mcu_spi_flash_general_erase(uint8_t* i2c_recv_buf, uint8_t cs)
{
    if (mcu_spi_flash_write_cmd(SPI_FLASH_CMD_WRITE_ENABLE, cs) == FALSE) {
        return FALSE;
    }
    g_spi_sta_buf[0] = i2c_recv_buf[1];
    g_spi_sta_buf[1] = i2c_recv_buf[2];
    g_spi_sta_buf[2] = i2c_recv_buf[3];
    return mcu_spi_flash_write_cmd_param(i2c_recv_buf[0], 3, cs);
}

static bool mcu_spi_flash_general_fast_read(bool dual, uint8_t *sec_adr, uint8_t *buf_adr, uint16_t len, uint8_t cs)
{
    uint16_t ptr;
    uint8_t l_spi_clk;
	SPISET_CLEAR;

    ptr = (uint16_t)buf_adr;
    mcu_spi_rw_addr_h = (uint8_t)(ptr >> 8);
    mcu_spi_rw_addr_l = (uint8_t)(ptr);
    mcu_spi_rw_len_h = (uint8_t)(len >> 8);
    mcu_spi_rw_len_l = (uint8_t)(len);

    /*if(~cs)
        SPSSN_CHIP_SEL_TCON_FLASH;
    else
        SPSSN_CHIP_SEL_DMU_FLASH; */
    //mcu_spi_sw_path(cs);
    mcu_spi_spssn_chip_sel(cs);
    l_spi_clk = g_spi_set_clk;
    /*if(~cs)
        l_spi_clk = g_rw_mcu_top_0032h.bits.r_c_board_spi_freq_sel; 
    else
        l_spi_clk = g_rw_mcu_top_0032h.bits.r_x_board_spi_freq_sel; */


    if (~dual) {
        mcu_spi_write_byte_done(SPI_FLASH_CMD_FAST_READ);
    } else {
        mcu_spi_write_byte_done(SPI_FLASH_CMD_FAST_READ_DUAL);
    }
    mcu_spi_write_byte_done(sec_adr[0]);
    mcu_spi_write_byte_done(sec_adr[1]);
    mcu_spi_write_byte_done(sec_adr[2]);
    mcu_spi_write_byte_done(0x00); // dummy
    if (~dual) {
        if(~l_spi_clk)  // 54MHZ
        {
            if(g_rw_mcu_top_0011h.bits.r_spi_cpha_mode)
                SPI_CPHA_EN;
            SPISET_SET_HIGH_SPEED_READ_MODE;
        }
        else
        {
            SPISET_SET_READ_MODE;
        }
    }
    else {
        if(~l_spi_clk)      // Dual 54M
        {
            if(g_rw_mcu_top_0011h.bits.r_spi_cpha_mode)
                SPI_CPHA_EN;
            SPISET_SET_HIGH_SPEED_READ_DUAL;
        }
        else
        {
            SPISET_SET_READ_DUAL;
        }
    }
    mcu_spi_write_byte_done(0x00); // dummy, trigger DMA and wait until done

    //mcu_spi_write_byte_dont_wait_done(0x00);

    SPSSN_CHIP_SEL_NONE;

    SPI_CPHA_DIS;

    SPISET_CLEAR;
    
    return TRUE;
}
bool mcu_spi_flash_read_id(uint8_t cmd,uint8_t *buf, uint8_t cs)
{
    /*uint8_t r_cnt;
    
    if(cmd == SPI_FLASH_CMD_READ_ID_2BYTE)
        r_cnt = 2;
    else if(cmd == SPI_FLASH_CMD_READ_ID_3BYTE)
        r_cnt = 3;*/

	//if(mcu_spi_flash_read_info(cmd, 3, r_cnt, cs) == TRUE)
    if(mcu_spi_flash_read_info(cmd,buf, 0, 4, cs) == TRUE)
        return TRUE;
	else
		return FALSE;
	
}

uint8_t mcu_spi_flash_read_status1(uint8_t cs)
{
if (mcu_spi_flash_read_info(SPI_FLASH_CMD_READ_STATUS1,&g_spi_sta_buf[0], 0, 1, cs) == FALSE) {        
    return 0x00;
}
    //return mcu_global_buf[0];
    return g_spi_sta_buf[0];
}
#if 0
uint8_t mcu_spi_flash_read_status2(uint8_t cs)
{
    if (mcu_spi_flash_read_info(SPI_FLASH_CMD_READ_STATUS2, 0, 1, cs) == FALSE) {
        return 0x00;
    }
    //return mcu_global_buf[0];
    return g_spi_sta_buf[0];
}
#endif

bool mcu_spi_flash_write_status(uint8_t status, uint8_t cs)
{
    bool exec_ok = FALSE;
    if (mcu_spi_flash_write_cmd(SPI_FLASH_CMD_WRITE_ENABLE, cs) == FALSE) {
        return FALSE;
    }

    //mcu_global_buf[0] = status;
    g_spi_sta_buf[0] = status;
    exec_ok = mcu_spi_flash_write_cmd_param(SPI_FLASH_CMD_WRITE_STATUS, 1, cs);
		
    return exec_ok;
}

bool mcu_spi_flash_chip_erase(uint8_t cs)
{
    if (mcu_spi_flash_write_cmd(SPI_FLASH_CMD_WRITE_ENABLE, cs) == FALSE) {
        return FALSE;
    }
    return mcu_spi_flash_write_cmd(mcu_i2c2spi_cmd_buf[0], cs);
    //return mcu_spi_flash_write_cmd(SPI_FLASH_CMD_CHIP_ERASE, cs);
}

bool mcu_spi_flash_erase(uint8_t* recv_buf, uint8_t cs)
{
    return mcu_spi_flash_general_erase(recv_buf, cs);
} 

bool mcu_spi_flash_merge_erase(uint8_t cmd, uint8_t* buf, uint8_t cs)
{
    uint8_t l_recv_buf[4];
    l_recv_buf[0] = cmd;
    l_recv_buf[1] = buf[0];
    l_recv_buf[2] = buf[1];
    l_recv_buf[3] = buf[2];
    return mcu_spi_flash_general_erase(&l_recv_buf[0], cs);
    //return mcu_spi_flash_general_erase(SPI_FLASH_CMD_SECTOR_ERASE, sec_adr_h, sec_adr_m, sec_adr_l, cs);
}

bool mcu_spi_flash_page_program(uint8_t *sec_adr, uint8_t *buf_adr, uint8_t len_h , uint8_t len_l, uint8_t cs)
{
    uint16_t ptr;
   
    if (mcu_spi_flash_write_cmd(SPI_FLASH_CMD_WRITE_ENABLE, cs) == FALSE) {
        return FALSE;
    }

    SPISET_CLEAR;

    ptr = (uint16_t)buf_adr;
    mcu_spi_rw_addr_h = (uint8_t)(ptr >> 8);
    mcu_spi_rw_addr_l = (uint8_t)(ptr);
    mcu_spi_rw_len_h = len_h;
    mcu_spi_rw_len_l = len_l;

    SPISET_SET_WRITE_MODE;

    /*if(~cs)
        SPSSN_CHIP_SEL_TCON_FLASH;
    else
        SPSSN_CHIP_SEL_DMU_FLASH;   */
    //mcu_spi_sw_path(cs);
    mcu_spi_spssn_chip_sel(cs);

    mcu_spi_write_byte_done(SPI_FLASH_CMD_PAGE_PROGRAM);
    mcu_spi_write_byte_done(sec_adr[0]);
    mcu_spi_write_byte_done(sec_adr[1]);
    mcu_spi_write_byte_done(sec_adr[2]);
    mcu_spi_write_byte_done(0x00); // wait SPI DMA Write done

    SPSSN_CHIP_SEL_NONE;

    SPISET_CLEAR;
 
    return TRUE;
}

bool mcu_spi_flash_fast_read(uint8_t *adr, uint8_t *buf, uint16_t len, uint8_t cs)
{
    return mcu_spi_flash_general_fast_read(tcon_spi_dual_mode, adr, buf, len, cs);
}

void mcu_spi_get_flash_size(uint8_t l_cs)
{
    uint32_t l_flash_size = 0;
    if(mcu_spi_flash_read_id(SPI_FLASH_CMD_READ_ID_3BYTE,&g_spi_sta_buf[0], l_cs) == TRUE)    
    {
        l_flash_size =  ((uint32_t)2 << (g_spi_sta_buf[2] -1));
    }

        g_xboard_flash_size = l_flash_size;
}

void mcu_spi_flash_protected(uint8_t l_cs)
{
    mcu_spi_flash_write_status(0x9C , l_cs);
}

void mcu_spi_sw_path(uint8_t path)
{
    /*/
    if(path == SPI_CFG_PATH)
        SPI_SEL_MCU &= ~0x03;
    else        // DMR 
            SPI_SEL_MCU |= 0x01;*/
}
#endif
#ifdef LD_LED_DRV_FUN
//-----------------------------------------------------------------------------------
// SPI to LED Driver
//-----------------------------------------------------------------------------------
bool mcu_spi_read_leddrv(uint8_t *cmd_ptr, uint8_t cmd_len, uint8_t *buf_adr, uint16_t rd_len, uint8_t cs_idx, uint8_t led_num)
{
    uint16_t tmp_16 = 0;
    uint8_t i = 0;

    SPISET_CLEAR;
    //SPI_SEL_MCU = LED_PATH;
    mcu_spi_sw_path(SPI_CFG_PATH);   // 20231003 add / need check
    
    SPSSN = cs_idx;
    // write 6 byte cmd
    for(i = 0 ; i < cmd_len; i++)
        mcu_spi_write_byte_done(*(cmd_ptr + i));
    
    for(i = 0; i < led_num; i++)
    {
        mcu_spi_write_byte_done(0x00);
        mcu_spi_write_byte_done(0x00);
    }

    for(i = 0 ; i < rd_len ; i++)
    {
        buf_adr[i] = mcu_spi_read_byte_done();
    }

    SPISET_CLEAR;

    SPSSN_CHIP_SEL_NONE;

    return TRUE;
}

bool mcu_spi_write_leddrv_fixed(uint8_t *cmd_ptr, uint8_t cmd_len, uint8_t* wr_val, uint16_t wr_len, uint8_t led_num, uint8_t cs_idx, uint8_t cksm_en)
{
    uint16_t i = 0;

    SPISET_CLEAR;
    //SPI_SEL_MCU = LED_PATH;
    mcu_spi_sw_path(SPI_CFG_PATH);   // 20231003 add / need check

    SPSSN = cs_idx;    
    if(cmd_len)
    {
        for(i = 0 ; i < cmd_len; i++)
            mcu_spi_write_byte_done(*(cmd_ptr + i));
    }

    for(i = 0; i < wr_len; i+=2)
    {
        mcu_spi_write_byte_done(wr_val[0]);
        mcu_spi_write_byte_done(wr_val[1]);
    }


    if(cksm_en)
    {
        mcu_spi_write_byte_done(0x00);
        mcu_spi_write_byte_done(0x00);
    }

    //dummy
    for(i = 0; i < (led_num - 1); i++)
    {
        mcu_spi_write_byte_done(0x00);
        mcu_spi_write_byte_done(0x00);
    }

    SPSSN_CHIP_SEL_NONE;

    return TRUE;
}
bool mcu_spi_write_leddrv(uint8_t *cmd_ptr, uint8_t cmd_len, uint8_t *ptr, uint16_t wr_len, uint8_t led_num, uint8_t cs_idx, uint8_t cksm_en)
{
    uint16_t tmp_16 = 0;
    uint16_t  i = 0;
       
    SPISET_CLEAR;
    //SPI_SEL_MCU = LED_PATH;
    mcu_spi_sw_path(SPI_CFG_PATH);   // 20231003 add / need check

    tmp_16 = (uint16_t)ptr;
    mcu_spi_rw_addr_h = (uint8_t)(tmp_16 >> 8);
    mcu_spi_rw_addr_l = (uint8_t)(tmp_16);
    tmp_16 = wr_len + cmd_len;    // must add cmd_len for HW
    mcu_spi_rw_len_h = (uint8_t)(tmp_16 >> 8);
    mcu_spi_rw_len_l = (uint8_t)(tmp_16 & 0xFF);

    //SPSSN_CHIP_SEL_TCON_FLASH;
    SPSSN = cs_idx;
    if(cmd_len)
    {
        for(i = 0 ; i < cmd_len; i++)
            mcu_spi_write_byte_done(*(cmd_ptr + i));
    }
    
    SPI_SEL_MCU |= SPISET_AUTO_DMA_BY_HW;
    SPISET_SET_WRITE_MODE;
    
    mcu_spi_write_byte_done(0x00); // wait SPI DMA Write done
    
    SPI_SEL_MCU &= ~SPISET_AUTO_DMA_BY_HW;
    SPISET_CLEAR;
    
    if(cksm_en)
    {
        // checksum
        mcu_spi_write_byte_done(0x00);
        mcu_spi_write_byte_done(0x00);
    }

    //dummy
    for(i = 0; i < (led_num - 1); i++)
    {
        mcu_spi_write_byte_done(0x00);
        mcu_spi_write_byte_done(0x00);
    }
    
    SPSSN_CHIP_SEL_NONE;

    //SPISET_CLEAR;
    return TRUE;

}

bool mcu_spi_write_leddrv_bc(uint8_t *cmd_ptr, uint8_t cmd_len, uint8_t *ptr, uint16_t wr_len, uint8_t led_num, uint8_t cs_idx, uint8_t cksm_en)
{
    uint16_t tmp_16 = 0;
    uint16_t l_wr_len;
    uint16_t  i = 0;
       
    SPISET_CLEAR;
    //SPI_SEL_MCU = LED_PATH;
    mcu_spi_sw_path(SPI_CFG_PATH);   // 20231003 add / need check

    tmp_16 = (uint16_t)ptr;
    mcu_spi_rw_addr_h = (uint8_t)(tmp_16 >> 8);
    mcu_spi_rw_addr_l = (uint8_t)(tmp_16);
    tmp_16 = wr_len + cmd_len;    // must add cmd_len for HW
    l_wr_len = tmp_16;
    mcu_spi_rw_len_h = (uint8_t)(tmp_16 >> 8);
    mcu_spi_rw_len_l = (uint8_t)(tmp_16 & 0xFF);

    //SPSSN_CHIP_SEL_TCON_FLASH;
    SPSSN = cs_idx;
    if(cmd_len)
    {
        for(i = 0 ; i < cmd_len; i++)
            mcu_spi_write_byte_done(*(cmd_ptr + i));
    }

    // seperate write for each led driver
    tmp_16 = (uint16_t)ptr;
    for(i = 0; i < led_num; i++)
    {
        SPI_SEL_MCU |= SPISET_AUTO_DMA_BY_HW;
        SPISET_SET_WRITE_MODE;
        
        mcu_spi_write_byte_done(0x00); // wait SPI DMA Write done
        
        SPI_SEL_MCU &= ~SPISET_AUTO_DMA_BY_HW;
        SPISET_CLEAR;
        l_wr_len += wr_len;
        mcu_spi_rw_len_h = (uint8_t)(l_wr_len >> 8);
        mcu_spi_rw_len_l = (uint8_t)(l_wr_len & 0xFF);
        //l_wr_len += wr_len;
        tmp_16 += g_block_step;
        mcu_spi_rw_addr_h = (uint8_t)(tmp_16 >> 8);
        mcu_spi_rw_addr_l = (uint8_t)(tmp_16);
    }

    if(cksm_en)
    {
        // checksum
        mcu_spi_write_byte_done(0x00);
        mcu_spi_write_byte_done(0x00);
    }

    //dummy
    for(i = 0; i < (led_num - 1); i++)
    {
        mcu_spi_write_byte_done(0x00);
        mcu_spi_write_byte_done(0x00);
    }
    
    SPSSN_CHIP_SEL_NONE;

    //SPISET_CLEAR;
    return TRUE;
}

bool mcu_spi_write_leddrv_frameend(uint8_t *cmd_ptr, uint8_t cmd_len, uint8_t led_num, uint8_t cs_idx, uint8_t cksm_en)
{
	uint8_t i = 0;
	
    SPISET_CLEAR;
    //SPI_SEL_MCU = LED_PATH;
    mcu_spi_sw_path(SPI_CFG_PATH);   // 20231003 add / need check

    SPSSN = cs_idx;

    for(i = 0 ; i < cmd_len; i++)
        mcu_spi_write_byte_done(*(cmd_ptr + i));

    if(cksm_en)
    {
        // checksum
        mcu_spi_write_byte_done(0x00);
        mcu_spi_write_byte_done(0x00);
    }

    //dummy for daisy chain
    for(i = 0; i < (led_num - 1); i++)
    {
        mcu_spi_write_byte_done(0x00);
        mcu_spi_write_byte_done(0x00);
    }
    
    SPSSN_CHIP_SEL_NONE;

    //SPISET_CLEAR;
    return TRUE;

}
//--------------------------------------------------
/*uint8_t xdata led_bc_cmd0[8] = 
{0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
uint8_t xdata led_bc_cmd1[8] = 
{0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57};*/
uint8_t xdata led_bc_dummy_array[8] = 
{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

uint8_t *g_leddrv_bc_sram_updt_ptr;
uint16_t xdata g_leddrv_bc_wr_len;

void spi_leddrv_start_update_bc(uint8_t *cmd_ptr, uint8_t cmd_len)
{
    uint16_t tmp_16 = 0;

    if(leddrv_spi_state != 0)
        return;
    // state1 - send cmd
    leddrv_spi_state = 1;
    mcu_int_int9_en();
    tmp_16 = (uint16_t)cmd_ptr;
    mcu_spi_rw_addr_h = (uint8_t)(tmp_16 >> 8);
    mcu_spi_rw_addr_l = (uint8_t)(tmp_16);

    mcu_spi_rw_len_h = (uint8_t)(cmd_len >> 8);;
    mcu_spi_rw_len_l = (uint8_t)(cmd_len);

    SPSSN_CHIP_SEL_TCON_FLASH;
    SPI_SEL_MCU |= SPISET_AUTO_DMA_BY_HW;
    SPISET_SET_WRITE_MODE;
    SPDAT = 0x00;   // trigger dma
}

bool mcu_spi_write_leddrv_bc_int(uint8_t *cmd_ptr, uint8_t cmd_len, uint8_t *ptr, uint16_t wr_len, uint8_t led_num, uint8_t cs_idx, uint8_t cksm_en)
{
    uint16_t tmp_16 = 0;
    uint16_t  i = 0;
       
    SPISET_CLEAR;
    mcu_spi_sw_path(SPI_CFG_PATH);   // 20231003 add / need check
    
    g_leddrv_bc_sram_updt_ptr = ptr;
    g_leddrv_bc_wr_len = wr_len;

    //SPSSN_CHIP_SEL_TCON_FLASH;
    SPSSN = cs_idx;
    if(cmd_len)
        spi_leddrv_start_update_bc(cmd_ptr, cmd_len);

    //SPISET_CLEAR;
    return TRUE;
}

/*void spi_leddrv_test_func(void)
{
    uint16_t tmp_16 = 0;
    uint16_t *ptr = &led_bc_cmd0[0];

    if(leddrv_spi_state != 0)
        return;
    // state1 - send cmd
    leddrv_spi_state = 1;
    mcu_int_int9_en();
    tmp_16 = (uint16_t)ptr;
    mcu_spi_rw_addr_h = (uint8_t)(tmp_16 >> 8);
    mcu_spi_rw_addr_l = (uint8_t)(tmp_16);

    mcu_spi_rw_len_h = 0;
    mcu_spi_rw_len_l = 4;

    SPSSN_CHIP_SEL_TCON_FLASH;
    SPI_SEL_MCU |= SPISET_AUTO_DMA_BY_HW;
    SPISET_SET_WRITE_MODE;
    SPDAT = 0x00;   // trigger dma
}*/
void mcu_spi_leddrv_irq_handler(void)
{
    uint16_t tmp_16 = 0;
	uint8_t *ptr;
    if(leddrv_spi_state == 1)
    {
        // state2 send data
        leddrv_spi_state = 2;
		tmp_16 = (uint16_t)g_leddrv_bc_sram_updt_ptr;
        mcu_spi_rw_addr_h = (uint8_t)(tmp_16 >> 8);
        mcu_spi_rw_addr_l = (uint8_t)(tmp_16);
        mcu_spi_rw_len_h = (uint8_t)(g_leddrv_bc_wr_len >> 8);
        mcu_spi_rw_len_l = (uint8_t)g_leddrv_bc_wr_len;

        // now use 1 led driver case / no sram add
        SPI_SEL_MCU |= SPISET_AUTO_DMA_BY_HW;
        SPISET_SET_WRITE_MODE;
        SPDAT = 0x00;   // trigger dma
    }
    else if(leddrv_spi_state == 2)
    {
        // state3 - send chksm & dummy
        leddrv_spi_state = 3;
        ptr = &led_bc_dummy_array[0];
			  tmp_16 = (uint16_t)ptr;
        mcu_spi_rw_addr_h = (uint8_t)(tmp_16 >> 8);
        mcu_spi_rw_addr_l = (uint8_t)(tmp_16);

        if(g_led_spi_chksm_en)
        {
        mcu_spi_rw_len_h = 0;
            mcu_spi_rw_len_l = 2;        

        SPI_SEL_MCU |= SPISET_AUTO_DMA_BY_HW;
        SPISET_SET_WRITE_MODE;
        SPDAT = 0x00;   // trigger dma
    }
        // now use 1 led driver case / no dummy byte now
    }
    else if(leddrv_spi_state == 3)
    {
        // CS pull high - back to idle
        SPSSN_CHIP_SEL_NONE;
        leddrv_spi_state = 0;
        SPI_SEL_MCU &= ~SPISET_AUTO_DMA_BY_HW;
        SPISET_CLEAR;
        mcu_int_int9_dis();
    }

}
#endif

#endif
