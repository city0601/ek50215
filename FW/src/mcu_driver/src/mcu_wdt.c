/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: MCU Watch Dog
		Platform: 8051
		Author: Eric Lee, 2021/09/10
		Description: MCU Watch Dog Driver
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"
#include "mcu_wdt.h"
// *******************************************************************
/* Watchdog Timer Calculation
 * (12 * 2 * 2^8 * (16 * WDTL[7]) * ((128 - WDTL[6:0]) * 256)) / 120MHz
 * WDTL = 0x7F => 13.1ms
 * WDTL = 0x7E => 26.2ms
 * WDTL = 0x7C => 52.4ms
 * WDTL = 0x78 => 104.8ms
 */
#if 0
void mcu_wdt_init(void)
{
//    WDTREL = 0x78; // 104.8ms @ 120MHz
//
//    mcu_wdt_reset();
}

void mcu_wdt_reset(void)
{
//    WDT = 1;
//    SWDT = 1;
}
#endif