/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: MCU Interrupt
		Platform: 8051
		Author: Eric Lee, 2021/04/06
		Description: interrupt Handler
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"
#include "mcu_int.h"
#include "mcu_i2c.h"
#include "mcu_uart.h"
#include "mcu_dma.h"
#include "irq.h"
#include "edp_link_proc.h"
#include "tcon_pll_remap.h"
#include "tcon_i2c_proc.h"
#include "tcon_misc.h"
#include "tcon_isp.h"
#include "tcon_cascade.h"
#ifdef DEMURA_CHK_FUN
#include "tcon_lut_transfer.h"
#endif
#ifdef EDP_FUN
#include "edp_misc.h"
#include "edp_global_vars.h"
#endif

#include "mcu_spi.h"
// *******************************************************************
// *******************************************************************
// define
#define MCU_INT_IEN0_EAL    (0x01 << 7)
#define MCU_INT_IEN0_WDT    (0x01 << 6)
#define MCU_INT_IEN0_ET2    (0x01 << 5)
#define MCU_INT_IEN0_ES0    (0x01 << 4)
#define MCU_INT_IEN0_ET1    (0x01 << 3)
#define MCU_INT_IEN0_EX1    (0x01 << 2)
#define MCU_INT_IEN0_ET0    (0x01 << 1)
#define MCU_INT_IEN0_EX0    (0x01)

#define MCU_INT_IEN1_EXEN2  (0x01 << 7)
#define MCU_INT_IEN1_SWDT   (0x01 << 6)
#define MCU_INT_IEN1_EX6    (0x01 << 5)
#define MCU_INT_IEN1_EX5    (0x01 << 4)
#define MCU_INT_IEN1_EX4    (0x01 << 3)
#define MCU_INT_IEN1_EX3    (0x01 << 2)
#define MCU_INT_IEN1_EX2    (0x01 << 1)
#define MCU_INT_IEN1_EX7    (0x01)

#define MCU_INT_IEN2_EX12   (0x01 << 5)
#define MCU_INT_IEN2_EX11   (0x01 << 4)
#define MCU_INT_IEN2_EX10   (0x01 << 3)
#define MCU_INT_IEN2_EX9    (0x01 << 2)
#define MCU_INT_IEN2_EX8    (0x01 << 1)
#define MCU_INT_IEN2_ES1    (0x01)

#define MCU_INT_IEN4_ERTC   (0x01 << 5)

// *******************************************************************
// *******************************************************************

void mcu_int_init(void)
{
    __WIN32_EMULATION_PRINTF("[INFO] Doing Interrupt Initialization...\n");

    //IEN0 = MCU_INT_IEN0_EAL | MCU_INT_IEN0_ES0 | MCU_INT_IEN0_ET2;  // Interrupt Enable, Serial Port 0 Interrupt Enable
    //IEN1 = MCU_INT_IEN1_EX7 | MCU_INT_IEN1_EX3 ;// | MCU_INT_IEN1_EX2; // I2C Interrupt Enable & INT3
    IT0 = 1; // EX0 falling edge
    IT1 = 1; // EX1 falling edge
	I2FR = 1;	// EX2 rising edge
	I3FR = 1;	// EX3 rising edge
    //IEN0 = MCU_INT_IEN0_EAL | MCU_INT_IEN0_ES0 | MCU_INT_IEN0_ET2 ;  // Interrupt Enable, Serial Port 0 Interrupt Enable
    //IEN1 = MCU_INT_IEN1_EX7 | MCU_INT_IEN1_EX3 | MCU_INT_IEN1_EX4 ;// | MCU_INT_IEN1_EX2; // I2C Interrupt Enable & INT3
    IEN0 = MCU_INT_IEN0_EAL | MCU_INT_IEN0_ES0; /*| MCU_INT_IEN0_ET2*/   	// Interrupt Enable, Serial Port 0 Interrupt Enable
    //IEN1 = /*MCU_INT_IEN1_EX7 |*/ MCU_INT_IEN1_EX3;// | MCU_INT_IEN1_EX2; // I2C Interrupt Enable & INT3
    //IT1 = 1; // EX1 Falling Edge
    g_mcu_int_flg = 0;
    // priority setting / i2c has highest priority
    /*IP1 &= 0xFD;   // timer2 / ext2 set to level 0
    IP0 &= 0xFD;

    IP1 |= 0x01;   // i2c set to level 1
    IP0 |= 0x01;

    IP1 |= 0x80;   // vsync set to level 2
    IP0 &= 0xF7;*/

    //IP1 = 0x09;
    //IP0 = 0x01;
	//-----------------------------------------------------------//
	// Group 0 > Group 2 > Group 3 > Group 1 = Group 4 = Group 5 //
	//-----------------------------------------------------------//
	IP1 = 0x05;
    IP0 = 0x09;
}

void mcu_int_tim2_en(void)
{
    IEN0 |= MCU_INT_IEN0_ET2;
}

void mcu_int_tim2_dis(void)
{
    IEN0 &= ~MCU_INT_IEN0_ET2;
}

void mcu_int_int0_en(void)              
{
    IEN0 |= MCU_INT_IEN0_EX0;           // Enable Aux port0 Interrupt / iex0           
}

void mcu_int_int1_en(void)              
{
    IEN0 |= MCU_INT_IEN0_EX1;           // Enable Aux port1 Interrupt / iex1          
}

void mcu_int_int3_en(void)
{
    IEN1 |= MCU_INT_IEN1_EX3;           // Enable PLL Remap Interrupt
}

void mcu_int_int4_en(void)
{
    IEN1 |= MCU_INT_IEN1_EX4;           // Enable Vsync Interrupt
}

void mcu_int_int5_en(void)
{
    IEN1 |= MCU_INT_IEN1_EX5;           // Enable port0 HPD Interrupt           
}

void mcu_int_int6_en(void)
{
    IEN1 |= MCU_INT_IEN1_EX6;           // Enable port1 HPD Interrupt           
}

void mcu_int_int8_en(void)
{
    IEN2 |= MCU_INT_IEN2_EX8;           // Enable cascade Interrupt / iex8           
}

void mcu_int_i2c_mask(bool en)
{
    IEN1 = (en)? IEN1 & (~MCU_INT_IEN1_EX7): IEN1 | MCU_INT_IEN1_EX7;
}

void mcu_int_i2c2_mask(bool en)
{
    IEN2 = (en)? IEN2 & (~MCU_INT_IEN2_EX10): IEN2 | MCU_INT_IEN2_EX10;
}

void mcu_int_int9_en(void)
{
    IEN2 |= MCU_INT_IEN2_EX9;           // Enable spi dma done Interrupt / iex9
}

void mcu_int_int9_dis(void)
{
    IEN2 &= (~MCU_INT_IEN2_EX9);        // Disable spi dma done Interrupt / iex9
}

void mcu_int_clear_iex4_isr_event(void)
{
    /*
    g_rw_irq_read_0000h.byte = 0;
    g_rw_irq_read_0001h.byte = 0;
    g_rw_irq_read_0002h_inproc_irq_ev = 0;
    g_rw_irq_read_0003h.byte = 0;
    g_rw_irq_read_0004h.byte = 0;
    g_rw_irq_read_0005h.byte = 0;
    g_rw_irq_read_0006h.byte = 0;
    g_rw_irq_read_0007h.byte = 0;
    */
   // Clear
   //if(g_rw_irq_read_0000h.byte)
    //g_rw_irq_read_0000h.byte = ~g_rw_irq_read_0000h.byte;
    g_rw_irq_read_0000h.byte = 0;
    g_rw_irq_read_0001h.byte = 0;
    g_rw_irq_read_0002h.byte = 0;
    g_rw_irq_read_0003h.byte = 0;
}

void mcu_int_003h(void) interrupt 0   // iex0 interrupt
{
}


void mcu_int_013h(void) interrupt 2   // iex1 interrupt
{
}


void mcu_int_023h(void) interrupt 4 
{
    mcu_uart_isr();
}

void mcu_int_02Bh(void) interrupt 5         // timer2
{
#ifdef EDP_FUN    
    edp_error_handling();
    if(g_edp_port_total_using > 1)
    edp_multiport_iadone_sync_timeout_handler();
#endif    
    mcu_timer2_isr();

    //i2c_timeout_handling();
}

void mcu_int_043h(void) interrupt 8         // iex7 / i2c interrupt
{
    mcu_i2c_isr();
}

void mcu_int_053h(void) interrupt 10        // iex3 / PLL Remap Interrupt
{
#ifdef PLL_REMAP_FUN
    //if((g_rw_mcu_top_0023h_i2c_over_fw_dev & 0x01) == 0x01)
    if(g_rw_mcu_top_0023h_i2c_over_fw_dev == (g_rw_mcu_top_0003h.bits.r_slv_dev << 1))
    {
        if(g_rw_mcu_top_0041h_i2c_over_cnt != 0)
            tcon_pll_remap_isr(); 
    }
#ifdef DLG_FUN
    //else if((g_rw_mcu_top_0023h_i2c_over_fw_dev & 0x02) == 0x02)
    else if((g_rw_mcu_top_0023h_i2c_over_fw_dev & 0xFE) == (g_rw_mcu_top_0031h_ext_dev << 1))
        tcon_i2c_dlg_isr(); 
#endif
#endif
}

/*
void mcu_int_check_corestate_chg(void)
{
    if(g_rw_dbg_mux_0042h_indbgr_8 == 2 || g_rw_dbg_mux_0042h_indbgr_8 == 3) // corestate  
        g_rw_sys_0000h.byte = 1;
}*/

void mcu_int_vsync(void)
{
    //P1_3 = 1;
    if(g_rw_irq_read_0001h.bits.vsync_ev)       // vsync event
    {
        g_vsync_irq_flg = 1;
        //mcu_int_check_corestate_chg();          // g_rw_dbg_mux_0042h_indbgr_8
#ifdef DEMURA_CHK_FUN     
        // Set Demura reload flag
        if(g_rw_demura_load_003Dh.bits.r_demura_dl_en)
        {
            if((g_rw_demura_load_0049h.bits.r_flash_reload == 1) || (g_rw_demura_load_0049h.bits.r_lut_reload == 1))
            {
                if(!dmr_flash_reload_flag)
                    dmr_flash_reload_flag = 1;
            } 
        }
#endif			
#ifdef EDP_FUN
        //edp_checkevent();
        if(epd_misc_get_edp_state() == EDP_STATE_NORMAL)
            g_edp_flag[0].timeout_go_err_chk = TRUE;
        if(g_rw_dpcd_reg_1_0007h_edp_state == EDP_STATE_NORMAL)
            g_edp_flag[1].timeout_go_err_chk = TRUE;

#ifdef EDP1_4_FUN
        if(g_rw_irq_read_0004h.bits.irq_psr_ev)
            g_psr_irq_flg = 1;
        
        if(g_rw_irq_read_0006h_irq_reserve_ev & 0x01)
            g_rsv_irq_flg = 1;

        if(g_rw_irq_read_0005h.bits.irq_intel_ubrr_ev)
            g_intel_ubrr_irq_flag = 1;
#endif
#endif

#ifdef ISP_FUN
        tcon_isp_check_event();
#endif

#ifdef LD_LED_DRV_FUN

#endif
    }
    //P1_3 = 0;
}

//void mcu_dummy_fun(void)
void mcu_int_05Bh(void) interrupt 11 // iex4
{
    //P1_3 = 1;
    g_mcu_int_flg = 1;                          // turn off saving mode
    //mcu_dummy_fun();
    mcu_int_vsync();

    //mcu_timer_set_timeout_us(5000);
    mcu_int_clear_iex4_isr_event();
    //P1_3 = 0;
}

void mcu_int_063h(void) interrupt 12    // iex5 / hpd port0
{
#ifdef EDP_FUN 
    g_hpd_irq_port0_flg = 1;
#endif
}

void mcu_int_06Bh(void) interrupt 13    // iex6 / hpd port1
{
#ifdef EDP_FUN 
    g_hpd_irq_port1_flg = 1;
#endif
}

void mcu_int_08Bh(void) interrupt 17    // iex8 / cascade interrupt
{
#ifdef CASCADE_FUN	
    g_cascade_flag_rcv_rx_int = 1;      // cascade rx handler
	#endif
}

void mcu_int_093h(void) interrupt 18    // iex9 / spi dma interrupt
{
#ifdef LD_LED_DRV_FUN
    //SPI DMA Done
    mcu_spi_leddrv_irq_handler();
	//DMAM1 &= 0x7F;
#endif
}

void mcu_int_09Bh(void) interrupt 19    // iex10 / i2c2 interrupt
{
    //P1_2 = 1;
	mcu_i2c2_isr();
    //P1_2 = 0;
}

/*
void mcu_int_0A3h(void) interrupt 20    // iex11 / i2x2 tx dma
{
}

void mcu_int_0ABh(void) interrupt 21    // iex12 / i2c2 rx dma
{
}
*/




