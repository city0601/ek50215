/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: MCU MDU
		Platform: 8051
		Author: Eric Lee, 2021/04/06
		Description: interrupt Handler
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include "mcu_reg51.h"
#include "mcu_mdu.h"
#include <intrins.h>          //for _nop_();
// *******************************************************************
#if 0
// *******************************************************************
//  Variable define section
typedef union {
  struct {
    unsigned char Byte3;      //MSB
    unsigned char Byte2;
    unsigned char Byte1;
    unsigned char Byte0;      //LSB
  }ss1;

  struct {
    unsigned int Word1;       //MSW
    unsigned int Word0;       //LSW
  }ss2;

  unsigned long Ldata;
}Long_Data;

typedef union {
  struct {
    unsigned char Byte1;      //MSB
    unsigned char Byte0;      //LSB
  }ss1;

    unsigned int Idata;
}Int_Data;
// *******************************************************************

// *******************************************************************
//  Function define section
unsigned int MduDiv_32_16(unsigned long u32_dividend,
                          unsigned int u16_divisor,
                          unsigned long *u32_consult)
{
  Long_Data data LD;
  Int_Data data ID;

  LD.Ldata = u32_dividend;
  ID.Idata = u16_divisor;

  MD0 = LD.ss1.Byte0;               //First Write
  MD1 = LD.ss1.Byte1;
  MD2 = LD.ss1.Byte2;
  MD3 = LD.ss1.Byte3;
  MD4 = ID.ss1.Byte0;
  MD5 = ID.ss1.Byte1;               //Start Division

  _nop_();_nop_();_nop_();_nop_();  //Delay for MDU finish
  _nop_();_nop_();_nop_();_nop_();
  _nop_();_nop_();_nop_();_nop_();
  _nop_();_nop_();_nop_();_nop_();
  _nop_();

  LD.ss1.Byte0 = MD0;               //First Read
  LD.ss1.Byte1 = MD1;
  LD.ss1.Byte2 = MD2;
  LD.ss1.Byte3 = MD3;
  ID.ss1.Byte0 = MD4;
  ID.ss1.Byte1 = MD5;               //Last Read  

  *u32_consult = LD.Ldata;          
  return ID.Idata;                  
}

unsigned int MduDiv_16_16(unsigned int u16_dividend,
                          unsigned int u16_divisor,
                          unsigned int *u16_consult)
{
  Int_Data data IDend;
  Int_Data data IDor;

  IDend.Idata = u16_dividend;
  IDor.Idata = u16_divisor;

  MD0 = IDend.ss1.Byte0;            //First Write
  MD1 = IDend.ss1.Byte1;
  MD4 = IDor.ss1.Byte0;
  MD5 = IDor.ss1.Byte1;             //Start Division

  //_nop_();_nop_();_nop_();_nop_();  //Delay for MDU finish
  _nop_();_nop_();_nop_();_nop_();
  //_nop_();

  IDend.ss1.Byte0 = MD0;            //First Read
  IDend.ss1.Byte1 = MD1;
  IDor.ss1.Byte0 = MD4;                             
  IDor.ss1.Byte1 = MD5;             //Last Read

  *u16_consult = IDend.Idata;       
  return IDor.Idata;                
}

/*void MduMul_16x16(unsigned int u16_multiplicand,
                  unsigned int u16_multiplicator,
                  unsigned long *u32_consult)*/
unsigned long  MduMul_16x16(unsigned int u16_multiplicand,
                  unsigned int u16_multiplicator)                  
{
  Int_Data data IDand;
  Int_Data data IDer;
  Long_Data data LD;

  IDand.Idata = u16_multiplicand;
  IDer.Idata = u16_multiplicator;

  MD0 = IDand.ss1.Byte0;            //First Write
  MD4 = IDer.ss1.Byte0;
  MD1 = IDand.ss1.Byte1;
  MD5 = IDer.ss1.Byte1;             //Start Division

  _nop_();_nop_();_nop_();_nop_();  //Delay for MDU finish
  _nop_();_nop_();_nop_();_nop_();
  /*_nop_();_nop_();_nop_();*/

  LD.ss1.Byte0 = MD0;               //First Read
  LD.ss1.Byte1 = MD1;
  LD.ss1.Byte2 = MD2;
  LD.ss1.Byte3 = MD3;               //Last Read

  //*u32_consult = LD.Ldata;
  return LD.Ldata;
}

void MduShift(unsigned long u32_shift_data,
              unsigned char u8_shift_lr,
              unsigned char u8_shift_counter,
              unsigned long *u32_consult)
{
  Long_Data data LD;

  LD.Ldata = u32_shift_data;

  MD0 = LD.ss1.Byte0;               //First Write
  MD1 = LD.ss1.Byte1;
  MD2 = LD.ss1.Byte2;
  MD3 = LD.ss1.Byte3;

  u8_shift_lr &= 0x01;
  u8_shift_counter &= 0x1F;

  if(u8_shift_lr==1)
    ARCON = (u8_shift_counter|0x20);
  else
    ARCON = u8_shift_counter;       

  _nop_();_nop_();_nop_();_nop_();  //Delay for MDU finish
  _nop_();_nop_();_nop_();_nop_();
  _nop_();_nop_();_nop_();_nop_();
  _nop_();_nop_();_nop_();_nop_();
  _nop_();_nop_();

  LD.ss1.Byte0 = MD0;               //First Read
  LD.ss1.Byte1 = MD1;
  LD.ss1.Byte2 = MD2;
  LD.ss1.Byte3 = MD3;               //Last Read

  *u32_consult = LD.Ldata;
}

void MduNormalizing(unsigned long u32_nor_data,
                    unsigned long *u32_consult)
{
  Long_Data data LD;

  LD.Ldata = u32_nor_data;

  MD0 = LD.ss1.Byte0;               //First Write
  MD1 = LD.ss1.Byte1;
  MD2 = LD.ss1.Byte2;
  MD3 = LD.ss1.Byte3;

  ARCON  = 0x00 ;                   // Start Normalizing
  _nop_();_nop_();_nop_();_nop_();  // Delay for MDU finish
  _nop_();_nop_();_nop_();_nop_();
  _nop_();_nop_();_nop_();_nop_();
  _nop_();_nop_();_nop_();_nop_();
  _nop_();_nop_();_nop_();

  LD.ss1.Byte0 = MD0;               //First Read
  LD.ss1.Byte1 = MD1;
  LD.ss1.Byte2 = MD2;
  LD.ss1.Byte3 = MD3;               //Last Read

  *u32_consult = LD.Ldata;
}
// *******************************************************************
#endif