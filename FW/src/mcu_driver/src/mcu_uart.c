/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: MCU uart
		Platform: 8051
		Author: Eric Lee, 2021/04/06
		Description: MCU Uart Driver
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"

#include "mcu_uart.h"
// *******************************************************************
// *******************************************************************
// define
//#define MCU_UART_BAUD_RATE_115200   (32)
#define MCU_UART_BAUD_RATE_115200   (29)
#define MCU_UART_BAUD_RATE_230400   (16)
#define MCU_UART_BAUD_RATE_460800   (8)
#define MCU_UART_BAUD_RATE_921600   (4)
#define MCU_UART_BUF_SZ (32)
// *******************************************************************
// *******************************************************************
// static function
static uint8_t xdata mcu_uart_buf[MCU_UART_BUF_SZ];
static uint8_t mcu_uart_buf_in_ptr;
static volatile uint8_t mcu_uart_buf_tx_ptr;
static volatile bool mcu_uart_tx_ongoing;
// *******************************************************************
void mcu_uart_init(void)
{
    __WIN32_EMULATION_PRINTF("[INFO] Doing UART Initialization...\n");

    SM0 = 0; // UART Mode 1, No Multiprocessor Communication
    SM1 = 1;
    SM20 = 0;

    PCON |= 0x80;  // Double Baudrate

    BD = 1; // Use Internal Counter, No Timer1 Overflow

    S0RELH = 0x03; // Fixed
    S0RELL = (256 - MCU_UART_BAUD_RATE_921600);
#ifdef DBG_MSG
    TI0 = 1;
#endif
    mcu_uart_buf_in_ptr = 0;
    mcu_uart_buf_tx_ptr = 0;
    mcu_uart_tx_ongoing = FALSE;
}

/*
 * ISR for Serial Port 0 Function
 * => called from mcu_isr_023h.
 */
void mcu_uart_isr(void)
{
    #ifndef DBG_MSG
    if (TI0) {
        if (mcu_uart_buf_tx_ptr != mcu_uart_buf_in_ptr) {
            S0BUF = mcu_uart_buf[mcu_uart_buf_tx_ptr];
            mcu_uart_buf_tx_ptr = ((mcu_uart_buf_tx_ptr + 1) % MCU_UART_BUF_SZ);
        }
        TI0 = 0;
        mcu_uart_tx_ongoing = FALSE;
    } else if (RI0) {
        //P0 = ' ';
        //P0 = S0BUF;
        RI0 = 0;
    }
    #endif
}

void mcu_uart_putchar(uint8_t ch)
{
    #ifndef DBG_MSG
    uint8_t in_ptr;

    if ((mcu_uart_tx_ongoing == FALSE) && (mcu_uart_buf_in_ptr == mcu_uart_buf_tx_ptr)) {
        S0BUF = ch;
        mcu_uart_tx_ongoing = TRUE;
    } else {
        in_ptr = (mcu_uart_buf_in_ptr + 1) % MCU_UART_BUF_SZ;
        while (1) {
            if (in_ptr != mcu_uart_buf_tx_ptr) {
                break;
            }
        }
        mcu_uart_buf[mcu_uart_buf_in_ptr] = ch;
        mcu_uart_buf_in_ptr = in_ptr;
    }
    #endif
}

void mcu_uart_puts(uint8_t *str)
{
    #ifndef DBG_MSG
    uint8_t *tmp_str = str;

    while (*tmp_str != 0) {
        mcu_uart_putchar(*tmp_str);
        tmp_str++;
    }
    #endif
}

#ifdef EDP_FUN
void mcu_uart_put_nibble_hex(uint8_t val)
{
    #ifndef DBG_MSG
    mcu_uart_putchar((val < 10)? (val + '0'): ((val - 10) + 'A'));
    #endif
}

void mcu_uart_put_hex(uint8_t val)
{
    #ifndef DBG_MSG
    mcu_uart_put_nibble_hex(val >> 4);
    mcu_uart_put_nibble_hex(val & 0x0F);
    #endif
}
#endif

#if 0
void mcu_uart_put_hex2n(uint8_t val1, uint8_t val2)
{
    #ifndef DBG_MSG
    mcu_uart_put_hex(val1);
    mcu_uart_put_hex(val2);
    mcu_uart_putchar('\n');
    #endif
}

void mcu_uart_put_hex3n(uint8_t val1, uint8_t val2, uint8_t val3)
{
    #ifndef DBG_MSG
    mcu_uart_put_hex(val1);
    mcu_uart_put_hex(val2);
    mcu_uart_put_hex(val3);
    mcu_uart_putchar('\n');
    #endif
}

void mcu_uart_put_dec(uint8_t val)
{
    #ifndef DBG_MSG
    uint8_t i;

    if (val >= 200) {
        mcu_uart_putchar('2');
        val -= 200;
    } else if (val >= 100) {
        mcu_uart_putchar('1');
        val -= 100;
    }

    i = 0;
    while (val >= 10) {
        val -= 10;
        i++;
    }
    mcu_uart_putchar(i + '0');

    mcu_uart_putchar(val + '0');
    #endif
}
#endif
