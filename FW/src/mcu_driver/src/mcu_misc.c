/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: Misc
		Platform: 8051
		Author: Eric Lee, 2021/04/06
		Description: MCU Misc Driver
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"
#include "mcu_misc.h"
#include "mcu_int.h"
//#include "mcu_dma.h"
#include "mcu_i2c.h"
//#include "mcu_sfr.h"
#include "mcu_spi.h"
#include "mcu_timer.h"
#include "mcu_uart.h"
#include "mcu_wdt.h"
#include "edp_link_proc.h"
#include "mcu_global_vars.h"
// *******************************************************************
// *******************************************************************
// define
#define CKCON_WAIT_STATE(prog_ws, data_ws) (CKCON = ((prog_ws << 4) | (data_ws)))
// *******************************************************************
void mcu_init(void)
{
    __WIN32_EMULATION_PRINTF("[INFO] Doing MCU Initialization...\n");

//    CKCON_WAIT_STATE(0, 0); // change CKCON @ STARTUP.A51
    
    // mcu_wdt_init();        
    // mcu_dma_init();
#ifdef FLASH_CODE 
    mcu_spi_init();
#endif

	mcu_timer_init();
    mcu_int_init();  
	mcu_timer_wait_dl_init();
	mcu_uart_init();    
    mcu_i2c1_init();
    mcu_int_tim2_en();
}

// *******************************************************************
void mcu_set_power_saving_mode(void)
{
	if(g_mcu_int_flg)
	{
		g_mcu_int_flg = 0;
		return;
	}
	//if(g_edp_cks_err_flg)
		//return;
	//mcu_int_tim2_dis();
	PCON |= 0x01;
	//mcu_int_tim2_en();
}


void mcu_dl_get_ee_path(void)
{
    g_cfg_rom_sel_path = (EXTDAT_SFR_71H & 0x80) >> 7;
    g_i2c0_spi1_sel = (EXTDAT_SFR_71H & 0x04) >> 2;
    #if 0    
    ADDR_MUX = 0x07 ; // switch entry EDID
    if((EXTDAT_SFR_72H & 0x02) == 0)
    {
        //g_vcom_sel_path = (EXTDAT_SFR_71H & 0x20) >> 5;
        //g_edid_sel_path = (EXTDAT_SFR_71H & 0x40) >> 6;
        //g_cfg_rom_sel_path = (EXTDAT_SFR_71H & 0x80) >> 7;
        g_cfg_rom_sel_path = (EXTDAT_SFR_71H & 0x80) >> 7;
        g_i2c0_spi1_sel = (EXTDAT_SFR_71H & 0x04) >> 2;
    }
    #endif
    #if 0
    else
    {
        if(EXTDAT_SFR_72H & 0x01)
        {
            g_vcom_sel_path = 0;
            g_edid_sel_path = 0;
            g_cfg_rom_sel_path = 1;
        }
        else
        {
            g_vcom_sel_path = 1;
            g_edid_sel_path = 0;
            g_cfg_rom_sel_path = 0;    
        }
    }
    #endif
}

void mcu_softrst(void)
{
    SRST = 0x01;
    SRST = 0x01;
    while(1);
}

