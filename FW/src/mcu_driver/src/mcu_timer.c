/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: MCU Timer
		Platform: 8051
		Author: Eric Lee, 2021/04/06
		Description: MCU Timer Driver
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"
#include "mcu_timer.h"
// *******************************************************************
// *******************************************************************

#define OSC_80MHZ
// Define
#define DL_TIMEOUT  5000    // 5's
// *******************************************************************
void mcu_timer_init(void)
{
    //__WIN32_EMULATION_PRINTF("[INFO] Doing Timer Initialization...\n");

    TMOD = 0x21; // Timer 0 = Mode 1 (16-bit Timer Mode)/ Timer 1 = Mode 2 / 8-bit Timer Mode
    TCON &= 0x0F; // Disable Timer 0 / 1
#ifdef OSC_120MHZ
// @ 120Mhz
    TH2 = 0xFC;
    TL2 = 0x18;
    CRCH = 0xFC;
    CRCL = 0x18; // 100us @ 120MHz
#endif
#ifdef OSC_108MHZ
// @ 108Mhz
    TH2 = 0xFC;
    TL2 = 0x7C;
    CRCH = 0xFC;
    CRCL = 0x7C; // 100us @ 108MHz
#endif
#ifdef OSC_80MHZ
// @ 80Mhz
    TH2 = 0xFD;
    TL2 = 0x62;
    CRCH = 0xFD;
    CRCL = 0x62; // 100us @ 80MHz
#endif
    T2CON = 0x11; // Reload Mode 0, Timer 1/12 Mode
}

void mcu_timer_wait_dl_init(void)
{
    //g_hw_dl_finish_flg = 0;
    mcu_wait_dl_tim_100us = 0;
    mcu_wait_dl_tim_1ms = 0;
}

void mcu_timer_wait_dl_timeout(void)      // 100us
{
    mcu_wait_dl_tim_100us++;
    if(mcu_wait_dl_tim_100us > 9)  // 1ms
    {
        mcu_wait_dl_tim_100us = 0;
        mcu_wait_dl_tim_1ms++;            
    }
}

bool mcu_timer_dl_timeout(void)
{
    if(mcu_wait_dl_tim_1ms > DL_TIMEOUT)
        return TRUE;
    else
        return FALSE;
}

void mcu_timer2_isr(void)
{
    //if(!g_hw_dl_finish_flg)
    mcu_timer_wait_dl_timeout();
    TF2 = 0;
}
/*
static void mcu_timer_delay_us_core_606(uint16_t us)
{
    TR0 = 0;
    if (us != 0) {
        us = us - 6;    // formula decided by simulation for us @ 120MHz
        us = 65535 - us;
        TL0 = us & 0xFF;
        TH0 = us >> 8;
        TR0 = 1;
        while (TF0 == 0);
        TR0 = 0;
        TF0 = 0;
    }
}*/

static void mcu_timer_delay_us_core(uint16_t us)
{
    TR0 = 0;
    if (us != 0) {

    //us = (us << 3)+us - 1; // us @ 108MHz
         //us = us * 9 - 1; // us @ 108MHz
        us = us * 7 - 1; // us @ 85MHz
        us = 65535 - us;
        TL0 = us & 0xFF;
        TH0 = us >> 8;
        TR0 = 1;
    }
}

#ifdef EDP_FUN
void mcu_timer_delay_us(uint16_t us)
{
    if (us != 0) {
        mcu_timer_delay_us_core(us);
    }
}
#endif

void mcu_timer_wait_us(uint16_t us)
{
    while (us > 0) {
        mcu_timer_delay_us_core(10);
        us--;
    }
}

#if 0
void mcu_timer_set_timeout_us(uint16_t us)
{
    TR1 = 0;
    if ((us != 0) && (us <= 6553)) {
        //us = 6553 - us;
        //us = (us * 10) + 6;
        us = us * 9 - 1; // us @ 108MHz
        us = 65535 - us;
        TL1 = us & 0xFF;
        TH1 = us >> 8;
        TR1 = 1;
    }
}
#endif

#ifdef EDP_FUN
bool mcu_timer_check_timeout_us(void)
{
    /*if (TR1 == 0) {
        return TRUE;
    } else if (TF1 == 1) {
        TR1 = 0;
        TF1 = 0;
        return TRUE;
    }
    return FALSE;*/
    if (TR0 == 0) {
        return TRUE;
    } else if (TF0 == 1) {
        TR0 = 0;
        TF0 = 0;
        return TRUE;
    }
    return FALSE;
}


void mcu_timer_stop_timeout_us(void)
{
    /*TR1 = 0;
    TF1 = 0;*/
    TR0 = 0;
    TF0 = 0;
}

/*void mcu_delay(uint16_t us)
{
    mcu_timer_delay_us(us);
    while(!mcu_timer_check_timeout_us());
}*/
#endif 