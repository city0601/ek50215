/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: MCU I2C
		Platform: 8051
		Author: Eric Lee, 2021/04/06
		Description: MCU I2C Driver
*/
// *******************************************************************
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"
#include "mcu_i2c.h"
#include "mcu_int.h"
#include "mcu_dma.h"
#include "tcon_i2c_proc.h"
#include "CSOT_I2c_slv.h"
#include "mcu_dl.h"
#include "tcon_lut_i2c_slv.h"
#include "mcu_timer.h"
#include "tcon_global_include.h"
#include "mcu_global_vars.h"
// *******************************************************************
// *******************************************************************
uint16_t g_i2c_wr_dly_time = 0;
uint16_t g_vcom_wr_dly_time = 0;
/*
 * I2C Slave Receiver / Transmitter Buffer & Registers
 */
static uint8_t mcu_i2c_slv_addr;
//static uint16_t mcu_i2c_slv_cur_rw_ptr;
static uint8_t mcu_i2c_slv_cur_data;

/*
 * I2C Master Receiver / Transmitter Buffer & Registers
 */
static bool mcu_i2c_mst_ongoing;
static bool mcu_i2c_mst_is_ack;
#ifdef I2C_RW_SAMSUMG_PAT_FUN
    static uint8_t g_samsung_i2c_sla_w_addr;
#endif
uint8_t g_i2csta_tmp = 0;
uint8_t g_i2c1sta_tmp = 0;
uint8_t g_i2c2sta_tmp = 0;
uint8_t g_i2cslvdat = 0;

// *******************************************************************
//  variable for i2c mastr inerrupt mode
// *******************************************************************
/*uint16_t g_i2c_timeout_cntr = 0;
static bool g_i2c_mstr_handler_doing;
static bool g_i2c_mstr_read_flag = 0;   // write = 0 / read = 1
uint8_t g_i2cmstr_devaddr = 0;*/

/*
 * I2C Driver Initialization
 */
void mcu_i2c1_init(void)
{
    __WIN32_EMULATION_PRINTF("[INFO] Doing I2C1 Initialization...\n");

    I2CCON = (I2CCON_CR | I2CCON_ENS1);  // for I2C Master

    mcu_i2c_clk_init();	
    mcu_global_buf_idx = 0;
    mcu_i2c_mst_ongoing = FALSE;
    mcu_i2c_mst_is_ack = FALSE;
}

void mcu_i2c2_init(void)
{
    __WIN32_EMULATION_PRINTF("[INFO] Doing I2C2 Initialization...\n");

    I2C2_SLV_CON = (I2CCON_CR | I2CCON_ENS1 | I2CCON_AA); // for I2C Slave
    //I2C2CON = (I2CCON_CR | I2CCON_ENS1 | I2CCON_AA); // for I2C Slave
#ifdef I2C_RW_SAMSUMG_PAT_FUN    
    I2C2_SLV_ADR = (SAMSUNG_SLV_ADR); // No Global Call
    g_samsung_i2c_sla_w_addr = SAMSUNG_SLV_ADR;
#endif
    //mcu_i2c_sec_addr_2 = (MCU_I2C_SLV_ADDR_I2C2SPI << 1);
#ifdef CSOT_FUN
    mcu_i2c_sec_addr_3 = (MCU_I2C_SLV_ADDR_CSOT << 1);
#endif

#ifdef LUT_SRW_FUN
    mcu_i2c_sec_addr_2 = (g_rw_mcu_top_0037h.bits.r_lut_srw_dev << 1);
#endif
    mcu_global_buf_idx = 0;
    mcu_i2c_slv_addr = 0;
    mcu_i2c_slv_cur_rw_ptr = 0;
    g_i2c_recv_done_flg = 0;
    g_i2c_proc_dev_fun = 0;
    mcu_i2c2spi_recv_len = 0;
    EXTDAT_SFR_2BH |= 0x80;     // catch i2c2 status
}

void mcu_i2c_clk_init(void)
{
    //TH1 = CLK_375KHZ;     // 375 KHZ
    //TR1 = 1;
    //I2CCON = 0xC2;        // 0xC2: 1.8 MHz; 0x40: 332 kHz
    //I2CCON = 0x40;        // 0x40: 332 kHz / for real chip
	I2CCON = CLK_400KHZ;
}

void mcu_get_i2cclk_set(void)
{
    uint8_t val;
    // by project 
    //uint8_t l_clk[] = {CLK_375KHZ,CLK_100KHZ,CLK_187KHZ,CLK_281KHZ,CLK_375KHZ,0xC2,0x41};
    uint8_t l_clk[] = {CLK_400KHZ,CLK_1MHZ,CLK_400KHZ,CLK_200KHZ,CLK_100KHZ,0xC2};
    val = g_rw_mcu_top_0002h.bits.r_mcu_i2c_mst_clk;
    /*if(val == 5 || val == 6)
        I2CCON = l_clk[val];
		else
        TH1 = l_clk[val];*/
		I2CCON = l_clk[val];

}

#if 0
static void mcu_i2c_slv_rx_init(void)
{
    mcu_global_buf_idx = 0;
    //mcu_i2c_slv_addr = I2C2DAT;
    mcu_i2c_slv_addr = g_i2cslvdat;
    if(mcu_i2c_slv_addr == MCU_I2C_SLA_W_CSOT)
        g_i2crecv_csot_flg = 1;
    else
        g_i2crecv_csot_flg = 0;

#ifdef CSOT_FUN
    mcu_i2c2spi_recv_len = 0;     
    //g_csot_read_reg_flag = 0;    
#endif
}
#endif

static bool mcu_i2c_rw_spat(void)
{
    if((mcu_global_buf[0] == g_samsung_i2c_sla_w_addr) && (BUS_ENI))    // Add 1128
    {
        //ptr = (uint16_t)&g_spattern_offset_base;
        if(mcu_global_buf_idx == 0)
            mcu_i2c_slv_cur_rw_ptr = mcu_global_buf[1];
        else
            mcu_i2c_slv_cur_rw_ptr++;

        if(mcu_i2c_slv_cur_rw_ptr < 0x23 || mcu_i2c_slv_cur_rw_ptr > 0x59)
        {
            I2C2CON_AA_0;
            return 1;
        }
    }
    return 0;
}

static void mcu_i2c_slv_rx_byte(void)
{
    //uint16_t ptr;
    if(!BUS_ENI)
    {
        mcu_global_buf_idx++;
        return;
    }

#ifdef I2C_RW_SAMSUMG_PAT_FUN 
    if(mcu_i2c_rw_spat() == 1)
        return;
    #if 0
    // special case  , Nack 
    // add samasung i2c function
    if((mcu_global_buf[0] == g_samsung_i2c_sla_w_addr) && (BUS_ENI))    // Add 1128
    {
        //ptr = (uint16_t)&g_spattern_offset_base;
        if(mcu_global_buf_idx == 0)
            mcu_i2c_slv_cur_rw_ptr = mcu_global_buf[1];
        else
            mcu_i2c_slv_cur_rw_ptr++;

        if(mcu_i2c_slv_cur_rw_ptr < 0x23 || mcu_i2c_slv_cur_rw_ptr > 0x59)
        {
            I2C2CON_AA_0;
            return;
        }
    }
    #endif
#endif
	mcu_global_buf_idx ++;
}

static void mcu_i2c_slv_rx_done(void)
{
    g_i2c_recv_done_flg = 1;
    g_i2c_recv_len = mcu_global_buf_idx;
    //I2C2CON_AA_1;
}

static void mcu_i2c_slv_tx_init(void)
{
    //I2C2CON_AA_1;
    mcu_i2c_slv_addr = I2C2_SLV_DAT;
    //mcu_i2c_slv_tx_byte();
}

static void mcu_i2c_slv_tx_done(void)
{
    tcon_i2c_over_spi_done();
        I2C2CON_AA_1;
}

/*
 * ISR for I2C Slave / Receiver
 */
uint8_t xdata g_gc_dat = 0;
//static bool mcu_i2c_isr_check_slv_rx(void)
static bool mcu_i2c_isr_check_slv_rx(uint8_t l_i2csta)
{
    //switch (I2C2STA) {
    switch (l_i2csta) {
    case I2CSTA_SLV_DATA_W_ACK_STOP: // A0h
        //g_i2c_slv_data_write_stop = 1;
        mcu_i2c_slv_rx_done();
        break;
    case I2CSTA_SLV_DATA_W_ACK: // 80h
        mcu_i2c_slv_rx_byte();
        break;
    case I2CSTA_SLV_SLA_W_ACK: // 60h
    case I2CSTA_SLV_ARB_LOST_SLA_W_ACK: // 68h
        //mcu_i2c_slv_rx_init();
        //IRCON2 |= 0x10;
        break;
    /*case I2CSTA_SLV_DATA_W_ACK: // 80h
        //IRCON2 |= 0x10;
        //mcu_global_buf_idx++;
        mcu_i2c_slv_rx_byte();
        break;*/
    case I2CSTA_SLV_DATA_W_NACK: // 88h => KKKK: we won't NACK???
        I2C2CON_AA_1;
        break;

    case I2CSTA_SLV_GC_ACK:     // 0x70
        //if(g_mcu_dbg_mode_flg)
           // g_recv_i2c_gc_flg = 1;
        break;
    case I2CSTA_SLV_GC_DATA_ACK: // 0x90
        g_gc_dat = I2C2_SLV_DAT;
        break;
    /*case I2CSTA_SLV_DATA_W_ACK_STOP: // A0h
        //g_i2c_slv_data_write_stop = 1;
        mcu_i2c_slv_rx_done();
        break;*/
    /*case I2CSTA_SLV_SLA_W_ACK: // 60h
    case I2CSTA_SLV_ARB_LOST_SLA_W_ACK: // 68h
        mcu_i2c_slv_rx_init();
        break;*/
    default:
        return FALSE;
    }
    return TRUE;
}

/*
 * ISR for I2C Slave / Transmitter
 */
//static bool mcu_i2c_isr_check_slv_tx(void)
static bool mcu_i2c_isr_check_slv_tx(uint8_t l_i2csta)
{
    //switch (I2C2STA) {
    switch (l_i2csta) {
    case I2CSTA_SLV_SLA_R_ACK: // A8h
    case I2CSTA_SLV_ARB_LOST_SLA_R_ACK: // B0h
        //IRCON2 |= 0x08;
        mcu_i2c_slv_tx_init();
        //IRCON |= 0x20;   // ext int 6
        mcu_i2c2spi_cmd_buf_idx ++;
        mcu_i2c2spi_cmd_doing = TRUE;
        //IRCON2 |= 0x08;
        break;
    case I2CSTA_SLV_DATA_R_ACK: // B8h
        //mcu_i2c_slv_tx_byte();
        //IRCON2 |= 0x08;
        mcu_i2c2spi_cmd_buf_idx ++;
        mcu_i2c2spi_cmd_doing = TRUE;
        //IRCON |= 0x20;   // ext int 6
        break;
    case I2CSTA_SLV_DATA_R_NACK: // C0h
        mcu_i2c_slv_tx_done();
        mcu_dma_ch0_rst();
        mcu_dma_ch1_rst();
        //mcu_dma_ch2_rst();
        break;
    case I2CSTA_SLV_LAST_DATA_R_ACK: // C8h => KKKK: we won't set AA = 0???
        I2C2CON_AA_1;
        break;
    default:
        return FALSE;
    }
    return TRUE;
}

/*
 * ISR for I2C Slave Function (Just Return for I2C Master Function / Pooling)
 * => called from mcu_isr_043h.
 */
void mcu_i2c_isr(void)
{
    //uint8_t g_i2csta_tmp = 0;
    // check Pri I2C
    if (I2C1CON_SI_IS_SET) {
        g_i2c1sta_tmp = I2C1_SLV_STA;
        I2C1CON_SI_CLEAR;

        if ((!mcu_i2c_isr_check_slv_rx(g_i2c1sta_tmp)) &&
            (!mcu_i2c_isr_check_slv_tx(g_i2c1sta_tmp)))  {
            // Do error handling...
            I2C1CON_CLEAR;
            return;
        }
        //I2C1CON_SI_CLEAR; // clear interrupt flag
    }
}
// new
void mcu_i2c2_isr(void)
{
    // check Secondary I2C
    if (I2C2CON_SI_IS_SET) {
        g_i2c2sta_tmp = I2C2_SLV_STA;
        I2C2CON_SI_CLEAR;

        if ((!mcu_i2c_isr_check_slv_rx(g_i2c2sta_tmp)) &&
            (!mcu_i2c_isr_check_slv_tx(g_i2c2sta_tmp)))  {
            // Do error handling...
            I2C2CON_CLEAR;
            return;
        }
        //I2C2CON_SI_CLEAR; // clear interrupt flag
    }
}

// *******************************************************************
//  I2c Master
// *******************************************************************
static uint8_t mcu_i2c_mst_wait_si(void)
{
    uint8_t i2c_sta;
    uint16_t timeout = MCU_I2C_WAIT_SI_TIMEOUT;
    while (timeout--) 
    {
        if (I2C1CON_SI_IS_SET) 
        {
            i2c_sta = I2CSTA;
            return i2c_sta;
        }
    }
    return 0xFF;
}

#ifdef EDP_FUN
void mcu_i2c_send_nack(void)        // eric add at 20201124
{
    uint8_t i2c_sta;

     //mcu_uart_puts("send i2c nack\n");

    if (mcu_i2c_mst_ongoing) {
        I2C1CON_AA_0;
        I2C1CON_SI_CLEAR;
        i2c_sta = mcu_i2c_mst_wait_si();
        if (i2c_sta == 0xFF) {
            mcu_i2c_mst_ongoing = FALSE;
            return ;
        }
        if (i2c_sta != I2CSTA_MST_DATA_R_NACK) {
            mcu_i2c_mst_ongoing = FALSE;
            return ;
            }
        return ;
    }
    return ;
} 
#endif

uint8_t mcu_i2c_mst_read_byte(bool last_byte)
{
    uint8_t i2c_sta;

    __WIN32_EMULATION_PRINTF2("\t\t\t\t[I2C] %02X\n", ++vvv);

    if (mcu_i2c_mst_ongoing) {
        if (~last_byte) {
            I2C1CON_AA_1;
        } else {
            I2C1CON_AA_0;
        }
        I2C1CON_SI_CLEAR;
        i2c_sta = mcu_i2c_mst_wait_si();
        if (i2c_sta == 0xFF) {
            mcu_i2c_mst_ongoing = FALSE;
            return 0;
        }
        if (~last_byte) {
            if (i2c_sta != I2CSTA_MST_DATA_R_ACK) {
                mcu_i2c_mst_ongoing = FALSE;
                return 0;
            }
        } else {
            if (i2c_sta != I2CSTA_MST_DATA_R_NACK) {
                mcu_i2c_mst_ongoing = FALSE;
                return 0;
            }
        }
        return I2CDAT;
    }
    return 0;
}

void mcu_i2c_mst_write_byte(uint8_t wt_val)
{
    uint8_t i2c_sta;

    __WIN32_EMULATION_PRINTF2("\t\t\t\t[I2C] %02X\n", wt_val);

    if (mcu_i2c_mst_ongoing) {
        I2CDAT = wt_val;
        I2C1CON_SI_CLEAR;
        i2c_sta = mcu_i2c_mst_wait_si();
        if (i2c_sta == 0xFF) {

            mcu_i2c_mst_ongoing = FALSE;
            //mcu_uart_puts("1\n");
            return;
        }
        if (i2c_sta == I2CSTA_MST_DATA_W_ACK) {
            mcu_i2c_mst_is_ack = TRUE;
        } else if (i2c_sta == I2CSTA_MST_DATA_W_NACK) {
            mcu_i2c_mst_is_ack = FALSE;
            //mcu_uart_puts("2\n");
        } else {
            mcu_i2c_mst_ongoing = FALSE;
            //mcu_uart_puts("3\n");
            return;
        }
        mcu_i2c_mst_ongoing = TRUE;
    }
}

bool mcu_i2c_mst_recv_ack(void)
{
    if (mcu_i2c_mst_ongoing & mcu_i2c_mst_is_ack) {
        return TRUE;
    }
    return FALSE;
}

void mcu_i2c_mst_write_stop(uint16_t wait_tm_us)
{
    __WIN32_EMULATION_PRINTF("\t\t\t\t[I2C] I2C_STOP\n");

    I2C1CON_CLEAR;
    I2C1CON_SEND_STOP;
    I2C1CON_SI_CLEAR;

    mcu_i2c_mst_ongoing = FALSE;

    I2C1CON_CLEAR;
    I2C1CON_SI_CLEAR;
    mcu_timer_wait_us(wait_tm_us);   // delay 10~20us for hw sending stop
    I2C1CON_DIS;
    I2C1CON_EN;
    mcu_int_i2c_mask(FALSE);

    // ek58216
    g_rw_mcu_top_0003h.bits.fw_i2c_access_vcom = 0;
}

void mcu_i2c_mst_write_start_addr(uint8_t addr, uint8_t wt0_rd1, bool rpt)      // wt0_rd1 : r/w bit
{
    uint8_t i2c_sta;
    uint8_t tmp;

    __WIN32_EMULATION_PRINTF(rpt ? "\t\t\t\t[I2C] I2C_REPEAT_START\n": "\t\t\t\t[I2C] I2C_START\n");
    __WIN32_EMULATION_PRINTF2("\t\t\t\t[I2C] SLAVE = %02X\n", (addr << 1) + wt0_rd1);
    mcu_int_i2c_mask(TRUE);  // fix i2c always interrupt
    // ek58216 
    g_rw_mcu_top_0003h.bits.fw_i2c_access_vcom = 1;
    I2C1CON_CLEAR;
    I2C1CON_SEND_START;
    I2C1CON_SI_CLEAR;
    i2c_sta = mcu_i2c_mst_wait_si();
    I2C1CON_CLEAR;
    if (i2c_sta == 0xFF) {
        mcu_i2c_mst_ongoing = FALSE;
        return;
    }
    if (!(((rpt == FALSE) && (i2c_sta == I2CSTA_MST_START)) ||
          ((rpt == TRUE) && (i2c_sta == I2CSTA_MST_RPT_START)))) {
        mcu_i2c_mst_ongoing = FALSE;
        return;
    }
    tmp = (addr << 1) + wt0_rd1;
    I2CDAT = tmp;
    I2C1CON_SI_CLEAR;
    i2c_sta = mcu_i2c_mst_wait_si();
    if (i2c_sta == 0xFF) {
        mcu_i2c_mst_ongoing = FALSE;
        return;
    }
    
    if (wt0_rd1 == 0) {
        
        if (i2c_sta == I2CSTA_MST_SLA_W_ACK) {
            mcu_i2c_mst_is_ack = TRUE;
        } else if (i2c_sta == I2CSTA_MST_SLA_W_NACK) {
            mcu_i2c_mst_is_ack = FALSE;
        } else {
            mcu_i2c_mst_ongoing = FALSE;
            return;
        }
    } else {
        
        if (i2c_sta == I2CSTA_MST_SLA_R_ACK) {
            mcu_i2c_mst_is_ack = TRUE;
        } else if (i2c_sta == I2CSTA_MST_SLA_R_NACK) {
            mcu_i2c_mst_is_ack = FALSE;
        } else {
            mcu_i2c_mst_ongoing = FALSE;
            return;
        }
    }
    mcu_i2c_mst_ongoing = TRUE;
}

void mcu_i2c_set_two_pin_sel(void)
{
    // I2C Two Pin
    //if((g_rw_dbg_mux_0028h_mcu_top_dbgr_17 & 0x80) == 0x80)   // check i2c two pin sel
		if((g_rw_dbg_mux_003Bh_mcu_top_dbgr_18 & 0x80) == 0x80)   // check i2c two pin sel
        g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 1;
    // I2C Four Pin
    else    
        g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;

}

bool mcu_i2c_mst_write(uint8_t dev_adr ,uint8_t* mem_reg, uint8_t mem_len, uint8_t* data_buf , uint16_t data_len, bool cal_dl_cks_flg, uint16_t wait_tm_us)   
{
    uint16_t cnt;
    //uint16_t l_write_len;
    //mcu_timer_stop_timeout_us();
    //mcu_timer_delay_us(6000);
    if(data_len == 0)
        return I2C_FAIL;
    // Step 1 
    // Send Dev Adr
    mcu_i2c_mst_write_start_addr(dev_adr, 0, FALSE);     // Dev Addr

    //ack = mcu_i2c_mst_recv_ack();
    if(mcu_i2c_mst_recv_ack() == FALSE)
    {
        mcu_i2c_mst_write_stop(wait_tm_us);
        return I2C_FAIL;
    }

    // Step 2
    // mem reg 
    for(cnt = 0; cnt < mem_len ; cnt++)
    {
        mcu_i2c_mst_write_byte(mem_reg[cnt]);               // mem 
        if (mcu_i2c_mst_recv_ack() == FALSE)
        {
            mcu_i2c_mst_write_stop(wait_tm_us);
            return I2C_FAIL;
        }
    }

    // Step 3
    if(cal_dl_cks_flg)
        mcu_dl_i2c_hw_cal_cks_en();
    // date
    for(cnt = 0; cnt < data_len ; cnt++)
    {
        // mcu_timer_delay_us(6000);
        mcu_i2c_mst_write_byte(data_buf[cnt]);               // D0...Dn
        if (mcu_i2c_mst_recv_ack() == FALSE)
        {
            mcu_i2c_mst_write_stop(wait_tm_us);
            return I2C_FAIL;
        }
        //    break;
    }
    //if(cal_dl_cks_flg)
    //{
        //mcu_dl_i2c_hw_read_cks();
    //}
    //mcu_i2c_mst_write_stop_by_reg();
    mcu_i2c_mst_write_stop(wait_tm_us);
    return I2C_PASS;
}

bool mcu_i2c_mst_read(uint8_t* w_buf , uint8_t mem_len , uint8_t* r_buf , uint16_t r_len , bool cal_dl_cks_flg , uint16_t wait_tm_us,bool no_stop_flg)     
{
    uint16_t cnt;
    bool ack;
    uint16_t ptr;
    uint8_t l_val;


    //mcu_timer_stop_timeout_us();
    //mcu_timer_delay_us(6000);
    if(r_len == 0)
        return I2C_FAIL;

    ptr = (uint16_t)r_buf;
    mcu_i2c_mst_write_start_addr(w_buf[0], 0, FALSE);     // Dev Addr
    ack = mcu_i2c_mst_recv_ack();
    if(ack == FALSE)
    {
        mcu_i2c_mst_write_stop(wait_tm_us);
        return I2C_FAIL;
    }
    for(cnt = 1 ; cnt < mem_len + 1 ; cnt++)
    {
        mcu_i2c_mst_write_byte(w_buf[cnt]);               // D0...Dn
        if (mcu_i2c_mst_recv_ack() == FALSE)
        {
            mcu_i2c_mst_write_stop(wait_tm_us);
            return I2C_FAIL;
        }
    }

    //*************************************************************
    // Restart
    mcu_i2c_mst_write_start_addr(w_buf[0], 1, TRUE);     // Dev Addr + R
    if (mcu_i2c_mst_recv_ack() == FALSE)
    {
        mcu_i2c_mst_write_stop(wait_tm_us);
        return I2C_FAIL;
    }

    for(cnt = 0 ; cnt < r_len ; cnt++)
    {
        l_val = mcu_i2c_mst_read_byte((cnt == r_len - 1)? TRUE: FALSE);
        if(mcu_i2c_mst_ongoing == TRUE)
        *(volatile char xdata *) (ptr + cnt) = l_val;
        else
            {
                mcu_i2c_mst_write_stop(wait_tm_us);
                return I2C_FAIL;
            }
        if(cal_dl_cks_flg)  // && (!g_mcu_old_ver))
            mcu_dl_i2c_cal_data_cks(l_val);
    }
    if(no_stop_flg)
        return I2C_PASS;
    mcu_i2c_mst_write_stop(wait_tm_us);
    return I2C_PASS;
}

bool mcu_i2c_mst_read_data(uint8_t* r_buf,uint16_t r_len,uint16_t wait_tm_us)
{
    uint8_t cnt;
    uint8_t l_val;
    uint16_t ptr;

    ptr = (uint16_t)r_buf;
    for(cnt = 0 ; cnt < r_len ; cnt++)
    {
        l_val = mcu_i2c_mst_read_byte((cnt == r_len - 1)? TRUE: FALSE);
        if(mcu_i2c_mst_ongoing == TRUE)
            *(volatile char xdata *) (ptr + cnt) = l_val;
        else
        {
            mcu_i2c_mst_write_stop(wait_tm_us);
            return I2C_FAIL;
        }
    }
    mcu_i2c_mst_write_stop(wait_tm_us);
    return I2C_PASS;
}
// *******************************************************************
//  I2c Process Recv Data
// *******************************************************************
void mcu_i2c_proc_slv_cmd(void)
{
    if(g_i2c_recv_done_flg)
    {
		// P0_4 = 1;
        tcon_i2c_over_spi_start(mcu_global_buf, g_i2c_recv_len);

        if(g_mcu_i2c2spi_dev_adr == MCU_I2C_SLA_W_CSOT){    // For enter i2c_cmd_02/05_proc function
            g_i2c_proc_dev_fun = 0;
            g_i2crecv_csot_flg = 1;
        }
        else
        {
            g_i2crecv_csot_flg = 0;
            if(g_mcu_i2c2spi_dev_adr == (g_rw_mcu_top_0037h.bits.r_lut_srw_dev << 1)){  
                g_i2c_proc_dev_fun = 2;
            }
        #ifdef I2C_RW_SAMSUMG_PAT_FUN
            else if((g_mcu_i2c2spi_dev_adr == g_samsung_i2c_sla_w_addr) && (BUS_ENI)){
                g_i2c_proc_dev_fun = 1;
            }
        #endif
            else{
                g_i2c_proc_dev_fun = 0;
            }
        }

        mcu_global_buf_idx = 0;
        g_i2c_recv_done_flg = 0;
        mcu_dma_ch0_rst();
        mcu_dma_ch1_rst();
        //mcu_dma_ch2_rst();
		// P0_4 = 0;
    }
}
// *******************************************************************
//  function for i2c mastr inerrupt mode
// *******************************************************************
#if 0
void mcu_i2c_mstr_isr(void)
{
    if (I2C1CON_SI_IS_SET)
    {
        g_i2c1_sta = I2CSTA;
        I2C1CON_SI_CLEAR;
        g_i2c_timeout_cntr = 0;     // timeout clear
        g_i2c_mstr_handler_doing = 1;
    }
    I2C1CON_CLEAR;
}


void mcu_i2c1_mst_write_start(uint8_t addr)      // wt0_rd1 : r/w bit
{
    uint8_t i2c_sta;
    uint8_t tmp;

    mcu_int_i2c_mask(TRUE);  // fix i2c always interrupt
    I2C1CON_CLEAR;
    I2C1CON_SEND_START;
    I2C1CON_SI_CLEAR;

    g_i2cmstr_devaddr = addr;
    mcu_i2c_mst_ongoing = TRUE;
}

void i2c_timeout_handling(void)
{
    if(g_i2c_timeout_cntr)
    {
        g_i2c_timeout_cntr --;
        if(!g_i2c_timeout_cntr)
        {
            // timeout function
        }
    }

}

// coefficient : I2CSTA, I2C2STA
void i2c_mstr_statemachine_handler(uint8_t i2c_sta)
{
    uint8_t addr_tmp;
    if(!g_i2c_mstr_handler_doing)
        return;

    switch(i2c_sta)   // real time i2c state
    {
        case I2CSTA_MST_START:
            // send slv addr
            addr_tmp =  (g_i2cmstr_devaddr << 1) + g_i2c_mstr_read_flag;
            I2CDAT = addr_tmp;
            I2C1CON_SI_CLEAR;
            g_i2c_timeout_cntr = MCU_I2C_WAIT_SI_TIMEOUT;   // start timeout 
            g_i2c1sta_tmp = i2c_sta;
            break;

        case I2CSTA_MST_SLA_W_ACK:
            if(g_i2c1sta_tmp = I2CSTA_MST_START)
            {
                mcu_i2c_mst_is_ack = TRUE;
                mcu_i2c_mst_ongoing = TRUE;
                i2c_mstr_write_data_handler();
            }
            g_i2c1sta_tmp = i2c_sta;
            break;

        case I2CSTA_MST_SLA_W_NACK:
            if(g_i2c1sta_tmp = I2CSTA_MST_START)
            {
                mcu_i2c_mst_is_ack = FALSE;
            }
            g_i2c1sta_tmp = i2c_sta;
            break;
        
        case I2CSTA_MST_SLA_R_ACK:
            break;

        case I2CSTA_MST_SLA_R_NACK:
            break;

        case I2CSTA_MST_DATA_W_ACK:
            if((g_i2c1sta_tmp == I2CSTA_MST_SLA_W_ACK) || (g_i2c1sta_tmp == I2CSTA_MST_DATA_W_ACK))
            {
                mcu_i2c_mst_is_ack = TRUE;
                mcu_i2c_mst_ongoing = TRUE;
                i2c_mstr_write_data_handler();
            }
            g_i2c1sta_tmp = i2c_sta;
            break;

        case I2CSTA_MST_DATA_W_NACK:
            mcu_i2c_mst_is_ack = FALSE;
            g_i2c1sta_tmp = i2c_sta;
            mcu_i2c_mst_write_stop(20);
            break;

        case I2CSTA_MST_DATA_R_ACK:
            break;

        case I2CSTA_MST_DATA_R_NACK:
            break;

        


        case 0xFF:
            mcu_i2c_mst_ongoing = FALSE;
            break;

    }
    g_i2c_mstr_handler_doing = 0;

}
#endif