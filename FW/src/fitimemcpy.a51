$NOMOD51
DPS	DATA	092H
DPC	DATA	093H

NAME	FITIMEMCPY

?PR?_FITI_Mmecpy_fun?FITIMEMCPY                     SEGMENT CODE
?DT?_FITI_Mmecpy_fun?FITIMEMCPY                     SEGMENT DATA OVERLAYABLE 

PUBLIC _FITI_Mmecpy_fun
PUBLIC	?_FITI_Mmecpy_fun?BYTE
;RSEG  ?PR?_FITI_Mmecpy_fun?FITIMEMCPY

;PUBLIC	?_FITI_Mmecpy_fun?BYTE
RSEG  ?DT?_FITI_Mmecpy_fun?FITIMEMCPY
?_FITI_Mmecpy_fun?BYTE:
    src:   DS   3
    dst:   DS   3
	;ORG  2
    len:   DS   1

RSEG  ?PR?_FITI_Mmecpy_fun?FITIMEMCPY
_FITI_Mmecpy_fun:
	USING	0

	MOV src, R3
	MOV src+1, R2
	MOV src+2, R1
	MOV  R7,len
	
	ORL DPS, #09H          ;DPS=1
	MOV DPH, src+1 
	MOV DPL, src+2 
	ORL DPC, #09H          ;DPC=9

	ANL DPS, #0FEH         ;DPS=0
	MOV DPH, dst+1 
	MOV DPL, dst+2 
	ORL  DPC, #09H          ;DPC=9
	CJNE R3,#0x01, COPY_INTERNAL
	
L0: MOVX A, @DPTR           ;DPTR0
	ORL  DPS, #09H          ;DPS=1
	MOVX @DPTR,A
	ANL  DPS, #0FEH         ;DPS=0
	DJNZ R7, L0
	AJMP CLR_DPS

COPY_INTERNAL:
	MOV R1, src+2 
	MOV R2, src+1

COPY_INTERNAL_LO:	
	MOVX A, @DPTR           ;DPTR0 to RAM
	MOV @R1, A
	ANL  DPS, #0FEH         ;DPS=0
	INC src+2
	AJMP CHK_ADDR

CHK_LEN_CNT:
	DJNZ R7, COPY_INTERNAL_LO
	AJMP CLR_DPS
	RET

CHK_ADDR:
	MOV R1, src+2 
	MOV R2, src+1 
	MOV A, R1
	JZ ADD_R2
	;CJNE R1,#0x00, ADD_R2
	AJMP CHK_LEN_CNT

ADD_R2:
	INC src+1
	MOV R2, src+1
	AJMP CHK_LEN_CNT


CLR_DPS:
	MOV DPC, #00H          ;DPC=0
	ORL DPS, #09H          ;DPS=1
	MOV DPC, #08H          ;DPC=0
	ANL DPS, #0F6H         ;DPS=0
	RET
END