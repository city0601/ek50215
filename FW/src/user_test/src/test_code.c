#include "mcu_spi.h"
#include "mcu_global_vars.h"
#include "tcon_i2c_proc.h"

//uint8_t xdata g_mem_val;
uint8_t xdata test_buf[128];
void test_nvt_cal_crc(void)
{
    uint16_t i , j;
    uint16_t crc = 0x0000;
    uint16_t crc_temp_1,crc_temp_2,crc_temp_3;
    uint8_t data_buffer;
    uint8_t len;
    uint8_t buffer[6] = {0x90,0x9,0x67,0x90,0x9,0x67};
    uint32_t checksum = 0;

    i = 0;
    len = 6;

    while(len --)
    {
        data_buffer = buffer[i++];
        checksum += data_buffer;
        crc_temp_1 = 0;
        crc_temp_2 = 0;
        crc_temp_3 = 0;

        for(j = 0 ; j < 8 ; j++)
        {
            crc_temp_1 = ((crc >> 15) ^ data_buffer) & 0x0001;
            crc_temp_2 = ((crc >> 1) ^ crc_temp_1) & 0x0001;
            crc_temp_3 = ((crc >> 14) ^ crc_temp_1) & 0x0001;
            crc = crc << 1;
            crc &= 0x7FFA;
            crc |= crc_temp_1;
            crc |= (crc_temp_2 << 2);
            crc |= (crc_temp_3 << 15);
            data_buffer >>= 1;
        }
    }
}

void Test_code(uint8_t test_num)
{
    uint16_t i;
    switch (test_num)
    {
        case 0:
            test_nvt_cal_crc();
        break;

        case 1:
            for(i = 0 ; i < 128 ; i ++)
            {
                test_buf[i] = i;
            }
            //mcu_spi_led_write(test_buf);
            //mcu_spi_led_read(&test_buf[0]);
            SPI_SEL_MCU &= ~0x03;
            mcu_spi_led_write(&test_buf[0], ~0x80);
            SPI_SEL_MCU |= 0x01;
            mcu_spi_led_write(&test_buf[0], ~0x40);
            mcu_spi_led_write(&test_buf[0], ~0x0F);
            SPI_SEL_MCU &= ~0x03;
            SPI_SEL_MCU |= 0x02;
            mcu_spi_led_write(&test_buf[0], ~0x80);
        break;

        case 2:
            P0 = 0;
            /*g_system_base = 0x55;
            if(g_system_base != 0x55)
                P0_2 = 1;
            g_system_base = 0xAA;
            if(g_system_base != 0xAA)
                P0_3 = 1;*/

            /*P0 = 0;
            g_aip_base = 0x55;
            if(g_aip_base != 0x55)
                P0_2 = 1;
            g_aip_base = 0xAA;
            if(g_aip_base != 0xAA)
                P0_3 = 1;*/

            P0 = 0;
            g_irq_reg_base = 0x55;
            if(g_irq_reg_base != 0x55)
                P0_2 = 1;
            g_irq_reg_base = 0xAA;
            if(g_irq_reg_base != 0xAA)
                P0_3 = 1;   

            P0 = 0;
            g_spattern_offset_base = 0x55;
            if(g_spattern_offset_base != 0x55)
                P0_2 = 1;
            g_spattern_offset_base = 0xAA;
            if(g_spattern_offset_base != 0xAA)
                P0_3 = 1;       

        break;

        case 3:
            P0 = 0;
            P0_1 = 1;
            for(i = 0 ; i<2 ;i++)
                tcon_i2c_read_reg(0x856+i);
            P0_1 = 0;
            P0_1 = 1;
            for(i = 0 ; i<500 ;i++)
                tcon_i2c_read_reg(0x1000+i); 
            P0_1 = 0;
            P0_1 = 1;
            for(i = 0 ; i<81 ;i++)
                tcon_i2c_read_reg(0x11F4+i);   
            P0_1 = 0;
            P0_1 = 1;
            for(i = 0 ; i<1086 ;i++)
                tcon_i2c_read_reg(0x1245+i);   
            P0_1 = 0;
        break;

        case 4:
            P0 = 0;
            P0_1 = 1;
            for(i = 0 ; i<149 ;i++)
                tcon_i2c_read_reg(0x1800+i);
            P0_1 = 0;
            P0_1 = 1;
            for(i = 0 ; i<193 ;i++)
                tcon_i2c_read_reg(0x1C00+i); 
            P0_1 = 0;
            P0_1 = 1;
            for(i = 0 ; i<1000 ;i++)
                tcon_i2c_read_reg(0x1000+i); 
            P0_1 = 0;
        break;

        case 5:
        P1 = SPI_SEL_MCU;

        P0 = 0;
        P0_1 = 1;
        for(i = 0 ; i<81 ;i++)
            tcon_i2c_read_reg(0x1000+i);
        P0_1 = 0;
        P0_1 = 1;
        for(i = 0 ; i<1086 ;i++)
            tcon_i2c_read_reg(0x1051+i); 
        P0_1 = 0;
        P0_1 = 1;
        for(i = 0 ; i<1000 ;i++)
            tcon_i2c_read_reg(0x1800+i); 
        P0_1 = 0;
        P0_1 = 1;
        for(i = 0 ; i<149 ;i++)
            tcon_i2c_read_reg(0x1C00+i); 
        P0_1 = 0;
        P0_1 = 1;
        for(i = 0 ; i<193 ;i++)
            tcon_i2c_read_reg(0x1E00+i); 
        P0_1 = 0;
        break;

        case 6:
            P0 = 0;
            tcon_i2c_write_reg(0x4800, 0x55);
            P0 = tcon_i2c_read_reg(0x4800);
            tcon_i2c_write_reg(0x4801, 0xAA);
            P0 = tcon_i2c_read_reg(0x4801);
            P0 = 0;

            P0 = 0;
            tcon_i2c_write_reg(0x5000, 0x55);
            P0 = tcon_i2c_read_reg(0x5000);
            tcon_i2c_write_reg(0x5001, 0xAA);
            P0 = tcon_i2c_read_reg(0x5001);
            P0 = 0;

            P0 = 0;
            tcon_i2c_write_reg(0x5800, 0x55);
            P0 = tcon_i2c_read_reg(0x5800);
            tcon_i2c_write_reg(0x5801, 0xAA);
            P0 = tcon_i2c_read_reg(0x5801);
            P0 = 0;
            break;
    }    
}

#if 0
//#define MAX_SIZE    8800
#define MAX_SIZE    1760
/*typedef union {
    uint16_t rwdata[2];
    struct 
    {
        uint16_t volt : 6;
        uint16_t mode_1 : 2;
        uint16_t volt_2 : 6;
        uint16_t mode_2 : 2;
    }bits;
}pwr_state;*/

struct rw_inx_demura_cfg
{
    uint8_t code_data[16];
    uint8_t mode[4];
    uint8_t range;
    uint8_t min;
};

struct rw_inx_decode_cfg
{
    uint8_t sub_range[3];
    //uint8_t sub_range_1;
    //uint8_t sub_range_2;
    //uint8_t sub_range_3;
    //uint8_t M;
    uint8_t N[4];              
    uint8_t Q[4];            
    uint8_t deLut;           
};

struct r_inx_m0
{
    uint8_t m0_code_0                   : 4;
    uint8_t m0_code_1                   : 4;

    uint8_t m0_code_2                   : 4;
    uint8_t m0_code_3                   : 4;

    uint8_t m0_code_4                   : 4;
    uint8_t m0_code_5                   : 4;

    uint8_t m0_code_6                   : 4;
    uint8_t m0_code_7                   : 4;

    uint8_t m0_code_8                   : 4;
    uint8_t m0_code_9                   : 4;

    uint8_t m0_code_10                  : 4;
    uint8_t m0_code_11                  : 4;

    uint8_t m0_code_12                  : 4;
    uint8_t m0_code_13                  : 4;

    uint8_t m0_code_14                  : 4;
    uint8_t m0_code_15                  : 4;
        
    uint8_t m0_mode_0                   : 3;
    uint8_t m0_mode_1                   : 3;
    uint8_t m0_mode_2_1_0               : 2;

    uint8_t m0_mode_2_2_1               : 1;
    uint8_t m0_mode_3                   : 3;
    uint8_t m0_range_3_0                : 4;

    uint8_t m0_range_6_4                : 3;
    uint8_t m0_min_4_0                  : 5;

    uint8_t m0_min_6_5                  : 2;
    uint8_t reserved                    : 6;
};

     
struct r_demura_cfg_0  
{
    struct r_inx_m0 Pn_Nx_16;
};

//volatile uint8_t xdata r_lut_table[1760] _at_ 0x9800;
//volatile uint8_t xdata FitiLut_Tab[256] _at_ 0x9100;
volatile struct r_demura_cfg_0  xdata g_r_inx_demura_cfg_u[155]          _at_    0x9500;        // 155 = (496/16) * 5
volatile struct rw_inx_demura_cfg xdata g_rw_inx_demura_cfg_u       _at_ 0x9900;
volatile struct rw_inx_decode_cfg xdata g_rw_inx_decode_cfg_u;

#define N1_N16_M0_CODE_0        g_r_inx_demura_cfg_u.P1_N1_16.m0_code_0
#define N1_N16_M0_CODE_1        g_r_inx_demura_cfg_u.P1_N1_16.m0_code_1
#define N1_N16_M0_CODE_2        g_r_inx_demura_cfg_u.P1_N1_16.m0_code_2
#define N1_N16_M0_CODE_3        g_r_inx_demura_cfg_u.P1_N1_16.m0_code_3
#define N1_N16_M0_CODE_4        g_r_inx_demura_cfg_u.P1_N1_16.m0_code_4
#define N1_N16_M0_CODE_5        g_r_inx_demura_cfg_u.P1_N1_16.m0_code_5
#define N1_N16_M0_CODE_6        g_r_inx_demura_cfg_u.P1_N1_16.m0_code_6
#define N1_N16_M0_CODE_7        g_r_inx_demura_cfg_u.P1_N1_16.m0_code_7
#define N1_N16_M0_CODE_8        g_r_inx_demura_cfg_u.P1_N1_16.m0_code_8
#define N1_N16_M0_CODE_9        g_r_inx_demura_cfg_u.P1_N1_16.m0_code_9
#define N1_N16_M0_CODE_10       g_r_inx_demura_cfg_u.P1_N1_16.m0_code_10
#define N1_N16_M0_CODE_11       g_r_inx_demura_cfg_u.P1_N1_16.m0_code_11
#define N1_N16_M0_CODE_12       g_r_inx_demura_cfg_u.P1_N1_16.m0_code_12
#define N1_N16_M0_CODE_13       g_r_inx_demura_cfg_u.P1_N1_16.m0_code_13
#define N1_N16_M0_CODE_14       g_r_inx_demura_cfg_u.P1_N1_16.m0_code_14
#define N1_N16_M0_CODE_15       g_r_inx_demura_cfg_u.P1_N1_16.m0_code_15

#define N1_N16_M0_MODE_0        g_r_inx_demura_cfg_u.P1_N1_16.m0_mode_0
#define N1_N16_M0_MODE_1        g_r_inx_demura_cfg_u.P1_N1_16.m0_mode_1
#define N1_N16_M0_MODE_2        (g_r_inx_demura_cfg_u.P1_N1_16.m0_mode_2_2_1 << 2 | g_r_inx_demura_cfg_u.P1_N1_16.m0_mode_2_1_0)
#define N1_N16_M0_MODE_3        g_r_inx_demura_cfg_u.P1_N1_16.m0_mode_3

#define N1_N16_M0_RANGE         (g_r_inx_demura_cfg_u.P1_N1_16.m0_range_6_4 << 4 | g_r_inx_demura_cfg_u.P1_N1_16.m0_range_3_0)
#define N1_N16_M0_MIN           (g_r_inx_demura_cfg_u.P1_N1_16.m0_min_6_5 << 5 | g_r_inx_demura_cfg_u.P1_N1_16.m0_min_4_0)

//-------------------------------------------------------------------------------------------------------------------------------//
/*
#define N1_N16_M1_CODE_1        ( g_r_inx_demura_cfg_u.P2_N1_16.m1_code_1_3_2 << 2 | g_r_inx_demura_cfg_u.P1_N1_16.m1_code_1_1_0)
#define N1_N16_M1_CODE_3        ( g_r_inx_demura_cfg_u.P2_N1_16.m1_code_3_3_2 << 2 | g_r_inx_demura_cfg_u.P2_N1_16.m1_code_3_1_0)
#define N1_N16_M1_CODE_5        ( g_r_inx_demura_cfg_u.P2_N1_16.m1_code_5_3_2 << 2 | g_r_inx_demura_cfg_u.P2_N1_16.m1_code_5_1_0)
#define N1_N16_M1_CODE_7        ( g_r_inx_demura_cfg_u.P2_N1_16.m1_code_7_3_2 << 2 | g_r_inx_demura_cfg_u.P2_N1_16.m1_code_7_1_0)
#define N1_N16_M1_CODE_9        ( g_r_inx_demura_cfg_u.P2_N1_16.m1_code_9_3_2 << 2 | g_r_inx_demura_cfg_u.P2_N1_16.m1_code_9_1_0)
#define N1_N16_M1_CODE_11       ( g_r_inx_demura_cfg_u.P2_N1_16.m1_code_11_3_2 << 2 | g_r_inx_demura_cfg_u.P2_N1_16.m1_code_11_1_0)
#define N1_N16_M1_CODE_13       ( g_r_inx_demura_cfg_u.P2_N1_16.m1_code_13_3_2 << 2 | g_r_inx_demura_cfg_u.P2_N1_16.m1_code_13_1_0)
#define N1_N16_M1_CODE_15       ( g_r_inx_demura_cfg_u.P2_N1_16.m1_code_15_3_2 << 2 | g_r_inx_demura_cfg_u.P2_N1_16.m1_code_15_1_0)
#define N1_N16_M1_RANGE         ( g_r_inx_demura_cfg_u.P2_N1_16.m1_range_6_2 << 2 | g_r_inx_demura_cfg_u.P2_N1_16.m1_range_1_0)
#define N1_N16_M1_MIN           ( g_r_inx_demura_cfg_u.P2_N1_16.m1_min_6_3 << 3 | g_r_inx_demura_cfg_u.P2_N1_16.m1_min_2_0)*/

//#define N1_N16_M2_MODE_1        (g_r_inx_demura_cfg_u.P2_N1_16)

static void Lut_Transfer_Align_Inx_Format(struct r_inx_m0 *val)
{
    g_rw_inx_demura_cfg_u.code_data[0] =  val->m0_code_0;
    g_rw_inx_demura_cfg_u.code_data[1] =  val->m0_code_1;
    g_rw_inx_demura_cfg_u.code_data[2] =  val->m0_code_2;
    g_rw_inx_demura_cfg_u.code_data[3] =  val->m0_code_3;
    g_rw_inx_demura_cfg_u.code_data[4] =  val->m0_code_4;
    g_rw_inx_demura_cfg_u.code_data[5] =  val->m0_code_5;
    g_rw_inx_demura_cfg_u.code_data[6] =  val->m0_code_6;
    g_rw_inx_demura_cfg_u.code_data[7] =  val->m0_code_7;
    g_rw_inx_demura_cfg_u.code_data[8] =  val->m0_code_8;
    g_rw_inx_demura_cfg_u.code_data[9] =  val->m0_code_9;
    g_rw_inx_demura_cfg_u.code_data[10] =  val->m0_code_10;
    g_rw_inx_demura_cfg_u.code_data[11] =  val->m0_code_11;
    g_rw_inx_demura_cfg_u.code_data[12] =  val->m0_code_12;
    g_rw_inx_demura_cfg_u.code_data[13] =  val->m0_code_13;
    g_rw_inx_demura_cfg_u.code_data[14] =  val->m0_code_14;
    g_rw_inx_demura_cfg_u.code_data[15] =  val->m0_code_15;

    g_rw_inx_demura_cfg_u.mode[0] = val->m0_mode_0;
    g_rw_inx_demura_cfg_u.mode[1] = val->m0_mode_1;
    g_rw_inx_demura_cfg_u.mode[2] = (val->m0_mode_2_2_1 << 2 | val->m0_mode_2_1_0);
    g_rw_inx_demura_cfg_u.mode[3] = val->m0_mode_3;

    g_rw_inx_demura_cfg_u.range = (val->m0_range_6_4 << 4 | val->m0_range_3_0);
    g_rw_inx_demura_cfg_u.min = (val->m0_min_6_5 << 5 | val->m0_min_4_0);

}

//static void cal_M_N_Q()

static void Lut_Transfer_Cal_SubRange(uint8_t *dst_val,uint8_t val_offset)
{
    uint16_t l_val_tmp = 0;
    uint8_t l_sub_val = 0;
    uint8_t l_ret_val = 0;

    l_val_tmp = g_rw_inx_demura_cfg_u.range;
    
    if(val_offset == 2)
        l_sub_val = g_rw_inx_demura_cfg_u.range;
    l_val_tmp = (l_val_tmp << val_offset) - l_sub_val;

    l_ret_val = l_val_tmp >> 2;
    if((l_val_tmp % 4) >=2)
        l_ret_val++;
    *dst_val =  l_ret_val + g_rw_inx_demura_cfg_u.min;    
}

static void Lut_Transfer_Cal_M_N_Q(uint8_t mode , struct rw_inx_decode_cfg *ptr, uint8_t seq)
{
    uint16_t l_m_val;
    switch(mode)
    {
        case 0:
            l_m_val = g_rw_inx_demura_cfg_u.min + g_rw_inx_demura_cfg_u.range;
            *(&ptr->N[seq]) = g_rw_inx_demura_cfg_u.min;
        break;
        case 1:
            l_m_val = ptr->sub_range_3;
            *(&ptr->N[seq]) = g_rw_inx_demura_cfg_u.min;
        break;
        case 2:
            l_m_val = ptr->sub_range_2;
            *(&ptr->N[seq]) = g_rw_inx_demura_cfg_u.min;
        break;
        case 3:
            l_m_val = ptr->sub_range_1;
            *(&ptr->N[seq]) = g_rw_inx_demura_cfg_u.min;
        break;
        case 4:
            l_m_val = g_rw_inx_demura_cfg_u.min + g_rw_inx_demura_cfg_u.range;
            *(&ptr->N[seq]) = ptr->sub_range_1;
        break;
        case 5:
            l_m_val = g_rw_inx_demura_cfg_u.min + g_rw_inx_demura_cfg_u.range;
            *(&ptr->N[seq]) = ptr->sub_range_2;
        break;
        case 6:
            l_m_val = g_rw_inx_demura_cfg_u.min + g_rw_inx_demura_cfg_u.range;
            *(&ptr->N[seq]) = ptr->sub_range_3;
        break;
        case 7:
            l_m_val = ptr->sub_range_3;
            *(&ptr->N[seq]) = ptr->sub_range_1;
        break;
        default:
            l_m_val = g_rw_inx_demura_cfg_u.min + g_rw_inx_demura_cfg_u.range;
            *(&ptr->N[seq]) = g_rw_inx_demura_cfg_u.min;
        break;
    }
//*val->Q
    *(&ptr->Q[seq]) = l_m_val - ptr->N[seq];
}

static void Lut_Transfer_Cal_delut(void)
{
    // cal delut = codebit[15 ~ 0] * 546 * Q
}

#endif
#if 0
void Test_code(uint8_t test_type)
{
    #if 0
    uint16_t line = 0;
    uint32_t r_spiaddr = 0;
    uint8_t range = 0;
    uint16_t sub1 = 0 , sub2 = 0 , sub3 = 0;
    uint32_t val = 0;
    uint16_t m_val = 0;
    uint16_t n_val = 0;
    uint32_t g_plane1_offset = 0, g_plane2_offset = 1,g_plane3_offset = 2 , g_plane4_offset = 3 , g_plane5_offset = 4;
    uint8_t i = 0 , bit_cnt = 0 , offset = 0;
    uint16_t proc_cnt = 0;
    uint8_t l_size_val = 0;

    // Lut Transfer 
    uint32_t l_inx_spi_addr = 0;
    uint8_t l_cal_cnt = 0;
    uint8_t l_fill_struct_cnt = 0;
#endif
    //l_size_val = sizeof(g_r_inx_demura_cfg_u);

    //pwr_state ps;
    //g_r_demura_cfg_0_u.bits.code_8 = 0x07;
    //ps.rwdata[0] = 0xE4;
    //ps.rwdata[1] = 0x23;

    
    
    if(test_type == 1)      // lut change
    {
        #if 0
        l_size_val = sizeof(g_r_inx_demura_cfg_u);
        for(line = 0; line <= 54; line++,r_spiaddr += MAX_SIZE)
        {
            //r_spiaddr <<= 8;   
            //mcu_spi_flash_fast_read((uint8_t *)&r_spiaddr + 1, &r_lut_table[0] , MAX_SIZE);  
            //r_spiaddr >>= 8;    

            for(proc_cnt = 0 ; proc_cnt < MAX_SIZE ; proc_cnt+= 11)
            {
                range++;
                /*for(bit_cnt = 0 ; bit_cnt < 4; bit_cnt++)
                {
                    val = val >> 0;
                    val = val >> 2 | val << 6;
                    val = val >> 4 | val << 4;
                    val = val >> 6 | val << 2;
                }*/
                /*
                val = range>>2;
                if((val % 4) >= 2)
                    val++;
                sub1 = val;       
                val = range>>2;
                if((val % 4) >= 2)
                    val++; 
                sub2 = val;      
                val = range>>2;
                if((val % 4) >= 2)
                    val++; 
                sub3 = val;
                */
                //for(offset = 0 ; offset < 4; offset++)
                //{
                    //m_val = sub1 + 4;
                    //n_val = sub2; 
                    /*for(i = 0 ; i < 16 ; i++)   
                    {
                        val = (1<<2) * 546 * 2;  
                        switch(i % 5)
                        {
                            case 1:     // plane1
                                FitiLut_Tab[g_plane1_offset%256] = val;
                                g_plane1_offset += 5;
                                break;
                            case 2:     // plane2
                                FitiLut_Tab[g_plane2_offset%256] = val;
                                g_plane2_offset += 5;
                                break;
                            case 3:     // plane3
                                FitiLut_Tab[g_plane3_offset%256] = val;
                                g_plane3_offset += 5;
                                break;
                            case 4:     // plane4
                                FitiLut_Tab[g_plane4_offset%256] = val;
                                g_plane4_offset += 5;
                                break;
                            default:    //plane5
                                FitiLut_Tab[g_plane5_offset%256] = val;
                                g_plane5_offset += 5;
                                break;
                        }
                    }*/
                //}
            }
        }
        #endif
    }
    else if(test_type == 2)
    {
        #if 0
        g_rw_sys_data_0D_u.bits.spi_dma_shift = 2;
        g_rw_sys_data_0F_u.byte = 2;
        r_spiaddr = 0;

        //mcu_spi_flash_fast_read((uint8_t *)&r_spiaddr + 1, &r_lut_table[0] , 72);  

        for(i = 0 ; i < 72 ; i++)
        {
            //P1 = r_lut_table[i];
        }
        while (1);
        #endif
    }

    else if (test_type == 3)
    {
        tcon_lut_transfer_InxtoFiti();
        #if 0
        //Lut_Transfer_Cal_SubRange(&g_rw_inx_decode_cfg_u.sub_range_1 , 2);
        // Set HW Offset 
        g_rw_sys_data_0D_u.bits.spi_dma_shift = 2;
        g_rw_sys_data_0F_u.byte = 11;
        l_inx_spi_addr = 0x9500;
        // Get Inx Format P1 ~ P5 N1 - 481
        mcu_spi_flash_fast_read((uint8_t *)&l_inx_spi_addr + 1, (uint8_t *)&g_r_inx_demura_cfg_u[0] , MAX_SIZE);  
        //r_spiaddr += MAX_SIZE;
        for(l_fill_struct_cnt = 0 ; l_fill_struct_cnt < 155 ; l_fill_struct_cnt ++)
        {
            Lut_Transfer_Align_Inx_Format(&g_r_inx_demura_cfg_u[l_fill_struct_cnt].Pn_Nx_16);
            // Cal Subrange 1 2 3
            for(cal_cnt = 0 ; cal_cnt < 3 ; cal_cnt++)
                Lut_Transfer_Cal_SubRange(&g_rw_inx_decode_cfg_u.sub_range[cal_cnt]] , 0);
            //Lut_Transfer_Cal_SubRange(&g_rw_inx_decode_cfg_u.sub_range_2 , 1);
            //Lut_Transfer_Cal_SubRange(&g_rw_inx_decode_cfg_u.sub_range_3 , 2);
            // Cal M N Q
            //Lut_Transfer_Cal_M_N_Q();
            //cal_M_N_Q();
        }
        #endif
    }
}
#endif