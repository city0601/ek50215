/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: eDP
		Platform: 8051
		Author: Eric Lee, 2021/04/07
		Description: tcon reg over aux
*/
// *******************************************************************
#include "mcu_fun_def.h"

#ifdef EDP_FUN
// Include Path
//#include "reg_include.h"
#include "mcu_spi.h"
#include "tcon_i2c_proc.h"
#include "ioa_tcon_reg.h"
#include "edp_misc.h"
#include "edp_global_vars.h"

// *******************************************************************
// *******************************************************************
extern void tcon_aux_tx_bug_fix(uint8_t val);
// *******************************************************************
static uint8_t xdata edp_aux_last_tcon[EDP_PORT_NUM];
static uint16_t g_ioa_rw_tcon_reg;
// *******************************************************************
static void ioa_tcon_write_to_reg(uint8_t cnt , uint8_t len)
{
    uint8_t i;

    for(i = cnt ; i < len ; i++)
    {
        tcon_i2c_write_reg(g_ioa_rw_tcon_reg ,edp_aux_rx_bytes[5+i]);
        g_ioa_rw_tcon_reg ++;
    }
    edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
    tcon_aux_tx_bug_fix(1);
}

void ioa_tcon_reg_init(uint8_t port)
{
    g_ioa_rw_tcon_reg = 0;
    edp_aux_last_tcon[port] = EDP_AUX_LAST_TCON_IDLE;
    //edp_aux_last_tcon[1] = EDP_AUX_LAST_TCON_IDLE;
}

void edp_tcon_reg_over_aux(uint8_t port)
{
    uint8_t seq_cnt = edp_aux_rx_bytes[0];          // recv data len
    uint8_t len = edp_aux_rx_bytes[4] + 1;          // rw len
    uint8_t cnt;
    
    if (g_edp_aux_req == EDP_AUX_REQ_WR)
    {
        if (seq_cnt > 5) 
        {
            if(edp_aux_last_tcon[port] == EDP_AUX_LAST_TCON_IDLE)
            {
                VAR_MERGER_16(g_ioa_rw_tcon_reg , edp_aux_rx_bytes[5]);
                ioa_tcon_write_to_reg(2, len);
                edp_aux_last_tcon[port] = EDP_AUX_LAST_TCON_WRITE;
            }
            else if(edp_aux_last_tcon[port] == EDP_AUX_LAST_TCON_WRITE)
            {
                ioa_tcon_write_to_reg(0, len);
            }
        }
        else
        {
            edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
            tcon_aux_tx_bug_fix(1);    
        }
    }
    else if(g_edp_aux_req == EDP_AUX_REQ_RD)
    {
        if (seq_cnt == 4)
        {
            edp_aux_last_tcon[port] = EDP_AUX_LAST_TCON_READ;
            for(cnt = 0; cnt < len ; cnt++)
            {
                edp_aux_tx_bytes[cnt + 2] = tcon_i2c_read_reg(g_ioa_rw_tcon_reg++);
            }
            edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
            tcon_aux_tx_bug_fix(len+1);
        }
        else
        {
            edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
            tcon_aux_tx_bug_fix(1);
        }
    }

    if (g_edp_aux_mot == 0) 
    {
        edp_aux_last_tcon[port] = EDP_AUX_LAST_TCON_IDLE;
    } 
}
#endif
