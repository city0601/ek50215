#include "mcu_fun_def.h"

#ifdef EDP_FUN
#include "edp_global_vars.h"
#include "hw_mem_map.h"
//---------------------------------------------------------------------
//#define EDID_RAM_FIELD_XMEM_BASE        (0x0000)     
//#define MCU_CFG_BASE                    (0x126F)
//#define LATCH_FIELD_XMEM_BASE           (0x1220)
//---------------------------------------------------------------------
volatile dpcd_ena_bits xdata g_dpcd_ena_bits;
//---------------------------------------------------------------------
volatile uint8_t xdata edp_aux_tx_bytes[EDP_AUX_BUF_SZ] _at_ (AUX_READ_FIELD_XMEM_BASE);
volatile uint8_t xdata edp_aux_rx_bytes[EDP_AUX_BUF_SZ] _at_ (AUX_READ_FIELD_XMEM_BASE + 0x20);
//---------------------------------------------------------------------
volatile struct edp_flag_t xdata g_edp_flag[EDP_PORT_NUM];

uint8_t edp_aux_in_defer[EDP_PORT_NUM];
uint8_t edp_ioa_mode_en_flag[EDP_PORT_NUM]; 

uint8_t xdata g_edp_port_max = 0;                       // 2port : port_max = 2 & port_type = 0 
uint8_t xdata g_edp_port_type = 0;                      // 1port(port0) : port_max = 1 & port_type = 0 / 1port(port1) : port_max = 2 & port_type = 1
uint8_t xdata g_edp_port_total_using = 0;
uint8_t xdata g_edp_port = 0;                           // real time port number
uint8_t xdata g_edp_multiport_iadone_sync_tmcnt = 0;
bool g_edp_flag_allport_iadone = 0;                     // all edp port are interlane align done 
bool g_edp_multiport_iadone_sync_start_timeout = 0; 
//---------------------------------------------------------------------
uint8_t xdata g_edp_i2c_rw_addr[EDP_PORT_NUM];
uint8_t xdata g_edp_aux_hpd_cnt[EDP_PORT_NUM];
uint8_t xdata g_edp_aux_type;
uint8_t xdata g_edp_aux_mot;
uint8_t xdata g_edp_aux_req;
uint8_t xdata g_edp_aux_last_i2c[EDP_PORT_NUM];
uint8_t xdata g_edp_aux_last_i2c_addr[EDP_PORT_NUM];
uint8_t xdata g_edp_aux_wr_byte_idx[EDP_PORT_NUM];
uint8_t xdata g_edp_aux_wr_byte_cnt[EDP_PORT_NUM];
uint8_t xdata g_edp_aux_defer_wr_st[EDP_PORT_NUM];
uint8_t xdata g_edp_aux_defer_wr_cnt[EDP_PORT_NUM];
uint8_t xdata g_edp_aux_defer_wr_idx[EDP_PORT_NUM];
uint8_t xdata g_edp_aux_edid_segment_addr;
uint8_t xdata g_edp_aux_i2c_en_addr;
uint8_t xdata g_edp_ioa_pmic_addr;
uint8_t xdata g_edp_ioa_dvcom_addr;
uint8_t xdata g_edp_ioa_ls1_addr;
uint8_t xdata g_edp_ioa_ls2_addr;
uint8_t xdata g_edp_ioa_gma_addr;
uint8_t xdata g_edp_ioa_edid_addr;
uint8_t xdata g_edp_ioa_cfg_addr;
uint8_t xdata g_edp_aux_edid_slave_addr;
uint8_t xdata g_edp_aux_edid_offset;
uint8_t xdata g_edp_ioa_spi_flash_addr;
uint8_t xdata g_edp_ioa_tcon_reg_addr;
//---------------------------------------------------------------------
uint16_t xdata g_edp_timeout_cnt[EDP_PORT_NUM] = 0;
uint16_t xdata g_edp_chk_timeout_cnt[EDP_PORT_NUM] = 0;

//---------------------------------------------------------------------
uint8_t idata g_edp_aux_bytes[EDP_AUX_BYTES_SZ];
//---------------------------------------------------------------------
uint16_t xdata g_no_lnk_trn_rst_chk_target = 200;               // 20ms / 20 * 10
uint16_t xdata g_no_lnk_trn_rst_double_chk_target = 400;        // 40ms / 40 * 10
uint16_t xdata g_edp_err_cnt_thd_target = 1000;
uint16_t xdata g_edp_aux_reply_timeout_target = 300;

uint8_t xdata g_edp_polling_time_irq = 0;

uint8_t xdata g_edp_aux_dpcd_030xh[EDP_DPCD_030X_SRC_DEVICE_SPECIFIC_SZ];

// cascade 
//bool g_master_flg;
//bool g_cascade_tx_flg;
#endif