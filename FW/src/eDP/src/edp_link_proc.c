#include "mcu_fun_def.h"

#ifdef EDP_FUN
#include "edp_link_proc.h"
#include "edp_misc.h"
#include "mcu_misc.h"

//uint8_t xdata g_edp_cks_err_flg = 0;

static bool edp_link_init_state(uint8_t port)
{
    if(epd_misc_get_edp_state() == EDP_STATE_INIT)
    {      
        //if (g_edp_in_reset == FALSE) 
        if (g_edp_flag[port].in_reset == FALSE)
        {
            //g_edp_in_reset = TRUE;
            g_edp_flag[port].in_reset = TRUE;
            edp_do_init_rst();
        } else if (edp_chk_rst_done()) 
        {
            //g_edp_in_reset = FALSE;
            g_edp_flag[port].in_reset = FALSE; 
            edp_misc_set_edp_state(EDP_STATE_NO_TRN);
        }
        return 1;
    }
    return 0;
}

static bool edp_link_pat_chg(uint8_t port)
{
    uint8_t trn_pat, qual_pat;
    uint8_t l_edp_sta; 

    trn_pat = g_rw_dpcd_reg_003Dh.bits.r_TRAIN_PAT_SET & 0x03;
    qual_pat = g_rw_dpcd_reg_003Dh.bits.r_TRAIN_PAT_SET & 0x0C;

    //if (g_edp_pat_set_chg) 
    if (g_edp_flag[port].pat_set_chg) 
    {
        //g_edp_pat_set_chg = FALSE;
        g_edp_flag[port].pat_set_chg = FALSE;
        if (trn_pat == EDP_TRN_PAT_TPS1) 
        {
            l_edp_sta = EDP_STATE_TRN_PHS1;
            edp_do_trn_phs1_state_init();
        } 
        else if ((trn_pat == EDP_TRN_PAT_TPS2) || (trn_pat == EDP_TRN_PAT_TPS3)) 
        {
            l_edp_sta = EDP_STATE_TRN_PHS2;
        } 
        else if (trn_pat >= EDP_TRN_PAT_TPS4) 
        {
            l_edp_sta = EDP_STATE_NO_TRN;
            edp_do_no_trn_state_init(port);
        } 
        else if (qual_pat != EDP_QUAL_PAT_OFF) 
        {
            l_edp_sta = EDP_STATE_QUAL_TEST;
            //edp_do_qual_test_state_init();
        } 
        else 
        {
            if (edp_link_status_ok()) 
            {
                l_edp_sta = EDP_STATE_NORMAL;
                edp_do_normal_state_init(port);
            } 
            else 
            {
                l_edp_sta = EDP_STATE_NO_TRN;
                edp_do_no_trn_state_init(port);
            }
        }
        edp_misc_set_edp_state(l_edp_sta);
        return 1;
    }
    return 0;
}

static bool edp_link_normal_state(uint8_t port)
{
#ifdef EDP1_4_FUN
    if((g_rw_dpcd_read_0048h.bits.r_SINK_DEVICE_SELF_REFRESH_STATUS != 0) || (g_rw_dpcd_read_0054h.bits.r_Sink_Device_Panel_Replay_Status != 0))
        return 1;
#endif
    if(epd_misc_get_edp_state() == EDP_STATE_NORMAL)
    {
        if ((!edp_check_link_sta()) || \
            ((g_edp_flag[port].timeout_go_err_chk) && (edp_get_err_cnt() > g_edp_err_cnt_thd_target))) 
        {
            g_edp_flag[g_edp_port].timeout_go_err_chk = FALSE;
            //g_rw_dpcd_read_000Bh.bits.r_LINK_STATUS_UPDATED  = 1;
            g_rw_dp_009Dh.bits.r_fw_link_status_update = 1;    // modified 230606 / g_rw_dpcd_read_000Bh is read only
            g_rw_dpcd_read_000Bh.bits.r_INTERLANE_ALIGN_DONE = 0;

            if(g_rw_dp_0093h.bits.HPD_DBG == 0 || 
               (g_rw_dp_0093h.bits.HPD_DBG == 1 && g_edp_flag[port].dbg_hpd_cnt == 0))
            {
                edp_assert_irq_hpd(port); 			
                if(g_rw_dp_0093h.bits.HPD_DBG) 
                    g_edp_flag[port].dbg_hpd_cnt = 1;   
            }
            edp_misc_set_edp_state(EDP_STATE_INIT);
            return 1;
        }
        if(g_edp_flag[port].timeout_go_err_chk)
            g_edp_flag[port].timeout_go_err_chk = FALSE;
        return 1;
    }
    return 0;
}

static bool edp_link_no_tra_state(uint8_t port)
{
    if(epd_misc_get_edp_state() == EDP_STATE_NO_TRN)
    {
        if (edp_link_status_ok()) 
        {
            edp_misc_set_edp_state(EDP_STATE_NORMAL);
            edp_do_normal_state_init(port);
        } 
        else if (g_edp_flag[port].timeout_go_phy_rst) 
        {
			g_edp_flag[port].timeout_go_phy_rst = FALSE;
            g_edp_flag[port].timeout_go_chk_crd = FALSE;
            edp_do_phy_rst();
        } 
        else if (g_edp_flag[port].timeout_go_chk_crd) 
        {
			g_edp_flag[port].timeout_go_chk_crd = FALSE;
            if(!edp_crd_status())
            {
                g_edp_chk_timeout_cnt[port] = 0;
                g_edp_flag[port].timeout_go_phy_rst = FALSE;
                edp_do_phy_rst();
            }
        }
        return 1;
    }
    return 0;
}

void edp_link_proc(uint8_t port)
{
    if(edp_link_init_state(port))
        return;
    else if(edp_link_pat_chg(port))
        return;
    else if(edp_link_normal_state(port))
        return;
    else 
        edp_link_no_tra_state(port);    
}

void edp_error_handling(void)           // 100us timing tick
{
    uint8_t port = 0;
    uint8_t port_use_in_loop;
	if(!g_edp_polling_time_irq)
	    return;
    port_use_in_loop = g_edp_port;
    //for(port = 0; port < EDP_PORT_NUM; port++)
    for(port = g_edp_port_type; port < g_edp_port_max; port ++)
    {
        edp_dpcd_reg_port_sel(port);                                                // remap dpcd register

        g_edp_timeout_cnt[port] += 1;
        g_edp_chk_timeout_cnt[port] += 1;
        //if (g_edp_timeout_cnt[port] >= g_no_lnk_trn_rst_chk_target * 10)            // 1ms
        if (g_edp_timeout_cnt[port] >= g_no_lnk_trn_rst_chk_target)
        {       
            g_edp_timeout_cnt[port] = 0;
            if(epd_misc_get_edp_state() == EDP_STATE_NO_TRN)
                g_edp_flag[port].timeout_go_chk_crd = TRUE;          
        }
        //if(g_edp_chk_timeout_cnt[port] >= g_no_lnk_trn_rst_double_chk_target * 10)  // 1ms 
        if(g_edp_chk_timeout_cnt[port] >= g_no_lnk_trn_rst_double_chk_target)
        {
            g_edp_chk_timeout_cnt[port] = 0;
            if(epd_misc_get_edp_state() == EDP_STATE_NO_TRN)
                g_edp_flag[port].timeout_go_phy_rst = TRUE;
        }

        edp_check_irq_hpd(port);    
    }
    edp_dpcd_reg_port_sel(port_use_in_loop);            // edp_port set back for main loop
}
#endif
