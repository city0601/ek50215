/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: JD1730
		Platform: 8051
		Author: Eric Lee, 2021/04/07
		Description: i2c over aux
*/
// *******************************************************************
#include "mcu_fun_def.h"

#ifdef EDP_FUN
#include "edp_global_vars.h"
#include "edp_misc.h"
#include "edp_aux.h"
// *******************************************************************
uint8_t xdata g_ioa_spcf_rx_device_addr[EDP_PORT_NUM];              // ioa specific slv addr
uint8_t xdata g_ioa_spc_mode[EDP_PORT_NUM];
uint8_t xdata g_ioa_spcf_i2c_rw_addr[EDP_PORT_NUM];                 // i2c transfer slv addr 
// *******************************************************************
void ioa_i2c_dev_chg_init(uint8_t port)
{
    uint8_t i;
    for(i = 0 ; i < port ; i++)
    {
        g_ioa_spcf_rx_device_addr[i] = 0;
        g_ioa_spcf_i2c_rw_addr[i] = 0;
        g_ioa_spc_mode[i] = 0;
    }
}
// *******************************************************************
uint8_t ioa_i2c_chg_check_adr(uint8_t aux_rx_i2c,uint8_t port)
{
    if(g_ioa_spc_mode[port] && edp_ioa_mode_en_flag[port])
    {
        if(aux_rx_i2c == g_ioa_spcf_rx_device_addr[port])
        {
            return g_ioa_spcf_i2c_rw_addr[port];
        }
    }
    return aux_rx_i2c;
}
// *******************************************************************
void ioa_i2c_dev_chg(uint8_t port)
{
    uint8_t seq_cnt = edp_aux_rx_bytes[0];          // recv data len
    uint8_t len = edp_aux_rx_bytes[4] + 1;          // rw len
    
    if (g_edp_aux_req == EDP_AUX_REQ_WR)
    {
        if (seq_cnt > 5) 
        {
            g_ioa_spcf_rx_device_addr[port] = edp_aux_rx_bytes[5];
            g_ioa_spcf_i2c_rw_addr[port] = edp_aux_rx_bytes[6];
            g_ioa_spc_mode[port] = 1;
            edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
            tcon_aux_tx_bug_fix(1); 
        }
        else
        {
            edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
            tcon_aux_tx_bug_fix(1); 
        }
    }
    else if(g_edp_aux_req == EDP_AUX_REQ_RD)
    {
        if (seq_cnt == 4)
        {
            if(len > 2)
            {
                edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_NACK;
                tcon_aux_tx_bug_fix(1);
            }
            else
            {
                edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
                edp_aux_tx_bytes[2] = g_ioa_spcf_rx_device_addr[port];
                edp_aux_tx_bytes[3] = g_ioa_spcf_i2c_rw_addr[port];
                tcon_aux_tx_bug_fix(len+1);
            }
        }
        else
        {
            edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
            tcon_aux_tx_bug_fix(1);
        }
    }
}

#endif
