/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: eDP
		Platform: 8051
		Author: Eric Lee, 2021/04/07
		Description: spi over aux
*/
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"

#ifdef EDP_FUN
#include "edp_aux.h"
#include "edp_dpcd_rd.h"
#include "edp_dpcd_wt.h"
#include "mcu_i2c.h"
#include "mcu_timer.h"
#include "mcu_uart.h"
#include "ioa.h"
#include "ioa_spi.h"
#include "edp_global_vars.h"
#include "ioa_tcon_reg.h"
#include "ioa_i2c_chg.h"
#include "reg_include.h"
#include "edp_misc.h"
// *******************************************************************
#define EDP_I2C_MODE            1
#define EDP_SPI_FLASH_MODE      2
#define EDP_SPI_LED_MODE        3
#define EDP_MIPI_MODE           4
#define EDP_RW_TCON_MODE        5

#define EDP_SPCF_IOA_FUN

#ifdef EDP_SPCF_IOA_FUN
#define EDP_SPCF_I2C_CHK_MODE   6
//#define EDP_SPCF_I2C_MST_MODE   7
//uint8_t xdata g_ioa_spcf_rx_device_addr[EDP_PORT_NUM];              // ioa specific slv addr
//uint8_t xdata g_ioa_spc_mode[EDP_PORT_NUM];
//uint8_t xdata g_ioa_spcf_i2c_rw_addr[EDP_PORT_NUM];                 // i2c transfer slv addr 

#endif
// *******************************************************************
extern void tcon_aux_tx_bug_fix(uint8_t val);
// *******************************************************************
void edp_aux_proc(uint8_t port)
{
    if (edp_aux_tx_bytes[0] == 0) {
        if (edp_aux_rx_bytes[0] != 0) {
            P1_3 = 1;
            mcu_timer_delay_us(g_edp_aux_reply_timeout_target);
            if (edp_chk_aux_seq_abnormal()) {
                #if 0
                mcu_uart_putchar('D');
                mcu_uart_put_hex(edp_aux_rx_bytes[0]);
                mcu_uart_put_hex(edp_aux_rx_bytes[1]);
                mcu_uart_put_hex(edp_aux_rx_bytes[2]);
                mcu_uart_put_hex(edp_aux_rx_bytes[3]);
                mcu_uart_put_hex(edp_aux_rx_bytes[4]);
                mcu_uart_put_hex(edp_aux_rx_bytes[5]);
                mcu_uart_put_hex(edp_aux_rx_bytes[6]);
                mcu_uart_put_hex(edp_aux_rx_bytes[7]);
                mcu_uart_put_hex(edp_aux_rx_bytes[8]);
                mcu_uart_putchar('F');
                #endif
                edp_aux_reply_aux_defer();
            } 
            else 
            {
                edp_aux_handler(port);
            }
            edp_aux_rx_bytes[0] = 0;
            mcu_timer_stop_timeout_us();
            P1_3 = 0;
        } 
        else if (edp_aux_in_defer[port]) {
            edp_aux_i2c_defer_handler(port);
        }
    }
}
// *******************************************************************
uint8_t check_aux_slv_addr(uint8_t val, uint8_t port)
{
    if(edp_ioa_mode_en_flag[port])
    {
        if((val == g_edp_ioa_edid_addr) || (val == g_rw_dp_009Bh_intel_i2c_slv_addr))
            return EDP_I2C_MODE;
        
        if(val == g_edp_ioa_spi_flash_addr)
            return EDP_SPI_FLASH_MODE;
        else if(val == g_edp_ioa_tcon_reg_addr)
            return EDP_RW_TCON_MODE;
    #ifdef EDP_SPCF_IOA_FUN
        else if(val == 0x66)
            return EDP_SPCF_I2C_CHK_MODE;
        //else if((val == g_ioa_spcf_rx_device_addr[port]) && (g_ioa_spc_mode[port]))
            //return EDP_SPCF_I2C_MST_MODE;
    #endif
    }
    
    if ((val != g_edp_aux_edid_segment_addr) && \
        (val != g_edp_aux_edid_slave_addr) && \
        (val != g_edp_aux_i2c_en_addr))   
        return EDP_I2C_MODE;
    else
        return 0;

}
// *******************************************************************
uint8_t cal_wr_byte_cnt(void)       // Eric Add by 20201109
{
    return (edp_aux_rx_bytes[4] + 1);
}
// *******************************************************************
static void edp_aux_proc_read_edid(uint8_t seq_cnt, uint8_t port)
{
    if (seq_cnt == 4) 
    {
        for (g_edp_aux_wr_byte_idx[port] = 0; g_edp_aux_wr_byte_idx < g_edp_aux_wr_byte_cnt[port]; g_edp_aux_wr_byte_idx[port]++) 
        {
            edp_aux_tx_bytes[g_edp_aux_wr_byte_idx[port] + 2] = edp_aux_edid_read();
        }
    } 
    if (g_edp_aux_mot == 0) 
    {
        g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_IDLE;
    }
    else 
    {
        g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_RD;
    }
    edp_aux_tx_bytes[1] = EDP_AUX_REPLY_ACK;
    tcon_aux_tx_bug_fix(g_edp_aux_wr_byte_idx[port] + 1);
    g_edp_aux_wr_byte_idx[port] = 0;
}
// *******************************************************************
static void edp_aux_rw_dpcd_proc(uint8_t port)
{
    uint8_t cnt, real_cnt, seq_cnt;
    uint8_t addr_m, addr_l;
    uint8_t tmp_cmd;

    seq_cnt = edp_aux_rx_bytes[0];                  // recv data len
    tmp_cmd = edp_aux_rx_bytes[1];
    addr_m = edp_aux_rx_bytes[2];                   // addr15:8
    addr_l = edp_aux_rx_bytes[3];                   // addr7:0
    cnt = edp_aux_rx_bytes[4];                      // len
	g_edp_aux_req = tmp_cmd & EDP_AUX_REQ_MASK_NATIVE;

    if (g_edp_aux_req == EDP_AUX_REQ_WR) {    // DPCD write type
        seq_cnt -= 5;                               // rx[0]~[4] : seq_cnt = seq_cnt - 5
        for (real_cnt = 0; real_cnt <= seq_cnt; real_cnt++) {
            if (edp_dpcd_write(addr_m, addr_l, edp_aux_rx_bytes[5 + real_cnt]) == FALSE) {  // edp_aux_rx_bytes[1] & 0x0F : addr19:16
                break;
            }
            addr_l++;
            if (addr_l == 0) {
                addr_m++;
            }
        }
        if (real_cnt == cnt + 1) {
            edp_aux_tx_bytes[1] = EDP_AUX_REPLY_ACK;
            tcon_aux_tx_bug_fix(1);
        } else {
            edp_aux_tx_bytes[1] = EDP_AUX_REPLY_NACK;
            edp_aux_tx_bytes[2] = real_cnt;
            tcon_aux_tx_bug_fix(2);
        }
    } else { // if (edp_aux_req == EDP_AUX_REQ_RD) { // already checked command correctness, either write or read
        for (real_cnt = 0; real_cnt <= cnt; real_cnt++) {
            edp_aux_tx_bytes[real_cnt + 2] = edp_dpcd_read(edp_aux_rx_bytes[1] & 0x0F, addr_m, addr_l);
            addr_l++;
            if (addr_l == 0) {
                addr_m++;
            }
        }
        edp_aux_tx_bytes[1] = EDP_AUX_REPLY_ACK;
        g_edp_flag[port].err_cnt_latch_flg = 0;
        tcon_aux_tx_bug_fix(real_cnt+1);
    }
}
// *******************************************************************
static void edp_i2c_req_status_upd(uint8_t port)
{
    if (g_edp_aux_last_i2c[port] == EDP_AUX_LAST_I2C_IDLE) {
        edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
        tcon_aux_tx_bug_fix(1);

    } else if ((g_edp_aux_last_i2c[port] == EDP_AUX_LAST_I2C_WR) && (g_edp_i2c_rw_addr[port] == g_edp_aux_last_i2c_addr[port])) {       // Send last status
        edp_aux_tx_bytes[1] = g_edp_aux_defer_wr_st[port];   
        if (g_edp_aux_defer_wr_cnt[port] == 0) {
            tcon_aux_tx_bug_fix(1);
        } else {
            edp_aux_tx_bytes[2] = g_edp_aux_defer_wr_idx[port];
            tcon_aux_tx_bug_fix(2);
        }
    } else {
        edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_NACK;
        tcon_aux_tx_bug_fix(1);
        edp_aux_in_defer[port] = FALSE;
        mcu_i2c_mst_write_stop(20);
        g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_IDLE;
        g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;   // need check
    }
}
// *******************************************************************
static void edp_i2c_req_write(uint8_t port)
{
    bool i2c_acked = 0 , timeout = 0;
    bool run_i2c_flag = 1;
    uint8_t seq_cnt = 0;
    uint8_t real_cnt;

    seq_cnt = edp_aux_rx_bytes[0];          // recv data len

    // i2c command : write  
    if (edp_aux_in_defer[port]) 
    {
        if ((g_edp_aux_last_i2c[port] == EDP_AUX_LAST_I2C_WR) && (seq_cnt == 3) && 
            (g_edp_aux_mot == 0) && (g_edp_i2c_rw_addr[port] == g_edp_aux_last_i2c_addr[port])) 
        {  
            edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
            tcon_aux_tx_bug_fix(1);
        } 
        else 
        {
            edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_NACK;       // >> ??
            tcon_aux_tx_bug_fix(1);
        }
        edp_aux_in_defer[port] = FALSE;
        mcu_i2c_mst_write_stop(20);
        g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_IDLE;
        g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;     // need check
    } 
    else 
    {   // Add ioa function , modify by Eric     
        i2c_acked = TRUE;
        if(check_aux_slv_addr(g_edp_i2c_rw_addr[port], port))     // eric add at 20201030
        {
            run_i2c_flag = check_ioa_cfg(g_edp_i2c_rw_addr[port], port); 
            if(run_i2c_flag) 
            {        // eric add at 20201030
                if (g_edp_aux_last_i2c[port] == EDP_AUX_LAST_I2C_IDLE) 
                {
                    mcu_i2c_mst_write_start_addr(g_edp_i2c_rw_addr[port], 0, FALSE); // Start
                    i2c_acked = mcu_i2c_mst_recv_ack();
                    //g_get_i2c_offset_flag = 0;
                } 
                else if (g_edp_aux_last_i2c[port] == EDP_AUX_LAST_I2C_RD) 
                {
                    mcu_i2c_mst_write_start_addr(g_edp_i2c_rw_addr[port], 0, TRUE); // Repeated Start
                    i2c_acked = mcu_i2c_mst_recv_ack();
                } 
                else if (g_edp_i2c_rw_addr[port] != g_edp_aux_last_i2c_addr[port]) 
                {
                    mcu_i2c_mst_write_start_addr(g_edp_i2c_rw_addr[port], 0, TRUE);
                    i2c_acked = mcu_i2c_mst_recv_ack();
                }
            }
        }

        if (i2c_acked) 
        {
            g_edp_aux_last_i2c_addr[port] = g_edp_i2c_rw_addr[port];

            if(check_aux_slv_addr(g_edp_i2c_rw_addr[port], port) && run_i2c_flag)     // eric add at 20201030
            {
                if (seq_cnt > 4) 
                {
                    g_edp_aux_wr_byte_cnt[port] = cal_wr_byte_cnt();  // eric add at 20201109  
                    timeout = FALSE;
                    for (real_cnt = 0; real_cnt < g_edp_aux_wr_byte_cnt[port]; real_cnt++) 
                    {
                        g_edp_aux_bytes[real_cnt] = edp_aux_rx_bytes[5 + real_cnt];
                    }
                    for (g_edp_aux_wr_byte_idx[port] = 0; g_edp_aux_wr_byte_idx[port] < g_edp_aux_wr_byte_cnt[port]; g_edp_aux_wr_byte_idx[port]++) 
                    {
                        if (mcu_timer_check_timeout_us()) 
                        {
                            timeout = TRUE;
                            break;
                        }
                        mcu_i2c_mst_write_byte(g_edp_aux_bytes[g_edp_aux_wr_byte_idx[port]]);
                        if (mcu_i2c_mst_recv_ack() == FALSE) 
                        {
                            break;
                        }
                    }
                    if (timeout) 
                    {
                        edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
                        edp_aux_tx_bytes[2] = g_edp_aux_wr_byte_idx[port];

                        tcon_aux_tx_bug_fix(2);
                        g_edp_aux_defer_wr_cnt[port] = g_edp_aux_wr_byte_cnt[port];
                        g_edp_aux_defer_wr_idx[port] = g_edp_aux_wr_byte_idx[port];
                        edp_aux_in_defer[port] = TRUE;
                    } 
                    else if (g_edp_aux_wr_byte_idx[port] >= g_edp_aux_wr_byte_cnt[port]) 
                    {
                        edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
                        tcon_aux_tx_bug_fix(1);
                        g_edp_aux_wr_byte_idx[port] = 0;
                    } else 
                    {
                        edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_NACK;
                        edp_aux_tx_bytes[2] = g_edp_aux_wr_byte_cnt[port];
                        tcon_aux_tx_bug_fix(2);
                        g_edp_aux_wr_byte_idx[port] = 0;
                    }
                } 
                else 
                {
                    edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
                     tcon_aux_tx_bug_fix(1);
                }
                if (g_edp_aux_mot == 0) 
                {
                    mcu_i2c_mst_write_stop(20);
                    g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_IDLE;
                    g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;
                } 
                else 
                {
                    g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_WR;
                }
            } 
            else if (g_edp_i2c_rw_addr[port] == g_edp_aux_edid_slave_addr) 
            {   // EDID
                if (seq_cnt > 4 ) 
                {
                    //(edp_aux_rx_bytes[5]);
                    g_edp_aux_edid_offset = edp_aux_rx_bytes[5];
                }
                edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
                tcon_aux_tx_bug_fix(1);

                if (g_edp_aux_mot == 0) 
                    g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_IDLE;
                else 
                    g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_WR;
            } 
            else if(g_edp_i2c_rw_addr[port] == g_edp_aux_edid_segment_addr)
            {   // segment offset
                edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
                tcon_aux_tx_bug_fix(1);
            }
            else if(g_edp_i2c_rw_addr[port] == g_edp_aux_i2c_en_addr)
            {
                ioa_unlock_handler(port);
                  
                if(g_edp_aux_last_i2c[port] != EDP_AUX_LAST_I2C_IDLE)
                {
                    mcu_i2c_mst_write_stop(20);
                    g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_IDLE;    
                    g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0; 
                }     
                edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
                tcon_aux_tx_bug_fix(1);
            }
            else 
            {
                edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_NACK;
                tcon_aux_tx_bug_fix(1);
                g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_WR;
            }
        } 
        else 
        {
            edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_NACK;
            tcon_aux_tx_bug_fix(1);
            //g_edp_aux_last_i2c = EDP_AUX_LAST_I2C_WR;
            mcu_i2c_mst_write_stop(20);                       // add for ioa issue from 303
            g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_IDLE;    
            g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0; 
        }
    }
}
// *******************************************************************
static void edp_i2c_req_read(uint8_t port)
{
    uint8_t seq_cnt = 0;
    bool i2c_acked = 0 , timeout = 0;
    bool run_i2c_flag = 1 , i2c_run_read_flag = 0 , i2c_send_nack_flag = 0;
    uint8_t real_cnt;

    seq_cnt = edp_aux_rx_bytes[0];          // recv data len
    g_edp_aux_wr_byte_cnt[port] = cal_wr_byte_cnt();  // Eric add by 20201109  

    if ((edp_aux_in_defer[port]) && \
        ((g_edp_aux_last_i2c[port] != EDP_AUX_LAST_I2C_RD) || (seq_cnt != 4) || (g_edp_i2c_rw_addr[port] != g_edp_aux_last_i2c_addr[port]))) 
    {
        if ((g_edp_aux_last_i2c[port] == EDP_AUX_LAST_I2C_RD) && (seq_cnt == 3) && (g_edp_aux_mot == 0) && \
            (g_edp_i2c_rw_addr[port] == g_edp_aux_last_i2c_addr[port])) 
        {
            edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
            tcon_aux_tx_bug_fix(1);
        } 
        else 
        {
            edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_NACK;
            tcon_aux_tx_bug_fix(1);
        }
        edp_aux_in_defer[port] = FALSE;
        mcu_i2c_mst_write_stop(20);
        g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_IDLE;
        g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;
    } 
    else 
    {
        i2c_acked = TRUE;
        if(check_aux_slv_addr(g_edp_i2c_rw_addr[port], port))     // eric add at 20201030
        {
            run_i2c_flag = check_ioa_cfg(g_edp_i2c_rw_addr[port], port);             
            if(run_i2c_flag) 
            {  // eric add at 20201030  
                if (g_edp_aux_last_i2c[port] == EDP_AUX_LAST_I2C_IDLE) 
                {
                    mcu_i2c_mst_write_start_addr(g_edp_i2c_rw_addr[port], 1, FALSE);
                    i2c_acked = mcu_i2c_mst_recv_ack();
                } 
                else if (g_edp_aux_last_i2c[port] == EDP_AUX_LAST_I2C_WR) 
                {
                    // patch 
                    if(seq_cnt == 3 && g_edp_aux_mot == 0)
                    {
                        edp_aux_tx_bytes[1] = EDP_AUX_REPLY_ACK;    // add for ioa issue from 303 
                        tcon_aux_tx_bug_fix(1);

                        mcu_i2c_mst_write_stop(20);
                        g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_IDLE;
                        g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;   // slave to i2c sec path
                        return;
                    }
                    else // patch end
                        mcu_i2c_mst_write_start_addr(g_edp_i2c_rw_addr[port], 1, TRUE);
                    
                    i2c_acked = mcu_i2c_mst_recv_ack();
                } 
                else if (g_edp_i2c_rw_addr[port] != g_edp_aux_last_i2c_addr[port]) 
                {
                    mcu_i2c_mst_write_start_addr(g_edp_i2c_rw_addr[port], 0, TRUE);
                    i2c_acked = mcu_i2c_mst_recv_ack();
                }
            }
        }
        if (i2c_acked) 
        {
            g_edp_aux_last_i2c_addr[port] = g_edp_i2c_rw_addr[port]; 
            if(check_aux_slv_addr(g_edp_i2c_rw_addr[port], port) && run_i2c_flag)     // eric add at 20201030
            {  // eric add at 20201030       
                if (seq_cnt == 4 || i2c_run_read_flag == 1) 
                {
                    //g_edp_aux_wr_byte_cnt = edp_aux_rx_bytes[4] + 1;
                    //g_edp_aux_wr_byte_cnt = cal_wr_byte_cnt();  // Eric add by 20201109  
                    if (edp_aux_in_defer[port]) 
                    {
                        if (g_edp_aux_defer_wr_idx[port] != 0) 
                        {
                            if (g_edp_aux_defer_wr_idx[port] >= g_edp_aux_wr_byte_cnt[port]) 
                            {
                                for (g_edp_aux_wr_byte_idx[port] = 0; g_edp_aux_wr_byte_idx[port] < g_edp_aux_wr_byte_cnt[port]; g_edp_aux_wr_byte_idx[port] ++) 
                                {
                                    edp_aux_tx_bytes[g_edp_aux_wr_byte_idx[port] + 2] = g_edp_aux_bytes[g_edp_aux_wr_byte_idx[port]];
                                }
                                g_edp_aux_defer_wr_idx[port] -= g_edp_aux_wr_byte_cnt[port];
                                g_edp_aux_defer_wr_cnt[port] = g_edp_aux_defer_wr_idx[port];
                                if (g_edp_aux_defer_wr_idx[port] != 0) 
                                {
                                    for (real_cnt = 0; real_cnt < g_edp_aux_defer_wr_idx[port]; real_cnt++) 
                                    {
                                        g_edp_aux_bytes[real_cnt] = g_edp_aux_bytes[real_cnt + g_edp_aux_wr_byte_cnt[port]];
                                    }
                                } 
                                else 
                                {
                                    edp_aux_in_defer[port] = FALSE;
                                }
                            } 
                            else 
                            {
                                for (g_edp_aux_wr_byte_idx[port] = 0; g_edp_aux_wr_byte_idx[port] < g_edp_aux_defer_wr_idx[port]; g_edp_aux_wr_byte_idx[port]++)
                                {
                                    edp_aux_tx_bytes[g_edp_aux_wr_byte_idx[port] + 2] = g_edp_aux_bytes[g_edp_aux_wr_byte_idx[port]];
                                }
                                edp_aux_in_defer[port] = FALSE;
                            }
                        } 
                        else 
                        {
                            g_edp_aux_wr_byte_idx[port] = 0;
                            edp_aux_in_defer[port] = FALSE;
                        }
                    } 
                    else 
                    {
                        g_edp_aux_wr_byte_idx[port] = 0;
                    }
                    timeout = FALSE;
                    for (/* */; g_edp_aux_wr_byte_idx[port] < g_edp_aux_wr_byte_cnt[port]; g_edp_aux_wr_byte_idx[port]++) 
                    {
                        if (mcu_timer_check_timeout_us()) 
                        {
                            timeout = TRUE;
                            break;
                        }
                        // Eric Add at 20201212
                        if((g_edp_aux_mot == 0) && (g_edp_aux_wr_byte_idx[port] == g_edp_aux_wr_byte_cnt[port] - 1))
                        {
                            i2c_send_nack_flag = TRUE;         
                        }
                        // Add End
                        edp_aux_tx_bytes[g_edp_aux_wr_byte_idx[port] + 2] = mcu_i2c_mst_read_byte(((g_edp_aux_mot == 0) && (g_edp_aux_wr_byte_idx[port] == g_edp_aux_wr_byte_cnt[port] - 1))? TRUE: FALSE);
                    }
                    edp_aux_tx_bytes[1] = EDP_AUX_REPLY_ACK;
                    tcon_aux_tx_bug_fix(g_edp_aux_wr_byte_idx[port] + 1);
                    if (timeout) 
                    {
                        edp_aux_in_defer[port] = TRUE;
                        g_edp_aux_defer_wr_cnt[port] = g_edp_aux_wr_byte_cnt[port] - g_edp_aux_wr_byte_idx[port];
                        g_edp_aux_defer_wr_idx[port] = 0;
                    }
                } 
                else 
                {
                    edp_aux_tx_bytes[1] = EDP_AUX_REPLY_ACK;
                    tcon_aux_tx_bug_fix(1);
                }
                if (g_edp_aux_mot == 0) 
                {
                    // Eric Add at 20201212
                    if(seq_cnt == 3 && g_edp_aux_last_i2c[port] == EDP_AUX_LAST_I2C_RD && i2c_send_nack_flag == 0)
                    {
                        mcu_i2c_send_nack();
                    }
                    mcu_i2c_mst_write_stop(20);
                    g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_IDLE;
                    g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;        // slave to i2c sec path
                } 
                else 
                {
                    g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_RD;
                }
            } 
            else if (g_edp_aux_last_i2c_addr[port] == g_edp_aux_edid_slave_addr) 
            { // EDID
                edp_aux_proc_read_edid(seq_cnt, port);
            } 
            else if (g_edp_aux_last_i2c_addr[port] == g_edp_aux_edid_segment_addr)
            { // eric add at 20201030
                edp_aux_tx_bytes[1] = EDP_AUX_REPLY_ACK;
                tcon_aux_tx_bug_fix(1);
            } 
            else 
            {    // eric add at 20201030
                if (g_edp_aux_mot == 0) 
                {
                    g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_IDLE;
                    edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
                }
                else
                {
                    g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_RD;
                    edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_NACK;
                }
                tcon_aux_tx_bug_fix(1);
            }
        } 
        else 
        {
            //g_edp_aux_last_i2c = EDP_AUX_LAST_I2C_RD;
            edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_NACK;
            tcon_aux_tx_bug_fix(1);
            
            mcu_i2c_mst_write_stop(20);        // fix ioa issue from 303
            g_edp_aux_last_i2c[port] = EDP_AUX_LAST_I2C_IDLE;
            g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;
        }
    }
}

// *******************************************************************
static void edp_i2c_over_aux(uint8_t port)
{
    if(g_edp_aux_req == EDP_AUX_REQ_WR_ST_UPD)
    {
        edp_i2c_req_status_upd(port);
    }
    else if(g_edp_aux_req == EDP_AUX_REQ_WR)
    {
        edp_i2c_req_write(port);
    }
    else if(g_edp_aux_req == EDP_AUX_REQ_RD)
    {
        edp_i2c_req_read(port);    
    }
}
// *******************************************************************
void edp_aux_handler(uint8_t port)
{
    uint8_t tmp_cmd;
    uint8_t edp_run_mode;
    uint8_t aux_rx_dev;

    tmp_cmd = edp_aux_rx_bytes[1]; 
    g_edp_aux_type = tmp_cmd & EDP_AUX_TYPE_MASK;

    if (g_edp_aux_type == EDP_AUX_TYPE_NATIVE)                      // bit3 = 1 (Native AUX CH transaction)
    {  
        edp_aux_rw_dpcd_proc(port);  
    }
    else        // I2C
    {
        g_edp_aux_mot = tmp_cmd & EDP_AUX_REQ_MASK_MOT;       // I2c Bit2 , MOT
        g_edp_aux_req = tmp_cmd & EDP_AUX_REQ_MASK_I2C;       // write / read / write status request
        aux_rx_dev = edp_aux_rx_bytes[3];
        //g_edp_i2c_rw_addr[port] = edp_aux_rx_bytes[3];    
        //edp_run_mode = check_aux_slv_addr(g_edp_i2c_rw_addr[port], port);
        edp_run_mode = check_aux_slv_addr(aux_rx_dev, port);
        if(edp_run_mode == EDP_SPI_FLASH_MODE)
        {
            edp_spi_over_aux(port);         
            return;    
        }
        else if (edp_run_mode == EDP_RW_TCON_MODE)
        {
            edp_tcon_reg_over_aux(port);   
            return;    
        }
        #ifdef EDP_SPCF_IOA_FUN
        else if (edp_run_mode == EDP_SPCF_I2C_CHK_MODE)
        {
            ioa_i2c_dev_chg(port);
            return;
        }
        #endif
        else 
        {
            g_edp_i2c_rw_addr[port] = ioa_i2c_chg_check_adr(edp_aux_rx_bytes[3],port); 
            edp_i2c_over_aux(port);
        }
    }
}
// *******************************************************************
void edp_aux_i2c_defer_handler(uint8_t port)
{
    if (g_edp_aux_last_i2c[port] == EDP_AUX_LAST_I2C_WR) {      // i2c write 
        mcu_i2c_mst_write_byte(g_edp_aux_bytes[g_edp_aux_defer_wr_idx[port]]);
        if (mcu_i2c_mst_recv_ack()) {
            g_edp_aux_defer_wr_idx[port] = g_edp_aux_defer_wr_idx[port] + 1;
            g_edp_aux_defer_wr_st[port] = EDP_AUX_REPLY_I2C_ACK;
            if (g_edp_aux_defer_wr_idx[port] >= g_edp_aux_defer_wr_cnt[port]) {
                g_edp_aux_defer_wr_cnt[port] = 0;
                if (g_edp_aux_mot == 0) {
                    mcu_i2c_mst_write_stop(20);
                    g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;   // i2c pri path change back to slave
                }
                edp_aux_in_defer[port] = FALSE;
            }
        } else {
            g_edp_aux_defer_wr_st[port] = EDP_AUX_REPLY_I2C_NACK;
            edp_aux_in_defer[port] = FALSE;
        }
    } else {            // i2c read
        if (g_edp_aux_defer_wr_idx[port] < g_edp_aux_defer_wr_cnt[port]) {
            g_edp_aux_bytes[g_edp_aux_defer_wr_idx[port]] = mcu_i2c_mst_read_byte(((g_edp_aux_mot == 0) && (g_edp_aux_defer_wr_idx[port] == g_edp_aux_defer_wr_cnt[port] - 1)) ? TRUE : FALSE);
            g_edp_aux_defer_wr_idx[port] = g_edp_aux_defer_wr_idx[port] + 1;
            if (g_edp_aux_defer_wr_idx[port] >= g_edp_aux_defer_wr_cnt[port]) {
                if (g_edp_aux_mot == 0) {
                    mcu_i2c_mst_write_stop(20);
                    g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;   // i2c pri path change back to slave
                }
            }
        }
    }
}

// *******************************************************************
bool edp_chk_aux_seq_abnormal(void)
{
    uint8_t seq_cnt, cnt;
    uint8_t tmp_cmd, aux_type, aux_req, aux_mot;

    seq_cnt = edp_aux_rx_bytes[0];
    tmp_cmd = edp_aux_rx_bytes[1];
    aux_type = tmp_cmd & EDP_AUX_TYPE_MASK;
    if (aux_type == EDP_AUX_TYPE_NATIVE) {
        aux_req = tmp_cmd & EDP_AUX_REQ_MASK_NATIVE;
        cnt = edp_aux_rx_bytes[4];
        if (aux_req == EDP_AUX_REQ_WR) {
            if ((seq_cnt <= 4) || (seq_cnt != cnt + 5)) {
                //mcu_uart_puts("AW err\n");
                //__WIN32_EMULATION_PRINTF(">>> ABN_AUX_SEQ <<<\n");
                return TRUE;
            }
        } else if (aux_req == EDP_AUX_REQ_RD) {
            if ((seq_cnt != 4) || (cnt > 15)) {
                //mcu_uart_puts("AR err\n");
                //__WIN32_EMULATION_PRINTF(">>> ABN_AUX_SEQ <<<\n");
                return TRUE;
            }
        } else {
            //__WIN32_EMULATION_PRINTF(">>> ABN_AUX_SEQ <<<\n");
            return TRUE;
        }
    } else { // EDP_AUX_TYPE_I2C
        aux_mot = tmp_cmd & EDP_AUX_REQ_MASK_MOT;
        aux_req = tmp_cmd & EDP_AUX_REQ_MASK_I2C;
        cnt = edp_aux_rx_bytes[4];
        if (aux_req == EDP_AUX_REQ_WR) {
            if ((seq_cnt != 3) && ((seq_cnt <= 4) || (seq_cnt != cnt + 5))) {
                //mcu_uart_puts("IW err\n");
                //__WIN32_EMULATION_PRINTF(">>> ABN_AUX_SEQ <<<\n");
                return TRUE;
            }
        } else if (aux_req == EDP_AUX_REQ_RD) {
            if ((seq_cnt != 3) && ((seq_cnt != 4) || (cnt > 15))) {
                //mcu_uart_puts("IR err\n");
                //__WIN32_EMULATION_PRINTF(">>> ABN_AUX_SEQ <<<\n");
                return TRUE;
            }
        }
    }

    return FALSE;
}
// *******************************************************************
void edp_aux_reply_aux_defer(void)
{
    edp_aux_tx_bytes[1] = EDP_AUX_REPLY_DEFER;
    tcon_aux_tx_bug_fix(1);
}

uint8_t edp_aux_edid_read(void)
{
    uint8_t *ptr = &g_rw_edid_0000h_00;
    return *(ptr + (g_edp_aux_edid_offset++));
    //return g_edp_aux_edid[g_edp_aux_edid_offset++];
}
#endif
