#include "mcu_fun_def.h"

#ifdef EDP_FUN
#include <string.h>
#include "edp_misc.h"
#include "tcon_misc.h"
#include "tcon_isp.h"
#include "reg_include.h"
#include "mcu_i2c.h"

#include "tcon_cascade.h"

//bool g_hpd_irq_flg;
bool g_hpd_irq_port0_flg = 0;
bool g_hpd_irq_port1_flg = 0;
bool g_psr_irq_flg = 0;
bool g_rsv_irq_flg = 0;
bool g_intel_ubrr_irq_flag = 0;

uint8_t xdata g_rx_lrd_adv_pol_rdd3_old;
uint8_t xdata g_edp_lrd_advance_pol_proc_cnt = 0;

#ifdef EDP1_4_FUN
uint8_t xdata g_intel_ubrr_status_old = 0;
uint8_t xdata g_intel_ubrr_entry = 0;
uint8_t xdata g_intel_ubrr_vsync_cnt = 0;
uint8_t xdata g_psr_cap_temp[5] = 0;
#endif

// cascade variable
#ifdef CASCADE_EDP_FUN
uint8_t xdata g_edp_cascade_mstr_data_buf[4] = 0;
uint8_t xdata g_edp_cascade_slv_data_buf[4] = 0;
#endif

void edp_enable_dpcd_reg_remap(uint8_t val)
{
    g_rw_mcu_top_0043h.bits.r_dpcd_remap_en = val;
}

void edp_dpcd_reg_port_sel(uint8_t port)
{
    g_rw_mcu_top_0043h.bits.r_dpcd_remap_sel = port;
    g_edp_port = port;
}

void edp_init(void)
{
    uint8_t edp_port = 0;
  	g_rw_dpcd_reg_0007h_edp_state = EDP_STATE_INIT;
    g_rw_dpcd_reg_1_0007h_edp_state = EDP_STATE_INIT;
    memset(g_edp_flag, 0, sizeof(g_edp_flag));
    memset(&g_dpcd_ena_bits, 0, sizeof(g_dpcd_ena_bits));   // 2023 1108 add

    for(edp_port = 0; edp_port < EDP_PORT_NUM; edp_port++)
    {
        edp_aux_in_defer[edp_port] = FALSE;
        g_edp_aux_last_i2c[edp_port] = EDP_AUX_LAST_I2C_IDLE;
        g_edp_aux_defer_wr_st[edp_port] = EDP_AUX_REPLY_I2C_ACK;
        g_edp_aux_defer_wr_cnt[edp_port] = 0;
        g_edp_aux_wr_byte_cnt[edp_port] = 0;
        g_edp_aux_hpd_cnt[edp_port] = 0;
        g_edp_i2c_rw_addr[edp_port] = 0;
        g_edp_aux_last_i2c_addr[edp_port] = 0;
        g_edp_aux_wr_byte_idx[edp_port] = 0;
    }

    g_edp_aux_edid_segment_addr = (uint8_t)g_rw_mcu_top_001Eh.bits.r_edid_segement_dev_addr;
    g_edp_aux_i2c_en_addr = 0x65;                                               // eric add at 20201030 / for ioa password
    g_edp_ioa_pmic_addr = g_rw_mcu_top_0008h.bits.r_pmic_dev_adr;    
    g_edp_ioa_ls1_addr = (uint8_t)g_rw_mcu_top_0019h.bits.r_ioa_ls1_dev_addr; 
    g_edp_ioa_ls2_addr = (uint8_t)g_rw_mcu_top_001Ah.bits.r_ioa_ls2_dev_addr;
    g_edp_ioa_gma_addr = (uint8_t)g_rw_mcu_top_001Bh.bits.r_ioa_gma_dev_addr;
    g_edp_ioa_dvcom_addr = (uint8_t)g_rw_mcu_top_001Ch.bits.r_ioa_dvcom_dev_addr;
    g_edp_ioa_edid_addr = (uint8_t)g_rw_mcu_top_0006h.bits.r_exedid_dev_addr;
    g_edp_aux_edid_slave_addr = (uint8_t)g_rw_mcu_top_0006h.bits.r_exedid_dev_addr;     
    g_edp_aux_edid_offset = 0;
    g_edp_ioa_spi_flash_addr = (uint8_t)g_rw_mcu_top_001Dh.bits.r_ioa_spi_flash_dev_addr;
    g_edp_ioa_tcon_reg_addr = (uint8_t)g_rw_mcu_top_0003h.bits.r_slv_dev;       // peter add at 20211012

    __WIN32_EMULATION_EDP_INIT();
}

void edp_dpcd_err_cnt_latch(void)
{
    g_rw_dp_0049h.bits.dpreg_dpcd_err_latch = 1;   // hw: dpreg_dpcd_err_latch
    g_rw_dp_0049h.bits.dpreg_dpcd_err_latch = 0;
}

void edp_polling_err_cnt_latch(void)
{
	g_rw_dp_0049h.bits.dpreg_mcu_8b10b_latch = 1;   // hw: dpreg_mcu_8b10b_latch
	g_rw_dp_0049h.bits.dpreg_mcu_8b10b_latch = 0;
}

void edp_do_init_rst(void)
{
    edp_do_phy_rst();
}

bool edp_chk_rst_done(void)
{
    return TRUE;
}

void edp_do_trn_phs1_state_init(void)
{
    edp_do_phy_rst();
}
/*
void edp_do_trn_phs2_state_init(void)
{
//    edp_do_phy_rst(); // only for deskew issue
}*/

void edp_do_normal_state_init(uint8_t port)
{
    g_edp_timeout_cnt[port] = 0;
    g_edp_chk_timeout_cnt[port] = 0;
}

void edp_do_no_trn_state_init(uint8_t port)
{
    g_edp_timeout_cnt[port] = 0;
    g_edp_chk_timeout_cnt[port] = 0;
    //edp_do_phy_rst(); // only for deskew issue Eric remove
}

/*void edp_do_qual_test_state_init(void)
{
}*/

uint16_t edp_get_err_cnt(void)
{
    uint8_t ln_cnt;
    uint16_t err_cnt;
    uint16_t tmp;
    uint16_t lane_err;

    edp_polling_err_cnt_latch();   // Eric Add at 20201228
    ln_cnt = g_rw_dpcd_reg_003Ch.bits.r_LANE_COUNT_SET;
    
    lane_err = ((uint16_t)g_rw_dp_009Fh_lane0_to_mcu_sym_err_cnt_o << 8 | g_rw_dp_009Eh_lane0_to_mcu_sym_err_cnt_o);
    err_cnt = (g_rw_dp_009Fh_lane0_to_mcu_sym_err_cnt_o & 0x80) ? (lane_err & 0x7FFF) : 0;
    
    if (ln_cnt >= 2) 
    {
        lane_err = ((uint16_t)g_rw_dp_00A1h_lane1_to_mcu_sym_err_cnt_o << 8 | g_rw_dp_00A0h_lane1_to_mcu_sym_err_cnt_o);
        tmp = (g_rw_dp_00A1h_lane1_to_mcu_sym_err_cnt_o & 0x80)? (lane_err & 0x7FFF) : 0;  
        err_cnt = (err_cnt > tmp)? err_cnt: tmp;
    }

    if (ln_cnt >= 4) 
    {
        lane_err = ((uint16_t)g_rw_dp_00A3h_lane2_to_mcu_sym_err_cnt_o << 8 | g_rw_dp_00A2h_lane2_to_mcu_sym_err_cnt_o);
        tmp = (g_rw_dp_00A3h_lane2_to_mcu_sym_err_cnt_o & 0x80)? (lane_err & 0x7FFF) : 0;  
        err_cnt = (err_cnt > tmp)? err_cnt : tmp;

        lane_err = ((uint16_t)g_rw_dp_00A5h_lane3_to_mcu_sym_err_cnt_o << 8 | g_rw_dp_00A4h_lane3_to_mcu_sym_err_cnt_o);
        tmp = (g_rw_dp_00A5h_lane3_to_mcu_sym_err_cnt_o & 0x80)? (lane_err & 0x7FFF) : 0;
        err_cnt = (err_cnt > tmp)? err_cnt: tmp;
    }
    return err_cnt;
}

void edp_assert_irq_hpd(uint8_t port)
{
    if (g_rw_dpcd_reg_0006h_edp_aux_hpd == 1) {
        g_rw_dpcd_reg_0006h_edp_aux_hpd = 0;
        g_edp_aux_hpd_cnt[port] = 6;
    }
}

void edp_check_irq_hpd(uint8_t port)
{
    if (g_edp_aux_hpd_cnt[port] > 0) {
        g_edp_aux_hpd_cnt[port] = g_edp_aux_hpd_cnt[port] - 1;
        if (g_edp_aux_hpd_cnt[port] == 0)
            g_rw_dpcd_reg_0006h_edp_aux_hpd = 1;
    }
}

bool edp_link_get_lane_sta(uint8_t lane) 
{
    if(g_rw_dpcd_read_000Bh.bits.r_INTERLANE_ALIGN_DONE != 0x01)
        return 0;
    if(g_rw_dpcd_read_0009h.bits.r_LANE0_STATUS != 0x07)
        return 0;
    if((lane == 2) && (g_rw_dpcd_read_0009h.bits.r_LANE1_STATUS != 0x7))
        return 0;
	// add for 4 lane / check lane2 3 status
	if((lane >= 3) && (g_rw_dpcd_read_000Ah.byte != 0x77 ))
		return 0;
    return 1;
}

bool edp_link_status_ok(void)
{
    uint8_t ln_cnt;
    bool ln_st_ok;
#ifdef DBG_MCU_LOG       
    static bool l_last_st_ok = 0;
#endif
    ln_cnt = g_rw_dpcd_reg_003Ch.bits.r_LANE_COUNT_SET;
    ln_st_ok = edp_link_get_lane_sta(ln_cnt);
         
#ifdef DBG_MCU_LOG      
    if(g_rw_dp_0092h.bits.DBG_MSG_EN && !ln_st_ok)
    {
        if(l_last_st_ok != ln_st_ok)
        {
            if(g_rw_dpcd_read_000Bh.bits.r_INTERLANE_ALIGN_DONE != 0x01)
                mcu_uart_puts("f_al\n");
            else if(g_rw_dpcd_read_0009h.bits.r_LANE0_STATUS != 0x07)
            {
                mcu_uart_puts("f_l0:\n");
                mcu_uart_put_hex(g_rw_dpcd_read_0009h.bits.LANE0_STATUS);
            }  
            else if(g_rw_dpcd_read_0009h.bits.r_LANE1_STATUS != 0x07)
            {
                mcu_uart_puts("f_l1:\n"); 
                mcu_uart_put_hex(g_rw_dpcd_read_0009h.bits.r_LANE1_STATUS);
            }
            else if(g_rw_dpcd_read_000Ah.bits.r_LANE2_STATUS != 0x07)
            {
                mcu_uart_puts("f_l2:\n"); 
                mcu_uart_put_hex(g_rw_dpcd_read_000Ah.bits.r_LANE2_STATUS);
            }   
            else if(g_rw_dpcd_read_000Ah.bits.r_LANE3_STATUS != 0x07)
            {
                mcu_uart_puts("f_l3:\n"); 
                mcu_uart_put_hex(g_rw_dpcd_read_000Ah.bits.r_LANE3_STATUS);
            }   
        }      
    }
    l_last_st_ok = ln_st_ok;
#endif
    return ln_st_ok;
}

bool polling_edp_link_status_ok(void)
{
    uint8_t ln_cnt;
    bool ln_st_ok;
    //static bool l_last_st_ok = 0;
    ln_cnt = g_rw_dpcd_reg_003Ch.bits.r_LANE_COUNT_SET;
    //---------------------------------------------------------------------------------//
    // Fix Unigraf Test Item -> align = 0 , hpd irq
    //---------------------------------------------------------------------------------//
    if(g_rw_dpcd_read_000Bh.bits.r_INTERLANE_ALIGN_DONE != 0x01)
        return 0;
    //---------------------------------------------------------------------------------//
    ln_st_ok = ((g_rw_dpcd_read_0009h.bits.r_LANE0_STATUS) != 0x07)? 0:
               ((ln_cnt >= 2) && (g_rw_dpcd_read_0009h.bits.r_LANE1_STATUS != 0x7))? 0: 
               ((ln_cnt >= 3) && (g_rw_dpcd_read_000Ah.byte != 0x77))? 0:1;

    
    return ln_st_ok;
}

bool edp_link_status_sel_ok(void)
{
    uint8_t ln_cnt;
    uint8_t ln_val;
    bool ln_st_ok = 1;     // initial lane ok as default 
#ifdef DBG_MCU_LOG 
    static bool l_last_st_ok = 0;
#endif

    ln_cnt = g_rw_dpcd_reg_003Ch.bits.r_LANE_COUNT_SET;

    ln_val = (g_rw_dp_0095h.byte >> 5) & 0x07;
    //---------------------------------------------------------------------------------//
    // Fix Unigraf Test Item -> align = 0 , hpd irq 
    //---------------------------------------------------------------------------------//
    if(g_rw_dp_0095h.bits.hpd_pulse_ref_inter_lane_align)
	{
		if(g_rw_dpcd_read_000Bh.bits.r_INTERLANE_ALIGN_DONE != 0x01)
			return 0;
	}
    //---------------------------------------------------------------------------------//

    if(g_rw_dp_0095h.bits.hpd_pulse_ref_CR_lock || g_rw_dp_0095h.bits.hpd_pulse_ref_EQ_done || g_rw_dp_0095h.bits.hpd_pulse_ref_sym_lock)
    {
        ln_st_ok = ((g_rw_dpcd_read_0009h.bits.r_LANE0_STATUS & ln_val) != ln_val)? 0:
                   ((ln_cnt >= 2) && ((g_rw_dpcd_read_0009h.bits.r_LANE1_STATUS & ln_val) != ln_val))? 0: 
                   ((ln_cnt >= 3) && ((g_rw_dpcd_read_000Ah.bits.r_LANE2_STATUS & ln_val) != ln_val))? 0:
                   ((ln_cnt >= 3) && ((g_rw_dpcd_read_000Ah.bits.r_LANE3_STATUS & ln_val) != ln_val))? 0:1;
    }
#ifdef DBG_MCU_LOG      
    if(g_rw_dp_0092h.bits.DBG_MSG_EN && !ln_st_ok)
    {
        if(l_last_st_ok != ln_st_ok)
        {
            if(g_rw_dpcd_read_000Bh.bits.r_INTERLANE_ALIGN_DONE != 0x01)
                mcu_uart_puts("f_al\n");
            else if(g_rw_dpcd_read_0009h.bits.r_LANE0_STATUS != 0x07)
            {
                mcu_uart_puts("f_l0:\n");
                mcu_uart_put_hex(g_rw_dpcd_read_0009h.bits.r_LANE0_STATUS);
            }  
            else if((ln_cnt >= 2) && (g_rw_dpcd_read_0009h.bits.r_LANE1_STATUS != 0x07))
            {
                mcu_uart_puts("f_l1:\n"); 
                mcu_uart_put_hex(g_rw_dpcd_read_0009h.bits.r_LANE1_STATUS);
            }
            else if((ln_cnt >= 3) && (g_rw_dpcd_read_000Ah.bits.r_LANE2_STATUS != 0x07))
            {
                mcu_uart_puts("f_l2:\n"); 
                mcu_uart_put_hex(g_rw_dpcd_read_000Ah.bits.r_LANE2_STATUS);
            }
            else if((ln_cnt >= 3) && (g_rw_dpcd_read_000Ah.bits.r_LANE3_STATUS != 0x07))
            {
                mcu_uart_puts("f_l3:\n"); 
                mcu_uart_put_hex(g_rw_dpcd_read_000Ah.bits.r_LANE3_STATUS);
            }         
        }  
    }
    l_last_st_ok = ln_st_ok;
#endif
    return ln_st_ok;
}

bool edp_check_link_sta(void)
{
    if(!g_rw_dp_0095h.bits.hpd_pulse_ref_inter_lane_align && !g_rw_dp_0095h.bits.hpd_pulse_ref_CR_lock && 
			 !g_rw_dp_0095h.bits.hpd_pulse_ref_EQ_done && !g_rw_dp_0095h.bits.hpd_pulse_ref_sym_lock)
        return polling_edp_link_status_ok();
    else
        return edp_link_status_sel_ok();
}

bool edp_crd_status(void)
{
    uint8_t ln_cnt;
    bool crd_ok;
#ifdef DBG_MCU_LOG 
    static bool l_last_crd_ok = 0;
#endif

    ln_cnt = g_rw_dpcd_reg_003Ch.bits.r_LANE_COUNT_SET;
    
    crd_ok = ((g_rw_dpcd_read_0009h.bits.r_LANE0_STATUS & 0x01) != 0x01) ? 0 :
             ((ln_cnt >= 2) && (g_rw_dpcd_read_0009h.bits.r_LANE1_STATUS & 0x01) != 0x01) ? 0 : 
             ((ln_cnt >= 3) && (g_rw_dpcd_read_000Ah.byte & 0x11) != 0x11) ? 0 : 1;     // check lane3 & 4

#ifdef DBG_MCU_LOG 
    if(g_rw_dp_0092h.bits.DBG_MSG_EN && !crd_ok)
    {
        if(l_last_crd_ok != crd_ok)
        {
            mcu_uart_puts("f_cr:\n"); 
            mcu_uart_put_hex(g_rw_dpcd_read_0009h.bits.r_LANE0_STATUS );
            mcu_uart_put_hex(g_rw_dpcd_read_0009h.bits.r_LANE1_STATUS );   
            mcu_uart_put_hex(g_rw_dpcd_read_0009h.bits.r_LANE2_STATUS );
            mcu_uart_put_hex(g_rw_dpcd_read_0009h.bits.r_LANE3_STATUS );
        }
    }        
#endif
    return  crd_ok;       
}

void edp_do_phy_rst(void)
{
    g_rw_aip_reg_00E1h.bits.r_aip_rx_mode_chg = 1;      // rx_aphy mode change
    g_rw_aip_reg_00E1h.bits.r_aip_rx_mode_chg = 0;      // must clear by FW
}


void edp_dphy_rst(void)
{
    //g_dp_reg_082h |= 0xC0;    // rx dphy mode change
    //g_dp_reg_082h &= 0x3F;    // rx dphy mode change for JC Design
	g_rw_dp_0081h.bits.dpreg_sym_lock_clr = 3;
	g_rw_dp_0081h.bits.dpreg_sym_lock_clr = 0;
}

void edp_get_eeprom_cfg(void)
{
    ADDR_MUX = 0x07 ; // switch entry EDID
	g_edp_ioa_cfg_addr = ((EXTDAT_SFR_72H & 0x1C) >> 2) | 0x50;
}
/*
static void epd_pwrc_sel(uint8_t val)
{
    switch(val)
    {
        case 0:
        case 1:
        //g_rw_p2p_017Eh.bits.r_isp_pol_2_0 = g_dp_reg_mcu_cfg_09Bh.bits.r_mcu_isp_pol_2_0;
        //g_rw_p2p_017Fh.bits.r_isp_pol_8_3 = g_dp_reg_mcu_cfg_09Ch.bits.r_mcu_isp_pol_8_3;
				// use g_rw_dp_0097h.bits.r_mcu_isp_dyn_pol_bits_8 / g_rw_dp_0096h_mcu_isp_dyn_pol
            if(val == 1)
            {
                g_rw_p2p_001Bh.bits.r_lcs_len = g_rw_dp_009Ah.bits.r_mcu_isp_dyn_lcs_len + 1;
                g_rw_p2p_004Fh.bits.r_lcs_cmd0_idx = 6;
            }
            else
            {
                g_rw_p2p_001Bh.bits.r_lcs_len = g_rw_dp_009Ah.bits.r_mcu_isp_dyn_lcs_len;
                g_rw_p2p_004Fh.bits.r_lcs_cmd0_idx = 1;
            }
            g_rw_p2p_0475h.bits.r_isp_polb_en = 0;
        break;

        case 2:
        case 3:
            g_rw_p2p_001Bh.bits.r_lcs_len = g_rw_dp_009Ah.bits.r_mcu_isp_dyn_lcs_len + 1;
            g_rw_p2p_004Fh.bits.r_lcs_cmd0_idx = 6;
            g_rw_p2p_0475h.bits.r_isp_polb_en = 1;
        break;
        
        default:
        break;
    }
}
*/
static uint16_t edp_get_mcu_cfg(uint8_t cfg1, uint8_t cfg2 , uint8_t flg)
{
    uint16_t l_var[4] = {20,40,1000,300};
    if(cfg1 != 0 || cfg2 !=0)
        return ((uint16_t)cfg1 << 8 | cfg2);
    else
        return l_var[flg];
}

void edp_get_eeprom_mcu_cfg(void)
{
    g_no_lnk_trn_rst_chk_target = (edp_get_mcu_cfg(g_rw_dp_008Ch_CRD_Check_Timeout_MSB, g_rw_dp_008Bh_CRD_Check_Timeout_LSB, 0)) * 10;
    g_no_lnk_trn_rst_double_chk_target = (edp_get_mcu_cfg(g_rw_dp_008Eh_Link_Rst_Timeout_MSB, g_rw_dp_008Dh_Link_Rst_Timeout_LSB, 1)) * 10;
    g_edp_err_cnt_thd_target = edp_get_mcu_cfg(g_rw_dp_0090h_Err_Cnt_MSB, g_rw_dp_008Fh_Err_Cnt_LSB, 2); 
    
	if(g_rw_dp_0094h_Aux_Reply_Timeout == 0)
        g_edp_aux_reply_timeout_target = 300;
    else    
        g_edp_aux_reply_timeout_target <<= 1;
}

void edp_i2cbus_mux(bool i2c_path) 
{
    g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = i2c_path;
}

void tcon_aux_tx_bug_fix(uint8_t val)
{
    edp_aux_tx_bytes[0] = val;  
    //while (edp_aux_tx_bytes[0] != 0);
}

uint8_t epd_misc_get_edp_state(void)
{
    return g_rw_dpcd_reg_0007h_edp_state;
}

uint8_t epd_misc_get_edp_port1_state(void)
{
    return g_rw_dpcd_reg_1_0007h_edp_state;
}

void edp_misc_set_edp_state(uint8_t sta)
{
    g_rw_dpcd_reg_0007h_edp_state = sta;
}

void edp_set_hdp(bool hpd_val)
{
    //__WIN32_EMULATION_PRINTF2("[INFO] HPD = %s.\n", (hpd_val)? "High": "Low");
    g_rw_dpcd_reg_0006h_edp_aux_hpd = hpd_val;
}

void edp_multiport_sync_handler(void)
{
    if(g_rw_dpcd_read_000Bh.bits.r_INTERLANE_ALIGN_DONE && g_rw_dpcd_read_1_000Bh.bits.r_INTERLANE_ALIGN_DONE)
    {
        if(g_edp_flag_allport_iadone)
            return;
        // both two ports are interlane align done
        g_edp_multiport_iadone_sync_start_timeout = 0;
        g_edp_multiport_iadone_sync_tmcnt = 0;
        g_edp_flag_allport_iadone = 1;
        g_rw_dp_00AFh.bits.dpreg_inx_dp_lock = 1;    
    }
    else if(g_rw_dpcd_read_000Bh.bits.r_INTERLANE_ALIGN_DONE || g_rw_dpcd_read_1_000Bh.bits.r_INTERLANE_ALIGN_DONE)
    {
        if(g_edp_flag_allport_iadone == 0)
            g_edp_multiport_iadone_sync_start_timeout = 1;
    }
    else
    {
        g_edp_multiport_iadone_sync_start_timeout = 0;
        g_edp_multiport_iadone_sync_tmcnt = 0;
        g_edp_flag_allport_iadone = 0;
        g_rw_dp_00AFh.bits.dpreg_inx_dp_lock = 0;     
    }
}

void edp_multiport_iadone_sync_timeout_handler(void)
{
    if(g_edp_multiport_iadone_sync_start_timeout)
    {
        g_edp_multiport_iadone_sync_tmcnt ++;
        if(g_edp_multiport_iadone_sync_tmcnt == 50)     // 5ms target
        {
            g_edp_flag_allport_iadone = 1;
            g_edp_multiport_iadone_sync_tmcnt = 0;
            g_edp_multiport_iadone_sync_start_timeout = 0;
            g_rw_dp_00AFh.bits.dpreg_inx_dp_lock = 1;     
        }
    }
}

void edp_port_cfg(void)
{
    if((g_rw_aip_reg_0075h.byte & 0x0F) && (g_rw_aip_reg_0075h.byte & 0xF0))
    {
        g_edp_port_type = 0;
        g_edp_port_max = 2;
        g_edp_port_total_using = 2;
    }
    else if(g_rw_aip_reg_0075h.byte & 0x0F)
    {
        g_edp_port_type = 0;
        g_edp_port_max = 1;
        g_edp_port_total_using = 1;
    }
    else if(g_rw_aip_reg_0075h.byte & 0xF0)
    {
        g_edp_port_type = 1;
        g_edp_port_max = 2;
        g_edp_port_total_using = 1;  
    }
}

#ifdef EDP1_4_FUN
uint8_t edp_check_did_vii_detailed_timing_hvtotal(void)
{
    uint8_t *addr = &g_rw_edid_0084h_84;   // extend bolck + 4
    uint16_t vtotal = 0;
    uint16_t htotal = 0;

    if(!g_rw_edid_007Eh_7e)     // check extension block
        return 0x00;
    
    while(addr <= &g_rw_edid_00FFh_ff)
    {
        if(*addr == 0x22)   // check typevii detailed timing
        {
            htotal = (*(addr + 7) | (*(addr + 8) << 8)) + (*(addr + 9) | (*(addr + 10) << 8));
            vtotal = (*(addr + 15) | (*(addr + 16) << 8)) + (*(addr + 17) | (*(addr + 18) << 8));
            // set register
            g_rw_psr_0011h_edid_max_vtotal = (uint8_t)(vtotal & 0xFF);
            g_rw_psr_0012h_edid_max_vtotal = (uint8_t)(vtotal >> 8);
            g_rw_psr_000Dh_edid_max_htotal = (uint8_t)(htotal & 0xFF);
            g_rw_psr_000Eh_edid_max_htotal = (uint8_t)(htotal >> 8);
            return 1;
        }   
        else 
            addr += *(addr + 2) + 2; 
    }
    return 0x00;
}

void edp_sharp_lrd_advance_pol_proc(void)
{
    uint8_t data_tmp;
    if( g_rw_psr_007Dh.bits.ro_rx_lrd_adv_pol_rrd3 == g_rx_lrd_adv_pol_rdd3_old)
    {
        g_edp_lrd_advance_pol_proc_cnt ++;
        if(g_edp_lrd_advance_pol_proc_cnt == 4)
        {
            //g_rw_oproc_0141h.bits.r_pol_sharp_adv = 1;
            if(g_rx_lrd_adv_pol_rdd3_old == 0)
            {
                data_tmp = g_rw_psr_0073h.bits.r_lrd_adv_pol_sef1;
            }
            else if(g_rx_lrd_adv_pol_rdd3_old == 1)
            {
                data_tmp = g_rw_psr_0073h.bits.r_lrd_adv_pol_sef2;
            }
            else if(g_rx_lrd_adv_pol_rdd3_old == 2)
            {
                data_tmp = g_rw_psr_0074h.bits.r_lrd_adv_pol_sef3;
            }
            else if(g_rx_lrd_adv_pol_rdd3_old == 3)
            {
                data_tmp = g_rw_psr_0074h.bits.r_lrd_adv_pol_sef4;
            }
            g_rw_oproc_0141h.bits.r_pol_sharp_rlt_bits_1_0 = data_tmp & 0x03;
            g_rw_oproc_0142h.bits.r_pol_sharp_rlt_bits_2 = data_tmp >> 2;
            g_edp_lrd_advance_pol_proc_cnt = 0;
        }
    }
    else
    {
        g_edp_lrd_advance_pol_proc_cnt = 0;
        g_rx_lrd_adv_pol_rdd3_old = g_rw_psr_007Dh.bits.ro_rx_lrd_adv_pol_rrd3;
    }
}

void edp_sharp_lrd_touch_sensing_en_proc(void)
{
    static uint8_t vsync_hyst = 0;
    if(g_rw_psr_006Ch.bits.r_lrd_touch_en)
        return;
    if(epd_misc_get_edp_state() == EDP_STATE_NORMAL)
    {
        if(vsync_hyst == 1)   // late 1 vsync
        {
            g_rw_psr_006Ch.bits.r_lrd_touch_en = 1;
            vsync_hyst = 0;
        }
        else
            vsync_hyst ++;
    }
}
#endif

#ifdef CASCADE_EDP_FUN
// cascade function
void edp_cascade_update_data(uint8_t *buf)
{
    buf[1] = g_rw_dpcd_reg_003Bh_LINK_BW_SET;                    // link rate
    //buf[2] = g_rw_dpcd_reg_003Ch.bits.r_LANE_COUNT_SET;          // lane count
    buf[2] = g_rw_dpcd_reg_003Ch.bits.r_LANE_COUNT_SET + g_rw_dpcd_reg_1_003Ch.bits.r_LANE_COUNT_SET; 
    //buf[3] = g_rw_dpcd_read_000Bh.bits.r_INTERLANE_ALIGN_DONE;   // link status
    buf[3] = g_edp_flag_allport_iadone;
}

void edp_cascade_master_tx_handler(void)
{
    uint8_t *buf_ptr = g_edp_cascade_mstr_data_buf;
    if(g_master_flg == MASTER_TYPE)
    {
        if(g_cascade_tx_flg == 0)       
        {
            //P0_2 = 0;
            buf_ptr[0] = CASCADE_DATA_ID_MSTR_EDP_STATUS;          // edp cascade master id 
            edp_cascade_update_data(buf_ptr);
            tcon_tx_cascade(buf_ptr);
            //P0_2 = 1;
        }
    }
}

void edp_cascade_master_rx_handler(void)
{
    if((g_rx_cscd.id != CASCADE_DATA_ID_SLV_SEND_EDP_STATUS) && (g_rx_cscd.id != CASCADE_DATA_ID_SLV_EDP_STATUS))
        return;
    
    // add content
}

void edp_cascade_slave_tx_handler(void)
{
    uint8_t *buf_ptr = g_edp_cascade_slv_data_buf;
    buf_ptr[0] = CASCADE_DATA_ID_SLV_SEND_EDP_STATUS;

    edp_cascade_update_data(buf_ptr);
    tcon_tx_cascade(buf_ptr);
}

void edp_cascade_slave_rx_handler(void)
{
    if(g_rx_cscd.id != CASCADE_DATA_ID_MSTR_EDP_STATUS)
        return;

    edp_cascade_slave_tx_handler();
}

void edp_cascade_rx_handler(void)   // call in main loop
{
    if(g_master_flg == MASTER_TYPE)
    edp_cascade_master_rx_handler();
    else
    edp_cascade_slave_rx_handler();
}
#endif

#ifdef EDP1_4_FUN
void edp_intel_ubrr_led_handler(bool ubrr_entry)
{
    uint8_t xdata dev_addr, mem_addr_len, data_byte_len, l_two_pin_sel;
    uint8_t* xdata mem_addr_ptr;
    uint8_t* xdata data_ptr;
    if(g_rw_sys_008Ch.bits.r_ubrr_gpio_en)
    {
        if(g_rw_sys_008Ch.bits.r_ubrr_gpio_st)
            UBRR_GPIO_OUT = ubrr_entry;   // high in ubrr mode / low in normal mode
        else
            UBRR_GPIO_OUT = ~ubrr_entry;  // low in ubrr mode / high in normal mode
    }
    if(g_rw_sys_008Ch.bits.r_ubrr_i2c1_en)
    {
        dev_addr = g_rw_sys_008Eh_ubrr_i2c1_dev_id;
        mem_addr_ptr = &g_rw_sys_008Fh_ubrr_i2c1_mem_addr;
        mem_addr_len = g_rw_sys_008Ch.bits.r_ubrr_i2c1_mem_adr_byte_len;
        data_byte_len = g_rw_sys_008Dh.bits.r_ubrr_i2c1_mem_dat_byte_len;
        if(ubrr_entry)
            data_ptr = &g_rw_sys_0091h_ubrr_i2c1_mem_enter_dat0;
        else
            data_ptr = &g_rw_sys_0099h_ubrr_i2c1_mem_exit_dat0;
   
        l_two_pin_sel = g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl;
        g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 1;
        mcu_i2c_mst_write(dev_addr, mem_addr_ptr, mem_addr_len, data_ptr, data_byte_len, 0, 20);
        g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = l_two_pin_sel;
    }
    if(g_rw_sys_008Ch.bits.r_ubrr_i2c2_en)
    {
        dev_addr = g_rw_sys_00A1h_ubrr_i2c2_dev_id;
        mem_addr_ptr = &g_rw_sys_00A2h_ubrr_i2c2_mem_addr;
        mem_addr_len = g_rw_sys_008Ch.bits.r_ubrr_i2c2_mem_adr_byte_len;  //check
        data_byte_len = g_rw_sys_008Dh.bits.r_ubrr_i2c2_mem_dat_byte_len;
        
        l_two_pin_sel = g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl;
        g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 1;
        mcu_i2c_mst_write(dev_addr, mem_addr_ptr, mem_addr_len, data_ptr, data_byte_len, 0, 20);
        g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = l_two_pin_sel;
    }

}

void edp_intel_ubrr_pwrproc_handler(uint8_t ubrr_entry)
{
    if(ubrr_entry)
    {
        g_rw_cabc_0087h.bits.r_rdp_bl_en = 0;   // power down backlight
        edp_intel_ubrr_led_handler(1);
    }
    else 
    {
        g_rw_cabc_0087h.bits.r_rdp_bl_en = 1;   // power on backlight
        edp_intel_ubrr_led_handler(0);
    }
}

void edp_intel_ubrr_vsync_handler(void)
{
    if(g_intel_ubrr_vsync_cnt == 0)
        return;
    
    if(g_intel_ubrr_vsync_cnt == 3)
    {
        edp_intel_ubrr_pwrproc_handler(g_intel_ubrr_entry);
        g_intel_ubrr_vsync_cnt = 0;
    }
    else 
        g_intel_ubrr_vsync_cnt++;

}

void edp_intel_ubrr_irq_handler(void)
{
    if(g_intel_ubrr_irq_flag)
    {
        g_intel_ubrr_vsync_cnt = 1;   //  = 1 start to calculate vsync
        if((g_rw_irq_read_0010h.bits.irq_intel_ubrr_st == 1) && (g_intel_ubrr_status_old & 0x01 == 0))
        {
            g_intel_ubrr_entry = 1;
        }
        else if((g_intel_ubrr_status_old == 0x01) && (g_rw_irq_read_0010h.bits.irq_intel_ubrr_st & 0x01 == 0))
        {
            g_intel_ubrr_entry = 0;
        }

        g_intel_ubrr_status_old = g_rw_irq_read_0010h.bits.irq_intel_ubrr_st;
        g_intel_ubrr_irq_flag = 0;
    }
}
void edp_store_psr_capability(void)
{
    uint8_t i = 0;
    uint8_t *ptr = &g_rw_dpcd_reg_002Dh_PSR_Support_and_Version;

    for(i = 0; i < 5; i++)
        g_psr_cap_temp[i] = *(ptr + i);

}

void edp_check_psr_capability_handler(void)
{
    bool change_flag = 0;
    uint8_t i = 0;
    uint8_t *ptr = &g_rw_dpcd_reg_002Dh_PSR_Support_and_Version;

    for(i = 0; i < 5; i++)
    {
        if(*(ptr + i) != g_psr_cap_temp[i]) // change
        {
            change_flag = 1;
        }
        g_psr_cap_temp[i] = *(ptr + i);
    } 

    if(change_flag)
    {
        g_rw_psr_000Bh.bits.r_fw_requested_psr_capability_change = 1;
        g_rw_psr_000Bh.bits.r_fw_requested_psr_capability_change = 0;
    }
}

void edp_amd_hdr_handler(void)
{
    static uint8_t dbg_mux_480h_old = 0;
    if((g_rw_dbg_mux_0480h_hdr_dbgr_0 & 0x02) != (dbg_mux_480h_old & 0x02))
    {
        if(g_rw_dbg_mux_0480h_hdr_dbgr_0 & 0x02)
        {
            g_rw_tone_0002h.bits.r_hdrgm_disable = 1;
            g_rw_hdrgm_00C5h.bits.r_hdr_gm_cs = 0;
        }
        else
            g_rw_tone_0002h.bits.r_hdrgm_disable = 0;
    }

    if((g_rw_dbg_mux_0480h_hdr_dbgr_0 & 0x01) != (dbg_mux_480h_old & 0x01))
    {
        if(g_rw_dbg_mux_0480h_hdr_dbgr_0 & 0x01)
        {
            g_rw_tone_0002h.bits.r_tone_mapping_en = 0;
            g_rw_tone_0002h.bits.r_eotf_trans_en = 0;
            g_rw_tone_0002h.bits.r_tone_mapping_mode = 0;
        }
        else
        {
            g_rw_tone_0002h.bits.r_tone_mapping_en = 1;
            g_rw_tone_0002h.bits.r_eotf_trans_en = 1;
            g_rw_tone_0002h.bits.r_tone_mapping_mode = 2;
        }
    }

    dbg_mux_480h_old = g_rw_dbg_mux_0480h_hdr_dbgr_0;
}
#endif          // for EDP1_4_FUN

#endif