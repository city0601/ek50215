#include "mcu_fun_def.h"

#ifdef EDP_FUN
#include "edp_dpcd_rd.h"
#include "edp_misc.h"
#include "reg_include.h"

/************************************/
//     local function               //
/************************************/

/******************************* 0x0000~0x000F *******************************************/
uint8_t edp_dpcd_read_0003(void)
{
    return  g_rw_dpcd_reg_000Ch.bits.r_TPS4_SUPPORTED << 7 | g_rw_dpcd_reg_000Ch.bits.r_NO_AUX_TRANSACTION_LINK_TRAINING << 6 |
            g_rw_dpcd_reg_000Ch.bits.r_STREAM_REGENERATION_STATUS_CAPABILITY << 1 | g_rw_dpcd_reg_000Ch.bits.r_MAX_DOWNSPREAD;
}

uint8_t edp_dpcd_read_0004(void)
{
    return g_rw_dpcd_reg_000Dh.bits.r_18V_DP_PWR_CAP << 7 | g_rw_dpcd_reg_000Ch.bits.r_12V_DP_PWR_CAP << 6 |
           g_rw_dpcd_reg_000Ch.bits.r_5V_DP_PWR_CAP << 5 | g_rw_dpcd_reg_000Ch.bits.r_CRC_3D_OPTIONS_SUPPORTED << 1 | 
           g_rw_dpcd_reg_000Ch.bits.r_NORP;
}

uint8_t edp_dpcd_read_0005(void)
{
    return g_rw_dpcd_reg_000Dh.bits.r_DETAILED_CAP_INFO_AVAILABLE << 4 | g_rw_dpcd_reg_000Dh.bits.r_FORMAT_CONVERSION << 3 |
           g_rw_dpcd_reg_000Dh.bits.r_DFP_TYPE << 1 | g_rw_dpcd_reg_000Dh.bits.r_DFP_PRESENT;
}

uint8_t edp_dpcd_read_0006(void)
{
    return g_rw_dpcd_reg_000Dh.bits.r_128b132b_SUPPORTED << 1 | g_rw_dpcd_reg_000Dh.bits.r_8b10b_SUPPORTED;
}

/************************************/
uint8_t edp_dpcd_read_0000_000F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_dpcd_reg_000Eh.byte;
    switch (addr_ll)
    {
        case 0x00:
            return g_rw_dpcd_reg_0009h_DPCD_REV;
        case 0x01:
            return g_rw_dpcd_reg_000Ah_8b10b_MAX_LINK_RATE;
        case 0x02:
            return g_rw_dpcd_reg_000Bh.byte;
        case 0x03:
            return edp_dpcd_read_0003();
        case 0x04:
            return edp_dpcd_read_0004();
        case 0x05:
            return edp_dpcd_read_0005();
        case 0x06:
            return edp_dpcd_read_0006(); 

        case 0x07:
        case 0x08:
        case 0x09:
        case 0x0A:
        case 0x0B:
        case 0x0C:
        case 0x0D:
        case 0x0E:
            return *(ptr + (addr_ll - 0x07));
 
        default:
        break;        
    }
    return 0x00;
}

/******************************* 0x0010~0x001F *******************************************/
uint8_t edp_dpcd_read_0010_001F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_dpcd_reg_0016h_SUPPORTED_LINK_RATES_0;

    if(addr_ll < 0x08)
        return *(ptr + addr_ll);
    
    return 0x00;
}
/******************************* 0x0020~0x002F *******************************************/
uint8_t edp_dpcd_read_0021(void)
{
    return g_rw_dpcd_reg_001Fh.bits.r_SINGLE_STREAM_SIDEBAND_MSG_SUPPORT << 1  |  g_rw_dpcd_reg_001Fh.bits.r_MST_CAP;
}

/*uint8_t edp_dpcd_read_002E(void)
{
    return g_rw_dpcd_reg_001Fh.bits.r_AUX_LESS_ALPM_CAP << 2 | g_rw_dpcd_reg_001Fh.bits.r_PM_State_2a_Support << 1 |
           g_rw_dpcd_reg_001Fh.bits.r_AUX_WAKE_ALPM_CAP;
}*/

uint8_t edp_dpcd_read_0020_002F(uint8_t addr_ll)
{
    switch (addr_ll)
    {
        case 0x00:  
            return g_rw_dpcd_reg_001Eh_SINK_VIDEO_FALLBACK_FORMATS;
        case 0x01:
            return edp_dpcd_read_0021();  
#ifdef EDP1_4_FUN
        case 0x0E:
            return edp_dpcd_read_002E();
        case 0x0F:
            return (uint8_t)g_rw_dpcd_reg_001Fh.bits.r_AUX_FRAME_SYNC_CAP;
#endif
        default:
        break;       
    }
    return 0x00;
}
/******************************* 0x0050~0x005F *******************************************/
uint8_t edp_dpcd_read_0058(void)
{
    return g_rw_dpcd_reg_001Fh.bits.r_TX_GTC_VALUE_PHASE_SKEW_EN << 1 |  g_rw_dpcd_reg_001Fh.bits.r_RX_GTC_MSTR_REQ;
}

uint8_t edp_dpcd_read_0050_005F(uint8_t addr_ll)
{
	  uint8_t *ptr1 = &g_rw_dpcd_read_0000h_RX_GTC_VALUE;
	  uint8_t *ptr2 = &g_rw_dpcd_read_0005h_RX_GTC_PHASE_SKEW_OFFSET;
	
    switch (addr_ll)
    {
        case 0x04:
        case 0x05:
        case 0x06:
        case 0x07:
            return *(ptr1 + (addr_ll - 0x04));   

        case 0x08:
            return edp_dpcd_read_0058();  
        case 0x09:
            return g_rw_dpcd_read_0004h.byte;

        case 0x0A:
        case 0x0B:
            return *(ptr2 + (addr_ll - 0x0A)); 
    }
    
    return 0x00;
}
/******************************* 0x0060~0x006F *******************************************/
uint8_t edp_dpcd_read_0060(void)
{
    return g_rw_dpcd_reg_0020h.bits.r_Dynamic_PPS_Update_Support_Uncompressed_to_from_Compressed << 3 | g_rw_dpcd_reg_0020h.bits.r_Dynamic_PPS_Update_Support_Compressed_to_Compressed << 2 |
           g_rw_dpcd_reg_0020h.bits.r_DSC_Pass_through_Support << 1 | g_rw_dpcd_reg_0020h.bits.r_DSC_SUPPORT;
}

uint8_t edp_dpcd_read_0066(void)
{
    return g_rw_dpcd_reg_0024h.bits.r_RGB_Color_Conversion_Bypass_Support << 1 | g_rw_dpcd_reg_0024h.bits.r_DSC_Block_Prediction_Support;
}

uint8_t edp_dpcd_read_0068(void)
{
    return g_rw_dpcd_reg_0026h.bits.r_MAX_BPP_DELTA_AVAILABILITY << 7 | g_rw_dpcd_reg_0026h.bits.r_MAX_BPP_DELTA_VERSION << 5 | 
           g_rw_dpcd_reg_0026h.bits.r_bits_per_pixel_bits_9_8;
}

uint8_t edp_dpcd_read_0069(void)
{
    return g_rw_dpcd_reg_0027h.bits.r_YCbCr_native_420_Support << 4 | g_rw_dpcd_reg_0027h.bits.r_YCbCr_native_422_Support << 3 |
           g_rw_dpcd_reg_0027h.bits.r_YCbCr_simple_422_Support << 2 | g_rw_dpcd_reg_0027h.bits.r_YCbCr_444_Support << 1 | 
           g_rw_dpcd_reg_0027h.bits.r_RGB_Support ;
}
uint8_t edp_dpcd_read_006A(void)
{
    return g_rw_dpcd_reg_0027h.bits.r_dsc_12_Bits_component_Support << 3 | g_rw_dpcd_reg_0027h.bits.r_dsc_10_Bits_component_Support << 2 |
           g_rw_dpcd_reg_0027h.bits.r_dsc_8_Bits_component_Support << 1;
}
uint8_t edp_dpcd_read_006F(void)
{
    return g_rw_dpcd_reg_002Ch.bits.r_NativeYCbCr422_MAX_BPP_DELTA << 3 | g_rw_dpcd_reg_002Ch.bits.r_INCREMENT_OF_bits_per_pixel_SUPPORTED_BY_THE_DECOMPRESSOR;
}

uint8_t edp_dpcd_read_0060_006F(uint8_t addr_ll)
{
    switch (addr_ll)
    {
        case 0x00:
            return edp_dpcd_read_0060();    
        case 0x01:
            return g_rw_dpcd_reg_0021h.byte;
 
        case 0x02:
            return g_rw_dpcd_reg_0020h.bits.r_DSC_RC_BUFFER_BLOCK_SIZE;
        case 0x03:
            return g_rw_dpcd_reg_0022h_DSC_RC_BUFFER_SIZE; 
        case 0x04:
            return g_rw_dpcd_reg_0023h.byte;

        case 0x05:
            return g_rw_dpcd_reg_0024h.bits.r_Line_Buffer_Bit_Depth;   // bit[3:0]
        case 0x06:
            return edp_dpcd_read_0066(); 
        case 0x07:
            return g_rw_dpcd_reg_0025h_bits_per_pixel; 
        case 0x08:
            return edp_dpcd_read_0068(); 
        case 0x09:
            return edp_dpcd_read_0069(); 
        case 0x0A:
            return edp_dpcd_read_006A(); 
        case 0x0B:
            return g_rw_dpcd_reg_0028h.byte; 
        case 0x0C:
            return g_rw_dpcd_reg_0029h_DSC_Maximum_Slice_Width; 
        case 0x0D:
            return g_rw_dpcd_reg_002Ah_DSC_SLICE_CAPABILITIES_2; 
        case 0x0E:
            return g_rw_dpcd_reg_002Bh.byte; 
        case 0x0F:
            return edp_dpcd_read_006F(); 
        default:
            break;
    }
    return 0x00;
}
/******************************* 0x0070~0x007F *******************************************/
uint8_t edp_dpcd_read_0070_007F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_dpcd_reg_002Dh_PSR_Support_and_Version;

    if(addr_ll < 0x05)
    {
        return *(ptr + addr_ll);
    }
    
    return 0x00;
}
/******************************* 0x0090~0x009F *******************************************/
uint8_t edp_dpcd_read_0090_009F(uint8_t addr_ll)
{
    switch (addr_ll)
    {
        case 0x00:
            return g_rw_dpcd_reg_0032h.byte;
#ifdef EDP1_4_FUN
        case 0x01:
            return g_rw_dpcd_reg_0033h.bits.r_AGGREGATED_ERROR_COUNTERS_CAPABLE;
#endif
        default:
            break;
    }   
    return 0x00;
}
/******************************* 0x00B0~0x00BF *******************************************/
uint8_t edp_dpcd_read_00B0(void)
{
    return  g_rw_dpcd_reg_0033h.bits.r_PR_Early_Transport_Support << 2 |  g_rw_dpcd_reg_0033h.bits.r_PR_Selective_Update_Support << 1 |
            g_rw_dpcd_reg_0033h.bits.r_Panel_Replay_Support;
}
uint8_t edp_dpcd_read_00B1(void)
{
    return g_rw_dpcd_reg_0033h.bits.r_PR_SU_Y_Granularity_Extended_Capability_Supported << 6 | g_rw_dpcd_reg_0033h.bits.r_PR_SU_Granularity_Needed << 5;
}

uint8_t edp_dpcd_read_00B0_00BF(uint8_t addr_ll)
{
    switch (addr_ll)
    {
        case 0x00:
            return edp_dpcd_read_00B0();
        case 0x01:
            return edp_dpcd_read_00B1();
        case 0x02:
            return g_rw_dpcd_reg_0034h_PR_SU_X_GRANULARITY;
        case 0x03:
            return g_rw_dpcd_reg_0035h_PR_SU_X_GRANULARITY;
        case 0x04:
            return g_rw_dpcd_reg_0036h_PR_SU_Y_GRANULARITY;
        case 0x05:
            return g_rw_dpcd_reg_0037h_PR_SU_Y_Granularity_Extended_Capability_0; 
        case 0x06:
            return g_rw_dpcd_reg_0038h_PR_SU_Y_Granularity_Extended_Capability_1;
        case 0x08:
            return g_rw_dpcd_reg_0039h.bits.r_ARP_SUPPORTED;

        default:
            break;
    }

    return 0x00;
}
//----------------------------------------------------------------------------------------- 
// read 0x0000~ 0x00FF 
//-----------------------------------------------------------------------------------------
uint8_t edp_dpcd_read_00xxh(uint8_t addr_l)
{
    uint8_t addr_lh, addr_ll;

    addr_lh = addr_l & 0xF0;
    addr_ll = addr_l & 0x0F;

    switch (addr_lh)
    {
        case 0x00:
            return edp_dpcd_read_0000_000F(addr_ll);
        case 0x10:
            #ifdef EDP1_4_FUN
            return edp_dpcd_read_0010_001F(addr_ll);
            #else
            return 0x00;
            #endif
        case 0x20:
            return edp_dpcd_read_0020_002F(addr_ll);
        case 0x50:
            return edp_dpcd_read_0050_005F(addr_ll);
        case 0x60:
            return edp_dpcd_read_0060_006F(addr_ll);
        case 0x70:
            #ifdef EDP1_4_FUN
            return edp_dpcd_read_0070_007F(addr_ll);
            #else
            return 0x00;
            #endif
        case 0x90:
            return edp_dpcd_read_0090_009F(addr_ll);
#ifdef EDP1_4_FUN
        case 0xB0:
            return edp_dpcd_read_00B0_00BF(addr_ll);
#endif
        
        default:
        break;
    }

    return 0;
}

/******************************* 0x0100~0x010F *******************************************/
/*uint8_t edp_dpcd_read_0101(void)
{
    return g_rw_dpcd_reg_003Ch.bits.r_ENHANCED_FRAME_EN << 7 | g_rw_dpcd_reg_003Ch.bits.r_POST_LT_ADJ_REQ_GRANTED << 5 | 
           g_rw_dpcd_reg_003Ch.bits.r_LANE_COUNT_SET;
}*/

/*uint8_t edp_dpcd_read_0102_0106(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_dpcd_reg_003Dh.byte;
    return *(ptr + addr_ll);
}*/

uint8_t edp_dpcd_read_0107(void)
{
    return g_rw_dpcd_reg_0042h.bits.r_MSA_TIMING_PAR_IGNORE_EN << 7 | g_rw_dpcd_reg_0042h.bits.r_ADAPTIVE_SYNC_SDP_EN << 6 | 
           g_rw_dpcd_reg_0042h.bits.r_SPREAD_AMP << 4; 
}

uint8_t edp_dpcd_read_010A(void)
{
    return g_rw_dpcd_reg_0042h.bits.r_PANEL_SELF_TEST_ENABLE << 7 | g_rw_dpcd_reg_0042h.bits.r_FRAMING_CHANGE_ENABLE << 1 | 
           g_rw_dpcd_reg_0042h.bits.r_ASSR;
}

/************************************/
uint8_t edp_dpcd_read_0100_010F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_dpcd_reg_003Ch.byte;

    switch (addr_ll)
    {
        case 0x00:
            return g_rw_dpcd_reg_003Bh_LINK_BW_SET;

        case 0x01:
            //return edp_dpcd_read_0101();
        case 0x02:
        case 0x03:
        case 0x04:
        case 0x05:
        case 0x06:
            return *(ptr + (addr_ll - 0x01));
        case 0x07:
            return edp_dpcd_read_0107();
        			
        case 0x08:          // DPCD0 0x0108	         
            return g_rw_dpcd_reg_0042h.bits.r_8B10B_LINK_LAYER;
				
        case 0x09:          // DPCD0 0x0109	
            return g_rw_dpcd_reg_0043h_I2C_SPEED_CTRL;
        case 0x0A:
            return edp_dpcd_read_010A();           
        case 0x0B:
            return g_rw_dpcd_reg_0044h.bits.r_LINK_QUAL_LANE0_SET;
        case 0x0C:
            return g_rw_dpcd_reg_0044h.bits.r_LINK_QUAL_LANE1_SET;
        case 0x0D:    
            return g_rw_dpcd_reg_0045h.bits.r_LINK_QUAL_LANE2_SET;
        case 0x0E:
            return g_rw_dpcd_reg_0045h.bits.r_LINK_QUAL_LANE3_SET;;
        default:  
        break;    
    }
    return 0x00;
}
/******************************* 0x0110~0x011F *******************************************/
uint8_t edp_dpcd_read_0115(void)
{
    return g_rw_dpcd_reg_0046h.bits.r_TX_GTC_SLAVE_CAP << 4 | g_rw_dpcd_reg_0046h.bits.r_TX_GTC_CAP << 3 | 
           g_rw_dpcd_reg_0046h.bits.r_LINK_RATE_SET;
}

uint8_t edp_dpcd_read_0116(void)
{
    return  g_rw_dpcd_reg_0047h.bits.r_Period_of_CDS_Phase << 3 |  g_rw_dpcd_reg_0046h.bits.r_ALPM_Mode_Selected << 2 |  
            g_rw_dpcd_reg_0046h.bits.r_ALPM_Lock_Error_IRQ_HPD_Enable << 1 | g_rw_dpcd_reg_0046h.bits.r_ALPM_Enable ;
}
uint8_t edp_dpcd_read_0117(void)
{
    return g_rw_dpcd_reg_0047h.bits.r_IRQ_HPD_Enable << 1 | g_rw_dpcd_reg_0047h.bits.r_AUX_FRAME_SYNC_Enable;
}

uint8_t edp_dpcd_read_0110_011F(uint8_t addr_ll)
{
    switch (addr_ll)
    {
        case 0x05:
            return edp_dpcd_read_0115();
#ifdef EDP1_4_FUN
        case 0x06:
            return edp_dpcd_read_0116();
        case 0x07:
            return edp_dpcd_read_0117();
#endif
        case 0x08:
            return g_rw_dpcd_reg_0047h.bits.r_DP_PWR_NOT_NEEDED_BY_UPSTREAM_DEVICE;
        case 0x09:
            return g_rw_dpcd_reg_0047h.bits.r_DPRX_SLEEP_WAKE_TIMEOUT_PERIOD_GRANTED;
#ifdef EDP1_4_FUN
        case 0x0A:
            return g_rw_dpcd_reg_0047h.bits.r_AS_SDP_SETUP_CONFIG_PR_ACTIVE << 6;
        case 0x0B:
            return g_rw_dpcd_reg_0047h.bits.r_AS_SDP_ONE_LINE_EARLIER_ENABLE << 7;
#endif
        default:
            break;
    }
    return 0x00;
}
/******************************* 0x0150~0x015F *******************************************/
#ifdef EDP1_4_FUN
uint8_t edp_dpcd_read_015C_015F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_dpcd_reg_004Eh_TX_VBE_PRIMARY_VALUE;
    return *(ptr + (addr_ll - 0x0C));
}
#endif

uint8_t edp_dpcd_read_0150_015F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_dpcd_reg_0048h_TX_GTC_VALUE;
    switch (addr_ll)
    {
        case 0x04:   // dpcd 0x154h
            //return g_rw_dpcd_reg_0048h_TX_GTC_VALUE;
        case 0x05:
            //return g_rw_dpcd_reg_0049h_TX_GTC_VALUE;
        case 0x06:
            //return g_rw_dpcd_reg_004Ah_TX_GTC_VALUE;
        case 0x07:
            //return g_rw_dpcd_reg_004Bh_TX_GTC_VALUE;
            return *(ptr + (addr_ll - 0x04));
        case 0x08:
            return g_rw_dpcd_reg_0052h.bits.r_RX_GTC_VALUE_PHASE_SKEW_EN;
        case 0x09:
            return g_rw_dpcd_reg_0052h.bits.r_TX_GTC_FREQ_LOCK_DONE;

        case 0x0A:
            return g_rw_dpcd_reg_004Ch_TX_GTC_PHASE_SKEW_OFFSET;
        case 0x0B:
            return g_rw_dpcd_reg_004Dh_TX_GTC_PHASE_SKEW_OFFSET;
#ifdef EDP1_4_FUN
        case 0x0C:
        case 0x0D:
        case 0x0E:
        case 0x0F:
            return edp_dpcd_read_015C_015F(addr_ll);
#endif
        default:
            break;
    }
		return 0x00;
}
/******************************* 0x0160~0x016F *******************************************/
uint8_t edp_dpcd_read_0160(void)
{
    return g_rw_dpcd_reg_0052h.bits.r_DSC_Passthrough_Enable << 1 | g_rw_dpcd_reg_0052h.bits.r_DSC_ENABLE ;
}

uint8_t edp_dpcd_read_0160_016F(uint8_t addr_ll)
{
    switch (addr_ll)
    {
        case 0x00:
            return edp_dpcd_read_0160();
        default:
            break;
    }
    return 0x00;
}
/******************************* 0x0170~0x017F *******************************************/
/*uint8_t edp_dpcd_read_0170_017F(uint8_t addr_ll)
{
    switch (addr_ll)
    {
        case 0x00:   // 0x0170h
            return g_rw_dpcd_reg_0053h.byte;
        default:
            break;
    }
    return 0x00;
}*/
/******************************* 0x01B0~0x01BF *******************************************/

uint8_t edp_dpcd_read_01B0_01BF(uint8_t addr_ll)
{
    switch (addr_ll)
    {
        case 0x00:   // 0x01B0h
            return g_rw_dpcd_reg_0054h.byte;
        case 0x01:   // 0x01B1h
            return g_rw_dpcd_reg_0055h.byte;
        case 0x08:
            return g_rw_dpcd_reg_0058h.byte;
        case 0x09:
            return g_rw_dpcd_reg_0056h_ARP_t2_MAX;
        case 0x0A:
            return g_rw_dpcd_reg_0057h_ARP_t2_MAX;
        default:
            break;
    }
    return 0x00;
}
//----------------------------------------------------------------------------------------- 
// read 0x0100~ 0x01FF 
//----------------------------------------------------------------------------------------- 
uint8_t edp_dpcd_read_01xxh(uint8_t addr_l)
{
    uint8_t addr_lh, addr_ll;
    addr_lh = addr_l & 0xF0;
    addr_ll = addr_l & 0x0F;

    switch (addr_lh)
    {
        case 0x00:
            return edp_dpcd_read_0100_010F(addr_ll);
        
        case 0x10:
            return edp_dpcd_read_0110_011F(addr_ll);

        case 0x50:
            return edp_dpcd_read_0150_015F(addr_ll);
        // for DSC
        case 0x60:
            return edp_dpcd_read_0160_016F(addr_ll);
#ifdef EDP1_4_FUN
        case 0x70:
            if(addr_ll == 0x00)     // 0x0170
                return g_rw_dpcd_reg_0053h.byte;
            else
                return 0x00;
        case 0xB0:
            return edp_dpcd_read_01B0_01BF(addr_ll);
#endif
        default:
            break;
    }
    return 0;
}
/******************************* 0x0200~0x020F *******************************************/
uint8_t edp_dpcd_read_0202h_lane0_1_status(void)
{
    uint8_t sta = 0x04;
    if(g_rw_dpcd_reg_0007h_edp_state == EDP_STATE_NO_TRN)
    {
        if( g_rw_dpcd_reg_003Ch.bits.r_LANE_COUNT_SET >= 2)
            sta = 0x44;
        if((g_rw_dpcd_read_0009h.byte & sta) != sta)
            return 0;
    }
    return g_rw_dpcd_read_0009h.byte;
}

uint8_t edp_dpcd_read_0203h_lane2_3_status(void)
{
    uint8_t sta = 0x04;
    if( g_rw_dpcd_reg_003Ch.bits.r_LANE_COUNT_SET < 3)
        return 0;

    if(g_rw_dpcd_reg_0007h_edp_state == EDP_STATE_NO_TRN)
    {
        if(g_rw_dpcd_reg_003Ch.bits.r_LANE_COUNT_SET == 4)
            sta = 0x44;
        if((g_rw_dpcd_read_000Ah.byte & sta) != sta)
            return 0;
    }
    return g_rw_dpcd_read_000Ah.byte;
}

uint8_t edp_dpcd_read_0204h_iad_and_status_upd(void)
{
    //unsigned char val = g_rw_dpcd_read_000Bh.byte;
	  unsigned char val = (g_rw_dp_009Dh.bits.r_fw_link_status_update << 7) | (g_rw_dpcd_read_000Bh.byte & 0x7F);
	
    //g_rw_dpcd_read_000Bh.bits.r_LINK_STATUS_UPDATED = 0; // RC
	  g_rw_dp_009Dh.bits.r_fw_link_status_update = 0;      // RC / modified 230606 / g_rw_dpcd_read_000Bh is read only

    if(g_rw_dp_0093h.bits.swing_en)   
        return ((g_rw_dpcd_reg_005Ah.byte >= g_rw_dp_0091h.byte)? 0x01: val);   // check DPCD 0x206h
    else
        return val;       
}

uint8_t edp_dpcd_read_0204h(void)
{
    //unsigned char val = g_rw_dpcd_read_000Bh.byte & 0x80;
	  unsigned char val = g_rw_dp_009Dh.bits.r_fw_link_status_update << 7;
	
    if(g_rw_dpcd_reg_0007h_edp_state == EDP_STATE_NO_TRN)
    {
        //g_rw_dpcd_read_000Bh.bits.r_LINK_STATUS_UPDATED = 0; // RC
			  g_rw_dp_009Dh.bits.r_fw_link_status_update = 0;      // RC / modified 230606 / g_rw_dpcd_read_000Bh is read only
        return (val);
    }
    else
        return edp_dpcd_read_0204h_iad_and_status_upd();
}

uint8_t edp_dpcd_read_0205h_port_0_status(void)
{
    if(g_rw_dpcd_read_000Ch.bits.r_RECEIVE_PORT_0_STATUS == 0)
        return 0;
    //if(g_rw_irq_0020h.bits.irq_inproc_st == 0x02)     // nead modify
    if(g_rw_irq_read_000Dh.bits.irq_corestate == 0x02)
        return 1;
    return 0;
}

/*uint8_t edp_dpcd_read_0205h_port_1_status(void)
{
    if(g_rw_dpcd_read_000Ch.bits.r_RECEIVE_PORT_1_STATUS == 0)
        return 0;
    //if(g_rw_irq_0020h.bits.irq_inproc_st == 0x02)     // nead modify
    if(g_rw_irq_read_000Dh.bits.irq_corestate == 0x02)
        return 1;
    return 0;
}

uint8_t edp_dpcd_read_0205h_port_status(void)
{
    uint8_t port0_sta = 0; 
	uint8_t port1_sta = 0; 
    
    port0_sta = (g_rw_aip_reg_0075h.byte & 0x0F)?(edp_dpcd_read_0205h_port_0_status()):(0);
    port1_sta = (g_rw_aip_reg_0075h.byte & 0xF0)?(edp_dpcd_read_0205h_port_1_status()):(0);

    return (uint8_t)((port1_sta << 1) | port0_sta);
}*/

static uint8_t edp_dpcd_swing_pre_adj_req(uint8_t cr_sl, uint8_t swg, uint8_t pre)  // 0x202[3:0] , 0x103[2:0] , 0x103[5:3] or 0x202[7:4] , 0x104[2:0] , 0x104[5:3]
{
    uint8_t sum, tgt;
    uint8_t swg_max, pre_max;

    swg_max = swg & 0x04;
    pre_max = pre & 0x04;
    swg &= 0x03;
    pre &= 0x03;

    sum = swg + pre;
    if ((cr_sl != 0x07) && (sum < 3)) {
        if (swg_max == 0)
            swg++;
        else if (pre_max == 0)
            pre++;
    }
    tgt = (pre << 2) + swg;

    return tgt;
}

uint8_t edp_dpcd_read_0206h_adj_req_lane0_1(void)
{
    if(g_rw_dpcd_reg_003Dh.bits.r_TRAIN_PAT_SET == 0)   
        return 0;

    g_rw_dpcd_reg_005Ah.byte = edp_dpcd_swing_pre_adj_req(g_rw_dpcd_read_0009h.bits.r_LANE0_STATUS, g_rw_dpcd_reg_003Eh.bits.r_TRAIN_LANE0_VOLTAGE_SWING_SET, g_rw_dpcd_reg_003Eh.bits.r_TRAIN_LANE0_PREEM_SET);

    if (g_rw_dpcd_reg_003Ch.bits.r_LANE_COUNT_SET >= 2) 
    {
        g_rw_dpcd_reg_005Ah.byte += (edp_dpcd_swing_pre_adj_req(g_rw_dpcd_read_0009h.bits.r_LANE1_STATUS, g_rw_dpcd_reg_003Fh.bits.r_TRAIN_LANE1_VOLTAGE_SWING_SET, g_rw_dpcd_reg_003Fh.bits.r_TRAIN_LANE1_PREEM_SET) << 4 );
    }

    return g_rw_dpcd_reg_005Ah.byte;
}

uint8_t edp_dpcd_read_0207h_adj_req_lane2_3(void)
{ 
    uint8_t ln_cnt;
    
    ln_cnt = g_rw_dpcd_reg_003Ch.bits.r_LANE_COUNT_SET;
    
    if(g_rw_dpcd_reg_003Dh.bits.r_TRAIN_PAT_SET == 0)   
        return 0;

    if (ln_cnt >= 4) 
    {
        g_rw_dpcd_reg_005Bh.byte = edp_dpcd_swing_pre_adj_req(g_rw_dpcd_read_000Ah.bits.r_LANE2_STATUS, g_rw_dpcd_reg_0040h.bits.r_TRAIN_LANE2_VOLTAGE_SWING_SET, g_rw_dpcd_reg_0040h.bits.r_TRAIN_LANE2_PREEM_SET);
        g_rw_dpcd_reg_005Bh.byte += (edp_dpcd_swing_pre_adj_req(g_rw_dpcd_read_000Ah.bits.r_LANE3_STATUS, g_rw_dpcd_reg_0041h.bits.r_TRAIN_LANE3_VOLTAGE_SWING_SET, g_rw_dpcd_reg_0041h.bits.r_TRAIN_LANE3_PREEM_SET) << 4);
    } 
    else {
        g_rw_dpcd_reg_005Bh.byte = 0;
    }

    return g_rw_dpcd_reg_005Bh.byte;
}

uint8_t edp_dpcd_read_020Ch(void)
{
    return g_rw_dpcd_reg_005Ch.bits.r_Link_Rate_Set_Status_Valid << 1 | g_rw_dpcd_reg_005Ch.bits.r_Link_Rate_Set_Status;
}

uint8_t edp_dpcd_read_020Fh(void)
{
    return g_rw_dpcd_reg_005Ch.bits.r_Chunk_Length_Error << 2 | g_rw_dpcd_reg_005Ch.bits.r_RC_Buffer_Overflow << 1 |
           g_rw_dpcd_reg_005Ch.bits.r_RC_Buffer_Under_run;
}

uint8_t edp_dpcd_read_0200_020F(uint8_t addr_ll)
{
    switch (addr_ll)
    {
        case 0x00:      // DPCD 0x0200h    
            return g_rw_dpcd_read_0007h_SINK_COUNT;
        case 0x01:      // DPCD 0x0201h   
            return g_rw_dpcd_read_0008h_DEVICE_SERVICE_IRQ_VECTOR;
        case 0x02:
            return edp_dpcd_read_0202h_lane0_1_status();
        case 0x03:
            return edp_dpcd_read_0203h_lane2_3_status();
        case 0x04:
            return edp_dpcd_read_0204h();
        case 0x05:
            return edp_dpcd_read_0205h_port_0_status();
        case 0x06:
            return edp_dpcd_read_0206h_adj_req_lane0_1();
        case 0x07:
            return edp_dpcd_read_0207h_adj_req_lane2_3();
        case 0x0C:
            return edp_dpcd_read_020Ch();
        case 0x0F:
            return edp_dpcd_read_020Fh();
        default:
            break;
    }
    return 0x00;
}

/******************************* 0x0210~0x021F *******************************************/
uint8_t edp_dpcd_read_0210h_0217h_err_cnt(uint8_t byte_idx)
{
    uint8_t err_cnt;
    // read error count sequentially => latch at first byte
    if(g_edp_flag[g_edp_port].err_cnt_latch_flg == 0)
        edp_dpcd_err_cnt_latch();   // Eric Add at 20201228

    g_edp_flag[g_edp_port].err_cnt_latch_flg = 1;
    
    err_cnt = (byte_idx == 0) ? (uint8_t)(g_rw_dpcd_read_000Dh_SYMBOL_ERROR_COUNT_LANE0):
              (byte_idx == 1) ? (uint8_t)(g_rw_dpcd_read_000Eh.byte):
              (byte_idx == 2) ? (uint8_t)(g_rw_dpcd_read_000Fh_SYMBOL_ERROR_COUNT_LANE1):
              (byte_idx == 3) ? (uint8_t)(g_rw_dpcd_read_0010h.byte):
              (byte_idx == 4) ? (uint8_t)(g_rw_dpcd_read_0011h_SYMBOL_ERROR_COUNT_LANE2):
              (byte_idx == 5) ? (uint8_t)(g_rw_dpcd_read_0012h.byte):
              (byte_idx == 6) ? (uint8_t)(g_rw_dpcd_read_0013h_SYMBOL_ERROR_COUNT_LANE3):
              (byte_idx == 7) ? (uint8_t)(g_rw_dpcd_read_0014h.byte):
              0;          
    return err_cnt;
}

uint8_t edp_dpcd_read_0210_021F(uint8_t addr_ll)
{
    if(addr_ll < 0x08)
        return edp_dpcd_read_0210h_0217h_err_cnt(addr_ll);
    else if(addr_ll == 0x08)     // DPCD 0x0218h
        return g_rw_dpcd_reg_005Dh.byte;
    else if(addr_ll == 0x09)      // DPCD 0x0219h
        return g_rw_dpcd_reg_005Eh_TEST_LINK_RATE;
    else 
        return 0x00;
}
/******************************* 0x0220~0x022F *******************************************/
uint8_t edp_dpcd_read_0220_022F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_dpcd_reg_005Fh_TEST_LANE_COUNT;   // ptr to mapping DPCD 0x0220
    return *(ptr + addr_ll);
}
/******************************* 0x0230~0x023F *******************************************/
/*uint8_t edp_dpcd_read_0233h(void)
{
    return g_rw_dpcd_reg_0072h.bits.r_TEST_INTERLACED << 1 | g_rw_dpcd_reg_0072h.bits.r_TEST_REFRESH_RATE_DENOMINATOR;
}*/
uint8_t edp_dpcd_read_0230_023F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_dpcd_reg_006Fh_TEST_V_HEIGHT0;
    if(addr_ll < 0x05)
        return *(ptr + addr_ll);
    else
        return 0x00;
    
    /*switch (addr_ll)
    {
        case 0x00:   // DPCD 0x0230
            return g_rw_dpcd_reg_006Fh_TEST_V_HEIGHT0;
        case 0x01:
            return g_rw_dpcd_reg_0070h_TEST_V_HEIGHT1;
        case 0x02:
            return g_rw_dpcd_reg_0071h.byte;
        case 0x03:
            return g_rw_dpcd_reg_0072h.byte;
        case 0x04:
            return g_rw_dpcd_reg_0073h_TEST_REFRESH_RATE_NUMERATOR;
        default:
            break;
    }
    return 0x00;*/
}

/******************************* 0x0240~0x024F *******************************************/
uint8_t edp_dpcd_read_0240h_0246h_test_crc(uint8_t byte_idx)
{    
    // 0x240 ~ 0x246
    uint8_t *ptr = &g_rw_dpcd_read_0015h_TEST_CRC_R_CR;
    return *(ptr + byte_idx); 
}

uint8_t edp_dpcd_read_0240_024F(uint8_t addr_ll)
{
    if(addr_ll < 0x07)
        return edp_dpcd_read_0240h_0246h_test_crc(addr_ll);
    else if(addr_ll == 0x08)
        return g_rw_dpcd_reg_0074h.byte;
    else if(addr_ll == 0x09)
        return g_rw_dpcd_reg_0075h_PHY_SQUARE_PATTERN_num_add_1;
    else if(addr_ll == 0x0A)
        return g_rw_dpcd_read_001Ch_HBR2_COMPLIANCE_SCRAMBLER_RESET_VALUE;
    else if(addr_ll == 0x0B)
        return g_rw_dpcd_read_001Dh_HBR2_COMPLIANCE_SCRAMBLER_RESET_VALUE;
    else
        return 0x00;
}

/******************************* 0x0250~0x025F *******************************************/
uint8_t edp_dpcd_read_0250h_0259h_test_cust_pattern(uint8_t byte_idx)
{   
    uint8_t *ptr = &g_rw_dpcd_reg_0076h_TEST_80BIT_CUSTOM_PATTERN;
    return *(ptr + byte_idx); 
}
/*uint8_t edp_dpcd_read_025Ah(void)
{
    return g_rw_dpcd_reg_0080h.bits.r_CONTINUOUS_80BIT_PATTERN_FROM_DPRX_AUX_CH_CAP | 0x00;
}
uint8_t edp_dpcd_read_025Bh(void)
{
    return g_rw_dpcd_reg_0080h.bits.r_DURATION_CTRL << 1 | g_rw_dpcd_reg_0080h.bits.r_CONTINUOUS_80BIT_PATTERN_FROM_DPRX_AUX_CH_EN;
}*/

uint8_t edp_dpcd_read_0250_025F(uint8_t addr_ll)
{
    if(addr_ll < 0x0A)
        return edp_dpcd_read_0250h_0259h_test_cust_pattern(addr_ll);
    else if(addr_ll == 0x0A)
        //eturn edp_dpcd_read_025Ah();
        return g_rw_dpcd_reg_0080h.bits.r_CONTINUOUS_80BIT_PATTERN_FROM_DPRX_AUX_CH_CAP;
    else if(addr_ll == 0x0B)
        //return edp_dpcd_read_025Bh();
        return g_rw_dpcd_reg_0080h.bits.r_DURATION_CTRL << 1 | g_rw_dpcd_reg_0080h.bits.r_CONTINUOUS_80BIT_PATTERN_FROM_DPRX_AUX_CH_EN;
    else 
        return 0x00;
}
/******************************* 0x0260~0x026F *******************************************/
uint8_t edp_dpcd_read_0260h(void)
{
    return g_rw_dpcd_reg_0080h.bits.r_TEST_EDID_CHECKSUM_WRITE << 2 | g_rw_dpcd_reg_0080h.bits.r_TEST_NAK << 1 |
           g_rw_dpcd_reg_0080h.bits.r_TEST_ACK;
}

uint8_t edp_dpcd_read_0262h_0267h_dsc_crc(uint8_t byte_idx)
{
    uint8_t *ptr = &g_rw_dpcd_reg_0082h_DSC_CRC_0;
    return *(ptr + byte_idx);
}

uint8_t edp_dpcd_read_0260_026F(uint8_t addr_ll)
{
    if(addr_ll == 0x00)
        return edp_dpcd_read_0260h();
    else if(addr_ll == 0x01)
        return g_rw_dpcd_reg_0081h_TEST_EDID_CHECKSUM;
    else if((addr_ll > 0x01) && (addr_ll < 0x08))
        return edp_dpcd_read_0262h_0267h_dsc_crc(addr_ll - 0x02);
    else 
        return 0x00;
}
/******************************* 0x0270~0x027F *******************************************/
uint8_t edp_dpcd_read_0270h(void)
{
    return  g_rw_dpcd_reg_0088h.bits.r_PHY_SINK_TEST_LANE_EN << 7 | g_rw_dpcd_reg_0088h.bits.r_PHY_SINK_TEST_LANE_SEL << 4 |
            g_rw_dpcd_reg_0088h.bits.r_CRC_3D_OPTION1_SELECT << 1 | g_rw_dpcd_reg_0088h.bits.r_TEST_SINK_START;
}

uint8_t edp_dpcd_read_0270_027F(uint8_t addr_ll)
{
    switch(addr_ll)
    {
        case 0x00:
            return edp_dpcd_read_0270h();
        default:
            break;
    }
    return 0x00;
}
/******************************* 0x0280~0x028F *******************************************/
/*uint8_t edp_dpcd_read_0280h(void)
{
    return g_rw_dpcd_reg_0089h.bits.r_FEC_RUNNING_INDICATOR << 2 | g_rw_dpcd_reg_0089h.bits.r_FEC_DECODE_DIS_DETECTED << 1 |
           g_rw_dpcd_reg_0089h.bits.r_FEC_DECODE_EN_DETECTED;
}*/
/*uint8_t edp_dpcd_read_0282h(void)
{
    return g_rw_dpcd_reg_008Bh.byte;
}*/
uint8_t edp_dpcd_read_0280_028F(uint8_t addr_ll)
{
    switch(addr_ll)
    {
        case 0x00:
            //return edp_dpcd_read_0280h();
            return g_rw_dpcd_reg_0089h.byte;
        case 0x01:
            return g_rw_dpcd_reg_008Ah_FEC_ERROR_COUNT;
        case 0x02:
            //return edp_dpcd_read_0282h();
            return g_rw_dpcd_reg_008Bh.byte;
        default :
            break;
    }
    return 0x00;
}

//----------------------------------------------------------------------------------------- 
// read 0x0200~ 0x02FF 
//----------------------------------------------------------------------------------------- 
uint8_t edp_dpcd_read_02xxh(uint8_t addr_l)
{
    uint8_t addr_lh, addr_ll;
    addr_lh = addr_l & 0xF0;
    addr_ll = addr_l & 0x0F;
    switch (addr_lh)
    {
        case 0x00:
            return edp_dpcd_read_0200_020F(addr_ll);
        case 0x10:
            return edp_dpcd_read_0210_021F(addr_ll);
        case 0x20:
            return edp_dpcd_read_0220_022F(addr_ll);
        case 0x30:
            return edp_dpcd_read_0230_023F(addr_ll);
        case 0x40:
            return edp_dpcd_read_0240_024F(addr_ll);
        case 0x50:
            return edp_dpcd_read_0250_025F(addr_ll);
        case 0x60:
            return edp_dpcd_read_0260_026F(addr_ll);
        case 0x70:
            return edp_dpcd_read_0270_027F(addr_ll);
        case 0x80:
            return edp_dpcd_read_0280_028F(addr_ll);    
        default:
            break;
    }
    return 0;
}
/******************************* 0x0370~0x037F *******************************************/
#ifdef AMD_DPCD_FUN
uint8_t edp_dpcd_read_0370_037F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_amd_dpcd_0011h.byte;
    if(addr_ll < 0x0B)
        return *(ptr + addr_ll);
    else
        return 0x00;
}
#endif

/******************************* 0x0350~0x035F *******************************************/
#ifdef INTEL_DPCD_FUN
uint8_t edp_dpcd_read_0350_035F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_intel_dpcd_000Bh_dpcd_00354h_DPCD_INTEL_EDP_BRIGHTNESS_NITS;
    if(addr_ll >= 0x04 && addr_ll <= 0x09)
         return *(ptr + (addr_ll - 0x04));
    return 0x00;
}
#endif
/******************************* 0x0340~0x034F *******************************************/
#ifdef INTEL_DPCD_FUN
uint8_t edp_dpcd_read_0340_034F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_intel_dpcd_0005h_dpcd_00340h_DPCD_INTEL_EDPHDR_CAPS;
    // 0x340~0x345
    if(addr_ll < 0x06)   
        return *(ptr + addr_ll);
    else 
        return 0x00;
}
#endif
/******************************* 0x0330~0x033F *******************************************/
#ifdef AMD_DPCD_FUN
uint8_t edp_dpcd_read_0330_033F(uint8_t addr_ll)
{
    if(addr_ll == 0x00)
        return g_rw_amd_dpcd_0010h_dpcd_00330h_Sink_ALPM_State;
    else
        return 0x00;
}
#endif
/******************************* 0x0320~0x032F *******************************************/
/*uint8_t edp_dpcd_read_032Eh(void)
{
    return g_rw_amd_dpcd_000Fh.bits.dpcd_0032Eh_bit0_backlight_Control_Method | 0x00;
}
uint8_t edp_dpcd_read_032Fh(void)
{
    return g_rw_amd_dpcd_000Fh.bits.dpcd_0032Fh_bit0_backlight_Enabled | 0x00;
}*/
#ifdef AMD_DPCD_FUN
uint8_t edp_dpcd_read_0320_032F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_amd_dpcd_0001h_dpcd_00320h_Backlight_Level_In_MilliNits;
    if(addr_ll < 0x0E)
        return *(ptr + addr_ll);
    else if(addr_ll == 0x0E)
        return (uint8_t)g_rw_amd_dpcd_000Fh.bits.dpcd_0032Eh_bit0_backlight_Control_Method;
    else if(addr_ll == 0x0F)
        return (uint8_t)g_rw_amd_dpcd_000Fh.bits.dpcd_0032Fh_bit0_backlight_Enabled;
    else 
        return 0x00;
}
#endif
/******************************* 0x0310~0x031F *******************************************/
/*uint8_t edp_dpcd_read_0317h(void)
{
     return g_rw_amd_dpcd_0000h.byte;
}*/

uint8_t edp_dpcd_read_0310_031F(uint8_t addr_ll)
{
    switch(addr_ll)
    {
#ifdef INTEL_DPCD_FUN
        case 0x04:      // 0x0314
            return g_rw_intel_dpcd_0003h_dpcd_00314h_Intel_Private_Power_Capabilities;
        case 0x06:      // 0x0316
            return g_rw_intel_dpcd_0004h_dpcd_00316h_Intel_Private_Power_Control;
#endif
#ifdef AMD_DPCD_FUN
        case 0x07:
            //return edp_dpcd_read_0317h();
            return g_rw_amd_dpcd_0000h.byte;
#endif
        default:
            break;
    }
    return 0x00;

}
/******************************* 0x0300~0x030F *******************************************/
uint8_t edp_dpcd_read_0300_030F(uint8_t addr_ll)
{
#ifdef INTEL_DPCD_FUN
    if(addr_ll == 0x00)
        return g_rw_intel_dpcd_0000h_sourcer_IEEE_OUI_byte0;
    else if(addr_ll == 0x01)
        return g_rw_intel_dpcd_0001h_sourcer_IEEE_OUI_byte1;
    else if(addr_ll == 0x02)
        return g_rw_intel_dpcd_0002h_sourcer_IEEE_OUI_byte2;
    else if(addr_ll < EDP_DPCD_030X_SRC_DEVICE_SPECIFIC_SZ)   // 0x303~0x30B
        return g_edp_aux_dpcd_030xh[addr_ll - 0x03];
#else
    if(addr_ll < EDP_DPCD_030X_SRC_DEVICE_SPECIFIC_SZ)        // 0x303~0x30B
        return g_edp_aux_dpcd_030xh[addr_ll - 0x03];
#endif
    return 0x00;
}

//----------------------------------------------------------------------------------------- 
// read 0x0300~ 0x03FF
//----------------------------------------------------------------------------------------- 
uint8_t edp_dpcd_read_03xxh(uint8_t addr_l)
{
    uint8_t addr_lh, addr_ll;
    addr_lh = addr_l & 0xF0;
    addr_ll = addr_l & 0x0F;
        switch (addr_lh)
    {
        case 0x00:
            return edp_dpcd_read_0300_030F(addr_ll);
        case 0x10:
            return edp_dpcd_read_0310_031F(addr_ll);
        case 0x20:
        #ifdef AMD_DPCD_FUN					
            return edp_dpcd_read_0320_032F(addr_ll);
				#else
				    return 0x00;
				#endif
        case 0x30:
				#ifdef AMD_DPCD_FUN
            return edp_dpcd_read_0330_033F(addr_ll);
				#else
				    return 0x00;
				#endif
        case 0x40:
				#ifdef INTEL_DPCD_FUN
            return edp_dpcd_read_0340_034F(addr_ll);
				#else
				    return 0x00;
				#endif
        case 0x50:
				#ifdef INTEL_DPCD_FUN
            return edp_dpcd_read_0350_035F(addr_ll);
				#else
				    return 0x00;
				#endif
        case 0x70:
	      #ifdef AMD_DPCD_FUN
            return edp_dpcd_read_0370_037F(addr_ll);
				#else
				    return 0x00;
				#endif
        default:
            break;
    }
    return 0x00;
}
//----------------------------------------------------------------------------------------- 
// read 0x0400~ 0x4FF 
//----------------------------------------------------------------------------------------- 
uint8_t edp_dpcd_read_04xxh(uint8_t addr_l)
{
    uint8_t *ptr = &g_rw_dpcd_reg_008Ch_IEEE_OUI_0;
    // dpcd 0x0400~0x040B
    if(addr_l < 0x0C)
        return *(ptr + addr_l);
    return 0;
}
//----------------------------------------------------------------------------------------- 
// read 0x0600~ 0x6FF 
//----------------------------------------------------------------------------------------- 
uint8_t edp_dpcd_read_06xxh(uint8_t addr_l)
{
    if (addr_l == 0x00)      // dpcd 0x0600
    {
        return g_rw_dpcd_reg_0098h.byte;
    }
    return 0;
}
/******************************* 0x0700~0x070F *******************************************/
uint8_t edp_dpcd_read_701(void)
{
#ifdef CABC_FUN
    uint8_t var0 = 0;
    var0 = g_rw_dpcd_reg_009Ah.byte & 0xF8;
    return  (g_rw_cabc_007Fh.bits.r_bl_aux_en_cap << 2)| (g_rw_cabc_007Fh.bits.r_bl_pin_en_cap << 1) | 
             g_rw_cabc_007Fh.bits.r_bl_adju_cap | var0;
#else
    return g_rw_dpcd_reg_009Ah.byte;
#endif
}

uint8_t edp_dpcd_read_703(void)
{
#ifdef CABC_FUN
    return g_rw_cabc_00A4h.bits.r_rdp_brightness_alignment << 1 | g_rw_dpcd_reg_009Ch.bits.r_OVERDRIVE_ENGINE_ENABLED;
#else
    return g_rw_dpcd_reg_009Ch.bits.r_BACKLIGHT_BRIGHTNESS_BIT_ALIGNMENT << 1 | g_rw_dpcd_reg_009Ch.bits.r_OVERDRIVE_ENGINE_ENABLED;
#endif
}

uint8_t edp_dpcd_read_0700_070F(uint8_t addr_ll)
{
    switch(addr_ll)
    {
        case 0x00:
            return g_rw_dpcd_reg_0099h_EDP_DPCD_REV;
        case 0x01:
            return edp_dpcd_read_701();
        case 0x02:
        #ifdef CABC_FUN
            return g_rw_cabc_0080h.byte;
        #else
            return g_rw_dpcd_reg_009Bh.byte;
        #endif
        case 0x03:
            return edp_dpcd_read_703();
#ifdef EDP1_4_FUN
        case 0x04:
            return g_rw_dpcd_reg_009Dh.byte;
#endif
        default:
        break;            
    }
    return 0;
}
/******************************* 0x0720~0x072F *******************************************/
uint8_t edp_dpcd_read_0720(void)
{
    //return (var0 | (g_rw_dpcd_reg_009Eh.bits.BLACK_VIDEO_ENABLE << 1) | (var1 << 5) | (var2 >> 1) | (var3 << 1));
    /*return (g_dpcd_ena_bits.bl_en_0720h | (g_rw_dpcd_reg_009Eh.bits.BLACK_VIDEO_ENABLE << 1) | (g_dpcd_ena_bits.frc_en_0720h << 2) | 
            (g_dpcd_ena_bits.color_engine_en_0720h << 3) | (g_dpcd_ena_bits.vblk_bl_updt_en_0720h << 7));*/
#ifdef CABC_FUN
    return (g_rw_cabc_0087h.bits.r_rdp_blsync2vs_en << 7) | (g_rw_daf_001Eh.bits.r_c3d_en << 3) | (g_rw_daf_0000h.bits.r_frc_en << 2) | 
           (g_rw_dpcd_reg_009Eh.bits.r_BLACK_VIDEO_ENABLE << 1) | g_rw_cabc_0087h.bits.r_rdp_bl_en | 0x00 ; 
#else
    return g_rw_dpcd_reg_009Eh.byte;
#endif
}

uint8_t edp_dpcd_read_0721(void)
{
    //return(var1| (g_dpcd_ena_bits.bl_frq_pwm_pin_passthru_en_0721h << 2 ) | 
    //       (g_dpcd_ena_bits.bl_frq_aux_set_en_0721h << 3) | (g_dpcd_ena_bits.dynamic_bl_en_0721h << 4));
#ifdef CABC_FUN
    return (g_rw_cabc_0087h.bits.r_rdp_bl_dbc_en << 4) | (g_rw_cabc_0087h.bits.r_rdp_bl_freq_set_en << 3) | 
           (g_rw_cabc_0087h.bits.r_rdp_pwm_pin_passthru_en << 2) | g_rw_cabc_008Bh.bits.r_rdp_bl_ctrlmode_en | 0x00;
#else
    return g_rw_dpcd_reg_009Fh.byte;
#endif
}

uint8_t edp_dpcd_read_0720_072F(uint8_t addr_ll)
{
    switch(addr_ll)
    {
        case 0x00:
            return edp_dpcd_read_0720();
        case 0x01:
            return edp_dpcd_read_0721();
        case 0x02:   // DPCD 0x0722
        #ifdef CABC_FUN
            return g_rw_cabc_0088h_rdp_bl_brmsb;
        #else
            return g_rw_dpcd_reg_00A0h_EDP_BACKLIGHT_BRIGHTNESS_MSB;
        #endif
        case 0x03:   // DPCD 0x0723
        #ifdef CABC_FUN
            return g_rw_cabc_0089h_rdp_bl_brlsb;
        #else
            return g_rw_dpcd_reg_00A1h_EDP_BACKLIGHT_BRIGHTNESS_LSB;
        #endif
        case 0x04:   // DPCD 0x0724
        #ifdef CABC_FUN
            return g_rw_cabc_008Bh.bits.r_rdp_pwmgen_res;
        #else
            return g_rw_dpcd_reg_00A2h.byte;
        #endif
        case 0x05:   // DPCD 0x0725
        #ifdef CABC_FUN
            return g_rw_cabc_0083h.bits.r_pwmgen_bit_cnt_min_cap;
        #else
            return g_rw_dpcd_reg_00A3h.byte;
        #endif
        case 0x06:   // DPCD 0x0726
        #ifdef CABC_FUN
            return g_rw_cabc_0086h.bits.r_pwmgen_bit_cnt_max_cap;
        #else
            return g_rw_dpcd_reg_00A4h.bits.r_EDP_PWMGEN_BIT_COUNT_CAP_MAX;
        #endif
        case 0x07:
            return 0;
        case 0x08:   // DPCD 0x0728
        #ifdef CABC_FUN
            return g_rw_cabc_008Ah_rdp_pwmgen_freq;
        #else
            return g_rw_dpcd_reg_00A5h_EDP_BACKLIGHT_FREQ_SET;
        #endif
        case 0x0A:   // DPCD 0x072A
        #ifdef CABC_FUN
            return g_rw_cabc_0081h_bl_freq_min_msb_cap;
        #else
            return g_rw_dpcd_reg_00A6h_EDP_BACKLIGHT_FREQ_CAP_MIN_MSB;
        #endif
        case 0x0B:   // DPCD 0x072B
        #ifdef CABC_FUN
            return g_rw_cabc_0082h_bl_freq_min_mid_cap;
        #else
            return g_rw_dpcd_reg_00A7h_EDP_BACKLIGHT_FREQ_CAP_MIN_MID;
        #endif
        case 0x0C:   // DPCD 0x072C
        #ifdef CABC_FUN
            return g_rw_cabc_0083h.bits.r_bl_freq_min_lsb_cap | 0x00;
        #else
            return g_rw_dpcd_reg_00A8h.byte;
        #endif
        case 0x0D:   // DPCD 0x072D
        #ifdef CABC_FUN
            return g_rw_cabc_0084h_bl_freq_max_msb_cap;
        #else
            return g_rw_dpcd_reg_00A9h_EDP_BACKLIGHT_FREQ_CAP_MAX_MSB;
        #endif
        case 0x0E:   // DPCD 0x072E
        #ifdef CABC_FUN
            return g_rw_cabc_0085h_bl_freq_max_mid_cap;
        #else
            return g_rw_dpcd_reg_00AAh_EDP_BACKLIGHT_FREQ_CAP_MAX_MID;
        #endif
        case 0x0F:   // DPCD 0x072F
        #ifdef CABC_FUN
            return g_rw_cabc_0086h.bits.r_bl_freq_max_lsb_cap | 0x00;
        #else
            return g_rw_dpcd_reg_00ABh.byte;
        #endif
        default:
        break;            
    }
    return 0;
}
/******************************* 0x0730~0x073F *******************************************/

uint8_t edp_dpcd_read_0732(void)
{
#ifdef CABC_FUN
    return (g_rw_cabc_008Ch.bits.r_rdp_dbc_brmin_bits_4_1 << 1) | g_rw_cabc_008Bh.bits.r_rdp_dbc_brmin_bits_0 | 0x00;
#else
    return g_rw_dpcd_reg_00ACh.byte;
#endif
}

uint8_t edp_dpcd_read_0733(void)
{
#ifdef CABC_FUN
    return (g_rw_cabc_008Dh.bits.r_rdp_dbc_brmax_bits_4 << 4) |  g_rw_cabc_008Ch.bits.r_rdp_dbc_brmax_bits_3_0 | 0x00;
#else
    return g_rw_dpcd_reg_00ADh.byte;
#endif
}
/******************************* 0x0740~0x074F *******************************************/
#ifdef EDP1_4_FUN
uint8_t edp_dpcd_read_0740_074F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_dpcd_reg_00AEh_REGION_INDEX_OFFSET;
    return *(ptr + addr_ll);
}
/******************************* 0x0750~0x075F *******************************************/
uint8_t edp_dpcd_read_0750_075F(uint8_t addr_ll)
{
    if(addr_ll == 0x00)    // 0x0750
        return g_rw_dpcd_reg_00BEh_SINK_VIDEO_READY_DELAY;
    else if(addr_ll == 0x01)
        return g_rw_dpcd_reg_00BFh_SINK_TRAILING_VIDEO_DELAY;
    else 
        return 0x00;
}
/******************************* 0x07A0~0x07AF *******************************************/
uint8_t edp_dpcd_read_07A0_07AF(uint8_t addr_ll)
{
    uint8_t *ptr1 = &g_rw_dpcd_read_001Eh_TEST_CRC_R_Cr_MSO_2; 
    uint8_t *ptr2 = &g_rw_dpcd_read_0024h_TEST_CRC_R_Cr_MSO_3;
    switch(addr_ll)
    {
        case 0x04:     // 0x7A4
            return g_rw_dpcd_reg_00C0h_MSO_LINK_CAPABILITIES;
        case 0x05:
        case 0x06:
        case 0x07:
        case 0x08:
        case 0x09:
        case 0x0A:
            return *(ptr1 + (addr_ll - 0x05));
        case 0x0B:
            return g_rw_dpcd_read_0056h_TEST_SINK_MISC_MSO_2;
        case 0x0C:
        case 0x0D:
        case 0x0E:
        case 0x0F:
            return *(ptr2 + (addr_ll - 0x0C));
        default :
            break;
    }
    return 0x00;
}
/******************************* 0x07B0~0x07BF *******************************************/
uint8_t edp_dpcd_read_07B0_07BF(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_dpcd_read_002Ah_TEST_CRC_R_Cr_MSO_4;
    switch(addr_ll)
    {
        case 0x00:   // 0x7B0
            return g_rw_dpcd_read_0028h_TEST_CRC_B_Cr_MSO_3;
        case 0x01:
            return g_rw_dpcd_read_0029h_TEST_CRC_B_Cr_MSO_3;
        case 0x02 :
            return g_rw_dpcd_read_0057h_TEST_SINK_MISC_MSO_3;
        case 0x03:
        case 0x04:
        case 0x05:
        case 0x06:
        case 0x07:
        case 0x08:
            return *(ptr + (addr_ll - 0x03));
        case 0x09:
            return g_rw_dpcd_read_0058h_TEST_SINK_MISC_MSO_4;
        default:
            break;
    }
    return 0x00;
}
/******************************* 0x07C0~0x07CF *******************************************/
uint8_t edp_dpcd_read_07C0_07CF(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_dpcd_read_0030h_DSC_CRC_0_MSO2;
    return *(ptr + addr_ll);
    
}
/******************************* 0x07D0~0x07DF *******************************************/
uint8_t edp_dpcd_read_07D0_07DF(uint8_t addr_ll)
{
    if(addr_ll == 0x00)     // 0x7D0
        return g_rw_dpcd_read_0040h_DSC_CRC_2_MSO4;
    else if(addr_ll == 0x01)
        return g_rw_dpcd_read_0041h_DSC_CRC_2_MSO4;
    else
        return 0x00;
}
#endif
//----------------------------------------------------------------------------------------- 
// read 0x700~ 0x7FF 
//----------------------------------------------------------------------------------------- 
uint8_t edp_dpcd_read_07xxh(uint8_t addr_l)
{
    uint8_t addr_lh, addr_ll;

    addr_lh = addr_l & 0xF0;
    addr_ll = addr_l & 0x0F;

    if (addr_lh == 0x00) 
        return edp_dpcd_read_0700_070F(addr_ll);
    else if (addr_lh == 0x20)
        return edp_dpcd_read_0720_072F(addr_ll);
    else if (addr_l == 0x32) 
        return edp_dpcd_read_0732();
    else if (addr_l == 0x33)
        return edp_dpcd_read_0733();
#ifdef EDP1_4_FUN
    else if(addr_lh == 0x40)
        return edp_dpcd_read_0740_074F(addr_ll);

    else if(addr_lh == 0x50)
        return edp_dpcd_read_0750_075F(addr_ll);
    else if(addr_lh == 0xA0)
        return edp_dpcd_read_07A0_07AF(addr_ll);
    else if(addr_lh == 0xB0)
        return edp_dpcd_read_07B0_07BF(addr_ll);
    else if(addr_lh == 0xC0)
        return edp_dpcd_read_07C0_07CF(addr_ll);
    else if(addr_lh == 0xD0)
        return edp_dpcd_read_07D0_07DF(addr_ll);
#endif
    else
        return 0;
}
/******************************* 0x2000~0x200F *******************************************/
uint8_t edp_dpcd_read_2000_200F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_dpcd_read_0042h_SINK_COUNT_ESI;
    
    switch(addr_ll)
    {
        case 0x02:   // 0x2002h
        case 0x03:
        case 0x04:
        case 0x05:
#ifdef EDP1_4_FUN
        case 0x06:
        case 0x07:
        case 0x08:
        case 0x09:
        case 0x0A:
        case 0x0B:
#endif
            return *(ptr + (addr_ll - 0x02));

        case 0x0C:
            //return g_rw_dpcd_read_004Ch.byte;
            if(g_rw_dpcd_reg_0009h_DPCD_REV == 0x13)
                return g_rw_dpcd_read_0050h.bits.r_AUX_FRAME_SYNC_LOCK_ERROR | 0x00;
            else
                return edp_dpcd_read_0202h_lane0_1_status();  // same 202h
        case 0x0D:
            //return g_rw_dpcd_read_004Dh.byte;
            if(g_rw_dpcd_reg_0009h_DPCD_REV == 0x13)
                return 0x00;
            else
                return edp_dpcd_read_0203h_lane2_3_status();  // same 203h 
        case 0x0E:
            return edp_dpcd_read_0204h();  // same 204h 
        case 0x0F:
            return edp_dpcd_read_0205h_port_0_status();  // same 205h
        default:
            break;
    }
    return 0x00;
}
/******************************* 0x2010~0x201F *******************************************/
#ifdef EDP1_4_FUN
uint8_t edp_dpcd_read_2010_201F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_dpcd_read_0050h.byte;

    if(addr_ll < 0x03)
        return *(ptr + addr_ll);

    else 
        return 0x00;
}
/******************************* 0x2020~0x202F *******************************************/
uint8_t edp_dpcd_read_2020_202F(uint8_t addr_ll)
{
    if(addr_ll == 0x00)         // 0x2020h
        return g_rw_dpcd_read_0053h.byte;
    else if(addr_ll == 0x02)    // 0x2022h
        return g_rw_dpcd_read_0054h.byte;
    else if(addr_ll == 0x05)    // 0x2005h
        return g_rw_dpcd_reg_00D0h.byte;
    else 
        return 0x00;
}
#endif
//----------------------------------------------------------------------------------------- 
// read 0x2000~ 0x20FF 
//----------------------------------------------------------------------------------------- 
uint8_t edp_dpcd_read_20xxh(uint8_t addr_l)
{
    uint8_t addr_lh, addr_ll;

    addr_lh = addr_l & 0xF0;
    addr_ll = addr_l & 0x0F;
    if(addr_lh == 0x00)
        return edp_dpcd_read_2000_200F(addr_ll);
#ifdef EDP1_4_FUN
    else if(addr_lh == 0x10)
        return edp_dpcd_read_2010_201F(addr_ll);
    else if(addr_lh == 0x20)
        return edp_dpcd_read_2020_202F(addr_ll);
#endif
    else
        return 0;
}

/******************************* 0x2200~0x220F *******************************************/
uint8_t edp_dpcd_read_2203(void)
{
    return g_rw_dpcd_reg_00C8h.bits.r_EXT_TPS4_SUPPORTED<< 7 |g_rw_dpcd_reg_00C8h.bits.r_EXT_NO_AUX_TRANSACTION_LINK_TRAINING << 6 |
           g_rw_dpcd_reg_00C8h.bits.r_EXT_STREAM_REGENERATION_STATUS_CAPABILITY << 1 |g_rw_dpcd_reg_00C8h.bits.r_EXT_MAX_DOWNSPREAD ;
}
uint8_t edp_dpcd_read_2204(void)
{
    return g_rw_dpcd_reg_00C9h.bits.r_EXT_18V_DP_PWR_CAP << 7 | g_rw_dpcd_reg_00C8h.bits.r_EXT_12V_DP_PWR_CAP << 6 |
           g_rw_dpcd_reg_00C8h.bits.r_EXT_5V_DP_PWR_CAP << 5 | g_rw_dpcd_reg_00C8h.bits.r_EXT_CRC_3D_OPTIONS_SUPPORTED << 1 | 
           g_rw_dpcd_reg_00C8h.bits.r_EXT_NORP;
}
uint8_t edp_dpcd_read_2205(void)
{
    return g_rw_dpcd_reg_00C9h.bits.r_EXT_DETAILED_CAP_INFO_AVAILABLE << 4 | g_rw_dpcd_reg_00C9h.bits.r_EXT_FORMAT_CONVERSION << 3 |
           g_rw_dpcd_reg_00C9h.bits.r_EXT_DFP_TYPE << 1 | g_rw_dpcd_reg_00C9h.bits.r_EXT_DFP_PRESENT;
}
uint8_t edp_dpcd_read_2206(void)
{
    return g_rw_dpcd_reg_00C9h.bits.r_EXT_128b132b_SUPPORTED << 1 | g_rw_dpcd_reg_00C9h.bits.r_EXT_8b10b_SUPPORTED ;
}
uint8_t edp_dpcd_read_220D(void)
{
    return g_rw_dpcd_reg_00D1h.bits.r_EXT_DPCD_DISPLAY_CONTROL_CAPABLE << 3 | g_rw_dpcd_reg_00D1h.bits.r_EXT_FRAMING_CHANGE << 1 |
           g_rw_dpcd_reg_00D1h.bits.r_EXT_ASSR;
}

uint8_t edp_dpcd_read_2200_220F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_dpcd_reg_00CAh.byte;
    switch(addr_ll)
    {
        case 0x00:     // 0x2200h
            return g_rw_dpcd_reg_00C5h_EXT_DPCD_REV;
        case 0x01:
            return g_rw_dpcd_reg_00C6h_EXT_8b10b_MAX_LINK_RATE;
        case 0x02:
            return g_rw_dpcd_reg_00C7h.byte;
        case 0x03:
            return edp_dpcd_read_2203();
        case 0x04:
            return edp_dpcd_read_2204();
        case 0x05:
            return edp_dpcd_read_2205();
        case 0x06:
            return edp_dpcd_read_2206();
        case 0x07:
        case 0x08:
        case 0x09:
        case 0x0A:
        case 0x0B:
        case 0x0C:
            return *(ptr + (addr_ll - 0x07));
        case 0x0D:
            return edp_dpcd_read_220D();
        case 0x0E:
            return g_rw_dpcd_reg_00D2h.byte;
        default:
            break;
    }
    return 0x00;
}
/******************************* 0x2210~0x221F *******************************************/
#ifdef EDP1_4_FUN
uint8_t edp_dpcd_read_2214(void)
{
    return g_rw_dpcd_reg_00D7h.bits.r_AS_SDP_FIRST_HALF_LINE_OR_3840_PIXEL_CYCLE_WINDOW_NOT_SUPPORTED << 1 | 
           g_rw_dpcd_reg_00D7h.bits.r_ADAPTIVE_SYNC_SDP_SUPPORTED;
}
uint8_t edp_dpcd_read_2218(void)
{
    return g_rw_dpcd_reg_00D7h.bits.r_ADAPTIVE_SYNC_SDP_T2_SUPPORTED_IN_ALL_PR_ACTIVE_STATES << 6;
}
#endif
uint8_t edp_dpcd_read_2210_221F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_dpcd_reg_00D3h.byte;
    
    if(addr_ll < 0x04)
        return *(ptr + addr_ll);
#ifdef EDP1_4_FUN
    else if(addr_ll == 0x04)
        return edp_dpcd_read_2214();
    else if(addr_ll == 0x08)
        return edp_dpcd_read_2218();
#endif
    
    return 0x00;
}
/******************************* 0x2260~0x226F *******************************************/
#ifdef EDP1_4_FUN
uint8_t edp_dpcd_read_2260_226F(uint8_t addr_ll)
{
    if(addr_ll == 0x07)
        return g_rw_dpcd_reg_00D8h_ext_bits_per_pixel;
    else if(addr_ll == 0x08)
        return g_rw_dpcd_reg_00D9h.byte;
    return 0x00;
}
#endif
//----------------------------------------------------------------------------------------- 
// read 0x2200~ 0x22FF 
//----------------------------------------------------------------------------------------- 
uint8_t edp_dpcd_read_22xxh(uint8_t addr_l)
{
    uint8_t addr_lh, addr_ll;

    addr_lh = addr_l & 0xF0;
    addr_ll = addr_l & 0x0F;

    if(addr_lh == 0x00)
        //return edp_dpcd_read_0000_000F(addr_ll); // for 303
        return edp_dpcd_read_2200_220F(addr_ll);
    else if(addr_lh == 0x10)
        return edp_dpcd_read_2210_221F(addr_ll);
#ifdef EDP1_4_FUN
    else if(addr_lh == 0x60)
        return edp_dpcd_read_2260_226F(addr_ll);
#endif
    return 0;
}
/******************************* 0x3050~0x305F *******************************************/
uint8_t edp_dpcd_read_3050_305F(uint8_t addr_ll)
{
    uint8_t *ptr = &g_rw_dpcd_reg_00D8h_ext_bits_per_pixel;
    
    if((addr_ll > 0x03) && (addr_ll < 0x0A))
        return *(ptr + (addr_ll - 0x04));
    return 0x00;
}
//----------------------------------------------------------------------------------------- 
// read 0x3000~ 0x30FF 
//----------------------------------------------------------------------------------------- 
uint8_t edp_dpcd_read_30xxh(uint8_t addr_l)
{
    uint8_t addr_lh, addr_ll;

    addr_lh = addr_l & 0xF0;
    addr_ll = addr_l & 0x0F;
    if(addr_lh == 0x50)
         return edp_dpcd_read_3050_305F(addr_ll);
    return 0x00;
}
//----------------------------------------------------------------------------------------- 
// read 0x3000~ 0x30FF 
//----------------------------------------------------------------------------------------- 
#ifdef EDP1_4_FUN
uint8_t edp_dpcd_read_60xxh(uint8_t addr_l)
{
    if(addr_l == 0x06)   // 0x6006h
        return g_rw_dpcd_reg_00E1h.byte;
    return 0x00;
}
#endif
//----------------------------------------------------------------------------------------- 
// dpcd read
//----------------------------------------------------------------------------------------- 
uint8_t edp_dpcd_read(uint8_t addr_h, uint8_t addr_m, uint8_t addr_l)
{
    if (addr_h == 0) {
        switch (addr_m) {
        case 0x00:
            return edp_dpcd_read_00xxh(addr_l);
        case 0x01:
            return edp_dpcd_read_01xxh(addr_l);
        case 0x02:
            return edp_dpcd_read_02xxh(addr_l);
        case 0x03:
            return edp_dpcd_read_03xxh(addr_l);
        case 0x04:
            return edp_dpcd_read_04xxh(addr_l);
        case 0x06:
            return edp_dpcd_read_06xxh(addr_l);
        case 0x07:
            return edp_dpcd_read_07xxh(addr_l);

        case 0x20:
            return edp_dpcd_read_20xxh(addr_l);
        case 0x22:
            return edp_dpcd_read_22xxh(addr_l);
        case 0x30:
            return edp_dpcd_read_30xxh(addr_l);
#ifdef EDP1_4_FUN
        case 0x60:
            return edp_dpcd_read_60xxh(addr_l);
#endif
        default:
            break;
        }
    }
    return 0;
}
#endif
