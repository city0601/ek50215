/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: eDP
		Platform: 8051
		Author: Eric Lee, 2021/04/07
		Description: spi over aux
*/
// *******************************************************************
#include "mcu_fun_def.h"

#ifdef EDP_FUN
// Include Path
#include "edp_global_vars.h"
#include "ioa_spi.h"
#include "mcu_spi.h"
#include "reg_include.h"
// *******************************************************************
// *******************************************************************
// SPI
#define IOA_SPI_BYTES_SZ                    (256)
#define EDP_AUX_LAST_SPI_IDLE               (0x00)
#define EDP_AUX_LAST_SPI_WRITE              (0x01)
#define EDP_AUX_LAST_SPI_READ               (0x02)
#define EDP_AUX_LAST_SPI_ERASE              (0x03)
#define EDP_AUX_LAST_SPI_READ_STATUS        (0x04)
#define EDP_AUX_LAST_SPI_WRITE_STATUS       (0x05)
#define EDP_AUX_LAST_SPI_READ_DL_STATUS     (0x06)
// *******************************************************************
// *******************************************************************
extern void tcon_aux_tx_bug_fix(uint8_t val);
static uint8_t xdata edp_aux_last_spi[EDP_PORT_NUM];
static uint8_t xdata g_spi_data_buf[IOA_SPI_BYTES_SZ];
static uint16_t xdata g_ioa_spi_buf_offset;
static uint32_t xdata g_spi_addr_offset = 0;
// *******************************************************************
void ioa_spi_init(uint8_t port)
{
    g_spi_addr_offset = 0;
    edp_aux_last_spi[port] = EDP_AUX_LAST_SPI_IDLE;
}

static void cal_spi_addr_offset(void)
{
    VAR_MERGER_32(g_spi_addr_offset, edp_aux_rx_bytes[6]);
    g_spi_addr_offset >>= 8;
    g_ioa_spi_buf_offset = 0;
}

static void storage_spi_date(uint8_t len)
{
    uint8_t cnt;

    for(cnt = 0 ; cnt < len ; cnt++)
    {
        g_spi_data_buf[g_ioa_spi_buf_offset + cnt] = edp_aux_rx_bytes[5 + cnt];
    }
}

static bool ioa_spi_check_cmd(uint8_t port)
{
    uint8_t seq_cnt = edp_aux_rx_bytes[0];          // recv data len
    uint8_t cmd = 0;
    uint8_t len = edp_aux_rx_bytes[4] + 1;          // rw len
    //bool res = TRUE;
    
    if(edp_aux_last_spi[port] == EDP_AUX_LAST_SPI_IDLE)
    {
		cmd = edp_aux_rx_bytes[5];
        if(cmd == SPI_FLASH_CMD_WRITE_STATUS)
        {
            if(seq_cnt == 0x06)
            {
                mcu_spi_flash_write_status(edp_aux_rx_bytes[6] , 0); 
                return TRUE;
            }
        } 
        else if(cmd == SPI_FLASH_CMD_SECTOR_ERASE)
        {
            if(seq_cnt == 0x08)
            {
                //mcu_spi_flash_sector_erase(edp_aux_rx_bytes[6],edp_aux_rx_bytes[7],edp_aux_rx_bytes[8], 0);  
                mcu_spi_flash_merge_erase(SPI_FLASH_CMD_SECTOR_ERASE,&edp_aux_rx_bytes[6], 0);							
                return TRUE;    
            }
        }   
        else if(cmd == SPI_FLASH_CMD_PAGE_PROGRAM)
        {
            if(seq_cnt == 0x08)
            {
                cal_spi_addr_offset();
                edp_aux_last_spi[port] = EDP_AUX_LAST_SPI_WRITE;
                return TRUE;
            }
        }
        else if(cmd == SPI_FLASH_CMD_FAST_READ)
        {
            if(seq_cnt == 0x08)
            {
                cal_spi_addr_offset();
                edp_aux_last_spi[port] = EDP_AUX_LAST_SPI_READ;
                return TRUE;
            }
        }
        else if(cmd == SPI_FLASH_CMD_READ_STATUS1)
        {
            if(seq_cnt == 0x05)
            {
                edp_aux_last_spi[port] = EDP_AUX_LAST_SPI_READ_STATUS;
                return TRUE;
            }
        }
        return FALSE;
    }

    if(edp_aux_last_spi[port] == EDP_AUX_LAST_SPI_WRITE)
    {
        if(g_edp_aux_req == EDP_AUX_REQ_WR)
        {
            storage_spi_date(len);
            g_ioa_spi_buf_offset += len;
            return TRUE;
        }
    }
    else if(edp_aux_last_spi[port] == EDP_AUX_LAST_SPI_READ)
    {
        if(g_edp_aux_req == EDP_AUX_REQ_RD)
        {
            mcu_spi_flash_fast_read((uint8_t *)&g_spi_addr_offset+1,&edp_aux_tx_bytes[2],len,0);    
            g_spi_addr_offset += len;
            return TRUE;
        }
    }
    else if(edp_aux_last_spi[port] == EDP_AUX_LAST_SPI_READ_STATUS)
    {
        if(g_edp_aux_req == EDP_AUX_REQ_RD && len == 1)
        {
            edp_aux_tx_bytes[2] = mcu_spi_flash_read_status1(0);
            return TRUE;
        }
    }
    return FALSE;
}

void edp_spi_over_aux(uint8_t port)
{
    uint8_t seq_cnt = edp_aux_rx_bytes[0];          // recv data len
    uint8_t len = edp_aux_rx_bytes[4] + 1;          // rw len
    bool l_res;

    mcu_spi_acc_bus(1);  
    if (g_edp_aux_req == EDP_AUX_REQ_WR)
    {
        if (seq_cnt > 4) 
        {
            l_res = ioa_spi_check_cmd(port);
            if(l_res)
                edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
            else
                edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_NACK;
            tcon_aux_tx_bug_fix(1);
        }
        else
        {
            edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
            tcon_aux_tx_bug_fix(1);
            //edp_aux_last_spi[port] = EDP_AUX_LAST_SPI_IDLE;
        }

        if (g_edp_aux_mot == 0) 
        {
            if(edp_aux_last_spi[port] == EDP_AUX_LAST_SPI_WRITE)
            {
                mcu_spi_flash_page_program((uint8_t *)&g_spi_addr_offset+1,&g_spi_data_buf[0],(g_ioa_spi_buf_offset >> 8),(g_ioa_spi_buf_offset & 0xFF), 0);
            }
			if(edp_aux_last_spi[port] != EDP_AUX_LAST_SPI_READ && edp_aux_last_spi[port] != EDP_AUX_LAST_SPI_READ_STATUS)
				edp_aux_last_spi[port] = EDP_AUX_LAST_SPI_IDLE;
        } 
    }
    else if(g_edp_aux_req == EDP_AUX_REQ_RD)
    {
        if (seq_cnt == 4)
        {
            l_res = ioa_spi_check_cmd(port);
            if(edp_aux_last_spi[port] == EDP_AUX_LAST_SPI_READ || edp_aux_last_spi[port] == EDP_AUX_LAST_SPI_READ_STATUS)
            {
                if(l_res)
                {
                    edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
                    tcon_aux_tx_bug_fix(len+1);
                }
                else
                {
                    edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_NACK;
                    tcon_aux_tx_bug_fix(1);
                }
            }
            else
            {
                if(l_res)
                    edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
                else
                    edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_NACK;
                //edp_aux_tx_bytes[0] = 1;
                tcon_aux_tx_bug_fix(1);
            }
        }
        else
        {
            edp_aux_tx_bytes[1] = EDP_AUX_REPLY_I2C_ACK;
            tcon_aux_tx_bug_fix(1);
        }
        if (g_edp_aux_mot == 0) 
        {
            edp_aux_last_spi[port] = EDP_AUX_LAST_SPI_IDLE;
        } 
    }
    mcu_spi_acc_bus(0);
}

#endif



