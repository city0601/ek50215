/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: JD1730
		Platform: 8051
		Author: Eric Lee, 2021/04/07
		Description: i2c over aux
*/
// *******************************************************************
#include "mcu_fun_def.h"

#ifdef EDP_FUN
#include "edp_global_vars.h"
#include "mcu_i2c.h"
#include "ioa_tcon_reg.h"
#include "edp_misc.h"
#include "ioa_spi.h"
#include "edp_aux.h"
#include "ioa_i2c_chg.h"
// *******************************************************************
// *******************************************************************
// Define
#define IOA_LEN_MAX                 6
// *******************************************************************
// *******************************************************************

//------------------------------------------------
// local parameter
static uint8_t xdata g_ioa_data_cnt[EDP_PORT_NUM];
static uint8_t xdata g_ioa_aux_rx_data[EDP_PORT_NUM][IOA_LEN_MAX];   
//------------------------------------------------

void mcu_ioa_init(void)
{
    uint8_t port = 0;
    uint8_t i;

    for(port = 0; port < EDP_PORT_NUM; port++)
    {
        edp_ioa_mode_en_flag[port] = 0;
        g_ioa_data_cnt[port] = 0;
        ioa_spi_init(port);
        ioa_tcon_reg_init(port);      // Peter add / 211004 
        ioa_i2c_dev_chg_init(port);
        for(i = 0;i<IOA_LEN_MAX;i++)
        {
            g_ioa_aux_rx_data[port][i] = 0;           
        }
    }
}

//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//
bool check_ioa_cfg(uint8_t addr, uint8_t port)
{
    /*if(!edp_ioa_mode_en_flag)
        return 1; */
    edp_i2cbus_mux(g_rw_mcu_top_001Fh.bits.ioa_i2c_path_ctrl);

    if(edp_ioa_mode_en_flag[port] == 1 && (addr == g_rw_dp_009Bh_intel_i2c_slv_addr))
    {
        addr = g_edp_ioa_edid_addr;
        g_edp_i2c_rw_addr[port] = g_edp_ioa_edid_addr;
	}
    
    if((addr != g_edp_ioa_pmic_addr) && (addr != g_edp_ioa_edid_addr) && \
       (addr != g_edp_ioa_cfg_addr) && (addr != g_edp_ioa_dvcom_addr) && \
       (addr != g_edp_ioa_ls1_addr) && (addr != g_edp_ioa_ls2_addr) && (addr != g_edp_ioa_gma_addr))  
    {
        return 1;
    }

    if(!edp_ioa_mode_en_flag[port])
        return 0;       

    return 1;
}

//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//
void ioa_unlock_handler(uint8_t port)
{
    uint8_t seq_cnt;          // recv data len
    uint8_t len;
    uint8_t temp_cnt;
    uint8_t i ;
    //bool res_pass_flg = TRUE;

    seq_cnt = edp_aux_rx_bytes[0];
    if(seq_cnt > 4)
    {
        len = cal_wr_byte_cnt();
        for(i = 0 ; i < len ; i++)
        {
            temp_cnt = g_ioa_data_cnt[port];
            g_ioa_aux_rx_data[port][temp_cnt] = edp_aux_rx_bytes[5+i];
            g_ioa_data_cnt[port] ++;
        }
    }

    if(g_edp_aux_mot == 0)
    {
        if(g_ioa_data_cnt[port] > 5)
        {
            g_ioa_data_cnt[port] = 0;
            if(g_ioa_aux_rx_data[port][0] == 0x29 && \
				g_ioa_aux_rx_data[port][2] == 0x00 && \
				g_ioa_aux_rx_data[port][3] == 0x10 && \
				g_ioa_aux_rx_data[port][5] == 0x00 
			)
            {
                if(g_ioa_aux_rx_data[port][1] == 0x95 && g_ioa_aux_rx_data[port][4] == 0x59)
				{
					edp_ioa_mode_en_flag[port] = 1;
				}
				if(g_ioa_aux_rx_data[port][1] == 0x00 && g_ioa_aux_rx_data[port][4] == 0x00)
				{
					edp_ioa_mode_en_flag[port] = 0;
				}
            }
        }
    }
}  
#endif

