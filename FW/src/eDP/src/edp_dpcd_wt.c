#include "mcu_fun_def.h"

#ifdef EDP_FUN
#include "edp_dpcd_wt.h"
#include "edp_misc.h"
extern void patch_code_0x101(uint8_t wt_val);
//-----------------------------------------------------------------------------------------
static void edp_black_video_by_direct_rgb(void)
{
    // direct rgb setting
    g_rw_daf_000Dh.bits.r_direct_rgb_mode = 0;
    g_rw_daf_0010h.bits.r_direct_r1_bits_4_0 = 0;
    g_rw_daf_0011h.bits.r_direct_r1_bits_11_5 = 0;
    g_rw_daf_0011h.bits.r_direct_g1_bits_0 = 0;
    g_rw_daf_0012h_direct_g1 = 0;
    g_rw_daf_0013h.bits.r_direct_g1_bits_11_9 = 0;
    g_rw_daf_0013h.bits.r_direct_b1_bits_4_0 = 0;
    g_rw_daf_0014h.bits.r_direct_b1_bits_11_5 = 0;
    // enable direct rgb
    g_rw_daf_000Dh.bits.r_direct_rgb_en = 1;
}
//-----------------------------------------------------------------------------------------
// write 0x0000~ 0x00FF
//-----------------------------------------------------------------------------------------
bool edp_dpcd_write_00xxh(uint8_t addr_l)
{
    uint8_t addr_lh, addr_ll;

    addr_lh = addr_l & 0xF0;
    addr_ll = addr_l & 0x0F;

    if (addr_lh == 0x30)
    {
        return TRUE;
    }
    return FALSE;
}
/******************************* 0x0100~0x010F *******************************************/
void edp_dpcd_write_0100h_link_bw_set(uint8_t wt_val)
{
    switch (wt_val)
    {
    case 20: // 5.4Gbps
        //g_rw_aip_reg_0100h.byte = 0x1A;
        //g_rw_aip_reg_0100h.bits.r_rx_cfg_sel = 2;
        g_rw_aip_reg_0169h.bits.r_rx_cfg_sel = 2;
        //g_rw_aip_reg_0100h.bits.r_rx_n_fb_sel = 6;
        g_rw_aip_reg_0169h.bits.r_rx_n_fb_sel_bits_1_0 = 2;
        g_rw_aip_reg_016Ah.bits.r_rx_n_fb_sel_bits_2 = 1;
        break;
    case 10: // 2.7Gbps
		    //g_rw_aip_reg_0100h.byte = 0x0D;
        //g_rw_aip_reg_0100h.bits.r_rx_cfg_sel = 1;
        g_rw_aip_reg_0169h.bits.r_rx_cfg_sel = 1;
        //g_rw_aip_reg_0100h.bits.r_rx_n_fb_sel = 3;
        g_rw_aip_reg_0169h.bits.r_rx_n_fb_sel_bits_1_0 = 3;
        g_rw_aip_reg_016Ah.bits.r_rx_n_fb_sel_bits_2 = 0;
        break;
    case 6: // 1.62G
		//g_rw_aip_reg_0100h.byte = 0;   
        //g_rw_aip_reg_0100h.bits.r_rx_n_fb_sel = 0;
        g_rw_aip_reg_0169h.bits.r_rx_cfg_sel = 0;
        //g_rw_aip_reg_0100h.bits.r_rx_cfg_sel = 0;
        g_rw_aip_reg_0169h.bits.r_rx_n_fb_sel_bits_1_0 = 0;
        g_rw_aip_reg_016Ah.bits.r_rx_n_fb_sel_bits_2 = 0; 
        break;
    case 0: 
        break;

    default: // others / keep original config
        break;
    }
    g_rw_dpcd_reg_003Bh_LINK_BW_SET = wt_val;
}

void edp_dpcd_write_0101h_lane_cnt_set(uint8_t wt_val)
{
    g_rw_dpcd_reg_003Ch.bits.r_LANE_COUNT_SET = wt_val;
    if(g_edp_port == 0)                                 // edp port 0
    {
        if (wt_val == 1)
        {
            g_rw_aip_reg_0075h.bits.r_rx_en_lane_l01 = 1;
            g_rw_aip_reg_0075h.bits.r_rx_en_lane_l23 = 0;
        }
        else if (wt_val == 2)
        {
            g_rw_aip_reg_0075h.bits.r_rx_en_lane_l01 = 3;
            g_rw_aip_reg_0075h.bits.r_rx_en_lane_l23 = 0;   
        }
        else if (wt_val == 4)
        {
            g_rw_aip_reg_0075h.bits.r_rx_en_lane_l01 = 3;
            g_rw_aip_reg_0075h.bits.r_rx_en_lane_l23 = 3;
        }    
    }
    else if(g_edp_port == 1)                            // edp port 1
    {
        if (wt_val == 1)
        {
            g_rw_aip_reg_0075h.bits.r_rx_en_lane_l45 = 1;
            g_rw_aip_reg_0075h.bits.r_rx_en_lane_l67 = 0;
        }
        else if (wt_val == 2)
        {
            g_rw_aip_reg_0075h.bits.r_rx_en_lane_l45 = 3;
            g_rw_aip_reg_0075h.bits.r_rx_en_lane_l67 = 0;
        }
        else if (wt_val == 4)
        {
            g_rw_aip_reg_0075h.bits.r_rx_en_lane_l45 = 3;
            g_rw_aip_reg_0075h.bits.r_rx_en_lane_l67 = 3;
        }  
    }
    edp_port_cfg();         // reconfig edp port variable
}

void edp_dpcd_write_0101h_post_lt_adj_req_granted(uint8_t wt_val)
{
    g_rw_dpcd_reg_003Ch.bits.r_POST_LT_ADJ_REQ_GRANTED = wt_val;
}

void edp_dpcd_write_0101h_enh_frm_en(uint8_t wt_val)
{
    g_dpcd_ena_bits.enh_frm_en_0101h = wt_val; // peter add

    if (g_rw_dpcd_reg_000Bh.bits.r_ENHANCED_FRAME_CAP)
    {
        if (g_rw_dpcd_reg_003Ch.bits.r_ENHANCED_FRAME_EN != wt_val)
        {
            g_rw_dpcd_reg_003Ch.bits.r_ENHANCED_FRAME_EN = wt_val;
            if (epd_misc_get_edp_state() == EDP_STATE_NORMAL)
                edp_dphy_rst();
        }
    }
}

void edp_dpcd_write_0102h_trn_pat_set(uint8_t wt_val)
{
    g_edp_flag[g_edp_port].pat_set_chg = TRUE;
    g_rw_dpcd_reg_003Dh.bits.r_TRAIN_PAT_SET = wt_val;

    if ((wt_val & 0x0C) == 0x0C) // PRBS7 transmitted
    {
        g_rw_dp_0083h.bits.dpreg_prbs7_en0 = 1;
        g_rw_dp_0089h.bits.r_err_cnt_skip_symlock0 = 1;
        g_rw_dp_0083h.bits.dpreg_prbs7_en1 = 1;
        g_rw_dp_0089h.bits.r_err_cnt_skip_symlock1 = 1;
        if (g_rw_dpcd_reg_003Ch.bits.r_LANE_COUNT_SET >= 3)
        {
            g_rw_dp_0083h.bits.dpreg_prbs7_en2 = 1;
            g_rw_dp_0089h.bits.r_err_cnt_skip_symlock2 = 1;
            g_rw_dp_0083h.bits.dpreg_prbs7_en3 = 1;
            g_rw_dp_0089h.bits.r_err_cnt_skip_symlock3 = 1;
        }
    }
    else
    {
        if (g_rw_dpcd_reg_0044h.bits.r_LINK_QUAL_LANE0_SET != 3)
        {
            g_rw_dp_0083h.bits.dpreg_prbs7_en0 = 0;
            g_rw_dp_0089h.bits.r_err_cnt_skip_symlock0 = 0;
        }
        if (g_rw_dpcd_reg_0044h.bits.r_LINK_QUAL_LANE1_SET != 3)
        {
            g_rw_dp_0083h.bits.dpreg_prbs7_en1 = 0;
            g_rw_dp_0089h.bits.r_err_cnt_skip_symlock1 = 0;
        }
        if (g_rw_dpcd_reg_0045h.bits.r_LINK_QUAL_LANE2_SET != 3)
        {
            g_rw_dp_0083h.bits.dpreg_prbs7_en2 = 0;
            g_rw_dp_0089h.bits.r_err_cnt_skip_symlock2 = 0;
        }
        if (g_rw_dpcd_reg_0045h.bits.r_LINK_QUAL_LANE3_SET != 3)
        {
            g_rw_dp_0083h.bits.dpreg_prbs7_en3 = 0;
            g_rw_dp_0089h.bits.r_err_cnt_skip_symlock3 = 0;
        }
    }
#ifdef EDP1_4_FUN
    if(wt_val != 0x00)
    {
        g_rw_psr_0061h.bits.r_fw_aux_write_dpcd_00102h_bit3_0_non_zero = 1;
        g_rw_psr_0061h.bits.r_fw_aux_write_dpcd_00102h_bit3_0_non_zero = 0;
    }
#endif
}

void edp_dpcd_write_0102h_rcvr_clk_out_en(uint8_t wt_val)
{
    g_rw_dpcd_reg_003Dh.bits.r_RECOVER_CLK_OUT_EN = wt_val;
}

void edp_dpcd_write_0102h_scrm_dis(uint8_t wt_val)
{
    if (g_rw_dpcd_reg_003Dh.bits.r_SCRAMBLING_DIS != wt_val)
    {
        g_rw_dpcd_reg_003Dh.bits.r_SCRAMBLING_DIS = wt_val;
        if (epd_misc_get_edp_state() == EDP_STATE_NORMAL)
            edp_dphy_rst();
    }
}

void edp_dpcd_write_0102h_sym_err_cnt_sel(uint8_t wt_val)
{
    g_rw_dpcd_reg_003Dh.bits.r_SYMBOL_ERR_CNT_SEL = wt_val;
}

void edp_dpcd_write_0103h_0106h_trn_lane_set(uint8_t ln_idx, uint8_t wt_val)
{
    uint8_t *ptr = &g_rw_dpcd_reg_003Eh;
    *(ptr + ln_idx) = wt_val;
}

void edp_dpcd_write_0107h_spread_amp(uint8_t wt_val)
{
    g_rw_dpcd_reg_0042h.bits.r_SPREAD_AMP = wt_val;
}

void edp_dpcd_write_0107h_adaptive_sync_en(uint8_t wt_val)
{
    g_rw_dpcd_reg_0042h.bits.r_ADAPTIVE_SYNC_SDP_EN = wt_val;
}

void edp_dpcd_write_0107h_msa_ignore_en(uint8_t wt_val)
{
    g_dpcd_ena_bits.msa_tm_par_ignore_en_0107h = wt_val;
    if (g_rw_dpcd_reg_000Eh.bits.r_MSA_TIMING_PAR_IGNORED) // check capability
        g_rw_dpcd_reg_0042h.bits.r_MSA_TIMING_PAR_IGNORE_EN = wt_val;
}

void edp_dpcd_write_0108h_8b10b_set(uint8_t wt_val)
{
    g_rw_dpcd_reg_0042h.bits.r_8B10B_LINK_LAYER = wt_val;
}

void edp_dpcd_write_0109h_i2c_speed_map(uint8_t wt_val)
{
    g_rw_dpcd_reg_0043h_I2C_SPEED_CTRL = wt_val;
}

void edp_dpcd_write_010Ah_assr(uint8_t wt_val)
{
    g_dpcd_ena_bits.alt_scrmbl_rst_en_010Ah = wt_val;

    if (g_rw_dpcd_reg_0014h.bits.r_ALTERNATE_SCRAMBLER_RESET_CAPABLE) // check dpcd capability
    {
        if (g_rw_dpcd_reg_0042h.bits.r_ASSR != wt_val)
        {
            g_rw_dpcd_reg_0042h.bits.r_ASSR = wt_val;
            if (epd_misc_get_edp_state() == EDP_STATE_NORMAL)
                edp_dphy_rst(); // Eric add at 20201201
        }
    }
}

void edp_dpcd_write_010Ah_frm_chg(uint8_t wt_val)
{
    g_dpcd_ena_bits.frm_change_en_010Ah = wt_val;

    if (g_rw_dpcd_reg_0014h.bits.r_FRAMING_CHANGE_CAPABLE) // check dpcd capability / dpcd 0x0D[1]
    {
        if (g_rw_dpcd_reg_0042h.bits.r_FRAMING_CHANGE_ENABLE != wt_val)
        {
            g_rw_dpcd_reg_0042h.bits.r_FRAMING_CHANGE_ENABLE = wt_val;
            if (epd_misc_get_edp_state() == EDP_STATE_NORMAL)
                edp_dphy_rst(); // Eric add at 20201201
        }
    }
}

void edp_dpcd_write_010Ah_panel_self_test_en(uint8_t wt_val)
{
    g_dpcd_ena_bits.panel_self_test_en_010Ah = wt_val;

    if (g_rw_dpcd_reg_009Ah.bits.r_PANEL_SELF_TEST_AUX_ENABLE_CAP) // check dpcd capability / dpcd 0x701[4]
    {
        if (wt_val == 1) // AUX BIST on
        {
            g_rw_dpcd_reg_0042h.bits.r_PANEL_SELF_TEST_ENABLE = 1; // set BIST on
			g_rw_inproc_0020h.bits.r_mode2_pr = 0;
            g_rw_inproc_0020h.bits.r_mode2_r = 1;

            // turn off black video
            if (g_rw_dpcd_reg_009Eh.bits.r_BLACK_VIDEO_ENABLE) // check 0x720[1] black video enable
            {
		        if(!g_rw_dp_00AFh.bits.r_rdp_blkv_en)
                    g_rw_inproc_003Ch.bits.r_mcu_bad_fmt_or = 0;
                else
                    g_rw_daf_000Dh.bits.r_direct_rgb_en = 0;
            }
        }
        else // AUX BIST Off
        {
            // check black video mode
            if (g_rw_dpcd_reg_009Eh.bits.r_BLACK_VIDEO_ENABLE)
            {
                if(!g_rw_dp_00AFh.bits.r_rdp_blkv_en)
                    g_rw_inproc_003Ch.bits.r_mcu_bad_fmt_or = 1;   // free run mode
                else
                    edp_black_video_by_direct_rgb();
            }
            g_rw_dpcd_reg_0042h.bits.r_PANEL_SELF_TEST_ENABLE = 0; // set BIST off
            g_rw_inproc_0020h.bits.r_mode2_pr = 1;
            g_rw_inproc_0020h.bits.r_mode2_r = 0;
        }
    }
}

void edp_dpcd_write_010Bh_link_qual_lane0_set(uint8_t wt_val)
{
    g_rw_dpcd_reg_0044h.bits.r_LINK_QUAL_LANE0_SET = wt_val;
    if (wt_val == 3)
    {
        g_rw_dp_0083h.bits.dpreg_prbs7_en0 = 1;
        g_rw_dp_0089h.bits.r_err_cnt_skip_symlock0 = 1;
    }
    else
    {
        if ((g_rw_dpcd_reg_003Dh.bits.r_TRAIN_PAT_SET & 0x0C) != 0x0C) // check dpcd 0x102[3:0] / for dpcd ver1.1
        {
            g_rw_dp_0083h.bits.dpreg_prbs7_en0 = 0;
            g_rw_dp_0089h.bits.r_err_cnt_skip_symlock0 = 0;
        }
    }
}
void edp_dpcd_write_010Ch_link_qual_lane1_set(uint8_t wt_val)
{
    g_rw_dpcd_reg_0044h.bits.r_LINK_QUAL_LANE1_SET = wt_val;
    if (wt_val == 3)
    {
        g_rw_dp_0083h.bits.dpreg_prbs7_en1 = 1;
        g_rw_dp_0089h.bits.r_err_cnt_skip_symlock1 = 1;
    }
    else
    {
        if ((g_rw_dpcd_reg_003Dh.bits.r_TRAIN_PAT_SET & 0x0C) != 0x0C)
        {
            g_rw_dp_0083h.bits.dpreg_prbs7_en1 = 0;
            g_rw_dp_0089h.bits.r_err_cnt_skip_symlock1 = 0;
        }
    }
}
void edp_dpcd_write_010Dh_link_qual_lane2_set(uint8_t wt_val)
{
    g_rw_dpcd_reg_0045h.bits.r_LINK_QUAL_LANE2_SET = wt_val;
    if (wt_val == 3)
    {
        g_rw_dp_0083h.bits.dpreg_prbs7_en2 = 1;
        g_rw_dp_0089h.bits.r_err_cnt_skip_symlock2 = 1;
    }
    else
    {
        if ((g_rw_dpcd_reg_003Dh.bits.r_TRAIN_PAT_SET & 0x0C) != 0x0C)
        {
            g_rw_dp_0083h.bits.dpreg_prbs7_en2 = 0;
            g_rw_dp_0089h.bits.r_err_cnt_skip_symlock2 = 0;
        }
    }
}

void edp_dpcd_write_010Eh_link_qual_lane3_set(uint8_t wt_val)
{
    g_rw_dpcd_reg_0045h.bits.r_LINK_QUAL_LANE3_SET = wt_val;
    if (wt_val == 3)
    {
        g_rw_dp_0083h.bits.dpreg_prbs7_en3 = 1;
        g_rw_dp_0089h.bits.r_err_cnt_skip_symlock3 = 1;
    }
    else
    {
        if ((g_rw_dpcd_reg_003Dh.bits.r_TRAIN_PAT_SET & 0x0C) != 0x0C)
        {
            g_rw_dp_0083h.bits.dpreg_prbs7_en3 = 0;
            g_rw_dp_0089h.bits.r_err_cnt_skip_symlock3 = 0;
        }
    }
}
/******************************* 0x0110~0x011F *******************************************/
void edp_dpcd_write_0115h_link_rate_set(uint8_t wt_val)
{
    g_rw_dpcd_reg_0046h.bits.r_LINK_RATE_SET = wt_val;
}

void edp_dpcd_write_0115h_tx_gtc_cap(uint8_t wt_val_0, uint8_t wt_val_1)
{
    g_rw_dpcd_reg_0046h.bits.r_TX_GTC_CAP = wt_val_0;
    g_rw_dpcd_reg_0046h.bits.r_TX_GTC_SLAVE_CAP = wt_val_1;
}

void edp_dpcd_write_0116h_alpm_cfg(uint8_t wt_val)
{
    g_rw_dpcd_reg_0046h.bits.r_ALPM_Enable = wt_val & 0x01;
    g_rw_dpcd_reg_0046h.bits.r_ALPM_Lock_Error_IRQ_HPD_Enable = (wt_val >> 1) & 0x01;
    g_rw_dpcd_reg_0046h.bits.r_ALPM_Mode_Selected = (wt_val >> 2) & 0x01;
}
void edp_dpcd_write_0116h_period_of_cds_phase(uint8_t wt_val)
{
    g_rw_dpcd_reg_0047h.bits.r_Period_of_CDS_Phase = wt_val;
}

void edp_dpcd_write_0117h_aux_frm_sync_cfg(uint8_t wt_val)
{
    g_rw_dpcd_reg_0047h.bits.r_AUX_FRAME_SYNC_Enable = wt_val & 0x01;
    g_rw_dpcd_reg_0047h.bits.r_IRQ_HPD_Enable = (wt_val >> 1) & 0x01;
#ifdef EDP1_4_FUN
    if (g_rw_dpcd_reg_0047h.bits.r_AUX_FRAME_SYNC_Enable = 0)
        g_rw_psr_000Bh.bits.r_fw_rx_gtc_en = 0;
#endif
}


void edp_dpcd_write_0119h_extended_sleep_wake_timeout_period_granted(uint8_t wt_val)
{
    g_rw_dpcd_reg_0047h.bits.r_DPRX_SLEEP_WAKE_TIMEOUT_PERIOD_GRANTED = wt_val;
}

void edp_dpcd_write_011Ah_as_sdp_setup_config_pr_active(uint8_t wt_val)
{
    g_rw_dpcd_reg_0047h.bits.r_AS_SDP_SETUP_CONFIG_PR_ACTIVE = wt_val;
}
void edp_dpcd_write_011Bh_as_sdp_one_line_earlier_enable(uint8_t wt_val)
{
    g_rw_dpcd_reg_0047h.bits.r_AS_SDP_ONE_LINE_EARLIER_ENABLE = wt_val;
}
/******************************* 0x0150~0x015F *******************************************/
void edp_dpcd_write_0154h_0157h_tx_gtc_val(uint8_t ln_idx, uint8_t wt_val)
{
    switch (ln_idx)
    {
    case 0x00:
    #ifdef EDP1_4_FUN
        g_rw_psr_000Bh.bits.r_fw_rx_gtc_en = 1;
        g_rw_psr_000Bh.bits.r_fw_tx_gtc_value_valid = 0;
    #endif
        g_rw_dpcd_reg_0048h_TX_GTC_VALUE = wt_val;
        break;
    case 0x01:
        g_rw_dpcd_reg_0049h_TX_GTC_VALUE = wt_val;
        break;
    case 0x02:
        g_rw_dpcd_reg_004Ah_TX_GTC_VALUE = wt_val;
        break;
    case 0x03:
        g_rw_dpcd_reg_004Bh_TX_GTC_VALUE = wt_val;
    #ifdef EDP1_4_FUN
        g_rw_psr_000Bh.bits.r_fw_tx_gtc_value_valid = 1;
    #endif
        break;
    default:
        break;
    }
    
}

void edp_dpcd_write_0158h_rx_gtc_phase_skew_en(uint8_t wt_val)
{
    g_rw_dpcd_reg_0052h.bits.r_RX_GTC_VALUE_PHASE_SKEW_EN = wt_val;
}

void edp_dpcd_write_0159h_tx_gtc_freq_lock_done(uint8_t wt_val)
{
    g_rw_dpcd_reg_0052h.bits.r_TX_GTC_FREQ_LOCK_DONE = wt_val;
}

void edp_dpcd_write_015Ah_015Bh_tx_gtc_phase_skew(uint8_t ln_idx, uint8_t wt_val)
{
    if (ln_idx == 0x00)
        g_rw_dpcd_reg_004Ch_TX_GTC_PHASE_SKEW_OFFSET = wt_val;
    else if (ln_idx == 0x01)
        g_rw_dpcd_reg_004Dh_TX_GTC_PHASE_SKEW_OFFSET = wt_val;
}
#ifdef EDP1_4_FUN
void edp_dpcd_write_015Ch_015Fh_aux_frm_sync_val(uint8_t ln_idx, uint8_t wt_val) // not support
{
    if (ln_idx == 0x00)
    {
        g_rw_psr_000Bh.bits.r_fw_tx_afs_value_valid = 0;
        g_rw_dpcd_reg_004Eh_TX_VBE_PRIMARY_VALUE = wt_val;
    }
    else if (ln_idx == 0x01)
        g_rw_dpcd_reg_004Fh_TX_VBE_PRIMARY_VALUE = wt_val;
    else if (ln_idx == 0x02)
        g_rw_dpcd_reg_0050h_TX_VBE_PRIMARY_VALUE = wt_val;
    else if (ln_idx == 0x03)
    {
        g_rw_dpcd_reg_0051h_TX_VBE_PRIMARY_VALUE = wt_val;
        g_rw_psr_000Bh.bits.r_fw_tx_afs_value_valid = 1;
    }
}
#endif
/******************************* 0x0160~0x016F *******************************************/
void edp_dpcd_write_0x0160h_dsc_enable(uint8_t wt_val)
{
    g_rw_dpcd_reg_0052h.bits.r_DSC_ENABLE = wt_val & 0x01;
    g_rw_dpcd_reg_0052h.bits.r_DSC_Passthrough_Enable = (wt_val >> 1) & 0x01;
}
/******************************* 0x0170~0x017F *******************************************/
bool edp_dpcd_write_0170h_psr_cfg(uint8_t wt_val)
{
    // if src enable PSR when interlane_align_done = 0, reply nack
    if ((wt_val & 0x01) && (g_rw_dpcd_read_000Bh.bits.r_INTERLANE_ALIGN_DONE != 1))
        return FALSE;

    g_rw_dpcd_reg_0053h.byte = wt_val;
    return TRUE;
}
/******************************* 0x01B0~0x01BF *******************************************/
void edp_dpcd_write_01B0h_pr_cfg1(uint8_t wt_val)
{
    g_rw_dpcd_reg_0054h.byte = wt_val;
}
void edp_dpcd_write_01B1h_pr_cfg2(uint8_t wt_val)
{
    g_rw_dpcd_reg_0055h.byte = wt_val;
}
void edp_dpcd_write_01B8h_arp_config(uint8_t wt_val)
{
    g_rw_dpcd_reg_0058h.bits.r_ARP_ENABLE = wt_val & 0x01;
    g_rw_dpcd_reg_0058h.bits.r_ARP_IRQ_HPD_ENABLE = (wt_val >> 1) & 0x01;

    if (g_rw_dpcd_reg_0058h.bits.r_ARP_ENABLE)
        g_rw_dpcd_reg_00D0h.bits.r_INVALID = 0;
    else
        g_rw_dpcd_reg_00D0h.bits.r_INVALID = 1;
}
void edp_dpcd_write_01B9h_01BAh_srp_t2_max(uint8_t ln_idx, uint8_t wt_val)
{
    if (ln_idx == 0x00)
        g_rw_dpcd_reg_0056h_ARP_t2_MAX = wt_val;
    else if (ln_idx == 0x01)
        g_rw_dpcd_reg_0057h_ARP_t2_MAX = wt_val;
}
//-----------------------------------------------------------------------------------------
// write 0x0100~ 0x01FF
//-----------------------------------------------------------------------------------------
bool edp_dpcd_write_01xxh(uint8_t addr_l, uint8_t wt_val)
{

    switch (addr_l)
    {
    case 0x00:
        edp_dpcd_write_0100h_link_bw_set(wt_val);
        break;
    case 0x01:
        edp_dpcd_write_0101h_lane_cnt_set(wt_val & 0x07);                   // bit 4:0
        edp_dpcd_write_0101h_post_lt_adj_req_granted((wt_val >> 5) & 0x01); // bit 5
        edp_dpcd_write_0101h_enh_frm_en((wt_val >> 7) & 0x01);              // bit 7
        break;
    case 0x02:
        edp_dpcd_write_0102h_trn_pat_set(wt_val & 0x0F);            // bit 3:0
        edp_dpcd_write_0102h_rcvr_clk_out_en((wt_val >> 4) & 0x01); // bit 4
        edp_dpcd_write_0102h_scrm_dis((wt_val >> 5) & 0x01);        // bit 5
        edp_dpcd_write_0102h_sym_err_cnt_sel((wt_val >> 6) & 0x03); // bit 7:6
        break;
    case 0x03:
    case 0x04:
    case 0x05:
    case 0x06:
        edp_dpcd_write_0103h_0106h_trn_lane_set(addr_l - 0x03, wt_val & 0x3F);
        break;

    case 0x07:
        edp_dpcd_write_0107h_spread_amp((wt_val >> 4) & 0x01);
        edp_dpcd_write_0107h_adaptive_sync_en((wt_val >> 6) & 0x01);
        edp_dpcd_write_0107h_msa_ignore_en((wt_val >> 7) & 0x01);
        break;
    case 0x08:
        edp_dpcd_write_0108h_8b10b_set(wt_val & 0x01);
        break;
    case 0x09:
        edp_dpcd_write_0109h_i2c_speed_map(wt_val);
        break;
    case 0x0A:
        edp_dpcd_write_010Ah_assr(wt_val & 0x01);
        edp_dpcd_write_010Ah_frm_chg((wt_val >> 1) & 0x01);
        edp_dpcd_write_010Ah_panel_self_test_en((wt_val >> 7) & 0x01);
        break;
    case 0x0B:
        edp_dpcd_write_010Bh_link_qual_lane0_set(wt_val & 0x07);
        break;
    case 0x0C:
        edp_dpcd_write_010Ch_link_qual_lane1_set(wt_val & 0x7F);
        break;
    case 0x0D:
        edp_dpcd_write_010Dh_link_qual_lane2_set(wt_val & 0x7F);
        break;
    case 0x0E:
        edp_dpcd_write_010Eh_link_qual_lane3_set(wt_val & 0x7F);
        break;
    case 0x11:
    case 0x12:
    case 0x13:
    case 0x14:
        break;
    case 0x15:
        edp_dpcd_write_0115h_link_rate_set(wt_val & 0x07);
        edp_dpcd_write_0115h_tx_gtc_cap((wt_val >> 3) & 0x01, (wt_val >> 4) & 0x01); // 0x115 [3][4]
        break;
#ifdef EDP1_4_FUN
    case 0x16:
        edp_dpcd_write_0116h_alpm_cfg(wt_val & 0x07);
        edp_dpcd_write_0116h_period_of_cds_phase((wt_val >> 3) & 0x01);
        break;
    case 0x17:
        edp_dpcd_write_0117h_aux_frm_sync_cfg(wt_val & 0x03);
        break;
#endif
    case 0x18:
        //edp_dpcd_write_0118h_updream_device_dp_pwr_need(wt_val & 0x01);
        g_rw_dpcd_reg_0047h.bits.r_DP_PWR_NOT_NEEDED_BY_UPSTREAM_DEVICE = wt_val & 0x01;
        break;
    case 0x19:
        edp_dpcd_write_0119h_extended_sleep_wake_timeout_period_granted(wt_val & 0x01);
        break;
#ifdef EDP1_4_FUN
    case 0x1A:
        edp_dpcd_write_011Ah_as_sdp_setup_config_pr_active((wt_val >> 6) & 0x03);
        break;
    case 0x1B:
        edp_dpcd_write_011Bh_as_sdp_one_line_earlier_enable((wt_val >> 7) & 0x01);
        break;
#endif
    case 0x54:
    case 0x55:
    case 0x56:
    case 0x57:
        edp_dpcd_write_0154h_0157h_tx_gtc_val(addr_l - 0x54, wt_val);
        break;
    case 0x58:
        edp_dpcd_write_0158h_rx_gtc_phase_skew_en(wt_val & 0x01);
        break;
    case 0x59:
        edp_dpcd_write_0159h_tx_gtc_freq_lock_done(wt_val & 0x01);
        break;
    case 0x5A:
    case 0x5B:
        edp_dpcd_write_015Ah_015Bh_tx_gtc_phase_skew(addr_l - 0x5A, wt_val);
        break;
#ifdef EDP1_4_FUN
    case 0x5C:
    case 0x5D:
    case 0x5E:
    case 0x5F:
        edp_dpcd_write_015Ch_015Fh_aux_frm_sync_val(addr_l - 0x5C, wt_val);
        break;
#endif
    case 0x60:
        edp_dpcd_write_0x0160h_dsc_enable(wt_val & 0x03);
        break;
#ifdef EDP1_4_FUN
    case 0x70:
        if (edp_dpcd_write_0170h_psr_cfg(wt_val) == FALSE)
            return FALSE;
        break;
    case 0xB0:
        edp_dpcd_write_01B0h_pr_cfg1(wt_val);
        break;
    case 0xB1:
        edp_dpcd_write_01B1h_pr_cfg2(wt_val);
        break;
    case 0xB8:
        edp_dpcd_write_01B8h_arp_config(wt_val & 0x03);
        break;
    case 0xB9:
    case 0xBA:
        edp_dpcd_write_01B9h_01BAh_srp_t2_max(addr_l - 0xB9, wt_val);
        break;
#endif
    default:
        return FALSE;
    }

    return TRUE;
}
/******************************* 0x0200~0x020F *******************************************/
void edp_dpcd_write_020Ch_link_config_status(uint8_t wt_val)
{
    g_rw_dpcd_reg_005Ch.bits.r_Link_Rate_Set_Status = wt_val & 0x01;
    g_rw_dpcd_reg_005Ch.bits.r_Link_Rate_Set_Status_Valid = (wt_val >> 1) & 0x01;
}

void edp_dpcd_write_020Fh_dsc_status(uint8_t wt_val)
{
    g_rw_dpcd_reg_005Ch.bits.r_RC_Buffer_Under_run = wt_val & 0x01;
    g_rw_dpcd_reg_005Ch.bits.r_RC_Buffer_Overflow = (wt_val >> 1) & 0x01;
    g_rw_dpcd_reg_005Ch.bits.r_Chunk_Length_Error = (wt_val >> 2) & 0x01;
}
/******************************* 0x0240~0x024F *******************************************/
void edp_dpcd_write_0249h_phy_square_pattern(uint8_t wt_val)
{
    g_rw_dpcd_reg_0075h_PHY_SQUARE_PATTERN_num_add_1 = wt_val;
}
/******************************* 0x0250~0x025F *******************************************/
void edp_dpcd_write_025Bh_conti_80bit_pattern_from_dprx_aux_ch_ctrl(uint8_t wt_val)
{
    g_rw_dpcd_reg_0080h.bits.r_CONTINUOUS_80BIT_PATTERN_FROM_DPRX_AUX_CH_EN = wt_val & 0x01;
    g_rw_dpcd_reg_0080h.bits.r_DURATION_CTRL = (wt_val >> 1) & 0x03;
}
/******************************* 0x0260~0x026F *******************************************/
void edp_dpcd_write_0260h_test_response(uint8_t wt_val)
{
    g_rw_dpcd_reg_0080h.bits.r_TEST_ACK = wt_val & 0x01;
    g_rw_dpcd_reg_0080h.bits.r_TEST_NAK = (wt_val >> 1) & 0x01;
    g_rw_dpcd_reg_0080h.bits.r_TEST_EDID_CHECKSUM_WRITE = (wt_val >> 2) & 0x01;
}
void edp_dpcd_write_0261h_test_edid_checksum(uint8_t wt_val)
{
    g_rw_dpcd_reg_0081h_TEST_EDID_CHECKSUM = wt_val;
}

/******************************* 0x0270~0x027F *******************************************/
void edp_dpcd_write_0270h_test_sink_start(uint8_t wt_val)
{
    g_rw_dpcd_reg_0088h.bits.r_TEST_SINK_START = wt_val;
}

void edp_dpcd_write_0270h_crc_3d_option1_sel(uint8_t wt_val)
{
    g_rw_dpcd_reg_0088h.bits.r_CRC_3D_OPTION1_SELECT = wt_val;
}

void edp_dpcd_write_0270h_phy_sink_test(uint8_t test_en, uint8_t test_lane)
{
    g_rw_dpcd_reg_0088h.bits.r_PHY_SINK_TEST_LANE_EN = test_en;
    g_rw_dpcd_reg_0088h.bits.r_PHY_SINK_TEST_LANE_SEL = test_lane;
}

void edp_dpcd_write_0280h_fec_status(uint8_t wt_val)
{
    if (wt_val & 0x01)
        g_rw_dpcd_reg_0089h.bits.r_FEC_DECODE_EN_DETECTED = 0;
    if (wt_val & 0x02)
        g_rw_dpcd_reg_0089h.bits.r_FEC_DECODE_DIS_DETECTED = 0;
    if (wt_val & 0x04)
        g_rw_dpcd_reg_0089h.bits.r_FEC_RUNNING_INDICATOR = 0;
}

//-----------------------------------------------------------------------------------------
// write 0x0200~ 0x02FF
//-----------------------------------------------------------------------------------------
bool edp_dpcd_write_02xxh(uint8_t addr_l, uint8_t wt_val)
{
    switch (addr_l)
    {
    case 0x0C:
        edp_dpcd_write_020Ch_link_config_status(wt_val & 0x03);
        break;
    case 0x0F:
        edp_dpcd_write_020Fh_dsc_status(wt_val & 0x07);
        break;
    case 0x49:
        edp_dpcd_write_0249h_phy_square_pattern(wt_val);
        break;
    case 0x5B:
        edp_dpcd_write_025Bh_conti_80bit_pattern_from_dprx_aux_ch_ctrl(wt_val & 0x07);
        break;
    case 0x60:
        edp_dpcd_write_0260h_test_response(wt_val & 0x07);
        break;
    case 0x61:
        edp_dpcd_write_0261h_test_edid_checksum(wt_val);
        break;
    case 0x70:
        edp_dpcd_write_0270h_test_sink_start(wt_val & 0x01);
        edp_dpcd_write_0270h_crc_3d_option1_sel((wt_val >> 1) & 0x01);
        edp_dpcd_write_0270h_phy_sink_test((wt_val >> 7) & 0x01, (wt_val >> 4) & 0x03);
        break;
    case 0x80:
        edp_dpcd_write_0280h_fec_status(wt_val & 0x07);
        break;
    default:
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------------------
// write 0x0300~ 0x03FF
//-----------------------------------------------------------------------------------------
#ifdef INTEL_DPCD_FUN
void edp_dpcd_write_0300h_0302h_sourcer_ieee_oui(uint8_t addr_l, uint8_t wt_val)
{
    uint8_t *ptr = &g_rw_intel_dpcd_0000h_sourcer_IEEE_OUI_byte0;
    *(ptr + addr_l) = wt_val;
}
#endif
void edp_dpcd_write_0303h_030Bh_source_specific_field(uint8_t indx, uint8_t wt_val)
{
    g_edp_aux_dpcd_030xh[indx] = wt_val;
}
#ifdef INTEL_DPCD_FUN
void edp_dpcd_write_0316h_intel_private_power_control(uint8_t wt_val)
{
    g_rw_intel_dpcd_0004h_dpcd_00316h_Intel_Private_Power_Control = wt_val;

    // bit2
    if(wt_val & 0x04)
    {
        g_rw_dwo_001Ah.bits.r_gam_vrr_lut_en = 1;
        g_rw_lxlod_0000h.bits.r_lxl_vrr_en = 1;
        g_rw_os_00C8h.bits.r_vrr_chg_lut_en = 1;
    }
    else
    {
        g_rw_dwo_001Ah.bits.r_gam_vrr_lut_en = 0;
        g_rw_lxlod_0000h.bits.r_lxl_vrr_en = 0;
        g_rw_os_00C8h.bits.r_vrr_chg_lut_en = 0;
    }
}
#endif
#ifdef AMD_DPCD_FUN
void edp_dpcd_write_0320h_0325h(uint8_t indx, uint8_t wt_val)
{
    uint8_t *ptr = &g_rw_amd_dpcd_0001h_dpcd_00320h_Backlight_Level_In_MilliNits;
    *(ptr + indx) = wt_val;
}

/*
void edp_dpcd_write_032Eh_amd_backlight_control_method(uint8_t wt_val)
{
    g_rw_amd_dpcd_000Fh.bits._0032Eh_bit0_backlight_Control_Method = wt_val;
}*/

void edp_dpcd_write_032Fh_amd_backlight_enabled(uint8_t wt_val)
{
    g_rw_amd_dpcd_000Fh.bits.dpcd_0032Fh_bit0_backlight_Enabled = wt_val;
}
#endif

#ifdef INTEL_DPCD_FUN
void edp_dpcd_write_0344h_intel_edphdr_getset_ctrl_params(uint8_t wt_val)
{
    g_rw_intel_dpcd_0008h_dpcd_00344h_DPCD_INTEL_EDPHDR_GETSET_CTRL_PARAMS = wt_val;
    
    // bit4 : 
    if(wt_val & 0x10)  
    {
        g_rw_cabc_008Bh.bits.r_rdp_bl_ctrlmode_en = 2;
        g_rw_tone_0017h.bits.r_intel_aux_mode = 1;
        g_rw_cabc_0087h.bits.r_rdp_bl_en = 1;
    }
    else
    {
        g_rw_cabc_008Bh.bits.r_rdp_bl_ctrlmode_en = 0;
        g_rw_tone_0017h.bits.r_intel_aux_mode = 0;
        g_rw_cabc_0087h.bits.r_rdp_bl_en = 0;
    }
    
    // bit0, 2
    switch(wt_val & 0x05)
    {
        case 0:
            g_rw_tone_0002h.bits.r_tone_mapping_en = 0;
            g_rw_tone_0002h.bits.r_eotf_trans_en = 0;
            g_rw_tone_0002h.bits.r_tone_mapping_mode = 0;
            break;
        case 1:
            g_rw_tone_0002h.bits.r_tone_mapping_en = 1;
            g_rw_tone_0002h.bits.r_eotf_trans_en = 1;
            g_rw_tone_0002h.bits.r_tone_mapping_mode = 0;
            break;
        case 4:
            g_rw_tone_0002h.bits.r_tone_mapping_en = 1;
            g_rw_tone_0002h.bits.r_eotf_trans_en = 0;
            g_rw_tone_0002h.bits.r_tone_mapping_mode = 2;
            break;
        case 5:
            g_rw_tone_0002h.bits.r_tone_mapping_en = 1;
            g_rw_tone_0002h.bits.r_eotf_trans_en = 1;
            g_rw_tone_0002h.bits.r_tone_mapping_mode = 2;
            break;
        default:
            break;
    }
    // bit 1, 5
    switch(wt_val & 0x22)
    {
        case 0:
            g_rw_tone_0002h.bits.r_hdrgm_disable = 1;
            g_rw_hdrgm_0002h.bits.r_hdr_gm_cs = 0;
            break;
        case 0x02:
            g_rw_tone_0002h.bits.r_hdrgm_disable = 0;
            break;
        case 0x20:
            g_rw_tone_0002h.bits.r_hdrgm_disable = 0;
            g_rw_hdrgm_0002h.bits.r_hdr_gm_cs = 2;
            break;
        case 0x22:
            g_rw_tone_0002h.bits.r_hdrgm_disable = 0;
            break;
        default:
            break;
    }
    // bit 7
    if(wt_val & 0x80)
        g_rw_tone_0017h.bits.r_sdp_over_intel_aux = 1;
    else
        g_rw_tone_0017h.bits.r_sdp_over_intel_aux = 0;
}

void edp_dpcd_write_0354h_0357h_intel_edp_brightness_nits(uint8_t indx, uint8_t wt_val)
{
    uint8_t *ptr = &g_rw_intel_dpcd_000Ah_dpcd_00354h_DPCD_INTEL_EDP_BRIGHTNESS_NITS;
    *(ptr + indx) = wt_val;

    if(indx == 0x00)        // DPCD 0x0354
        g_rw_tone_0000h_manu_MaxCLL = wt_val;
    else if(indx == 0x01)   // DPCD 0x0355
        g_rw_tone_0001h_manu_MaxCLL = wt_val;
}
void edp_dpcd_write_0358h_0359h_intel_edp_brightness_optimization(uint8_t indx, uint8_t wt_val)
{
    if (indx == 0x00)   // DPCD 0x358
    {
        g_rw_intel_dpcd_000Eh_dpcd_00358h_DPCD_INTEL_EDP_BRIGHTNESS_OPTIMIZATION = wt_val;
        // bit 4
        g_rw_daf_001Fh.bits.r_optics_ac = (wt_val & 0x10) >> 4;
        // bit [7:5]
        g_rw_daf_001Fh.bits.r_optics_strength = (wt_val & 0xE0) >> 5;
    }
    else    // DPCD 0x359
        g_rw_intel_dpcd_000Fh_dpcd_00358h_DPCD_INTEL_EDP_BRIGHTNESS_OPTIMIZATION = wt_val;
}
#endif
#ifdef AMD_DPCD_FUN
void edp_dpcd_write_0371h_0372h_psr_fast_resync_point(uint8_t indx, uint8_t wt_val)
{
    if (indx == 0x00)
        g_rw_amd_dpcd_0012h_dpcd_00371h_PSR_Fast_Resync_Point = wt_val;
    else
        g_rw_amd_dpcd_0013h_dpcd_00371h_PSR_Fast_Resync_Point = wt_val;
}

void edp_dpcd_write_0376h_0377h_sink_psr_active_vtotal(uint8_t indx, uint8_t wt_val)
{
    if (indx == 0x00)
        g_rw_amd_dpcd_0017h_dpcd_00376h_Sink_Psr_Active_VTotal = wt_val;
    else
        g_rw_amd_dpcd_0018h_dpcd_00376h_Sink_Psr_Active_VTotal = wt_val;
}
#endif
#ifdef AMD_DPCD_FUN
void edp_dpcd_write_clear_0378h(uint8_t wt_val)
{
    if(wt_val & 0x02)    // bit1 / rfb_collision_status_clear
    {
        g_rw_psr_005Bh.bits.r_fw_rfb_collision_status_clear = 1;
        g_rw_psr_005Bh.bits.r_fw_rfb_collision_status_clear = 0;   
    }
}
#endif

bool edp_dpcd_write_03xxh(uint8_t addr_l, uint8_t wt_val)
{
#ifdef INTEL_DPCD_FUN
    if (addr_l < 0x03) // 0x300~0x302
        edp_dpcd_write_0300h_0302h_sourcer_ieee_oui(addr_l, wt_val);
    else if (addr_l < EDP_DPCD_030X_SRC_DEVICE_SPECIFIC_SZ) // 0x303~0x30B
#else
		if (addr_l < EDP_DPCD_030X_SRC_DEVICE_SPECIFIC_SZ) // 0x303~0x30B
#endif
        edp_dpcd_write_0303h_030Bh_source_specific_field(addr_l - 0x03, wt_val);
		
#ifdef INTEL_DPCD_FUN
    else if (addr_l == 0x16)
        edp_dpcd_write_0316h_intel_private_power_control(wt_val);
#endif
#ifdef AMD_DPCD_FUN
    else if ((addr_l >= 0x20) && (addr_l <= 0x25))
        edp_dpcd_write_0320h_0325h(addr_l - 0x20, wt_val);
    else if (addr_l == 0x2E)
        //edp_dpcd_write_032Eh_amd_backlight_control_method(wt_val & 0x01);
        g_rw_amd_dpcd_000Fh.bits.dpcd_0032Eh_bit0_backlight_Control_Method = wt_val & 0x01;
    else if (addr_l == 0x2F)
        edp_dpcd_write_032Fh_amd_backlight_enabled(wt_val & 0x01);
#endif
#ifdef INTEL_DPCD_FUN
    else if (addr_l == 0x44)
        edp_dpcd_write_0344h_intel_edphdr_getset_ctrl_params(wt_val);

    else if(addr_l == 0x45)
        g_rw_intel_dpcd_0009h_dpcd_00344h_DPCD_INTEL_EDPHDR_GETSET_CTRL_PARAMS = wt_val;

    else if ((addr_l >= 0x54) && (addr_l <= 0x57))
        edp_dpcd_write_0354h_0357h_intel_edp_brightness_nits(addr_l - 0x54, wt_val);

    else if ((addr_l == 0x58) || (addr_l == 0x59))
        edp_dpcd_write_0358h_0359h_intel_edp_brightness_optimization(addr_l - 0x58, wt_val);
#endif
#ifdef AMD_DPCD_FUN
    else if ((addr_l == 0x71) || (addr_l == 0x72))
        edp_dpcd_write_0371h_0372h_psr_fast_resync_point(addr_l - 0x71, wt_val);
    else if ((addr_l == 0x76) || (addr_l == 0x77))
        edp_dpcd_write_0376h_0377h_sink_psr_active_vtotal(addr_l - 0x76, wt_val);
    else if(addr_l == 0x78)
        edp_dpcd_write_clear_0378h(wt_val); 
#endif   

    else
        return FALSE;

    return TRUE;
}
//-----------------------------------------------------------------------------------------
// write 0x0400~ 0x04FF
//-----------------------------------------------------------------------------------------
bool edp_dpcd_write_04xxh(void)
{
    return FALSE;
}

/******************************* 0x0600~0x060F *******************************************/
void edp_dpcd_write_0600h_set_power_state(uint8_t wt_val)
{
    g_rw_dpcd_reg_0098h.byte = wt_val;
#ifdef EDP1_4_FUN
    if((wt_val & 0x07) == 0x01)
    {
        g_rw_psr_0061h.bits.r_fw_aux_write_dpcd_00600h_value_001b = 1;
        g_rw_psr_0061h.bits.r_fw_aux_write_dpcd_00600h_value_001b = 0;
    }
#endif
}
//-----------------------------------------------------------------------------------------
// write 0x0600~ 0x06FF
//-----------------------------------------------------------------------------------------
bool edp_dpcd_write_06xxh(uint8_t addr_l, uint8_t wt_val)
{
    if (addr_l == 0)
    {
        // edp_aux_dpcd_0600h = wt_val;
        edp_dpcd_write_0600h_set_power_state(wt_val & 0x07);
        return TRUE;
    }

    return FALSE;
}

/******************************* 0x0720~0x072F *******************************************/
void edp_dpcd_write_0720h_set_black_video(uint8_t wt_val)
{
    if(wt_val)   // black video on
    {
        // check bist setting
        if(!g_rw_dpcd_reg_0042h.bits.r_PANEL_SELF_TEST_ENABLE)
        {
            g_rw_dpcd_reg_009Eh.bits.r_BLACK_VIDEO_ENABLE = 1;
            // check r_black_video_set
			if(!g_rw_dp_00AFh.bits.r_rdp_blkv_en)
                g_rw_inproc_003Ch.bits.r_mcu_bad_fmt_or = 1;   // free run mode
            else
                 edp_black_video_by_direct_rgb();
        }    
    }
    else   // black video off
    {
        g_rw_dpcd_reg_009Eh.bits.r_BLACK_VIDEO_ENABLE = 0;
		if(!g_rw_dp_00AFh.bits.r_rdp_blkv_en)
            g_rw_inproc_003Ch.bits.r_mcu_bad_fmt_or = 0;
        else
            g_rw_daf_000Dh.bits.r_direct_rgb_en = 0;
    }
}

void edp_dpcd_write_0720(uint8_t val)
{
    g_rw_dpcd_reg_009Eh.byte = val;
    
    // 0720[1] / black_vieo_enable
    edp_dpcd_write_0720h_set_black_video(val & 0x02);
#ifdef CABC_FUN
    if(g_rw_cabc_007Fh.bits.r_bl_aux_en_cap)  // check 0x701[2] / cabc+0x87[7]
    {
        // 0x720[0] : backlight enable / mapping to cabc+0x87[0]
        g_rw_cabc_0087h.bits.r_rdp_bl_en = g_rw_dpcd_reg_009Eh.bits.r_BACKLIGHT_ENABLE;
    }
#endif
#ifdef EDP1_4_FUN
    if(g_rw_dpcd_reg_009Ah.bits.r_FRC_ENABLE_CAP)   // check frc_cap 0x701[5] /
    {
        // 0x720[2] : frc enable / mapping to daf+0x00[4]
        g_rw_daf_0000h.bits.r_frc_en = g_rw_dpcd_reg_009Eh.bits.r_FRC_ENABLE;
    }
#endif
    if(g_rw_dpcd_reg_009Ah.bits.r_COLOR_ENGINE_CAP)   // check cm cap / 0x701[6] 
    {
        // 0x720[3] : color engine enable / mapping to daf+0x1E[2] 
        g_rw_daf_001Eh.bits.r_c3d_en =  g_rw_dpcd_reg_009Eh.bits.r_COLOR_ENGINE_ENABLE;
    }
#ifdef CABC_FUN
    if(g_rw_cabc_0080h.bits.r_vblank_bl_update_cap)   // check r_vblank_bl_update_cap / 0x702[7] / cabc+0x80[7] 
    {
        // 0x720[7] : vblank backlight update enable / mapping to cabc+0x87[2]
        g_rw_cabc_0087h.bits.r_rdp_blsync2vs_en = g_rw_dpcd_reg_009Eh.bits.r_VBLANK_BACKLIGHT_UPDATE_ENABLE;
    }
#endif
}

void edp_dpcd_write_0721(uint8_t val)
{
    g_rw_dpcd_reg_009Fh.byte = val;
#ifdef CABC_FUN
    // 0x721[1:0] mapping to cabc+0x8B[1:0]
    g_rw_cabc_008Bh.bits.r_rdp_bl_ctrlmode_en = g_rw_dpcd_reg_009Fh.bits.r_BL_CTL_MODE;

    if(g_rw_cabc_0080h.bits.r_bl_freq_pwm_pinpassth_cap)   // check r_bl_freq_pwm_pinpassth_cap / cabc+0x80[4] / 0x720[4]
    {
        // 0x721[2] : backlight freq pwm pin pass through enable / mapping to cabc+0x87[3]
        g_rw_cabc_0087h.bits.r_rdp_pwm_pin_passthru_en = g_rw_dpcd_reg_009Fh.bits.r_BL_FREQ_PWM_PIN_PASSTHRU_EN;
    }

    if(g_rw_cabc_0080h.bits.r_bl_freq_aux_set_cap)   // check r_bl_freq_aux_set_cap / cabc+0x80[5]
    {
        // 0x721[3] : backlight freq aux set enable / mapping to cabc+0x87[4]
        g_rw_cabc_0087h.bits.r_rdp_bl_freq_set_en = g_rw_dpcd_reg_009Fh.bits.r_BL_FREQ_AUX_SET_EN;
    }

    if(g_rw_cabc_0080h.bits.r_dyn_bl_cap)   // check r_dyn_bl_cap / cabc+0x80[6] / 0x720[5]
    {
        // 0x721[4] : dynamic backlight enable / mapping to cabc+0x87[5]
        g_rw_cabc_0087h.bits.r_rdp_bl_dbc_en = g_rw_dpcd_reg_009Fh.bits.r_DYN_BL_EN;
    }
#endif
}

void edp_dpcd_write_0722(uint8_t val)
{
#ifdef CABC_FUN
    //*(volatile char xdata *)(g_edp_cabc_start_addr + 0x88) = val;
    g_rw_cabc_0088h_rdp_bl_brmsb = val; 
#endif
    g_rw_dpcd_reg_00A0h_EDP_BACKLIGHT_BRIGHTNESS_MSB = val;
}

void edp_dpcd_write_0723(uint8_t val)
{
#ifdef CABC_FUN
    //*(volatile char xdata *)(g_edp_cabc_start_addr + 0x89) = val;
    g_rw_cabc_0089h_rdp_bl_brlsb = val;
#endif 
    g_rw_dpcd_reg_00A1h_EDP_BACKLIGHT_BRIGHTNESS_LSB = val;
}

void edp_dpcd_write_0724(uint8_t val)
{
#ifdef CABC_FUN
    // 0x721[4:0] mapping to cabc+8B[6:2]
    g_rw_cabc_008Bh.bits.r_rdp_pwmgen_res = val & 0x1F;
#endif
    g_rw_dpcd_reg_00A2h.byte = val;
}

void edp_dpcd_write_0728(uint8_t val)
{
#ifdef CABC_FUN
    //*(volatile char xdata *)(g_edp_cabc_start_addr + 0x8A) = val;
    // 0x728 mapping to cabc+8A
    g_rw_cabc_008Ah_rdp_pwmgen_freq = val;
#endif
    g_rw_dpcd_reg_00A5h_EDP_BACKLIGHT_FREQ_SET = val;
}

bool edp_dpcd_write_0720h_072Fh(uint8_t addr_ll, uint8_t wt_val)
{
    switch (addr_ll)
    {
    case 0x00:
        edp_dpcd_write_0720(wt_val & 0x8F);
        break;
    case 0x01:
        edp_dpcd_write_0721(wt_val & 0x1F);
        break;
    case 0x02:
        edp_dpcd_write_0722(wt_val);
        break;
    case 0x03:
        edp_dpcd_write_0723(wt_val);
        break;
    case 0x04:
        edp_dpcd_write_0724(wt_val & 0x1F);
        break;
    case 0x08:
        edp_dpcd_write_0728(wt_val);
        break;

    default:
        return 0;
    }
    return 1;
}
/******************************* 0x0730~0x073F *******************************************/
void edp_dpcd_write_0732(uint8_t val)
{
#ifdef CABC_FUN
    // 0x732[0] mapping to cabc+8B[7] / 0x732[4:1] mapping to c
    g_rw_cabc_008Bh.bits.r_rdp_dbc_brmin_bits_0 = val & 0x01;
    g_rw_cabc_008Ch.bits.r_rdp_dbc_brmin_bits_4_1 = (val & 0x1E) >> 1;
#endif
    g_rw_dpcd_reg_00ACh.byte = val;

}

void edp_dpcd_write_0733(uint8_t val)
{
#ifdef CABC_FUN
    // 0x733[3:0] mapping to cabc+8C[7:4] /  0x733[4] mapping to cabc+8D[0]
    g_rw_cabc_008Ch.bits.r_rdp_dbc_brmax_bits_3_0 = val & 0x0F;
    g_rw_cabc_008Dh.bits.r_rdp_dbc_brmax_bits_4 = (val & 0x10) >> 4; 
#endif
    g_rw_dpcd_reg_00ADh.byte = val;

}
/******************************* 0x0740~0x074F *******************************************/
void edp_dpcd_write_0740h_074Fh(uint8_t addr_ll, uint8_t wt_val)
{
    uint8_t *ptr = &g_rw_dpcd_reg_00AEh_REGION_INDEX_OFFSET;
    *(ptr + addr_ll) = wt_val;
}

//-----------------------------------------------------------------------------------------
// write 0x0700~ 0x07FF
//-----------------------------------------------------------------------------------------
bool edp_dpcd_write_07xxh(uint8_t addr_l, uint8_t wt_val)
{
    uint8_t addr_lh, addr_ll;

    addr_lh = addr_l & 0xF0;
    addr_ll = addr_l & 0x0F;

    if (addr_lh == 0x20)
    {
        if (addr_ll == 9)
            return FALSE;
        else
        {
            if (edp_dpcd_write_0720h_072Fh(addr_ll, wt_val))
                return TRUE;
            else
                return FALSE;
        }
    }
    else if (addr_l == 0x32)
    {
        edp_dpcd_write_0732(wt_val);
        return TRUE;
    }
    else if (addr_l == 0x33)
    {
        edp_dpcd_write_0733(wt_val);
        return TRUE;
    }
#ifdef EDP1_4_FUN
    else if(addr_lh == 0x40)
    {
        edp_dpcd_write_0740h_074Fh(addr_ll, wt_val);
        return TRUE;
    }
#endif
    return FALSE;
}
/******************************* 0x2000~0x20FF *******************************************/
#ifdef EDP1_4_FUN
void edp_dpcd_write_clear_2004(uint8_t val)
{
    if(val & 0x01)
    {
        g_rw_psr_005Bh.bits.r_fw_RX_GTC_MSTR_REQ_STATUS_CHANGE_clear = 1;
        g_rw_psr_005Bh.bits.r_fw_RX_GTC_MSTR_REQ_STATUS_CHANGE_clear = 0;
    }
    if(val & 0x02)
    {
        g_rw_psr_005Bh.bits.r_fw_LOCK_ACQUISITION_REQUEST_clear = 1;
        g_rw_psr_005Bh.bits.r_fw_LOCK_ACQUISITION_REQUEST_clear = 0;
    }
    if(val & 0x08)
    {
        g_rw_psr_005Bh.bits.r_fw_PANEL_REPLAY_ERROR_STATUS_clear = 1;
        g_rw_psr_005Bh.bits.r_fw_PANEL_REPLAY_ERROR_STATUS_clear = 0;
    }
}
void edp_dpcd_write_clear_2006(uint8_t val)
{
    if(val & 0x01)   // bit0
    {
        g_rw_psr_005Bh.bits.r_fw_psr_link_crc_error_clear = 1;
        g_rw_psr_005Bh.bits.r_fw_psr_link_crc_error_clear = 0;
    }
    if(val & 0x02)   // bit1
    {
        g_rw_psr_005Bh.bits.r_fw_psr_rfb_storage_error_clear = 1;
        g_rw_psr_005Bh.bits.r_fw_psr_rfb_storage_error_clear = 0;
    }
    if(val & 0x04)   // bit2
    {
        g_rw_psr_005Bh.bits.r_fw_psr2_vsc_sdp_uncorrectable_error_clear = 1;
        g_rw_psr_005Bh.bits.r_fw_psr2_vsc_sdp_uncorrectable_error_clear = 0;
    }

}
void edp_dpcd_write_clear_2007(uint8_t val)
{
    if (val & 0x01)
    {
        g_rw_psr_005Bh.bits.r_fw_sink_device_su_psr_capability_change_clear = 1;
        g_rw_psr_005Bh.bits.r_fw_sink_device_su_psr_capability_change_clear = 0;
    }
}

void edp_dpcd_write_clear_200B(uint8_t val)
{
    if (val & 0x01)
    {
        g_rw_psr_005Ch.bits.r_fw_aux_wake_alpm_lock_timout_error_status_clear = 1;
        g_rw_psr_005Ch.bits.r_fw_aux_wake_alpm_lock_timout_error_status_clear = 0;
    }
    if(val & 0x02)
    {
        g_rw_psr_005Ch.bits.r_fw_arp_screen_refresh_irq_clear = 1;
        g_rw_psr_005Ch.bits.r_fw_arp_screen_refresh_irq_clear = 0;
    }
    if(val & 0x04)
    {
        g_rw_psr_005Ch.bits.r_fw_aux_less_alpm_lock_timout_error_status_clear = 1;
        g_rw_psr_005Ch.bits.r_fw_aux_less_alpm_lock_timout_error_status_clear = 0;
    }
}

void edp_dpcd_write_clear_2010(uint8_t val)
{
    if(val & 0x01)   // bit0
    {
        g_rw_psr_005Ch.bits.r_fw_aux_frame_sync_lock_error_clear = 1;
        g_rw_psr_005Ch.bits.r_fw_aux_frame_sync_lock_error_clear = 0;
    }
}

void edp_dpcd_write_clear_2020(uint8_t val)
{
    if(val & 0x01)   // bit0
    {
        g_rw_psr_005Ch.bits.r_fw_PR_ACTIVE_FRAME_CRC_ERROR_clear = 1;
        g_rw_psr_005Ch.bits.r_fw_PR_ACTIVE_FRAME_CRC_ERROR_clear = 0;
    }
    if(val & 0x02)   // bit1
    {
        g_rw_psr_005Ch.bits.r_fw_PR_RFB_STORAGE_ERROR_clear = 1;
        g_rw_psr_005Ch.bits.r_fw_PR_RFB_STORAGE_ERROR_clear = 0;
    }
    if(val & 0x04)   // bit2
    {
        g_rw_psr_005Ch.bits.r_fw_PR_VSC_SDP_UNCORRECTABLE_ERROR_clear = 1;
        g_rw_psr_005Ch.bits.r_fw_PR_VSC_SDP_UNCORRECTABLE_ERROR_clear = 0;
    }
    if(val & 0x08)   // bit3
    {
        g_rw_psr_005Ch.bits.r_fw_ADAPTIVE_SYNC_SDP_MISSING_AND_NOT_DISABLED_clear = 1;
        g_rw_psr_005Ch.bits.r_fw_ADAPTIVE_SYNC_SDP_MISSING_AND_NOT_DISABLED_clear = 0;
    }
}
#endif
#ifdef EDP1_4_FUN
bool edp_dpcd_write_20xxh(uint8_t addr_l, uint8_t wt_val)
{
    switch(addr_l)
    {
        case 0x04:
            edp_dpcd_write_clear_2004(wt_val);
            break;
        case 0x06:
            edp_dpcd_write_clear_2006(wt_val);
            break;
        case 0x07:
            edp_dpcd_write_clear_2007(wt_val);
            break;
        case 0x0B:
            edp_dpcd_write_clear_200B(wt_val);
            break;
        case 0x10:
            edp_dpcd_write_clear_2010(wt_val);
            break;
        case 0x20:
            edp_dpcd_write_clear_2020(wt_val);
            break;            
        default :
            return FALSE;
    }
    return TRUE;
}
#endif
/******************************* 0x2200~0x22FF *******************************************/
bool edp_dpcd_write_22xxh(void)
{
    return FALSE;
}

/******************************* 0x3050~0x305F *******************************************/
void edp_dpcd_write_3054h_3057h_output_htotal_hstart(uint8_t offset, uint8_t wt_val)
{
    if (offset == 0x00)
        g_rw_dpcd_reg_00DBh_OUTPUT_HTOTAL = wt_val;
    else if (offset == 0x01)
        g_rw_dpcd_reg_00DCh_OUTPUT_HTOTAL = wt_val;
    else if (offset == 0x02)
        g_rw_dpcd_reg_00DDh_OUTPUT_HSTART = wt_val;
    else if (offset == 0x03)
        g_rw_dpcd_reg_00DEh_OUTPUT_HSTART = wt_val;
}
void edp_dpcd_write_3058h_3059h_output_hsp(uint8_t offset, uint8_t wt_val)
{
    if (offset == 0x00)
        g_rw_dpcd_reg_00DFh_OUTPUT_HSP_HSW = wt_val;
    else
        g_rw_dpcd_reg_00E0h.byte = wt_val;
}

bool edp_dpcd_write_30xxh(uint8_t addr_l, uint8_t wt_val)
{
    switch (addr_l)
    {
    case 0x54:
    case 0x55:
    case 0x56:
    case 0x57:
        edp_dpcd_write_3054h_3057h_output_htotal_hstart(addr_l - 0x54, wt_val);
        break;
    case 0x58:
    case 0x59:
        edp_dpcd_write_3058h_3059h_output_hsp(addr_l - 0x58, wt_val);
        break;
    default:
        return FALSE;
    }
    return TRUE;
}

bool edp_dpcd_write(uint8_t addr_m, uint8_t addr_l, uint8_t wt_val)
{
    switch (addr_m)
    {
    case 0x00:
        return edp_dpcd_write_00xxh(addr_l);
    case 0x01:
        return edp_dpcd_write_01xxh(addr_l, wt_val);
    case 0x02:
        return edp_dpcd_write_02xxh(addr_l, wt_val);
    case 0x03:
        return edp_dpcd_write_03xxh(addr_l, wt_val);
    case 0x04:
        return edp_dpcd_write_04xxh();
    case 0x06:
        return edp_dpcd_write_06xxh(addr_l, wt_val);
    case 0x07:
        return edp_dpcd_write_07xxh(addr_l, wt_val);
#ifdef EDP1_4_FUN
    case 0x20:
        return edp_dpcd_write_20xxh(addr_l, wt_val);
#endif
    case 0x22:
        return edp_dpcd_write_22xxh();
    case 0x30:
        return edp_dpcd_write_30xxh(addr_l, wt_val);
    default:
        break;
    }

    return FALSE;
}
#endif
