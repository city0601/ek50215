#ifndef __EDP_GLOBAL_VARS_H__
#define __EDP_GLOBAL_VARS_H__
#ifdef EDP_FUN
#include "mcu_global_vars.h"
#include "edp_const.h"
#include "edp_macro.h"

#define RW_FROM_EDID            1
#define RW_FROM_DVCOM_PMIC      2
#define I2C_ONE_BYTE_MODE       0
#define I2C_TWO_BYTE_MODE       1

#define EDP_PORT_NUM            2       // 2 port edp 

#define EDP_PORT_0              0
#define EDP_PORT_1              1

#define EDP_AUX_BUF_SZ (32)
//#define EDP_AUX_EDID_SZ (256)
#define EDP_AUX_EDID_SZ (318)     // modified to 3-128 bytes / need more check

#define EDP_AUX_DPCD_000XH_SZ (15)
#define EDP_AUX_DPCD_001XH_SZ (16)
#define EDP_AUX_DPCD_003XH_SZ (16)
#define EDP_AUX_DPCD_0054H_SZ (6)
#define EDP_AUX_DPCD_006XH_SZ (16)
#define EDP_AUX_DPCD_007XH_SZ (5)
#define EDP_AUX_DPCD_010XH_SZ (15)
#define EDP_AUX_DPCD_0154H_SZ (12)
#define EDP_AUX_DPCD_0240H_SZ (6)
//#define EDP_AUX_DPCD_030XH_SZ (12)
#define EDP_AUX_DPCD_040XH_SZ (12)
#define EDP_AUX_DPCD_070XH_SZ (4)
#define EDP_AUX_DPCD_0720H_SZ (5)
#define EDP_AUX_DPCD_0725H_SZ (2)
#define EDP_AUX_DPCD_072AH_SZ (6)
#define EDP_AUX_DPCD_2004H_SZ (8)

#define EDP_DPCD_030X_SRC_DEVICE_SPECIFIC_SZ (9) // 0x0303~0x030B

#define DBG_DO_CHEAT_LINK_STATUS
//---------------------------------------------------------------------
extern volatile uint8_t xdata edp_aux_tx_bytes[EDP_AUX_BUF_SZ];
extern volatile uint8_t xdata edp_aux_rx_bytes[EDP_AUX_BUF_SZ];
//---------------------------------------------------------------------
typedef struct 
{
    uint8_t enh_frm_en_0101h                 : 1;
    uint8_t msa_tm_par_ignore_en_0107h       : 1;
    uint8_t panel_self_test_en_010Ah         : 1;
    uint8_t frm_change_en_010Ah              : 1;
    uint8_t alt_scrmbl_rst_en_010Ah          : 1;
    uint8_t bl_en_0720h                      : 1;
    uint8_t frc_en_0720h                     : 1;
    uint8_t color_engine_en_0720h            : 1;
    uint8_t vblk_bl_updt_en_0720h            : 1;
    uint8_t bl_frq_pwm_pin_passthru_en_0721h : 1;
    uint8_t bl_frq_aux_set_en_0721h          : 1;
    uint8_t dynamic_bl_en_0721h              : 1;
}dpcd_ena_bits;

extern volatile dpcd_ena_bits xdata g_dpcd_ena_bits;
//---------------------------------------------------------------------
extern uint8_t edp_aux_in_defer[EDP_PORT_NUM];
extern uint8_t edp_ioa_mode_en_flag[EDP_PORT_NUM]; 

extern uint8_t xdata g_edp_port_max; 
extern uint8_t xdata g_edp_port_type;
extern uint8_t xdata g_edp_port_total_using;

extern uint8_t xdata g_edp_port;
extern uint8_t xdata g_edp_multiport_iadone_sync_tmcnt;
extern bool g_edp_flag_allport_iadone;
extern bool g_edp_multiport_iadone_sync_start_timeout;
//---------------------------------------------------------------------
extern uint8_t xdata g_edp_i2c_rw_addr[EDP_PORT_NUM];
extern uint8_t xdata g_edp_aux_hpd_cnt[EDP_PORT_NUM];
extern uint8_t xdata g_edp_aux_type;
extern uint8_t xdata g_edp_aux_mot;
extern uint8_t xdata g_edp_aux_req;
extern uint8_t xdata g_edp_aux_last_i2c[EDP_PORT_NUM];
extern uint8_t xdata g_edp_aux_last_i2c_addr[EDP_PORT_NUM];
extern uint8_t xdata g_edp_aux_wr_byte_idx[EDP_PORT_NUM];
extern uint8_t xdata g_edp_aux_wr_byte_cnt[EDP_PORT_NUM];
extern uint8_t xdata g_edp_aux_defer_wr_st[EDP_PORT_NUM];
extern uint8_t xdata g_edp_aux_defer_wr_cnt[EDP_PORT_NUM];
extern uint8_t xdata g_edp_aux_defer_wr_idx[EDP_PORT_NUM];
//---------------------------------------------------------------------
extern uint8_t xdata g_edp_aux_edid_segment_addr;
extern uint8_t xdata g_edp_aux_i2c_en_addr;
extern uint8_t xdata g_edp_ioa_pmic_addr;
extern uint8_t xdata g_edp_ioa_dvcom_addr;
extern uint8_t xdata g_edp_ioa_ls1_addr;
extern uint8_t xdata g_edp_ioa_ls2_addr;
extern uint8_t xdata g_edp_ioa_gma_addr;
extern uint8_t xdata g_edp_ioa_edid_addr;
extern uint8_t xdata g_edp_ioa_cfg_addr;
extern uint8_t xdata g_edp_aux_edid_slave_addr;
extern uint8_t xdata g_edp_aux_edid_offset;
extern uint8_t xdata g_edp_ioa_spi_flash_addr;
extern uint8_t xdata g_edp_ioa_tcon_reg_addr;
//---------------------------------------------------------------------
extern uint16_t xdata g_edp_timeout_cnt[EDP_PORT_NUM];
extern uint16_t xdata g_edp_chk_timeout_cnt[EDP_PORT_NUM];

extern uint16_t xdata g_edp_frc_start_addr;
extern uint16_t xdata g_edp_cm_start_addr;
extern uint16_t xdata g_edp_cabc_start_addr;
//---------------------------------------------------------------------
extern uint8_t idata g_edp_aux_bytes[EDP_AUX_BYTES_SZ];
//---------------------------------------------------------------------
extern uint16_t xdata g_no_lnk_trn_rst_chk_target;
extern uint16_t xdata g_no_lnk_trn_rst_double_chk_target;
extern uint16_t xdata g_edp_err_cnt_thd_target;
extern uint16_t xdata g_edp_aux_reply_timeout_target;

extern uint8_t xdata g_edp_polling_time_irq;
//---------------------------------------------------------------------
/*union hw_dbg_cfg_1      // 0x4800
{
    unsigned char byte;
    struct 
    {
        unsigned char r_ip_dbgo_sel           : 5;
        unsigned char reserved_0              : 3;
    } bits;    
};
extern volatile union hw_dbg_cfg_1 xdata g_hw_dbg_cfg_1_u;*/
//---------------------------------------------------------------------
/*union hw_dbg_cfg_2      // 0x4802
{
    unsigned char byte;
    struct 
    {
        unsigned char r_p2p_dbgo_sel          : 6;
        unsigned char reserved_0              : 2;
    } bits;    
};
extern volatile union hw_dbg_cfg_2 xdata g_hw_dbg_cfg_2_u;*/
//---------------------------------------------------------------------
union hw_hit_cfg        // 0x4D00
{
    unsigned char byte;
    struct 
    {
        unsigned char reserved_0              : 1;
        unsigned char pdi_smart_cs            : 1;
        unsigned char sig_1                   : 1;
        unsigned char sig_2                   : 1;
        unsigned char sig_3                   : 1;
        unsigned char reserved_1              : 3;
    } bits;    
};
//extern volatile union hw_hit_cfg xdata g_hw_hit_cfg_u;
//extern volatile union hw_hit_cfg xdata g_hw_hit_cfg_r_u; 

extern uint8_t xdata g_edp_aux_dpcd_030xh[EDP_DPCD_030X_SRC_DEVICE_SPECIFIC_SZ];
//---------------------------------------------------------------------
struct edp_flag_t
{
    uint8_t dbg_hpd_cnt         : 1;
    uint8_t in_reset            : 1;   
    uint8_t pat_set_chg         : 1;   
    uint8_t timeout_go_err_chk  : 1;   
    uint8_t timeout_go_phy_rst  : 1;   
    uint8_t timeout_go_chk_crd  : 1;   
    uint8_t err_cnt_latch_flg   : 1;  
    uint8_t reserved            : 1; 
};

extern volatile struct edp_flag_t xdata g_edp_flag[EDP_PORT_NUM];

//extern bool g_master_flg;
//extern bool g_cascade_tx_flg;
#endif
#endif
