#ifndef __EDP_AUX_H__
#define __EDP_AUX_H__

//#include "edp_global_vars.h"
#include "reg_include.h"

extern void edp_aux_proc(uint8_t port);
void edp_aux_handler(uint8_t port);
void edp_aux_i2c_defer_handler(uint8_t port);
bool edp_chk_aux_seq_abnormal(void);
void edp_aux_reply_aux_defer(void);
//void edp_aux_edid_set_offset(uint8_t off);
//void edp_aux_edid_write(uint8_t w_val);
uint8_t edp_aux_edid_read(void);
uint8_t cal_wr_byte_cnt(void);
#endif

