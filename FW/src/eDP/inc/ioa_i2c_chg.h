#ifndef __IOA_I2C_CHG_H__
#define __IOA_I2C_CHG_H__

#include "reg_include.h"
#include "edp_global_vars.h"
extern uint8_t xdata g_ioa_spcf_rx_device_addr[EDP_PORT_NUM];              // ioa specific slv addr
extern uint8_t xdata g_ioa_spc_mode[EDP_PORT_NUM];
extern uint8_t xdata g_ioa_spcf_i2c_rw_addr[EDP_PORT_NUM]; 

void ioa_i2c_dev_chg_init(uint8_t port);
void ioa_i2c_dev_chg(uint8_t port);
uint8_t ioa_i2c_chg_check_adr(uint8_t aux_rx_i2c,uint8_t port);
#endif