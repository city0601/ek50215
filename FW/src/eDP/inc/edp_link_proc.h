#ifndef __EDP_LINK_PROC_H__
#define __EDP_LINK_PROC_H__

//#include "edp_global_vars.h"
#include "reg_include.h"

//extern uint8_t xdata g_edp_cks_err_flg;

void edp_link_proc(uint8_t port);
void edp_error_handling(void);

#endif
