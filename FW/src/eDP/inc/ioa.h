#ifndef __IOA_H__
#define __IOA_H__

#include "reg_include.h"
#include "edp_global_vars.h"

void mcu_ioa_init(void);
bool check_ioa_cfg(uint8_t addr, uint8_t port);
void ioa_unlock_handler(uint8_t port); 
#endif
