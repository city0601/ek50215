#ifndef __EDP_MISC_H__
#define __EDP_MISC_H__

#include "edp_global_vars.h"
#include "reg_include.h"

void edp_enable_dpcd_reg_remap(uint8_t val);
void edp_dpcd_reg_port_sel(uint8_t port);

void edp_init(void);
void edp_do_init_rst(void);
bool edp_chk_rst_done(void);

void edp_do_trn_phs1_state_init(void);
//void edp_do_trn_phs2_state_init(void);
void edp_do_normal_state_init(uint8_t port);
void edp_do_no_trn_state_init(uint8_t port);
//void edp_do_qual_test_state_init(void);

uint16_t edp_get_err_cnt(void);
void edp_assert_irq_hpd(uint8_t port);
void edp_check_irq_hpd(uint8_t port);
bool edp_link_status_ok(void);
bool edp_link_status_sel_ok(void);                  // eric add at 20210115
void edp_do_phy_rst(void);
void edp_dphy_rst(void);
//void edp_get_dpcd_frc_cabc_offset(void);
void edp_get_eeprom_cfg(void);
void edp_checkevent(void);
void edp_dpcd_err_cnt_latch(void);                  // Eric add at 20201228
void edp_polling_err_cnt_latch(void);               // Eric add at 20210106
void edp_get_eeprom_mcu_cfg(void);
bool edp_crd_status(void);
//void edp_isp_proc(void);
bool edp_check_link_sta(void);
void edp_i2cbus_mux(bool i2c_path); 
void tcon_aux_tx_bug_fix(uint8_t val);
uint8_t epd_misc_get_edp_state(void);
uint8_t epd_misc_get_edp_port1_state(void);
void edp_misc_set_edp_state(uint8_t sta);
void edp_set_hdp(bool hpd_val);

extern uint8_t edp_check_did_vii_detailed_timing_hvtotal(void);
extern void edp_sharp_lrd_advance_pol_proc(void);

//extern bool g_hpd_irq_flg;
extern bool g_hpd_irq_port0_flg;
extern bool g_hpd_irq_port1_flg;
extern bool g_psr_irq_flg;
extern bool g_rsv_irq_flg;
extern bool g_intel_ubrr_irq_flag;

#ifdef EDP1_4_FUN
extern void edp_intel_ubrr_irq_handler(void);
extern void edp_intel_ubrr_vsync_handler(void);
extern void edp_store_psr_capability(void);
extern void edp_check_psr_capability_handler(void);
extern void edp_amd_hdr_handler(void);
#endif

extern void edp_sharp_lrd_touch_sensing_en_proc(void);
extern void edp_multiport_sync_handler(void);
extern void edp_multiport_iadone_sync_timeout_handler(void);
extern void edp_port_cfg(void);

#ifdef CASCADE_EDP_FUN
extern void edp_cascade_master_tx_handler(void);
extern void edp_cascade_master_rx_handler(void);
//extern void edp_cascade_slave_tx_handler(void);
//extern void edp_cascade_slave_rx_handler(void);
extern void edp_cascade_rx_handler(void);
#endif

#endif
