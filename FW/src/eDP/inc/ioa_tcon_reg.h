#ifndef __IOA_TCON_REG_H__
#define __IOA_TCON_REG_H__

//#include "edp_global_vars.h"
//#include "reg_include.h"

extern void ioa_tcon_reg_init(uint8_t port);
extern void edp_tcon_reg_over_aux(uint8_t port);


#define EDP_AUX_LAST_TCON_IDLE              (0x00)
#define EDP_AUX_LAST_TCON_WRITE             (0x01)
#define EDP_AUX_LAST_TCON_READ              (0x02)

#endif