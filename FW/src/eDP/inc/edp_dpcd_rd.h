#ifndef __EDP_DPCD_RD_H__
#define __EDP_DPCD_RD_H__

#include "edp_global_vars.h"

uint8_t edp_dpcd_read(uint8_t addr_h, uint8_t addr_m, uint8_t addr_l);

#endif
