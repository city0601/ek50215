/*
		Copyright 2021 FITIPOWER., Ltd.
		Module: N/A
		Project: eDP
		Platform: 8051
		Author: Eric Lee, 2021/03/23
		Description: main
*/
// *******************************************************************
// *******************************************************************
// entry_xx_finfail_en = 1 , entry_xx_finfail_idx = 1 --> sys finish
// entry_xx_finfail_en = 1 , entry_xx_finfail_idx = 2 --> reg finish
// entry_xx_finfail_en = 1 , entry_xx_finfail_idx = 3 --> lut finish
// *******************************************************************
// *******************************************************************
// Include Path
#include "mcu_fun_def.h"
#include "reg_include.h"
#include "mcu_misc.h"
#include "mcu_dl.h"
#include "mcu_int.h"
#include "mcu_i2c.h"
#include "mcu_uart.h"
//#include "mcu_spi.h"
#include "tcon_i2c_proc.h"
#include "tcon_global_include.h"
#include "tcon_isp.h"
#include "tcon_misc.h"
//#include "tcon_cascade.h"
#include "mcu_dma.h"
#ifdef DEMURA_TRANS_FUN
#include "tcon_lut_transfer.h"
#endif
#ifdef DEMURA_CHK_FUN
#include "tcon_lut_transfer.h"
#endif
#ifdef CSOT_FUN
    #include "csot_global_include.h"
#endif
#ifdef PLL_REMAP_FUN
    #include "tcon_pll_remap.h"
#endif 
#ifdef EDP_FUN
    #include "ioa.h"
    #include "edp_misc.h"
	#include "edp_aux.h"
	#include "edp_global_vars.h"
#endif
#ifdef VD_TRANS_FUN
    #include "tcon_lut_transfer_vd.h"	
#endif

#ifdef LD_LED_DRV_FUN
    #include "tcon_spi_leddrv.h"
#endif
//extern volatile bit  data     DEVICE_MODE; // Variable with current device status
//uint32_t xdata g_spi_test_addr_offset = 0x1000;
//uint8_t xdata g_test_spi_buf[256];
//#define I2C_MASTER_TEST
#ifdef I2C_MASTER_TEST
uint8_t xdata g_i2c_mst_reg[2];
uint8_t xdata g_i2c_mst_rbuf[16] = {0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x10};
#endif
//#define HW_CSOT_CRC_TEST
#ifdef HW_CSOT_CRC_TEST
uint8_t g_res_flg = 0;
#endif
//---------------------------------------------------------------------
static void tcon_init(void)
{
    tcon_i2c_proc_init();
#ifdef CSOT_FUN
    g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;   // slave to secondary side
    tcon_CSOT_I2c_Slv_Init();     
#endif
#ifdef LUT_SRW_FUN
    g_lut_i2c_read_reg_ptr = 0;
    g_lut_i2c_slv_flag.byte = 0x00;
#endif
}
/*void mcu_check_2k_eeprom(void)
{
    if(EXTDAT_SFR_2BH & 0x08)
	{
		mcu_entry_dl_hdr();
	}
}*/

void tcon_det_lut_i2c_adr(void)
{
    if(!g_rw_mcu_top_0003h.bits.reserved1)
        return;
    mcu_i2c_sec_addr_2 = (g_rw_mcu_top_0037h.bits.r_lut_srw_dev << 1);
}

void main(void)
{
	mcu_init();
    mcu_dl_entry_cks();
	//mcu_timer_wait_us(1000);
    //g_rw_mcu_top_0007h.bits.reserved3 = 3;
    /*P0 = 0x00;
    P0 = 0xFF;
    P0 = 0x00;*/
    g_fw_ver = 0x06;
    mcu_dma_i2c_slv_set_ch1();
    mcu_dma_i2c_slv_set_ch0();
    mcu_uart_puts("wait dl finish\n");
   
    tcon_wait_sys_reg_lut_finish();
    mcu_dl_get_ee_path();
#ifdef CSOT_FUN
    CSOT_RTPM_Init();
#endif
    mcu_i2c1_init();
    mcu_i2c2_init();
    tcon_init();
    mcu_get_i2cclk_set();
    if(tcon_check_dl_sta() == HW_DL_FAIL)        // hw dl fail
    {
        tcon_error_handler();
    }
    mcu_int_int4_en();                  // Enable Vsync Interrupt
    
    mcu_int_clear_iex4_isr_event();

    //mcu_int_i2c_mask(FALSE);
    mcu_int_i2c2_mask(FALSE);
#ifdef HW_CSOT_CRC_TEST
    mcu_uart_puts("HW CSOT CRC Test\n");
    g_res_flg = CSOT_RTPM_Cal_PMIC_Status(0xFFFF, 0xFFFF);
    g_res_flg = CSOT_RTPM_Cal_PMIC_Status(0x0000, 0x0000);
    while(1);
#endif

#ifdef I2C_MASTER_TEST
    // ------------------------------------------------------//
    // i2c master test
    // ------------------------------------------------------//
    mcu_uart_puts("I2c Master Test\n");
    g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 1;
    g_i2c_mst_reg[0] = 0x00;
    g_i2c_mst_reg[1] = 0x01;
    mcu_i2c_mst_write(0x50, &g_i2c_mst_reg[0], 2, &g_i2c_mst_rbuf[0], 16, 0, 0x5);
    g_rw_mcu_top_0003h.bits.mcu_i2c_ctrl = 0;
#endif

#if 0
    // ------------------------------------------------------//
    // spi test
    // ------------------------------------------------------//
    //for(test_cnt = 0 ; test_cnt< 256 ; test_cnt++)
    //    g_test_spi_buf[test_cnt] = test_cnt+1;
    //mcu_uart_puts("SPI Write Test\n");
    //mcu_spi_flash_page_program((uint8_t *)&g_spi_test_addr_offset+1, &g_test_spi_buf[0], 0x01 , 0x00, 0);
    if(g_rw_mcu_top_0043h.bits.cpol)
        SPCON |= (1 << 3);
    if(g_rw_mcu_top_0043h.bits.cpha)
        SPCON |= (1 << 2);
    mcu_uart_puts("SPI Read Test\n");
    mcu_spi_flash_fast_read((uint8_t *)&g_spi_test_addr_offset+1,&g_test_spi_buf[0],256,0);  
    while(1);
#endif

    mcu_uart_puts("EK50215AA_Fiti_0129 Loop Start...\n");
    while(1)
    {
    #ifdef CSOT_SPI_CMD_SUP
        CSOT_Write_PMIC_Code();     // JC add 220315
    #endif
			
        tcon_det_lut_i2c_adr();
        tcon_i2c_over_spi_proc();
        if(g_vsync_irq_flg)
        {
        
        #ifdef LUT_SRW_FUN
            Lut_I2c_Slv_RW_Vsync_Handler();     // LUT SRW
        #endif
        #ifdef CSOT_FUN
            CSOT_RTPM_Handler();     
        #endif
            tcon_vsync_update_vrr_dlg_proc();
            g_vsync_irq_flg = 0;      
        }
        //if(epd_misc_get_edp_state() == EDP_STATE_NORMAL)
        
        if(g_rw_mcu_top_0027h.bits.r_mcu_idle_en) mcu_set_power_saving_mode();
		}
}