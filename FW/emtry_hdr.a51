public eeprom_entry_hdr_var
public eeprom_entry_data_cks_adr
public eeprom_entry_data_cks_len
cseg at 02E00H
eeprom_entry_data_cks_adr:
DB 007H
DB 0F0H	
eeprom_entry_data_cks_len:
DB 00EH
eeprom_entry_hdr_var:
; // -------------------- //
; // entry 01 ~ entry 14  //
; // -------------------- //
; // ----- entry 01 ----- //
DB 01AH
DB 000H
DB 000H
DB 000H
DB 010H
DB 02DH
DB 000H
DB 0BAH
DB 000H
DB 0EDH
; // ----- entry 02 ----- //
DB 047H
DB 000H
DB 000H
DB 05FH
DB 010H
DB 00DH
DB 000H
DB 078H
DB 012H
DB 0B1H
; // ----- entry 03 ----- //
DB 054H
DB 000H
DB 000H
DB 000H
DB 018H
DB 055H
DB 000H
DB 0D4H
DB 000H
DB 069H
; // ----- entry 04 ----- //
DB 0A9H
DB 000H
DB 000H
DB 000H
DB 020H
DB 004H
DB 000H
DB 024H
DB 000H
DB 00EH
; // ----- entry 05 ----- //
DB 0ADH
DB 000H
DB 000H
DB 000H
DB 028H
DB 01EH
DB 000H
DB 0F1H
DB 000H
DB 01AH
; // ----- entry 06 ----- //
DB 0CBH
DB 000H
DB 000H
DB 000H
DB 030H
DB 043H
DB 000H
DB 0CCH
DB 000H
DB 0F3H
; // ----- entry 07 ----- //
DB 00EH
DB 001H
DB 000H
DB 000H
DB 038H
DB 014H
DB 000H
DB 0B4H
DB 000H
DB 0EFH
; // ----- entry 08 ----- //
DB 022H
DB 001H
DB 000H
DB 000H
DB 040H
DB 07CH
DB 001H
DB 0FBH
DB 000H
DB 023H
; // ----- entry 09 ----- //
DB 09EH
DB 002H
DB 000H
DB 000H
DB 048H
DB 016H
DB 000H
DB 0FFH
DB 000H
DB 001H
; // ----- entry 10 ----- //
DB 0B4H
DB 002H
DB 000H
DB 000H
DB 058H
DB 043H
DB 000H
DB 038H
DB 000H
DB 075H
; // ----- entry 11 ----- //
DB 0F7H
DB 002H
DB 000H
DB 000H
DB 060H
DB 037H
DB 000H
DB 08FH
DB 022H
DB 0BCH
; // ----- entry 12 ----- //
DB 02EH
DB 003H
DB 000H
DB 000H
DB 068H
DB 0B4H
DB 000H
DB 026H
DB 000H
DB 08BH
; // ----- entry 13 ----- //
DB 0E2H
DB 003H
DB 000H
DB 000H
DB 078H
DB 006H
DB 003H
DB 01BH
DB 000H
DB 07DH
; // ----- entry 14 ----- //
DB 0E8H
DB 006H
DB 000H
DB 000H
DB 098H
DB 000H
DB 001H
DB 07FH
DB 032H
DB 0C5H
; // -------------------- //
; // 128 bytes
; // -------------------- //
DB 0E8H
DB 006H
DB 000H
DB 000H
DB 098H
DB 080H
DB 000H
DB 07FH
DB 032H
DB 046H
END