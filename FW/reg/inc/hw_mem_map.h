// Create Date : 2024-01-11 10:47:28
// Excel name : _map.xlsx
// Tool version : FWHeaderGen_v1.0.0
//------------------------------------------------------------------------------------------------------------------------

#ifndef _HW_MEM_MAP_H_
#define _HW_MEM_MAP_H_

#define SRW_FIELD_XMEM_BASE                                               (0x0000)
#define SPAT_FIELD_XMEM_BASE                                              (0x0800)
#define SYS_FIELD_XMEM_BASE                                               (0x1000)
#define AIP_REG_FIELD_XMEM_BASE                                           (0x1800)

#define MCU_TOP_FIELD_XMEM_BASE                                           (0x2000)
#define CFGHDR_REG_FIELD_XMEM_BASE                                        (0x2100)
#define NPAT_FIELD_XMEM_BASE                                              (0x2800)
#define INPROC_FIELD_XMEM_BASE                                            (0x3000)
#define DAF_FIELD_XMEM_BASE                                               (0x3800)

#define PDF_FIELD_XMEM_BASE                                               (0x4000)
#define IRQ_FIELD_XMEM_BASE                                               (0x4800)
#define IRQ_READ_FIELD_XMEM_BASE                                          (0x4900)
#define LXLOD_FIELD_XMEM_BASE                                             (0x5000)
#define OPROC_FIELD_XMEM_BASE                                             (0x5800)

#define P2P_FIELD_XMEM_BASE                                               (0x6000)
#define CTRL_FIELD_XMEM_BASE                                              (0x6800)
#define EFUSE_FIELD_XMEM_BASE                                             (0x7000)
#define GAM_LUT_FIELD_XMEM_BASE                                           (0x7800)
#define GAMVRR0_FIELD_XMEM_BASE                                           (0x7C90)

#define GAMVRR1_FIELD_XMEM_BASE                                           (0x8000)
#define GAMVRR2_FIELD_XMEM_BASE                                           (0x8400)
#define LXL_GRAYLUT_FIELD_XMEM_BASE                                       (0x8800)
#define LXL_GAINLUT_FIELD_XMEM_BASE                                       (0x9000)
#define LXL_VRRLUT_FIELD_XMEM_BASE                                        (0x9400)

#define FRC_SET_MODE3_FIELD_XMEM_BASE                                     (0x9800)
#define FRC_LUT_MODE3_FIELD_XMEM_BASE                                     (0x9900)
#define DBG_MUX_FIELD_XMEM_BASE                                           (0xF800)

#endif