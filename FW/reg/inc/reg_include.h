#ifndef _REG_INCLUDE_H_
#define _REG_INCLUDE_H_

#include "mcu_type.h"

#include "srw.h"
#include "spat.h"
#include "sys.h"
#include "aip_reg.h"

#include "mcu_top.h"
#include "cfghdr_reg.h"
#include "npat.h"
#include "inproc.h"
#include "daf.h"

#include "pdf.h"
#include "irq.h"
#include "irq_read.h"
#include "lxlod.h"
#include "oproc.h"

#include "p2p.h"
#include "ctrl.h"
#include "efuse.h"
#include "gam_lut.h"
#include "gamvrr0.h"

#include "gamvrr1.h"
#include "gamvrr2.h"
#include "lxl_graylut.h"
#include "lxl_gainlut.h"
#include "lxl_vrrlut.h"

#include "frc_set_mode3.h"
#include "frc_lut_mode3.h"
#include "dbg_mux.h"

#endif