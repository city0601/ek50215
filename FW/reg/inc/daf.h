#ifndef _DAF_H_
#define _DAF_H_
#include "reg_include.h"

union rw_daf_0000h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gam_en                                                               : 1;        // val = 1
        unsigned char r_gam_clk_en                                                           : 1;        // val = 0
        unsigned char r_gam_demo_en                                                          : 1;        // val = 0
        unsigned char r_gam_addr_8b10b                                                       : 1;        // val = 0
        unsigned char r_gam_rlut_add                                                         : 1;        // val = 0
        unsigned char r_gam_glut_add                                                         : 1;        // val = 0
        unsigned char r_gam_blut_add                                                         : 1;        // val = 0
        unsigned char r_gam_10lut_en                                                         : 1;        // val = 0
    }bits;
};
extern volatile union rw_daf_0000h xdata g_rw_daf_0000h;    // Absolute Address = 3800h

union rw_daf_0001h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved1                                                              : 1;        // val = 0
        unsigned char r_lxl_od_en                                                            : 1;        // val = 1
        unsigned char r_lxl_od_clk_en                                                        : 1;        // val = 0
        unsigned char r_lxl_byp_en                                                           : 1;        // val = 0
        unsigned char r_lxl_byp_clk_en                                                       : 1;        // val = 0
        unsigned char r_patdet_en                                                            : 1;        // val = 1
        unsigned char r_patdet_clk_en                                                        : 1;        // val = 0
        unsigned char r_patdet_bypass_pat                                                    : 1;        // val = 0
    }bits;
};
extern volatile union rw_daf_0001h xdata g_rw_daf_0001h;    // Absolute Address = 3801h

union rw_daf_0002h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gam_shift_en                                                         : 1;        // val = 0
        unsigned char r_frc_en                                                               : 1;        // val = 1
        unsigned char r_frc_clk_en                                                           : 1;        // val = 1
        unsigned char r_frc_order_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_frc_state_chg_en                                                     : 1;        // val = 0
        unsigned char r_frc_obit_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_daf_0002h xdata g_rw_daf_0002h;    // Absolute Address = 3802h

union rw_daf_0003h
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_p_col_mask                                                       : 1;        // val = 0
        unsigned char r_frc_trigate_en                                                       : 1;        // val = 0
        unsigned char r_frc_mode                                                             : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_frc_4pat                                                             : 1;        // val = 0
        unsigned char r_frc_lvl_fill                                                         : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_frc_rgbchg_trigate                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_daf_0003h xdata g_rw_daf_0003h;    // Absolute Address = 3803h

union rw_daf_0004h
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_rgbchg_trigate_inner                                             : 1;        // val = 0
        unsigned char r_frc_mode_sel                                                         : 1;        // val = 0
        unsigned char r_frc_rgbchg_in_r1                                                     : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char r_frc_rgbchg_in_g1_bits_0                                              : 1;        // val = 1
    }bits;
};
extern volatile union rw_daf_0004h xdata g_rw_daf_0004h;    // Absolute Address = 3804h

//------------------------------------------------------------------------------------------------------------------------
union rw_daf_0005h
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_rgbchg_in_g1_bits_4_1                                            : 4;        // [msb:lsb] = [4:1], val = 0
        unsigned char r_frc_rgbchg_in_b1_bits_3_0                                            : 4;        // [msb:lsb] = [3:0], val = 2
    }bits;
};
extern volatile union rw_daf_0005h xdata g_rw_daf_0005h;    // Absolute Address = 3805h

union rw_daf_0006h
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_rgbchg_in_b1_bits_4                                              : 1;        // val = 0
        unsigned char r_frc_md_sel                                                           : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_period_h_thr_bits_4_0                                                : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_daf_0006h xdata g_rw_daf_0006h;    // Absolute Address = 3806h

union rw_daf_0008h
{
    unsigned char byte;
    struct
    {
        unsigned char r_period_h_thr_bits_15_13                                              : 3;        // [msb:lsb] = [15:13], val = 0
        unsigned char r_div_cnt_bits_4_0                                                     : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_daf_0008h xdata g_rw_daf_0008h;    // Absolute Address = 3808h

union rw_daf_0009h
{
    unsigned char byte;
    struct
    {
        unsigned char r_div_cnt_bits_7_5                                                     : 3;        // [msb:lsb] = [7:5], val = 0
        unsigned char r_frc_rgbchg_out_r1                                                    : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_daf_0009h xdata g_rw_daf_0009h;    // Absolute Address = 3809h

union rw_daf_000Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_rgbchg_out_g1                                                    : 5;        // [msb:lsb] = [4:0], val = 1
        unsigned char r_frc_rgbchg_out_b1_bits_2_0                                           : 3;        // [msb:lsb] = [2:0], val = 2
    }bits;
};
extern volatile union rw_daf_000Ah xdata g_rw_daf_000Ah;    // Absolute Address = 380Ah

//------------------------------------------------------------------------------------------------------------------------
union rw_daf_000Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_rgbchg_out_b1_bits_4_3                                           : 2;        // [msb:lsb] = [4:3], val = 0
        unsigned char r_gam_lut_stack_en                                                     : 1;        // val = 0
        unsigned char r_gam_lut_ofst_r                                                       : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gam_lut_ofst_g                                                       : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gam_lut_ofst_b_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_daf_000Bh xdata g_rw_daf_000Bh;    // Absolute Address = 380Bh

union rw_daf_000Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gam_lut_ofst_b_bits_1                                                : 1;        // val = 0
        unsigned char r_frc_fcyc_sel                                                         : 1;        // val = 0
        unsigned char r_frc_low_frm_en                                                       : 1;        // val = 0
        unsigned char r_frc_frm_swap_en                                                      : 1;        // val = 0
        unsigned char r_gam_lut_ofst7_en                                                     : 1;        // val = 0
        unsigned char r_frc_newfrc_mode                                                      : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_frc_output_mode_bits_0                                               : 1;        // val = 0
    }bits;
};
extern volatile union rw_daf_000Ch xdata g_rw_daf_000Ch;    // Absolute Address = 380Ch

union rw_daf_000Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_output_mode_bits_1                                               : 1;        // val = 0
        unsigned char r_frc_max_10b_bits_6_0                                                 : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_daf_000Dh xdata g_rw_daf_000Dh;    // Absolute Address = 380Dh

union rw_daf_000Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_max_10b_bits_9_7                                                 : 3;        // [msb:lsb] = [9:7], val = 0
        unsigned char r_frc_debug_en                                                         : 1;        // val = 0
        unsigned char r_frc_dbg_frm_cnt                                                      : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_period_bypass                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_daf_000Eh xdata g_rw_daf_000Eh;    // Absolute Address = 380Eh

union rw_daf_0011h
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_mframe_cnt_0                                                     : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_frc_mframe_cnt_1                                                     : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_frc_mframe_cnt_2_bits_1_0                                            : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_daf_0011h xdata g_rw_daf_0011h;    // Absolute Address = 3811h

//------------------------------------------------------------------------------------------------------------------------
union rw_daf_0012h
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_mframe_cnt_2_bits_2                                              : 1;        // val = 0
        unsigned char r_frc_mframe_cnt_3                                                     : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_frc_mframe_cnt_4                                                     : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_frc_mframe_cnt_5_bits_0                                              : 1;        // val = 0
    }bits;
};
extern volatile union rw_daf_0012h xdata g_rw_daf_0012h;    // Absolute Address = 3812h

union rw_daf_0013h
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_mframe_cnt_5_bits_2_1                                            : 2;        // [msb:lsb] = [2:1], val = 0
        unsigned char r_frc_mframe_cnt_6                                                     : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_frc_mframe_cnt_7                                                     : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_daf_0013h xdata g_rw_daf_0013h;    // Absolute Address = 3813h

union rw_daf_0016h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved2                                                              : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_vact_diff_bits_5_0                                                   : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_daf_0016h xdata g_rw_daf_0016h;    // Absolute Address = 3816h

union rw_daf_0017h
{
    unsigned char byte;
    struct
    {
        unsigned char r_vact_diff_bits_7_6                                                   : 2;        // [msb:lsb] = [7:6], val = 0
        unsigned char r_vrr_dbgen                                                            : 1;        // val = 0
        unsigned char r_dbg_period_bits_4_0                                                  : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_daf_0017h xdata g_rw_daf_0017h;    // Absolute Address = 3817h

union rw_daf_0019h
{
    unsigned char byte;
    struct
    {
        unsigned char r_dbg_period_bits_15_13                                                : 3;        // [msb:lsb] = [15:13], val = 0
        unsigned char r_gam_vrr_lut_en                                                       : 1;        // val = 1
        unsigned char r_gam_vrr_ind_t1_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_daf_0019h xdata g_rw_daf_0019h;    // Absolute Address = 3819h

//------------------------------------------------------------------------------------------------------------------------
union rw_daf_001Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gam_vrr_ind_t1_bits_15_12                                            : 4;        // [msb:lsb] = [15:12], val = 0
        unsigned char r_gam_vrr_ind_t2_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 12
    }bits;
};
extern volatile union rw_daf_001Bh xdata g_rw_daf_001Bh;    // Absolute Address = 381Bh

union rw_daf_001Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gam_vrr_ind_t2_bits_15_12                                            : 4;        // [msb:lsb] = [15:12], val = 0
        unsigned char r_gam_vrr_ind_t3_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 12
    }bits;
};
extern volatile union rw_daf_001Dh xdata g_rw_daf_001Dh;    // Absolute Address = 381Dh

union rw_daf_001Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gam_vrr_ind_t3_bits_15_12                                            : 4;        // [msb:lsb] = [15:12], val = 0
        unsigned char r_gam_vrr_ind_t4_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 12
    }bits;
};
extern volatile union rw_daf_001Fh xdata g_rw_daf_001Fh;    // Absolute Address = 381Fh

union rw_daf_0021h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gam_vrr_ind_t4_bits_15_12                                            : 4;        // [msb:lsb] = [15:12], val = 0
        unsigned char reserved3                                                              : 1;        // val = 0
        unsigned char r_direct_rgb_en                                                        : 1;        // val = 0
        unsigned char r_direct_rgb_mode                                                      : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_daf_0021h xdata g_rw_daf_0021h;    // Absolute Address = 3821h

union rw_daf_0023h
{
    unsigned char byte;
    struct
    {
        unsigned char r_h_point_bits_11_8                                                    : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_v_point_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_daf_0023h xdata g_rw_daf_0023h;    // Absolute Address = 3823h

//------------------------------------------------------------------------------------------------------------------------
union rw_daf_0026h
{
    unsigned char byte;
    struct
    {
        unsigned char r_direct_r1_bits_11_8                                                  : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_direct_g1_bits_3_0                                                   : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_daf_0026h xdata g_rw_daf_0026h;    // Absolute Address = 3826h

union rw_daf_0029h
{
    unsigned char byte;
    struct
    {
        unsigned char r_direct_b1_bits_11_8                                                  : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_cap_en                                                               : 1;        // val = 0
        unsigned char r_cap_sel_bits_2_0                                                     : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_daf_0029h xdata g_rw_daf_0029h;    // Absolute Address = 3829h

union rw_daf_002Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_cap_sel_bits_3                                                       : 1;        // val = 0
        unsigned char r_capture_port                                                         : 1;        // val = 0
        unsigned char r_capture_cycle                                                        : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char r_capture_frame_bits_0                                                 : 1;        // val = 0
    }bits;
};
extern volatile union rw_daf_002Ah xdata g_rw_daf_002Ah;    // Absolute Address = 382Ah

union rw_daf_002Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_capture_frame_bits_4_1                                               : 4;        // [msb:lsb] = [4:1], val = 0
        unsigned char r_capture_line_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_daf_002Bh xdata g_rw_daf_002Bh;    // Absolute Address = 382Bh

union rw_daf_002Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_capture_pixel_bits_11_8                                              : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_frc_demo_en                                                          : 1;        // val = 0
        unsigned char r_frc_pat_demo                                                         : 1;        // val = 0
        unsigned char r_frc_test_mode                                                        : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_daf_002Eh xdata g_rw_daf_002Eh;    // Absolute Address = 382Eh

//------------------------------------------------------------------------------------------------------------------------
union rw_daf_002Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_f_sel                                                            : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_frc_rgb_sel                                                          : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_frc_rgbchg_half_pass_in_bits_0                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_daf_002Fh xdata g_rw_daf_002Fh;    // Absolute Address = 382Fh

union rw_daf_0030h
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_rgbchg_half_pass_in_bits_1                                       : 1;        // val = 0
        unsigned char r_frc_rgbchg_half_pass_out                                             : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_frc_rgbchg_pass_inner                                                : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_frc_single_ram                                                       : 1;        // val = 0
        unsigned char r_frc_daul_lut_en                                                      : 1;        // val = 0
        unsigned char r_frc_daul_lut_thd_bits_0                                              : 1;        // val = 0
    }bits;
};
extern volatile union rw_daf_0030h xdata g_rw_daf_0030h;    // Absolute Address = 3830h

union rw_daf_0032h
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_daul_lut_thd_bits_11_9                                           : 3;        // [msb:lsb] = [11:9], val = 0
        unsigned char reserved4                                                              : 1;        // val = 0
        unsigned char reserved5                                                              : 1;        // val = 0
        unsigned char r_frc_core_pass                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_frc_frm_base                                                         : 1;        // val = 0
    }bits;
};
extern volatile union rw_daf_0032h xdata g_rw_daf_0032h;    // Absolute Address = 3832h

union rw_daf_0035h
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_frm_stack_bits_22_16                                             : 7;        // [msb:lsb] = [22:16], val = 0
        unsigned char r_dt_cscd_en                                                           : 1;        // val = 0
    }bits;
};
extern volatile union rw_daf_0035h xdata g_rw_daf_0035h;    // Absolute Address = 3835h

union rw_daf_0036h
{
    unsigned char byte;
    struct
    {
        unsigned char r_dt_cscd_edge_sel                                                     : 1;        // val = 0
        unsigned char reserved6                                                              : 1;        // val = 0
        unsigned char r_frc_add_value_en                                                     : 1;        // val = 0
        unsigned char r_frc_dbg_sel                                                          : 1;        // val = 0
        unsigned char r_frc_frame_demo_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_daf_0036h xdata g_rw_daf_0036h;    // Absolute Address = 3836h

//------------------------------------------------------------------------------------------------------------------------
union rw_daf_0037h
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_frame_demo_bits_4                                                : 1;        // val = 0
        unsigned char r_frc_add_value                                                        : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_period_deb1_en                                                       : 1;        // val = 0
        unsigned char r_period_deb2_en                                                       : 1;        // val = 0
        unsigned char r_period_deb3_en                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_daf_0037h xdata g_rw_daf_0037h;    // Absolute Address = 3837h

union rw_daf_0038h
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_dbg_mode                                                         : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_frc_dbg_frc_ratio                                                    : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char reserved7                                                              : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_daf_0038h xdata g_rw_daf_0038h;    // Absolute Address = 3838h

union rw_daf_003Ch
{
    unsigned char byte;
    struct
    {
        unsigned char capture_r_bits_11_8                                                    : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char capture_g_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_daf_003Ch xdata g_rw_daf_003Ch;    // Absolute Address = 383Ch

union rw_daf_003Fh
{
    unsigned char byte;
    struct
    {
        unsigned char capture_b_bits_11_8                                                    : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char reserved8                                                              : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_daf_003Fh xdata g_rw_daf_003Fh;    // Absolute Address = 383Fh

union rw_daf_0041h
{
    unsigned char byte;
    struct
    {
        unsigned char capture_htot_rec_bits_13_8                                             : 6;        // [msb:lsb] = [13:8], val = 0
        unsigned char reserved9                                                              : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_daf_0041h xdata g_rw_daf_0041h;    // Absolute Address = 3841h

//------------------------------------------------------------------------------------------------------------------------
union rw_daf_0043h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved11                                                             : 7;        // [msb:lsb] = [6:0], val = 0
        unsigned char LUT_ready                                                              : 1;        // val = 0
    }bits;
};
extern volatile union rw_daf_0043h xdata g_rw_daf_0043h;    // Absolute Address = 3843h


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_daf_0007h_period_h_thr;                                               // [msb:lsb] = [12:5], val = 0, 	Absolute Address = 3807h
extern volatile uint8_t xdata g_rw_daf_000Fh_frc_frame_order;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 380Fh
extern volatile uint8_t xdata g_rw_daf_0010h_frc_frame_order;                                            // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 3810h
extern volatile uint8_t xdata g_rw_daf_0014h_period_l_thr;                                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 3814h
extern volatile uint8_t xdata g_rw_daf_0015h_period_l_thr;                                               // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 3815h

extern volatile uint8_t xdata g_rw_daf_0018h_dbg_period;                                                 // [msb:lsb] = [12:5], val = 0, 	Absolute Address = 3818h
extern volatile uint8_t xdata g_rw_daf_001Ah_gam_vrr_ind_t1;                                             // [msb:lsb] = [11:4], val = 75, 	Absolute Address = 381Ah
extern volatile uint8_t xdata g_rw_daf_001Ch_gam_vrr_ind_t2;                                             // [msb:lsb] = [11:4], val = 68, 	Absolute Address = 381Ch
extern volatile uint8_t xdata g_rw_daf_001Eh_gam_vrr_ind_t3;                                             // [msb:lsb] = [11:4], val = 68, 	Absolute Address = 381Eh
extern volatile uint8_t xdata g_rw_daf_0020h_gam_vrr_ind_t4;                                             // [msb:lsb] = [11:4], val = 68, 	Absolute Address = 3820h

extern volatile uint8_t xdata g_rw_daf_0022h_h_point;                                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 3822h
extern volatile uint8_t xdata g_rw_daf_0024h_v_point;                                                    // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 3824h
extern volatile uint8_t xdata g_rw_daf_0025h_direct_r1;                                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 3825h
extern volatile uint8_t xdata g_rw_daf_0027h_direct_g1;                                                  // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 3827h
extern volatile uint8_t xdata g_rw_daf_0028h_direct_b1;                                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 3828h

extern volatile uint8_t xdata g_rw_daf_002Ch_capture_line;                                               // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 382Ch
extern volatile uint8_t xdata g_rw_daf_002Dh_capture_pixel;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 382Dh
extern volatile uint8_t xdata g_rw_daf_0031h_frc_daul_lut_thd;                                           // [msb:lsb] = [8:1], val = 0, 	Absolute Address = 3831h
extern volatile uint8_t xdata g_rw_daf_0033h_frc_frm_stack;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 3833h
extern volatile uint8_t xdata g_rw_daf_0034h_frc_frm_stack;                                              // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 3834h

extern volatile uint8_t xdata g_rw_daf_0039h_period_time;                                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 3839h
extern volatile uint8_t xdata g_rw_daf_003Ah_period_time;                                                // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 383Ah
extern volatile uint8_t xdata g_rw_daf_003Bh_capture_r;                                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 383Bh
extern volatile uint8_t xdata g_rw_daf_003Dh_capture_g;                                                  // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 383Dh
extern volatile uint8_t xdata g_rw_daf_003Eh_capture_b;                                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 383Eh

extern volatile uint8_t xdata g_rw_daf_0040h_capture_htot_rec;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 3840h
extern volatile uint8_t xdata g_rw_daf_0042h_reserved10;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 3842h

#endif