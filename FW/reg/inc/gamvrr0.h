#ifndef _GAMVRR0_H_
#define _GAMVRR0_H_
#include "reg_include.h"


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_gamvrr0_0000h_GAM_VRR_LUT_OFST12_0_new_data_0;                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C90h
extern volatile uint8_t xdata g_rw_gamvrr0_0001h_GAM_VRR_LUT_OFST12_0_new_data_1;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7C91h
extern volatile uint8_t xdata g_rw_gamvrr0_0002h_GAM_VRR_LUT_OFST12_0_new_data_2;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7C92h
extern volatile uint8_t xdata g_rw_gamvrr0_0003h_GAM_VRR_LUT_OFST12_0_new_data_3;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7C93h
extern volatile uint8_t xdata g_rw_gamvrr0_0004h_GAM_VRR_LUT_OFST12_0_new_data_4;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7C94h

extern volatile uint8_t xdata g_rw_gamvrr0_0005h_GAM_VRR_LUT_OFST12_0_new_data_5;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7C95h
extern volatile uint8_t xdata g_rw_gamvrr0_0006h_GAM_VRR_LUT_OFST12_0_new_data_6;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7C96h
extern volatile uint8_t xdata g_rw_gamvrr0_0007h_GAM_VRR_LUT_OFST12_0_new_data_7;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7C97h
extern volatile uint8_t xdata g_rw_gamvrr0_0008h_GAM_VRR_LUT_OFST12_0_new_data_8;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7C98h
extern volatile uint8_t xdata g_rw_gamvrr0_0009h_GAM_VRR_LUT_OFST12_0_new_data_9;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7C99h

extern volatile uint8_t xdata g_rw_gamvrr0_000Ah_GAM_VRR_LUT_OFST12_0_new_data_10;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7C9Ah
extern volatile uint8_t xdata g_rw_gamvrr0_000Bh_GAM_VRR_LUT_OFST12_0_new_data_11;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7C9Bh
extern volatile uint8_t xdata g_rw_gamvrr0_000Ch_GAM_VRR_LUT_OFST12_0_new_data_12;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7C9Ch
extern volatile uint8_t xdata g_rw_gamvrr0_000Dh_GAM_VRR_LUT_OFST12_0_new_data_13;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7C9Dh
extern volatile uint8_t xdata g_rw_gamvrr0_000Eh_GAM_VRR_LUT_OFST12_0_new_data_14;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7C9Eh

extern volatile uint8_t xdata g_rw_gamvrr0_000Fh_GAM_VRR_LUT_OFST12_0_new_data_15;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7C9Fh
extern volatile uint8_t xdata g_rw_gamvrr0_0010h_GAM_VRR_LUT_OFST12_0_new_data_16;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CA0h
extern volatile uint8_t xdata g_rw_gamvrr0_0011h_GAM_VRR_LUT_OFST12_0_new_data_17;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CA1h
extern volatile uint8_t xdata g_rw_gamvrr0_0012h_GAM_VRR_LUT_OFST12_0_new_data_18;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CA2h
extern volatile uint8_t xdata g_rw_gamvrr0_0013h_GAM_VRR_LUT_OFST12_0_new_data_19;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CA3h

extern volatile uint8_t xdata g_rw_gamvrr0_0014h_GAM_VRR_LUT_OFST12_0_new_data_20;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CA4h
extern volatile uint8_t xdata g_rw_gamvrr0_0015h_GAM_VRR_LUT_OFST12_0_new_data_21;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CA5h
extern volatile uint8_t xdata g_rw_gamvrr0_0016h_GAM_VRR_LUT_OFST12_0_new_data_22;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CA6h
extern volatile uint8_t xdata g_rw_gamvrr0_0017h_GAM_VRR_LUT_OFST12_0_new_data_23;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CA7h
extern volatile uint8_t xdata g_rw_gamvrr0_0018h_GAM_VRR_LUT_OFST12_0_new_data_24;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CA8h

extern volatile uint8_t xdata g_rw_gamvrr0_0019h_GAM_VRR_LUT_OFST12_0_new_data_25;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CA9h
extern volatile uint8_t xdata g_rw_gamvrr0_001Ah_GAM_VRR_LUT_OFST12_0_new_data_26;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CAAh
extern volatile uint8_t xdata g_rw_gamvrr0_001Bh_GAM_VRR_LUT_OFST12_0_new_data_27;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CABh
extern volatile uint8_t xdata g_rw_gamvrr0_001Ch_GAM_VRR_LUT_OFST12_0_new_data_28;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CACh
extern volatile uint8_t xdata g_rw_gamvrr0_001Dh_GAM_VRR_LUT_OFST12_0_new_data_29;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CADh

extern volatile uint8_t xdata g_rw_gamvrr0_001Eh_GAM_VRR_LUT_OFST12_0_new_data_30;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CAEh
extern volatile uint8_t xdata g_rw_gamvrr0_001Fh_GAM_VRR_LUT_OFST12_0_new_data_31;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CAFh
extern volatile uint8_t xdata g_rw_gamvrr0_0020h_GAM_VRR_LUT_OFST12_0_new_data_32;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CB0h
extern volatile uint8_t xdata g_rw_gamvrr0_0021h_GAM_VRR_LUT_OFST12_0_new_data_33;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CB1h
extern volatile uint8_t xdata g_rw_gamvrr0_0022h_GAM_VRR_LUT_OFST12_0_new_data_34;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CB2h

extern volatile uint8_t xdata g_rw_gamvrr0_0023h_GAM_VRR_LUT_OFST12_0_new_data_35;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CB3h
extern volatile uint8_t xdata g_rw_gamvrr0_0024h_GAM_VRR_LUT_OFST12_0_new_data_36;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CB4h
extern volatile uint8_t xdata g_rw_gamvrr0_0025h_GAM_VRR_LUT_OFST12_0_new_data_37;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CB5h
extern volatile uint8_t xdata g_rw_gamvrr0_0026h_GAM_VRR_LUT_OFST12_0_new_data_38;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CB6h
extern volatile uint8_t xdata g_rw_gamvrr0_0027h_GAM_VRR_LUT_OFST12_0_new_data_39;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CB7h

extern volatile uint8_t xdata g_rw_gamvrr0_0028h_GAM_VRR_LUT_OFST12_0_new_data_40;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CB8h
extern volatile uint8_t xdata g_rw_gamvrr0_0029h_GAM_VRR_LUT_OFST12_0_new_data_41;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CB9h
extern volatile uint8_t xdata g_rw_gamvrr0_002Ah_GAM_VRR_LUT_OFST12_0_new_data_42;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CBAh
extern volatile uint8_t xdata g_rw_gamvrr0_002Bh_GAM_VRR_LUT_OFST12_0_new_data_43;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CBBh
extern volatile uint8_t xdata g_rw_gamvrr0_002Ch_GAM_VRR_LUT_OFST12_0_new_data_44;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CBCh

extern volatile uint8_t xdata g_rw_gamvrr0_002Dh_GAM_VRR_LUT_OFST12_0_new_data_45;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CBDh
extern volatile uint8_t xdata g_rw_gamvrr0_002Eh_GAM_VRR_LUT_OFST12_0_new_data_46;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CBEh
extern volatile uint8_t xdata g_rw_gamvrr0_002Fh_GAM_VRR_LUT_OFST12_0_new_data_47;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CBFh
extern volatile uint8_t xdata g_rw_gamvrr0_0030h_GAM_VRR_LUT_OFST12_0_new_data_48;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CC0h
extern volatile uint8_t xdata g_rw_gamvrr0_0031h_GAM_VRR_LUT_OFST12_0_new_data_49;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CC1h

extern volatile uint8_t xdata g_rw_gamvrr0_0032h_GAM_VRR_LUT_OFST12_0_new_data_50;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CC2h
extern volatile uint8_t xdata g_rw_gamvrr0_0033h_GAM_VRR_LUT_OFST12_0_new_data_51;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CC3h
extern volatile uint8_t xdata g_rw_gamvrr0_0034h_GAM_VRR_LUT_OFST12_0_new_data_52;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CC4h
extern volatile uint8_t xdata g_rw_gamvrr0_0035h_GAM_VRR_LUT_OFST12_0_new_data_53;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CC5h
extern volatile uint8_t xdata g_rw_gamvrr0_0036h_GAM_VRR_LUT_OFST12_0_new_data_54;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CC6h

extern volatile uint8_t xdata g_rw_gamvrr0_0037h_GAM_VRR_LUT_OFST12_0_new_data_55;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CC7h
extern volatile uint8_t xdata g_rw_gamvrr0_0038h_GAM_VRR_LUT_OFST12_0_new_data_56;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CC8h
extern volatile uint8_t xdata g_rw_gamvrr0_0039h_GAM_VRR_LUT_OFST12_0_new_data_57;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CC9h
extern volatile uint8_t xdata g_rw_gamvrr0_003Ah_GAM_VRR_LUT_OFST12_0_new_data_58;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CCAh
extern volatile uint8_t xdata g_rw_gamvrr0_003Bh_GAM_VRR_LUT_OFST12_0_new_data_59;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CCBh

extern volatile uint8_t xdata g_rw_gamvrr0_003Ch_GAM_VRR_LUT_OFST12_0_new_data_60;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CCCh
extern volatile uint8_t xdata g_rw_gamvrr0_003Dh_GAM_VRR_LUT_OFST12_0_new_data_61;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CCDh
extern volatile uint8_t xdata g_rw_gamvrr0_003Eh_GAM_VRR_LUT_OFST12_0_new_data_62;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CCEh
extern volatile uint8_t xdata g_rw_gamvrr0_003Fh_GAM_VRR_LUT_OFST12_0_new_data_63;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CCFh
extern volatile uint8_t xdata g_rw_gamvrr0_0040h_GAM_VRR_LUT_OFST12_0_new_data_64;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CD0h

extern volatile uint8_t xdata g_rw_gamvrr0_0041h_GAM_VRR_LUT_OFST12_0_new_data_65;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CD1h
extern volatile uint8_t xdata g_rw_gamvrr0_0042h_GAM_VRR_LUT_OFST12_0_new_data_66;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CD2h
extern volatile uint8_t xdata g_rw_gamvrr0_0043h_GAM_VRR_LUT_OFST12_0_new_data_67;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CD3h
extern volatile uint8_t xdata g_rw_gamvrr0_0044h_GAM_VRR_LUT_OFST12_0_new_data_68;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CD4h
extern volatile uint8_t xdata g_rw_gamvrr0_0045h_GAM_VRR_LUT_OFST12_0_new_data_69;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CD5h

extern volatile uint8_t xdata g_rw_gamvrr0_0046h_GAM_VRR_LUT_OFST12_0_new_data_70;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CD6h
extern volatile uint8_t xdata g_rw_gamvrr0_0047h_GAM_VRR_LUT_OFST12_0_new_data_71;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CD7h
extern volatile uint8_t xdata g_rw_gamvrr0_0048h_GAM_VRR_LUT_OFST12_0_new_data_72;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CD8h
extern volatile uint8_t xdata g_rw_gamvrr0_0049h_GAM_VRR_LUT_OFST12_0_new_data_73;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CD9h
extern volatile uint8_t xdata g_rw_gamvrr0_004Ah_GAM_VRR_LUT_OFST12_0_new_data_74;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CDAh

extern volatile uint8_t xdata g_rw_gamvrr0_004Bh_GAM_VRR_LUT_OFST12_0_new_data_75;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CDBh
extern volatile uint8_t xdata g_rw_gamvrr0_004Ch_GAM_VRR_LUT_OFST12_0_new_data_76;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CDCh
extern volatile uint8_t xdata g_rw_gamvrr0_004Dh_GAM_VRR_LUT_OFST12_0_new_data_77;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CDDh
extern volatile uint8_t xdata g_rw_gamvrr0_004Eh_GAM_VRR_LUT_OFST12_0_new_data_78;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CDEh
extern volatile uint8_t xdata g_rw_gamvrr0_004Fh_GAM_VRR_LUT_OFST12_0_new_data_79;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CDFh

extern volatile uint8_t xdata g_rw_gamvrr0_0050h_GAM_VRR_LUT_OFST12_0_new_data_80;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CE0h
extern volatile uint8_t xdata g_rw_gamvrr0_0051h_GAM_VRR_LUT_OFST12_0_new_data_81;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CE1h
extern volatile uint8_t xdata g_rw_gamvrr0_0052h_GAM_VRR_LUT_OFST12_0_new_data_82;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CE2h
extern volatile uint8_t xdata g_rw_gamvrr0_0053h_GAM_VRR_LUT_OFST12_0_new_data_83;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CE3h
extern volatile uint8_t xdata g_rw_gamvrr0_0054h_GAM_VRR_LUT_OFST12_0_new_data_84;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CE4h

extern volatile uint8_t xdata g_rw_gamvrr0_0055h_GAM_VRR_LUT_OFST12_0_new_data_85;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CE5h
extern volatile uint8_t xdata g_rw_gamvrr0_0056h_GAM_VRR_LUT_OFST12_0_new_data_86;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CE6h
extern volatile uint8_t xdata g_rw_gamvrr0_0057h_GAM_VRR_LUT_OFST12_0_new_data_87;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CE7h
extern volatile uint8_t xdata g_rw_gamvrr0_0058h_GAM_VRR_LUT_OFST12_0_new_data_88;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CE8h
extern volatile uint8_t xdata g_rw_gamvrr0_0059h_GAM_VRR_LUT_OFST12_0_new_data_89;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CE9h

extern volatile uint8_t xdata g_rw_gamvrr0_005Ah_GAM_VRR_LUT_OFST12_0_new_data_90;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CEAh
extern volatile uint8_t xdata g_rw_gamvrr0_005Bh_GAM_VRR_LUT_OFST12_0_new_data_91;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CEBh
extern volatile uint8_t xdata g_rw_gamvrr0_005Ch_GAM_VRR_LUT_OFST12_0_new_data_92;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CECh
extern volatile uint8_t xdata g_rw_gamvrr0_005Dh_GAM_VRR_LUT_OFST12_0_new_data_93;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CEDh
extern volatile uint8_t xdata g_rw_gamvrr0_005Eh_GAM_VRR_LUT_OFST12_0_new_data_94;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CEEh

extern volatile uint8_t xdata g_rw_gamvrr0_005Fh_GAM_VRR_LUT_OFST12_0_new_data_95;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CEFh
extern volatile uint8_t xdata g_rw_gamvrr0_0060h_GAM_VRR_LUT_OFST12_0_new_data_96;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CF0h
extern volatile uint8_t xdata g_rw_gamvrr0_0061h_GAM_VRR_LUT_OFST12_0_new_data_97;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CF1h
extern volatile uint8_t xdata g_rw_gamvrr0_0062h_GAM_VRR_LUT_OFST12_0_new_data_98;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CF2h
extern volatile uint8_t xdata g_rw_gamvrr0_0063h_GAM_VRR_LUT_OFST12_0_new_data_99;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CF3h

extern volatile uint8_t xdata g_rw_gamvrr0_0064h_GAM_VRR_LUT_OFST12_0_new_data_100;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CF4h
extern volatile uint8_t xdata g_rw_gamvrr0_0065h_GAM_VRR_LUT_OFST12_0_new_data_101;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CF5h
extern volatile uint8_t xdata g_rw_gamvrr0_0066h_GAM_VRR_LUT_OFST12_0_new_data_102;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CF6h
extern volatile uint8_t xdata g_rw_gamvrr0_0067h_GAM_VRR_LUT_OFST12_0_new_data_103;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CF7h
extern volatile uint8_t xdata g_rw_gamvrr0_0068h_GAM_VRR_LUT_OFST12_0_new_data_104;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CF8h

extern volatile uint8_t xdata g_rw_gamvrr0_0069h_GAM_VRR_LUT_OFST12_0_new_data_105;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CF9h
extern volatile uint8_t xdata g_rw_gamvrr0_006Ah_GAM_VRR_LUT_OFST12_0_new_data_106;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CFAh
extern volatile uint8_t xdata g_rw_gamvrr0_006Bh_GAM_VRR_LUT_OFST12_0_new_data_107;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CFBh
extern volatile uint8_t xdata g_rw_gamvrr0_006Ch_GAM_VRR_LUT_OFST12_0_new_data_108;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CFCh
extern volatile uint8_t xdata g_rw_gamvrr0_006Dh_GAM_VRR_LUT_OFST12_0_new_data_109;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CFDh

extern volatile uint8_t xdata g_rw_gamvrr0_006Eh_GAM_VRR_LUT_OFST12_0_new_data_110;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CFEh
extern volatile uint8_t xdata g_rw_gamvrr0_006Fh_GAM_VRR_LUT_OFST12_0_new_data_111;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7CFFh
extern volatile uint8_t xdata g_rw_gamvrr0_0070h_GAM_VRR_LUT_OFST12_0_new_data_112;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D00h
extern volatile uint8_t xdata g_rw_gamvrr0_0071h_GAM_VRR_LUT_OFST12_0_new_data_113;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D01h
extern volatile uint8_t xdata g_rw_gamvrr0_0072h_GAM_VRR_LUT_OFST12_0_new_data_114;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D02h

extern volatile uint8_t xdata g_rw_gamvrr0_0073h_GAM_VRR_LUT_OFST12_0_new_data_115;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D03h
extern volatile uint8_t xdata g_rw_gamvrr0_0074h_GAM_VRR_LUT_OFST12_0_new_data_116;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D04h
extern volatile uint8_t xdata g_rw_gamvrr0_0075h_GAM_VRR_LUT_OFST12_0_new_data_117;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D05h
extern volatile uint8_t xdata g_rw_gamvrr0_0076h_GAM_VRR_LUT_OFST12_0_new_data_118;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D06h
extern volatile uint8_t xdata g_rw_gamvrr0_0077h_GAM_VRR_LUT_OFST12_0_new_data_119;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D07h

extern volatile uint8_t xdata g_rw_gamvrr0_0078h_GAM_VRR_LUT_OFST12_0_new_data_120;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D08h
extern volatile uint8_t xdata g_rw_gamvrr0_0079h_GAM_VRR_LUT_OFST12_0_new_data_121;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D09h
extern volatile uint8_t xdata g_rw_gamvrr0_007Ah_GAM_VRR_LUT_OFST12_0_new_data_122;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D0Ah
extern volatile uint8_t xdata g_rw_gamvrr0_007Bh_GAM_VRR_LUT_OFST12_0_new_data_123;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D0Bh
extern volatile uint8_t xdata g_rw_gamvrr0_007Ch_GAM_VRR_LUT_OFST12_0_new_data_124;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D0Ch

extern volatile uint8_t xdata g_rw_gamvrr0_007Dh_GAM_VRR_LUT_OFST12_0_new_data_125;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D0Dh
extern volatile uint8_t xdata g_rw_gamvrr0_007Eh_GAM_VRR_LUT_OFST12_0_new_data_126;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D0Eh
extern volatile uint8_t xdata g_rw_gamvrr0_007Fh_GAM_VRR_LUT_OFST12_0_new_data_127;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D0Fh
extern volatile uint8_t xdata g_rw_gamvrr0_0080h_GAM_VRR_LUT_OFST12_0_new_data_128;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D10h
extern volatile uint8_t xdata g_rw_gamvrr0_0081h_GAM_VRR_LUT_OFST12_0_new_data_129;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D11h

extern volatile uint8_t xdata g_rw_gamvrr0_0082h_GAM_VRR_LUT_OFST12_0_new_data_130;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D12h
extern volatile uint8_t xdata g_rw_gamvrr0_0083h_GAM_VRR_LUT_OFST12_0_new_data_131;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D13h
extern volatile uint8_t xdata g_rw_gamvrr0_0084h_GAM_VRR_LUT_OFST12_0_new_data_132;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D14h
extern volatile uint8_t xdata g_rw_gamvrr0_0085h_GAM_VRR_LUT_OFST12_0_new_data_133;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D15h
extern volatile uint8_t xdata g_rw_gamvrr0_0086h_GAM_VRR_LUT_OFST12_0_new_data_134;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D16h

extern volatile uint8_t xdata g_rw_gamvrr0_0087h_GAM_VRR_LUT_OFST12_0_new_data_135;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D17h
extern volatile uint8_t xdata g_rw_gamvrr0_0088h_GAM_VRR_LUT_OFST12_0_new_data_136;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D18h
extern volatile uint8_t xdata g_rw_gamvrr0_0089h_GAM_VRR_LUT_OFST12_0_new_data_137;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D19h
extern volatile uint8_t xdata g_rw_gamvrr0_008Ah_GAM_VRR_LUT_OFST12_0_new_data_138;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D1Ah
extern volatile uint8_t xdata g_rw_gamvrr0_008Bh_GAM_VRR_LUT_OFST12_0_new_data_139;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D1Bh

extern volatile uint8_t xdata g_rw_gamvrr0_008Ch_GAM_VRR_LUT_OFST12_0_new_data_140;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D1Ch
extern volatile uint8_t xdata g_rw_gamvrr0_008Dh_GAM_VRR_LUT_OFST12_0_new_data_141;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D1Dh
extern volatile uint8_t xdata g_rw_gamvrr0_008Eh_GAM_VRR_LUT_OFST12_0_new_data_142;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D1Eh
extern volatile uint8_t xdata g_rw_gamvrr0_008Fh_GAM_VRR_LUT_OFST12_0_new_data_143;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D1Fh
extern volatile uint8_t xdata g_rw_gamvrr0_0090h_GAM_VRR_LUT_OFST12_0_new_data_144;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D20h

extern volatile uint8_t xdata g_rw_gamvrr0_0091h_GAM_VRR_LUT_OFST12_0_new_data_145;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D21h
extern volatile uint8_t xdata g_rw_gamvrr0_0092h_GAM_VRR_LUT_OFST12_0_new_data_146;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D22h
extern volatile uint8_t xdata g_rw_gamvrr0_0093h_GAM_VRR_LUT_OFST12_0_new_data_147;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D23h
extern volatile uint8_t xdata g_rw_gamvrr0_0094h_GAM_VRR_LUT_OFST12_0_new_data_148;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D24h
extern volatile uint8_t xdata g_rw_gamvrr0_0095h_GAM_VRR_LUT_OFST12_0_new_data_149;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D25h

extern volatile uint8_t xdata g_rw_gamvrr0_0096h_GAM_VRR_LUT_OFST12_0_new_data_150;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D26h
extern volatile uint8_t xdata g_rw_gamvrr0_0097h_GAM_VRR_LUT_OFST12_0_new_data_151;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D27h
extern volatile uint8_t xdata g_rw_gamvrr0_0098h_GAM_VRR_LUT_OFST12_0_new_data_152;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D28h
extern volatile uint8_t xdata g_rw_gamvrr0_0099h_GAM_VRR_LUT_OFST12_0_new_data_153;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D29h
extern volatile uint8_t xdata g_rw_gamvrr0_009Ah_GAM_VRR_LUT_OFST12_0_new_data_154;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D2Ah

extern volatile uint8_t xdata g_rw_gamvrr0_009Bh_GAM_VRR_LUT_OFST12_0_new_data_155;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D2Bh
extern volatile uint8_t xdata g_rw_gamvrr0_009Ch_GAM_VRR_LUT_OFST12_0_new_data_156;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D2Ch
extern volatile uint8_t xdata g_rw_gamvrr0_009Dh_GAM_VRR_LUT_OFST12_0_new_data_157;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D2Dh
extern volatile uint8_t xdata g_rw_gamvrr0_009Eh_GAM_VRR_LUT_OFST12_0_new_data_158;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D2Eh
extern volatile uint8_t xdata g_rw_gamvrr0_009Fh_GAM_VRR_LUT_OFST12_0_new_data_159;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D2Fh

extern volatile uint8_t xdata g_rw_gamvrr0_00A0h_GAM_VRR_LUT_OFST12_0_new_data_160;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D30h
extern volatile uint8_t xdata g_rw_gamvrr0_00A1h_GAM_VRR_LUT_OFST12_0_new_data_161;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D31h
extern volatile uint8_t xdata g_rw_gamvrr0_00A2h_GAM_VRR_LUT_OFST12_0_new_data_162;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D32h
extern volatile uint8_t xdata g_rw_gamvrr0_00A3h_GAM_VRR_LUT_OFST12_0_new_data_163;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D33h
extern volatile uint8_t xdata g_rw_gamvrr0_00A4h_GAM_VRR_LUT_OFST12_0_new_data_164;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D34h

extern volatile uint8_t xdata g_rw_gamvrr0_00A5h_GAM_VRR_LUT_OFST12_0_new_data_165;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D35h
extern volatile uint8_t xdata g_rw_gamvrr0_00A6h_GAM_VRR_LUT_OFST12_0_new_data_166;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D36h
extern volatile uint8_t xdata g_rw_gamvrr0_00A7h_GAM_VRR_LUT_OFST12_0_new_data_167;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D37h
extern volatile uint8_t xdata g_rw_gamvrr0_00A8h_GAM_VRR_LUT_OFST12_0_new_data_168;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D38h
extern volatile uint8_t xdata g_rw_gamvrr0_00A9h_GAM_VRR_LUT_OFST12_0_new_data_169;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D39h

extern volatile uint8_t xdata g_rw_gamvrr0_00AAh_GAM_VRR_LUT_OFST12_0_new_data_170;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D3Ah
extern volatile uint8_t xdata g_rw_gamvrr0_00ABh_GAM_VRR_LUT_OFST12_0_new_data_171;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D3Bh
extern volatile uint8_t xdata g_rw_gamvrr0_00ACh_GAM_VRR_LUT_OFST12_0_new_data_172;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D3Ch
extern volatile uint8_t xdata g_rw_gamvrr0_00ADh_GAM_VRR_LUT_OFST12_0_new_data_173;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D3Dh
extern volatile uint8_t xdata g_rw_gamvrr0_00AEh_GAM_VRR_LUT_OFST12_0_new_data_174;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D3Eh

extern volatile uint8_t xdata g_rw_gamvrr0_00AFh_GAM_VRR_LUT_OFST12_0_new_data_175;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D3Fh
extern volatile uint8_t xdata g_rw_gamvrr0_00B0h_GAM_VRR_LUT_OFST12_0_new_data_176;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D40h
extern volatile uint8_t xdata g_rw_gamvrr0_00B1h_GAM_VRR_LUT_OFST12_0_new_data_177;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D41h
extern volatile uint8_t xdata g_rw_gamvrr0_00B2h_GAM_VRR_LUT_OFST12_0_new_data_178;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D42h
extern volatile uint8_t xdata g_rw_gamvrr0_00B3h_GAM_VRR_LUT_OFST12_0_new_data_179;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D43h

extern volatile uint8_t xdata g_rw_gamvrr0_00B4h_GAM_VRR_LUT_OFST12_0_new_data_180;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D44h
extern volatile uint8_t xdata g_rw_gamvrr0_00B5h_GAM_VRR_LUT_OFST12_0_new_data_181;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D45h
extern volatile uint8_t xdata g_rw_gamvrr0_00B6h_GAM_VRR_LUT_OFST12_0_new_data_182;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D46h
extern volatile uint8_t xdata g_rw_gamvrr0_00B7h_GAM_VRR_LUT_OFST12_0_new_data_183;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D47h
extern volatile uint8_t xdata g_rw_gamvrr0_00B8h_GAM_VRR_LUT_OFST12_0_new_data_184;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D48h

extern volatile uint8_t xdata g_rw_gamvrr0_00B9h_GAM_VRR_LUT_OFST12_0_new_data_185;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D49h
extern volatile uint8_t xdata g_rw_gamvrr0_00BAh_GAM_VRR_LUT_OFST12_0_new_data_186;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D4Ah
extern volatile uint8_t xdata g_rw_gamvrr0_00BBh_GAM_VRR_LUT_OFST12_0_new_data_187;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D4Bh
extern volatile uint8_t xdata g_rw_gamvrr0_00BCh_GAM_VRR_LUT_OFST12_0_new_data_188;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D4Ch
extern volatile uint8_t xdata g_rw_gamvrr0_00BDh_GAM_VRR_LUT_OFST12_0_new_data_189;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D4Dh

extern volatile uint8_t xdata g_rw_gamvrr0_00BEh_GAM_VRR_LUT_OFST12_0_new_data_190;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D4Eh
extern volatile uint8_t xdata g_rw_gamvrr0_00BFh_GAM_VRR_LUT_OFST12_0_new_data_191;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D4Fh
extern volatile uint8_t xdata g_rw_gamvrr0_00C0h_GAM_VRR_LUT_OFST12_0_new_data_192;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D50h
extern volatile uint8_t xdata g_rw_gamvrr0_00C1h_GAM_VRR_LUT_OFST12_0_new_data_193;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D51h
extern volatile uint8_t xdata g_rw_gamvrr0_00C2h_GAM_VRR_LUT_OFST12_0_new_data_194;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D52h

extern volatile uint8_t xdata g_rw_gamvrr0_00C3h_GAM_VRR_LUT_OFST12_0_new_data_195;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D53h
extern volatile uint8_t xdata g_rw_gamvrr0_00C4h_GAM_VRR_LUT_OFST12_0_new_data_196;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D54h
extern volatile uint8_t xdata g_rw_gamvrr0_00C5h_GAM_VRR_LUT_OFST12_0_new_data_197;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D55h
extern volatile uint8_t xdata g_rw_gamvrr0_00C6h_GAM_VRR_LUT_OFST12_0_new_data_198;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D56h
extern volatile uint8_t xdata g_rw_gamvrr0_00C7h_GAM_VRR_LUT_OFST12_0_new_data_199;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D57h

extern volatile uint8_t xdata g_rw_gamvrr0_00C8h_GAM_VRR_LUT_OFST12_0_new_data_200;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D58h
extern volatile uint8_t xdata g_rw_gamvrr0_00C9h_GAM_VRR_LUT_OFST12_0_new_data_201;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D59h
extern volatile uint8_t xdata g_rw_gamvrr0_00CAh_GAM_VRR_LUT_OFST12_0_new_data_202;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D5Ah
extern volatile uint8_t xdata g_rw_gamvrr0_00CBh_GAM_VRR_LUT_OFST12_0_new_data_203;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D5Bh
extern volatile uint8_t xdata g_rw_gamvrr0_00CCh_GAM_VRR_LUT_OFST12_0_new_data_204;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D5Ch

extern volatile uint8_t xdata g_rw_gamvrr0_00CDh_GAM_VRR_LUT_OFST12_0_new_data_205;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D5Dh
extern volatile uint8_t xdata g_rw_gamvrr0_00CEh_GAM_VRR_LUT_OFST12_0_new_data_206;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D5Eh
extern volatile uint8_t xdata g_rw_gamvrr0_00CFh_GAM_VRR_LUT_OFST12_0_new_data_207;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D5Fh
extern volatile uint8_t xdata g_rw_gamvrr0_00D0h_GAM_VRR_LUT_OFST12_0_new_data_208;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D60h
extern volatile uint8_t xdata g_rw_gamvrr0_00D1h_GAM_VRR_LUT_OFST12_0_new_data_209;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D61h

extern volatile uint8_t xdata g_rw_gamvrr0_00D2h_GAM_VRR_LUT_OFST12_0_new_data_210;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D62h
extern volatile uint8_t xdata g_rw_gamvrr0_00D3h_GAM_VRR_LUT_OFST12_0_new_data_211;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D63h
extern volatile uint8_t xdata g_rw_gamvrr0_00D4h_GAM_VRR_LUT_OFST12_0_new_data_212;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D64h
extern volatile uint8_t xdata g_rw_gamvrr0_00D5h_GAM_VRR_LUT_OFST12_0_new_data_213;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D65h
extern volatile uint8_t xdata g_rw_gamvrr0_00D6h_GAM_VRR_LUT_OFST12_0_new_data_214;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D66h

extern volatile uint8_t xdata g_rw_gamvrr0_00D7h_GAM_VRR_LUT_OFST12_0_new_data_215;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D67h
extern volatile uint8_t xdata g_rw_gamvrr0_00D8h_GAM_VRR_LUT_OFST12_0_new_data_216;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D68h
extern volatile uint8_t xdata g_rw_gamvrr0_00D9h_GAM_VRR_LUT_OFST12_0_new_data_217;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D69h
extern volatile uint8_t xdata g_rw_gamvrr0_00DAh_GAM_VRR_LUT_OFST12_0_new_data_218;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D6Ah
extern volatile uint8_t xdata g_rw_gamvrr0_00DBh_GAM_VRR_LUT_OFST12_0_new_data_219;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D6Bh

extern volatile uint8_t xdata g_rw_gamvrr0_00DCh_GAM_VRR_LUT_OFST12_0_new_data_220;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D6Ch
extern volatile uint8_t xdata g_rw_gamvrr0_00DDh_GAM_VRR_LUT_OFST12_0_new_data_221;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D6Dh
extern volatile uint8_t xdata g_rw_gamvrr0_00DEh_GAM_VRR_LUT_OFST12_0_new_data_222;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D6Eh
extern volatile uint8_t xdata g_rw_gamvrr0_00DFh_GAM_VRR_LUT_OFST12_0_new_data_223;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D6Fh
extern volatile uint8_t xdata g_rw_gamvrr0_00E0h_GAM_VRR_LUT_OFST12_0_new_data_224;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D70h

extern volatile uint8_t xdata g_rw_gamvrr0_00E1h_GAM_VRR_LUT_OFST12_0_new_data_225;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D71h
extern volatile uint8_t xdata g_rw_gamvrr0_00E2h_GAM_VRR_LUT_OFST12_0_new_data_226;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D72h
extern volatile uint8_t xdata g_rw_gamvrr0_00E3h_GAM_VRR_LUT_OFST12_0_new_data_227;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D73h
extern volatile uint8_t xdata g_rw_gamvrr0_00E4h_GAM_VRR_LUT_OFST12_0_new_data_228;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D74h
extern volatile uint8_t xdata g_rw_gamvrr0_00E5h_GAM_VRR_LUT_OFST12_0_new_data_229;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D75h

extern volatile uint8_t xdata g_rw_gamvrr0_00E6h_GAM_VRR_LUT_OFST12_0_new_data_230;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D76h
extern volatile uint8_t xdata g_rw_gamvrr0_00E7h_GAM_VRR_LUT_OFST12_0_new_data_231;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D77h
extern volatile uint8_t xdata g_rw_gamvrr0_00E8h_GAM_VRR_LUT_OFST12_0_new_data_232;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D78h
extern volatile uint8_t xdata g_rw_gamvrr0_00E9h_GAM_VRR_LUT_OFST12_0_new_data_233;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D79h
extern volatile uint8_t xdata g_rw_gamvrr0_00EAh_GAM_VRR_LUT_OFST12_0_new_data_234;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D7Ah

extern volatile uint8_t xdata g_rw_gamvrr0_00EBh_GAM_VRR_LUT_OFST12_0_new_data_235;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D7Bh
extern volatile uint8_t xdata g_rw_gamvrr0_00ECh_GAM_VRR_LUT_OFST12_0_new_data_236;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D7Ch
extern volatile uint8_t xdata g_rw_gamvrr0_00EDh_GAM_VRR_LUT_OFST12_0_new_data_237;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D7Dh
extern volatile uint8_t xdata g_rw_gamvrr0_00EEh_GAM_VRR_LUT_OFST12_0_new_data_238;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D7Eh
extern volatile uint8_t xdata g_rw_gamvrr0_00EFh_GAM_VRR_LUT_OFST12_0_new_data_239;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D7Fh

extern volatile uint8_t xdata g_rw_gamvrr0_00F0h_GAM_VRR_LUT_OFST12_0_new_data_240;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D80h
extern volatile uint8_t xdata g_rw_gamvrr0_00F1h_GAM_VRR_LUT_OFST12_0_new_data_241;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D81h
extern volatile uint8_t xdata g_rw_gamvrr0_00F2h_GAM_VRR_LUT_OFST12_0_new_data_242;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D82h
extern volatile uint8_t xdata g_rw_gamvrr0_00F3h_GAM_VRR_LUT_OFST12_0_new_data_243;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D83h
extern volatile uint8_t xdata g_rw_gamvrr0_00F4h_GAM_VRR_LUT_OFST12_0_new_data_244;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D84h

extern volatile uint8_t xdata g_rw_gamvrr0_00F5h_GAM_VRR_LUT_OFST12_0_new_data_245;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D85h
extern volatile uint8_t xdata g_rw_gamvrr0_00F6h_GAM_VRR_LUT_OFST12_0_new_data_246;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D86h
extern volatile uint8_t xdata g_rw_gamvrr0_00F7h_GAM_VRR_LUT_OFST12_0_new_data_247;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D87h
extern volatile uint8_t xdata g_rw_gamvrr0_00F8h_GAM_VRR_LUT_OFST12_0_new_data_248;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D88h
extern volatile uint8_t xdata g_rw_gamvrr0_00F9h_GAM_VRR_LUT_OFST12_0_new_data_249;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D89h

extern volatile uint8_t xdata g_rw_gamvrr0_00FAh_GAM_VRR_LUT_OFST12_0_new_data_250;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D8Ah
extern volatile uint8_t xdata g_rw_gamvrr0_00FBh_GAM_VRR_LUT_OFST12_0_new_data_251;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D8Bh
extern volatile uint8_t xdata g_rw_gamvrr0_00FCh_GAM_VRR_LUT_OFST12_0_new_data_252;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D8Ch
extern volatile uint8_t xdata g_rw_gamvrr0_00FDh_GAM_VRR_LUT_OFST12_0_new_data_253;                      // [msb:lsb] = [7:0], val = 32, 	Absolute Address = 7D8Dh
extern volatile uint8_t xdata g_rw_gamvrr0_00FEh_GAM_VRR_LUT_OFST12_0_new_data_254;                      // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 7D8Eh

extern volatile uint8_t xdata g_rw_gamvrr0_00FFh_GAM_VRR_LUT_OFST12_0_new_data_255;                      // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 7D8Fh
extern volatile uint8_t xdata g_rw_gamvrr0_0100h_GAM_VRR_LUT_OFST12_0_new_data_256;                      // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 7D90h
extern volatile uint8_t xdata g_rw_gamvrr0_0101h_GAM_VRR_LUT_OFST12_0_new_data_257;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7D91h
extern volatile uint8_t xdata g_rw_gamvrr0_0102h_GAM_VRR_LUT_OFST12_0_new_data_258;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7D92h
extern volatile uint8_t xdata g_rw_gamvrr0_0103h_GAM_VRR_LUT_OFST12_0_new_data_259;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D93h

extern volatile uint8_t xdata g_rw_gamvrr0_0104h_GAM_VRR_LUT_OFST12_0_new_data_260;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D94h
extern volatile uint8_t xdata g_rw_gamvrr0_0105h_GAM_VRR_LUT_OFST12_0_new_data_261;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D95h
extern volatile uint8_t xdata g_rw_gamvrr0_0106h_GAM_VRR_LUT_OFST12_0_new_data_262;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D96h
extern volatile uint8_t xdata g_rw_gamvrr0_0107h_GAM_VRR_LUT_OFST12_0_new_data_263;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D97h
extern volatile uint8_t xdata g_rw_gamvrr0_0108h_GAM_VRR_LUT_OFST12_0_new_data_264;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D98h

extern volatile uint8_t xdata g_rw_gamvrr0_0109h_GAM_VRR_LUT_OFST12_0_new_data_265;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D99h
extern volatile uint8_t xdata g_rw_gamvrr0_010Ah_GAM_VRR_LUT_OFST12_0_new_data_266;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D9Ah
extern volatile uint8_t xdata g_rw_gamvrr0_010Bh_GAM_VRR_LUT_OFST12_0_new_data_267;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D9Bh
extern volatile uint8_t xdata g_rw_gamvrr0_010Ch_GAM_VRR_LUT_OFST12_0_new_data_268;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D9Ch
extern volatile uint8_t xdata g_rw_gamvrr0_010Dh_GAM_VRR_LUT_OFST12_0_new_data_269;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D9Dh

extern volatile uint8_t xdata g_rw_gamvrr0_010Eh_GAM_VRR_LUT_OFST12_0_new_data_270;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D9Eh
extern volatile uint8_t xdata g_rw_gamvrr0_010Fh_GAM_VRR_LUT_OFST12_0_new_data_271;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7D9Fh
extern volatile uint8_t xdata g_rw_gamvrr0_0110h_GAM_VRR_LUT_OFST12_0_new_data_272;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DA0h
extern volatile uint8_t xdata g_rw_gamvrr0_0111h_GAM_VRR_LUT_OFST12_0_new_data_273;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DA1h
extern volatile uint8_t xdata g_rw_gamvrr0_0112h_GAM_VRR_LUT_OFST12_0_new_data_274;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DA2h

extern volatile uint8_t xdata g_rw_gamvrr0_0113h_GAM_VRR_LUT_OFST12_0_new_data_275;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DA3h
extern volatile uint8_t xdata g_rw_gamvrr0_0114h_GAM_VRR_LUT_OFST12_0_new_data_276;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DA4h
extern volatile uint8_t xdata g_rw_gamvrr0_0115h_GAM_VRR_LUT_OFST12_0_new_data_277;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DA5h
extern volatile uint8_t xdata g_rw_gamvrr0_0116h_GAM_VRR_LUT_OFST12_0_new_data_278;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DA6h
extern volatile uint8_t xdata g_rw_gamvrr0_0117h_GAM_VRR_LUT_OFST12_0_new_data_279;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DA7h

extern volatile uint8_t xdata g_rw_gamvrr0_0118h_GAM_VRR_LUT_OFST12_0_new_data_280;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DA8h
extern volatile uint8_t xdata g_rw_gamvrr0_0119h_GAM_VRR_LUT_OFST12_0_new_data_281;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DA9h
extern volatile uint8_t xdata g_rw_gamvrr0_011Ah_GAM_VRR_LUT_OFST12_0_new_data_282;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DAAh
extern volatile uint8_t xdata g_rw_gamvrr0_011Bh_GAM_VRR_LUT_OFST12_0_new_data_283;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DABh
extern volatile uint8_t xdata g_rw_gamvrr0_011Ch_GAM_VRR_LUT_OFST12_0_new_data_284;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DACh

extern volatile uint8_t xdata g_rw_gamvrr0_011Dh_GAM_VRR_LUT_OFST12_0_new_data_285;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DADh
extern volatile uint8_t xdata g_rw_gamvrr0_011Eh_GAM_VRR_LUT_OFST12_0_new_data_286;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DAEh
extern volatile uint8_t xdata g_rw_gamvrr0_011Fh_GAM_VRR_LUT_OFST12_0_new_data_287;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DAFh
extern volatile uint8_t xdata g_rw_gamvrr0_0120h_GAM_VRR_LUT_OFST12_0_new_data_288;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DB0h
extern volatile uint8_t xdata g_rw_gamvrr0_0121h_GAM_VRR_LUT_OFST12_0_new_data_289;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DB1h

extern volatile uint8_t xdata g_rw_gamvrr0_0122h_GAM_VRR_LUT_OFST12_0_new_data_290;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DB2h
extern volatile uint8_t xdata g_rw_gamvrr0_0123h_GAM_VRR_LUT_OFST12_0_new_data_291;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DB3h
extern volatile uint8_t xdata g_rw_gamvrr0_0124h_GAM_VRR_LUT_OFST12_0_new_data_292;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DB4h
extern volatile uint8_t xdata g_rw_gamvrr0_0125h_GAM_VRR_LUT_OFST12_0_new_data_293;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DB5h
extern volatile uint8_t xdata g_rw_gamvrr0_0126h_GAM_VRR_LUT_OFST12_0_new_data_294;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DB6h

extern volatile uint8_t xdata g_rw_gamvrr0_0127h_GAM_VRR_LUT_OFST12_0_new_data_295;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DB7h
extern volatile uint8_t xdata g_rw_gamvrr0_0128h_GAM_VRR_LUT_OFST12_0_new_data_296;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DB8h
extern volatile uint8_t xdata g_rw_gamvrr0_0129h_GAM_VRR_LUT_OFST12_0_new_data_297;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DB9h
extern volatile uint8_t xdata g_rw_gamvrr0_012Ah_GAM_VRR_LUT_OFST12_0_new_data_298;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DBAh
extern volatile uint8_t xdata g_rw_gamvrr0_012Bh_GAM_VRR_LUT_OFST12_0_new_data_299;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DBBh

extern volatile uint8_t xdata g_rw_gamvrr0_012Ch_GAM_VRR_LUT_OFST12_0_new_data_300;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DBCh
extern volatile uint8_t xdata g_rw_gamvrr0_012Dh_GAM_VRR_LUT_OFST12_0_new_data_301;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DBDh
extern volatile uint8_t xdata g_rw_gamvrr0_012Eh_GAM_VRR_LUT_OFST12_0_new_data_302;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DBEh
extern volatile uint8_t xdata g_rw_gamvrr0_012Fh_GAM_VRR_LUT_OFST12_0_new_data_303;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DBFh
extern volatile uint8_t xdata g_rw_gamvrr0_0130h_GAM_VRR_LUT_OFST12_0_new_data_304;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DC0h

extern volatile uint8_t xdata g_rw_gamvrr0_0131h_GAM_VRR_LUT_OFST12_0_new_data_305;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DC1h
extern volatile uint8_t xdata g_rw_gamvrr0_0132h_GAM_VRR_LUT_OFST12_0_new_data_306;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DC2h
extern volatile uint8_t xdata g_rw_gamvrr0_0133h_GAM_VRR_LUT_OFST12_0_new_data_307;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DC3h
extern volatile uint8_t xdata g_rw_gamvrr0_0134h_GAM_VRR_LUT_OFST12_0_new_data_308;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DC4h
extern volatile uint8_t xdata g_rw_gamvrr0_0135h_GAM_VRR_LUT_OFST12_0_new_data_309;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DC5h

extern volatile uint8_t xdata g_rw_gamvrr0_0136h_GAM_VRR_LUT_OFST12_0_new_data_310;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DC6h
extern volatile uint8_t xdata g_rw_gamvrr0_0137h_GAM_VRR_LUT_OFST12_0_new_data_311;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DC7h
extern volatile uint8_t xdata g_rw_gamvrr0_0138h_GAM_VRR_LUT_OFST12_0_new_data_312;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DC8h
extern volatile uint8_t xdata g_rw_gamvrr0_0139h_GAM_VRR_LUT_OFST12_0_new_data_313;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DC9h
extern volatile uint8_t xdata g_rw_gamvrr0_013Ah_GAM_VRR_LUT_OFST12_0_new_data_314;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DCAh

extern volatile uint8_t xdata g_rw_gamvrr0_013Bh_GAM_VRR_LUT_OFST12_0_new_data_315;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DCBh
extern volatile uint8_t xdata g_rw_gamvrr0_013Ch_GAM_VRR_LUT_OFST12_0_new_data_316;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DCCh
extern volatile uint8_t xdata g_rw_gamvrr0_013Dh_GAM_VRR_LUT_OFST12_0_new_data_317;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DCDh
extern volatile uint8_t xdata g_rw_gamvrr0_013Eh_GAM_VRR_LUT_OFST12_0_new_data_318;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DCEh
extern volatile uint8_t xdata g_rw_gamvrr0_013Fh_GAM_VRR_LUT_OFST12_0_new_data_319;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DCFh

extern volatile uint8_t xdata g_rw_gamvrr0_0140h_GAM_VRR_LUT_OFST12_0_new_data_320;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DD0h
extern volatile uint8_t xdata g_rw_gamvrr0_0141h_GAM_VRR_LUT_OFST12_0_new_data_321;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DD1h
extern volatile uint8_t xdata g_rw_gamvrr0_0142h_GAM_VRR_LUT_OFST12_0_new_data_322;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DD2h
extern volatile uint8_t xdata g_rw_gamvrr0_0143h_GAM_VRR_LUT_OFST12_0_new_data_323;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DD3h
extern volatile uint8_t xdata g_rw_gamvrr0_0144h_GAM_VRR_LUT_OFST12_0_new_data_324;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DD4h

extern volatile uint8_t xdata g_rw_gamvrr0_0145h_GAM_VRR_LUT_OFST12_0_new_data_325;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DD5h
extern volatile uint8_t xdata g_rw_gamvrr0_0146h_GAM_VRR_LUT_OFST12_0_new_data_326;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DD6h
extern volatile uint8_t xdata g_rw_gamvrr0_0147h_GAM_VRR_LUT_OFST12_0_new_data_327;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DD7h
extern volatile uint8_t xdata g_rw_gamvrr0_0148h_GAM_VRR_LUT_OFST12_0_new_data_328;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DD8h
extern volatile uint8_t xdata g_rw_gamvrr0_0149h_GAM_VRR_LUT_OFST12_0_new_data_329;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DD9h

extern volatile uint8_t xdata g_rw_gamvrr0_014Ah_GAM_VRR_LUT_OFST12_0_new_data_330;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DDAh
extern volatile uint8_t xdata g_rw_gamvrr0_014Bh_GAM_VRR_LUT_OFST12_0_new_data_331;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DDBh
extern volatile uint8_t xdata g_rw_gamvrr0_014Ch_GAM_VRR_LUT_OFST12_0_new_data_332;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DDCh
extern volatile uint8_t xdata g_rw_gamvrr0_014Dh_GAM_VRR_LUT_OFST12_0_new_data_333;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DDDh
extern volatile uint8_t xdata g_rw_gamvrr0_014Eh_GAM_VRR_LUT_OFST12_0_new_data_334;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DDEh

extern volatile uint8_t xdata g_rw_gamvrr0_014Fh_GAM_VRR_LUT_OFST12_0_new_data_335;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DDFh
extern volatile uint8_t xdata g_rw_gamvrr0_0150h_GAM_VRR_LUT_OFST12_0_new_data_336;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DE0h
extern volatile uint8_t xdata g_rw_gamvrr0_0151h_GAM_VRR_LUT_OFST12_0_new_data_337;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DE1h
extern volatile uint8_t xdata g_rw_gamvrr0_0152h_GAM_VRR_LUT_OFST12_0_new_data_338;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DE2h
extern volatile uint8_t xdata g_rw_gamvrr0_0153h_GAM_VRR_LUT_OFST12_0_new_data_339;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DE3h

extern volatile uint8_t xdata g_rw_gamvrr0_0154h_GAM_VRR_LUT_OFST12_0_new_data_340;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DE4h
extern volatile uint8_t xdata g_rw_gamvrr0_0155h_GAM_VRR_LUT_OFST12_0_new_data_341;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DE5h
extern volatile uint8_t xdata g_rw_gamvrr0_0156h_GAM_VRR_LUT_OFST12_0_new_data_342;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DE6h
extern volatile uint8_t xdata g_rw_gamvrr0_0157h_GAM_VRR_LUT_OFST12_0_new_data_343;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DE7h
extern volatile uint8_t xdata g_rw_gamvrr0_0158h_GAM_VRR_LUT_OFST12_0_new_data_344;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DE8h

extern volatile uint8_t xdata g_rw_gamvrr0_0159h_GAM_VRR_LUT_OFST12_0_new_data_345;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DE9h
extern volatile uint8_t xdata g_rw_gamvrr0_015Ah_GAM_VRR_LUT_OFST12_0_new_data_346;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DEAh
extern volatile uint8_t xdata g_rw_gamvrr0_015Bh_GAM_VRR_LUT_OFST12_0_new_data_347;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DEBh
extern volatile uint8_t xdata g_rw_gamvrr0_015Ch_GAM_VRR_LUT_OFST12_0_new_data_348;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DECh
extern volatile uint8_t xdata g_rw_gamvrr0_015Dh_GAM_VRR_LUT_OFST12_0_new_data_349;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DEDh

extern volatile uint8_t xdata g_rw_gamvrr0_015Eh_GAM_VRR_LUT_OFST12_0_new_data_350;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DEEh
extern volatile uint8_t xdata g_rw_gamvrr0_015Fh_GAM_VRR_LUT_OFST12_0_new_data_351;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DEFh
extern volatile uint8_t xdata g_rw_gamvrr0_0160h_GAM_VRR_LUT_OFST12_0_new_data_352;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DF0h
extern volatile uint8_t xdata g_rw_gamvrr0_0161h_GAM_VRR_LUT_OFST12_0_new_data_353;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DF1h
extern volatile uint8_t xdata g_rw_gamvrr0_0162h_GAM_VRR_LUT_OFST12_0_new_data_354;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DF2h

extern volatile uint8_t xdata g_rw_gamvrr0_0163h_GAM_VRR_LUT_OFST12_0_new_data_355;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DF3h
extern volatile uint8_t xdata g_rw_gamvrr0_0164h_GAM_VRR_LUT_OFST12_0_new_data_356;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DF4h
extern volatile uint8_t xdata g_rw_gamvrr0_0165h_GAM_VRR_LUT_OFST12_0_new_data_357;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DF5h
extern volatile uint8_t xdata g_rw_gamvrr0_0166h_GAM_VRR_LUT_OFST12_0_new_data_358;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DF6h
extern volatile uint8_t xdata g_rw_gamvrr0_0167h_GAM_VRR_LUT_OFST12_0_new_data_359;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DF7h

extern volatile uint8_t xdata g_rw_gamvrr0_0168h_GAM_VRR_LUT_OFST12_0_new_data_360;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DF8h
extern volatile uint8_t xdata g_rw_gamvrr0_0169h_GAM_VRR_LUT_OFST12_0_new_data_361;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DF9h
extern volatile uint8_t xdata g_rw_gamvrr0_016Ah_GAM_VRR_LUT_OFST12_0_new_data_362;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DFAh
extern volatile uint8_t xdata g_rw_gamvrr0_016Bh_GAM_VRR_LUT_OFST12_0_new_data_363;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DFBh
extern volatile uint8_t xdata g_rw_gamvrr0_016Ch_GAM_VRR_LUT_OFST12_0_new_data_364;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DFCh

extern volatile uint8_t xdata g_rw_gamvrr0_016Dh_GAM_VRR_LUT_OFST12_0_new_data_365;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DFDh
extern volatile uint8_t xdata g_rw_gamvrr0_016Eh_GAM_VRR_LUT_OFST12_0_new_data_366;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DFEh
extern volatile uint8_t xdata g_rw_gamvrr0_016Fh_GAM_VRR_LUT_OFST12_0_new_data_367;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7DFFh
extern volatile uint8_t xdata g_rw_gamvrr0_0170h_GAM_VRR_LUT_OFST12_0_new_data_368;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E00h
extern volatile uint8_t xdata g_rw_gamvrr0_0171h_GAM_VRR_LUT_OFST12_0_new_data_369;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E01h

extern volatile uint8_t xdata g_rw_gamvrr0_0172h_GAM_VRR_LUT_OFST12_0_new_data_370;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E02h
extern volatile uint8_t xdata g_rw_gamvrr0_0173h_GAM_VRR_LUT_OFST12_0_new_data_371;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E03h
extern volatile uint8_t xdata g_rw_gamvrr0_0174h_GAM_VRR_LUT_OFST12_0_new_data_372;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E04h
extern volatile uint8_t xdata g_rw_gamvrr0_0175h_GAM_VRR_LUT_OFST12_0_new_data_373;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E05h
extern volatile uint8_t xdata g_rw_gamvrr0_0176h_GAM_VRR_LUT_OFST12_0_new_data_374;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E06h

extern volatile uint8_t xdata g_rw_gamvrr0_0177h_GAM_VRR_LUT_OFST12_0_new_data_375;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E07h
extern volatile uint8_t xdata g_rw_gamvrr0_0178h_GAM_VRR_LUT_OFST12_0_new_data_376;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E08h
extern volatile uint8_t xdata g_rw_gamvrr0_0179h_GAM_VRR_LUT_OFST12_0_new_data_377;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E09h
extern volatile uint8_t xdata g_rw_gamvrr0_017Ah_GAM_VRR_LUT_OFST12_0_new_data_378;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E0Ah
extern volatile uint8_t xdata g_rw_gamvrr0_017Bh_GAM_VRR_LUT_OFST12_0_new_data_379;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E0Bh

extern volatile uint8_t xdata g_rw_gamvrr0_017Ch_GAM_VRR_LUT_OFST12_0_new_data_380;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E0Ch
extern volatile uint8_t xdata g_rw_gamvrr0_017Dh_GAM_VRR_LUT_OFST12_0_new_data_381;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E0Dh
extern volatile uint8_t xdata g_rw_gamvrr0_017Eh_GAM_VRR_LUT_OFST12_0_new_data_382;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E0Eh
extern volatile uint8_t xdata g_rw_gamvrr0_017Fh_GAM_VRR_LUT_OFST12_0_new_data_383;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E0Fh
extern volatile uint8_t xdata g_rw_gamvrr0_0180h_GAM_VRR_LUT_OFST12_0_new_data_384;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E10h

extern volatile uint8_t xdata g_rw_gamvrr0_0181h_GAM_VRR_LUT_OFST12_0_new_data_385;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E11h
extern volatile uint8_t xdata g_rw_gamvrr0_0182h_GAM_VRR_LUT_OFST12_0_new_data_386;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E12h
extern volatile uint8_t xdata g_rw_gamvrr0_0183h_GAM_VRR_LUT_OFST12_0_new_data_387;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E13h
extern volatile uint8_t xdata g_rw_gamvrr0_0184h_GAM_VRR_LUT_OFST12_0_new_data_388;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E14h
extern volatile uint8_t xdata g_rw_gamvrr0_0185h_GAM_VRR_LUT_OFST12_0_new_data_389;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E15h

extern volatile uint8_t xdata g_rw_gamvrr0_0186h_GAM_VRR_LUT_OFST12_0_new_data_390;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E16h
extern volatile uint8_t xdata g_rw_gamvrr0_0187h_GAM_VRR_LUT_OFST12_0_new_data_391;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E17h
extern volatile uint8_t xdata g_rw_gamvrr0_0188h_GAM_VRR_LUT_OFST12_0_new_data_392;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E18h
extern volatile uint8_t xdata g_rw_gamvrr0_0189h_GAM_VRR_LUT_OFST12_0_new_data_393;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E19h
extern volatile uint8_t xdata g_rw_gamvrr0_018Ah_GAM_VRR_LUT_OFST12_0_new_data_394;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E1Ah

extern volatile uint8_t xdata g_rw_gamvrr0_018Bh_GAM_VRR_LUT_OFST12_0_new_data_395;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E1Bh
extern volatile uint8_t xdata g_rw_gamvrr0_018Ch_GAM_VRR_LUT_OFST12_0_new_data_396;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E1Ch
extern volatile uint8_t xdata g_rw_gamvrr0_018Dh_GAM_VRR_LUT_OFST12_0_new_data_397;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E1Dh
extern volatile uint8_t xdata g_rw_gamvrr0_018Eh_GAM_VRR_LUT_OFST12_0_new_data_398;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E1Eh
extern volatile uint8_t xdata g_rw_gamvrr0_018Fh_GAM_VRR_LUT_OFST12_0_new_data_399;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E1Fh

extern volatile uint8_t xdata g_rw_gamvrr0_0190h_GAM_VRR_LUT_OFST12_0_new_data_400;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E20h
extern volatile uint8_t xdata g_rw_gamvrr0_0191h_GAM_VRR_LUT_OFST12_0_new_data_401;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E21h
extern volatile uint8_t xdata g_rw_gamvrr0_0192h_GAM_VRR_LUT_OFST12_0_new_data_402;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E22h
extern volatile uint8_t xdata g_rw_gamvrr0_0193h_GAM_VRR_LUT_OFST12_0_new_data_403;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E23h
extern volatile uint8_t xdata g_rw_gamvrr0_0194h_GAM_VRR_LUT_OFST12_0_new_data_404;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E24h

extern volatile uint8_t xdata g_rw_gamvrr0_0195h_GAM_VRR_LUT_OFST12_0_new_data_405;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E25h
extern volatile uint8_t xdata g_rw_gamvrr0_0196h_GAM_VRR_LUT_OFST12_0_new_data_406;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E26h
extern volatile uint8_t xdata g_rw_gamvrr0_0197h_GAM_VRR_LUT_OFST12_0_new_data_407;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E27h
extern volatile uint8_t xdata g_rw_gamvrr0_0198h_GAM_VRR_LUT_OFST12_0_new_data_408;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E28h
extern volatile uint8_t xdata g_rw_gamvrr0_0199h_GAM_VRR_LUT_OFST12_0_new_data_409;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E29h

extern volatile uint8_t xdata g_rw_gamvrr0_019Ah_GAM_VRR_LUT_OFST12_0_new_data_410;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E2Ah
extern volatile uint8_t xdata g_rw_gamvrr0_019Bh_GAM_VRR_LUT_OFST12_0_new_data_411;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E2Bh
extern volatile uint8_t xdata g_rw_gamvrr0_019Ch_GAM_VRR_LUT_OFST12_0_new_data_412;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E2Ch
extern volatile uint8_t xdata g_rw_gamvrr0_019Dh_GAM_VRR_LUT_OFST12_0_new_data_413;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E2Dh
extern volatile uint8_t xdata g_rw_gamvrr0_019Eh_GAM_VRR_LUT_OFST12_0_new_data_414;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E2Eh

extern volatile uint8_t xdata g_rw_gamvrr0_019Fh_GAM_VRR_LUT_OFST12_0_new_data_415;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E2Fh
extern volatile uint8_t xdata g_rw_gamvrr0_01A0h_GAM_VRR_LUT_OFST12_0_new_data_416;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E30h
extern volatile uint8_t xdata g_rw_gamvrr0_01A1h_GAM_VRR_LUT_OFST12_0_new_data_417;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E31h
extern volatile uint8_t xdata g_rw_gamvrr0_01A2h_GAM_VRR_LUT_OFST12_0_new_data_418;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E32h
extern volatile uint8_t xdata g_rw_gamvrr0_01A3h_GAM_VRR_LUT_OFST12_0_new_data_419;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E33h

extern volatile uint8_t xdata g_rw_gamvrr0_01A4h_GAM_VRR_LUT_OFST12_0_new_data_420;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E34h
extern volatile uint8_t xdata g_rw_gamvrr0_01A5h_GAM_VRR_LUT_OFST12_0_new_data_421;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E35h
extern volatile uint8_t xdata g_rw_gamvrr0_01A6h_GAM_VRR_LUT_OFST12_0_new_data_422;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E36h
extern volatile uint8_t xdata g_rw_gamvrr0_01A7h_GAM_VRR_LUT_OFST12_0_new_data_423;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E37h
extern volatile uint8_t xdata g_rw_gamvrr0_01A8h_GAM_VRR_LUT_OFST12_0_new_data_424;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E38h

extern volatile uint8_t xdata g_rw_gamvrr0_01A9h_GAM_VRR_LUT_OFST12_0_new_data_425;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E39h
extern volatile uint8_t xdata g_rw_gamvrr0_01AAh_GAM_VRR_LUT_OFST12_0_new_data_426;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E3Ah
extern volatile uint8_t xdata g_rw_gamvrr0_01ABh_GAM_VRR_LUT_OFST12_0_new_data_427;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E3Bh
extern volatile uint8_t xdata g_rw_gamvrr0_01ACh_GAM_VRR_LUT_OFST12_0_new_data_428;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E3Ch
extern volatile uint8_t xdata g_rw_gamvrr0_01ADh_GAM_VRR_LUT_OFST12_0_new_data_429;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E3Dh

extern volatile uint8_t xdata g_rw_gamvrr0_01AEh_GAM_VRR_LUT_OFST12_0_new_data_430;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E3Eh
extern volatile uint8_t xdata g_rw_gamvrr0_01AFh_GAM_VRR_LUT_OFST12_0_new_data_431;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E3Fh
extern volatile uint8_t xdata g_rw_gamvrr0_01B0h_GAM_VRR_LUT_OFST12_0_new_data_432;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E40h
extern volatile uint8_t xdata g_rw_gamvrr0_01B1h_GAM_VRR_LUT_OFST12_0_new_data_433;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E41h
extern volatile uint8_t xdata g_rw_gamvrr0_01B2h_GAM_VRR_LUT_OFST12_0_new_data_434;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E42h

extern volatile uint8_t xdata g_rw_gamvrr0_01B3h_GAM_VRR_LUT_OFST12_0_new_data_435;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E43h
extern volatile uint8_t xdata g_rw_gamvrr0_01B4h_GAM_VRR_LUT_OFST12_0_new_data_436;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E44h
extern volatile uint8_t xdata g_rw_gamvrr0_01B5h_GAM_VRR_LUT_OFST12_0_new_data_437;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E45h
extern volatile uint8_t xdata g_rw_gamvrr0_01B6h_GAM_VRR_LUT_OFST12_0_new_data_438;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E46h
extern volatile uint8_t xdata g_rw_gamvrr0_01B7h_GAM_VRR_LUT_OFST12_0_new_data_439;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E47h

extern volatile uint8_t xdata g_rw_gamvrr0_01B8h_GAM_VRR_LUT_OFST12_0_new_data_440;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E48h
extern volatile uint8_t xdata g_rw_gamvrr0_01B9h_GAM_VRR_LUT_OFST12_0_new_data_441;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E49h
extern volatile uint8_t xdata g_rw_gamvrr0_01BAh_GAM_VRR_LUT_OFST12_0_new_data_442;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E4Ah
extern volatile uint8_t xdata g_rw_gamvrr0_01BBh_GAM_VRR_LUT_OFST12_0_new_data_443;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E4Bh
extern volatile uint8_t xdata g_rw_gamvrr0_01BCh_GAM_VRR_LUT_OFST12_0_new_data_444;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E4Ch

extern volatile uint8_t xdata g_rw_gamvrr0_01BDh_GAM_VRR_LUT_OFST12_0_new_data_445;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E4Dh
extern volatile uint8_t xdata g_rw_gamvrr0_01BEh_GAM_VRR_LUT_OFST12_0_new_data_446;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E4Eh
extern volatile uint8_t xdata g_rw_gamvrr0_01BFh_GAM_VRR_LUT_OFST12_0_new_data_447;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E4Fh
extern volatile uint8_t xdata g_rw_gamvrr0_01C0h_GAM_VRR_LUT_OFST12_0_new_data_448;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E50h
extern volatile uint8_t xdata g_rw_gamvrr0_01C1h_GAM_VRR_LUT_OFST12_0_new_data_449;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E51h

extern volatile uint8_t xdata g_rw_gamvrr0_01C2h_GAM_VRR_LUT_OFST12_0_new_data_450;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E52h
extern volatile uint8_t xdata g_rw_gamvrr0_01C3h_GAM_VRR_LUT_OFST12_0_new_data_451;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E53h
extern volatile uint8_t xdata g_rw_gamvrr0_01C4h_GAM_VRR_LUT_OFST12_0_new_data_452;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E54h
extern volatile uint8_t xdata g_rw_gamvrr0_01C5h_GAM_VRR_LUT_OFST12_0_new_data_453;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E55h
extern volatile uint8_t xdata g_rw_gamvrr0_01C6h_GAM_VRR_LUT_OFST12_0_new_data_454;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E56h

extern volatile uint8_t xdata g_rw_gamvrr0_01C7h_GAM_VRR_LUT_OFST12_0_new_data_455;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E57h
extern volatile uint8_t xdata g_rw_gamvrr0_01C8h_GAM_VRR_LUT_OFST12_0_new_data_456;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E58h
extern volatile uint8_t xdata g_rw_gamvrr0_01C9h_GAM_VRR_LUT_OFST12_0_new_data_457;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E59h
extern volatile uint8_t xdata g_rw_gamvrr0_01CAh_GAM_VRR_LUT_OFST12_0_new_data_458;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E5Ah
extern volatile uint8_t xdata g_rw_gamvrr0_01CBh_GAM_VRR_LUT_OFST12_0_new_data_459;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E5Bh

extern volatile uint8_t xdata g_rw_gamvrr0_01CCh_GAM_VRR_LUT_OFST12_0_new_data_460;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E5Ch
extern volatile uint8_t xdata g_rw_gamvrr0_01CDh_GAM_VRR_LUT_OFST12_0_new_data_461;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E5Dh
extern volatile uint8_t xdata g_rw_gamvrr0_01CEh_GAM_VRR_LUT_OFST12_0_new_data_462;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E5Eh
extern volatile uint8_t xdata g_rw_gamvrr0_01CFh_GAM_VRR_LUT_OFST12_0_new_data_463;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E5Fh
extern volatile uint8_t xdata g_rw_gamvrr0_01D0h_GAM_VRR_LUT_OFST12_0_new_data_464;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E60h

extern volatile uint8_t xdata g_rw_gamvrr0_01D1h_GAM_VRR_LUT_OFST12_0_new_data_465;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E61h
extern volatile uint8_t xdata g_rw_gamvrr0_01D2h_GAM_VRR_LUT_OFST12_0_new_data_466;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E62h
extern volatile uint8_t xdata g_rw_gamvrr0_01D3h_GAM_VRR_LUT_OFST12_0_new_data_467;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E63h
extern volatile uint8_t xdata g_rw_gamvrr0_01D4h_GAM_VRR_LUT_OFST12_0_new_data_468;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E64h
extern volatile uint8_t xdata g_rw_gamvrr0_01D5h_GAM_VRR_LUT_OFST12_0_new_data_469;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E65h

extern volatile uint8_t xdata g_rw_gamvrr0_01D6h_GAM_VRR_LUT_OFST12_0_new_data_470;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E66h
extern volatile uint8_t xdata g_rw_gamvrr0_01D7h_GAM_VRR_LUT_OFST12_0_new_data_471;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E67h
extern volatile uint8_t xdata g_rw_gamvrr0_01D8h_GAM_VRR_LUT_OFST12_0_new_data_472;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E68h
extern volatile uint8_t xdata g_rw_gamvrr0_01D9h_GAM_VRR_LUT_OFST12_0_new_data_473;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E69h
extern volatile uint8_t xdata g_rw_gamvrr0_01DAh_GAM_VRR_LUT_OFST12_0_new_data_474;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E6Ah

extern volatile uint8_t xdata g_rw_gamvrr0_01DBh_GAM_VRR_LUT_OFST12_0_new_data_475;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E6Bh
extern volatile uint8_t xdata g_rw_gamvrr0_01DCh_GAM_VRR_LUT_OFST12_0_new_data_476;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E6Ch
extern volatile uint8_t xdata g_rw_gamvrr0_01DDh_GAM_VRR_LUT_OFST12_0_new_data_477;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E6Dh
extern volatile uint8_t xdata g_rw_gamvrr0_01DEh_GAM_VRR_LUT_OFST12_0_new_data_478;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E6Eh
extern volatile uint8_t xdata g_rw_gamvrr0_01DFh_GAM_VRR_LUT_OFST12_0_new_data_479;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E6Fh

extern volatile uint8_t xdata g_rw_gamvrr0_01E0h_GAM_VRR_LUT_OFST12_0_new_data_480;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E70h
extern volatile uint8_t xdata g_rw_gamvrr0_01E1h_GAM_VRR_LUT_OFST12_0_new_data_481;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E71h
extern volatile uint8_t xdata g_rw_gamvrr0_01E2h_GAM_VRR_LUT_OFST12_0_new_data_482;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E72h
extern volatile uint8_t xdata g_rw_gamvrr0_01E3h_GAM_VRR_LUT_OFST12_0_new_data_483;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E73h
extern volatile uint8_t xdata g_rw_gamvrr0_01E4h_GAM_VRR_LUT_OFST12_0_new_data_484;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E74h

extern volatile uint8_t xdata g_rw_gamvrr0_01E5h_GAM_VRR_LUT_OFST12_0_new_data_485;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E75h
extern volatile uint8_t xdata g_rw_gamvrr0_01E6h_GAM_VRR_LUT_OFST12_0_new_data_486;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E76h
extern volatile uint8_t xdata g_rw_gamvrr0_01E7h_GAM_VRR_LUT_OFST12_0_new_data_487;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E77h
extern volatile uint8_t xdata g_rw_gamvrr0_01E8h_GAM_VRR_LUT_OFST12_0_new_data_488;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E78h
extern volatile uint8_t xdata g_rw_gamvrr0_01E9h_GAM_VRR_LUT_OFST12_0_new_data_489;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E79h

extern volatile uint8_t xdata g_rw_gamvrr0_01EAh_GAM_VRR_LUT_OFST12_0_new_data_490;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E7Ah
extern volatile uint8_t xdata g_rw_gamvrr0_01EBh_GAM_VRR_LUT_OFST12_0_new_data_491;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E7Bh
extern volatile uint8_t xdata g_rw_gamvrr0_01ECh_GAM_VRR_LUT_OFST12_0_new_data_492;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E7Ch
extern volatile uint8_t xdata g_rw_gamvrr0_01EDh_GAM_VRR_LUT_OFST12_0_new_data_493;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E7Dh
extern volatile uint8_t xdata g_rw_gamvrr0_01EEh_GAM_VRR_LUT_OFST12_0_new_data_494;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E7Eh

extern volatile uint8_t xdata g_rw_gamvrr0_01EFh_GAM_VRR_LUT_OFST12_0_new_data_495;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E7Fh
extern volatile uint8_t xdata g_rw_gamvrr0_01F0h_GAM_VRR_LUT_OFST12_0_new_data_496;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E80h
extern volatile uint8_t xdata g_rw_gamvrr0_01F1h_GAM_VRR_LUT_OFST12_0_new_data_497;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E81h
extern volatile uint8_t xdata g_rw_gamvrr0_01F2h_GAM_VRR_LUT_OFST12_0_new_data_498;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E82h
extern volatile uint8_t xdata g_rw_gamvrr0_01F3h_GAM_VRR_LUT_OFST12_0_new_data_499;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E83h

extern volatile uint8_t xdata g_rw_gamvrr0_01F4h_GAM_VRR_LUT_OFST12_0_new_data_500;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E84h
extern volatile uint8_t xdata g_rw_gamvrr0_01F5h_GAM_VRR_LUT_OFST12_0_new_data_501;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E85h
extern volatile uint8_t xdata g_rw_gamvrr0_01F6h_GAM_VRR_LUT_OFST12_0_new_data_502;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E86h
extern volatile uint8_t xdata g_rw_gamvrr0_01F7h_GAM_VRR_LUT_OFST12_0_new_data_503;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E87h
extern volatile uint8_t xdata g_rw_gamvrr0_01F8h_GAM_VRR_LUT_OFST12_0_new_data_504;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E88h

extern volatile uint8_t xdata g_rw_gamvrr0_01F9h_GAM_VRR_LUT_OFST12_0_new_data_505;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E89h
extern volatile uint8_t xdata g_rw_gamvrr0_01FAh_GAM_VRR_LUT_OFST12_0_new_data_506;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E8Ah
extern volatile uint8_t xdata g_rw_gamvrr0_01FBh_GAM_VRR_LUT_OFST12_0_new_data_507;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E8Bh
extern volatile uint8_t xdata g_rw_gamvrr0_01FCh_GAM_VRR_LUT_OFST12_0_new_data_508;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E8Ch
extern volatile uint8_t xdata g_rw_gamvrr0_01FDh_GAM_VRR_LUT_OFST12_0_new_data_509;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E8Dh

extern volatile uint8_t xdata g_rw_gamvrr0_01FEh_GAM_VRR_LUT_OFST12_0_new_data_510;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E8Eh
extern volatile uint8_t xdata g_rw_gamvrr0_01FFh_GAM_VRR_LUT_OFST12_0_new_data_511;                      // [msb:lsb] = [7:0], val = 32, 	Absolute Address = 7E8Fh
extern volatile uint8_t xdata g_rw_gamvrr0_0200h_GAM_VRR_LUT_OFST12_0_new_data_512;                      // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 7E90h
extern volatile uint8_t xdata g_rw_gamvrr0_0201h_GAM_VRR_LUT_OFST12_0_new_data_513;                      // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 7E91h
extern volatile uint8_t xdata g_rw_gamvrr0_0202h_GAM_VRR_LUT_OFST12_0_new_data_514;                      // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 7E92h

extern volatile uint8_t xdata g_rw_gamvrr0_0203h_GAM_VRR_LUT_OFST12_0_new_data_515;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7E93h
extern volatile uint8_t xdata g_rw_gamvrr0_0204h_GAM_VRR_LUT_OFST12_0_new_data_516;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7E94h
extern volatile uint8_t xdata g_rw_gamvrr0_0205h_GAM_VRR_LUT_OFST12_0_new_data_517;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E95h
extern volatile uint8_t xdata g_rw_gamvrr0_0206h_GAM_VRR_LUT_OFST12_0_new_data_518;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E96h
extern volatile uint8_t xdata g_rw_gamvrr0_0207h_GAM_VRR_LUT_OFST12_0_new_data_519;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E97h

extern volatile uint8_t xdata g_rw_gamvrr0_0208h_GAM_VRR_LUT_OFST12_0_new_data_520;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E98h
extern volatile uint8_t xdata g_rw_gamvrr0_0209h_GAM_VRR_LUT_OFST12_0_new_data_521;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E99h
extern volatile uint8_t xdata g_rw_gamvrr0_020Ah_GAM_VRR_LUT_OFST12_0_new_data_522;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E9Ah
extern volatile uint8_t xdata g_rw_gamvrr0_020Bh_GAM_VRR_LUT_OFST12_0_new_data_523;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E9Bh
extern volatile uint8_t xdata g_rw_gamvrr0_020Ch_GAM_VRR_LUT_OFST12_0_new_data_524;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E9Ch

extern volatile uint8_t xdata g_rw_gamvrr0_020Dh_GAM_VRR_LUT_OFST12_0_new_data_525;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E9Dh
extern volatile uint8_t xdata g_rw_gamvrr0_020Eh_GAM_VRR_LUT_OFST12_0_new_data_526;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E9Eh
extern volatile uint8_t xdata g_rw_gamvrr0_020Fh_GAM_VRR_LUT_OFST12_0_new_data_527;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7E9Fh
extern volatile uint8_t xdata g_rw_gamvrr0_0210h_GAM_VRR_LUT_OFST12_0_new_data_528;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EA0h
extern volatile uint8_t xdata g_rw_gamvrr0_0211h_GAM_VRR_LUT_OFST12_0_new_data_529;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EA1h

extern volatile uint8_t xdata g_rw_gamvrr0_0212h_GAM_VRR_LUT_OFST12_0_new_data_530;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EA2h
extern volatile uint8_t xdata g_rw_gamvrr0_0213h_GAM_VRR_LUT_OFST12_0_new_data_531;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EA3h
extern volatile uint8_t xdata g_rw_gamvrr0_0214h_GAM_VRR_LUT_OFST12_0_new_data_532;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EA4h
extern volatile uint8_t xdata g_rw_gamvrr0_0215h_GAM_VRR_LUT_OFST12_0_new_data_533;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EA5h
extern volatile uint8_t xdata g_rw_gamvrr0_0216h_GAM_VRR_LUT_OFST12_0_new_data_534;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EA6h

extern volatile uint8_t xdata g_rw_gamvrr0_0217h_GAM_VRR_LUT_OFST12_0_new_data_535;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EA7h
extern volatile uint8_t xdata g_rw_gamvrr0_0218h_GAM_VRR_LUT_OFST12_0_new_data_536;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EA8h
extern volatile uint8_t xdata g_rw_gamvrr0_0219h_GAM_VRR_LUT_OFST12_0_new_data_537;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EA9h
extern volatile uint8_t xdata g_rw_gamvrr0_021Ah_GAM_VRR_LUT_OFST12_0_new_data_538;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EAAh
extern volatile uint8_t xdata g_rw_gamvrr0_021Bh_GAM_VRR_LUT_OFST12_0_new_data_539;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EABh

extern volatile uint8_t xdata g_rw_gamvrr0_021Ch_GAM_VRR_LUT_OFST12_0_new_data_540;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EACh
extern volatile uint8_t xdata g_rw_gamvrr0_021Dh_GAM_VRR_LUT_OFST12_0_new_data_541;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EADh
extern volatile uint8_t xdata g_rw_gamvrr0_021Eh_GAM_VRR_LUT_OFST12_0_new_data_542;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EAEh
extern volatile uint8_t xdata g_rw_gamvrr0_021Fh_GAM_VRR_LUT_OFST12_0_new_data_543;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EAFh
extern volatile uint8_t xdata g_rw_gamvrr0_0220h_GAM_VRR_LUT_OFST12_0_new_data_544;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EB0h

extern volatile uint8_t xdata g_rw_gamvrr0_0221h_GAM_VRR_LUT_OFST12_0_new_data_545;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EB1h
extern volatile uint8_t xdata g_rw_gamvrr0_0222h_GAM_VRR_LUT_OFST12_0_new_data_546;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EB2h
extern volatile uint8_t xdata g_rw_gamvrr0_0223h_GAM_VRR_LUT_OFST12_0_new_data_547;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EB3h
extern volatile uint8_t xdata g_rw_gamvrr0_0224h_GAM_VRR_LUT_OFST12_0_new_data_548;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EB4h
extern volatile uint8_t xdata g_rw_gamvrr0_0225h_GAM_VRR_LUT_OFST12_0_new_data_549;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EB5h

extern volatile uint8_t xdata g_rw_gamvrr0_0226h_GAM_VRR_LUT_OFST12_0_new_data_550;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EB6h
extern volatile uint8_t xdata g_rw_gamvrr0_0227h_GAM_VRR_LUT_OFST12_0_new_data_551;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EB7h
extern volatile uint8_t xdata g_rw_gamvrr0_0228h_GAM_VRR_LUT_OFST12_0_new_data_552;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EB8h
extern volatile uint8_t xdata g_rw_gamvrr0_0229h_GAM_VRR_LUT_OFST12_0_new_data_553;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EB9h
extern volatile uint8_t xdata g_rw_gamvrr0_022Ah_GAM_VRR_LUT_OFST12_0_new_data_554;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EBAh

extern volatile uint8_t xdata g_rw_gamvrr0_022Bh_GAM_VRR_LUT_OFST12_0_new_data_555;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EBBh
extern volatile uint8_t xdata g_rw_gamvrr0_022Ch_GAM_VRR_LUT_OFST12_0_new_data_556;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EBCh
extern volatile uint8_t xdata g_rw_gamvrr0_022Dh_GAM_VRR_LUT_OFST12_0_new_data_557;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EBDh
extern volatile uint8_t xdata g_rw_gamvrr0_022Eh_GAM_VRR_LUT_OFST12_0_new_data_558;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EBEh
extern volatile uint8_t xdata g_rw_gamvrr0_022Fh_GAM_VRR_LUT_OFST12_0_new_data_559;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EBFh

extern volatile uint8_t xdata g_rw_gamvrr0_0230h_GAM_VRR_LUT_OFST12_0_new_data_560;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EC0h
extern volatile uint8_t xdata g_rw_gamvrr0_0231h_GAM_VRR_LUT_OFST12_0_new_data_561;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EC1h
extern volatile uint8_t xdata g_rw_gamvrr0_0232h_GAM_VRR_LUT_OFST12_0_new_data_562;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EC2h
extern volatile uint8_t xdata g_rw_gamvrr0_0233h_GAM_VRR_LUT_OFST12_0_new_data_563;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EC3h
extern volatile uint8_t xdata g_rw_gamvrr0_0234h_GAM_VRR_LUT_OFST12_0_new_data_564;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EC4h

extern volatile uint8_t xdata g_rw_gamvrr0_0235h_GAM_VRR_LUT_OFST12_0_new_data_565;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EC5h
extern volatile uint8_t xdata g_rw_gamvrr0_0236h_GAM_VRR_LUT_OFST12_0_new_data_566;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EC6h
extern volatile uint8_t xdata g_rw_gamvrr0_0237h_GAM_VRR_LUT_OFST12_0_new_data_567;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EC7h
extern volatile uint8_t xdata g_rw_gamvrr0_0238h_GAM_VRR_LUT_OFST12_0_new_data_568;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EC8h
extern volatile uint8_t xdata g_rw_gamvrr0_0239h_GAM_VRR_LUT_OFST12_0_new_data_569;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EC9h

extern volatile uint8_t xdata g_rw_gamvrr0_023Ah_GAM_VRR_LUT_OFST12_0_new_data_570;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ECAh
extern volatile uint8_t xdata g_rw_gamvrr0_023Bh_GAM_VRR_LUT_OFST12_0_new_data_571;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ECBh
extern volatile uint8_t xdata g_rw_gamvrr0_023Ch_GAM_VRR_LUT_OFST12_0_new_data_572;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ECCh
extern volatile uint8_t xdata g_rw_gamvrr0_023Dh_GAM_VRR_LUT_OFST12_0_new_data_573;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ECDh
extern volatile uint8_t xdata g_rw_gamvrr0_023Eh_GAM_VRR_LUT_OFST12_0_new_data_574;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ECEh

extern volatile uint8_t xdata g_rw_gamvrr0_023Fh_GAM_VRR_LUT_OFST12_0_new_data_575;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ECFh
extern volatile uint8_t xdata g_rw_gamvrr0_0240h_GAM_VRR_LUT_OFST12_0_new_data_576;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ED0h
extern volatile uint8_t xdata g_rw_gamvrr0_0241h_GAM_VRR_LUT_OFST12_0_new_data_577;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ED1h
extern volatile uint8_t xdata g_rw_gamvrr0_0242h_GAM_VRR_LUT_OFST12_0_new_data_578;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ED2h
extern volatile uint8_t xdata g_rw_gamvrr0_0243h_GAM_VRR_LUT_OFST12_0_new_data_579;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ED3h

extern volatile uint8_t xdata g_rw_gamvrr0_0244h_GAM_VRR_LUT_OFST12_0_new_data_580;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ED4h
extern volatile uint8_t xdata g_rw_gamvrr0_0245h_GAM_VRR_LUT_OFST12_0_new_data_581;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ED5h
extern volatile uint8_t xdata g_rw_gamvrr0_0246h_GAM_VRR_LUT_OFST12_0_new_data_582;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ED6h
extern volatile uint8_t xdata g_rw_gamvrr0_0247h_GAM_VRR_LUT_OFST12_0_new_data_583;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ED7h
extern volatile uint8_t xdata g_rw_gamvrr0_0248h_GAM_VRR_LUT_OFST12_0_new_data_584;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ED8h

extern volatile uint8_t xdata g_rw_gamvrr0_0249h_GAM_VRR_LUT_OFST12_0_new_data_585;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ED9h
extern volatile uint8_t xdata g_rw_gamvrr0_024Ah_GAM_VRR_LUT_OFST12_0_new_data_586;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EDAh
extern volatile uint8_t xdata g_rw_gamvrr0_024Bh_GAM_VRR_LUT_OFST12_0_new_data_587;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EDBh
extern volatile uint8_t xdata g_rw_gamvrr0_024Ch_GAM_VRR_LUT_OFST12_0_new_data_588;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EDCh
extern volatile uint8_t xdata g_rw_gamvrr0_024Dh_GAM_VRR_LUT_OFST12_0_new_data_589;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EDDh

extern volatile uint8_t xdata g_rw_gamvrr0_024Eh_GAM_VRR_LUT_OFST12_0_new_data_590;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EDEh
extern volatile uint8_t xdata g_rw_gamvrr0_024Fh_GAM_VRR_LUT_OFST12_0_new_data_591;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EDFh
extern volatile uint8_t xdata g_rw_gamvrr0_0250h_GAM_VRR_LUT_OFST12_0_new_data_592;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EE0h
extern volatile uint8_t xdata g_rw_gamvrr0_0251h_GAM_VRR_LUT_OFST12_0_new_data_593;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EE1h
extern volatile uint8_t xdata g_rw_gamvrr0_0252h_GAM_VRR_LUT_OFST12_0_new_data_594;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EE2h

extern volatile uint8_t xdata g_rw_gamvrr0_0253h_GAM_VRR_LUT_OFST12_0_new_data_595;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EE3h
extern volatile uint8_t xdata g_rw_gamvrr0_0254h_GAM_VRR_LUT_OFST12_0_new_data_596;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EE4h
extern volatile uint8_t xdata g_rw_gamvrr0_0255h_GAM_VRR_LUT_OFST12_0_new_data_597;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EE5h
extern volatile uint8_t xdata g_rw_gamvrr0_0256h_GAM_VRR_LUT_OFST12_0_new_data_598;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EE6h
extern volatile uint8_t xdata g_rw_gamvrr0_0257h_GAM_VRR_LUT_OFST12_0_new_data_599;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EE7h

extern volatile uint8_t xdata g_rw_gamvrr0_0258h_GAM_VRR_LUT_OFST12_0_new_data_600;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EE8h
extern volatile uint8_t xdata g_rw_gamvrr0_0259h_GAM_VRR_LUT_OFST12_0_new_data_601;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EE9h
extern volatile uint8_t xdata g_rw_gamvrr0_025Ah_GAM_VRR_LUT_OFST12_0_new_data_602;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EEAh
extern volatile uint8_t xdata g_rw_gamvrr0_025Bh_GAM_VRR_LUT_OFST12_0_new_data_603;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EEBh
extern volatile uint8_t xdata g_rw_gamvrr0_025Ch_GAM_VRR_LUT_OFST12_0_new_data_604;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EECh

extern volatile uint8_t xdata g_rw_gamvrr0_025Dh_GAM_VRR_LUT_OFST12_0_new_data_605;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EEDh
extern volatile uint8_t xdata g_rw_gamvrr0_025Eh_GAM_VRR_LUT_OFST12_0_new_data_606;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EEEh
extern volatile uint8_t xdata g_rw_gamvrr0_025Fh_GAM_VRR_LUT_OFST12_0_new_data_607;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EEFh
extern volatile uint8_t xdata g_rw_gamvrr0_0260h_GAM_VRR_LUT_OFST12_0_new_data_608;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EF0h
extern volatile uint8_t xdata g_rw_gamvrr0_0261h_GAM_VRR_LUT_OFST12_0_new_data_609;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EF1h

extern volatile uint8_t xdata g_rw_gamvrr0_0262h_GAM_VRR_LUT_OFST12_0_new_data_610;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EF2h
extern volatile uint8_t xdata g_rw_gamvrr0_0263h_GAM_VRR_LUT_OFST12_0_new_data_611;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EF3h
extern volatile uint8_t xdata g_rw_gamvrr0_0264h_GAM_VRR_LUT_OFST12_0_new_data_612;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EF4h
extern volatile uint8_t xdata g_rw_gamvrr0_0265h_GAM_VRR_LUT_OFST12_0_new_data_613;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EF5h
extern volatile uint8_t xdata g_rw_gamvrr0_0266h_GAM_VRR_LUT_OFST12_0_new_data_614;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EF6h

extern volatile uint8_t xdata g_rw_gamvrr0_0267h_GAM_VRR_LUT_OFST12_0_new_data_615;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EF7h
extern volatile uint8_t xdata g_rw_gamvrr0_0268h_GAM_VRR_LUT_OFST12_0_new_data_616;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EF8h
extern volatile uint8_t xdata g_rw_gamvrr0_0269h_GAM_VRR_LUT_OFST12_0_new_data_617;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EF9h
extern volatile uint8_t xdata g_rw_gamvrr0_026Ah_GAM_VRR_LUT_OFST12_0_new_data_618;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EFAh
extern volatile uint8_t xdata g_rw_gamvrr0_026Bh_GAM_VRR_LUT_OFST12_0_new_data_619;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EFBh

extern volatile uint8_t xdata g_rw_gamvrr0_026Ch_GAM_VRR_LUT_OFST12_0_new_data_620;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EFCh
extern volatile uint8_t xdata g_rw_gamvrr0_026Dh_GAM_VRR_LUT_OFST12_0_new_data_621;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EFDh
extern volatile uint8_t xdata g_rw_gamvrr0_026Eh_GAM_VRR_LUT_OFST12_0_new_data_622;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EFEh
extern volatile uint8_t xdata g_rw_gamvrr0_026Fh_GAM_VRR_LUT_OFST12_0_new_data_623;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7EFFh
extern volatile uint8_t xdata g_rw_gamvrr0_0270h_GAM_VRR_LUT_OFST12_0_new_data_624;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F00h

extern volatile uint8_t xdata g_rw_gamvrr0_0271h_GAM_VRR_LUT_OFST12_0_new_data_625;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F01h
extern volatile uint8_t xdata g_rw_gamvrr0_0272h_GAM_VRR_LUT_OFST12_0_new_data_626;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F02h
extern volatile uint8_t xdata g_rw_gamvrr0_0273h_GAM_VRR_LUT_OFST12_0_new_data_627;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F03h
extern volatile uint8_t xdata g_rw_gamvrr0_0274h_GAM_VRR_LUT_OFST12_0_new_data_628;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F04h
extern volatile uint8_t xdata g_rw_gamvrr0_0275h_GAM_VRR_LUT_OFST12_0_new_data_629;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F05h

extern volatile uint8_t xdata g_rw_gamvrr0_0276h_GAM_VRR_LUT_OFST12_0_new_data_630;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F06h
extern volatile uint8_t xdata g_rw_gamvrr0_0277h_GAM_VRR_LUT_OFST12_0_new_data_631;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F07h
extern volatile uint8_t xdata g_rw_gamvrr0_0278h_GAM_VRR_LUT_OFST12_0_new_data_632;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F08h
extern volatile uint8_t xdata g_rw_gamvrr0_0279h_GAM_VRR_LUT_OFST12_0_new_data_633;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F09h
extern volatile uint8_t xdata g_rw_gamvrr0_027Ah_GAM_VRR_LUT_OFST12_0_new_data_634;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F0Ah

extern volatile uint8_t xdata g_rw_gamvrr0_027Bh_GAM_VRR_LUT_OFST12_0_new_data_635;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F0Bh
extern volatile uint8_t xdata g_rw_gamvrr0_027Ch_GAM_VRR_LUT_OFST12_0_new_data_636;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F0Ch
extern volatile uint8_t xdata g_rw_gamvrr0_027Dh_GAM_VRR_LUT_OFST12_0_new_data_637;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F0Dh
extern volatile uint8_t xdata g_rw_gamvrr0_027Eh_GAM_VRR_LUT_OFST12_0_new_data_638;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F0Eh
extern volatile uint8_t xdata g_rw_gamvrr0_027Fh_GAM_VRR_LUT_OFST12_0_new_data_639;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F0Fh

extern volatile uint8_t xdata g_rw_gamvrr0_0280h_GAM_VRR_LUT_OFST12_0_new_data_640;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F10h
extern volatile uint8_t xdata g_rw_gamvrr0_0281h_GAM_VRR_LUT_OFST12_0_new_data_641;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F11h
extern volatile uint8_t xdata g_rw_gamvrr0_0282h_GAM_VRR_LUT_OFST12_0_new_data_642;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F12h
extern volatile uint8_t xdata g_rw_gamvrr0_0283h_GAM_VRR_LUT_OFST12_0_new_data_643;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F13h
extern volatile uint8_t xdata g_rw_gamvrr0_0284h_GAM_VRR_LUT_OFST12_0_new_data_644;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F14h

extern volatile uint8_t xdata g_rw_gamvrr0_0285h_GAM_VRR_LUT_OFST12_0_new_data_645;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F15h
extern volatile uint8_t xdata g_rw_gamvrr0_0286h_GAM_VRR_LUT_OFST12_0_new_data_646;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F16h
extern volatile uint8_t xdata g_rw_gamvrr0_0287h_GAM_VRR_LUT_OFST12_0_new_data_647;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F17h
extern volatile uint8_t xdata g_rw_gamvrr0_0288h_GAM_VRR_LUT_OFST12_0_new_data_648;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F18h
extern volatile uint8_t xdata g_rw_gamvrr0_0289h_GAM_VRR_LUT_OFST12_0_new_data_649;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F19h

extern volatile uint8_t xdata g_rw_gamvrr0_028Ah_GAM_VRR_LUT_OFST12_0_new_data_650;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F1Ah
extern volatile uint8_t xdata g_rw_gamvrr0_028Bh_GAM_VRR_LUT_OFST12_0_new_data_651;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F1Bh
extern volatile uint8_t xdata g_rw_gamvrr0_028Ch_GAM_VRR_LUT_OFST12_0_new_data_652;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F1Ch
extern volatile uint8_t xdata g_rw_gamvrr0_028Dh_GAM_VRR_LUT_OFST12_0_new_data_653;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F1Dh
extern volatile uint8_t xdata g_rw_gamvrr0_028Eh_GAM_VRR_LUT_OFST12_0_new_data_654;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F1Eh

extern volatile uint8_t xdata g_rw_gamvrr0_028Fh_GAM_VRR_LUT_OFST12_0_new_data_655;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F1Fh
extern volatile uint8_t xdata g_rw_gamvrr0_0290h_GAM_VRR_LUT_OFST12_0_new_data_656;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F20h
extern volatile uint8_t xdata g_rw_gamvrr0_0291h_GAM_VRR_LUT_OFST12_0_new_data_657;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F21h
extern volatile uint8_t xdata g_rw_gamvrr0_0292h_GAM_VRR_LUT_OFST12_0_new_data_658;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F22h
extern volatile uint8_t xdata g_rw_gamvrr0_0293h_GAM_VRR_LUT_OFST12_0_new_data_659;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F23h

extern volatile uint8_t xdata g_rw_gamvrr0_0294h_GAM_VRR_LUT_OFST12_0_new_data_660;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F24h
extern volatile uint8_t xdata g_rw_gamvrr0_0295h_GAM_VRR_LUT_OFST12_0_new_data_661;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F25h
extern volatile uint8_t xdata g_rw_gamvrr0_0296h_GAM_VRR_LUT_OFST12_0_new_data_662;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F26h
extern volatile uint8_t xdata g_rw_gamvrr0_0297h_GAM_VRR_LUT_OFST12_0_new_data_663;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F27h
extern volatile uint8_t xdata g_rw_gamvrr0_0298h_GAM_VRR_LUT_OFST12_0_new_data_664;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F28h

extern volatile uint8_t xdata g_rw_gamvrr0_0299h_GAM_VRR_LUT_OFST12_0_new_data_665;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F29h
extern volatile uint8_t xdata g_rw_gamvrr0_029Ah_GAM_VRR_LUT_OFST12_0_new_data_666;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F2Ah
extern volatile uint8_t xdata g_rw_gamvrr0_029Bh_GAM_VRR_LUT_OFST12_0_new_data_667;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F2Bh
extern volatile uint8_t xdata g_rw_gamvrr0_029Ch_GAM_VRR_LUT_OFST12_0_new_data_668;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F2Ch
extern volatile uint8_t xdata g_rw_gamvrr0_029Dh_GAM_VRR_LUT_OFST12_0_new_data_669;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F2Dh

extern volatile uint8_t xdata g_rw_gamvrr0_029Eh_GAM_VRR_LUT_OFST12_0_new_data_670;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F2Eh
extern volatile uint8_t xdata g_rw_gamvrr0_029Fh_GAM_VRR_LUT_OFST12_0_new_data_671;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F2Fh
extern volatile uint8_t xdata g_rw_gamvrr0_02A0h_GAM_VRR_LUT_OFST12_0_new_data_672;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F30h
extern volatile uint8_t xdata g_rw_gamvrr0_02A1h_GAM_VRR_LUT_OFST12_0_new_data_673;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F31h
extern volatile uint8_t xdata g_rw_gamvrr0_02A2h_GAM_VRR_LUT_OFST12_0_new_data_674;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F32h

extern volatile uint8_t xdata g_rw_gamvrr0_02A3h_GAM_VRR_LUT_OFST12_0_new_data_675;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F33h
extern volatile uint8_t xdata g_rw_gamvrr0_02A4h_GAM_VRR_LUT_OFST12_0_new_data_676;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F34h
extern volatile uint8_t xdata g_rw_gamvrr0_02A5h_GAM_VRR_LUT_OFST12_0_new_data_677;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F35h
extern volatile uint8_t xdata g_rw_gamvrr0_02A6h_GAM_VRR_LUT_OFST12_0_new_data_678;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F36h
extern volatile uint8_t xdata g_rw_gamvrr0_02A7h_GAM_VRR_LUT_OFST12_0_new_data_679;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F37h

extern volatile uint8_t xdata g_rw_gamvrr0_02A8h_GAM_VRR_LUT_OFST12_0_new_data_680;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F38h
extern volatile uint8_t xdata g_rw_gamvrr0_02A9h_GAM_VRR_LUT_OFST12_0_new_data_681;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F39h
extern volatile uint8_t xdata g_rw_gamvrr0_02AAh_GAM_VRR_LUT_OFST12_0_new_data_682;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F3Ah
extern volatile uint8_t xdata g_rw_gamvrr0_02ABh_GAM_VRR_LUT_OFST12_0_new_data_683;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F3Bh
extern volatile uint8_t xdata g_rw_gamvrr0_02ACh_GAM_VRR_LUT_OFST12_0_new_data_684;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F3Ch

extern volatile uint8_t xdata g_rw_gamvrr0_02ADh_GAM_VRR_LUT_OFST12_0_new_data_685;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F3Dh
extern volatile uint8_t xdata g_rw_gamvrr0_02AEh_GAM_VRR_LUT_OFST12_0_new_data_686;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F3Eh
extern volatile uint8_t xdata g_rw_gamvrr0_02AFh_GAM_VRR_LUT_OFST12_0_new_data_687;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F3Fh
extern volatile uint8_t xdata g_rw_gamvrr0_02B0h_GAM_VRR_LUT_OFST12_0_new_data_688;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F40h
extern volatile uint8_t xdata g_rw_gamvrr0_02B1h_GAM_VRR_LUT_OFST12_0_new_data_689;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F41h

extern volatile uint8_t xdata g_rw_gamvrr0_02B2h_GAM_VRR_LUT_OFST12_0_new_data_690;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F42h
extern volatile uint8_t xdata g_rw_gamvrr0_02B3h_GAM_VRR_LUT_OFST12_0_new_data_691;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F43h
extern volatile uint8_t xdata g_rw_gamvrr0_02B4h_GAM_VRR_LUT_OFST12_0_new_data_692;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F44h
extern volatile uint8_t xdata g_rw_gamvrr0_02B5h_GAM_VRR_LUT_OFST12_0_new_data_693;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F45h
extern volatile uint8_t xdata g_rw_gamvrr0_02B6h_GAM_VRR_LUT_OFST12_0_new_data_694;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F46h

extern volatile uint8_t xdata g_rw_gamvrr0_02B7h_GAM_VRR_LUT_OFST12_0_new_data_695;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F47h
extern volatile uint8_t xdata g_rw_gamvrr0_02B8h_GAM_VRR_LUT_OFST12_0_new_data_696;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F48h
extern volatile uint8_t xdata g_rw_gamvrr0_02B9h_GAM_VRR_LUT_OFST12_0_new_data_697;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F49h
extern volatile uint8_t xdata g_rw_gamvrr0_02BAh_GAM_VRR_LUT_OFST12_0_new_data_698;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F4Ah
extern volatile uint8_t xdata g_rw_gamvrr0_02BBh_GAM_VRR_LUT_OFST12_0_new_data_699;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F4Bh

extern volatile uint8_t xdata g_rw_gamvrr0_02BCh_GAM_VRR_LUT_OFST12_0_new_data_700;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F4Ch
extern volatile uint8_t xdata g_rw_gamvrr0_02BDh_GAM_VRR_LUT_OFST12_0_new_data_701;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F4Dh
extern volatile uint8_t xdata g_rw_gamvrr0_02BEh_GAM_VRR_LUT_OFST12_0_new_data_702;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F4Eh
extern volatile uint8_t xdata g_rw_gamvrr0_02BFh_GAM_VRR_LUT_OFST12_0_new_data_703;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F4Fh
extern volatile uint8_t xdata g_rw_gamvrr0_02C0h_GAM_VRR_LUT_OFST12_0_new_data_704;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F50h

extern volatile uint8_t xdata g_rw_gamvrr0_02C1h_GAM_VRR_LUT_OFST12_0_new_data_705;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F51h
extern volatile uint8_t xdata g_rw_gamvrr0_02C2h_GAM_VRR_LUT_OFST12_0_new_data_706;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F52h
extern volatile uint8_t xdata g_rw_gamvrr0_02C3h_GAM_VRR_LUT_OFST12_0_new_data_707;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F53h
extern volatile uint8_t xdata g_rw_gamvrr0_02C4h_GAM_VRR_LUT_OFST12_0_new_data_708;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F54h
extern volatile uint8_t xdata g_rw_gamvrr0_02C5h_GAM_VRR_LUT_OFST12_0_new_data_709;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F55h

extern volatile uint8_t xdata g_rw_gamvrr0_02C6h_GAM_VRR_LUT_OFST12_0_new_data_710;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F56h
extern volatile uint8_t xdata g_rw_gamvrr0_02C7h_GAM_VRR_LUT_OFST12_0_new_data_711;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F57h
extern volatile uint8_t xdata g_rw_gamvrr0_02C8h_GAM_VRR_LUT_OFST12_0_new_data_712;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F58h
extern volatile uint8_t xdata g_rw_gamvrr0_02C9h_GAM_VRR_LUT_OFST12_0_new_data_713;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F59h
extern volatile uint8_t xdata g_rw_gamvrr0_02CAh_GAM_VRR_LUT_OFST12_0_new_data_714;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F5Ah

extern volatile uint8_t xdata g_rw_gamvrr0_02CBh_GAM_VRR_LUT_OFST12_0_new_data_715;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F5Bh
extern volatile uint8_t xdata g_rw_gamvrr0_02CCh_GAM_VRR_LUT_OFST12_0_new_data_716;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F5Ch
extern volatile uint8_t xdata g_rw_gamvrr0_02CDh_GAM_VRR_LUT_OFST12_0_new_data_717;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F5Dh
extern volatile uint8_t xdata g_rw_gamvrr0_02CEh_GAM_VRR_LUT_OFST12_0_new_data_718;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F5Eh
extern volatile uint8_t xdata g_rw_gamvrr0_02CFh_GAM_VRR_LUT_OFST12_0_new_data_719;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F5Fh

extern volatile uint8_t xdata g_rw_gamvrr0_02D0h_GAM_VRR_LUT_OFST12_0_new_data_720;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F60h
extern volatile uint8_t xdata g_rw_gamvrr0_02D1h_GAM_VRR_LUT_OFST12_0_new_data_721;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F61h
extern volatile uint8_t xdata g_rw_gamvrr0_02D2h_GAM_VRR_LUT_OFST12_0_new_data_722;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F62h
extern volatile uint8_t xdata g_rw_gamvrr0_02D3h_GAM_VRR_LUT_OFST12_0_new_data_723;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F63h
extern volatile uint8_t xdata g_rw_gamvrr0_02D4h_GAM_VRR_LUT_OFST12_0_new_data_724;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F64h

extern volatile uint8_t xdata g_rw_gamvrr0_02D5h_GAM_VRR_LUT_OFST12_0_new_data_725;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F65h
extern volatile uint8_t xdata g_rw_gamvrr0_02D6h_GAM_VRR_LUT_OFST12_0_new_data_726;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F66h
extern volatile uint8_t xdata g_rw_gamvrr0_02D7h_GAM_VRR_LUT_OFST12_0_new_data_727;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F67h
extern volatile uint8_t xdata g_rw_gamvrr0_02D8h_GAM_VRR_LUT_OFST12_0_new_data_728;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F68h
extern volatile uint8_t xdata g_rw_gamvrr0_02D9h_GAM_VRR_LUT_OFST12_0_new_data_729;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F69h

extern volatile uint8_t xdata g_rw_gamvrr0_02DAh_GAM_VRR_LUT_OFST12_0_new_data_730;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F6Ah
extern volatile uint8_t xdata g_rw_gamvrr0_02DBh_GAM_VRR_LUT_OFST12_0_new_data_731;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F6Bh
extern volatile uint8_t xdata g_rw_gamvrr0_02DCh_GAM_VRR_LUT_OFST12_0_new_data_732;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F6Ch
extern volatile uint8_t xdata g_rw_gamvrr0_02DDh_GAM_VRR_LUT_OFST12_0_new_data_733;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F6Dh
extern volatile uint8_t xdata g_rw_gamvrr0_02DEh_GAM_VRR_LUT_OFST12_0_new_data_734;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F6Eh

extern volatile uint8_t xdata g_rw_gamvrr0_02DFh_GAM_VRR_LUT_OFST12_0_new_data_735;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F6Fh
extern volatile uint8_t xdata g_rw_gamvrr0_02E0h_GAM_VRR_LUT_OFST12_0_new_data_736;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F70h
extern volatile uint8_t xdata g_rw_gamvrr0_02E1h_GAM_VRR_LUT_OFST12_0_new_data_737;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F71h
extern volatile uint8_t xdata g_rw_gamvrr0_02E2h_GAM_VRR_LUT_OFST12_0_new_data_738;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F72h
extern volatile uint8_t xdata g_rw_gamvrr0_02E3h_GAM_VRR_LUT_OFST12_0_new_data_739;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F73h

extern volatile uint8_t xdata g_rw_gamvrr0_02E4h_GAM_VRR_LUT_OFST12_0_new_data_740;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F74h
extern volatile uint8_t xdata g_rw_gamvrr0_02E5h_GAM_VRR_LUT_OFST12_0_new_data_741;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F75h
extern volatile uint8_t xdata g_rw_gamvrr0_02E6h_GAM_VRR_LUT_OFST12_0_new_data_742;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F76h
extern volatile uint8_t xdata g_rw_gamvrr0_02E7h_GAM_VRR_LUT_OFST12_0_new_data_743;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F77h
extern volatile uint8_t xdata g_rw_gamvrr0_02E8h_GAM_VRR_LUT_OFST12_0_new_data_744;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F78h

extern volatile uint8_t xdata g_rw_gamvrr0_02E9h_GAM_VRR_LUT_OFST12_0_new_data_745;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F79h
extern volatile uint8_t xdata g_rw_gamvrr0_02EAh_GAM_VRR_LUT_OFST12_0_new_data_746;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F7Ah
extern volatile uint8_t xdata g_rw_gamvrr0_02EBh_GAM_VRR_LUT_OFST12_0_new_data_747;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F7Bh
extern volatile uint8_t xdata g_rw_gamvrr0_02ECh_GAM_VRR_LUT_OFST12_0_new_data_748;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F7Ch
extern volatile uint8_t xdata g_rw_gamvrr0_02EDh_GAM_VRR_LUT_OFST12_0_new_data_749;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F7Dh

extern volatile uint8_t xdata g_rw_gamvrr0_02EEh_GAM_VRR_LUT_OFST12_0_new_data_750;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F7Eh
extern volatile uint8_t xdata g_rw_gamvrr0_02EFh_GAM_VRR_LUT_OFST12_0_new_data_751;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F7Fh
extern volatile uint8_t xdata g_rw_gamvrr0_02F0h_GAM_VRR_LUT_OFST12_0_new_data_752;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F80h
extern volatile uint8_t xdata g_rw_gamvrr0_02F1h_GAM_VRR_LUT_OFST12_0_new_data_753;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F81h
extern volatile uint8_t xdata g_rw_gamvrr0_02F2h_GAM_VRR_LUT_OFST12_0_new_data_754;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F82h

extern volatile uint8_t xdata g_rw_gamvrr0_02F3h_GAM_VRR_LUT_OFST12_0_new_data_755;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F83h
extern volatile uint8_t xdata g_rw_gamvrr0_02F4h_GAM_VRR_LUT_OFST12_0_new_data_756;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F84h
extern volatile uint8_t xdata g_rw_gamvrr0_02F5h_GAM_VRR_LUT_OFST12_0_new_data_757;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F85h
extern volatile uint8_t xdata g_rw_gamvrr0_02F6h_GAM_VRR_LUT_OFST12_0_new_data_758;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F86h
extern volatile uint8_t xdata g_rw_gamvrr0_02F7h_GAM_VRR_LUT_OFST12_0_new_data_759;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F87h

extern volatile uint8_t xdata g_rw_gamvrr0_02F8h_GAM_VRR_LUT_OFST12_0_new_data_760;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F88h
extern volatile uint8_t xdata g_rw_gamvrr0_02F9h_GAM_VRR_LUT_OFST12_0_new_data_761;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F89h
extern volatile uint8_t xdata g_rw_gamvrr0_02FAh_GAM_VRR_LUT_OFST12_0_new_data_762;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F8Ah
extern volatile uint8_t xdata g_rw_gamvrr0_02FBh_GAM_VRR_LUT_OFST12_0_new_data_763;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F8Bh
extern volatile uint8_t xdata g_rw_gamvrr0_02FCh_GAM_VRR_LUT_OFST12_0_new_data_764;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F8Ch

extern volatile uint8_t xdata g_rw_gamvrr0_02FDh_GAM_VRR_LUT_OFST12_0_new_data_765;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F8Dh
extern volatile uint8_t xdata g_rw_gamvrr0_02FEh_GAM_VRR_LUT_OFST12_0_new_data_766;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F8Eh
extern volatile uint8_t xdata g_rw_gamvrr0_02FFh_GAM_VRR_LUT_OFST12_0_new_data_767;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F8Fh
extern volatile uint8_t xdata g_rw_gamvrr0_0300h_GAM_VRR_LUT_OFST12_0_new_data_768;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7F90h
extern volatile uint8_t xdata g_rw_gamvrr0_0301h_GAM_VRR_LUT_OFST12_0_new_data_769;                      // [msb:lsb] = [7:0], val = 32, 	Absolute Address = 7F91h

extern volatile uint8_t xdata g_rw_gamvrr0_0302h_GAM_VRR_LUT_OFST12_0_new_data_770;                      // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 7F92h
extern volatile uint8_t xdata g_rw_gamvrr0_0303h_GAM_VRR_LUT_OFST12_0_new_data_771;                      // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 7F93h
extern volatile uint8_t xdata g_rw_gamvrr0_0304h_GAM_VRR_LUT_OFST12_0_new_data_772;                      // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 7F94h
extern volatile uint8_t xdata g_rw_gamvrr0_0305h_GAM_VRR_LUT_OFST12_0_new_data_773;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7F95h

#endif