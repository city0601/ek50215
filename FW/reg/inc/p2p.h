#ifndef _P2P_H_
#define _P2P_H_
#include "reg_include.h"

union rw_p2p_0000h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_port                                                              : 5;        // [msb:lsb] = [4:0], val = 1
        unsigned char r_tx_pair                                                              : 1;        // val = 0
        unsigned char r_obit_sel                                                             : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_p2p_0000h xdata g_rw_p2p_0000h;    // Absolute Address = 6000h

union rw_p2p_0001h
{
    unsigned char byte;
    struct
    {
        unsigned char r_lbuf_rd_no                                                           : 5;        // [msb:lsb] = [4:0], val = 8
        unsigned char r_tx_8bit_sel                                                          : 1;        // val = 0
        unsigned char reserved_1                                                             : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_p2p_0001h xdata g_rw_p2p_0001h;    // Absolute Address = 6001h

union rw_p2p_0002h
{
    unsigned char byte;
    struct
    {
        unsigned char r_predmy_num                                                           : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char reserved_2                                                             : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_p2p_0002h xdata g_rw_p2p_0002h;    // Absolute Address = 6002h

union rw_p2p_0004h
{
    unsigned char byte;
    struct
    {
        unsigned char r_vact_num_bits_13_8                                                   : 6;        // [msb:lsb] = [13:8], val = 5
        unsigned char reserved_3                                                             : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_p2p_0004h xdata g_rw_p2p_0004h;    // Absolute Address = 6004h

union rw_p2p_0006h
{
    unsigned char byte;
    struct
    {
        unsigned char r_dly_len                                                              : 4;        // [msb:lsb] = [3:0], val = 2
        unsigned char r_mrst_len                                                             : 3;        // [msb:lsb] = [2:0], val = 4
        unsigned char reserved_4                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_p2p_0006h xdata g_rw_p2p_0006h;    // Absolute Address = 6006h

//------------------------------------------------------------------------------------------------------------------------
union rw_p2p_0009h
{
    unsigned char byte;
    struct
    {
        unsigned char r_lcmd_en                                                              : 1;        // val = 0
        unsigned char r_pol_sel                                                              : 2;        // [msb:lsb] = [1:0], val = 1
        unsigned char reserved_5                                                             : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_p2p_0009h xdata g_rw_p2p_0009h;    // Absolute Address = 6009h

union rw_p2p_000Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gam_en                                                               : 1;        // val = 0
        unsigned char r_gam_type                                                             : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gam_lv1_only                                                         : 1;        // val = 0
        unsigned char r_rstpol_no                                                            : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_p2p_000Eh xdata g_rw_p2p_000Eh;    // Absolute Address = 600Eh

union rw_p2p_0024h
{
    unsigned char byte;
    struct
    {
        unsigned char r_ctrl_xrst_dly                                                        : 7;        // [msb:lsb] = [6:0], val = 0
        unsigned char r_ctrl_xrst_sel                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_p2p_0024h xdata g_rw_p2p_0024h;    // Absolute Address = 6024h

union rw_p2p_0025h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rd_xrst_ltc                                                          : 7;        // [msb:lsb] = [6:0], val = 0
        unsigned char r_rd_xrst_ltc_en                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_p2p_0025h xdata g_rw_p2p_0025h;    // Absolute Address = 6025h

union rw_p2p_0026h
{
    unsigned char byte;
    struct
    {
        unsigned char r_port_0_sel                                                           : 3;        // [msb:lsb] = [2:0], val = 3
        unsigned char r_clk_inv_en                                                           : 1;        // val = 0
        unsigned char r_port_1_sel                                                           : 3;        // [msb:lsb] = [2:0], val = 4
        unsigned char reserved_6                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_p2p_0026h xdata g_rw_p2p_0026h;    // Absolute Address = 6026h

//------------------------------------------------------------------------------------------------------------------------
union rw_p2p_0027h
{
    unsigned char byte;
    struct
    {
        unsigned char r_port_2_sel                                                           : 3;        // [msb:lsb] = [2:0], val = 5
        unsigned char reserved_7                                                             : 1;        // val = 0
        unsigned char r_port_3_sel                                                           : 3;        // [msb:lsb] = [2:0], val = 6
        unsigned char reserved_8                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_p2p_0027h xdata g_rw_p2p_0027h;    // Absolute Address = 6027h

union rw_p2p_0028h
{
    unsigned char byte;
    struct
    {
        unsigned char r_port_4_sel                                                           : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char reserved_9                                                             : 1;        // val = 0
        unsigned char r_port_5_sel                                                           : 3;        // [msb:lsb] = [2:0], val = 1
        unsigned char reserved_10                                                            : 1;        // val = 0
    }bits;
};
extern volatile union rw_p2p_0028h xdata g_rw_p2p_0028h;    // Absolute Address = 6028h

union rw_p2p_0029h
{
    unsigned char byte;
    struct
    {
        unsigned char r_port_6_sel                                                           : 3;        // [msb:lsb] = [2:0], val = 2
        unsigned char reserved_11                                                            : 1;        // val = 0
        unsigned char r_port_7_sel                                                           : 3;        // [msb:lsb] = [2:0], val = 7
        unsigned char reserved_12                                                            : 1;        // val = 0
    }bits;
};
extern volatile union rw_p2p_0029h xdata g_rw_p2p_0029h;    // Absolute Address = 6029h

union rw_p2p_002Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_chpn_p0                                                              : 1;        // val = 0
        unsigned char r_chpn_p1                                                              : 1;        // val = 0
        unsigned char r_chpn_p2                                                              : 1;        // val = 0
        unsigned char r_chpn_p3                                                              : 1;        // val = 0
        unsigned char r_chpn_p4                                                              : 1;        // val = 0
        unsigned char r_chpn_p5                                                              : 1;        // val = 0
        unsigned char r_chpn_p6                                                              : 1;        // val = 0
        unsigned char r_chpn_p7                                                              : 1;        // val = 0
    }bits;
};
extern volatile union rw_p2p_002Ah xdata g_rw_p2p_002Ah;    // Absolute Address = 602Ah

union rw_p2p_002Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_lane_skew_8b                                                         : 1;        // val = 0
        unsigned char reserved_13                                                            : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_lane1_skew                                                           : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_p2p_002Bh xdata g_rw_p2p_002Bh;    // Absolute Address = 602Bh

//------------------------------------------------------------------------------------------------------------------------
union rw_p2p_002Ch
{
    unsigned char byte;
    struct
    {
        unsigned char reserved_14                                                            : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_lane2_skew                                                           : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_p2p_002Ch xdata g_rw_p2p_002Ch;    // Absolute Address = 602Ch

union rw_p2p_002Dh
{
    unsigned char byte;
    struct
    {
        unsigned char reserved_15                                                            : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_lane3_skew                                                           : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_p2p_002Dh xdata g_rw_p2p_002Dh;    // Absolute Address = 602Dh

union rw_p2p_002Eh
{
    unsigned char byte;
    struct
    {
        unsigned char reserved_16                                                            : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_lane4_skew                                                           : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_p2p_002Eh xdata g_rw_p2p_002Eh;    // Absolute Address = 602Eh

union rw_p2p_0031h
{
    unsigned char byte;
    struct
    {
        unsigned char r_dc_test_dt_bits_19_16                                                : 4;        // [msb:lsb] = [19:16], val = 0
        unsigned char r_dc_test                                                              : 1;        // val = 0
        unsigned char r_loopb_en                                                             : 1;        // val = 0
        unsigned char r_vx1_prbs_en                                                          : 1;        // val = 0
        unsigned char r_prbs_dt_rev                                                          : 1;        // val = 0
    }bits;
};
extern volatile union rw_p2p_0031h xdata g_rw_p2p_0031h;    // Absolute Address = 6031h

union rw_p2p_0033h
{
    unsigned char byte;
    struct
    {
        unsigned char r_mil_ck_val                                                           : 7;        // [msb:lsb] = [6:0], val = 0
        unsigned char r_mil_chksum_clr                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_p2p_0033h xdata g_rw_p2p_0033h;    // Absolute Address = 6033h

//------------------------------------------------------------------------------------------------------------------------
union rw_p2p_0035h
{
    unsigned char byte;
    struct
    {
        unsigned char r_mil_chksum_frm                                                       : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_p2p_hdisp_bits_3_0                                                   : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_p2p_0035h xdata g_rw_p2p_0035h;    // Absolute Address = 6035h

union rw_p2p_0036h
{
    unsigned char byte;
    struct
    {
        unsigned char r_p2p_hdisp_bits_10_4                                                  : 7;        // [msb:lsb] = [10:4], val = 0
        unsigned char r_p2p_rd_manu                                                          : 1;        // val = 0
    }bits;
};
extern volatile union rw_p2p_0036h xdata g_rw_p2p_0036h;    // Absolute Address = 6036h


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_p2p_0003h_vact_num;                                                   // [msb:lsb] = [7:0], val = 160, 	Absolute Address = 6003h
extern volatile uint8_t xdata g_rw_p2p_0005h_enddmy_num;                                                 // [msb:lsb] = [7:0], val = 1, 	Absolute Address = 6005h
extern volatile uint8_t xdata g_rw_p2p_0007h_mrst_stdt;                                                  // [msb:lsb] = [7:0], val = 248, 	Absolute Address = 6007h
extern volatile uint8_t xdata g_rw_p2p_0008h_mrst_enddt;                                                 // [msb:lsb] = [7:0], val = 31, 	Absolute Address = 6008h
extern volatile uint8_t xdata g_rw_p2p_000Ah_pol_posdt;                                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 600Ah

extern volatile uint8_t xdata g_rw_p2p_000Bh_pol_negdt;                                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 600Bh
extern volatile uint8_t xdata g_rw_p2p_000Ch_lcmd1;                                                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 600Ch
extern volatile uint8_t xdata g_rw_p2p_000Dh_lcmd2;                                                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 600Dh
extern volatile uint8_t xdata g_rw_p2p_000Fh_key_code;                                                   // [msb:lsb] = [7:0], val = 28, 	Absolute Address = 600Fh
extern volatile uint8_t xdata g_rw_p2p_0010h_key_code;                                                   // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 6010h

extern volatile uint8_t xdata g_rw_p2p_0011h_gam1;                                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6011h
extern volatile uint8_t xdata g_rw_p2p_0012h_gam2;                                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6012h
extern volatile uint8_t xdata g_rw_p2p_0013h_gam3;                                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6013h
extern volatile uint8_t xdata g_rw_p2p_0014h_gam4;                                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6014h
extern volatile uint8_t xdata g_rw_p2p_0015h_gam5;                                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6015h

extern volatile uint8_t xdata g_rw_p2p_0016h_gam6;                                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6016h
extern volatile uint8_t xdata g_rw_p2p_0017h_gam7;                                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6017h
extern volatile uint8_t xdata g_rw_p2p_0018h_gam8;                                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6018h
extern volatile uint8_t xdata g_rw_p2p_0019h_gam9;                                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6019h
extern volatile uint8_t xdata g_rw_p2p_001Ah_gam10;                                                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 601Ah

extern volatile uint8_t xdata g_rw_p2p_001Bh_gam11;                                                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 601Bh
extern volatile uint8_t xdata g_rw_p2p_001Ch_gam12;                                                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 601Ch
extern volatile uint8_t xdata g_rw_p2p_001Dh_gam13;                                                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 601Dh
extern volatile uint8_t xdata g_rw_p2p_001Eh_gam14;                                                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 601Eh
extern volatile uint8_t xdata g_rw_p2p_001Fh_byte15;                                                     // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 601Fh

extern volatile uint8_t xdata g_rw_p2p_0020h_reserve1;                                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6020h
extern volatile uint8_t xdata g_rw_p2p_0021h_reserve2;                                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6021h
extern volatile uint8_t xdata g_rw_p2p_0022h_reserve3;                                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6022h
extern volatile uint8_t xdata g_rw_p2p_0023h_reserve4;                                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6023h
extern volatile uint8_t xdata g_rw_p2p_002Fh_dc_test_dt;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 602Fh

extern volatile uint8_t xdata g_rw_p2p_0030h_dc_test_dt;                                                 // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 6030h
extern volatile uint8_t xdata g_rw_p2p_0032h_prbs_ck_en;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6032h
extern volatile uint8_t xdata g_rw_p2p_0034h_mil_chksum;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6034h

#endif