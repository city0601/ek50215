#ifndef _SPAT_H_
#define _SPAT_H_
#include "reg_include.h"

union rw_spat_0018h
{
    unsigned char byte;
    struct
    {
        unsigned char r_aging_time                                                           : 4;        // [msb:lsb] = [3:0], val = 2
        unsigned char r_aging_ptn                                                            : 3;        // [msb:lsb] = [2:0], val = 6
        unsigned char r_aging_runhold                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_spat_0018h xdata g_rw_spat_0018h;    // Absolute Address = 0818h

union rw_spat_001Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_box_h_bits_11_8                                                      : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_box_w_bits_3_0                                                       : 4;        // [msb:lsb] = [3:0], val = 3
    }bits;
};
extern volatile union rw_spat_001Ah xdata g_rw_spat_001Ah;    // Absolute Address = 081Ah

union rw_spat_001Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_vp2_bits_11_8                                                        : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_vp1_bits_3_0                                                         : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_spat_001Dh xdata g_rw_spat_001Dh;    // Absolute Address = 081Dh

union rw_spat_0020h
{
    unsigned char byte;
    struct
    {
        unsigned char r_hp2_bits_11_8                                                        : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_hp1_bits_3_0                                                         : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_spat_0020h xdata g_rw_spat_0020h;    // Absolute Address = 0820h

union rw_spat_0030h
{
    unsigned char byte;
    struct
    {
        unsigned char r_sec_h_line                                                           : 1;        // val = 0
        unsigned char r_sec_v_line                                                           : 1;        // val = 0
        unsigned char r_flicker_mask                                                         : 1;        // val = 1
        unsigned char r_win_sel                                                              : 1;        // val = 1
        unsigned char r_aging_speed_pat0_sel                                                 : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_aging_speed_pat1_sel                                                 : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_spat_0030h xdata g_rw_spat_0030h;    // Absolute Address = 0830h

//------------------------------------------------------------------------------------------------------------------------
union rw_spat_0031h
{
    unsigned char byte;
    struct
    {
        unsigned char r_aging_speed_pat2_sel                                                 : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_aging_speed_pat3_sel                                                 : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_aging_speed_pat4_sel                                                 : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_aging_speed_pat5_sel                                                 : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_spat_0031h xdata g_rw_spat_0031h;    // Absolute Address = 0831h

union rw_spat_0035h
{
    unsigned char byte;
    struct
    {
        unsigned char r_4pix_flick_en                                                        : 1;        // val = 0
        unsigned char r_fiti_spat_en                                                         : 1;        // val = 0
        unsigned char reserved1                                                              : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_vp1                                                                  : 1;        // val = 0
        unsigned char r_vp2                                                                  : 1;        // val = 0
        unsigned char r_hp1                                                                  : 1;        // val = 0
        unsigned char r_hp2                                                                  : 1;        // val = 0
    }bits;
};
extern volatile union rw_spat_0035h xdata g_rw_spat_0035h;    // Absolute Address = 0835h


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_spat_0000h_aging0;                                                    // [msb:lsb] = [31:24], val = 0, 	Absolute Address = 0800h
extern volatile uint8_t xdata g_rw_spat_0001h_aging0;                                                    // [msb:lsb] = [23:16], val = 0, 	Absolute Address = 0801h
extern volatile uint8_t xdata g_rw_spat_0002h_aging0;                                                    // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 0802h
extern volatile uint8_t xdata g_rw_spat_0003h_aging0;                                                    // [msb:lsb] = [7:0], val = 4, 	Absolute Address = 0803h
extern volatile uint8_t xdata g_rw_spat_0004h_aging1;                                                    // [msb:lsb] = [31:24], val = 0, 	Absolute Address = 0804h

extern volatile uint8_t xdata g_rw_spat_0005h_aging1;                                                    // [msb:lsb] = [23:16], val = 0, 	Absolute Address = 0805h
extern volatile uint8_t xdata g_rw_spat_0006h_aging1;                                                    // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 0806h
extern volatile uint8_t xdata g_rw_spat_0007h_aging1;                                                    // [msb:lsb] = [7:0], val = 2, 	Absolute Address = 0807h
extern volatile uint8_t xdata g_rw_spat_0008h_aging2;                                                    // [msb:lsb] = [31:24], val = 0, 	Absolute Address = 0808h
extern volatile uint8_t xdata g_rw_spat_0009h_aging2;                                                    // [msb:lsb] = [23:16], val = 0, 	Absolute Address = 0809h

extern volatile uint8_t xdata g_rw_spat_000Ah_aging2;                                                    // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 080Ah
extern volatile uint8_t xdata g_rw_spat_000Bh_aging2;                                                    // [msb:lsb] = [7:0], val = 1, 	Absolute Address = 080Bh
extern volatile uint8_t xdata g_rw_spat_000Ch_aging3;                                                    // [msb:lsb] = [31:24], val = 0, 	Absolute Address = 080Ch
extern volatile uint8_t xdata g_rw_spat_000Dh_aging3;                                                    // [msb:lsb] = [23:16], val = 0, 	Absolute Address = 080Dh
extern volatile uint8_t xdata g_rw_spat_000Eh_aging3;                                                    // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 080Eh

extern volatile uint8_t xdata g_rw_spat_000Fh_aging3;                                                    // [msb:lsb] = [7:0], val = 7, 	Absolute Address = 080Fh
extern volatile uint8_t xdata g_rw_spat_0010h_aging4;                                                    // [msb:lsb] = [31:24], val = 0, 	Absolute Address = 0810h
extern volatile uint8_t xdata g_rw_spat_0011h_aging4;                                                    // [msb:lsb] = [23:16], val = 0, 	Absolute Address = 0811h
extern volatile uint8_t xdata g_rw_spat_0012h_aging4;                                                    // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 0812h
extern volatile uint8_t xdata g_rw_spat_0013h_aging4;                                                    // [msb:lsb] = [7:0], val = 7, 	Absolute Address = 0813h

extern volatile uint8_t xdata g_rw_spat_0014h_aging5;                                                    // [msb:lsb] = [31:24], val = 0, 	Absolute Address = 0814h
extern volatile uint8_t xdata g_rw_spat_0015h_aging5;                                                    // [msb:lsb] = [23:16], val = 0, 	Absolute Address = 0815h
extern volatile uint8_t xdata g_rw_spat_0016h_aging5;                                                    // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 0816h
extern volatile uint8_t xdata g_rw_spat_0017h_aging5;                                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 0817h
extern volatile uint8_t xdata g_rw_spat_0019h_box_w;                                                     // [msb:lsb] = [11:4], val = 1, 	Absolute Address = 0819h

extern volatile uint8_t xdata g_rw_spat_001Bh_box_h;                                                     // [msb:lsb] = [7:0], val = 216, 	Absolute Address = 081Bh
extern volatile uint8_t xdata g_rw_spat_001Ch_vp1;                                                       // [msb:lsb] = [11:4], val = 1, 	Absolute Address = 081Ch
extern volatile uint8_t xdata g_rw_spat_001Eh_vp2;                                                       // [msb:lsb] = [7:0], val = 208, 	Absolute Address = 081Eh
extern volatile uint8_t xdata g_rw_spat_001Fh_hp1;                                                       // [msb:lsb] = [11:4], val = 2, 	Absolute Address = 081Fh
extern volatile uint8_t xdata g_rw_spat_0021h_hp2;                                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 0821h

extern volatile uint8_t xdata g_rw_spat_0022h_flicker_gray1;                                             // [msb:lsb] = [7:0], val = 127, 	Absolute Address = 0822h
extern volatile uint8_t xdata g_rw_spat_0023h_aging_w;                                                   // [msb:lsb] = [7:0], val = 85, 	Absolute Address = 0823h
extern volatile uint8_t xdata g_rw_spat_0024h_flicker_gray2;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 0824h
extern volatile uint8_t xdata g_rw_spat_0025h_flicker_r;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 0825h
extern volatile uint8_t xdata g_rw_spat_0026h_flicker_g;                                                 // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 0826h

extern volatile uint8_t xdata g_rw_spat_0027h_flicker_b;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 0827h
extern volatile uint8_t xdata g_rw_spat_0028h_flicker_pat_num;                                           // [msb:lsb] = [7:0], val = 18, 	Absolute Address = 0828h
extern volatile uint8_t xdata g_rw_spat_0029h_flicker_gray1_for_pin;                                     // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 0829h
extern volatile uint8_t xdata g_rw_spat_002Ah_flicker_gray2_for_pin;                                     // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 082Ah
extern volatile uint8_t xdata g_rw_spat_002Bh_fcnt_0_1sec;                                               // [msb:lsb] = [7:0], val = 60, 	Absolute Address = 082Bh

extern volatile uint8_t xdata g_rw_spat_002Ch_fcnt_1_1sec;                                               // [msb:lsb] = [7:0], val = 60, 	Absolute Address = 082Ch
extern volatile uint8_t xdata g_rw_spat_002Dh_fcnt_2_1sec;                                               // [msb:lsb] = [7:0], val = 60, 	Absolute Address = 082Dh
extern volatile uint8_t xdata g_rw_spat_002Eh_fcnt_3_1sec;                                               // [msb:lsb] = [7:0], val = 60, 	Absolute Address = 082Eh
extern volatile uint8_t xdata g_rw_spat_002Fh_fcnt_sub_1sec;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 082Fh
extern volatile uint8_t xdata g_rw_spat_0032h_flicker_loc_r;                                             // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 0832h

extern volatile uint8_t xdata g_rw_spat_0033h_flicker_loc_g;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 0833h
extern volatile uint8_t xdata g_rw_spat_0034h_flicker_loc_b;                                             // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 0834h

#endif