#ifndef _GAMVRR2_H_
#define _GAMVRR2_H_
#include "reg_include.h"


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_gamvrr2_0000h_GAM_VRR_LUT_OFST12_2_new_data_0;                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8400h
extern volatile uint8_t xdata g_rw_gamvrr2_0001h_GAM_VRR_LUT_OFST12_2_new_data_1;                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8401h
extern volatile uint8_t xdata g_rw_gamvrr2_0002h_GAM_VRR_LUT_OFST12_2_new_data_2;                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8402h
extern volatile uint8_t xdata g_rw_gamvrr2_0003h_GAM_VRR_LUT_OFST12_2_new_data_3;                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8403h
extern volatile uint8_t xdata g_rw_gamvrr2_0004h_GAM_VRR_LUT_OFST12_2_new_data_4;                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8404h

extern volatile uint8_t xdata g_rw_gamvrr2_0005h_GAM_VRR_LUT_OFST12_2_new_data_5;                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8405h
extern volatile uint8_t xdata g_rw_gamvrr2_0006h_GAM_VRR_LUT_OFST12_2_new_data_6;                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8406h
extern volatile uint8_t xdata g_rw_gamvrr2_0007h_GAM_VRR_LUT_OFST12_2_new_data_7;                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8407h
extern volatile uint8_t xdata g_rw_gamvrr2_0008h_GAM_VRR_LUT_OFST12_2_new_data_8;                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8408h
extern volatile uint8_t xdata g_rw_gamvrr2_0009h_GAM_VRR_LUT_OFST12_2_new_data_9;                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8409h

extern volatile uint8_t xdata g_rw_gamvrr2_000Ah_GAM_VRR_LUT_OFST12_2_new_data_10;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 840Ah
extern volatile uint8_t xdata g_rw_gamvrr2_000Bh_GAM_VRR_LUT_OFST12_2_new_data_11;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 840Bh
extern volatile uint8_t xdata g_rw_gamvrr2_000Ch_GAM_VRR_LUT_OFST12_2_new_data_12;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 840Ch
extern volatile uint8_t xdata g_rw_gamvrr2_000Dh_GAM_VRR_LUT_OFST12_2_new_data_13;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 840Dh
extern volatile uint8_t xdata g_rw_gamvrr2_000Eh_GAM_VRR_LUT_OFST12_2_new_data_14;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 840Eh

extern volatile uint8_t xdata g_rw_gamvrr2_000Fh_GAM_VRR_LUT_OFST12_2_new_data_15;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 840Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0010h_GAM_VRR_LUT_OFST12_2_new_data_16;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8410h
extern volatile uint8_t xdata g_rw_gamvrr2_0011h_GAM_VRR_LUT_OFST12_2_new_data_17;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8411h
extern volatile uint8_t xdata g_rw_gamvrr2_0012h_GAM_VRR_LUT_OFST12_2_new_data_18;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8412h
extern volatile uint8_t xdata g_rw_gamvrr2_0013h_GAM_VRR_LUT_OFST12_2_new_data_19;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8413h

extern volatile uint8_t xdata g_rw_gamvrr2_0014h_GAM_VRR_LUT_OFST12_2_new_data_20;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8414h
extern volatile uint8_t xdata g_rw_gamvrr2_0015h_GAM_VRR_LUT_OFST12_2_new_data_21;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8415h
extern volatile uint8_t xdata g_rw_gamvrr2_0016h_GAM_VRR_LUT_OFST12_2_new_data_22;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8416h
extern volatile uint8_t xdata g_rw_gamvrr2_0017h_GAM_VRR_LUT_OFST12_2_new_data_23;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8417h
extern volatile uint8_t xdata g_rw_gamvrr2_0018h_GAM_VRR_LUT_OFST12_2_new_data_24;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8418h

extern volatile uint8_t xdata g_rw_gamvrr2_0019h_GAM_VRR_LUT_OFST12_2_new_data_25;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8419h
extern volatile uint8_t xdata g_rw_gamvrr2_001Ah_GAM_VRR_LUT_OFST12_2_new_data_26;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 841Ah
extern volatile uint8_t xdata g_rw_gamvrr2_001Bh_GAM_VRR_LUT_OFST12_2_new_data_27;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 841Bh
extern volatile uint8_t xdata g_rw_gamvrr2_001Ch_GAM_VRR_LUT_OFST12_2_new_data_28;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 841Ch
extern volatile uint8_t xdata g_rw_gamvrr2_001Dh_GAM_VRR_LUT_OFST12_2_new_data_29;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 841Dh

extern volatile uint8_t xdata g_rw_gamvrr2_001Eh_GAM_VRR_LUT_OFST12_2_new_data_30;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 841Eh
extern volatile uint8_t xdata g_rw_gamvrr2_001Fh_GAM_VRR_LUT_OFST12_2_new_data_31;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 841Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0020h_GAM_VRR_LUT_OFST12_2_new_data_32;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8420h
extern volatile uint8_t xdata g_rw_gamvrr2_0021h_GAM_VRR_LUT_OFST12_2_new_data_33;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8421h
extern volatile uint8_t xdata g_rw_gamvrr2_0022h_GAM_VRR_LUT_OFST12_2_new_data_34;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8422h

extern volatile uint8_t xdata g_rw_gamvrr2_0023h_GAM_VRR_LUT_OFST12_2_new_data_35;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8423h
extern volatile uint8_t xdata g_rw_gamvrr2_0024h_GAM_VRR_LUT_OFST12_2_new_data_36;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8424h
extern volatile uint8_t xdata g_rw_gamvrr2_0025h_GAM_VRR_LUT_OFST12_2_new_data_37;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8425h
extern volatile uint8_t xdata g_rw_gamvrr2_0026h_GAM_VRR_LUT_OFST12_2_new_data_38;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8426h
extern volatile uint8_t xdata g_rw_gamvrr2_0027h_GAM_VRR_LUT_OFST12_2_new_data_39;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8427h

extern volatile uint8_t xdata g_rw_gamvrr2_0028h_GAM_VRR_LUT_OFST12_2_new_data_40;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8428h
extern volatile uint8_t xdata g_rw_gamvrr2_0029h_GAM_VRR_LUT_OFST12_2_new_data_41;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8429h
extern volatile uint8_t xdata g_rw_gamvrr2_002Ah_GAM_VRR_LUT_OFST12_2_new_data_42;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 842Ah
extern volatile uint8_t xdata g_rw_gamvrr2_002Bh_GAM_VRR_LUT_OFST12_2_new_data_43;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 842Bh
extern volatile uint8_t xdata g_rw_gamvrr2_002Ch_GAM_VRR_LUT_OFST12_2_new_data_44;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 842Ch

extern volatile uint8_t xdata g_rw_gamvrr2_002Dh_GAM_VRR_LUT_OFST12_2_new_data_45;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 842Dh
extern volatile uint8_t xdata g_rw_gamvrr2_002Eh_GAM_VRR_LUT_OFST12_2_new_data_46;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 842Eh
extern volatile uint8_t xdata g_rw_gamvrr2_002Fh_GAM_VRR_LUT_OFST12_2_new_data_47;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 842Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0030h_GAM_VRR_LUT_OFST12_2_new_data_48;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8430h
extern volatile uint8_t xdata g_rw_gamvrr2_0031h_GAM_VRR_LUT_OFST12_2_new_data_49;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8431h

extern volatile uint8_t xdata g_rw_gamvrr2_0032h_GAM_VRR_LUT_OFST12_2_new_data_50;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8432h
extern volatile uint8_t xdata g_rw_gamvrr2_0033h_GAM_VRR_LUT_OFST12_2_new_data_51;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8433h
extern volatile uint8_t xdata g_rw_gamvrr2_0034h_GAM_VRR_LUT_OFST12_2_new_data_52;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8434h
extern volatile uint8_t xdata g_rw_gamvrr2_0035h_GAM_VRR_LUT_OFST12_2_new_data_53;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8435h
extern volatile uint8_t xdata g_rw_gamvrr2_0036h_GAM_VRR_LUT_OFST12_2_new_data_54;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8436h

extern volatile uint8_t xdata g_rw_gamvrr2_0037h_GAM_VRR_LUT_OFST12_2_new_data_55;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8437h
extern volatile uint8_t xdata g_rw_gamvrr2_0038h_GAM_VRR_LUT_OFST12_2_new_data_56;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8438h
extern volatile uint8_t xdata g_rw_gamvrr2_0039h_GAM_VRR_LUT_OFST12_2_new_data_57;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8439h
extern volatile uint8_t xdata g_rw_gamvrr2_003Ah_GAM_VRR_LUT_OFST12_2_new_data_58;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 843Ah
extern volatile uint8_t xdata g_rw_gamvrr2_003Bh_GAM_VRR_LUT_OFST12_2_new_data_59;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 843Bh

extern volatile uint8_t xdata g_rw_gamvrr2_003Ch_GAM_VRR_LUT_OFST12_2_new_data_60;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 843Ch
extern volatile uint8_t xdata g_rw_gamvrr2_003Dh_GAM_VRR_LUT_OFST12_2_new_data_61;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 843Dh
extern volatile uint8_t xdata g_rw_gamvrr2_003Eh_GAM_VRR_LUT_OFST12_2_new_data_62;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 843Eh
extern volatile uint8_t xdata g_rw_gamvrr2_003Fh_GAM_VRR_LUT_OFST12_2_new_data_63;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 843Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0040h_GAM_VRR_LUT_OFST12_2_new_data_64;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8440h

extern volatile uint8_t xdata g_rw_gamvrr2_0041h_GAM_VRR_LUT_OFST12_2_new_data_65;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8441h
extern volatile uint8_t xdata g_rw_gamvrr2_0042h_GAM_VRR_LUT_OFST12_2_new_data_66;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8442h
extern volatile uint8_t xdata g_rw_gamvrr2_0043h_GAM_VRR_LUT_OFST12_2_new_data_67;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8443h
extern volatile uint8_t xdata g_rw_gamvrr2_0044h_GAM_VRR_LUT_OFST12_2_new_data_68;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8444h
extern volatile uint8_t xdata g_rw_gamvrr2_0045h_GAM_VRR_LUT_OFST12_2_new_data_69;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8445h

extern volatile uint8_t xdata g_rw_gamvrr2_0046h_GAM_VRR_LUT_OFST12_2_new_data_70;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8446h
extern volatile uint8_t xdata g_rw_gamvrr2_0047h_GAM_VRR_LUT_OFST12_2_new_data_71;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8447h
extern volatile uint8_t xdata g_rw_gamvrr2_0048h_GAM_VRR_LUT_OFST12_2_new_data_72;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8448h
extern volatile uint8_t xdata g_rw_gamvrr2_0049h_GAM_VRR_LUT_OFST12_2_new_data_73;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8449h
extern volatile uint8_t xdata g_rw_gamvrr2_004Ah_GAM_VRR_LUT_OFST12_2_new_data_74;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 844Ah

extern volatile uint8_t xdata g_rw_gamvrr2_004Bh_GAM_VRR_LUT_OFST12_2_new_data_75;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 844Bh
extern volatile uint8_t xdata g_rw_gamvrr2_004Ch_GAM_VRR_LUT_OFST12_2_new_data_76;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 844Ch
extern volatile uint8_t xdata g_rw_gamvrr2_004Dh_GAM_VRR_LUT_OFST12_2_new_data_77;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 844Dh
extern volatile uint8_t xdata g_rw_gamvrr2_004Eh_GAM_VRR_LUT_OFST12_2_new_data_78;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 844Eh
extern volatile uint8_t xdata g_rw_gamvrr2_004Fh_GAM_VRR_LUT_OFST12_2_new_data_79;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 844Fh

extern volatile uint8_t xdata g_rw_gamvrr2_0050h_GAM_VRR_LUT_OFST12_2_new_data_80;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8450h
extern volatile uint8_t xdata g_rw_gamvrr2_0051h_GAM_VRR_LUT_OFST12_2_new_data_81;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8451h
extern volatile uint8_t xdata g_rw_gamvrr2_0052h_GAM_VRR_LUT_OFST12_2_new_data_82;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8452h
extern volatile uint8_t xdata g_rw_gamvrr2_0053h_GAM_VRR_LUT_OFST12_2_new_data_83;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8453h
extern volatile uint8_t xdata g_rw_gamvrr2_0054h_GAM_VRR_LUT_OFST12_2_new_data_84;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8454h

extern volatile uint8_t xdata g_rw_gamvrr2_0055h_GAM_VRR_LUT_OFST12_2_new_data_85;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8455h
extern volatile uint8_t xdata g_rw_gamvrr2_0056h_GAM_VRR_LUT_OFST12_2_new_data_86;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8456h
extern volatile uint8_t xdata g_rw_gamvrr2_0057h_GAM_VRR_LUT_OFST12_2_new_data_87;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8457h
extern volatile uint8_t xdata g_rw_gamvrr2_0058h_GAM_VRR_LUT_OFST12_2_new_data_88;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8458h
extern volatile uint8_t xdata g_rw_gamvrr2_0059h_GAM_VRR_LUT_OFST12_2_new_data_89;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8459h

extern volatile uint8_t xdata g_rw_gamvrr2_005Ah_GAM_VRR_LUT_OFST12_2_new_data_90;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 845Ah
extern volatile uint8_t xdata g_rw_gamvrr2_005Bh_GAM_VRR_LUT_OFST12_2_new_data_91;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 845Bh
extern volatile uint8_t xdata g_rw_gamvrr2_005Ch_GAM_VRR_LUT_OFST12_2_new_data_92;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 845Ch
extern volatile uint8_t xdata g_rw_gamvrr2_005Dh_GAM_VRR_LUT_OFST12_2_new_data_93;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 845Dh
extern volatile uint8_t xdata g_rw_gamvrr2_005Eh_GAM_VRR_LUT_OFST12_2_new_data_94;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 845Eh

extern volatile uint8_t xdata g_rw_gamvrr2_005Fh_GAM_VRR_LUT_OFST12_2_new_data_95;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 845Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0060h_GAM_VRR_LUT_OFST12_2_new_data_96;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8460h
extern volatile uint8_t xdata g_rw_gamvrr2_0061h_GAM_VRR_LUT_OFST12_2_new_data_97;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8461h
extern volatile uint8_t xdata g_rw_gamvrr2_0062h_GAM_VRR_LUT_OFST12_2_new_data_98;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8462h
extern volatile uint8_t xdata g_rw_gamvrr2_0063h_GAM_VRR_LUT_OFST12_2_new_data_99;                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8463h

extern volatile uint8_t xdata g_rw_gamvrr2_0064h_GAM_VRR_LUT_OFST12_2_new_data_100;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8464h
extern volatile uint8_t xdata g_rw_gamvrr2_0065h_GAM_VRR_LUT_OFST12_2_new_data_101;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8465h
extern volatile uint8_t xdata g_rw_gamvrr2_0066h_GAM_VRR_LUT_OFST12_2_new_data_102;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8466h
extern volatile uint8_t xdata g_rw_gamvrr2_0067h_GAM_VRR_LUT_OFST12_2_new_data_103;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8467h
extern volatile uint8_t xdata g_rw_gamvrr2_0068h_GAM_VRR_LUT_OFST12_2_new_data_104;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8468h

extern volatile uint8_t xdata g_rw_gamvrr2_0069h_GAM_VRR_LUT_OFST12_2_new_data_105;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8469h
extern volatile uint8_t xdata g_rw_gamvrr2_006Ah_GAM_VRR_LUT_OFST12_2_new_data_106;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 846Ah
extern volatile uint8_t xdata g_rw_gamvrr2_006Bh_GAM_VRR_LUT_OFST12_2_new_data_107;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 846Bh
extern volatile uint8_t xdata g_rw_gamvrr2_006Ch_GAM_VRR_LUT_OFST12_2_new_data_108;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 846Ch
extern volatile uint8_t xdata g_rw_gamvrr2_006Dh_GAM_VRR_LUT_OFST12_2_new_data_109;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 846Dh

extern volatile uint8_t xdata g_rw_gamvrr2_006Eh_GAM_VRR_LUT_OFST12_2_new_data_110;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 846Eh
extern volatile uint8_t xdata g_rw_gamvrr2_006Fh_GAM_VRR_LUT_OFST12_2_new_data_111;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 846Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0070h_GAM_VRR_LUT_OFST12_2_new_data_112;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8470h
extern volatile uint8_t xdata g_rw_gamvrr2_0071h_GAM_VRR_LUT_OFST12_2_new_data_113;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8471h
extern volatile uint8_t xdata g_rw_gamvrr2_0072h_GAM_VRR_LUT_OFST12_2_new_data_114;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8472h

extern volatile uint8_t xdata g_rw_gamvrr2_0073h_GAM_VRR_LUT_OFST12_2_new_data_115;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8473h
extern volatile uint8_t xdata g_rw_gamvrr2_0074h_GAM_VRR_LUT_OFST12_2_new_data_116;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8474h
extern volatile uint8_t xdata g_rw_gamvrr2_0075h_GAM_VRR_LUT_OFST12_2_new_data_117;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8475h
extern volatile uint8_t xdata g_rw_gamvrr2_0076h_GAM_VRR_LUT_OFST12_2_new_data_118;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8476h
extern volatile uint8_t xdata g_rw_gamvrr2_0077h_GAM_VRR_LUT_OFST12_2_new_data_119;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8477h

extern volatile uint8_t xdata g_rw_gamvrr2_0078h_GAM_VRR_LUT_OFST12_2_new_data_120;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8478h
extern volatile uint8_t xdata g_rw_gamvrr2_0079h_GAM_VRR_LUT_OFST12_2_new_data_121;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8479h
extern volatile uint8_t xdata g_rw_gamvrr2_007Ah_GAM_VRR_LUT_OFST12_2_new_data_122;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 847Ah
extern volatile uint8_t xdata g_rw_gamvrr2_007Bh_GAM_VRR_LUT_OFST12_2_new_data_123;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 847Bh
extern volatile uint8_t xdata g_rw_gamvrr2_007Ch_GAM_VRR_LUT_OFST12_2_new_data_124;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 847Ch

extern volatile uint8_t xdata g_rw_gamvrr2_007Dh_GAM_VRR_LUT_OFST12_2_new_data_125;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 847Dh
extern volatile uint8_t xdata g_rw_gamvrr2_007Eh_GAM_VRR_LUT_OFST12_2_new_data_126;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 847Eh
extern volatile uint8_t xdata g_rw_gamvrr2_007Fh_GAM_VRR_LUT_OFST12_2_new_data_127;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 847Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0080h_GAM_VRR_LUT_OFST12_2_new_data_128;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8480h
extern volatile uint8_t xdata g_rw_gamvrr2_0081h_GAM_VRR_LUT_OFST12_2_new_data_129;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8481h

extern volatile uint8_t xdata g_rw_gamvrr2_0082h_GAM_VRR_LUT_OFST12_2_new_data_130;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8482h
extern volatile uint8_t xdata g_rw_gamvrr2_0083h_GAM_VRR_LUT_OFST12_2_new_data_131;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8483h
extern volatile uint8_t xdata g_rw_gamvrr2_0084h_GAM_VRR_LUT_OFST12_2_new_data_132;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8484h
extern volatile uint8_t xdata g_rw_gamvrr2_0085h_GAM_VRR_LUT_OFST12_2_new_data_133;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8485h
extern volatile uint8_t xdata g_rw_gamvrr2_0086h_GAM_VRR_LUT_OFST12_2_new_data_134;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8486h

extern volatile uint8_t xdata g_rw_gamvrr2_0087h_GAM_VRR_LUT_OFST12_2_new_data_135;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8487h
extern volatile uint8_t xdata g_rw_gamvrr2_0088h_GAM_VRR_LUT_OFST12_2_new_data_136;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8488h
extern volatile uint8_t xdata g_rw_gamvrr2_0089h_GAM_VRR_LUT_OFST12_2_new_data_137;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8489h
extern volatile uint8_t xdata g_rw_gamvrr2_008Ah_GAM_VRR_LUT_OFST12_2_new_data_138;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 848Ah
extern volatile uint8_t xdata g_rw_gamvrr2_008Bh_GAM_VRR_LUT_OFST12_2_new_data_139;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 848Bh

extern volatile uint8_t xdata g_rw_gamvrr2_008Ch_GAM_VRR_LUT_OFST12_2_new_data_140;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 848Ch
extern volatile uint8_t xdata g_rw_gamvrr2_008Dh_GAM_VRR_LUT_OFST12_2_new_data_141;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 848Dh
extern volatile uint8_t xdata g_rw_gamvrr2_008Eh_GAM_VRR_LUT_OFST12_2_new_data_142;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 848Eh
extern volatile uint8_t xdata g_rw_gamvrr2_008Fh_GAM_VRR_LUT_OFST12_2_new_data_143;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 848Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0090h_GAM_VRR_LUT_OFST12_2_new_data_144;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8490h

extern volatile uint8_t xdata g_rw_gamvrr2_0091h_GAM_VRR_LUT_OFST12_2_new_data_145;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8491h
extern volatile uint8_t xdata g_rw_gamvrr2_0092h_GAM_VRR_LUT_OFST12_2_new_data_146;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8492h
extern volatile uint8_t xdata g_rw_gamvrr2_0093h_GAM_VRR_LUT_OFST12_2_new_data_147;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8493h
extern volatile uint8_t xdata g_rw_gamvrr2_0094h_GAM_VRR_LUT_OFST12_2_new_data_148;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8494h
extern volatile uint8_t xdata g_rw_gamvrr2_0095h_GAM_VRR_LUT_OFST12_2_new_data_149;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8495h

extern volatile uint8_t xdata g_rw_gamvrr2_0096h_GAM_VRR_LUT_OFST12_2_new_data_150;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8496h
extern volatile uint8_t xdata g_rw_gamvrr2_0097h_GAM_VRR_LUT_OFST12_2_new_data_151;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8497h
extern volatile uint8_t xdata g_rw_gamvrr2_0098h_GAM_VRR_LUT_OFST12_2_new_data_152;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8498h
extern volatile uint8_t xdata g_rw_gamvrr2_0099h_GAM_VRR_LUT_OFST12_2_new_data_153;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8499h
extern volatile uint8_t xdata g_rw_gamvrr2_009Ah_GAM_VRR_LUT_OFST12_2_new_data_154;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 849Ah

extern volatile uint8_t xdata g_rw_gamvrr2_009Bh_GAM_VRR_LUT_OFST12_2_new_data_155;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 849Bh
extern volatile uint8_t xdata g_rw_gamvrr2_009Ch_GAM_VRR_LUT_OFST12_2_new_data_156;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 849Ch
extern volatile uint8_t xdata g_rw_gamvrr2_009Dh_GAM_VRR_LUT_OFST12_2_new_data_157;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 849Dh
extern volatile uint8_t xdata g_rw_gamvrr2_009Eh_GAM_VRR_LUT_OFST12_2_new_data_158;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 849Eh
extern volatile uint8_t xdata g_rw_gamvrr2_009Fh_GAM_VRR_LUT_OFST12_2_new_data_159;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 849Fh

extern volatile uint8_t xdata g_rw_gamvrr2_00A0h_GAM_VRR_LUT_OFST12_2_new_data_160;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84A0h
extern volatile uint8_t xdata g_rw_gamvrr2_00A1h_GAM_VRR_LUT_OFST12_2_new_data_161;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84A1h
extern volatile uint8_t xdata g_rw_gamvrr2_00A2h_GAM_VRR_LUT_OFST12_2_new_data_162;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84A2h
extern volatile uint8_t xdata g_rw_gamvrr2_00A3h_GAM_VRR_LUT_OFST12_2_new_data_163;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84A3h
extern volatile uint8_t xdata g_rw_gamvrr2_00A4h_GAM_VRR_LUT_OFST12_2_new_data_164;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84A4h

extern volatile uint8_t xdata g_rw_gamvrr2_00A5h_GAM_VRR_LUT_OFST12_2_new_data_165;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84A5h
extern volatile uint8_t xdata g_rw_gamvrr2_00A6h_GAM_VRR_LUT_OFST12_2_new_data_166;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84A6h
extern volatile uint8_t xdata g_rw_gamvrr2_00A7h_GAM_VRR_LUT_OFST12_2_new_data_167;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84A7h
extern volatile uint8_t xdata g_rw_gamvrr2_00A8h_GAM_VRR_LUT_OFST12_2_new_data_168;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84A8h
extern volatile uint8_t xdata g_rw_gamvrr2_00A9h_GAM_VRR_LUT_OFST12_2_new_data_169;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84A9h

extern volatile uint8_t xdata g_rw_gamvrr2_00AAh_GAM_VRR_LUT_OFST12_2_new_data_170;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84AAh
extern volatile uint8_t xdata g_rw_gamvrr2_00ABh_GAM_VRR_LUT_OFST12_2_new_data_171;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84ABh
extern volatile uint8_t xdata g_rw_gamvrr2_00ACh_GAM_VRR_LUT_OFST12_2_new_data_172;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84ACh
extern volatile uint8_t xdata g_rw_gamvrr2_00ADh_GAM_VRR_LUT_OFST12_2_new_data_173;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84ADh
extern volatile uint8_t xdata g_rw_gamvrr2_00AEh_GAM_VRR_LUT_OFST12_2_new_data_174;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84AEh

extern volatile uint8_t xdata g_rw_gamvrr2_00AFh_GAM_VRR_LUT_OFST12_2_new_data_175;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84AFh
extern volatile uint8_t xdata g_rw_gamvrr2_00B0h_GAM_VRR_LUT_OFST12_2_new_data_176;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84B0h
extern volatile uint8_t xdata g_rw_gamvrr2_00B1h_GAM_VRR_LUT_OFST12_2_new_data_177;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84B1h
extern volatile uint8_t xdata g_rw_gamvrr2_00B2h_GAM_VRR_LUT_OFST12_2_new_data_178;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84B2h
extern volatile uint8_t xdata g_rw_gamvrr2_00B3h_GAM_VRR_LUT_OFST12_2_new_data_179;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84B3h

extern volatile uint8_t xdata g_rw_gamvrr2_00B4h_GAM_VRR_LUT_OFST12_2_new_data_180;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84B4h
extern volatile uint8_t xdata g_rw_gamvrr2_00B5h_GAM_VRR_LUT_OFST12_2_new_data_181;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84B5h
extern volatile uint8_t xdata g_rw_gamvrr2_00B6h_GAM_VRR_LUT_OFST12_2_new_data_182;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84B6h
extern volatile uint8_t xdata g_rw_gamvrr2_00B7h_GAM_VRR_LUT_OFST12_2_new_data_183;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84B7h
extern volatile uint8_t xdata g_rw_gamvrr2_00B8h_GAM_VRR_LUT_OFST12_2_new_data_184;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84B8h

extern volatile uint8_t xdata g_rw_gamvrr2_00B9h_GAM_VRR_LUT_OFST12_2_new_data_185;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84B9h
extern volatile uint8_t xdata g_rw_gamvrr2_00BAh_GAM_VRR_LUT_OFST12_2_new_data_186;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84BAh
extern volatile uint8_t xdata g_rw_gamvrr2_00BBh_GAM_VRR_LUT_OFST12_2_new_data_187;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84BBh
extern volatile uint8_t xdata g_rw_gamvrr2_00BCh_GAM_VRR_LUT_OFST12_2_new_data_188;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84BCh
extern volatile uint8_t xdata g_rw_gamvrr2_00BDh_GAM_VRR_LUT_OFST12_2_new_data_189;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84BDh

extern volatile uint8_t xdata g_rw_gamvrr2_00BEh_GAM_VRR_LUT_OFST12_2_new_data_190;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84BEh
extern volatile uint8_t xdata g_rw_gamvrr2_00BFh_GAM_VRR_LUT_OFST12_2_new_data_191;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84BFh
extern volatile uint8_t xdata g_rw_gamvrr2_00C0h_GAM_VRR_LUT_OFST12_2_new_data_192;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84C0h
extern volatile uint8_t xdata g_rw_gamvrr2_00C1h_GAM_VRR_LUT_OFST12_2_new_data_193;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84C1h
extern volatile uint8_t xdata g_rw_gamvrr2_00C2h_GAM_VRR_LUT_OFST12_2_new_data_194;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84C2h

extern volatile uint8_t xdata g_rw_gamvrr2_00C3h_GAM_VRR_LUT_OFST12_2_new_data_195;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84C3h
extern volatile uint8_t xdata g_rw_gamvrr2_00C4h_GAM_VRR_LUT_OFST12_2_new_data_196;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84C4h
extern volatile uint8_t xdata g_rw_gamvrr2_00C5h_GAM_VRR_LUT_OFST12_2_new_data_197;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84C5h
extern volatile uint8_t xdata g_rw_gamvrr2_00C6h_GAM_VRR_LUT_OFST12_2_new_data_198;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84C6h
extern volatile uint8_t xdata g_rw_gamvrr2_00C7h_GAM_VRR_LUT_OFST12_2_new_data_199;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84C7h

extern volatile uint8_t xdata g_rw_gamvrr2_00C8h_GAM_VRR_LUT_OFST12_2_new_data_200;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84C8h
extern volatile uint8_t xdata g_rw_gamvrr2_00C9h_GAM_VRR_LUT_OFST12_2_new_data_201;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84C9h
extern volatile uint8_t xdata g_rw_gamvrr2_00CAh_GAM_VRR_LUT_OFST12_2_new_data_202;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84CAh
extern volatile uint8_t xdata g_rw_gamvrr2_00CBh_GAM_VRR_LUT_OFST12_2_new_data_203;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84CBh
extern volatile uint8_t xdata g_rw_gamvrr2_00CCh_GAM_VRR_LUT_OFST12_2_new_data_204;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84CCh

extern volatile uint8_t xdata g_rw_gamvrr2_00CDh_GAM_VRR_LUT_OFST12_2_new_data_205;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84CDh
extern volatile uint8_t xdata g_rw_gamvrr2_00CEh_GAM_VRR_LUT_OFST12_2_new_data_206;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84CEh
extern volatile uint8_t xdata g_rw_gamvrr2_00CFh_GAM_VRR_LUT_OFST12_2_new_data_207;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84CFh
extern volatile uint8_t xdata g_rw_gamvrr2_00D0h_GAM_VRR_LUT_OFST12_2_new_data_208;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84D0h
extern volatile uint8_t xdata g_rw_gamvrr2_00D1h_GAM_VRR_LUT_OFST12_2_new_data_209;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84D1h

extern volatile uint8_t xdata g_rw_gamvrr2_00D2h_GAM_VRR_LUT_OFST12_2_new_data_210;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84D2h
extern volatile uint8_t xdata g_rw_gamvrr2_00D3h_GAM_VRR_LUT_OFST12_2_new_data_211;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84D3h
extern volatile uint8_t xdata g_rw_gamvrr2_00D4h_GAM_VRR_LUT_OFST12_2_new_data_212;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84D4h
extern volatile uint8_t xdata g_rw_gamvrr2_00D5h_GAM_VRR_LUT_OFST12_2_new_data_213;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84D5h
extern volatile uint8_t xdata g_rw_gamvrr2_00D6h_GAM_VRR_LUT_OFST12_2_new_data_214;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84D6h

extern volatile uint8_t xdata g_rw_gamvrr2_00D7h_GAM_VRR_LUT_OFST12_2_new_data_215;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84D7h
extern volatile uint8_t xdata g_rw_gamvrr2_00D8h_GAM_VRR_LUT_OFST12_2_new_data_216;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84D8h
extern volatile uint8_t xdata g_rw_gamvrr2_00D9h_GAM_VRR_LUT_OFST12_2_new_data_217;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84D9h
extern volatile uint8_t xdata g_rw_gamvrr2_00DAh_GAM_VRR_LUT_OFST12_2_new_data_218;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84DAh
extern volatile uint8_t xdata g_rw_gamvrr2_00DBh_GAM_VRR_LUT_OFST12_2_new_data_219;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84DBh

extern volatile uint8_t xdata g_rw_gamvrr2_00DCh_GAM_VRR_LUT_OFST12_2_new_data_220;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84DCh
extern volatile uint8_t xdata g_rw_gamvrr2_00DDh_GAM_VRR_LUT_OFST12_2_new_data_221;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84DDh
extern volatile uint8_t xdata g_rw_gamvrr2_00DEh_GAM_VRR_LUT_OFST12_2_new_data_222;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84DEh
extern volatile uint8_t xdata g_rw_gamvrr2_00DFh_GAM_VRR_LUT_OFST12_2_new_data_223;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84DFh
extern volatile uint8_t xdata g_rw_gamvrr2_00E0h_GAM_VRR_LUT_OFST12_2_new_data_224;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84E0h

extern volatile uint8_t xdata g_rw_gamvrr2_00E1h_GAM_VRR_LUT_OFST12_2_new_data_225;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84E1h
extern volatile uint8_t xdata g_rw_gamvrr2_00E2h_GAM_VRR_LUT_OFST12_2_new_data_226;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84E2h
extern volatile uint8_t xdata g_rw_gamvrr2_00E3h_GAM_VRR_LUT_OFST12_2_new_data_227;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84E3h
extern volatile uint8_t xdata g_rw_gamvrr2_00E4h_GAM_VRR_LUT_OFST12_2_new_data_228;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84E4h
extern volatile uint8_t xdata g_rw_gamvrr2_00E5h_GAM_VRR_LUT_OFST12_2_new_data_229;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84E5h

extern volatile uint8_t xdata g_rw_gamvrr2_00E6h_GAM_VRR_LUT_OFST12_2_new_data_230;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84E6h
extern volatile uint8_t xdata g_rw_gamvrr2_00E7h_GAM_VRR_LUT_OFST12_2_new_data_231;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84E7h
extern volatile uint8_t xdata g_rw_gamvrr2_00E8h_GAM_VRR_LUT_OFST12_2_new_data_232;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84E8h
extern volatile uint8_t xdata g_rw_gamvrr2_00E9h_GAM_VRR_LUT_OFST12_2_new_data_233;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84E9h
extern volatile uint8_t xdata g_rw_gamvrr2_00EAh_GAM_VRR_LUT_OFST12_2_new_data_234;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84EAh

extern volatile uint8_t xdata g_rw_gamvrr2_00EBh_GAM_VRR_LUT_OFST12_2_new_data_235;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84EBh
extern volatile uint8_t xdata g_rw_gamvrr2_00ECh_GAM_VRR_LUT_OFST12_2_new_data_236;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84ECh
extern volatile uint8_t xdata g_rw_gamvrr2_00EDh_GAM_VRR_LUT_OFST12_2_new_data_237;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84EDh
extern volatile uint8_t xdata g_rw_gamvrr2_00EEh_GAM_VRR_LUT_OFST12_2_new_data_238;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84EEh
extern volatile uint8_t xdata g_rw_gamvrr2_00EFh_GAM_VRR_LUT_OFST12_2_new_data_239;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84EFh

extern volatile uint8_t xdata g_rw_gamvrr2_00F0h_GAM_VRR_LUT_OFST12_2_new_data_240;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84F0h
extern volatile uint8_t xdata g_rw_gamvrr2_00F1h_GAM_VRR_LUT_OFST12_2_new_data_241;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84F1h
extern volatile uint8_t xdata g_rw_gamvrr2_00F2h_GAM_VRR_LUT_OFST12_2_new_data_242;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84F2h
extern volatile uint8_t xdata g_rw_gamvrr2_00F3h_GAM_VRR_LUT_OFST12_2_new_data_243;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84F3h
extern volatile uint8_t xdata g_rw_gamvrr2_00F4h_GAM_VRR_LUT_OFST12_2_new_data_244;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84F4h

extern volatile uint8_t xdata g_rw_gamvrr2_00F5h_GAM_VRR_LUT_OFST12_2_new_data_245;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84F5h
extern volatile uint8_t xdata g_rw_gamvrr2_00F6h_GAM_VRR_LUT_OFST12_2_new_data_246;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84F6h
extern volatile uint8_t xdata g_rw_gamvrr2_00F7h_GAM_VRR_LUT_OFST12_2_new_data_247;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84F7h
extern volatile uint8_t xdata g_rw_gamvrr2_00F8h_GAM_VRR_LUT_OFST12_2_new_data_248;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84F8h
extern volatile uint8_t xdata g_rw_gamvrr2_00F9h_GAM_VRR_LUT_OFST12_2_new_data_249;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84F9h

extern volatile uint8_t xdata g_rw_gamvrr2_00FAh_GAM_VRR_LUT_OFST12_2_new_data_250;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84FAh
extern volatile uint8_t xdata g_rw_gamvrr2_00FBh_GAM_VRR_LUT_OFST12_2_new_data_251;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84FBh
extern volatile uint8_t xdata g_rw_gamvrr2_00FCh_GAM_VRR_LUT_OFST12_2_new_data_252;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84FCh
extern volatile uint8_t xdata g_rw_gamvrr2_00FDh_GAM_VRR_LUT_OFST12_2_new_data_253;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84FDh
extern volatile uint8_t xdata g_rw_gamvrr2_00FEh_GAM_VRR_LUT_OFST12_2_new_data_254;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84FEh

extern volatile uint8_t xdata g_rw_gamvrr2_00FFh_GAM_VRR_LUT_OFST12_2_new_data_255;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 84FFh
extern volatile uint8_t xdata g_rw_gamvrr2_0100h_GAM_VRR_LUT_OFST12_2_new_data_256;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8500h
extern volatile uint8_t xdata g_rw_gamvrr2_0101h_GAM_VRR_LUT_OFST12_2_new_data_257;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8501h
extern volatile uint8_t xdata g_rw_gamvrr2_0102h_GAM_VRR_LUT_OFST12_2_new_data_258;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8502h
extern volatile uint8_t xdata g_rw_gamvrr2_0103h_GAM_VRR_LUT_OFST12_2_new_data_259;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8503h

extern volatile uint8_t xdata g_rw_gamvrr2_0104h_GAM_VRR_LUT_OFST12_2_new_data_260;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8504h
extern volatile uint8_t xdata g_rw_gamvrr2_0105h_GAM_VRR_LUT_OFST12_2_new_data_261;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8505h
extern volatile uint8_t xdata g_rw_gamvrr2_0106h_GAM_VRR_LUT_OFST12_2_new_data_262;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8506h
extern volatile uint8_t xdata g_rw_gamvrr2_0107h_GAM_VRR_LUT_OFST12_2_new_data_263;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8507h
extern volatile uint8_t xdata g_rw_gamvrr2_0108h_GAM_VRR_LUT_OFST12_2_new_data_264;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8508h

extern volatile uint8_t xdata g_rw_gamvrr2_0109h_GAM_VRR_LUT_OFST12_2_new_data_265;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8509h
extern volatile uint8_t xdata g_rw_gamvrr2_010Ah_GAM_VRR_LUT_OFST12_2_new_data_266;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 850Ah
extern volatile uint8_t xdata g_rw_gamvrr2_010Bh_GAM_VRR_LUT_OFST12_2_new_data_267;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 850Bh
extern volatile uint8_t xdata g_rw_gamvrr2_010Ch_GAM_VRR_LUT_OFST12_2_new_data_268;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 850Ch
extern volatile uint8_t xdata g_rw_gamvrr2_010Dh_GAM_VRR_LUT_OFST12_2_new_data_269;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 850Dh

extern volatile uint8_t xdata g_rw_gamvrr2_010Eh_GAM_VRR_LUT_OFST12_2_new_data_270;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 850Eh
extern volatile uint8_t xdata g_rw_gamvrr2_010Fh_GAM_VRR_LUT_OFST12_2_new_data_271;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 850Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0110h_GAM_VRR_LUT_OFST12_2_new_data_272;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8510h
extern volatile uint8_t xdata g_rw_gamvrr2_0111h_GAM_VRR_LUT_OFST12_2_new_data_273;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8511h
extern volatile uint8_t xdata g_rw_gamvrr2_0112h_GAM_VRR_LUT_OFST12_2_new_data_274;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8512h

extern volatile uint8_t xdata g_rw_gamvrr2_0113h_GAM_VRR_LUT_OFST12_2_new_data_275;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8513h
extern volatile uint8_t xdata g_rw_gamvrr2_0114h_GAM_VRR_LUT_OFST12_2_new_data_276;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8514h
extern volatile uint8_t xdata g_rw_gamvrr2_0115h_GAM_VRR_LUT_OFST12_2_new_data_277;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8515h
extern volatile uint8_t xdata g_rw_gamvrr2_0116h_GAM_VRR_LUT_OFST12_2_new_data_278;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8516h
extern volatile uint8_t xdata g_rw_gamvrr2_0117h_GAM_VRR_LUT_OFST12_2_new_data_279;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8517h

extern volatile uint8_t xdata g_rw_gamvrr2_0118h_GAM_VRR_LUT_OFST12_2_new_data_280;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8518h
extern volatile uint8_t xdata g_rw_gamvrr2_0119h_GAM_VRR_LUT_OFST12_2_new_data_281;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8519h
extern volatile uint8_t xdata g_rw_gamvrr2_011Ah_GAM_VRR_LUT_OFST12_2_new_data_282;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 851Ah
extern volatile uint8_t xdata g_rw_gamvrr2_011Bh_GAM_VRR_LUT_OFST12_2_new_data_283;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 851Bh
extern volatile uint8_t xdata g_rw_gamvrr2_011Ch_GAM_VRR_LUT_OFST12_2_new_data_284;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 851Ch

extern volatile uint8_t xdata g_rw_gamvrr2_011Dh_GAM_VRR_LUT_OFST12_2_new_data_285;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 851Dh
extern volatile uint8_t xdata g_rw_gamvrr2_011Eh_GAM_VRR_LUT_OFST12_2_new_data_286;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 851Eh
extern volatile uint8_t xdata g_rw_gamvrr2_011Fh_GAM_VRR_LUT_OFST12_2_new_data_287;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 851Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0120h_GAM_VRR_LUT_OFST12_2_new_data_288;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8520h
extern volatile uint8_t xdata g_rw_gamvrr2_0121h_GAM_VRR_LUT_OFST12_2_new_data_289;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8521h

extern volatile uint8_t xdata g_rw_gamvrr2_0122h_GAM_VRR_LUT_OFST12_2_new_data_290;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8522h
extern volatile uint8_t xdata g_rw_gamvrr2_0123h_GAM_VRR_LUT_OFST12_2_new_data_291;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8523h
extern volatile uint8_t xdata g_rw_gamvrr2_0124h_GAM_VRR_LUT_OFST12_2_new_data_292;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8524h
extern volatile uint8_t xdata g_rw_gamvrr2_0125h_GAM_VRR_LUT_OFST12_2_new_data_293;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8525h
extern volatile uint8_t xdata g_rw_gamvrr2_0126h_GAM_VRR_LUT_OFST12_2_new_data_294;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8526h

extern volatile uint8_t xdata g_rw_gamvrr2_0127h_GAM_VRR_LUT_OFST12_2_new_data_295;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8527h
extern volatile uint8_t xdata g_rw_gamvrr2_0128h_GAM_VRR_LUT_OFST12_2_new_data_296;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8528h
extern volatile uint8_t xdata g_rw_gamvrr2_0129h_GAM_VRR_LUT_OFST12_2_new_data_297;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8529h
extern volatile uint8_t xdata g_rw_gamvrr2_012Ah_GAM_VRR_LUT_OFST12_2_new_data_298;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 852Ah
extern volatile uint8_t xdata g_rw_gamvrr2_012Bh_GAM_VRR_LUT_OFST12_2_new_data_299;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 852Bh

extern volatile uint8_t xdata g_rw_gamvrr2_012Ch_GAM_VRR_LUT_OFST12_2_new_data_300;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 852Ch
extern volatile uint8_t xdata g_rw_gamvrr2_012Dh_GAM_VRR_LUT_OFST12_2_new_data_301;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 852Dh
extern volatile uint8_t xdata g_rw_gamvrr2_012Eh_GAM_VRR_LUT_OFST12_2_new_data_302;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 852Eh
extern volatile uint8_t xdata g_rw_gamvrr2_012Fh_GAM_VRR_LUT_OFST12_2_new_data_303;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 852Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0130h_GAM_VRR_LUT_OFST12_2_new_data_304;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8530h

extern volatile uint8_t xdata g_rw_gamvrr2_0131h_GAM_VRR_LUT_OFST12_2_new_data_305;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8531h
extern volatile uint8_t xdata g_rw_gamvrr2_0132h_GAM_VRR_LUT_OFST12_2_new_data_306;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8532h
extern volatile uint8_t xdata g_rw_gamvrr2_0133h_GAM_VRR_LUT_OFST12_2_new_data_307;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8533h
extern volatile uint8_t xdata g_rw_gamvrr2_0134h_GAM_VRR_LUT_OFST12_2_new_data_308;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8534h
extern volatile uint8_t xdata g_rw_gamvrr2_0135h_GAM_VRR_LUT_OFST12_2_new_data_309;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8535h

extern volatile uint8_t xdata g_rw_gamvrr2_0136h_GAM_VRR_LUT_OFST12_2_new_data_310;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8536h
extern volatile uint8_t xdata g_rw_gamvrr2_0137h_GAM_VRR_LUT_OFST12_2_new_data_311;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8537h
extern volatile uint8_t xdata g_rw_gamvrr2_0138h_GAM_VRR_LUT_OFST12_2_new_data_312;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8538h
extern volatile uint8_t xdata g_rw_gamvrr2_0139h_GAM_VRR_LUT_OFST12_2_new_data_313;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8539h
extern volatile uint8_t xdata g_rw_gamvrr2_013Ah_GAM_VRR_LUT_OFST12_2_new_data_314;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 853Ah

extern volatile uint8_t xdata g_rw_gamvrr2_013Bh_GAM_VRR_LUT_OFST12_2_new_data_315;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 853Bh
extern volatile uint8_t xdata g_rw_gamvrr2_013Ch_GAM_VRR_LUT_OFST12_2_new_data_316;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 853Ch
extern volatile uint8_t xdata g_rw_gamvrr2_013Dh_GAM_VRR_LUT_OFST12_2_new_data_317;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 853Dh
extern volatile uint8_t xdata g_rw_gamvrr2_013Eh_GAM_VRR_LUT_OFST12_2_new_data_318;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 853Eh
extern volatile uint8_t xdata g_rw_gamvrr2_013Fh_GAM_VRR_LUT_OFST12_2_new_data_319;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 853Fh

extern volatile uint8_t xdata g_rw_gamvrr2_0140h_GAM_VRR_LUT_OFST12_2_new_data_320;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8540h
extern volatile uint8_t xdata g_rw_gamvrr2_0141h_GAM_VRR_LUT_OFST12_2_new_data_321;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8541h
extern volatile uint8_t xdata g_rw_gamvrr2_0142h_GAM_VRR_LUT_OFST12_2_new_data_322;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8542h
extern volatile uint8_t xdata g_rw_gamvrr2_0143h_GAM_VRR_LUT_OFST12_2_new_data_323;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8543h
extern volatile uint8_t xdata g_rw_gamvrr2_0144h_GAM_VRR_LUT_OFST12_2_new_data_324;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8544h

extern volatile uint8_t xdata g_rw_gamvrr2_0145h_GAM_VRR_LUT_OFST12_2_new_data_325;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8545h
extern volatile uint8_t xdata g_rw_gamvrr2_0146h_GAM_VRR_LUT_OFST12_2_new_data_326;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8546h
extern volatile uint8_t xdata g_rw_gamvrr2_0147h_GAM_VRR_LUT_OFST12_2_new_data_327;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8547h
extern volatile uint8_t xdata g_rw_gamvrr2_0148h_GAM_VRR_LUT_OFST12_2_new_data_328;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8548h
extern volatile uint8_t xdata g_rw_gamvrr2_0149h_GAM_VRR_LUT_OFST12_2_new_data_329;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8549h

extern volatile uint8_t xdata g_rw_gamvrr2_014Ah_GAM_VRR_LUT_OFST12_2_new_data_330;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 854Ah
extern volatile uint8_t xdata g_rw_gamvrr2_014Bh_GAM_VRR_LUT_OFST12_2_new_data_331;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 854Bh
extern volatile uint8_t xdata g_rw_gamvrr2_014Ch_GAM_VRR_LUT_OFST12_2_new_data_332;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 854Ch
extern volatile uint8_t xdata g_rw_gamvrr2_014Dh_GAM_VRR_LUT_OFST12_2_new_data_333;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 854Dh
extern volatile uint8_t xdata g_rw_gamvrr2_014Eh_GAM_VRR_LUT_OFST12_2_new_data_334;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 854Eh

extern volatile uint8_t xdata g_rw_gamvrr2_014Fh_GAM_VRR_LUT_OFST12_2_new_data_335;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 854Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0150h_GAM_VRR_LUT_OFST12_2_new_data_336;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8550h
extern volatile uint8_t xdata g_rw_gamvrr2_0151h_GAM_VRR_LUT_OFST12_2_new_data_337;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8551h
extern volatile uint8_t xdata g_rw_gamvrr2_0152h_GAM_VRR_LUT_OFST12_2_new_data_338;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8552h
extern volatile uint8_t xdata g_rw_gamvrr2_0153h_GAM_VRR_LUT_OFST12_2_new_data_339;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8553h

extern volatile uint8_t xdata g_rw_gamvrr2_0154h_GAM_VRR_LUT_OFST12_2_new_data_340;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8554h
extern volatile uint8_t xdata g_rw_gamvrr2_0155h_GAM_VRR_LUT_OFST12_2_new_data_341;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8555h
extern volatile uint8_t xdata g_rw_gamvrr2_0156h_GAM_VRR_LUT_OFST12_2_new_data_342;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8556h
extern volatile uint8_t xdata g_rw_gamvrr2_0157h_GAM_VRR_LUT_OFST12_2_new_data_343;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8557h
extern volatile uint8_t xdata g_rw_gamvrr2_0158h_GAM_VRR_LUT_OFST12_2_new_data_344;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8558h

extern volatile uint8_t xdata g_rw_gamvrr2_0159h_GAM_VRR_LUT_OFST12_2_new_data_345;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8559h
extern volatile uint8_t xdata g_rw_gamvrr2_015Ah_GAM_VRR_LUT_OFST12_2_new_data_346;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 855Ah
extern volatile uint8_t xdata g_rw_gamvrr2_015Bh_GAM_VRR_LUT_OFST12_2_new_data_347;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 855Bh
extern volatile uint8_t xdata g_rw_gamvrr2_015Ch_GAM_VRR_LUT_OFST12_2_new_data_348;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 855Ch
extern volatile uint8_t xdata g_rw_gamvrr2_015Dh_GAM_VRR_LUT_OFST12_2_new_data_349;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 855Dh

extern volatile uint8_t xdata g_rw_gamvrr2_015Eh_GAM_VRR_LUT_OFST12_2_new_data_350;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 855Eh
extern volatile uint8_t xdata g_rw_gamvrr2_015Fh_GAM_VRR_LUT_OFST12_2_new_data_351;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 855Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0160h_GAM_VRR_LUT_OFST12_2_new_data_352;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8560h
extern volatile uint8_t xdata g_rw_gamvrr2_0161h_GAM_VRR_LUT_OFST12_2_new_data_353;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8561h
extern volatile uint8_t xdata g_rw_gamvrr2_0162h_GAM_VRR_LUT_OFST12_2_new_data_354;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8562h

extern volatile uint8_t xdata g_rw_gamvrr2_0163h_GAM_VRR_LUT_OFST12_2_new_data_355;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8563h
extern volatile uint8_t xdata g_rw_gamvrr2_0164h_GAM_VRR_LUT_OFST12_2_new_data_356;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8564h
extern volatile uint8_t xdata g_rw_gamvrr2_0165h_GAM_VRR_LUT_OFST12_2_new_data_357;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8565h
extern volatile uint8_t xdata g_rw_gamvrr2_0166h_GAM_VRR_LUT_OFST12_2_new_data_358;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8566h
extern volatile uint8_t xdata g_rw_gamvrr2_0167h_GAM_VRR_LUT_OFST12_2_new_data_359;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8567h

extern volatile uint8_t xdata g_rw_gamvrr2_0168h_GAM_VRR_LUT_OFST12_2_new_data_360;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8568h
extern volatile uint8_t xdata g_rw_gamvrr2_0169h_GAM_VRR_LUT_OFST12_2_new_data_361;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8569h
extern volatile uint8_t xdata g_rw_gamvrr2_016Ah_GAM_VRR_LUT_OFST12_2_new_data_362;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 856Ah
extern volatile uint8_t xdata g_rw_gamvrr2_016Bh_GAM_VRR_LUT_OFST12_2_new_data_363;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 856Bh
extern volatile uint8_t xdata g_rw_gamvrr2_016Ch_GAM_VRR_LUT_OFST12_2_new_data_364;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 856Ch

extern volatile uint8_t xdata g_rw_gamvrr2_016Dh_GAM_VRR_LUT_OFST12_2_new_data_365;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 856Dh
extern volatile uint8_t xdata g_rw_gamvrr2_016Eh_GAM_VRR_LUT_OFST12_2_new_data_366;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 856Eh
extern volatile uint8_t xdata g_rw_gamvrr2_016Fh_GAM_VRR_LUT_OFST12_2_new_data_367;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 856Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0170h_GAM_VRR_LUT_OFST12_2_new_data_368;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8570h
extern volatile uint8_t xdata g_rw_gamvrr2_0171h_GAM_VRR_LUT_OFST12_2_new_data_369;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8571h

extern volatile uint8_t xdata g_rw_gamvrr2_0172h_GAM_VRR_LUT_OFST12_2_new_data_370;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8572h
extern volatile uint8_t xdata g_rw_gamvrr2_0173h_GAM_VRR_LUT_OFST12_2_new_data_371;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8573h
extern volatile uint8_t xdata g_rw_gamvrr2_0174h_GAM_VRR_LUT_OFST12_2_new_data_372;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8574h
extern volatile uint8_t xdata g_rw_gamvrr2_0175h_GAM_VRR_LUT_OFST12_2_new_data_373;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8575h
extern volatile uint8_t xdata g_rw_gamvrr2_0176h_GAM_VRR_LUT_OFST12_2_new_data_374;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8576h

extern volatile uint8_t xdata g_rw_gamvrr2_0177h_GAM_VRR_LUT_OFST12_2_new_data_375;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8577h
extern volatile uint8_t xdata g_rw_gamvrr2_0178h_GAM_VRR_LUT_OFST12_2_new_data_376;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8578h
extern volatile uint8_t xdata g_rw_gamvrr2_0179h_GAM_VRR_LUT_OFST12_2_new_data_377;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8579h
extern volatile uint8_t xdata g_rw_gamvrr2_017Ah_GAM_VRR_LUT_OFST12_2_new_data_378;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 857Ah
extern volatile uint8_t xdata g_rw_gamvrr2_017Bh_GAM_VRR_LUT_OFST12_2_new_data_379;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 857Bh

extern volatile uint8_t xdata g_rw_gamvrr2_017Ch_GAM_VRR_LUT_OFST12_2_new_data_380;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 857Ch
extern volatile uint8_t xdata g_rw_gamvrr2_017Dh_GAM_VRR_LUT_OFST12_2_new_data_381;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 857Dh
extern volatile uint8_t xdata g_rw_gamvrr2_017Eh_GAM_VRR_LUT_OFST12_2_new_data_382;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 857Eh
extern volatile uint8_t xdata g_rw_gamvrr2_017Fh_GAM_VRR_LUT_OFST12_2_new_data_383;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 857Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0180h_GAM_VRR_LUT_OFST12_2_new_data_384;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8580h

extern volatile uint8_t xdata g_rw_gamvrr2_0181h_GAM_VRR_LUT_OFST12_2_new_data_385;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8581h
extern volatile uint8_t xdata g_rw_gamvrr2_0182h_GAM_VRR_LUT_OFST12_2_new_data_386;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8582h
extern volatile uint8_t xdata g_rw_gamvrr2_0183h_GAM_VRR_LUT_OFST12_2_new_data_387;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8583h
extern volatile uint8_t xdata g_rw_gamvrr2_0184h_GAM_VRR_LUT_OFST12_2_new_data_388;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8584h
extern volatile uint8_t xdata g_rw_gamvrr2_0185h_GAM_VRR_LUT_OFST12_2_new_data_389;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8585h

extern volatile uint8_t xdata g_rw_gamvrr2_0186h_GAM_VRR_LUT_OFST12_2_new_data_390;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8586h
extern volatile uint8_t xdata g_rw_gamvrr2_0187h_GAM_VRR_LUT_OFST12_2_new_data_391;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8587h
extern volatile uint8_t xdata g_rw_gamvrr2_0188h_GAM_VRR_LUT_OFST12_2_new_data_392;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8588h
extern volatile uint8_t xdata g_rw_gamvrr2_0189h_GAM_VRR_LUT_OFST12_2_new_data_393;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8589h
extern volatile uint8_t xdata g_rw_gamvrr2_018Ah_GAM_VRR_LUT_OFST12_2_new_data_394;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 858Ah

extern volatile uint8_t xdata g_rw_gamvrr2_018Bh_GAM_VRR_LUT_OFST12_2_new_data_395;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 858Bh
extern volatile uint8_t xdata g_rw_gamvrr2_018Ch_GAM_VRR_LUT_OFST12_2_new_data_396;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 858Ch
extern volatile uint8_t xdata g_rw_gamvrr2_018Dh_GAM_VRR_LUT_OFST12_2_new_data_397;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 858Dh
extern volatile uint8_t xdata g_rw_gamvrr2_018Eh_GAM_VRR_LUT_OFST12_2_new_data_398;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 858Eh
extern volatile uint8_t xdata g_rw_gamvrr2_018Fh_GAM_VRR_LUT_OFST12_2_new_data_399;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 858Fh

extern volatile uint8_t xdata g_rw_gamvrr2_0190h_GAM_VRR_LUT_OFST12_2_new_data_400;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8590h
extern volatile uint8_t xdata g_rw_gamvrr2_0191h_GAM_VRR_LUT_OFST12_2_new_data_401;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8591h
extern volatile uint8_t xdata g_rw_gamvrr2_0192h_GAM_VRR_LUT_OFST12_2_new_data_402;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8592h
extern volatile uint8_t xdata g_rw_gamvrr2_0193h_GAM_VRR_LUT_OFST12_2_new_data_403;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8593h
extern volatile uint8_t xdata g_rw_gamvrr2_0194h_GAM_VRR_LUT_OFST12_2_new_data_404;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8594h

extern volatile uint8_t xdata g_rw_gamvrr2_0195h_GAM_VRR_LUT_OFST12_2_new_data_405;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8595h
extern volatile uint8_t xdata g_rw_gamvrr2_0196h_GAM_VRR_LUT_OFST12_2_new_data_406;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8596h
extern volatile uint8_t xdata g_rw_gamvrr2_0197h_GAM_VRR_LUT_OFST12_2_new_data_407;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8597h
extern volatile uint8_t xdata g_rw_gamvrr2_0198h_GAM_VRR_LUT_OFST12_2_new_data_408;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8598h
extern volatile uint8_t xdata g_rw_gamvrr2_0199h_GAM_VRR_LUT_OFST12_2_new_data_409;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8599h

extern volatile uint8_t xdata g_rw_gamvrr2_019Ah_GAM_VRR_LUT_OFST12_2_new_data_410;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 859Ah
extern volatile uint8_t xdata g_rw_gamvrr2_019Bh_GAM_VRR_LUT_OFST12_2_new_data_411;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 859Bh
extern volatile uint8_t xdata g_rw_gamvrr2_019Ch_GAM_VRR_LUT_OFST12_2_new_data_412;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 859Ch
extern volatile uint8_t xdata g_rw_gamvrr2_019Dh_GAM_VRR_LUT_OFST12_2_new_data_413;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 859Dh
extern volatile uint8_t xdata g_rw_gamvrr2_019Eh_GAM_VRR_LUT_OFST12_2_new_data_414;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 859Eh

extern volatile uint8_t xdata g_rw_gamvrr2_019Fh_GAM_VRR_LUT_OFST12_2_new_data_415;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 859Fh
extern volatile uint8_t xdata g_rw_gamvrr2_01A0h_GAM_VRR_LUT_OFST12_2_new_data_416;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85A0h
extern volatile uint8_t xdata g_rw_gamvrr2_01A1h_GAM_VRR_LUT_OFST12_2_new_data_417;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85A1h
extern volatile uint8_t xdata g_rw_gamvrr2_01A2h_GAM_VRR_LUT_OFST12_2_new_data_418;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85A2h
extern volatile uint8_t xdata g_rw_gamvrr2_01A3h_GAM_VRR_LUT_OFST12_2_new_data_419;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85A3h

extern volatile uint8_t xdata g_rw_gamvrr2_01A4h_GAM_VRR_LUT_OFST12_2_new_data_420;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85A4h
extern volatile uint8_t xdata g_rw_gamvrr2_01A5h_GAM_VRR_LUT_OFST12_2_new_data_421;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85A5h
extern volatile uint8_t xdata g_rw_gamvrr2_01A6h_GAM_VRR_LUT_OFST12_2_new_data_422;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85A6h
extern volatile uint8_t xdata g_rw_gamvrr2_01A7h_GAM_VRR_LUT_OFST12_2_new_data_423;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85A7h
extern volatile uint8_t xdata g_rw_gamvrr2_01A8h_GAM_VRR_LUT_OFST12_2_new_data_424;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85A8h

extern volatile uint8_t xdata g_rw_gamvrr2_01A9h_GAM_VRR_LUT_OFST12_2_new_data_425;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85A9h
extern volatile uint8_t xdata g_rw_gamvrr2_01AAh_GAM_VRR_LUT_OFST12_2_new_data_426;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85AAh
extern volatile uint8_t xdata g_rw_gamvrr2_01ABh_GAM_VRR_LUT_OFST12_2_new_data_427;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85ABh
extern volatile uint8_t xdata g_rw_gamvrr2_01ACh_GAM_VRR_LUT_OFST12_2_new_data_428;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85ACh
extern volatile uint8_t xdata g_rw_gamvrr2_01ADh_GAM_VRR_LUT_OFST12_2_new_data_429;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85ADh

extern volatile uint8_t xdata g_rw_gamvrr2_01AEh_GAM_VRR_LUT_OFST12_2_new_data_430;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85AEh
extern volatile uint8_t xdata g_rw_gamvrr2_01AFh_GAM_VRR_LUT_OFST12_2_new_data_431;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85AFh
extern volatile uint8_t xdata g_rw_gamvrr2_01B0h_GAM_VRR_LUT_OFST12_2_new_data_432;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85B0h
extern volatile uint8_t xdata g_rw_gamvrr2_01B1h_GAM_VRR_LUT_OFST12_2_new_data_433;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85B1h
extern volatile uint8_t xdata g_rw_gamvrr2_01B2h_GAM_VRR_LUT_OFST12_2_new_data_434;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85B2h

extern volatile uint8_t xdata g_rw_gamvrr2_01B3h_GAM_VRR_LUT_OFST12_2_new_data_435;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85B3h
extern volatile uint8_t xdata g_rw_gamvrr2_01B4h_GAM_VRR_LUT_OFST12_2_new_data_436;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85B4h
extern volatile uint8_t xdata g_rw_gamvrr2_01B5h_GAM_VRR_LUT_OFST12_2_new_data_437;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85B5h
extern volatile uint8_t xdata g_rw_gamvrr2_01B6h_GAM_VRR_LUT_OFST12_2_new_data_438;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85B6h
extern volatile uint8_t xdata g_rw_gamvrr2_01B7h_GAM_VRR_LUT_OFST12_2_new_data_439;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85B7h

extern volatile uint8_t xdata g_rw_gamvrr2_01B8h_GAM_VRR_LUT_OFST12_2_new_data_440;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85B8h
extern volatile uint8_t xdata g_rw_gamvrr2_01B9h_GAM_VRR_LUT_OFST12_2_new_data_441;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85B9h
extern volatile uint8_t xdata g_rw_gamvrr2_01BAh_GAM_VRR_LUT_OFST12_2_new_data_442;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85BAh
extern volatile uint8_t xdata g_rw_gamvrr2_01BBh_GAM_VRR_LUT_OFST12_2_new_data_443;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85BBh
extern volatile uint8_t xdata g_rw_gamvrr2_01BCh_GAM_VRR_LUT_OFST12_2_new_data_444;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85BCh

extern volatile uint8_t xdata g_rw_gamvrr2_01BDh_GAM_VRR_LUT_OFST12_2_new_data_445;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85BDh
extern volatile uint8_t xdata g_rw_gamvrr2_01BEh_GAM_VRR_LUT_OFST12_2_new_data_446;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85BEh
extern volatile uint8_t xdata g_rw_gamvrr2_01BFh_GAM_VRR_LUT_OFST12_2_new_data_447;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85BFh
extern volatile uint8_t xdata g_rw_gamvrr2_01C0h_GAM_VRR_LUT_OFST12_2_new_data_448;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85C0h
extern volatile uint8_t xdata g_rw_gamvrr2_01C1h_GAM_VRR_LUT_OFST12_2_new_data_449;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85C1h

extern volatile uint8_t xdata g_rw_gamvrr2_01C2h_GAM_VRR_LUT_OFST12_2_new_data_450;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85C2h
extern volatile uint8_t xdata g_rw_gamvrr2_01C3h_GAM_VRR_LUT_OFST12_2_new_data_451;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85C3h
extern volatile uint8_t xdata g_rw_gamvrr2_01C4h_GAM_VRR_LUT_OFST12_2_new_data_452;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85C4h
extern volatile uint8_t xdata g_rw_gamvrr2_01C5h_GAM_VRR_LUT_OFST12_2_new_data_453;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85C5h
extern volatile uint8_t xdata g_rw_gamvrr2_01C6h_GAM_VRR_LUT_OFST12_2_new_data_454;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85C6h

extern volatile uint8_t xdata g_rw_gamvrr2_01C7h_GAM_VRR_LUT_OFST12_2_new_data_455;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85C7h
extern volatile uint8_t xdata g_rw_gamvrr2_01C8h_GAM_VRR_LUT_OFST12_2_new_data_456;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85C8h
extern volatile uint8_t xdata g_rw_gamvrr2_01C9h_GAM_VRR_LUT_OFST12_2_new_data_457;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85C9h
extern volatile uint8_t xdata g_rw_gamvrr2_01CAh_GAM_VRR_LUT_OFST12_2_new_data_458;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85CAh
extern volatile uint8_t xdata g_rw_gamvrr2_01CBh_GAM_VRR_LUT_OFST12_2_new_data_459;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85CBh

extern volatile uint8_t xdata g_rw_gamvrr2_01CCh_GAM_VRR_LUT_OFST12_2_new_data_460;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85CCh
extern volatile uint8_t xdata g_rw_gamvrr2_01CDh_GAM_VRR_LUT_OFST12_2_new_data_461;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85CDh
extern volatile uint8_t xdata g_rw_gamvrr2_01CEh_GAM_VRR_LUT_OFST12_2_new_data_462;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85CEh
extern volatile uint8_t xdata g_rw_gamvrr2_01CFh_GAM_VRR_LUT_OFST12_2_new_data_463;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85CFh
extern volatile uint8_t xdata g_rw_gamvrr2_01D0h_GAM_VRR_LUT_OFST12_2_new_data_464;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85D0h

extern volatile uint8_t xdata g_rw_gamvrr2_01D1h_GAM_VRR_LUT_OFST12_2_new_data_465;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85D1h
extern volatile uint8_t xdata g_rw_gamvrr2_01D2h_GAM_VRR_LUT_OFST12_2_new_data_466;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85D2h
extern volatile uint8_t xdata g_rw_gamvrr2_01D3h_GAM_VRR_LUT_OFST12_2_new_data_467;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85D3h
extern volatile uint8_t xdata g_rw_gamvrr2_01D4h_GAM_VRR_LUT_OFST12_2_new_data_468;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85D4h
extern volatile uint8_t xdata g_rw_gamvrr2_01D5h_GAM_VRR_LUT_OFST12_2_new_data_469;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85D5h

extern volatile uint8_t xdata g_rw_gamvrr2_01D6h_GAM_VRR_LUT_OFST12_2_new_data_470;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85D6h
extern volatile uint8_t xdata g_rw_gamvrr2_01D7h_GAM_VRR_LUT_OFST12_2_new_data_471;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85D7h
extern volatile uint8_t xdata g_rw_gamvrr2_01D8h_GAM_VRR_LUT_OFST12_2_new_data_472;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85D8h
extern volatile uint8_t xdata g_rw_gamvrr2_01D9h_GAM_VRR_LUT_OFST12_2_new_data_473;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85D9h
extern volatile uint8_t xdata g_rw_gamvrr2_01DAh_GAM_VRR_LUT_OFST12_2_new_data_474;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85DAh

extern volatile uint8_t xdata g_rw_gamvrr2_01DBh_GAM_VRR_LUT_OFST12_2_new_data_475;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85DBh
extern volatile uint8_t xdata g_rw_gamvrr2_01DCh_GAM_VRR_LUT_OFST12_2_new_data_476;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85DCh
extern volatile uint8_t xdata g_rw_gamvrr2_01DDh_GAM_VRR_LUT_OFST12_2_new_data_477;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85DDh
extern volatile uint8_t xdata g_rw_gamvrr2_01DEh_GAM_VRR_LUT_OFST12_2_new_data_478;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85DEh
extern volatile uint8_t xdata g_rw_gamvrr2_01DFh_GAM_VRR_LUT_OFST12_2_new_data_479;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85DFh

extern volatile uint8_t xdata g_rw_gamvrr2_01E0h_GAM_VRR_LUT_OFST12_2_new_data_480;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85E0h
extern volatile uint8_t xdata g_rw_gamvrr2_01E1h_GAM_VRR_LUT_OFST12_2_new_data_481;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85E1h
extern volatile uint8_t xdata g_rw_gamvrr2_01E2h_GAM_VRR_LUT_OFST12_2_new_data_482;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85E2h
extern volatile uint8_t xdata g_rw_gamvrr2_01E3h_GAM_VRR_LUT_OFST12_2_new_data_483;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85E3h
extern volatile uint8_t xdata g_rw_gamvrr2_01E4h_GAM_VRR_LUT_OFST12_2_new_data_484;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85E4h

extern volatile uint8_t xdata g_rw_gamvrr2_01E5h_GAM_VRR_LUT_OFST12_2_new_data_485;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85E5h
extern volatile uint8_t xdata g_rw_gamvrr2_01E6h_GAM_VRR_LUT_OFST12_2_new_data_486;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85E6h
extern volatile uint8_t xdata g_rw_gamvrr2_01E7h_GAM_VRR_LUT_OFST12_2_new_data_487;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85E7h
extern volatile uint8_t xdata g_rw_gamvrr2_01E8h_GAM_VRR_LUT_OFST12_2_new_data_488;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85E8h
extern volatile uint8_t xdata g_rw_gamvrr2_01E9h_GAM_VRR_LUT_OFST12_2_new_data_489;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85E9h

extern volatile uint8_t xdata g_rw_gamvrr2_01EAh_GAM_VRR_LUT_OFST12_2_new_data_490;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85EAh
extern volatile uint8_t xdata g_rw_gamvrr2_01EBh_GAM_VRR_LUT_OFST12_2_new_data_491;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85EBh
extern volatile uint8_t xdata g_rw_gamvrr2_01ECh_GAM_VRR_LUT_OFST12_2_new_data_492;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85ECh
extern volatile uint8_t xdata g_rw_gamvrr2_01EDh_GAM_VRR_LUT_OFST12_2_new_data_493;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85EDh
extern volatile uint8_t xdata g_rw_gamvrr2_01EEh_GAM_VRR_LUT_OFST12_2_new_data_494;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85EEh

extern volatile uint8_t xdata g_rw_gamvrr2_01EFh_GAM_VRR_LUT_OFST12_2_new_data_495;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85EFh
extern volatile uint8_t xdata g_rw_gamvrr2_01F0h_GAM_VRR_LUT_OFST12_2_new_data_496;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85F0h
extern volatile uint8_t xdata g_rw_gamvrr2_01F1h_GAM_VRR_LUT_OFST12_2_new_data_497;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85F1h
extern volatile uint8_t xdata g_rw_gamvrr2_01F2h_GAM_VRR_LUT_OFST12_2_new_data_498;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85F2h
extern volatile uint8_t xdata g_rw_gamvrr2_01F3h_GAM_VRR_LUT_OFST12_2_new_data_499;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85F3h

extern volatile uint8_t xdata g_rw_gamvrr2_01F4h_GAM_VRR_LUT_OFST12_2_new_data_500;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85F4h
extern volatile uint8_t xdata g_rw_gamvrr2_01F5h_GAM_VRR_LUT_OFST12_2_new_data_501;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85F5h
extern volatile uint8_t xdata g_rw_gamvrr2_01F6h_GAM_VRR_LUT_OFST12_2_new_data_502;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85F6h
extern volatile uint8_t xdata g_rw_gamvrr2_01F7h_GAM_VRR_LUT_OFST12_2_new_data_503;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85F7h
extern volatile uint8_t xdata g_rw_gamvrr2_01F8h_GAM_VRR_LUT_OFST12_2_new_data_504;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85F8h

extern volatile uint8_t xdata g_rw_gamvrr2_01F9h_GAM_VRR_LUT_OFST12_2_new_data_505;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85F9h
extern volatile uint8_t xdata g_rw_gamvrr2_01FAh_GAM_VRR_LUT_OFST12_2_new_data_506;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85FAh
extern volatile uint8_t xdata g_rw_gamvrr2_01FBh_GAM_VRR_LUT_OFST12_2_new_data_507;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85FBh
extern volatile uint8_t xdata g_rw_gamvrr2_01FCh_GAM_VRR_LUT_OFST12_2_new_data_508;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85FCh
extern volatile uint8_t xdata g_rw_gamvrr2_01FDh_GAM_VRR_LUT_OFST12_2_new_data_509;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85FDh

extern volatile uint8_t xdata g_rw_gamvrr2_01FEh_GAM_VRR_LUT_OFST12_2_new_data_510;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85FEh
extern volatile uint8_t xdata g_rw_gamvrr2_01FFh_GAM_VRR_LUT_OFST12_2_new_data_511;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 85FFh
extern volatile uint8_t xdata g_rw_gamvrr2_0200h_GAM_VRR_LUT_OFST12_2_new_data_512;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8600h
extern volatile uint8_t xdata g_rw_gamvrr2_0201h_GAM_VRR_LUT_OFST12_2_new_data_513;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8601h
extern volatile uint8_t xdata g_rw_gamvrr2_0202h_GAM_VRR_LUT_OFST12_2_new_data_514;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8602h

extern volatile uint8_t xdata g_rw_gamvrr2_0203h_GAM_VRR_LUT_OFST12_2_new_data_515;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8603h
extern volatile uint8_t xdata g_rw_gamvrr2_0204h_GAM_VRR_LUT_OFST12_2_new_data_516;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8604h
extern volatile uint8_t xdata g_rw_gamvrr2_0205h_GAM_VRR_LUT_OFST12_2_new_data_517;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8605h
extern volatile uint8_t xdata g_rw_gamvrr2_0206h_GAM_VRR_LUT_OFST12_2_new_data_518;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8606h
extern volatile uint8_t xdata g_rw_gamvrr2_0207h_GAM_VRR_LUT_OFST12_2_new_data_519;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8607h

extern volatile uint8_t xdata g_rw_gamvrr2_0208h_GAM_VRR_LUT_OFST12_2_new_data_520;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8608h
extern volatile uint8_t xdata g_rw_gamvrr2_0209h_GAM_VRR_LUT_OFST12_2_new_data_521;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8609h
extern volatile uint8_t xdata g_rw_gamvrr2_020Ah_GAM_VRR_LUT_OFST12_2_new_data_522;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 860Ah
extern volatile uint8_t xdata g_rw_gamvrr2_020Bh_GAM_VRR_LUT_OFST12_2_new_data_523;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 860Bh
extern volatile uint8_t xdata g_rw_gamvrr2_020Ch_GAM_VRR_LUT_OFST12_2_new_data_524;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 860Ch

extern volatile uint8_t xdata g_rw_gamvrr2_020Dh_GAM_VRR_LUT_OFST12_2_new_data_525;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 860Dh
extern volatile uint8_t xdata g_rw_gamvrr2_020Eh_GAM_VRR_LUT_OFST12_2_new_data_526;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 860Eh
extern volatile uint8_t xdata g_rw_gamvrr2_020Fh_GAM_VRR_LUT_OFST12_2_new_data_527;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 860Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0210h_GAM_VRR_LUT_OFST12_2_new_data_528;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8610h
extern volatile uint8_t xdata g_rw_gamvrr2_0211h_GAM_VRR_LUT_OFST12_2_new_data_529;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8611h

extern volatile uint8_t xdata g_rw_gamvrr2_0212h_GAM_VRR_LUT_OFST12_2_new_data_530;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8612h
extern volatile uint8_t xdata g_rw_gamvrr2_0213h_GAM_VRR_LUT_OFST12_2_new_data_531;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8613h
extern volatile uint8_t xdata g_rw_gamvrr2_0214h_GAM_VRR_LUT_OFST12_2_new_data_532;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8614h
extern volatile uint8_t xdata g_rw_gamvrr2_0215h_GAM_VRR_LUT_OFST12_2_new_data_533;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8615h
extern volatile uint8_t xdata g_rw_gamvrr2_0216h_GAM_VRR_LUT_OFST12_2_new_data_534;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8616h

extern volatile uint8_t xdata g_rw_gamvrr2_0217h_GAM_VRR_LUT_OFST12_2_new_data_535;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8617h
extern volatile uint8_t xdata g_rw_gamvrr2_0218h_GAM_VRR_LUT_OFST12_2_new_data_536;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8618h
extern volatile uint8_t xdata g_rw_gamvrr2_0219h_GAM_VRR_LUT_OFST12_2_new_data_537;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8619h
extern volatile uint8_t xdata g_rw_gamvrr2_021Ah_GAM_VRR_LUT_OFST12_2_new_data_538;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 861Ah
extern volatile uint8_t xdata g_rw_gamvrr2_021Bh_GAM_VRR_LUT_OFST12_2_new_data_539;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 861Bh

extern volatile uint8_t xdata g_rw_gamvrr2_021Ch_GAM_VRR_LUT_OFST12_2_new_data_540;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 861Ch
extern volatile uint8_t xdata g_rw_gamvrr2_021Dh_GAM_VRR_LUT_OFST12_2_new_data_541;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 861Dh
extern volatile uint8_t xdata g_rw_gamvrr2_021Eh_GAM_VRR_LUT_OFST12_2_new_data_542;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 861Eh
extern volatile uint8_t xdata g_rw_gamvrr2_021Fh_GAM_VRR_LUT_OFST12_2_new_data_543;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 861Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0220h_GAM_VRR_LUT_OFST12_2_new_data_544;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8620h

extern volatile uint8_t xdata g_rw_gamvrr2_0221h_GAM_VRR_LUT_OFST12_2_new_data_545;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8621h
extern volatile uint8_t xdata g_rw_gamvrr2_0222h_GAM_VRR_LUT_OFST12_2_new_data_546;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8622h
extern volatile uint8_t xdata g_rw_gamvrr2_0223h_GAM_VRR_LUT_OFST12_2_new_data_547;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8623h
extern volatile uint8_t xdata g_rw_gamvrr2_0224h_GAM_VRR_LUT_OFST12_2_new_data_548;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8624h
extern volatile uint8_t xdata g_rw_gamvrr2_0225h_GAM_VRR_LUT_OFST12_2_new_data_549;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8625h

extern volatile uint8_t xdata g_rw_gamvrr2_0226h_GAM_VRR_LUT_OFST12_2_new_data_550;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8626h
extern volatile uint8_t xdata g_rw_gamvrr2_0227h_GAM_VRR_LUT_OFST12_2_new_data_551;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8627h
extern volatile uint8_t xdata g_rw_gamvrr2_0228h_GAM_VRR_LUT_OFST12_2_new_data_552;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8628h
extern volatile uint8_t xdata g_rw_gamvrr2_0229h_GAM_VRR_LUT_OFST12_2_new_data_553;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8629h
extern volatile uint8_t xdata g_rw_gamvrr2_022Ah_GAM_VRR_LUT_OFST12_2_new_data_554;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 862Ah

extern volatile uint8_t xdata g_rw_gamvrr2_022Bh_GAM_VRR_LUT_OFST12_2_new_data_555;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 862Bh
extern volatile uint8_t xdata g_rw_gamvrr2_022Ch_GAM_VRR_LUT_OFST12_2_new_data_556;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 862Ch
extern volatile uint8_t xdata g_rw_gamvrr2_022Dh_GAM_VRR_LUT_OFST12_2_new_data_557;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 862Dh
extern volatile uint8_t xdata g_rw_gamvrr2_022Eh_GAM_VRR_LUT_OFST12_2_new_data_558;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 862Eh
extern volatile uint8_t xdata g_rw_gamvrr2_022Fh_GAM_VRR_LUT_OFST12_2_new_data_559;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 862Fh

extern volatile uint8_t xdata g_rw_gamvrr2_0230h_GAM_VRR_LUT_OFST12_2_new_data_560;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8630h
extern volatile uint8_t xdata g_rw_gamvrr2_0231h_GAM_VRR_LUT_OFST12_2_new_data_561;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8631h
extern volatile uint8_t xdata g_rw_gamvrr2_0232h_GAM_VRR_LUT_OFST12_2_new_data_562;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8632h
extern volatile uint8_t xdata g_rw_gamvrr2_0233h_GAM_VRR_LUT_OFST12_2_new_data_563;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8633h
extern volatile uint8_t xdata g_rw_gamvrr2_0234h_GAM_VRR_LUT_OFST12_2_new_data_564;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8634h

extern volatile uint8_t xdata g_rw_gamvrr2_0235h_GAM_VRR_LUT_OFST12_2_new_data_565;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8635h
extern volatile uint8_t xdata g_rw_gamvrr2_0236h_GAM_VRR_LUT_OFST12_2_new_data_566;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8636h
extern volatile uint8_t xdata g_rw_gamvrr2_0237h_GAM_VRR_LUT_OFST12_2_new_data_567;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8637h
extern volatile uint8_t xdata g_rw_gamvrr2_0238h_GAM_VRR_LUT_OFST12_2_new_data_568;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8638h
extern volatile uint8_t xdata g_rw_gamvrr2_0239h_GAM_VRR_LUT_OFST12_2_new_data_569;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8639h

extern volatile uint8_t xdata g_rw_gamvrr2_023Ah_GAM_VRR_LUT_OFST12_2_new_data_570;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 863Ah
extern volatile uint8_t xdata g_rw_gamvrr2_023Bh_GAM_VRR_LUT_OFST12_2_new_data_571;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 863Bh
extern volatile uint8_t xdata g_rw_gamvrr2_023Ch_GAM_VRR_LUT_OFST12_2_new_data_572;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 863Ch
extern volatile uint8_t xdata g_rw_gamvrr2_023Dh_GAM_VRR_LUT_OFST12_2_new_data_573;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 863Dh
extern volatile uint8_t xdata g_rw_gamvrr2_023Eh_GAM_VRR_LUT_OFST12_2_new_data_574;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 863Eh

extern volatile uint8_t xdata g_rw_gamvrr2_023Fh_GAM_VRR_LUT_OFST12_2_new_data_575;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 863Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0240h_GAM_VRR_LUT_OFST12_2_new_data_576;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8640h
extern volatile uint8_t xdata g_rw_gamvrr2_0241h_GAM_VRR_LUT_OFST12_2_new_data_577;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8641h
extern volatile uint8_t xdata g_rw_gamvrr2_0242h_GAM_VRR_LUT_OFST12_2_new_data_578;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8642h
extern volatile uint8_t xdata g_rw_gamvrr2_0243h_GAM_VRR_LUT_OFST12_2_new_data_579;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8643h

extern volatile uint8_t xdata g_rw_gamvrr2_0244h_GAM_VRR_LUT_OFST12_2_new_data_580;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8644h
extern volatile uint8_t xdata g_rw_gamvrr2_0245h_GAM_VRR_LUT_OFST12_2_new_data_581;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8645h
extern volatile uint8_t xdata g_rw_gamvrr2_0246h_GAM_VRR_LUT_OFST12_2_new_data_582;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8646h
extern volatile uint8_t xdata g_rw_gamvrr2_0247h_GAM_VRR_LUT_OFST12_2_new_data_583;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8647h
extern volatile uint8_t xdata g_rw_gamvrr2_0248h_GAM_VRR_LUT_OFST12_2_new_data_584;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8648h

extern volatile uint8_t xdata g_rw_gamvrr2_0249h_GAM_VRR_LUT_OFST12_2_new_data_585;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8649h
extern volatile uint8_t xdata g_rw_gamvrr2_024Ah_GAM_VRR_LUT_OFST12_2_new_data_586;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 864Ah
extern volatile uint8_t xdata g_rw_gamvrr2_024Bh_GAM_VRR_LUT_OFST12_2_new_data_587;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 864Bh
extern volatile uint8_t xdata g_rw_gamvrr2_024Ch_GAM_VRR_LUT_OFST12_2_new_data_588;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 864Ch
extern volatile uint8_t xdata g_rw_gamvrr2_024Dh_GAM_VRR_LUT_OFST12_2_new_data_589;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 864Dh

extern volatile uint8_t xdata g_rw_gamvrr2_024Eh_GAM_VRR_LUT_OFST12_2_new_data_590;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 864Eh
extern volatile uint8_t xdata g_rw_gamvrr2_024Fh_GAM_VRR_LUT_OFST12_2_new_data_591;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 864Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0250h_GAM_VRR_LUT_OFST12_2_new_data_592;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8650h
extern volatile uint8_t xdata g_rw_gamvrr2_0251h_GAM_VRR_LUT_OFST12_2_new_data_593;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8651h
extern volatile uint8_t xdata g_rw_gamvrr2_0252h_GAM_VRR_LUT_OFST12_2_new_data_594;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8652h

extern volatile uint8_t xdata g_rw_gamvrr2_0253h_GAM_VRR_LUT_OFST12_2_new_data_595;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8653h
extern volatile uint8_t xdata g_rw_gamvrr2_0254h_GAM_VRR_LUT_OFST12_2_new_data_596;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8654h
extern volatile uint8_t xdata g_rw_gamvrr2_0255h_GAM_VRR_LUT_OFST12_2_new_data_597;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8655h
extern volatile uint8_t xdata g_rw_gamvrr2_0256h_GAM_VRR_LUT_OFST12_2_new_data_598;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8656h
extern volatile uint8_t xdata g_rw_gamvrr2_0257h_GAM_VRR_LUT_OFST12_2_new_data_599;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8657h

extern volatile uint8_t xdata g_rw_gamvrr2_0258h_GAM_VRR_LUT_OFST12_2_new_data_600;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8658h
extern volatile uint8_t xdata g_rw_gamvrr2_0259h_GAM_VRR_LUT_OFST12_2_new_data_601;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8659h
extern volatile uint8_t xdata g_rw_gamvrr2_025Ah_GAM_VRR_LUT_OFST12_2_new_data_602;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 865Ah
extern volatile uint8_t xdata g_rw_gamvrr2_025Bh_GAM_VRR_LUT_OFST12_2_new_data_603;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 865Bh
extern volatile uint8_t xdata g_rw_gamvrr2_025Ch_GAM_VRR_LUT_OFST12_2_new_data_604;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 865Ch

extern volatile uint8_t xdata g_rw_gamvrr2_025Dh_GAM_VRR_LUT_OFST12_2_new_data_605;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 865Dh
extern volatile uint8_t xdata g_rw_gamvrr2_025Eh_GAM_VRR_LUT_OFST12_2_new_data_606;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 865Eh
extern volatile uint8_t xdata g_rw_gamvrr2_025Fh_GAM_VRR_LUT_OFST12_2_new_data_607;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 865Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0260h_GAM_VRR_LUT_OFST12_2_new_data_608;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8660h
extern volatile uint8_t xdata g_rw_gamvrr2_0261h_GAM_VRR_LUT_OFST12_2_new_data_609;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8661h

extern volatile uint8_t xdata g_rw_gamvrr2_0262h_GAM_VRR_LUT_OFST12_2_new_data_610;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8662h
extern volatile uint8_t xdata g_rw_gamvrr2_0263h_GAM_VRR_LUT_OFST12_2_new_data_611;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8663h
extern volatile uint8_t xdata g_rw_gamvrr2_0264h_GAM_VRR_LUT_OFST12_2_new_data_612;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8664h
extern volatile uint8_t xdata g_rw_gamvrr2_0265h_GAM_VRR_LUT_OFST12_2_new_data_613;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8665h
extern volatile uint8_t xdata g_rw_gamvrr2_0266h_GAM_VRR_LUT_OFST12_2_new_data_614;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8666h

extern volatile uint8_t xdata g_rw_gamvrr2_0267h_GAM_VRR_LUT_OFST12_2_new_data_615;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8667h
extern volatile uint8_t xdata g_rw_gamvrr2_0268h_GAM_VRR_LUT_OFST12_2_new_data_616;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8668h
extern volatile uint8_t xdata g_rw_gamvrr2_0269h_GAM_VRR_LUT_OFST12_2_new_data_617;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8669h
extern volatile uint8_t xdata g_rw_gamvrr2_026Ah_GAM_VRR_LUT_OFST12_2_new_data_618;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 866Ah
extern volatile uint8_t xdata g_rw_gamvrr2_026Bh_GAM_VRR_LUT_OFST12_2_new_data_619;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 866Bh

extern volatile uint8_t xdata g_rw_gamvrr2_026Ch_GAM_VRR_LUT_OFST12_2_new_data_620;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 866Ch
extern volatile uint8_t xdata g_rw_gamvrr2_026Dh_GAM_VRR_LUT_OFST12_2_new_data_621;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 866Dh
extern volatile uint8_t xdata g_rw_gamvrr2_026Eh_GAM_VRR_LUT_OFST12_2_new_data_622;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 866Eh
extern volatile uint8_t xdata g_rw_gamvrr2_026Fh_GAM_VRR_LUT_OFST12_2_new_data_623;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 866Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0270h_GAM_VRR_LUT_OFST12_2_new_data_624;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8670h

extern volatile uint8_t xdata g_rw_gamvrr2_0271h_GAM_VRR_LUT_OFST12_2_new_data_625;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8671h
extern volatile uint8_t xdata g_rw_gamvrr2_0272h_GAM_VRR_LUT_OFST12_2_new_data_626;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8672h
extern volatile uint8_t xdata g_rw_gamvrr2_0273h_GAM_VRR_LUT_OFST12_2_new_data_627;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8673h
extern volatile uint8_t xdata g_rw_gamvrr2_0274h_GAM_VRR_LUT_OFST12_2_new_data_628;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8674h
extern volatile uint8_t xdata g_rw_gamvrr2_0275h_GAM_VRR_LUT_OFST12_2_new_data_629;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8675h

extern volatile uint8_t xdata g_rw_gamvrr2_0276h_GAM_VRR_LUT_OFST12_2_new_data_630;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8676h
extern volatile uint8_t xdata g_rw_gamvrr2_0277h_GAM_VRR_LUT_OFST12_2_new_data_631;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8677h
extern volatile uint8_t xdata g_rw_gamvrr2_0278h_GAM_VRR_LUT_OFST12_2_new_data_632;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8678h
extern volatile uint8_t xdata g_rw_gamvrr2_0279h_GAM_VRR_LUT_OFST12_2_new_data_633;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8679h
extern volatile uint8_t xdata g_rw_gamvrr2_027Ah_GAM_VRR_LUT_OFST12_2_new_data_634;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 867Ah

extern volatile uint8_t xdata g_rw_gamvrr2_027Bh_GAM_VRR_LUT_OFST12_2_new_data_635;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 867Bh
extern volatile uint8_t xdata g_rw_gamvrr2_027Ch_GAM_VRR_LUT_OFST12_2_new_data_636;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 867Ch
extern volatile uint8_t xdata g_rw_gamvrr2_027Dh_GAM_VRR_LUT_OFST12_2_new_data_637;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 867Dh
extern volatile uint8_t xdata g_rw_gamvrr2_027Eh_GAM_VRR_LUT_OFST12_2_new_data_638;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 867Eh
extern volatile uint8_t xdata g_rw_gamvrr2_027Fh_GAM_VRR_LUT_OFST12_2_new_data_639;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 867Fh

extern volatile uint8_t xdata g_rw_gamvrr2_0280h_GAM_VRR_LUT_OFST12_2_new_data_640;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8680h
extern volatile uint8_t xdata g_rw_gamvrr2_0281h_GAM_VRR_LUT_OFST12_2_new_data_641;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8681h
extern volatile uint8_t xdata g_rw_gamvrr2_0282h_GAM_VRR_LUT_OFST12_2_new_data_642;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8682h
extern volatile uint8_t xdata g_rw_gamvrr2_0283h_GAM_VRR_LUT_OFST12_2_new_data_643;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8683h
extern volatile uint8_t xdata g_rw_gamvrr2_0284h_GAM_VRR_LUT_OFST12_2_new_data_644;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8684h

extern volatile uint8_t xdata g_rw_gamvrr2_0285h_GAM_VRR_LUT_OFST12_2_new_data_645;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8685h
extern volatile uint8_t xdata g_rw_gamvrr2_0286h_GAM_VRR_LUT_OFST12_2_new_data_646;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8686h
extern volatile uint8_t xdata g_rw_gamvrr2_0287h_GAM_VRR_LUT_OFST12_2_new_data_647;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8687h
extern volatile uint8_t xdata g_rw_gamvrr2_0288h_GAM_VRR_LUT_OFST12_2_new_data_648;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8688h
extern volatile uint8_t xdata g_rw_gamvrr2_0289h_GAM_VRR_LUT_OFST12_2_new_data_649;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8689h

extern volatile uint8_t xdata g_rw_gamvrr2_028Ah_GAM_VRR_LUT_OFST12_2_new_data_650;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 868Ah
extern volatile uint8_t xdata g_rw_gamvrr2_028Bh_GAM_VRR_LUT_OFST12_2_new_data_651;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 868Bh
extern volatile uint8_t xdata g_rw_gamvrr2_028Ch_GAM_VRR_LUT_OFST12_2_new_data_652;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 868Ch
extern volatile uint8_t xdata g_rw_gamvrr2_028Dh_GAM_VRR_LUT_OFST12_2_new_data_653;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 868Dh
extern volatile uint8_t xdata g_rw_gamvrr2_028Eh_GAM_VRR_LUT_OFST12_2_new_data_654;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 868Eh

extern volatile uint8_t xdata g_rw_gamvrr2_028Fh_GAM_VRR_LUT_OFST12_2_new_data_655;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 868Fh
extern volatile uint8_t xdata g_rw_gamvrr2_0290h_GAM_VRR_LUT_OFST12_2_new_data_656;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8690h
extern volatile uint8_t xdata g_rw_gamvrr2_0291h_GAM_VRR_LUT_OFST12_2_new_data_657;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8691h
extern volatile uint8_t xdata g_rw_gamvrr2_0292h_GAM_VRR_LUT_OFST12_2_new_data_658;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8692h
extern volatile uint8_t xdata g_rw_gamvrr2_0293h_GAM_VRR_LUT_OFST12_2_new_data_659;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8693h

extern volatile uint8_t xdata g_rw_gamvrr2_0294h_GAM_VRR_LUT_OFST12_2_new_data_660;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8694h
extern volatile uint8_t xdata g_rw_gamvrr2_0295h_GAM_VRR_LUT_OFST12_2_new_data_661;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8695h
extern volatile uint8_t xdata g_rw_gamvrr2_0296h_GAM_VRR_LUT_OFST12_2_new_data_662;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8696h
extern volatile uint8_t xdata g_rw_gamvrr2_0297h_GAM_VRR_LUT_OFST12_2_new_data_663;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8697h
extern volatile uint8_t xdata g_rw_gamvrr2_0298h_GAM_VRR_LUT_OFST12_2_new_data_664;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8698h

extern volatile uint8_t xdata g_rw_gamvrr2_0299h_GAM_VRR_LUT_OFST12_2_new_data_665;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8699h
extern volatile uint8_t xdata g_rw_gamvrr2_029Ah_GAM_VRR_LUT_OFST12_2_new_data_666;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 869Ah
extern volatile uint8_t xdata g_rw_gamvrr2_029Bh_GAM_VRR_LUT_OFST12_2_new_data_667;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 869Bh
extern volatile uint8_t xdata g_rw_gamvrr2_029Ch_GAM_VRR_LUT_OFST12_2_new_data_668;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 869Ch
extern volatile uint8_t xdata g_rw_gamvrr2_029Dh_GAM_VRR_LUT_OFST12_2_new_data_669;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 869Dh

extern volatile uint8_t xdata g_rw_gamvrr2_029Eh_GAM_VRR_LUT_OFST12_2_new_data_670;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 869Eh
extern volatile uint8_t xdata g_rw_gamvrr2_029Fh_GAM_VRR_LUT_OFST12_2_new_data_671;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 869Fh
extern volatile uint8_t xdata g_rw_gamvrr2_02A0h_GAM_VRR_LUT_OFST12_2_new_data_672;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86A0h
extern volatile uint8_t xdata g_rw_gamvrr2_02A1h_GAM_VRR_LUT_OFST12_2_new_data_673;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86A1h
extern volatile uint8_t xdata g_rw_gamvrr2_02A2h_GAM_VRR_LUT_OFST12_2_new_data_674;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86A2h

extern volatile uint8_t xdata g_rw_gamvrr2_02A3h_GAM_VRR_LUT_OFST12_2_new_data_675;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86A3h
extern volatile uint8_t xdata g_rw_gamvrr2_02A4h_GAM_VRR_LUT_OFST12_2_new_data_676;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86A4h
extern volatile uint8_t xdata g_rw_gamvrr2_02A5h_GAM_VRR_LUT_OFST12_2_new_data_677;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86A5h
extern volatile uint8_t xdata g_rw_gamvrr2_02A6h_GAM_VRR_LUT_OFST12_2_new_data_678;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86A6h
extern volatile uint8_t xdata g_rw_gamvrr2_02A7h_GAM_VRR_LUT_OFST12_2_new_data_679;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86A7h

extern volatile uint8_t xdata g_rw_gamvrr2_02A8h_GAM_VRR_LUT_OFST12_2_new_data_680;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86A8h
extern volatile uint8_t xdata g_rw_gamvrr2_02A9h_GAM_VRR_LUT_OFST12_2_new_data_681;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86A9h
extern volatile uint8_t xdata g_rw_gamvrr2_02AAh_GAM_VRR_LUT_OFST12_2_new_data_682;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86AAh
extern volatile uint8_t xdata g_rw_gamvrr2_02ABh_GAM_VRR_LUT_OFST12_2_new_data_683;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86ABh
extern volatile uint8_t xdata g_rw_gamvrr2_02ACh_GAM_VRR_LUT_OFST12_2_new_data_684;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86ACh

extern volatile uint8_t xdata g_rw_gamvrr2_02ADh_GAM_VRR_LUT_OFST12_2_new_data_685;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86ADh
extern volatile uint8_t xdata g_rw_gamvrr2_02AEh_GAM_VRR_LUT_OFST12_2_new_data_686;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86AEh
extern volatile uint8_t xdata g_rw_gamvrr2_02AFh_GAM_VRR_LUT_OFST12_2_new_data_687;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86AFh
extern volatile uint8_t xdata g_rw_gamvrr2_02B0h_GAM_VRR_LUT_OFST12_2_new_data_688;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86B0h
extern volatile uint8_t xdata g_rw_gamvrr2_02B1h_GAM_VRR_LUT_OFST12_2_new_data_689;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86B1h

extern volatile uint8_t xdata g_rw_gamvrr2_02B2h_GAM_VRR_LUT_OFST12_2_new_data_690;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86B2h
extern volatile uint8_t xdata g_rw_gamvrr2_02B3h_GAM_VRR_LUT_OFST12_2_new_data_691;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86B3h
extern volatile uint8_t xdata g_rw_gamvrr2_02B4h_GAM_VRR_LUT_OFST12_2_new_data_692;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86B4h
extern volatile uint8_t xdata g_rw_gamvrr2_02B5h_GAM_VRR_LUT_OFST12_2_new_data_693;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86B5h
extern volatile uint8_t xdata g_rw_gamvrr2_02B6h_GAM_VRR_LUT_OFST12_2_new_data_694;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86B6h

extern volatile uint8_t xdata g_rw_gamvrr2_02B7h_GAM_VRR_LUT_OFST12_2_new_data_695;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86B7h
extern volatile uint8_t xdata g_rw_gamvrr2_02B8h_GAM_VRR_LUT_OFST12_2_new_data_696;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86B8h
extern volatile uint8_t xdata g_rw_gamvrr2_02B9h_GAM_VRR_LUT_OFST12_2_new_data_697;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86B9h
extern volatile uint8_t xdata g_rw_gamvrr2_02BAh_GAM_VRR_LUT_OFST12_2_new_data_698;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86BAh
extern volatile uint8_t xdata g_rw_gamvrr2_02BBh_GAM_VRR_LUT_OFST12_2_new_data_699;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86BBh

extern volatile uint8_t xdata g_rw_gamvrr2_02BCh_GAM_VRR_LUT_OFST12_2_new_data_700;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86BCh
extern volatile uint8_t xdata g_rw_gamvrr2_02BDh_GAM_VRR_LUT_OFST12_2_new_data_701;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86BDh
extern volatile uint8_t xdata g_rw_gamvrr2_02BEh_GAM_VRR_LUT_OFST12_2_new_data_702;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86BEh
extern volatile uint8_t xdata g_rw_gamvrr2_02BFh_GAM_VRR_LUT_OFST12_2_new_data_703;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86BFh
extern volatile uint8_t xdata g_rw_gamvrr2_02C0h_GAM_VRR_LUT_OFST12_2_new_data_704;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86C0h

extern volatile uint8_t xdata g_rw_gamvrr2_02C1h_GAM_VRR_LUT_OFST12_2_new_data_705;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86C1h
extern volatile uint8_t xdata g_rw_gamvrr2_02C2h_GAM_VRR_LUT_OFST12_2_new_data_706;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86C2h
extern volatile uint8_t xdata g_rw_gamvrr2_02C3h_GAM_VRR_LUT_OFST12_2_new_data_707;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86C3h
extern volatile uint8_t xdata g_rw_gamvrr2_02C4h_GAM_VRR_LUT_OFST12_2_new_data_708;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86C4h
extern volatile uint8_t xdata g_rw_gamvrr2_02C5h_GAM_VRR_LUT_OFST12_2_new_data_709;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86C5h

extern volatile uint8_t xdata g_rw_gamvrr2_02C6h_GAM_VRR_LUT_OFST12_2_new_data_710;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86C6h
extern volatile uint8_t xdata g_rw_gamvrr2_02C7h_GAM_VRR_LUT_OFST12_2_new_data_711;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86C7h
extern volatile uint8_t xdata g_rw_gamvrr2_02C8h_GAM_VRR_LUT_OFST12_2_new_data_712;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86C8h
extern volatile uint8_t xdata g_rw_gamvrr2_02C9h_GAM_VRR_LUT_OFST12_2_new_data_713;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86C9h
extern volatile uint8_t xdata g_rw_gamvrr2_02CAh_GAM_VRR_LUT_OFST12_2_new_data_714;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86CAh

extern volatile uint8_t xdata g_rw_gamvrr2_02CBh_GAM_VRR_LUT_OFST12_2_new_data_715;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86CBh
extern volatile uint8_t xdata g_rw_gamvrr2_02CCh_GAM_VRR_LUT_OFST12_2_new_data_716;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86CCh
extern volatile uint8_t xdata g_rw_gamvrr2_02CDh_GAM_VRR_LUT_OFST12_2_new_data_717;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86CDh
extern volatile uint8_t xdata g_rw_gamvrr2_02CEh_GAM_VRR_LUT_OFST12_2_new_data_718;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86CEh
extern volatile uint8_t xdata g_rw_gamvrr2_02CFh_GAM_VRR_LUT_OFST12_2_new_data_719;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86CFh

extern volatile uint8_t xdata g_rw_gamvrr2_02D0h_GAM_VRR_LUT_OFST12_2_new_data_720;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86D0h
extern volatile uint8_t xdata g_rw_gamvrr2_02D1h_GAM_VRR_LUT_OFST12_2_new_data_721;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86D1h
extern volatile uint8_t xdata g_rw_gamvrr2_02D2h_GAM_VRR_LUT_OFST12_2_new_data_722;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86D2h
extern volatile uint8_t xdata g_rw_gamvrr2_02D3h_GAM_VRR_LUT_OFST12_2_new_data_723;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86D3h
extern volatile uint8_t xdata g_rw_gamvrr2_02D4h_GAM_VRR_LUT_OFST12_2_new_data_724;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86D4h

extern volatile uint8_t xdata g_rw_gamvrr2_02D5h_GAM_VRR_LUT_OFST12_2_new_data_725;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86D5h
extern volatile uint8_t xdata g_rw_gamvrr2_02D6h_GAM_VRR_LUT_OFST12_2_new_data_726;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86D6h
extern volatile uint8_t xdata g_rw_gamvrr2_02D7h_GAM_VRR_LUT_OFST12_2_new_data_727;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86D7h
extern volatile uint8_t xdata g_rw_gamvrr2_02D8h_GAM_VRR_LUT_OFST12_2_new_data_728;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86D8h
extern volatile uint8_t xdata g_rw_gamvrr2_02D9h_GAM_VRR_LUT_OFST12_2_new_data_729;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86D9h

extern volatile uint8_t xdata g_rw_gamvrr2_02DAh_GAM_VRR_LUT_OFST12_2_new_data_730;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86DAh
extern volatile uint8_t xdata g_rw_gamvrr2_02DBh_GAM_VRR_LUT_OFST12_2_new_data_731;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86DBh
extern volatile uint8_t xdata g_rw_gamvrr2_02DCh_GAM_VRR_LUT_OFST12_2_new_data_732;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86DCh
extern volatile uint8_t xdata g_rw_gamvrr2_02DDh_GAM_VRR_LUT_OFST12_2_new_data_733;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86DDh
extern volatile uint8_t xdata g_rw_gamvrr2_02DEh_GAM_VRR_LUT_OFST12_2_new_data_734;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86DEh

extern volatile uint8_t xdata g_rw_gamvrr2_02DFh_GAM_VRR_LUT_OFST12_2_new_data_735;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86DFh
extern volatile uint8_t xdata g_rw_gamvrr2_02E0h_GAM_VRR_LUT_OFST12_2_new_data_736;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86E0h
extern volatile uint8_t xdata g_rw_gamvrr2_02E1h_GAM_VRR_LUT_OFST12_2_new_data_737;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86E1h
extern volatile uint8_t xdata g_rw_gamvrr2_02E2h_GAM_VRR_LUT_OFST12_2_new_data_738;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86E2h
extern volatile uint8_t xdata g_rw_gamvrr2_02E3h_GAM_VRR_LUT_OFST12_2_new_data_739;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86E3h

extern volatile uint8_t xdata g_rw_gamvrr2_02E4h_GAM_VRR_LUT_OFST12_2_new_data_740;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86E4h
extern volatile uint8_t xdata g_rw_gamvrr2_02E5h_GAM_VRR_LUT_OFST12_2_new_data_741;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86E5h
extern volatile uint8_t xdata g_rw_gamvrr2_02E6h_GAM_VRR_LUT_OFST12_2_new_data_742;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86E6h
extern volatile uint8_t xdata g_rw_gamvrr2_02E7h_GAM_VRR_LUT_OFST12_2_new_data_743;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86E7h
extern volatile uint8_t xdata g_rw_gamvrr2_02E8h_GAM_VRR_LUT_OFST12_2_new_data_744;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86E8h

extern volatile uint8_t xdata g_rw_gamvrr2_02E9h_GAM_VRR_LUT_OFST12_2_new_data_745;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86E9h
extern volatile uint8_t xdata g_rw_gamvrr2_02EAh_GAM_VRR_LUT_OFST12_2_new_data_746;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86EAh
extern volatile uint8_t xdata g_rw_gamvrr2_02EBh_GAM_VRR_LUT_OFST12_2_new_data_747;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86EBh
extern volatile uint8_t xdata g_rw_gamvrr2_02ECh_GAM_VRR_LUT_OFST12_2_new_data_748;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86ECh
extern volatile uint8_t xdata g_rw_gamvrr2_02EDh_GAM_VRR_LUT_OFST12_2_new_data_749;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86EDh

extern volatile uint8_t xdata g_rw_gamvrr2_02EEh_GAM_VRR_LUT_OFST12_2_new_data_750;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86EEh
extern volatile uint8_t xdata g_rw_gamvrr2_02EFh_GAM_VRR_LUT_OFST12_2_new_data_751;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86EFh
extern volatile uint8_t xdata g_rw_gamvrr2_02F0h_GAM_VRR_LUT_OFST12_2_new_data_752;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86F0h
extern volatile uint8_t xdata g_rw_gamvrr2_02F1h_GAM_VRR_LUT_OFST12_2_new_data_753;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86F1h
extern volatile uint8_t xdata g_rw_gamvrr2_02F2h_GAM_VRR_LUT_OFST12_2_new_data_754;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86F2h

extern volatile uint8_t xdata g_rw_gamvrr2_02F3h_GAM_VRR_LUT_OFST12_2_new_data_755;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86F3h
extern volatile uint8_t xdata g_rw_gamvrr2_02F4h_GAM_VRR_LUT_OFST12_2_new_data_756;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86F4h
extern volatile uint8_t xdata g_rw_gamvrr2_02F5h_GAM_VRR_LUT_OFST12_2_new_data_757;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86F5h
extern volatile uint8_t xdata g_rw_gamvrr2_02F6h_GAM_VRR_LUT_OFST12_2_new_data_758;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86F6h
extern volatile uint8_t xdata g_rw_gamvrr2_02F7h_GAM_VRR_LUT_OFST12_2_new_data_759;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86F7h

extern volatile uint8_t xdata g_rw_gamvrr2_02F8h_GAM_VRR_LUT_OFST12_2_new_data_760;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86F8h
extern volatile uint8_t xdata g_rw_gamvrr2_02F9h_GAM_VRR_LUT_OFST12_2_new_data_761;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86F9h
extern volatile uint8_t xdata g_rw_gamvrr2_02FAh_GAM_VRR_LUT_OFST12_2_new_data_762;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86FAh
extern volatile uint8_t xdata g_rw_gamvrr2_02FBh_GAM_VRR_LUT_OFST12_2_new_data_763;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86FBh
extern volatile uint8_t xdata g_rw_gamvrr2_02FCh_GAM_VRR_LUT_OFST12_2_new_data_764;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86FCh

extern volatile uint8_t xdata g_rw_gamvrr2_02FDh_GAM_VRR_LUT_OFST12_2_new_data_765;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86FDh
extern volatile uint8_t xdata g_rw_gamvrr2_02FEh_GAM_VRR_LUT_OFST12_2_new_data_766;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86FEh
extern volatile uint8_t xdata g_rw_gamvrr2_02FFh_GAM_VRR_LUT_OFST12_2_new_data_767;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 86FFh
extern volatile uint8_t xdata g_rw_gamvrr2_0300h_GAM_VRR_LUT_OFST12_2_new_data_768;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8700h
extern volatile uint8_t xdata g_rw_gamvrr2_0301h_GAM_VRR_LUT_OFST12_2_new_data_769;                      // [msb:lsb] = [7:0], val = 32, 	Absolute Address = 8701h

extern volatile uint8_t xdata g_rw_gamvrr2_0302h_GAM_VRR_LUT_OFST12_2_new_data_770;                      // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 8702h
extern volatile uint8_t xdata g_rw_gamvrr2_0303h_GAM_VRR_LUT_OFST12_2_new_data_771;                      // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 8703h
extern volatile uint8_t xdata g_rw_gamvrr2_0304h_GAM_VRR_LUT_OFST12_2_new_data_772;                      // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 8704h
extern volatile uint8_t xdata g_rw_gamvrr2_0305h_GAM_VRR_LUT_OFST12_2_new_data_773;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8705h

#endif