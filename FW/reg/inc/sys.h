#ifndef _SYS_H_
#define _SYS_H_
#include "reg_include.h"

union rw_sys_0000h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved1                                                              : 1;        // val = 0
        unsigned char r_aip_tx_mode_chg                                                      : 1;        // val = 0
        unsigned char reserved2_bits_5_0                                                     : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_sys_0000h xdata g_rw_sys_0000h;    // Absolute Address = 1000h

union rw_sys_0001h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved2_bits_6                                                       : 1;        // val = 0
        unsigned char r_aip_tx_en_wait_dl_finish                                             : 1;        // val = 1
        unsigned char reserved3                                                              : 1;        // val = 0
        unsigned char r_aip_tx_out_dly_en                                                    : 1;        // val = 0
        unsigned char r_aip_tx_reg_sw_en                                                     : 1;        // val = 0
        unsigned char reserved4_bits_2_0                                                     : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_sys_0001h xdata g_rw_sys_0001h;    // Absolute Address = 1001h

union rw_sys_0002h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved4_bits_5_3                                                     : 3;        // [msb:lsb] = [5:3], val = 0
        unsigned char r_aip_proc_tx_pll_rstn_cnt_thd                                         : 1;        // val = 0
        unsigned char r_aip_proc_tx_pll_rstn_f_idx                                           : 1;        // val = 0
        unsigned char r_aip_proc_tx_pll_rstn_r_idx                                           : 1;        // val = 0
        unsigned char r_aip_proc_tx_prechb_f_idx                                             : 1;        // val = 0
        unsigned char r_aip_proc_tx_prechb_r_idx                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0002h xdata g_rw_sys_0002h;    // Absolute Address = 1002h

union rw_sys_0003h
{
    unsigned char byte;
    struct
    {
        unsigned char r_aip_proc_tx_rstn_lane_cnt_thd                                        : 1;        // val = 0
        unsigned char r_aip_proc_tx_rstn_lane_f_idx                                          : 1;        // val = 0
        unsigned char r_aip_proc_tx_rstn_lane_r_idx                                          : 1;        // val = 0
        unsigned char r_aip_proc_tx_out_en_idx                                               : 1;        // val = 0
        unsigned char reserved5                                                              : 1;        // val = 0
        unsigned char r_cfg_end_sel                                                          : 1;        // val = 0
        unsigned char reserved6                                                              : 2;        // val = 0
    }bits;
};
extern volatile union rw_sys_0003h xdata g_rw_sys_0003h;    // Absolute Address = 1003h

union rw_sys_0006h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gpio1_oe                                                             : 1;        // val = 1
        unsigned char r_gpio2_oe                                                             : 1;        // val = 1
        unsigned char r_gpio3_oe                                                             : 1;        // val = 1
        unsigned char r_gpio4_oe                                                             : 1;        // val = 1
        unsigned char r_gpio5_oe                                                             : 1;        // val = 1
        unsigned char r_gpio6_oe                                                             : 1;        // val = 1
        unsigned char r_gpio7_oe                                                             : 1;        // val = 1
        unsigned char r_gpio8_oe                                                             : 1;        // val = 1
    }bits;
};
extern volatile union rw_sys_0006h xdata g_rw_sys_0006h;    // Absolute Address = 1006h

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_0007h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gpio9_oe                                                             : 1;        // val = 1
        unsigned char r_gpio10_oe                                                            : 1;        // val = 1
        unsigned char r_gpio11_oe                                                            : 1;        // val = 1
        unsigned char r_gpio12_oe                                                            : 1;        // val = 1
        unsigned char r_gpio13_oe                                                            : 1;        // val = 1
        unsigned char r_gpio14_oe                                                            : 1;        // val = 1
        unsigned char r_gpio15_oe                                                            : 1;        // val = 1
        unsigned char r_gpio16_oe                                                            : 1;        // val = 1
    }bits;
};
extern volatile union rw_sys_0007h xdata g_rw_sys_0007h;    // Absolute Address = 1007h

union rw_sys_0008h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gpio17_oe                                                            : 1;        // val = 1
        unsigned char r_test_pd                                                              : 1;        // val = 1
        unsigned char r_test_pu                                                              : 1;        // val = 0
        unsigned char r_test_sr                                                              : 1;        // val = 0
        unsigned char r_test_pin1                                                            : 1;        // val = 0
        unsigned char r_test_pin2                                                            : 1;        // val = 0
        unsigned char r_bist_pd                                                              : 1;        // val = 1
        unsigned char r_bist_pu                                                              : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0008h xdata g_rw_sys_0008h;    // Absolute Address = 1008h

union rw_sys_0009h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist_sr                                                              : 1;        // val = 0
        unsigned char r_bist_pin1                                                            : 1;        // val = 0
        unsigned char r_bist_pin2                                                            : 1;        // val = 0
        unsigned char r_scl_pd                                                               : 1;        // val = 1
        unsigned char r_scl_pu                                                               : 1;        // val = 0
        unsigned char r_scl_sr                                                               : 1;        // val = 0
        unsigned char r_scl_pin1                                                             : 1;        // val = 0
        unsigned char r_scl_pin2                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0009h xdata g_rw_sys_0009h;    // Absolute Address = 1009h

union rw_sys_000Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_sda_pd                                                               : 1;        // val = 1
        unsigned char r_sda_pu                                                               : 1;        // val = 0
        unsigned char r_sda_sr                                                               : 1;        // val = 0
        unsigned char r_sda_pin1                                                             : 1;        // val = 0
        unsigned char r_sda_pin2                                                             : 1;        // val = 0
        unsigned char r_gpio1_pd                                                             : 1;        // val = 1
        unsigned char r_gpio1_pu                                                             : 1;        // val = 0
        unsigned char r_gpio1_sr                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_000Ah xdata g_rw_sys_000Ah;    // Absolute Address = 100Ah

union rw_sys_000Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gpio1_pin1                                                           : 1;        // val = 0
        unsigned char r_gpio1_pin2                                                           : 1;        // val = 0
        unsigned char r_gpio2_pd                                                             : 1;        // val = 1
        unsigned char r_gpio2_pu                                                             : 1;        // val = 0
        unsigned char r_gpio2_sr                                                             : 1;        // val = 0
        unsigned char r_gpio2_pin1                                                           : 1;        // val = 0
        unsigned char r_gpio2_pin2                                                           : 1;        // val = 0
        unsigned char r_gpio3_pd                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_000Bh xdata g_rw_sys_000Bh;    // Absolute Address = 100Bh

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_000Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gpio3_pu                                                             : 1;        // val = 0
        unsigned char r_gpio3_sr                                                             : 1;        // val = 0
        unsigned char r_gpio3_pin1                                                           : 1;        // val = 0
        unsigned char r_gpio3_pin2                                                           : 1;        // val = 0
        unsigned char r_gpio4_pd                                                             : 1;        // val = 0
        unsigned char r_gpio4_pu                                                             : 1;        // val = 0
        unsigned char r_gpio4_sr                                                             : 1;        // val = 0
        unsigned char r_gpio4_pin1                                                           : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_000Ch xdata g_rw_sys_000Ch;    // Absolute Address = 100Ch

union rw_sys_000Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gpio4_pin2                                                           : 1;        // val = 0
        unsigned char r_gpio5_pd                                                             : 1;        // val = 1
        unsigned char r_gpio5_pu                                                             : 1;        // val = 0
        unsigned char r_gpio5_sr                                                             : 1;        // val = 0
        unsigned char r_gpio5_pin1                                                           : 1;        // val = 0
        unsigned char r_gpio5_pin2                                                           : 1;        // val = 0
        unsigned char r_gpio6_pd                                                             : 1;        // val = 1
        unsigned char r_gpio6_pu                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_000Dh xdata g_rw_sys_000Dh;    // Absolute Address = 100Dh

union rw_sys_000Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gpio6_sr                                                             : 1;        // val = 0
        unsigned char r_gpio6_pin1                                                           : 1;        // val = 0
        unsigned char r_gpio6_pin2                                                           : 1;        // val = 0
        unsigned char r_gpio7_pd                                                             : 1;        // val = 0
        unsigned char r_gpio7_pu                                                             : 1;        // val = 0
        unsigned char r_gpio7_sr                                                             : 1;        // val = 0
        unsigned char r_gpio7_pin1                                                           : 1;        // val = 0
        unsigned char r_gpio7_pin2                                                           : 1;        // val = 1
    }bits;
};
extern volatile union rw_sys_000Eh xdata g_rw_sys_000Eh;    // Absolute Address = 100Eh

union rw_sys_000Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gpio8_pd                                                             : 1;        // val = 1
        unsigned char r_gpio8_pu                                                             : 1;        // val = 0
        unsigned char r_gpio8_sr                                                             : 1;        // val = 0
        unsigned char r_gpio8_pin1                                                           : 1;        // val = 0
        unsigned char r_gpio8_pin2                                                           : 1;        // val = 0
        unsigned char r_gpio9_pd                                                             : 1;        // val = 1
        unsigned char r_gpio9_pu                                                             : 1;        // val = 0
        unsigned char r_gpio9_sr                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_000Fh xdata g_rw_sys_000Fh;    // Absolute Address = 100Fh

union rw_sys_0010h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gpio9_pin1                                                           : 1;        // val = 0
        unsigned char r_gpio9_pin2                                                           : 1;        // val = 0
        unsigned char r_gpio10_pd                                                            : 1;        // val = 1
        unsigned char r_gpio10_pu                                                            : 1;        // val = 0
        unsigned char r_gpio10_sr                                                            : 1;        // val = 0
        unsigned char r_gpio10_pin1                                                          : 1;        // val = 0
        unsigned char r_gpio10_pin2                                                          : 1;        // val = 0
        unsigned char r_gpio11_pd                                                            : 1;        // val = 1
    }bits;
};
extern volatile union rw_sys_0010h xdata g_rw_sys_0010h;    // Absolute Address = 1010h

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_0011h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gpio11_pu                                                            : 1;        // val = 0
        unsigned char r_gpio11_sr                                                            : 1;        // val = 0
        unsigned char r_gpio11_pin1                                                          : 1;        // val = 0
        unsigned char r_gpio11_pin2                                                          : 1;        // val = 0
        unsigned char r_gpio12_pd                                                            : 1;        // val = 1
        unsigned char r_gpio12_pu                                                            : 1;        // val = 0
        unsigned char r_gpio12_sr                                                            : 1;        // val = 0
        unsigned char r_gpio12_pin1                                                          : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0011h xdata g_rw_sys_0011h;    // Absolute Address = 1011h

union rw_sys_0012h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gpio12_pin2                                                          : 1;        // val = 0
        unsigned char r_gpio13_pd                                                            : 1;        // val = 1
        unsigned char r_gpio13_pu                                                            : 1;        // val = 0
        unsigned char r_gpio13_sr                                                            : 1;        // val = 0
        unsigned char r_gpio13_pin1                                                          : 1;        // val = 0
        unsigned char r_gpio13_pin2                                                          : 1;        // val = 0
        unsigned char r_gpio14_pd                                                            : 1;        // val = 1
        unsigned char r_gpio14_pu                                                            : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0012h xdata g_rw_sys_0012h;    // Absolute Address = 1012h

union rw_sys_0013h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gpio14_sr                                                            : 1;        // val = 0
        unsigned char r_gpio14_pin1                                                          : 1;        // val = 0
        unsigned char r_gpio14_pin2                                                          : 1;        // val = 0
        unsigned char r_gpio15_pd                                                            : 1;        // val = 1
        unsigned char r_gpio15_pu                                                            : 1;        // val = 0
        unsigned char r_gpio15_sr                                                            : 1;        // val = 0
        unsigned char r_gpio15_pin1                                                          : 1;        // val = 0
        unsigned char r_gpio15_pin2                                                          : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0013h xdata g_rw_sys_0013h;    // Absolute Address = 1013h

union rw_sys_0014h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gpio16_pd                                                            : 1;        // val = 1
        unsigned char r_gpio16_pu                                                            : 1;        // val = 0
        unsigned char r_gpio16_sr                                                            : 1;        // val = 0
        unsigned char r_gpio16_pin1                                                          : 1;        // val = 0
        unsigned char r_gpio16_pin2                                                          : 1;        // val = 0
        unsigned char r_gpio17_pd                                                            : 1;        // val = 1
        unsigned char r_gpio17_pu                                                            : 1;        // val = 0
        unsigned char r_gpio17_sr                                                            : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0014h xdata g_rw_sys_0014h;    // Absolute Address = 1014h

union rw_sys_0015h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gpio17_pin1                                                          : 1;        // val = 0
        unsigned char r_gpio17_pin2                                                          : 1;        // val = 0
        unsigned char r_a2d_gpio0_deb_dis                                                    : 1;        // val = 1
        unsigned char r_a2d_gpio1_deb_dis                                                    : 1;        // val = 1
        unsigned char r_a2d_gpio2_deb_dis                                                    : 1;        // val = 1
        unsigned char r_a2d_gpio3_deb_dis                                                    : 1;        // val = 1
        unsigned char r_a2d_gpio4_deb_dis                                                    : 1;        // val = 1
        unsigned char r_a2d_gpio5_deb_dis                                                    : 1;        // val = 1
    }bits;
};
extern volatile union rw_sys_0015h xdata g_rw_sys_0015h;    // Absolute Address = 1015h

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_0016h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_gpio6_deb_dis                                                    : 1;        // val = 1
        unsigned char r_a2d_gpio7_deb_dis                                                    : 1;        // val = 1
        unsigned char r_a2d_gpio8_deb_dis                                                    : 1;        // val = 1
        unsigned char r_a2d_gpio9_deb_dis                                                    : 1;        // val = 1
        unsigned char r_esd_det_dip_sel                                                      : 3;        // [msb:lsb] = [2:0], val = 1
        unsigned char r_esd_det_ack_sel                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0016h xdata g_rw_sys_0016h;    // Absolute Address = 1016h

union rw_sys_0017h
{
    unsigned char byte;
    struct
    {
        unsigned char r_esd_aip_dip_sel                                                      : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_esd_mask_time                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char reserved8                                                              : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_sys_0017h xdata g_rw_sys_0017h;    // Absolute Address = 1017h

union rw_sys_001Eh
{
    unsigned char byte;
    struct
    {
        unsigned char reserved15                                                             : 1;        // val = 0
        unsigned char reserved16                                                             : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_inproc_prede_num                                                     : 2;        // [msb:lsb] = [1:0], val = 1
        unsigned char reserved17                                                             : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char reserved18_bits_0                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_001Eh xdata g_rw_sys_001Eh;    // Absolute Address = 101Eh

union rw_sys_001Fh
{
    unsigned char byte;
    struct
    {
        unsigned char reserved18_bits_1                                                      : 1;        // val = 0
        unsigned char r_sys_ud_sel                                                           : 1;        // val = 0
        unsigned char r_sys_reverse_sel                                                      : 1;        // val = 0
        unsigned char r_pol_sel_en                                                           : 1;        // val = 0
        unsigned char r_lvds_lock1_ref_en                                                    : 1;        // val = 0
        unsigned char r_lvds_lock2_ref_en                                                    : 1;        // val = 0
        unsigned char r_lvds_act1_ref_en                                                     : 1;        // val = 0
        unsigned char r_lvds_act2_ref_en                                                     : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_001Fh xdata g_rw_sys_001Fh;    // Absolute Address = 101Fh

union rw_sys_0020h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_prbs_en                                                           : 1;        // val = 0
        unsigned char r_loop_back_en                                                         : 1;        // val = 0
        unsigned char reserved19                                                             : 1;        // val = 0
        unsigned char reserved20                                                             : 1;        // val = 0
        unsigned char reserved21                                                             : 1;        // val = 0
        unsigned char reserved22                                                             : 1;        // val = 0
        unsigned char reserved23                                                             : 1;        // val = 0
        unsigned char r_mbist_int_clksel                                                     : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0020h xdata g_rw_sys_0020h;    // Absolute Address = 1020h

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_0021h
{
    unsigned char byte;
    struct
    {
        unsigned char r_aip_int_clksel                                                       : 1;        // val = 0
        unsigned char r_efuse_ext_clksel                                                     : 1;        // val = 0
        unsigned char r_a2d_gpio10_deb_dis                                                   : 1;        // val = 1
        unsigned char r_a2d_gpio11_deb_dis                                                   : 1;        // val = 1
        unsigned char r_a2d_gpio12_deb_dis                                                   : 1;        // val = 1
        unsigned char r_a2d_gpio13_deb_dis                                                   : 1;        // val = 1
        unsigned char r_a2d_gpio14_deb_dis                                                   : 1;        // val = 1
        unsigned char r_a2d_gpio15_deb_dis                                                   : 1;        // val = 1
    }bits;
};
extern volatile union rw_sys_0021h xdata g_rw_sys_0021h;    // Absolute Address = 1021h

union rw_sys_0022h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_gpio16_deb_dis                                                   : 1;        // val = 1
        unsigned char r_a2d_gpio17_deb_dis                                                   : 1;        // val = 1
        unsigned char r_a2d_bist_deb_dis                                                     : 1;        // val = 0
        unsigned char r_rxclk_sel                                                            : 1;        // val = 0
        unsigned char r_spll_fix_freq                                                        : 1;        // val = 0
        unsigned char r_mpll_fix_freq                                                        : 1;        // val = 0
        unsigned char r_spll_bypass                                                          : 1;        // val = 0
        unsigned char reserved24                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0022h xdata g_rw_sys_0022h;    // Absolute Address = 1022h

union rw_sys_0023h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved25                                                             : 1;        // val = 0
        unsigned char r_spll_mux_gf_en                                                       : 1;        // val = 1
        unsigned char r_clk_dbg_en                                                           : 1;        // val = 0
        unsigned char r_ipram_mux_gf_en                                                      : 1;        // val = 1
        unsigned char r_opram_mux_gf_en                                                      : 1;        // val = 1
        unsigned char r_sysclk_sel                                                           : 1;        // val = 0
        unsigned char r_gck_mask_by_stchg                                                    : 1;        // val = 0
        unsigned char r_mcu_mask_en                                                          : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0023h xdata g_rw_sys_0023h;    // Absolute Address = 1023h

union rw_sys_0024h
{
    unsigned char byte;
    struct
    {
        unsigned char r_flick_en                                                             : 1;        // val = 0
        unsigned char r_gck_mask_by_ckchg                                                    : 1;        // val = 0
        unsigned char reserved26                                                             : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_tx_clk_bypin_sel                                                     : 1;        // val = 0
        unsigned char r_aging_eni_xor                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0024h xdata g_rw_sys_0024h;    // Absolute Address = 1024h

union rw_sys_0025h
{
    unsigned char byte;
    struct
    {
        unsigned char r_esd_det_vot_1                                                        : 1;        // val = 0
        unsigned char r_esd_det_vot_2                                                        : 1;        // val = 0
        unsigned char r_esd_det_vot_3                                                        : 1;        // val = 0
        unsigned char r_esd_det_vot_4                                                        : 1;        // val = 0
        unsigned char r_esd_det_vot_5                                                        : 1;        // val = 0
        unsigned char r_tx_p2s_clk_sel                                                       : 1;        // val = 0
        unsigned char reserved27_bits_1_0                                                    : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_sys_0025h xdata g_rw_sys_0025h;    // Absolute Address = 1025h

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_0026h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved27_bits_5_2                                                    : 4;        // [msb:lsb] = [5:2], val = 0
        unsigned char r_sscg_mfsel                                                           : 3;        // [msb:lsb] = [2:0], val = 7
        unsigned char reserved28                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0026h xdata g_rw_sys_0026h;    // Absolute Address = 1026h

union rw_sys_0027h
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscg_mrsel                                                           : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_txsdm_noref_lock                                                     : 1;        // val = 0
        unsigned char r_tx_clk_bypin_sel_t                                                   : 1;        // val = 0
        unsigned char reserved29                                                             : 1;        // val = 0
        unsigned char r_pg_clk_en                                                            : 1;        // val = 0
        unsigned char r_spi_clk_en                                                           : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0027h xdata g_rw_sys_0027h;    // Absolute Address = 1027h

union rw_sys_0028h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pol_pol_mode_1                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol_m_mode_1                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol_init_pol_val_1                                                   : 1;        // val = 0
        unsigned char r_sysclk_pllsel                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0028h xdata g_rw_sys_0028h;    // Absolute Address = 1028h

union rw_sys_0029h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pol_pol_mode_2                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol_m_mode_2                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol_init_pol_val_2                                                   : 1;        // val = 0
        unsigned char r_sdr_rdclk_inv                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0029h xdata g_rw_sys_0029h;    // Absolute Address = 1029h

union rw_sys_002Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_pol_pol_mode_3                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol_m_mode_3                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol_init_pol_val_3                                                   : 1;        // val = 0
        unsigned char r_mpll_src_syspll                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_002Ah xdata g_rw_sys_002Ah;    // Absolute Address = 102Ah

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_002Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_pol_pol_mode_4                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol_m_mode_4                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol_init_pol_val_4                                                   : 1;        // val = 0
        unsigned char r_pad_sysfinish                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_002Bh xdata g_rw_sys_002Bh;    // Absolute Address = 102Bh

union rw_sys_002Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_pol_pol_mode_5                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol_m_mode_5                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol_init_pol_val_5                                                   : 1;        // val = 0
        unsigned char reserved30                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_002Ch xdata g_rw_sys_002Ch;    // Absolute Address = 102Ch

union rw_sys_002Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_pol_pol_mode_6                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol_m_mode_6                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol_init_pol_val_6                                                   : 1;        // val = 0
        unsigned char reserved31                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_002Dh xdata g_rw_sys_002Dh;    // Absolute Address = 102Dh

union rw_sys_002Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_pol_pol_mode_7                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol_m_mode_7                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol_init_pol_val_7                                                   : 1;        // val = 0
        unsigned char reserved32                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_002Eh xdata g_rw_sys_002Eh;    // Absolute Address = 102Eh

union rw_sys_002Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_pol_pol_mode_8                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol_m_mode_8                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol_init_pol_val_8                                                   : 1;        // val = 0
        unsigned char reserved33                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_002Fh xdata g_rw_sys_002Fh;    // Absolute Address = 102Fh

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_0030h
{
    unsigned char byte;
    struct
    {
        unsigned char r_aip_proc_tx_rstn_bist_r_idx                                          : 1;        // val = 0
        unsigned char r_aip_proc_tx_rstn_bist_f_idx                                          : 1;        // val = 0
        unsigned char r_aip_proc_tx_rstn_bist_cnt_thd                                        : 1;        // val = 0
        unsigned char reserved34                                                             : 1;        // val = 0
        unsigned char r_wosscg_syspll                                                        : 1;        // val = 0
        unsigned char r_wosscg_sdrclk                                                        : 1;        // val = 0
        unsigned char r_syspll_r1_div2                                                       : 1;        // val = 0
        unsigned char r_syspll_r2_div2                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0030h xdata g_rw_sys_0030h;    // Absolute Address = 1030h

union rw_sys_0031h
{
    unsigned char byte;
    struct
    {
        unsigned char r_esd_deb_target_sel                                                   : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_esd_det_force_bist                                                   : 1;        // val = 0
        unsigned char reserved35                                                             : 1;        // val = 0
        unsigned char r_ocds_en                                                              : 1;        // val = 0
        unsigned char r_rxclk0_lthsel                                                        : 1;        // val = 0
        unsigned char r_rxclk1_lthsel                                                        : 1;        // val = 0
        unsigned char reserved36_bits_0                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0031h xdata g_rw_sys_0031h;    // Absolute Address = 1031h

union rw_sys_0032h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved36_bits_3_1                                                    : 3;        // [msb:lsb] = [3:1], val = 0
        unsigned char r_ctrlclk_sel                                                          : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char reserved37_bits_2_0                                                    : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_sys_0032h xdata g_rw_sys_0032h;    // Absolute Address = 1032h

union rw_sys_0033h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved37_bits_3                                                      : 1;        // val = 0
        unsigned char r_pns_detect                                                           : 1;        // val = 0
        unsigned char r_esd_maskgck_en                                                       : 1;        // val = 1
        unsigned char r_paddet_byp                                                           : 1;        // val = 0
        unsigned char reserved38                                                             : 1;        // val = 0
        unsigned char r_lxlod_byp                                                            : 1;        // val = 0
        unsigned char r_gam_byp                                                              : 1;        // val = 0
        unsigned char r_frc_byp                                                              : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0033h xdata g_rw_sys_0033h;    // Absolute Address = 1033h

union rw_sys_0038h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel0                                                         : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char r_a2d_mux_sel1_bits_2_0                                                : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_sys_0038h xdata g_rw_sys_0038h;    // Absolute Address = 1038h

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_0039h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel1_bits_4_3                                                : 2;        // [msb:lsb] = [4:3], val = 3
        unsigned char r_a2d_mux_sel2                                                         : 5;        // [msb:lsb] = [4:0], val = 31
        unsigned char r_a2d_mux_sel3_bits_0                                                  : 1;        // val = 1
    }bits;
};
extern volatile union rw_sys_0039h xdata g_rw_sys_0039h;    // Absolute Address = 1039h

union rw_sys_003Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel3_bits_4_1                                                : 4;        // [msb:lsb] = [4:1], val = 15
        unsigned char r_a2d_mux_sel4_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_sys_003Ah xdata g_rw_sys_003Ah;    // Absolute Address = 103Ah

union rw_sys_003Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel4_bits_4                                                  : 1;        // val = 1
        unsigned char r_a2d_mux_sel5                                                         : 5;        // [msb:lsb] = [4:0], val = 18
        unsigned char r_a2d_mux_sel6_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_sys_003Bh xdata g_rw_sys_003Bh;    // Absolute Address = 103Bh

union rw_sys_003Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel6_bits_4_2                                                : 3;        // [msb:lsb] = [4:2], val = 4
        unsigned char r_a2d_mux_sel7                                                         : 5;        // [msb:lsb] = [4:0], val = 31
    }bits;
};
extern volatile union rw_sys_003Ch xdata g_rw_sys_003Ch;    // Absolute Address = 103Ch

union rw_sys_003Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel8                                                         : 5;        // [msb:lsb] = [4:0], val = 31
        unsigned char r_a2d_mux_sel9_bits_2_0                                                : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_sys_003Dh xdata g_rw_sys_003Dh;    // Absolute Address = 103Dh

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_003Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel9_bits_4_3                                                : 2;        // [msb:lsb] = [4:3], val = 3
        unsigned char r_a2d_mux_sel10                                                        : 5;        // [msb:lsb] = [4:0], val = 31
        unsigned char r_a2d_mux_sel11_bits_0                                                 : 1;        // val = 1
    }bits;
};
extern volatile union rw_sys_003Eh xdata g_rw_sys_003Eh;    // Absolute Address = 103Eh

union rw_sys_003Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel11_bits_4_1                                               : 4;        // [msb:lsb] = [4:1], val = 15
        unsigned char r_a2d_mux_sel12_bits_3_0                                               : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_sys_003Fh xdata g_rw_sys_003Fh;    // Absolute Address = 103Fh

union rw_sys_0040h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel12_bits_4                                                 : 1;        // val = 1
        unsigned char r_a2d_mux_sel13                                                        : 5;        // [msb:lsb] = [4:0], val = 31
        unsigned char r_a2d_mux_sel14_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_sys_0040h xdata g_rw_sys_0040h;    // Absolute Address = 1040h

union rw_sys_0041h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel14_bits_4_2                                               : 3;        // [msb:lsb] = [4:2], val = 7
        unsigned char r_a2d_mux_sel15                                                        : 5;        // [msb:lsb] = [4:0], val = 31
    }bits;
};
extern volatile union rw_sys_0041h xdata g_rw_sys_0041h;    // Absolute Address = 1041h

union rw_sys_0042h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel16                                                        : 5;        // [msb:lsb] = [4:0], val = 31
        unsigned char r_a2d_mux_sel17_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_sys_0042h xdata g_rw_sys_0042h;    // Absolute Address = 1042h

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_0043h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel17_bits_4_3                                               : 2;        // [msb:lsb] = [4:3], val = 3
        unsigned char r_a2d_mux_sel18                                                        : 5;        // [msb:lsb] = [4:0], val = 31
        unsigned char r_a2d_mux_sel19_bits_0                                                 : 1;        // val = 1
    }bits;
};
extern volatile union rw_sys_0043h xdata g_rw_sys_0043h;    // Absolute Address = 1043h

union rw_sys_0044h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel19_bits_4_1                                               : 4;        // [msb:lsb] = [4:1], val = 15
        unsigned char r_a2d_mux_sel20_bits_3_0                                               : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_sys_0044h xdata g_rw_sys_0044h;    // Absolute Address = 1044h

union rw_sys_0045h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel20_bits_4                                                 : 1;        // val = 1
        unsigned char r_a2d_mux_sel21                                                        : 5;        // [msb:lsb] = [4:0], val = 31
        unsigned char r_a2d_mux_sel22_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_sys_0045h xdata g_rw_sys_0045h;    // Absolute Address = 1045h

union rw_sys_0046h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel22_bits_4_2                                               : 3;        // [msb:lsb] = [4:2], val = 7
        unsigned char r_a2d_mux_sel23                                                        : 5;        // [msb:lsb] = [4:0], val = 31
    }bits;
};
extern volatile union rw_sys_0046h xdata g_rw_sys_0046h;    // Absolute Address = 1046h

union rw_sys_0047h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel24                                                        : 5;        // [msb:lsb] = [4:0], val = 31
        unsigned char r_a2d_mux_sel25_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_sys_0047h xdata g_rw_sys_0047h;    // Absolute Address = 1047h

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_0048h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel25_bits_4_3                                               : 2;        // [msb:lsb] = [4:3], val = 3
        unsigned char r_a2d_mux_sel26                                                        : 5;        // [msb:lsb] = [4:0], val = 31
        unsigned char r_a2d_mux_sel27_bits_0                                                 : 1;        // val = 1
    }bits;
};
extern volatile union rw_sys_0048h xdata g_rw_sys_0048h;    // Absolute Address = 1048h

union rw_sys_0049h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel27_bits_4_1                                               : 4;        // [msb:lsb] = [4:1], val = 15
        unsigned char r_a2d_mux_sel28_bits_3_0                                               : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_sys_0049h xdata g_rw_sys_0049h;    // Absolute Address = 1049h

union rw_sys_004Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel28_bits_4                                                 : 1;        // val = 1
        unsigned char r_a2d_mux_sel29                                                        : 5;        // [msb:lsb] = [4:0], val = 31
        unsigned char r_a2d_mux_sel30_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_sys_004Ah xdata g_rw_sys_004Ah;    // Absolute Address = 104Ah

union rw_sys_004Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel30_bits_4_2                                               : 3;        // [msb:lsb] = [4:2], val = 7
        unsigned char r_a2d_mux_sel31                                                        : 5;        // [msb:lsb] = [4:0], val = 31
    }bits;
};
extern volatile union rw_sys_004Bh xdata g_rw_sys_004Bh;    // Absolute Address = 104Bh

union rw_sys_004Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel32                                                        : 5;        // [msb:lsb] = [4:0], val = 31
        unsigned char r_a2d_mux_sel33_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_sys_004Ch xdata g_rw_sys_004Ch;    // Absolute Address = 104Ch

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_004Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel33_bits_4_3                                               : 2;        // [msb:lsb] = [4:3], val = 3
        unsigned char r_a2d_mux_sel34                                                        : 5;        // [msb:lsb] = [4:0], val = 31
        unsigned char r_a2d_mux_sel35_bits_0                                                 : 1;        // val = 1
    }bits;
};
extern volatile union rw_sys_004Dh xdata g_rw_sys_004Dh;    // Absolute Address = 104Dh

union rw_sys_004Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel35_bits_4_1                                               : 4;        // [msb:lsb] = [4:1], val = 15
        unsigned char r_a2d_mux_sel36_bits_3_0                                               : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_sys_004Eh xdata g_rw_sys_004Eh;    // Absolute Address = 104Eh

union rw_sys_004Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel36_bits_4                                                 : 1;        // val = 1
        unsigned char r_a2d_mux_sel37                                                        : 5;        // [msb:lsb] = [4:0], val = 31
        unsigned char r_a2d_mux_sel38_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_sys_004Fh xdata g_rw_sys_004Fh;    // Absolute Address = 104Fh

union rw_sys_0050h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel38_bits_4_2                                               : 3;        // [msb:lsb] = [4:2], val = 7
        unsigned char r_a2d_mux_sel39                                                        : 5;        // [msb:lsb] = [4:0], val = 31
    }bits;
};
extern volatile union rw_sys_0050h xdata g_rw_sys_0050h;    // Absolute Address = 1050h

union rw_sys_0051h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel40                                                        : 5;        // [msb:lsb] = [4:0], val = 31
        unsigned char r_a2d_mux_sel41_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 5
    }bits;
};
extern volatile union rw_sys_0051h xdata g_rw_sys_0051h;    // Absolute Address = 1051h

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_0052h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel41_bits_4_3                                               : 2;        // [msb:lsb] = [4:3], val = 0
        unsigned char r_a2d_mux_sel42                                                        : 5;        // [msb:lsb] = [4:0], val = 6
        unsigned char r_a2d_mux_sel43_bits_0                                                 : 1;        // val = 1
    }bits;
};
extern volatile union rw_sys_0052h xdata g_rw_sys_0052h;    // Absolute Address = 1052h

union rw_sys_0053h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel43_bits_4_1                                               : 4;        // [msb:lsb] = [4:1], val = 3
        unsigned char r_a2d_mux_sel44_bits_3_0                                               : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_sys_0053h xdata g_rw_sys_0053h;    // Absolute Address = 1053h

union rw_sys_0054h
{
    unsigned char byte;
    struct
    {
        unsigned char r_a2d_mux_sel44_bits_4                                                 : 1;        // val = 0
        unsigned char reserved39                                                             : 5;        // [msb:lsb] = [4:0], val = 31
        unsigned char reserved40_bits_1_0                                                    : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_sys_0054h xdata g_rw_sys_0054h;    // Absolute Address = 1054h

union rw_sys_0055h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved40_bits_4_2                                                    : 3;        // [msb:lsb] = [4:2], val = 7
        unsigned char reserved41                                                             : 5;        // [msb:lsb] = [4:0], val = 31
    }bits;
};
extern volatile union rw_sys_0055h xdata g_rw_sys_0055h;    // Absolute Address = 1055h

union rw_sys_0056h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved42                                                             : 5;        // [msb:lsb] = [4:0], val = 31
        unsigned char reserved43_bits_2_0                                                    : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_sys_0056h xdata g_rw_sys_0056h;    // Absolute Address = 1056h

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_0057h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved43_bits_4_3                                                    : 2;        // [msb:lsb] = [4:3], val = 3
        unsigned char reserved44                                                             : 5;        // [msb:lsb] = [4:0], val = 31
        unsigned char reserved45_bits_0                                                      : 1;        // val = 1
    }bits;
};
extern volatile union rw_sys_0057h xdata g_rw_sys_0057h;    // Absolute Address = 1057h

union rw_sys_0058h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved45_bits_4_1                                                    : 4;        // [msb:lsb] = [4:1], val = 15
        unsigned char r_oen_mux_sel0                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_oen_mux_sel1                                                         : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_sys_0058h xdata g_rw_sys_0058h;    // Absolute Address = 1058h

union rw_sys_0059h
{
    unsigned char byte;
    struct
    {
        unsigned char r_oen_mux_sel2                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_oen_mux_sel3                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_oen_mux_sel4                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_oen_mux_sel5                                                         : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_sys_0059h xdata g_rw_sys_0059h;    // Absolute Address = 1059h

union rw_sys_005Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_oen_mux_sel6                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_oen_mux_sel7                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_oen_mux_sel8                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_oen_mux_sel9                                                         : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_sys_005Ah xdata g_rw_sys_005Ah;    // Absolute Address = 105Ah

union rw_sys_005Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_oen_mux_sel10                                                        : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_oen_mux_sel11                                                        : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_oen_mux_sel12                                                        : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_oen_mux_sel13                                                        : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_sys_005Bh xdata g_rw_sys_005Bh;    // Absolute Address = 105Bh

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_005Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_oen_mux_sel14                                                        : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_oen_mux_sel15                                                        : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_oen_mux_sel16                                                        : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_oen_mux_sel17                                                        : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_sys_005Ch xdata g_rw_sys_005Ch;    // Absolute Address = 105Ch

union rw_sys_005Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_oen_mux_sel18                                                        : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_oen_mux_sel19                                                        : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char reserved46                                                             : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char reserved47                                                             : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_sys_005Dh xdata g_rw_sys_005Dh;    // Absolute Address = 105Dh

union rw_sys_005Eh
{
    unsigned char byte;
    struct
    {
        unsigned char reserved48                                                             : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char reserved49                                                             : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_d2a_mux_sel0_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_sys_005Eh xdata g_rw_sys_005Eh;    // Absolute Address = 105Eh

union rw_sys_005Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_d2a_mux_sel0_bits_5_4                                                : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_d2a_mux_sel1                                                         : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_sys_005Fh xdata g_rw_sys_005Fh;    // Absolute Address = 105Fh

union rw_sys_0060h
{
    unsigned char byte;
    struct
    {
        unsigned char r_d2a_mux_sel2                                                         : 6;        // [msb:lsb] = [5:0], val = 1
        unsigned char r_d2a_mux_sel3_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_sys_0060h xdata g_rw_sys_0060h;    // Absolute Address = 1060h

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_0061h
{
    unsigned char byte;
    struct
    {
        unsigned char r_d2a_mux_sel3_bits_5_2                                                : 4;        // [msb:lsb] = [5:2], val = 0
        unsigned char r_d2a_mux_sel4_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 3
    }bits;
};
extern volatile union rw_sys_0061h xdata g_rw_sys_0061h;    // Absolute Address = 1061h

union rw_sys_0062h
{
    unsigned char byte;
    struct
    {
        unsigned char r_d2a_mux_sel4_bits_5_4                                                : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_d2a_mux_sel5                                                         : 6;        // [msb:lsb] = [5:0], val = 4
    }bits;
};
extern volatile union rw_sys_0062h xdata g_rw_sys_0062h;    // Absolute Address = 1062h

union rw_sys_0063h
{
    unsigned char byte;
    struct
    {
        unsigned char r_d2a_mux_sel6                                                         : 6;        // [msb:lsb] = [5:0], val = 5
        unsigned char r_d2a_mux_sel7_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_sys_0063h xdata g_rw_sys_0063h;    // Absolute Address = 1063h

union rw_sys_0064h
{
    unsigned char byte;
    struct
    {
        unsigned char r_d2a_mux_sel7_bits_5_2                                                : 4;        // [msb:lsb] = [5:2], val = 1
        unsigned char r_d2a_mux_sel8_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 7
    }bits;
};
extern volatile union rw_sys_0064h xdata g_rw_sys_0064h;    // Absolute Address = 1064h

union rw_sys_0065h
{
    unsigned char byte;
    struct
    {
        unsigned char r_d2a_mux_sel8_bits_5_4                                                : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_d2a_mux_sel9                                                         : 6;        // [msb:lsb] = [5:0], val = 8
    }bits;
};
extern volatile union rw_sys_0065h xdata g_rw_sys_0065h;    // Absolute Address = 1065h

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_0066h
{
    unsigned char byte;
    struct
    {
        unsigned char r_d2a_mux_sel10                                                        : 6;        // [msb:lsb] = [5:0], val = 41
        unsigned char r_d2a_mux_sel11_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_sys_0066h xdata g_rw_sys_0066h;    // Absolute Address = 1066h

union rw_sys_0067h
{
    unsigned char byte;
    struct
    {
        unsigned char r_d2a_mux_sel11_bits_5_2                                               : 4;        // [msb:lsb] = [5:2], val = 2
        unsigned char r_d2a_mux_sel12_bits_3_0                                               : 4;        // [msb:lsb] = [3:0], val = 11
    }bits;
};
extern volatile union rw_sys_0067h xdata g_rw_sys_0067h;    // Absolute Address = 1067h

union rw_sys_0068h
{
    unsigned char byte;
    struct
    {
        unsigned char r_d2a_mux_sel12_bits_5_4                                               : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_d2a_mux_sel13                                                        : 6;        // [msb:lsb] = [5:0], val = 12
    }bits;
};
extern volatile union rw_sys_0068h xdata g_rw_sys_0068h;    // Absolute Address = 1068h

union rw_sys_0069h
{
    unsigned char byte;
    struct
    {
        unsigned char r_d2a_mux_sel14                                                        : 6;        // [msb:lsb] = [5:0], val = 13
        unsigned char r_d2a_mux_sel15_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_sys_0069h xdata g_rw_sys_0069h;    // Absolute Address = 1069h

union rw_sys_006Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_d2a_mux_sel15_bits_5_2                                               : 4;        // [msb:lsb] = [5:2], val = 3
        unsigned char r_d2a_mux_sel16_bits_3_0                                               : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_sys_006Ah xdata g_rw_sys_006Ah;    // Absolute Address = 106Ah

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_006Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_d2a_mux_sel16_bits_5_4                                               : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_d2a_mux_sel17                                                        : 6;        // [msb:lsb] = [5:0], val = 40
    }bits;
};
extern volatile union rw_sys_006Bh xdata g_rw_sys_006Bh;    // Absolute Address = 106Bh

union rw_sys_006Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_d2a_mux_sel18                                                        : 6;        // [msb:lsb] = [5:0], val = 18
        unsigned char r_d2a_mux_sel19_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_sys_006Ch xdata g_rw_sys_006Ch;    // Absolute Address = 106Ch

union rw_sys_006Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_d2a_mux_sel19_bits_5_2                                               : 4;        // [msb:lsb] = [5:2], val = 4
        unsigned char reserved50_bits_3_0                                                    : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_sys_006Dh xdata g_rw_sys_006Dh;    // Absolute Address = 106Dh

union rw_sys_006Eh
{
    unsigned char byte;
    struct
    {
        unsigned char reserved50_bits_5_4                                                    : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char reserved51                                                             : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_sys_006Eh xdata g_rw_sys_006Eh;    // Absolute Address = 106Eh

union rw_sys_006Fh
{
    unsigned char byte;
    struct
    {
        unsigned char reserved52                                                             : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char reserved53_bits_1_0                                                    : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_sys_006Fh xdata g_rw_sys_006Fh;    // Absolute Address = 106Fh

//------------------------------------------------------------------------------------------------------------------------
union rw_sys_0070h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved53_bits_5_2                                                    : 4;        // [msb:lsb] = [5:2], val = 0
        unsigned char reserved54                                                             : 1;        // val = 0
        unsigned char r_esd_state_mask                                                       : 1;        // val = 0
        unsigned char r_esd_line_mask                                                        : 1;        // val = 0
        unsigned char r_esd_chg_flag_en                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_sys_0070h xdata g_rw_sys_0070h;    // Absolute Address = 1070h

union rw_sys_0071h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_clk_bypin_a_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_tx_clk_bypin_b_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_sys_0071h xdata g_rw_sys_0071h;    // Absolute Address = 1071h

union rw_sys_0074h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rx_chksum_en                                                         : 1;        // val = 0
        unsigned char r_rx_chksum_clr                                                        : 1;        // val = 0
        unsigned char reserved55                                                             : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_sys_0074h xdata g_rw_sys_0074h;    // Absolute Address = 1074h


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_sys_0004h_reserved6;                                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 1004h
extern volatile uint8_t xdata g_rw_sys_0005h_reserved7;                                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 1005h
extern volatile uint8_t xdata g_rw_sys_0018h_reserved9;                                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 1018h
extern volatile uint8_t xdata g_rw_sys_0019h_reserved10;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 1019h
extern volatile uint8_t xdata g_rw_sys_001Ah_reserved11;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 101Ah

extern volatile uint8_t xdata g_rw_sys_001Bh_reserved12;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 101Bh
extern volatile uint8_t xdata g_rw_sys_001Ch_reserved13;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 101Ch
extern volatile uint8_t xdata g_rw_sys_001Dh_reserved14;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 101Dh
extern volatile uint8_t xdata g_rw_sys_0034h_xmem_clk_en;                                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 1034h
extern volatile uint8_t xdata g_rw_sys_0035h_xmem_clk_en;                                                // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 1035h

extern volatile uint8_t xdata g_rw_sys_0036h_xmem_clk_en;                                                // [msb:lsb] = [23:16], val = 0, 	Absolute Address = 1036h
extern volatile uint8_t xdata g_rw_sys_0037h_xmem_clk_en;                                                // [msb:lsb] = [31:24], val = 0, 	Absolute Address = 1037h
extern volatile uint8_t xdata g_rw_sys_0072h_rx_chksum;                                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 1072h
extern volatile uint8_t xdata g_rw_sys_0073h_rx_chksum;                                                  // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 1073h
extern volatile uint8_t xdata g_rw_sys_0075h_reserved56;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 1075h

extern volatile uint8_t xdata g_rw_sys_0076h_reserved57;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 1076h
extern volatile uint8_t xdata g_rw_sys_0077h_reserved58;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 1077h
extern volatile uint8_t xdata g_rw_sys_0078h_reserved59;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 1078h

#endif