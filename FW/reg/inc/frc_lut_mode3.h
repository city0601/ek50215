#ifndef _FRC_LUT_MODE3_H_
#define _FRC_LUT_MODE3_H_
#include "reg_include.h"


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0000h_FRC_LUT_1_data_0;                                 // [msb:lsb] = [7:0], val = 127, 	Absolute Address = 9901h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0001h_FRC_LUT_1_data_1;                                 // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9902h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0002h_FRC_LUT_1_data_2;                                 // [msb:lsb] = [7:0], val = 224, 	Absolute Address = 9903h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0003h_FRC_LUT_1_data_3;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9904h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0004h_FRC_LUT_1_data_4;                                 // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9905h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0005h_FRC_LUT_1_data_5;                                 // [msb:lsb] = [7:0], val = 126, 	Absolute Address = 9906h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0006h_FRC_LUT_1_data_6;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9907h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0007h_FRC_LUT_1_data_7;                                 // [msb:lsb] = [7:0], val = 224, 	Absolute Address = 9908h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0008h_FRC_LUT_1_data_8;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9909h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0009h_FRC_LUT_1_data_9;                                 // [msb:lsb] = [7:0], val = 224, 	Absolute Address = 990Ah

extern volatile uint8_t xdata g_rw_frc_lut_mode3_000Ah_FRC_LUT_1_data_10;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 990Bh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_000Bh_FRC_LUT_1_data_11;                                // [msb:lsb] = [7:0], val = 126, 	Absolute Address = 990Ch
extern volatile uint8_t xdata g_rw_frc_lut_mode3_000Ch_FRC_LUT_1_data_12;                                // [msb:lsb] = [7:0], val = 224, 	Absolute Address = 990Dh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_000Dh_FRC_LUT_1_data_13;                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 990Eh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_000Eh_FRC_LUT_1_data_14;                                // [msb:lsb] = [7:0], val = 126, 	Absolute Address = 990Fh

extern volatile uint8_t xdata g_rw_frc_lut_mode3_000Fh_FRC_LUT_1_data_15;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9910h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0010h_FRC_LUT_1_data_16;                                // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 9911h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0011h_FRC_LUT_1_data_17;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9912h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0012h_FRC_LUT_1_data_18;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9913h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0013h_FRC_LUT_1_data_19;                                // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 9914h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0014h_FRC_LUT_1_data_20;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9915h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0015h_FRC_LUT_1_data_21;                                // [msb:lsb] = [7:0], val = 105, 	Absolute Address = 9916h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0016h_FRC_LUT_1_data_22;                                // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 9917h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0017h_FRC_LUT_1_data_23;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9918h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0018h_FRC_LUT_1_data_24;                                // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 9919h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0019h_FRC_LUT_1_data_25;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 991Ah
extern volatile uint8_t xdata g_rw_frc_lut_mode3_001Ah_FRC_LUT_1_data_26;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 991Bh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_001Bh_FRC_LUT_1_data_27;                                // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 991Ch
extern volatile uint8_t xdata g_rw_frc_lut_mode3_001Ch_FRC_LUT_1_data_28;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 991Dh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_001Dh_FRC_LUT_1_data_29;                                // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 991Eh

extern volatile uint8_t xdata g_rw_frc_lut_mode3_001Eh_FRC_LUT_1_data_30;                                // [msb:lsb] = [7:0], val = 105, 	Absolute Address = 991Fh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_001Fh_FRC_LUT_1_data_31;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9920h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0020h_FRC_LUT_1_data_32;                                // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 9921h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0021h_FRC_LUT_1_data_33;                                // [msb:lsb] = [7:0], val = 132, 	Absolute Address = 9922h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0022h_FRC_LUT_1_data_34;                                // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 9923h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0023h_FRC_LUT_1_data_35;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9924h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0024h_FRC_LUT_1_data_36;                                // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 9925h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0025h_FRC_LUT_1_data_37;                                // [msb:lsb] = [7:0], val = 97, 	Absolute Address = 9926h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0026h_FRC_LUT_1_data_38;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9927h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0027h_FRC_LUT_1_data_39;                                // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 9928h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0028h_FRC_LUT_1_data_40;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9929h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0029h_FRC_LUT_1_data_41;                                // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 992Ah
extern volatile uint8_t xdata g_rw_frc_lut_mode3_002Ah_FRC_LUT_1_data_42;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 992Bh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_002Bh_FRC_LUT_1_data_43;                                // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 992Ch
extern volatile uint8_t xdata g_rw_frc_lut_mode3_002Ch_FRC_LUT_1_data_44;                                // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 992Dh

extern volatile uint8_t xdata g_rw_frc_lut_mode3_002Dh_FRC_LUT_1_data_45;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 992Eh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_002Eh_FRC_LUT_1_data_46;                                // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 992Fh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_002Fh_FRC_LUT_1_data_47;                                // [msb:lsb] = [7:0], val = 132, 	Absolute Address = 9930h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0030h_FRC_LUT_1_data_48;                                // [msb:lsb] = [7:0], val = 1, 	Absolute Address = 9931h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0031h_FRC_LUT_1_data_49;                                // [msb:lsb] = [7:0], val = 150, 	Absolute Address = 9932h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0032h_FRC_LUT_1_data_50;                                // [msb:lsb] = [7:0], val = 124, 	Absolute Address = 9933h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0033h_FRC_LUT_1_data_51;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9934h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0034h_FRC_LUT_1_data_52;                                // [msb:lsb] = [7:0], val = 150, 	Absolute Address = 9935h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0035h_FRC_LUT_1_data_53;                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9936h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0036h_FRC_LUT_1_data_54;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9937h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0037h_FRC_LUT_1_data_55;                                // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 9938h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0038h_FRC_LUT_1_data_56;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9939h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0039h_FRC_LUT_1_data_57;                                // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 993Ah
extern volatile uint8_t xdata g_rw_frc_lut_mode3_003Ah_FRC_LUT_1_data_58;                                // [msb:lsb] = [7:0], val = 150, 	Absolute Address = 993Bh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_003Bh_FRC_LUT_1_data_59;                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 993Ch

extern volatile uint8_t xdata g_rw_frc_lut_mode3_003Ch_FRC_LUT_1_data_60;                                // [msb:lsb] = [7:0], val = 108, 	Absolute Address = 993Dh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_003Dh_FRC_LUT_1_data_61;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 993Eh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_003Eh_FRC_LUT_1_data_62;                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 993Fh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_003Fh_FRC_LUT_1_data_63;                                // [msb:lsb] = [7:0], val = 150, 	Absolute Address = 9940h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0040h_FRC_LUT_1_data_64;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9941h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0041h_FRC_LUT_1_data_65;                                // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 9942h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0042h_FRC_LUT_1_data_66;                                // [msb:lsb] = [7:0], val = 22, 	Absolute Address = 9943h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0043h_FRC_LUT_1_data_67;                                // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 9944h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0044h_FRC_LUT_1_data_68;                                // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 9945h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0045h_FRC_LUT_1_data_69;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9946h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0046h_FRC_LUT_1_data_70;                                // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 9947h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0047h_FRC_LUT_1_data_71;                                // [msb:lsb] = [7:0], val = 86, 	Absolute Address = 9948h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0048h_FRC_LUT_1_data_72;                                // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 9949h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0049h_FRC_LUT_1_data_73;                                // [msb:lsb] = [7:0], val = 86, 	Absolute Address = 994Ah
extern volatile uint8_t xdata g_rw_frc_lut_mode3_004Ah_FRC_LUT_1_data_74;                                // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 994Bh

extern volatile uint8_t xdata g_rw_frc_lut_mode3_004Bh_FRC_LUT_1_data_75;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 994Ch
extern volatile uint8_t xdata g_rw_frc_lut_mode3_004Ch_FRC_LUT_1_data_76;                                // [msb:lsb] = [7:0], val = 6, 	Absolute Address = 994Dh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_004Dh_FRC_LUT_1_data_77;                                // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 994Eh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_004Eh_FRC_LUT_1_data_78;                                // [msb:lsb] = [7:0], val = 129, 	Absolute Address = 994Fh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_004Fh_FRC_LUT_1_data_79;                                // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 9950h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0050h_FRC_LUT_1_data_80;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9951h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0051h_FRC_LUT_1_data_81;                                // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 9952h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0052h_FRC_LUT_1_data_82;                                // [msb:lsb] = [7:0], val = 80, 	Absolute Address = 9953h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0053h_FRC_LUT_1_data_83;                                // [msb:lsb] = [7:0], val = 232, 	Absolute Address = 9954h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0054h_FRC_LUT_1_data_84;                                // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 9955h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0055h_FRC_LUT_1_data_85;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9956h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0056h_FRC_LUT_1_data_86;                                // [msb:lsb] = [7:0], val = 232, 	Absolute Address = 9957h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0057h_FRC_LUT_1_data_87;                                // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 9958h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0058h_FRC_LUT_1_data_88;                                // [msb:lsb] = [7:0], val = 232, 	Absolute Address = 9959h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0059h_FRC_LUT_1_data_89;                                // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 995Ah

extern volatile uint8_t xdata g_rw_frc_lut_mode3_005Ah_FRC_LUT_1_data_90;                                // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 995Bh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_005Bh_FRC_LUT_1_data_91;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 995Ch
extern volatile uint8_t xdata g_rw_frc_lut_mode3_005Ch_FRC_LUT_1_data_92;                                // [msb:lsb] = [7:0], val = 80, 	Absolute Address = 995Dh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_005Dh_FRC_LUT_1_data_93;                                // [msb:lsb] = [7:0], val = 232, 	Absolute Address = 995Eh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_005Eh_FRC_LUT_1_data_94;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 995Fh

extern volatile uint8_t xdata g_rw_frc_lut_mode3_005Fh_FRC_LUT_1_data_95;                                // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 9960h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0060h_FRC_LUT_1_data_96;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9961h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0061h_FRC_LUT_1_data_97;                                // [msb:lsb] = [7:0], val = 72, 	Absolute Address = 9962h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0062h_FRC_LUT_1_data_98;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9963h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0063h_FRC_LUT_1_data_99;                                // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 9964h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0064h_FRC_LUT_1_data_100;                               // [msb:lsb] = [7:0], val = 76, 	Absolute Address = 9965h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0065h_FRC_LUT_1_data_101;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9966h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0066h_FRC_LUT_1_data_102;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 9967h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0067h_FRC_LUT_1_data_103;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9968h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0068h_FRC_LUT_1_data_104;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 9969h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0069h_FRC_LUT_1_data_105;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 996Ah
extern volatile uint8_t xdata g_rw_frc_lut_mode3_006Ah_FRC_LUT_1_data_106;                               // [msb:lsb] = [7:0], val = 76, 	Absolute Address = 996Bh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_006Bh_FRC_LUT_1_data_107;                               // [msb:lsb] = [7:0], val = 129, 	Absolute Address = 996Ch
extern volatile uint8_t xdata g_rw_frc_lut_mode3_006Ch_FRC_LUT_1_data_108;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 996Dh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_006Dh_FRC_LUT_1_data_109;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 996Eh

extern volatile uint8_t xdata g_rw_frc_lut_mode3_006Eh_FRC_LUT_1_data_110;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 996Fh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_006Fh_FRC_LUT_1_data_111;                               // [msb:lsb] = [7:0], val = 72, 	Absolute Address = 9970h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0070h_FRC_LUT_1_data_112;                               // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 9971h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0071h_FRC_LUT_1_data_113;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 9972h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0072h_FRC_LUT_1_data_114;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9973h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0073h_FRC_LUT_1_data_115;                               // [msb:lsb] = [7:0], val = 102, 	Absolute Address = 9974h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0074h_FRC_LUT_1_data_116;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 9975h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0075h_FRC_LUT_1_data_117;                               // [msb:lsb] = [7:0], val = 148, 	Absolute Address = 9976h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0076h_FRC_LUT_1_data_118;                               // [msb:lsb] = [7:0], val = 102, 	Absolute Address = 9977h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0077h_FRC_LUT_1_data_119;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9978h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0078h_FRC_LUT_1_data_120;                               // [msb:lsb] = [7:0], val = 102, 	Absolute Address = 9979h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0079h_FRC_LUT_1_data_121;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 997Ah
extern volatile uint8_t xdata g_rw_frc_lut_mode3_007Ah_FRC_LUT_1_data_122;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 997Bh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_007Bh_FRC_LUT_1_data_123;                               // [msb:lsb] = [7:0], val = 149, 	Absolute Address = 997Ch
extern volatile uint8_t xdata g_rw_frc_lut_mode3_007Ch_FRC_LUT_1_data_124;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 997Dh

extern volatile uint8_t xdata g_rw_frc_lut_mode3_007Dh_FRC_LUT_1_data_125;                               // [msb:lsb] = [7:0], val = 102, 	Absolute Address = 997Eh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_007Eh_FRC_LUT_1_data_126;                               // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 997Fh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_007Fh_FRC_LUT_1_data_127;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 9980h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0080h_FRC_LUT_1_data_128;                               // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 9981h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0081h_FRC_LUT_1_data_129;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9982h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0082h_FRC_LUT_1_data_130;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9983h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0083h_FRC_LUT_1_data_131;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9984h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0084h_FRC_LUT_1_data_132;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9985h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0085h_FRC_LUT_1_data_133;                               // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 9986h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0086h_FRC_LUT_1_data_134;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9987h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0087h_FRC_LUT_1_data_135;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9988h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0088h_FRC_LUT_1_data_136;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9989h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0089h_FRC_LUT_1_data_137;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 998Ah
extern volatile uint8_t xdata g_rw_frc_lut_mode3_008Ah_FRC_LUT_1_data_138;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 998Bh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_008Bh_FRC_LUT_1_data_139;                               // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 998Ch

extern volatile uint8_t xdata g_rw_frc_lut_mode3_008Ch_FRC_LUT_1_data_140;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 998Dh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_008Dh_FRC_LUT_1_data_141;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 998Eh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_008Eh_FRC_LUT_1_data_142;                               // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 998Fh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_008Fh_FRC_LUT_1_data_143;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9990h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0090h_FRC_LUT_1_data_144;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 9991h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0091h_FRC_LUT_1_data_145;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9992h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0092h_FRC_LUT_1_data_146;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9993h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0093h_FRC_LUT_1_data_147;                               // [msb:lsb] = [7:0], val = 118, 	Absolute Address = 9994h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0094h_FRC_LUT_1_data_148;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9995h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0095h_FRC_LUT_1_data_149;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 9996h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_0096h_FRC_LUT_1_data_150;                               // [msb:lsb] = [7:0], val = 118, 	Absolute Address = 9997h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0097h_FRC_LUT_1_data_151;                               // [msb:lsb] = [7:0], val = 129, 	Absolute Address = 9998h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0098h_FRC_LUT_1_data_152;                               // [msb:lsb] = [7:0], val = 118, 	Absolute Address = 9999h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0099h_FRC_LUT_1_data_153;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 999Ah
extern volatile uint8_t xdata g_rw_frc_lut_mode3_009Ah_FRC_LUT_1_data_154;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 999Bh

extern volatile uint8_t xdata g_rw_frc_lut_mode3_009Bh_FRC_LUT_1_data_155;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 999Ch
extern volatile uint8_t xdata g_rw_frc_lut_mode3_009Ch_FRC_LUT_1_data_156;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 999Dh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_009Dh_FRC_LUT_1_data_157;                               // [msb:lsb] = [7:0], val = 118, 	Absolute Address = 999Eh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_009Eh_FRC_LUT_1_data_158;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 999Fh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_009Fh_FRC_LUT_1_data_159;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99A0h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00A0h_FRC_LUT_1_data_160;                               // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 99A1h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00A1h_FRC_LUT_1_data_161;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99A2h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00A2h_FRC_LUT_1_data_162;                               // [msb:lsb] = [7:0], val = 126, 	Absolute Address = 99A3h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00A3h_FRC_LUT_1_data_163;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99A4h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00A4h_FRC_LUT_1_data_164;                               // [msb:lsb] = [7:0], val = 132, 	Absolute Address = 99A5h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00A5h_FRC_LUT_1_data_165;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 99A6h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00A6h_FRC_LUT_1_data_166;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99A7h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00A7h_FRC_LUT_1_data_167;                               // [msb:lsb] = [7:0], val = 126, 	Absolute Address = 99A8h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00A8h_FRC_LUT_1_data_168;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99A9h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00A9h_FRC_LUT_1_data_169;                               // [msb:lsb] = [7:0], val = 127, 	Absolute Address = 99AAh

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00AAh_FRC_LUT_1_data_170;                               // [msb:lsb] = [7:0], val = 148, 	Absolute Address = 99ABh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00ABh_FRC_LUT_1_data_171;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 99ACh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00ACh_FRC_LUT_1_data_172;                               // [msb:lsb] = [7:0], val = 126, 	Absolute Address = 99ADh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00ADh_FRC_LUT_1_data_173;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99AEh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00AEh_FRC_LUT_1_data_174;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 99AFh

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00AFh_FRC_LUT_1_data_175;                               // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 99B0h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00B0h_FRC_LUT_1_data_176;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 99B1h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00B1h_FRC_LUT_1_data_177;                               // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 99B2h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00B2h_FRC_LUT_1_data_178;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 99B3h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00B3h_FRC_LUT_1_data_179;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99B4h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00B4h_FRC_LUT_1_data_180;                               // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 99B5h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00B5h_FRC_LUT_1_data_181;                               // [msb:lsb] = [7:0], val = 4, 	Absolute Address = 99B6h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00B6h_FRC_LUT_1_data_182;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99B7h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00B7h_FRC_LUT_1_data_183;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 99B8h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00B8h_FRC_LUT_1_data_184;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99B9h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00B9h_FRC_LUT_1_data_185;                               // [msb:lsb] = [7:0], val = 105, 	Absolute Address = 99BAh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00BAh_FRC_LUT_1_data_186;                               // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 99BBh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00BBh_FRC_LUT_1_data_187;                               // [msb:lsb] = [7:0], val = 4, 	Absolute Address = 99BCh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00BCh_FRC_LUT_1_data_188;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 99BDh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00BDh_FRC_LUT_1_data_189;                               // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 99BEh

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00BEh_FRC_LUT_1_data_190;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 99BFh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00BFh_FRC_LUT_1_data_191;                               // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 99C0h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00C0h_FRC_LUT_1_data_192;                               // [msb:lsb] = [7:0], val = 192, 	Absolute Address = 99C1h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00C1h_FRC_LUT_1_data_193;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 99C2h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00C2h_FRC_LUT_1_data_194;                               // [msb:lsb] = [7:0], val = 97, 	Absolute Address = 99C3h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00C3h_FRC_LUT_1_data_195;                               // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 99C4h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00C4h_FRC_LUT_1_data_196;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 99C5h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00C5h_FRC_LUT_1_data_197;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99C6h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00C6h_FRC_LUT_1_data_198;                               // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 99C7h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00C7h_FRC_LUT_1_data_199;                               // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 99C8h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00C8h_FRC_LUT_1_data_200;                               // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 99C9h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00C9h_FRC_LUT_1_data_201;                               // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 99CAh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00CAh_FRC_LUT_1_data_202;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 99CBh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00CBh_FRC_LUT_1_data_203;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99CCh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00CCh_FRC_LUT_1_data_204;                               // [msb:lsb] = [7:0], val = 97, 	Absolute Address = 99CDh

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00CDh_FRC_LUT_1_data_205;                               // [msb:lsb] = [7:0], val = 152, 	Absolute Address = 99CEh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00CEh_FRC_LUT_1_data_206;                               // [msb:lsb] = [7:0], val = 192, 	Absolute Address = 99CFh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00CFh_FRC_LUT_1_data_207;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 99D0h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00D0h_FRC_LUT_1_data_208;                               // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 99D1h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00D1h_FRC_LUT_1_data_209;                               // [msb:lsb] = [7:0], val = 102, 	Absolute Address = 99D2h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00D2h_FRC_LUT_1_data_210;                               // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 99D3h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00D3h_FRC_LUT_1_data_211;                               // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 99D4h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00D4h_FRC_LUT_1_data_212;                               // [msb:lsb] = [7:0], val = 102, 	Absolute Address = 99D5h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00D5h_FRC_LUT_1_data_213;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99D6h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00D6h_FRC_LUT_1_data_214;                               // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 99D7h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00D7h_FRC_LUT_1_data_215;                               // [msb:lsb] = [7:0], val = 80, 	Absolute Address = 99D8h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00D8h_FRC_LUT_1_data_216;                               // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 99D9h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00D9h_FRC_LUT_1_data_217;                               // [msb:lsb] = [7:0], val = 80, 	Absolute Address = 99DAh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00DAh_FRC_LUT_1_data_218;                               // [msb:lsb] = [7:0], val = 118, 	Absolute Address = 99DBh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00DBh_FRC_LUT_1_data_219;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99DCh

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00DCh_FRC_LUT_1_data_220;                               // [msb:lsb] = [7:0], val = 17, 	Absolute Address = 99DDh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00DDh_FRC_LUT_1_data_221;                               // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 99DEh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00DEh_FRC_LUT_1_data_222;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99DFh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00DFh_FRC_LUT_1_data_223;                               // [msb:lsb] = [7:0], val = 118, 	Absolute Address = 99E0h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00E0h_FRC_LUT_1_data_224;                               // [msb:lsb] = [7:0], val = 230, 	Absolute Address = 99E1h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00E1h_FRC_LUT_1_data_225;                               // [msb:lsb] = [7:0], val = 8, 	Absolute Address = 99E2h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00E2h_FRC_LUT_1_data_226;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99E3h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00E3h_FRC_LUT_1_data_227;                               // [msb:lsb] = [7:0], val = 116, 	Absolute Address = 99E4h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00E4h_FRC_LUT_1_data_228;                               // [msb:lsb] = [7:0], val = 8, 	Absolute Address = 99E5h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00E5h_FRC_LUT_1_data_229;                               // [msb:lsb] = [7:0], val = 230, 	Absolute Address = 99E6h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00E6h_FRC_LUT_1_data_230;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 99E7h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00E7h_FRC_LUT_1_data_231;                               // [msb:lsb] = [7:0], val = 129, 	Absolute Address = 99E8h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00E8h_FRC_LUT_1_data_232;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 99E9h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00E9h_FRC_LUT_1_data_233;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99EAh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00EAh_FRC_LUT_1_data_234;                               // [msb:lsb] = [7:0], val = 8, 	Absolute Address = 99EBh

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00EBh_FRC_LUT_1_data_235;                               // [msb:lsb] = [7:0], val = 230, 	Absolute Address = 99ECh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00ECh_FRC_LUT_1_data_236;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99EDh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00EDh_FRC_LUT_1_data_237;                               // [msb:lsb] = [7:0], val = 116, 	Absolute Address = 99EEh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00EEh_FRC_LUT_1_data_238;                               // [msb:lsb] = [7:0], val = 230, 	Absolute Address = 99EFh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00EFh_FRC_LUT_1_data_239;                               // [msb:lsb] = [7:0], val = 8, 	Absolute Address = 99F0h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00F0h_FRC_LUT_1_data_240;                               // [msb:lsb] = [7:0], val = 148, 	Absolute Address = 99F1h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00F1h_FRC_LUT_1_data_241;                               // [msb:lsb] = [7:0], val = 72, 	Absolute Address = 99F2h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00F2h_FRC_LUT_1_data_242;                               // [msb:lsb] = [7:0], val = 129, 	Absolute Address = 99F3h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00F3h_FRC_LUT_1_data_243;                               // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 99F4h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00F4h_FRC_LUT_1_data_244;                               // [msb:lsb] = [7:0], val = 72, 	Absolute Address = 99F5h

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00F5h_FRC_LUT_1_data_245;                               // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 99F6h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00F6h_FRC_LUT_1_data_246;                               // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 99F7h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00F7h_FRC_LUT_1_data_247;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99F8h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00F8h_FRC_LUT_1_data_248;                               // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 99F9h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00F9h_FRC_LUT_1_data_249;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99FAh

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00FAh_FRC_LUT_1_data_250;                               // [msb:lsb] = [7:0], val = 72, 	Absolute Address = 99FBh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00FBh_FRC_LUT_1_data_251;                               // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 99FCh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00FCh_FRC_LUT_1_data_252;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 99FDh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00FDh_FRC_LUT_1_data_253;                               // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 99FEh
extern volatile uint8_t xdata g_rw_frc_lut_mode3_00FEh_FRC_LUT_1_data_254;                               // [msb:lsb] = [7:0], val = 148, 	Absolute Address = 99FFh

extern volatile uint8_t xdata g_rw_frc_lut_mode3_00FFh_FRC_LUT_1_data_255;                               // [msb:lsb] = [7:0], val = 72, 	Absolute Address = 9A00h
extern volatile uint8_t xdata g_rw_frc_lut_mode3_0100h_FRC_LUT_1_data_256;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9A01h

#endif