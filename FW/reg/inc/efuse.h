#ifndef _EFUSE_H_
#define _EFUSE_H_
#include "reg_include.h"

union rw_efuse_0002h
{
    unsigned char byte;
    struct
    {
        unsigned char r_central_tc                                                           : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char intpll_pll_lock_ef                                                     : 1;        // val = 0
        unsigned char sscgpll_pll_lock_ef                                                    : 1;        // val = 0
        unsigned char tx_pll_lock_ef                                                         : 1;        // val = 0
    }bits;
};
extern volatile union rw_efuse_0002h xdata g_rw_efuse_0002h;    // Absolute Address = 7002h

union rw_efuse_0003h
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_prech_lpf                                                     : 1;        // val = 0
        unsigned char r_tx_prech_lpf                                                         : 1;        // val = 0
        unsigned char r_osc_selcs_2x                                                         : 1;        // val = 0
        unsigned char r_osc_seli_2x                                                          : 1;        // val = 0
        unsigned char r_osc_selc_2x                                                          : 1;        // val = 0
        unsigned char r_osc_sel_div2                                                         : 1;        // val = 0
        unsigned char r_osc_add_i_cmp                                                        : 1;        // val = 0
        unsigned char r_osc_cs_trim_bits_0                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_efuse_0003h xdata g_rw_efuse_0003h;    // Absolute Address = 7003h

union rw_efuse_0006h
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscgpll_sel_icp                                                      : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char r_sscgpll_sel_ldo_cp                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_efuse_0006h xdata g_rw_efuse_0006h;    // Absolute Address = 7006h

union rw_efuse_0007h
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscgpll_sel_ldo_cco                                                  : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_sscgpll_twr                                                          : 1;        // val = 0
        unsigned char r_syspll_sel_ldo_cco                                                   : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_syspll_twr                                                           : 1;        // val = 0
    }bits;
};
extern volatile union rw_efuse_0007h xdata g_rw_efuse_0007h;    // Absolute Address = 7007h

union rw_efuse_0008h
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_sel_icp                                                       : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char r_syspll_sel_ldo_cp                                                    : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_efuse_0008h xdata g_rw_efuse_0008h;    // Absolute Address = 7008h

//------------------------------------------------------------------------------------------------------------------------
union rw_efuse_000Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_trim_ipema                                                        : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char r_tx_prech_lpf                                                         : 1;        // val = 0
        unsigned char r_ldo_aip_en_ldo_a                                                     : 1;        // val = 0
        unsigned char r_ldo_aip_en_ldo_b                                                     : 1;        // val = 0
    }bits;
};
extern volatile union rw_efuse_000Ah xdata g_rw_efuse_000Ah;    // Absolute Address = 700Ah

union rw_efuse_000Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_trim_ibnpe                                                        : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_ldo_apr_sel_vref                                                     : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_efuse_000Bh xdata g_rw_efuse_000Bh;    // Absolute Address = 700Bh

union rw_efuse_000Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_rx_sel_ldorx                                                         : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_romchk_en                                                            : 1;        // val = 0
        unsigned char r_sscg2_read_skew_xor                                                  : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_efuse_000Dh xdata g_rw_efuse_000Dh;    // Absolute Address = 700Dh

union rw_efuse_000Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscg2_write_skew_xor                                                 : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_debc_rstsel                                                          : 1;        // val = 0
        unsigned char mcu_efuse_o_bits_3_0                                                   : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_efuse_000Eh xdata g_rw_efuse_000Eh;    // Absolute Address = 700Eh

union rw_efuse_0010h
{
    unsigned char byte;
    struct
    {
        unsigned char mcu_efuse_o_bits_13_12                                                 : 2;        // [msb:lsb] = [13:12], val = 0
        unsigned char r_init_i2c_rst_dly_en                                                  : 1;        // val = 0
        unsigned char r_i2c_2pin_sel_fuse                                                    : 1;        // val = 0
        unsigned char reserved1_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_efuse_0010h xdata g_rw_efuse_0010h;    // Absolute Address = 7010h

//------------------------------------------------------------------------------------------------------------------------
union rw_efuse_0011h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved1_bits_7_4                                                     : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char reserved2_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_efuse_0011h xdata g_rw_efuse_0011h;    // Absolute Address = 7011h

union rw_efuse_0012h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved2_bits_7_4                                                     : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char reserved3_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_efuse_0012h xdata g_rw_efuse_0012h;    // Absolute Address = 7012h

union rw_efuse_0013h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved3_bits_7_4                                                     : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char reserved4_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_efuse_0013h xdata g_rw_efuse_0013h;    // Absolute Address = 7013h

union rw_efuse_0014h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved4_bits_7_4                                                     : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char reserved5_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_efuse_0014h xdata g_rw_efuse_0014h;    // Absolute Address = 7014h

union rw_efuse_0015h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved5_bits_7_4                                                     : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char reserved6_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_efuse_0015h xdata g_rw_efuse_0015h;    // Absolute Address = 7015h

//------------------------------------------------------------------------------------------------------------------------
union rw_efuse_0016h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved6_bits_7_4                                                     : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char reserved7_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_efuse_0016h xdata g_rw_efuse_0016h;    // Absolute Address = 7016h

union rw_efuse_0017h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved7_bits_7_4                                                     : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char reserved8_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_efuse_0017h xdata g_rw_efuse_0017h;    // Absolute Address = 7017h

union rw_efuse_0018h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved8_bits_7_4                                                     : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char reserved9_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_efuse_0018h xdata g_rw_efuse_0018h;    // Absolute Address = 7018h

union rw_efuse_0019h
{
    unsigned char byte;
    struct
    {
        unsigned char reserved9_bits_7_4                                                     : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char reserved10_bits_3_0                                                    : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_efuse_0019h xdata g_rw_efuse_0019h;    // Absolute Address = 7019h

union rw_efuse_001Ah
{
    unsigned char byte;
    struct
    {
        unsigned char reserved10_bits_7_4                                                    : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char reserved11_bits_3_0                                                    : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_efuse_001Ah xdata g_rw_efuse_001Ah;    // Absolute Address = 701Ah

//------------------------------------------------------------------------------------------------------------------------
union rw_efuse_001Bh
{
    unsigned char byte;
    struct
    {
        unsigned char reserved11_bits_7_4                                                    : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char reserved_3_by_tool                                                     : 1;
        unsigned char reserved_2_by_tool                                                     : 1;
        unsigned char reserved_1_by_tool                                                     : 1;
        unsigned char reserved_0_by_tool                                                     : 1;
    }bits;
};
extern volatile union rw_efuse_001Bh xdata g_rw_efuse_001Bh;    // Absolute Address = 701Bh


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_efuse_0000h_Project_id_LSB;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7000h
extern volatile uint8_t xdata g_rw_efuse_0001h_Project_id_MSB;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7001h
extern volatile uint8_t xdata g_rw_efuse_0004h_osc_cs_trim;                                              // [msb:lsb] = [8:1], val = 0, 	Absolute Address = 7004h
extern volatile uint8_t xdata g_rw_efuse_0005h_osc_reserve;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7005h
extern volatile uint8_t xdata g_rw_efuse_0009h_tx_reserve_5;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7009h

extern volatile uint8_t xdata g_rw_efuse_000Ch_rx_reserve_2;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 700Ch
extern volatile uint8_t xdata g_rw_efuse_000Fh_mcu_o;                                                    // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 700Fh

#endif