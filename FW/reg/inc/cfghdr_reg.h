#ifndef _CFGHDR_REG_H_
#define _CFGHDR_REG_H_
#include "reg_include.h"


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_cfghdr_reg_0000h_cfg_reserved0;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2100h
extern volatile uint8_t xdata g_rw_cfghdr_reg_0001h_cfg_reserved1;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2101h

#endif