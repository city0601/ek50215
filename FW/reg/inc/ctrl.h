#ifndef _CTRL_H_
#define _CTRL_H_
#include "reg_include.h"

union rw_ctrl_0000h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck00_nor_mask_en                                                    : 1;        // val = 1
        unsigned char r_gck00_rev_set                                                        : 1;        // val = 0
        unsigned char r_gck00_pol                                                            : 1;        // val = 0
        unsigned char r_gck00_end_pol                                                        : 1;        // val = 0
        unsigned char r_gck00_frm_stcut_en                                                   : 1;        // val = 1
        unsigned char r_gck00_dis                                                            : 1;        // val = 0
        unsigned char r_gck00_init_val                                                       : 1;        // val = 0
        unsigned char r_gck00_cycle_sel_bits_0                                               : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0000h xdata g_rw_ctrl_0000h;    // Absolute Address = 6800h

union rw_ctrl_0001h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck00_cycle_sel_bits_1                                               : 1;        // val = 0
        unsigned char r_gck00_f_sel                                                          : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_gck00_frm_inv                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck00_cycle_bits_1_0                                                 : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0001h xdata g_rw_ctrl_0001h;    // Absolute Address = 6801h

union rw_ctrl_0002h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck00_cycle_bits_3_2                                                 : 2;        // [msb:lsb] = [3:2], val = 0
        unsigned char r_gck00_r_no                                                           : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck00_f_no_bits_1_0                                                  : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0002h xdata g_rw_ctrl_0002h;    // Absolute Address = 6802h

union rw_ctrl_0003h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck00_f_no_bits_3_2                                                  : 2;        // [msb:lsb] = [3:2], val = 0
        unsigned char r_gck00_vblk_v_line                                                    : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck00_frm_cycle_bits_1_0                                             : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_0003h xdata g_rw_ctrl_0003h;    // Absolute Address = 6803h

union rw_ctrl_0004h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck00_frm_cycle_bits_8_2                                             : 7;        // [msb:lsb] = [8:2], val = 0
        unsigned char r_gck00_init_bits_0                                                    : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0004h xdata g_rw_ctrl_0004h;    // Absolute Address = 6804h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0005h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck00_init_bits_7_1                                                  : 7;        // [msb:lsb] = [7:1], val = 0
        unsigned char r_gck00_st_v_line_bits_0                                               : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0005h xdata g_rw_ctrl_0005h;    // Absolute Address = 6805h

union rw_ctrl_0007h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck00_st_v_line_bits_13_9                                            : 5;        // [msb:lsb] = [13:9], val = 0
        unsigned char r_gck00_end_v_line_bits_2_0                                            : 3;        // [msb:lsb] = [2:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_0007h xdata g_rw_ctrl_0007h;    // Absolute Address = 6807h

union rw_ctrl_0009h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck00_end_v_line_bits_13_11                                          : 3;        // [msb:lsb] = [13:11], val = 0
        unsigned char r_gck00_sh_bits_4_0                                                    : 5;        // [msb:lsb] = [4:0], val = 20
    }bits;
};
extern volatile union rw_ctrl_0009h xdata g_rw_ctrl_0009h;    // Absolute Address = 6809h

union rw_ctrl_000Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck00_w_bits_12_8                                                    : 5;        // [msb:lsb] = [12:8], val = 0
        unsigned char r_gck01_nor_mask_en                                                    : 1;        // val = 1
        unsigned char r_gck01_rev_set                                                        : 1;        // val = 0
        unsigned char r_gck01_pol                                                            : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_000Ch xdata g_rw_ctrl_000Ch;    // Absolute Address = 680Ch

union rw_ctrl_000Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck01_end_pol                                                        : 1;        // val = 0
        unsigned char r_gck01_frm_stcut_en                                                   : 1;        // val = 1
        unsigned char r_gck01_dis                                                            : 1;        // val = 0
        unsigned char r_gck01_init_val                                                       : 1;        // val = 0
        unsigned char r_gck01_cycle_sel                                                      : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck01_f_sel_bits_1_0                                                 : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_000Dh xdata g_rw_ctrl_000Dh;    // Absolute Address = 680Dh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_000Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck01_f_sel_bits_2                                                   : 1;        // val = 0
        unsigned char r_gck01_frm_inv                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck01_cycle                                                          : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck01_r_no_bits_0                                                    : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_000Eh xdata g_rw_ctrl_000Eh;    // Absolute Address = 680Eh

union rw_ctrl_000Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck01_r_no_bits_3_1                                                  : 3;        // [msb:lsb] = [3:1], val = 0
        unsigned char r_gck01_f_no                                                           : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck01_vblk_v_line_bits_0                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_000Fh xdata g_rw_ctrl_000Fh;    // Absolute Address = 680Fh

union rw_ctrl_0010h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck01_vblk_v_line_bits_3_1                                           : 3;        // [msb:lsb] = [3:1], val = 0
        unsigned char r_gck01_frm_cycle_bits_4_0                                             : 5;        // [msb:lsb] = [4:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_0010h xdata g_rw_ctrl_0010h;    // Absolute Address = 6810h

union rw_ctrl_0011h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck01_frm_cycle_bits_8_5                                             : 4;        // [msb:lsb] = [8:5], val = 0
        unsigned char r_gck01_init_bits_3_0                                                  : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0011h xdata g_rw_ctrl_0011h;    // Absolute Address = 6811h

union rw_ctrl_0012h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck01_init_bits_7_4                                                  : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char r_gck01_st_v_line_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_ctrl_0012h xdata g_rw_ctrl_0012h;    // Absolute Address = 6812h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0014h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck01_st_v_line_bits_13_12                                           : 2;        // [msb:lsb] = [13:12], val = 0
        unsigned char r_gck01_end_v_line_bits_5_0                                            : 6;        // [msb:lsb] = [5:0], val = 51
    }bits;
};
extern volatile union rw_ctrl_0014h xdata g_rw_ctrl_0014h;    // Absolute Address = 6814h

union rw_ctrl_0017h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck01_sh_bits_12_8                                                   : 5;        // [msb:lsb] = [12:8], val = 1
        unsigned char r_gck01_w_bits_2_0                                                     : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0017h xdata g_rw_ctrl_0017h;    // Absolute Address = 6817h

union rw_ctrl_0019h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck01_w_bits_12_11                                                   : 2;        // [msb:lsb] = [12:11], val = 0
        unsigned char r_gck02_nor_mask_en                                                    : 1;        // val = 0
        unsigned char r_gck02_rev_set                                                        : 1;        // val = 0
        unsigned char r_gck02_pol                                                            : 1;        // val = 0
        unsigned char r_gck02_end_pol                                                        : 1;        // val = 0
        unsigned char r_gck02_frm_stcut_en                                                   : 1;        // val = 0
        unsigned char r_gck02_dis                                                            : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0019h xdata g_rw_ctrl_0019h;    // Absolute Address = 6819h

union rw_ctrl_001Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck02_init_val                                                       : 1;        // val = 0
        unsigned char r_gck02_cycle_sel                                                      : 2;        // [msb:lsb] = [1:0], val = 1
        unsigned char r_gck02_f_sel                                                          : 3;        // [msb:lsb] = [2:0], val = 4
        unsigned char r_gck02_frm_inv                                                        : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_001Ah xdata g_rw_ctrl_001Ah;    // Absolute Address = 681Ah

union rw_ctrl_001Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck02_cycle                                                          : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck02_r_no                                                           : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_001Bh xdata g_rw_ctrl_001Bh;    // Absolute Address = 681Bh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_001Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck02_f_no                                                           : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck02_vblk_v_line                                                    : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_001Ch xdata g_rw_ctrl_001Ch;    // Absolute Address = 681Ch

union rw_ctrl_001Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck02_frm_cycle_bits_8                                               : 1;        // val = 0
        unsigned char r_gck02_init_bits_6_0                                                  : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_001Eh xdata g_rw_ctrl_001Eh;    // Absolute Address = 681Eh

union rw_ctrl_001Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck02_init_bits_7                                                    : 1;        // val = 0
        unsigned char r_gck02_st_v_line_bits_6_0                                             : 7;        // [msb:lsb] = [6:0], val = 60
    }bits;
};
extern volatile union rw_ctrl_001Fh xdata g_rw_ctrl_001Fh;    // Absolute Address = 681Fh

union rw_ctrl_0020h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck02_st_v_line_bits_13_7                                            : 7;        // [msb:lsb] = [13:7], val = 8
        unsigned char r_gck02_end_v_line_bits_0                                              : 1;        // val = 1
    }bits;
};
extern volatile union rw_ctrl_0020h xdata g_rw_ctrl_0020h;    // Absolute Address = 6820h

union rw_ctrl_0022h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck02_end_v_line_bits_13_9                                           : 5;        // [msb:lsb] = [13:9], val = 7
        unsigned char r_gck02_sh_bits_2_0                                                    : 3;        // [msb:lsb] = [2:0], val = 4
    }bits;
};
extern volatile union rw_ctrl_0022h xdata g_rw_ctrl_0022h;    // Absolute Address = 6822h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0024h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck02_sh_bits_12_11                                                  : 2;        // [msb:lsb] = [12:11], val = 0
        unsigned char r_gck02_w_bits_5_0                                                     : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0024h xdata g_rw_ctrl_0024h;    // Absolute Address = 6824h

union rw_ctrl_0025h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck02_w_bits_12_6                                                    : 7;        // [msb:lsb] = [12:6], val = 0
        unsigned char r_gck03_nor_mask_en                                                    : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0025h xdata g_rw_ctrl_0025h;    // Absolute Address = 6825h

union rw_ctrl_0026h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck03_rev_set                                                        : 1;        // val = 0
        unsigned char r_gck03_pol                                                            : 1;        // val = 0
        unsigned char r_gck03_end_pol                                                        : 1;        // val = 0
        unsigned char r_gck03_frm_stcut_en                                                   : 1;        // val = 0
        unsigned char r_gck03_dis                                                            : 1;        // val = 0
        unsigned char r_gck03_init_val                                                       : 1;        // val = 0
        unsigned char r_gck03_cycle_sel                                                      : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_0026h xdata g_rw_ctrl_0026h;    // Absolute Address = 6826h

union rw_ctrl_0027h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck03_f_sel                                                          : 3;        // [msb:lsb] = [2:0], val = 4
        unsigned char r_gck03_frm_inv                                                        : 2;        // [msb:lsb] = [1:0], val = 1
        unsigned char r_gck03_cycle_bits_2_0                                                 : 3;        // [msb:lsb] = [2:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_0027h xdata g_rw_ctrl_0027h;    // Absolute Address = 6827h

union rw_ctrl_0028h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck03_cycle_bits_3                                                   : 1;        // val = 0
        unsigned char r_gck03_r_no                                                           : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck03_f_no_bits_2_0                                                  : 3;        // [msb:lsb] = [2:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_0028h xdata g_rw_ctrl_0028h;    // Absolute Address = 6828h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0029h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck03_f_no_bits_3                                                    : 1;        // val = 0
        unsigned char r_gck03_vblk_v_line                                                    : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck03_frm_cycle_bits_2_0                                             : 3;        // [msb:lsb] = [2:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_0029h xdata g_rw_ctrl_0029h;    // Absolute Address = 6829h

union rw_ctrl_002Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck03_frm_cycle_bits_8_3                                             : 6;        // [msb:lsb] = [8:3], val = 0
        unsigned char r_gck03_init_bits_1_0                                                  : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_002Ah xdata g_rw_ctrl_002Ah;    // Absolute Address = 682Ah

union rw_ctrl_002Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck03_init_bits_7_2                                                  : 6;        // [msb:lsb] = [7:2], val = 0
        unsigned char r_gck03_st_v_line_bits_1_0                                             : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_002Bh xdata g_rw_ctrl_002Bh;    // Absolute Address = 682Bh

union rw_ctrl_002Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck03_st_v_line_bits_13_10                                           : 4;        // [msb:lsb] = [13:10], val = 1
        unsigned char r_gck03_end_v_line_bits_3_0                                            : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_ctrl_002Dh xdata g_rw_ctrl_002Dh;    // Absolute Address = 682Dh

union rw_ctrl_002Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck03_end_v_line_bits_13_12                                          : 2;        // [msb:lsb] = [13:12], val = 0
        unsigned char r_gck03_sh_bits_5_0                                                    : 6;        // [msb:lsb] = [5:0], val = 10
    }bits;
};
extern volatile union rw_ctrl_002Fh xdata g_rw_ctrl_002Fh;    // Absolute Address = 682Fh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0030h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck03_sh_bits_12_6                                                   : 7;        // [msb:lsb] = [12:6], val = 0
        unsigned char r_gck03_w_bits_0                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0030h xdata g_rw_ctrl_0030h;    // Absolute Address = 6830h

union rw_ctrl_0032h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck03_w_bits_12_9                                                    : 4;        // [msb:lsb] = [12:9], val = 0
        unsigned char r_gck04_nor_mask_en                                                    : 1;        // val = 0
        unsigned char r_gck04_rev_set                                                        : 1;        // val = 0
        unsigned char r_gck04_pol                                                            : 1;        // val = 0
        unsigned char r_gck04_end_pol                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0032h xdata g_rw_ctrl_0032h;    // Absolute Address = 6832h

union rw_ctrl_0033h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck04_frm_stcut_en                                                   : 1;        // val = 0
        unsigned char r_gck04_dis                                                            : 1;        // val = 0
        unsigned char r_gck04_init_val                                                       : 1;        // val = 0
        unsigned char r_gck04_cycle_sel                                                      : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck04_f_sel                                                          : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0033h xdata g_rw_ctrl_0033h;    // Absolute Address = 6833h

union rw_ctrl_0034h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck04_frm_inv                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck04_cycle                                                          : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck04_r_no_bits_1_0                                                  : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0034h xdata g_rw_ctrl_0034h;    // Absolute Address = 6834h

union rw_ctrl_0035h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck04_r_no_bits_3_2                                                  : 2;        // [msb:lsb] = [3:2], val = 0
        unsigned char r_gck04_f_no                                                           : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck04_vblk_v_line_bits_1_0                                           : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0035h xdata g_rw_ctrl_0035h;    // Absolute Address = 6835h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0036h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck04_vblk_v_line_bits_3_2                                           : 2;        // [msb:lsb] = [3:2], val = 0
        unsigned char r_gck04_frm_cycle_bits_5_0                                             : 6;        // [msb:lsb] = [5:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_0036h xdata g_rw_ctrl_0036h;    // Absolute Address = 6836h

union rw_ctrl_0037h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck04_frm_cycle_bits_8_6                                             : 3;        // [msb:lsb] = [8:6], val = 0
        unsigned char r_gck04_init_bits_4_0                                                  : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0037h xdata g_rw_ctrl_0037h;    // Absolute Address = 6837h

union rw_ctrl_0038h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck04_init_bits_7_5                                                  : 3;        // [msb:lsb] = [7:5], val = 0
        unsigned char r_gck04_st_v_line_bits_4_0                                             : 5;        // [msb:lsb] = [4:0], val = 8
    }bits;
};
extern volatile union rw_ctrl_0038h xdata g_rw_ctrl_0038h;    // Absolute Address = 6838h

union rw_ctrl_003Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck04_st_v_line_bits_13                                              : 1;        // val = 0
        unsigned char r_gck04_end_v_line_bits_6_0                                            : 7;        // [msb:lsb] = [6:0], val = 127
    }bits;
};
extern volatile union rw_ctrl_003Ah xdata g_rw_ctrl_003Ah;    // Absolute Address = 683Ah

union rw_ctrl_003Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck04_end_v_line_bits_13_7                                           : 7;        // [msb:lsb] = [13:7], val = 31
        unsigned char r_gck04_sh_bits_0                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_003Bh xdata g_rw_ctrl_003Bh;    // Absolute Address = 683Bh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_003Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck04_sh_bits_12_9                                                   : 4;        // [msb:lsb] = [12:9], val = 5
        unsigned char r_gck04_w_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 2
    }bits;
};
extern volatile union rw_ctrl_003Dh xdata g_rw_ctrl_003Dh;    // Absolute Address = 683Dh

union rw_ctrl_003Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck04_w_bits_12                                                      : 1;        // val = 0
        unsigned char r_gck05_nor_mask_en                                                    : 1;        // val = 0
        unsigned char r_gck05_rev_set                                                        : 1;        // val = 0
        unsigned char r_gck05_pol                                                            : 1;        // val = 0
        unsigned char r_gck05_end_pol                                                        : 1;        // val = 0
        unsigned char r_gck05_frm_stcut_en                                                   : 1;        // val = 1
        unsigned char r_gck05_dis                                                            : 1;        // val = 0
        unsigned char r_gck05_init_val                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_003Fh xdata g_rw_ctrl_003Fh;    // Absolute Address = 683Fh

union rw_ctrl_0040h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck05_cycle_sel                                                      : 2;        // [msb:lsb] = [1:0], val = 1
        unsigned char r_gck05_f_sel                                                          : 3;        // [msb:lsb] = [2:0], val = 1
        unsigned char r_gck05_frm_inv                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck05_cycle_bits_0                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0040h xdata g_rw_ctrl_0040h;    // Absolute Address = 6840h

union rw_ctrl_0041h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck05_cycle_bits_3_1                                                 : 3;        // [msb:lsb] = [3:1], val = 4
        unsigned char r_gck05_r_no                                                           : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck05_f_no_bits_0                                                    : 1;        // val = 1
    }bits;
};
extern volatile union rw_ctrl_0041h xdata g_rw_ctrl_0041h;    // Absolute Address = 6841h

union rw_ctrl_0042h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck05_f_no_bits_3_1                                                  : 3;        // [msb:lsb] = [3:1], val = 2
        unsigned char r_gck05_vblk_v_line                                                    : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck05_frm_cycle_bits_0                                               : 1;        // val = 1
    }bits;
};
extern volatile union rw_ctrl_0042h xdata g_rw_ctrl_0042h;    // Absolute Address = 6842h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0046h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck05_st_v_line_bits_13_8                                            : 6;        // [msb:lsb] = [13:8], val = 0
        unsigned char r_gck05_end_v_line_bits_1_0                                            : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_ctrl_0046h xdata g_rw_ctrl_0046h;    // Absolute Address = 6846h

union rw_ctrl_0048h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck05_end_v_line_bits_13_10                                          : 4;        // [msb:lsb] = [13:10], val = 3
        unsigned char r_gck05_sh_bits_3_0                                                    : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_ctrl_0048h xdata g_rw_ctrl_0048h;    // Absolute Address = 6848h

union rw_ctrl_004Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck05_sh_bits_12                                                     : 1;        // val = 0
        unsigned char r_gck05_w_bits_6_0                                                     : 7;        // [msb:lsb] = [6:0], val = 88
    }bits;
};
extern volatile union rw_ctrl_004Ah xdata g_rw_ctrl_004Ah;    // Absolute Address = 684Ah

union rw_ctrl_004Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck05_w_bits_12_7                                                    : 6;        // [msb:lsb] = [12:7], val = 4
        unsigned char r_gck06_nor_mask_en                                                    : 1;        // val = 0
        unsigned char r_gck06_rev_set                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_004Bh xdata g_rw_ctrl_004Bh;    // Absolute Address = 684Bh

union rw_ctrl_004Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck06_pol                                                            : 1;        // val = 0
        unsigned char r_gck06_end_pol                                                        : 1;        // val = 0
        unsigned char r_gck06_frm_stcut_en                                                   : 1;        // val = 1
        unsigned char r_gck06_dis                                                            : 1;        // val = 0
        unsigned char r_gck06_init_val                                                       : 1;        // val = 0
        unsigned char r_gck06_cycle_sel                                                      : 2;        // [msb:lsb] = [1:0], val = 1
        unsigned char r_gck06_f_sel_bits_0                                                   : 1;        // val = 1
    }bits;
};
extern volatile union rw_ctrl_004Ch xdata g_rw_ctrl_004Ch;    // Absolute Address = 684Ch

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_004Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck06_f_sel_bits_2_1                                                 : 2;        // [msb:lsb] = [2:1], val = 0
        unsigned char r_gck06_frm_inv                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck06_cycle                                                          : 4;        // [msb:lsb] = [3:0], val = 2
    }bits;
};
extern volatile union rw_ctrl_004Dh xdata g_rw_ctrl_004Dh;    // Absolute Address = 684Dh

union rw_ctrl_004Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck06_r_no                                                           : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck06_f_no                                                           : 4;        // [msb:lsb] = [3:0], val = 2
    }bits;
};
extern volatile union rw_ctrl_004Eh xdata g_rw_ctrl_004Eh;    // Absolute Address = 684Eh

union rw_ctrl_004Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck06_vblk_v_line                                                    : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck06_frm_cycle_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_004Fh xdata g_rw_ctrl_004Fh;    // Absolute Address = 684Fh

union rw_ctrl_0050h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck06_frm_cycle_bits_8_4                                             : 5;        // [msb:lsb] = [8:4], val = 0
        unsigned char r_gck06_init_bits_2_0                                                  : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0050h xdata g_rw_ctrl_0050h;    // Absolute Address = 6850h

union rw_ctrl_0051h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck06_init_bits_7_3                                                  : 5;        // [msb:lsb] = [7:3], val = 0
        unsigned char r_gck06_st_v_line_bits_2_0                                             : 3;        // [msb:lsb] = [2:0], val = 4
    }bits;
};
extern volatile union rw_ctrl_0051h xdata g_rw_ctrl_0051h;    // Absolute Address = 6851h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0053h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck06_st_v_line_bits_13_11                                           : 3;        // [msb:lsb] = [13:11], val = 0
        unsigned char r_gck06_end_v_line_bits_4_0                                            : 5;        // [msb:lsb] = [4:0], val = 31
    }bits;
};
extern volatile union rw_ctrl_0053h xdata g_rw_ctrl_0053h;    // Absolute Address = 6853h

union rw_ctrl_0055h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck06_end_v_line_bits_13                                             : 1;        // val = 0
        unsigned char r_gck06_sh_bits_6_0                                                    : 7;        // [msb:lsb] = [6:0], val = 10
    }bits;
};
extern volatile union rw_ctrl_0055h xdata g_rw_ctrl_0055h;    // Absolute Address = 6855h

union rw_ctrl_0056h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck06_sh_bits_12_7                                                   : 6;        // [msb:lsb] = [12:7], val = 0
        unsigned char r_gck06_w_bits_1_0                                                     : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_ctrl_0056h xdata g_rw_ctrl_0056h;    // Absolute Address = 6856h

union rw_ctrl_0058h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck06_w_bits_12_10                                                   : 3;        // [msb:lsb] = [12:10], val = 0
        unsigned char r_gck07_nor_mask_en                                                    : 1;        // val = 0
        unsigned char r_gck07_rev_set                                                        : 1;        // val = 0
        unsigned char r_gck07_pol                                                            : 1;        // val = 0
        unsigned char r_gck07_end_pol                                                        : 1;        // val = 0
        unsigned char r_gck07_frm_stcut_en                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0058h xdata g_rw_ctrl_0058h;    // Absolute Address = 6858h

union rw_ctrl_0059h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck07_dis                                                            : 1;        // val = 0
        unsigned char r_gck07_init_val                                                       : 1;        // val = 0
        unsigned char r_gck07_cycle_sel                                                      : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck07_f_sel                                                          : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_gck07_frm_inv_bits_0                                                 : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0059h xdata g_rw_ctrl_0059h;    // Absolute Address = 6859h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_005Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck07_frm_inv_bits_1                                                 : 1;        // val = 0
        unsigned char r_gck07_cycle                                                          : 4;        // [msb:lsb] = [3:0], val = 5
        unsigned char r_gck07_r_no_bits_2_0                                                  : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_005Ah xdata g_rw_ctrl_005Ah;    // Absolute Address = 685Ah

union rw_ctrl_005Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck07_r_no_bits_3                                                    : 1;        // val = 0
        unsigned char r_gck07_f_no                                                           : 4;        // [msb:lsb] = [3:0], val = 2
        unsigned char r_gck07_vblk_v_line_bits_2_0                                           : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_005Bh xdata g_rw_ctrl_005Bh;    // Absolute Address = 685Bh

union rw_ctrl_005Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck07_vblk_v_line_bits_3                                             : 1;        // val = 0
        unsigned char r_gck07_frm_cycle_bits_6_0                                             : 7;        // [msb:lsb] = [6:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_005Ch xdata g_rw_ctrl_005Ch;    // Absolute Address = 685Ch

union rw_ctrl_005Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck07_frm_cycle_bits_8_7                                             : 2;        // [msb:lsb] = [8:7], val = 0
        unsigned char r_gck07_init_bits_5_0                                                  : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_005Dh xdata g_rw_ctrl_005Dh;    // Absolute Address = 685Dh

union rw_ctrl_005Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck07_init_bits_7_6                                                  : 2;        // [msb:lsb] = [7:6], val = 0
        unsigned char r_gck07_st_v_line_bits_5_0                                             : 6;        // [msb:lsb] = [5:0], val = 8
    }bits;
};
extern volatile union rw_ctrl_005Eh xdata g_rw_ctrl_005Eh;    // Absolute Address = 685Eh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0061h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck07_end_v_line_bits_13_8                                           : 6;        // [msb:lsb] = [13:8], val = 5
        unsigned char r_gck07_sh_bits_1_0                                                    : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0061h xdata g_rw_ctrl_0061h;    // Absolute Address = 6861h

union rw_ctrl_0063h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck07_sh_bits_12_10                                                  : 3;        // [msb:lsb] = [12:10], val = 1
        unsigned char r_gck07_w_bits_4_0                                                     : 5;        // [msb:lsb] = [4:0], val = 20
    }bits;
};
extern volatile union rw_ctrl_0063h xdata g_rw_ctrl_0063h;    // Absolute Address = 6863h

union rw_ctrl_0065h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck08_nor_mask_en                                                    : 1;        // val = 0
        unsigned char r_gck08_rev_set                                                        : 1;        // val = 0
        unsigned char r_gck08_pol                                                            : 1;        // val = 0
        unsigned char r_gck08_end_pol                                                        : 1;        // val = 0
        unsigned char r_gck08_frm_stcut_en                                                   : 1;        // val = 1
        unsigned char r_gck08_dis                                                            : 1;        // val = 0
        unsigned char r_gck08_init_val                                                       : 1;        // val = 0
        unsigned char r_gck08_cycle_sel_bits_0                                               : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0065h xdata g_rw_ctrl_0065h;    // Absolute Address = 6865h

union rw_ctrl_0066h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck08_cycle_sel_bits_1                                               : 1;        // val = 0
        unsigned char r_gck08_f_sel                                                          : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_gck08_frm_inv                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck08_cycle_bits_1_0                                                 : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_0066h xdata g_rw_ctrl_0066h;    // Absolute Address = 6866h

union rw_ctrl_0067h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck08_cycle_bits_3_2                                                 : 2;        // [msb:lsb] = [3:2], val = 1
        unsigned char r_gck08_r_no                                                           : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck08_f_no_bits_1_0                                                  : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_ctrl_0067h xdata g_rw_ctrl_0067h;    // Absolute Address = 6867h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0068h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck08_f_no_bits_3_2                                                  : 2;        // [msb:lsb] = [3:2], val = 0
        unsigned char r_gck08_vblk_v_line                                                    : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck08_frm_cycle_bits_1_0                                             : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_0068h xdata g_rw_ctrl_0068h;    // Absolute Address = 6868h

union rw_ctrl_0069h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck08_frm_cycle_bits_8_2                                             : 7;        // [msb:lsb] = [8:2], val = 0
        unsigned char r_gck08_init_bits_0                                                    : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0069h xdata g_rw_ctrl_0069h;    // Absolute Address = 6869h

union rw_ctrl_006Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck08_init_bits_7_1                                                  : 7;        // [msb:lsb] = [7:1], val = 0
        unsigned char r_gck08_st_v_line_bits_0                                               : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_006Ah xdata g_rw_ctrl_006Ah;    // Absolute Address = 686Ah

union rw_ctrl_006Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck08_st_v_line_bits_13_9                                            : 5;        // [msb:lsb] = [13:9], val = 0
        unsigned char r_gck08_end_v_line_bits_2_0                                            : 3;        // [msb:lsb] = [2:0], val = 4
    }bits;
};
extern volatile union rw_ctrl_006Ch xdata g_rw_ctrl_006Ch;    // Absolute Address = 686Ch

union rw_ctrl_006Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck08_end_v_line_bits_13_11                                          : 3;        // [msb:lsb] = [13:11], val = 0
        unsigned char r_gck08_sh_bits_4_0                                                    : 5;        // [msb:lsb] = [4:0], val = 8
    }bits;
};
extern volatile union rw_ctrl_006Eh xdata g_rw_ctrl_006Eh;    // Absolute Address = 686Eh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0071h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck08_w_bits_12_8                                                    : 5;        // [msb:lsb] = [12:8], val = 1
        unsigned char r_gck09_nor_mask_en                                                    : 1;        // val = 0
        unsigned char r_gck09_rev_set                                                        : 1;        // val = 0
        unsigned char r_gck09_pol                                                            : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0071h xdata g_rw_ctrl_0071h;    // Absolute Address = 6871h

union rw_ctrl_0072h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck09_end_pol                                                        : 1;        // val = 0
        unsigned char r_gck09_frm_stcut_en                                                   : 1;        // val = 1
        unsigned char r_gck09_dis                                                            : 1;        // val = 0
        unsigned char r_gck09_init_val                                                       : 1;        // val = 0
        unsigned char r_gck09_cycle_sel                                                      : 2;        // [msb:lsb] = [1:0], val = 2
        unsigned char r_gck09_f_sel_bits_1_0                                                 : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_0072h xdata g_rw_ctrl_0072h;    // Absolute Address = 6872h

union rw_ctrl_0073h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck09_f_sel_bits_2                                                   : 1;        // val = 0
        unsigned char r_gck09_frm_inv                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck09_cycle                                                          : 4;        // [msb:lsb] = [3:0], val = 5
        unsigned char r_gck09_r_no_bits_0                                                    : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0073h xdata g_rw_ctrl_0073h;    // Absolute Address = 6873h

union rw_ctrl_0074h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck09_r_no_bits_3_1                                                  : 3;        // [msb:lsb] = [3:1], val = 1
        unsigned char r_gck09_f_no                                                           : 4;        // [msb:lsb] = [3:0], val = 4
        unsigned char r_gck09_vblk_v_line_bits_0                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0074h xdata g_rw_ctrl_0074h;    // Absolute Address = 6874h

union rw_ctrl_0075h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck09_vblk_v_line_bits_3_1                                           : 3;        // [msb:lsb] = [3:1], val = 0
        unsigned char r_gck09_frm_cycle_bits_4_0                                             : 5;        // [msb:lsb] = [4:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_0075h xdata g_rw_ctrl_0075h;    // Absolute Address = 6875h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0076h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck09_frm_cycle_bits_8_5                                             : 4;        // [msb:lsb] = [8:5], val = 0
        unsigned char r_gck09_init_bits_3_0                                                  : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0076h xdata g_rw_ctrl_0076h;    // Absolute Address = 6876h

union rw_ctrl_0077h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck09_init_bits_7_4                                                  : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char r_gck09_st_v_line_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 3
    }bits;
};
extern volatile union rw_ctrl_0077h xdata g_rw_ctrl_0077h;    // Absolute Address = 6877h

union rw_ctrl_0079h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck09_st_v_line_bits_13_12                                           : 2;        // [msb:lsb] = [13:12], val = 0
        unsigned char r_gck09_end_v_line_bits_5_0                                            : 6;        // [msb:lsb] = [5:0], val = 63
    }bits;
};
extern volatile union rw_ctrl_0079h xdata g_rw_ctrl_0079h;    // Absolute Address = 6879h

union rw_ctrl_007Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck09_sh_bits_12_8                                                   : 5;        // [msb:lsb] = [12:8], val = 1
        unsigned char r_gck09_w_bits_2_0                                                     : 3;        // [msb:lsb] = [2:0], val = 4
    }bits;
};
extern volatile union rw_ctrl_007Ch xdata g_rw_ctrl_007Ch;    // Absolute Address = 687Ch

union rw_ctrl_007Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck09_w_bits_12_11                                                   : 2;        // [msb:lsb] = [12:11], val = 0
        unsigned char r_gck00_gate_mask_en                                                   : 1;        // val = 0
        unsigned char r_gck01_gate_mask_en                                                   : 1;        // val = 0
        unsigned char r_yoe_mask_type_sel                                                    : 1;        // val = 0
        unsigned char r_tp_muxs1_en                                                          : 1;        // val = 0
        unsigned char r_tp_mux_en                                                            : 1;        // val = 0
        unsigned char r_tp_sen                                                               : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_007Eh xdata g_rw_ctrl_007Eh;    // Absolute Address = 687Eh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0080h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck00_w_cs_en                                                        : 1;        // val = 0
        unsigned char r_gck01_w_cs_en                                                        : 1;        // val = 0
        unsigned char r_gck02_w_cs_en                                                        : 1;        // val = 0
        unsigned char r_gck03_w_cs_en                                                        : 1;        // val = 0
        unsigned char r_gck04_w_cs_en                                                        : 1;        // val = 0
        unsigned char r_gck05_w_cs_en                                                        : 1;        // val = 0
        unsigned char r_gck06_w_cs_en                                                        : 1;        // val = 0
        unsigned char r_gck07_w_cs_en                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0080h xdata g_rw_ctrl_0080h;    // Absolute Address = 6880h

union rw_ctrl_0081h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck08_w_cs_en                                                        : 1;        // val = 0
        unsigned char r_gck09_w_cs_en                                                        : 1;        // val = 0
        unsigned char r_gck00_sq_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_gck01_sq_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0081h xdata g_rw_ctrl_0081h;    // Absolute Address = 6881h

union rw_ctrl_0082h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck02_sq_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_gck03_sq_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_gck04_sq_sel_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0082h xdata g_rw_ctrl_0082h;    // Absolute Address = 6882h

union rw_ctrl_0083h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck04_sq_sel_bits_2                                                  : 1;        // val = 0
        unsigned char r_gck05_sq_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_gck06_sq_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_gck07_sq_sel_bits_0                                                  : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0083h xdata g_rw_ctrl_0083h;    // Absolute Address = 6883h

union rw_ctrl_0084h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck07_sq_sel_bits_2_1                                                : 2;        // [msb:lsb] = [2:1], val = 0
        unsigned char r_gck08_sq_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_gck09_sq_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0084h xdata g_rw_ctrl_0084h;    // Absolute Address = 6884h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0086h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck_vblk_end_v_line_bits_11_8                                        : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gck00_vblk_fall_sel                                                  : 1;        // val = 0
        unsigned char r_gck01_vblk_fall_sel                                                  : 1;        // val = 0
        unsigned char r_gck02_vblk_fall_sel                                                  : 1;        // val = 0
        unsigned char r_gck03_vblk_fall_sel                                                  : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0086h xdata g_rw_ctrl_0086h;    // Absolute Address = 6886h

union rw_ctrl_0087h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck04_vblk_fall_sel                                                  : 1;        // val = 0
        unsigned char r_gck05_vblk_fall_sel                                                  : 1;        // val = 0
        unsigned char r_gck06_vblk_fall_sel                                                  : 1;        // val = 0
        unsigned char r_gck07_vblk_fall_sel                                                  : 1;        // val = 0
        unsigned char r_gck08_vblk_fall_sel                                                  : 1;        // val = 0
        unsigned char r_gck09_vblk_fall_sel                                                  : 1;        // val = 0
        unsigned char r_mask_fnum_bits_1_0                                                   : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_ctrl_0087h xdata g_rw_ctrl_0087h;    // Absolute Address = 6887h

union rw_ctrl_0088h
{
    unsigned char byte;
    struct
    {
        unsigned char r_mask_fnum_bits_2                                                     : 1;        // val = 0
        unsigned char r_mask_by_vs                                                           : 1;        // val = 0
        unsigned char r_gck00_msk_refa_sel                                                   : 1;        // val = 0
        unsigned char r_gck01_msk_refa_sel                                                   : 1;        // val = 0
        unsigned char r_gck02_msk_refa_sel                                                   : 1;        // val = 0
        unsigned char r_gck03_msk_refa_sel                                                   : 1;        // val = 0
        unsigned char r_gck04_msk_refa_sel                                                   : 1;        // val = 0
        unsigned char r_gck05_msk_refa_sel                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0088h xdata g_rw_ctrl_0088h;    // Absolute Address = 6888h

union rw_ctrl_0089h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck06_msk_refa_sel                                                   : 1;        // val = 0
        unsigned char r_gck07_msk_refa_sel                                                   : 1;        // val = 0
        unsigned char r_gck08_msk_refa_sel                                                   : 1;        // val = 0
        unsigned char r_gck09_msk_refa_sel                                                   : 1;        // val = 0
        unsigned char r_gck00_esdmask_en                                                     : 1;        // val = 0
        unsigned char r_gck01_esdmask_en                                                     : 1;        // val = 0
        unsigned char r_gck02_esdmask_en                                                     : 1;        // val = 0
        unsigned char r_gck03_esdmask_en                                                     : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0089h xdata g_rw_ctrl_0089h;    // Absolute Address = 6889h

union rw_ctrl_008Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck04_esdmask_en                                                     : 1;        // val = 0
        unsigned char r_gck05_esdmask_en                                                     : 1;        // val = 0
        unsigned char r_gck06_esdmask_en                                                     : 1;        // val = 0
        unsigned char r_gck07_esdmask_en                                                     : 1;        // val = 0
        unsigned char r_gck08_esdmask_en                                                     : 1;        // val = 0
        unsigned char r_gck09_esdmask_en                                                     : 1;        // val = 0
        unsigned char r_gck01_log_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_008Ah xdata g_rw_ctrl_008Ah;    // Absolute Address = 688Ah

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_008Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck45_log_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck89_log_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gckcd_log_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck01_log_inv                                                        : 1;        // val = 0
        unsigned char r_gck45_log_inv                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_008Bh xdata g_rw_ctrl_008Bh;    // Absolute Address = 688Bh

union rw_ctrl_008Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck89_log_inv                                                        : 1;        // val = 0
        unsigned char r_gckcd_log_inv                                                        : 1;        // val = 0
        unsigned char r_gck23_log_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck67_log_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gckab_log_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_008Ch xdata g_rw_ctrl_008Ch;    // Absolute Address = 688Ch

union rw_ctrl_008Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gckef_log_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck23_log_inv                                                        : 1;        // val = 0
        unsigned char r_gck67_log_inv                                                        : 1;        // val = 0
        unsigned char r_gckab_log_inv                                                        : 1;        // val = 0
        unsigned char r_gckef_log_inv                                                        : 1;        // val = 0
        unsigned char r_gck00_sig_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_008Dh xdata g_rw_ctrl_008Dh;    // Absolute Address = 688Dh

union rw_ctrl_008Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck01_sig_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck02_sig_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck03_sig_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck04_sig_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_008Eh xdata g_rw_ctrl_008Eh;    // Absolute Address = 688Eh

union rw_ctrl_008Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck05_sig_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck06_sig_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck07_sig_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck08_sig_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_008Fh xdata g_rw_ctrl_008Fh;    // Absolute Address = 688Fh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0090h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck09_sig_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck0a_sig_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck0b_sig_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck0c_sig_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0090h xdata g_rw_ctrl_0090h;    // Absolute Address = 6890h

union rw_ctrl_0091h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0d_sig_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck0e_sig_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck0f_sig_sel                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck00_pin_sel_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_0091h xdata g_rw_ctrl_0091h;    // Absolute Address = 6891h

union rw_ctrl_0092h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck00_pin_sel_bits_5_2                                               : 4;        // [msb:lsb] = [5:2], val = 1
        unsigned char r_gck01_pin_sel_bits_3_0                                               : 4;        // [msb:lsb] = [3:0], val = 6
    }bits;
};
extern volatile union rw_ctrl_0092h xdata g_rw_ctrl_0092h;    // Absolute Address = 6892h

union rw_ctrl_0093h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck01_pin_sel_bits_5_4                                               : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_gck02_pin_sel                                                        : 6;        // [msb:lsb] = [5:0], val = 7
    }bits;
};
extern volatile union rw_ctrl_0093h xdata g_rw_ctrl_0093h;    // Absolute Address = 6893h

union rw_ctrl_0094h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck03_pin_sel                                                        : 6;        // [msb:lsb] = [5:0], val = 8
        unsigned char r_gck04_pin_sel_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0094h xdata g_rw_ctrl_0094h;    // Absolute Address = 6894h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0095h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck04_pin_sel_bits_5_2                                               : 4;        // [msb:lsb] = [5:2], val = 1
        unsigned char r_gck05_pin_sel_bits_3_0                                               : 4;        // [msb:lsb] = [3:0], val = 3
    }bits;
};
extern volatile union rw_ctrl_0095h xdata g_rw_ctrl_0095h;    // Absolute Address = 6895h

union rw_ctrl_0096h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck05_pin_sel_bits_5_4                                               : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_gck06_pin_sel                                                        : 6;        // [msb:lsb] = [5:0], val = 15
    }bits;
};
extern volatile union rw_ctrl_0096h xdata g_rw_ctrl_0096h;    // Absolute Address = 6896h

union rw_ctrl_0097h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck07_pin_sel                                                        : 6;        // [msb:lsb] = [5:0], val = 15
        unsigned char r_gck08_pin_sel_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_ctrl_0097h xdata g_rw_ctrl_0097h;    // Absolute Address = 6897h

union rw_ctrl_0098h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck08_pin_sel_bits_5_2                                               : 4;        // [msb:lsb] = [5:2], val = 3
        unsigned char r_gck09_pin_sel_bits_3_0                                               : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_ctrl_0098h xdata g_rw_ctrl_0098h;    // Absolute Address = 6898h

union rw_ctrl_0099h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck09_pin_sel_bits_5_4                                               : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_gck0a_pin_sel                                                        : 6;        // [msb:lsb] = [5:0], val = 15
    }bits;
};
extern volatile union rw_ctrl_0099h xdata g_rw_ctrl_0099h;    // Absolute Address = 6899h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_009Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0b_pin_sel                                                        : 6;        // [msb:lsb] = [5:0], val = 15
        unsigned char r_gck0c_pin_sel_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_ctrl_009Ah xdata g_rw_ctrl_009Ah;    // Absolute Address = 689Ah

union rw_ctrl_009Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0c_pin_sel_bits_5_2                                               : 4;        // [msb:lsb] = [5:2], val = 3
        unsigned char r_gck0d_pin_sel_bits_3_0                                               : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_ctrl_009Bh xdata g_rw_ctrl_009Bh;    // Absolute Address = 689Bh

union rw_ctrl_009Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0d_pin_sel_bits_5_4                                               : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_gck0e_pin_sel                                                        : 6;        // [msb:lsb] = [5:0], val = 15
    }bits;
};
extern volatile union rw_ctrl_009Ch xdata g_rw_ctrl_009Ch;    // Absolute Address = 689Ch

union rw_ctrl_009Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0f_pin_sel                                                        : 6;        // [msb:lsb] = [5:0], val = 15
        unsigned char r_gckmd0_sel                                                           : 1;        // val = 0
        unsigned char r_gckmd1_sel                                                           : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_009Dh xdata g_rw_ctrl_009Dh;    // Absolute Address = 689Dh

union rw_ctrl_009Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_ltps_mux2                                                            : 1;        // val = 1
        unsigned char r_ltps_mux3                                                            : 1;        // val = 0
        unsigned char r_ltps_rotate                                                          : 1;        // val = 0
        unsigned char r_ltps_rotate_sel                                                      : 1;        // val = 0
        unsigned char r_28s_gck_en                                                           : 1;        // val = 0
        unsigned char r_28s_gck_sh1_bits_2_0                                                 : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_009Eh xdata g_rw_ctrl_009Eh;    // Absolute Address = 689Eh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_00A0h
{
    unsigned char byte;
    struct
    {
        unsigned char r_28s_gck_sh1_bits_12_11                                               : 2;        // [msb:lsb] = [12:11], val = 0
        unsigned char r_28s_gck_sh2_bits_5_0                                                 : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00A0h xdata g_rw_ctrl_00A0h;    // Absolute Address = 68A0h

union rw_ctrl_00A1h
{
    unsigned char byte;
    struct
    {
        unsigned char r_28s_gck_sh2_bits_12_6                                                : 7;        // [msb:lsb] = [12:6], val = 0
        unsigned char r_28s_gck_sh3_bits_0                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_00A1h xdata g_rw_ctrl_00A1h;    // Absolute Address = 68A1h

union rw_ctrl_00A3h
{
    unsigned char byte;
    struct
    {
        unsigned char r_28s_gck_sh3_bits_12_9                                                : 4;        // [msb:lsb] = [12:9], val = 0
        unsigned char r_28s_gck_w1_bits_3_0                                                  : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00A3h xdata g_rw_ctrl_00A3h;    // Absolute Address = 68A3h

union rw_ctrl_00A5h
{
    unsigned char byte;
    struct
    {
        unsigned char r_28s_gck_w1_bits_12                                                   : 1;        // val = 0
        unsigned char r_28s_gck_w2_bits_6_0                                                  : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00A5h xdata g_rw_ctrl_00A5h;    // Absolute Address = 68A5h

union rw_ctrl_00A6h
{
    unsigned char byte;
    struct
    {
        unsigned char r_28s_gck_w2_bits_12_7                                                 : 6;        // [msb:lsb] = [12:7], val = 0
        unsigned char r_28s_gck_w3_bits_1_0                                                  : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00A6h xdata g_rw_ctrl_00A6h;    // Absolute Address = 68A6h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_00A8h
{
    unsigned char byte;
    struct
    {
        unsigned char r_28s_gck_w3_bits_12_10                                                : 3;        // [msb:lsb] = [12:10], val = 0
        unsigned char r_28s_ld_en                                                            : 1;        // val = 0
        unsigned char r_28s_ld_sh1_bits_3_0                                                  : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00A8h xdata g_rw_ctrl_00A8h;    // Absolute Address = 68A8h

union rw_ctrl_00AAh
{
    unsigned char byte;
    struct
    {
        unsigned char r_28s_ld_sh1_bits_12                                                   : 1;        // val = 0
        unsigned char r_28s_ld_sh2_bits_6_0                                                  : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00AAh xdata g_rw_ctrl_00AAh;    // Absolute Address = 68AAh

union rw_ctrl_00ABh
{
    unsigned char byte;
    struct
    {
        unsigned char r_28s_ld_sh2_bits_12_7                                                 : 6;        // [msb:lsb] = [12:7], val = 0
        unsigned char r_28s_ld_sh3_bits_1_0                                                  : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00ABh xdata g_rw_ctrl_00ABh;    // Absolute Address = 68ABh

union rw_ctrl_00ADh
{
    unsigned char byte;
    struct
    {
        unsigned char r_28s_ld_sh3_bits_12_10                                                : 3;        // [msb:lsb] = [12:10], val = 0
        unsigned char r_28s_ld_w1_bits_4_0                                                   : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00ADh xdata g_rw_ctrl_00ADh;    // Absolute Address = 68ADh

union rw_ctrl_00B0h
{
    unsigned char byte;
    struct
    {
        unsigned char r_28s_ld_w2_bits_12_8                                                  : 5;        // [msb:lsb] = [12:8], val = 0
        unsigned char r_28s_ld_w3_bits_2_0                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00B0h xdata g_rw_ctrl_00B0h;    // Absolute Address = 68B0h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_00B2h
{
    unsigned char byte;
    struct
    {
        unsigned char r_28s_ld_w3_bits_12_11                                                 : 2;        // [msb:lsb] = [12:11], val = 0
        unsigned char r_gck02_gate_mask_en                                                   : 1;        // val = 0
        unsigned char r_gck03_gate_mask_en                                                   : 1;        // val = 0
        unsigned char r_gck04_gate_mask_en                                                   : 1;        // val = 0
        unsigned char r_gck05_gate_mask_en                                                   : 1;        // val = 0
        unsigned char r_gck06_gate_mask_en                                                   : 1;        // val = 0
        unsigned char r_gck07_gate_mask_en                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_00B2h xdata g_rw_ctrl_00B2h;    // Absolute Address = 68B2h

union rw_ctrl_00B3h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck08_gate_mask_en                                                   : 1;        // val = 0
        unsigned char r_gck09_gate_mask_en                                                   : 1;        // val = 0
        unsigned char r_gck0a_gate_mask_en                                                   : 1;        // val = 0
        unsigned char r_gck0b_gate_mask_en                                                   : 1;        // val = 0
        unsigned char r_gck0c_gate_mask_en                                                   : 1;        // val = 0
        unsigned char r_gck0d_gate_mask_en                                                   : 1;        // val = 0
        unsigned char r_gck0e_gate_mask_en                                                   : 1;        // val = 0
        unsigned char r_gck0f_gate_mask_en                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_00B3h xdata g_rw_ctrl_00B3h;    // Absolute Address = 68B3h

union rw_ctrl_00B4h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0a_nor_mask_en                                                    : 1;        // val = 0
        unsigned char r_gck0a_rev_set                                                        : 1;        // val = 0
        unsigned char r_gck0a_pol                                                            : 1;        // val = 0
        unsigned char r_gck0a_end_pol                                                        : 1;        // val = 0
        unsigned char r_gck0a_frm_stcut_en                                                   : 1;        // val = 1
        unsigned char r_gck0a_dis                                                            : 1;        // val = 0
        unsigned char r_gck0a_init_val                                                       : 1;        // val = 0
        unsigned char r_gck0a_cycle_sel_bits_0                                               : 1;        // val = 1
    }bits;
};
extern volatile union rw_ctrl_00B4h xdata g_rw_ctrl_00B4h;    // Absolute Address = 68B4h

union rw_ctrl_00B5h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0a_cycle_sel_bits_1                                               : 1;        // val = 0
        unsigned char r_gck0a_f_sel                                                          : 3;        // [msb:lsb] = [2:0], val = 1
        unsigned char r_gck0a_frm_inv                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck0a_cycle_bits_1_0                                                 : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_ctrl_00B5h xdata g_rw_ctrl_00B5h;    // Absolute Address = 68B5h

union rw_ctrl_00B6h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0a_cycle_bits_3_2                                                 : 2;        // [msb:lsb] = [3:2], val = 0
        unsigned char r_gck0a_r_no                                                           : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck0a_f_no_bits_1_0                                                  : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_ctrl_00B6h xdata g_rw_ctrl_00B6h;    // Absolute Address = 68B6h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_00B7h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0a_f_no_bits_3_2                                                  : 2;        // [msb:lsb] = [3:2], val = 0
        unsigned char r_gck0a_vblk_v_line                                                    : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck0a_frm_cycle_bits_1_0                                             : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_00B7h xdata g_rw_ctrl_00B7h;    // Absolute Address = 68B7h

union rw_ctrl_00B8h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0a_frm_cycle_bits_8_2                                             : 7;        // [msb:lsb] = [8:2], val = 0
        unsigned char r_gck0a_init_bits_0                                                    : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_00B8h xdata g_rw_ctrl_00B8h;    // Absolute Address = 68B8h

union rw_ctrl_00B9h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0a_init_bits_7_1                                                  : 7;        // [msb:lsb] = [7:1], val = 0
        unsigned char r_gck0a_st_v_line_bits_0                                               : 1;        // val = 1
    }bits;
};
extern volatile union rw_ctrl_00B9h xdata g_rw_ctrl_00B9h;    // Absolute Address = 68B9h

union rw_ctrl_00BBh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0a_st_v_line_bits_13_9                                            : 5;        // [msb:lsb] = [13:9], val = 3
        unsigned char r_gck0a_end_v_line_bits_2_0                                            : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_ctrl_00BBh xdata g_rw_ctrl_00BBh;    // Absolute Address = 68BBh

union rw_ctrl_00BDh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0a_end_v_line_bits_13_11                                          : 3;        // [msb:lsb] = [13:11], val = 1
        unsigned char r_gck0a_sh_bits_4_0                                                    : 5;        // [msb:lsb] = [4:0], val = 8
    }bits;
};
extern volatile union rw_ctrl_00BDh xdata g_rw_ctrl_00BDh;    // Absolute Address = 68BDh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_00C0h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0a_w_bits_12_8                                                    : 5;        // [msb:lsb] = [12:8], val = 1
        unsigned char r_gck0b_nor_mask_en                                                    : 1;        // val = 0
        unsigned char r_gck0b_rev_set                                                        : 1;        // val = 0
        unsigned char r_gck0b_pol                                                            : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_00C0h xdata g_rw_ctrl_00C0h;    // Absolute Address = 68C0h

union rw_ctrl_00C1h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0b_end_pol                                                        : 1;        // val = 0
        unsigned char r_gck0b_frm_stcut_en                                                   : 1;        // val = 0
        unsigned char r_gck0b_dis                                                            : 1;        // val = 0
        unsigned char r_gck0b_init_val                                                       : 1;        // val = 0
        unsigned char r_gck0b_cycle_sel                                                      : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck0b_f_sel_bits_1_0                                                 : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00C1h xdata g_rw_ctrl_00C1h;    // Absolute Address = 68C1h

union rw_ctrl_00C2h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0b_f_sel_bits_2                                                   : 1;        // val = 0
        unsigned char r_gck0b_frm_inv                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck0b_cycle                                                          : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck0b_r_no_bits_0                                                    : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_00C2h xdata g_rw_ctrl_00C2h;    // Absolute Address = 68C2h

union rw_ctrl_00C3h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0b_r_no_bits_3_1                                                  : 3;        // [msb:lsb] = [3:1], val = 0
        unsigned char r_gck0b_f_no                                                           : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck0b_vblk_v_line_bits_0                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_00C3h xdata g_rw_ctrl_00C3h;    // Absolute Address = 68C3h

union rw_ctrl_00C4h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0b_vblk_v_line_bits_3_1                                           : 3;        // [msb:lsb] = [3:1], val = 0
        unsigned char r_gck0b_frm_cycle_bits_4_0                                             : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00C4h xdata g_rw_ctrl_00C4h;    // Absolute Address = 68C4h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_00C5h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0b_frm_cycle_bits_8_5                                             : 4;        // [msb:lsb] = [8:5], val = 0
        unsigned char r_gck0b_init_bits_3_0                                                  : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00C5h xdata g_rw_ctrl_00C5h;    // Absolute Address = 68C5h

union rw_ctrl_00C6h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0b_init_bits_7_4                                                  : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char r_gck0b_st_v_line_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00C6h xdata g_rw_ctrl_00C6h;    // Absolute Address = 68C6h

union rw_ctrl_00C8h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0b_st_v_line_bits_13_12                                           : 2;        // [msb:lsb] = [13:12], val = 0
        unsigned char r_gck0b_end_v_line_bits_5_0                                            : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00C8h xdata g_rw_ctrl_00C8h;    // Absolute Address = 68C8h

union rw_ctrl_00CBh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0b_sh_bits_12_8                                                   : 5;        // [msb:lsb] = [12:8], val = 0
        unsigned char r_gck0b_w_bits_2_0                                                     : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00CBh xdata g_rw_ctrl_00CBh;    // Absolute Address = 68CBh

union rw_ctrl_00CDh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0b_w_bits_12_11                                                   : 2;        // [msb:lsb] = [12:11], val = 0
        unsigned char r_gck0c_nor_mask_en                                                    : 1;        // val = 0
        unsigned char r_gck0c_rev_set                                                        : 1;        // val = 0
        unsigned char r_gck0c_pol                                                            : 1;        // val = 0
        unsigned char r_gck0c_end_pol                                                        : 1;        // val = 0
        unsigned char r_gck0c_frm_stcut_en                                                   : 1;        // val = 0
        unsigned char r_gck0c_dis                                                            : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_00CDh xdata g_rw_ctrl_00CDh;    // Absolute Address = 68CDh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_00CEh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0c_init_val                                                       : 1;        // val = 0
        unsigned char r_gck0c_cycle_sel                                                      : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck0c_f_sel                                                          : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_gck0c_frm_inv                                                        : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00CEh xdata g_rw_ctrl_00CEh;    // Absolute Address = 68CEh

union rw_ctrl_00CFh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0c_cycle                                                          : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck0c_r_no                                                           : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00CFh xdata g_rw_ctrl_00CFh;    // Absolute Address = 68CFh

union rw_ctrl_00D0h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0c_f_no                                                           : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck0c_vblk_v_line                                                    : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00D0h xdata g_rw_ctrl_00D0h;    // Absolute Address = 68D0h

union rw_ctrl_00D2h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0c_frm_cycle_bits_8                                               : 1;        // val = 0
        unsigned char r_gck0c_init_bits_6_0                                                  : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00D2h xdata g_rw_ctrl_00D2h;    // Absolute Address = 68D2h

union rw_ctrl_00D3h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0c_init_bits_7                                                    : 1;        // val = 0
        unsigned char r_gck0c_st_v_line_bits_6_0                                             : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00D3h xdata g_rw_ctrl_00D3h;    // Absolute Address = 68D3h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_00D4h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0c_st_v_line_bits_13_7                                            : 7;        // [msb:lsb] = [13:7], val = 0
        unsigned char r_gck0c_end_v_line_bits_0                                              : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_00D4h xdata g_rw_ctrl_00D4h;    // Absolute Address = 68D4h

union rw_ctrl_00D6h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0c_end_v_line_bits_13_9                                           : 5;        // [msb:lsb] = [13:9], val = 0
        unsigned char r_gck0c_sh_bits_2_0                                                    : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00D6h xdata g_rw_ctrl_00D6h;    // Absolute Address = 68D6h

union rw_ctrl_00D8h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0c_sh_bits_12_11                                                  : 2;        // [msb:lsb] = [12:11], val = 0
        unsigned char r_gck0c_w_bits_5_0                                                     : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00D8h xdata g_rw_ctrl_00D8h;    // Absolute Address = 68D8h

union rw_ctrl_00D9h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0c_w_bits_12_6                                                    : 7;        // [msb:lsb] = [12:6], val = 0
        unsigned char r_gck0d_nor_mask_en                                                    : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_00D9h xdata g_rw_ctrl_00D9h;    // Absolute Address = 68D9h

union rw_ctrl_00DAh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0d_rev_set                                                        : 1;        // val = 0
        unsigned char r_gck0d_pol                                                            : 1;        // val = 0
        unsigned char r_gck0d_end_pol                                                        : 1;        // val = 0
        unsigned char r_gck0d_frm_stcut_en                                                   : 1;        // val = 0
        unsigned char r_gck0d_dis                                                            : 1;        // val = 0
        unsigned char r_gck0d_init_val                                                       : 1;        // val = 0
        unsigned char r_gck0d_cycle_sel                                                      : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00DAh xdata g_rw_ctrl_00DAh;    // Absolute Address = 68DAh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_00DBh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0d_f_sel                                                          : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_gck0d_frm_inv                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck0d_cycle_bits_2_0                                                 : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00DBh xdata g_rw_ctrl_00DBh;    // Absolute Address = 68DBh

union rw_ctrl_00DCh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0d_cycle_bits_3                                                   : 1;        // val = 0
        unsigned char r_gck0d_r_no                                                           : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck0d_f_no_bits_2_0                                                  : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00DCh xdata g_rw_ctrl_00DCh;    // Absolute Address = 68DCh

union rw_ctrl_00DDh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0d_f_no_bits_3                                                    : 1;        // val = 0
        unsigned char r_gck0d_vblk_v_line                                                    : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck0d_frm_cycle_bits_2_0                                             : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00DDh xdata g_rw_ctrl_00DDh;    // Absolute Address = 68DDh

union rw_ctrl_00DEh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0d_frm_cycle_bits_8_3                                             : 6;        // [msb:lsb] = [8:3], val = 0
        unsigned char r_gck0d_init_bits_1_0                                                  : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00DEh xdata g_rw_ctrl_00DEh;    // Absolute Address = 68DEh

union rw_ctrl_00DFh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0d_init_bits_7_2                                                  : 6;        // [msb:lsb] = [7:2], val = 0
        unsigned char r_gck0d_st_v_line_bits_1_0                                             : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00DFh xdata g_rw_ctrl_00DFh;    // Absolute Address = 68DFh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_00E1h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0d_st_v_line_bits_13_10                                           : 4;        // [msb:lsb] = [13:10], val = 0
        unsigned char r_gck0d_end_v_line_bits_3_0                                            : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00E1h xdata g_rw_ctrl_00E1h;    // Absolute Address = 68E1h

union rw_ctrl_00E3h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0d_end_v_line_bits_13_12                                          : 2;        // [msb:lsb] = [13:12], val = 0
        unsigned char r_gck0d_sh_bits_5_0                                                    : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00E3h xdata g_rw_ctrl_00E3h;    // Absolute Address = 68E3h

union rw_ctrl_00E4h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0d_sh_bits_12_6                                                   : 7;        // [msb:lsb] = [12:6], val = 0
        unsigned char r_gck0d_w_bits_0                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_00E4h xdata g_rw_ctrl_00E4h;    // Absolute Address = 68E4h

union rw_ctrl_00E6h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0d_w_bits_12_9                                                    : 4;        // [msb:lsb] = [12:9], val = 0
        unsigned char r_gck0e_nor_mask_en                                                    : 1;        // val = 0
        unsigned char r_gck0e_rev_set                                                        : 1;        // val = 0
        unsigned char r_gck0e_pol                                                            : 1;        // val = 0
        unsigned char r_gck0e_end_pol                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_00E6h xdata g_rw_ctrl_00E6h;    // Absolute Address = 68E6h

union rw_ctrl_00E7h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0e_frm_stcut_en                                                   : 1;        // val = 0
        unsigned char r_gck0e_dis                                                            : 1;        // val = 0
        unsigned char r_gck0e_init_val                                                       : 1;        // val = 0
        unsigned char r_gck0e_cycle_sel                                                      : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck0e_f_sel                                                          : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00E7h xdata g_rw_ctrl_00E7h;    // Absolute Address = 68E7h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_00E8h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0e_frm_inv                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck0e_cycle                                                          : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck0e_r_no_bits_1_0                                                  : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00E8h xdata g_rw_ctrl_00E8h;    // Absolute Address = 68E8h

union rw_ctrl_00E9h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0e_r_no_bits_3_2                                                  : 2;        // [msb:lsb] = [3:2], val = 0
        unsigned char r_gck0e_f_no                                                           : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck0e_vblk_v_line_bits_1_0                                           : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00E9h xdata g_rw_ctrl_00E9h;    // Absolute Address = 68E9h

union rw_ctrl_00EAh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0e_vblk_v_line_bits_3_2                                           : 2;        // [msb:lsb] = [3:2], val = 0
        unsigned char r_gck0e_frm_cycle_bits_5_0                                             : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00EAh xdata g_rw_ctrl_00EAh;    // Absolute Address = 68EAh

union rw_ctrl_00EBh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0e_frm_cycle_bits_8_6                                             : 3;        // [msb:lsb] = [8:6], val = 0
        unsigned char r_gck0e_init_bits_4_0                                                  : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00EBh xdata g_rw_ctrl_00EBh;    // Absolute Address = 68EBh

union rw_ctrl_00ECh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0e_init_bits_7_5                                                  : 3;        // [msb:lsb] = [7:5], val = 0
        unsigned char r_gck0e_st_v_line_bits_4_0                                             : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00ECh xdata g_rw_ctrl_00ECh;    // Absolute Address = 68ECh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_00EEh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0e_st_v_line_bits_13                                              : 1;        // val = 0
        unsigned char r_gck0e_end_v_line_bits_6_0                                            : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00EEh xdata g_rw_ctrl_00EEh;    // Absolute Address = 68EEh

union rw_ctrl_00EFh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0e_end_v_line_bits_13_7                                           : 7;        // [msb:lsb] = [13:7], val = 0
        unsigned char r_gck0e_sh_bits_0                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_00EFh xdata g_rw_ctrl_00EFh;    // Absolute Address = 68EFh

union rw_ctrl_00F1h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0e_sh_bits_12_9                                                   : 4;        // [msb:lsb] = [12:9], val = 0
        unsigned char r_gck0e_w_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00F1h xdata g_rw_ctrl_00F1h;    // Absolute Address = 68F1h

union rw_ctrl_00F3h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0e_w_bits_12                                                      : 1;        // val = 0
        unsigned char r_gck0f_nor_mask_en                                                    : 1;        // val = 0
        unsigned char r_gck0f_rev_set                                                        : 1;        // val = 0
        unsigned char r_gck0f_pol                                                            : 1;        // val = 0
        unsigned char r_gck0f_end_pol                                                        : 1;        // val = 0
        unsigned char r_gck0f_frm_stcut_en                                                   : 1;        // val = 0
        unsigned char r_gck0f_dis                                                            : 1;        // val = 0
        unsigned char r_gck0f_init_val                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_00F3h xdata g_rw_ctrl_00F3h;    // Absolute Address = 68F3h

union rw_ctrl_00F4h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0f_cycle_sel                                                      : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck0f_f_sel                                                          : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_gck0f_frm_inv                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gck0f_cycle_bits_0                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_00F4h xdata g_rw_ctrl_00F4h;    // Absolute Address = 68F4h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_00F5h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0f_cycle_bits_3_1                                                 : 3;        // [msb:lsb] = [3:1], val = 0
        unsigned char r_gck0f_r_no                                                           : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck0f_f_no_bits_0                                                    : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_00F5h xdata g_rw_ctrl_00F5h;    // Absolute Address = 68F5h

union rw_ctrl_00F6h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0f_f_no_bits_3_1                                                  : 3;        // [msb:lsb] = [3:1], val = 0
        unsigned char r_gck0f_vblk_v_line                                                    : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gck0f_frm_cycle_bits_0                                               : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_00F6h xdata g_rw_ctrl_00F6h;    // Absolute Address = 68F6h

union rw_ctrl_00FAh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0f_st_v_line_bits_13_8                                            : 6;        // [msb:lsb] = [13:8], val = 0
        unsigned char r_gck0f_end_v_line_bits_1_0                                            : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00FAh xdata g_rw_ctrl_00FAh;    // Absolute Address = 68FAh

union rw_ctrl_00FCh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0f_end_v_line_bits_13_10                                          : 4;        // [msb:lsb] = [13:10], val = 0
        unsigned char r_gck0f_sh_bits_3_0                                                    : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00FCh xdata g_rw_ctrl_00FCh;    // Absolute Address = 68FCh

union rw_ctrl_00FEh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0f_sh_bits_12                                                     : 1;        // val = 0
        unsigned char r_gck0f_w_bits_6_0                                                     : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_00FEh xdata g_rw_ctrl_00FEh;    // Absolute Address = 68FEh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_00FFh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0f_w_bits_12_7                                                    : 6;        // [msb:lsb] = [12:7], val = 0
        unsigned char r_rs_gck00_pin_sel_bits_1_0                                            : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_ctrl_00FFh xdata g_rw_ctrl_00FFh;    // Absolute Address = 68FFh

union rw_ctrl_0100h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rs_gck00_pin_sel_bits_5_2                                            : 4;        // [msb:lsb] = [5:2], val = 3
        unsigned char r_rs_gck01_pin_sel_bits_3_0                                            : 4;        // [msb:lsb] = [3:0], val = 14
    }bits;
};
extern volatile union rw_ctrl_0100h xdata g_rw_ctrl_0100h;    // Absolute Address = 6900h

union rw_ctrl_0101h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rs_gck01_pin_sel_bits_5_4                                            : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_rs_gck02_pin_sel                                                     : 6;        // [msb:lsb] = [5:0], val = 13
    }bits;
};
extern volatile union rw_ctrl_0101h xdata g_rw_ctrl_0101h;    // Absolute Address = 6901h

union rw_ctrl_0102h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rs_gck03_pin_sel                                                     : 6;        // [msb:lsb] = [5:0], val = 12
        unsigned char r_rs_gck04_pin_sel_bits_1_0                                            : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_ctrl_0102h xdata g_rw_ctrl_0102h;    // Absolute Address = 6902h

union rw_ctrl_0103h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rs_gck04_pin_sel_bits_5_2                                            : 4;        // [msb:lsb] = [5:2], val = 2
        unsigned char r_rs_gck05_pin_sel_bits_3_0                                            : 4;        // [msb:lsb] = [3:0], val = 10
    }bits;
};
extern volatile union rw_ctrl_0103h xdata g_rw_ctrl_0103h;    // Absolute Address = 6903h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0104h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rs_gck05_pin_sel_bits_5_4                                            : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_rs_gck06_pin_sel                                                     : 6;        // [msb:lsb] = [5:0], val = 9
    }bits;
};
extern volatile union rw_ctrl_0104h xdata g_rw_ctrl_0104h;    // Absolute Address = 6904h

union rw_ctrl_0105h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rs_gck07_pin_sel                                                     : 6;        // [msb:lsb] = [5:0], val = 8
        unsigned char r_rs_gck08_pin_sel_bits_1_0                                            : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_ctrl_0105h xdata g_rw_ctrl_0105h;    // Absolute Address = 6905h

union rw_ctrl_0106h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rs_gck08_pin_sel_bits_5_2                                            : 4;        // [msb:lsb] = [5:2], val = 1
        unsigned char r_rs_gck09_pin_sel_bits_3_0                                            : 4;        // [msb:lsb] = [3:0], val = 6
    }bits;
};
extern volatile union rw_ctrl_0106h xdata g_rw_ctrl_0106h;    // Absolute Address = 6906h

union rw_ctrl_0107h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rs_gck09_pin_sel_bits_5_4                                            : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_rs_gck0a_pin_sel                                                     : 6;        // [msb:lsb] = [5:0], val = 5
    }bits;
};
extern volatile union rw_ctrl_0107h xdata g_rw_ctrl_0107h;    // Absolute Address = 6907h

union rw_ctrl_0108h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rs_gck0b_pin_sel                                                     : 6;        // [msb:lsb] = [5:0], val = 4
        unsigned char r_rs_gck0c_pin_sel_bits_1_0                                            : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_ctrl_0108h xdata g_rw_ctrl_0108h;    // Absolute Address = 6908h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0109h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rs_gck0c_pin_sel_bits_5_2                                            : 4;        // [msb:lsb] = [5:2], val = 0
        unsigned char r_rs_gck0d_pin_sel_bits_3_0                                            : 4;        // [msb:lsb] = [3:0], val = 2
    }bits;
};
extern volatile union rw_ctrl_0109h xdata g_rw_ctrl_0109h;    // Absolute Address = 6909h

union rw_ctrl_010Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_rs_gck0d_pin_sel_bits_5_4                                            : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_rs_gck0e_pin_sel                                                     : 6;        // [msb:lsb] = [5:0], val = 1
    }bits;
};
extern volatile union rw_ctrl_010Ah xdata g_rw_ctrl_010Ah;    // Absolute Address = 690Ah

union rw_ctrl_010Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_rs_gck0f_pin_sel                                                     : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_gck0a_w_cs_en                                                        : 1;        // val = 0
        unsigned char r_gck0b_w_cs_en                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_010Bh xdata g_rw_ctrl_010Bh;    // Absolute Address = 690Bh

union rw_ctrl_010Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0c_w_cs_en                                                        : 1;        // val = 0
        unsigned char r_gck0d_w_cs_en                                                        : 1;        // val = 0
        unsigned char r_gck0e_w_cs_en                                                        : 1;        // val = 0
        unsigned char r_gck0f_w_cs_en                                                        : 1;        // val = 0
        unsigned char r_gck0a_sq_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_gck0b_sq_sel_bits_0                                                  : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_010Ch xdata g_rw_ctrl_010Ch;    // Absolute Address = 690Ch

union rw_ctrl_010Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0b_sq_sel_bits_2_1                                                : 2;        // [msb:lsb] = [2:1], val = 0
        unsigned char r_gck0c_sq_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_gck0d_sq_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_010Dh xdata g_rw_ctrl_010Dh;    // Absolute Address = 690Dh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_010Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0e_sq_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_gck0f_sq_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_gck0a_vblk_fall_sel                                                  : 1;        // val = 0
        unsigned char r_gck0b_vblk_fall_sel                                                  : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_010Eh xdata g_rw_ctrl_010Eh;    // Absolute Address = 690Eh

union rw_ctrl_010Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0c_vblk_fall_sel                                                  : 1;        // val = 0
        unsigned char r_gck0d_vblk_fall_sel                                                  : 1;        // val = 0
        unsigned char r_gck0e_vblk_fall_sel                                                  : 1;        // val = 0
        unsigned char r_gck0f_vblk_fall_sel                                                  : 1;        // val = 0
        unsigned char r_gck0a_msk_refa_sel                                                   : 1;        // val = 0
        unsigned char r_gck0b_msk_refa_sel                                                   : 1;        // val = 0
        unsigned char r_gck0c_msk_refa_sel                                                   : 1;        // val = 0
        unsigned char r_gck0d_msk_refa_sel                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_010Fh xdata g_rw_ctrl_010Fh;    // Absolute Address = 690Fh

union rw_ctrl_0110h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0e_msk_refa_sel                                                   : 1;        // val = 0
        unsigned char r_gck0f_msk_refa_sel                                                   : 1;        // val = 0
        unsigned char r_gck0a_esdmask_en                                                     : 1;        // val = 0
        unsigned char r_gck0b_esdmask_en                                                     : 1;        // val = 0
        unsigned char r_gck0c_esdmask_en                                                     : 1;        // val = 0
        unsigned char r_gck0d_esdmask_en                                                     : 1;        // val = 0
        unsigned char r_gck0e_esdmask_en                                                     : 1;        // val = 0
        unsigned char r_gck0f_esdmask_en                                                     : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0110h xdata g_rw_ctrl_0110h;    // Absolute Address = 6910h

union rw_ctrl_0111h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck00_28s_en                                                         : 1;        // val = 0
        unsigned char r_gck01_28s_en                                                         : 1;        // val = 0
        unsigned char r_gck02_28s_en                                                         : 1;        // val = 0
        unsigned char r_gck03_28s_en                                                         : 1;        // val = 0
        unsigned char r_gck04_28s_en                                                         : 1;        // val = 0
        unsigned char r_gck05_28s_en                                                         : 1;        // val = 0
        unsigned char r_gck06_28s_en                                                         : 1;        // val = 0
        unsigned char r_gck07_28s_en                                                         : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0111h xdata g_rw_ctrl_0111h;    // Absolute Address = 6911h

union rw_ctrl_0112h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck08_28s_en                                                         : 1;        // val = 0
        unsigned char r_gck09_28s_en                                                         : 1;        // val = 0
        unsigned char r_gck0a_28s_en                                                         : 1;        // val = 0
        unsigned char r_gck0b_28s_en                                                         : 1;        // val = 0
        unsigned char r_gck0c_28s_en                                                         : 1;        // val = 0
        unsigned char r_gck0d_28s_en                                                         : 1;        // val = 0
        unsigned char r_gck0e_28s_en                                                         : 1;        // val = 0
        unsigned char r_gck0f_28s_en                                                         : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0112h xdata g_rw_ctrl_0112h;    // Absolute Address = 6912h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0114h
{
    unsigned char byte;
    struct
    {
        unsigned char r_28s_w1_bits_12_8                                                     : 5;        // [msb:lsb] = [12:8], val = 0
        unsigned char r_28s_w2_bits_2_0                                                      : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0114h xdata g_rw_ctrl_0114h;    // Absolute Address = 6914h

union rw_ctrl_0116h
{
    unsigned char byte;
    struct
    {
        unsigned char r_28s_w2_bits_12_11                                                    : 2;        // [msb:lsb] = [12:11], val = 0
        unsigned char r_28s_w3_bits_5_0                                                      : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0116h xdata g_rw_ctrl_0116h;    // Absolute Address = 6916h

union rw_ctrl_0117h
{
    unsigned char byte;
    struct
    {
        unsigned char r_28s_w3_bits_12_6                                                     : 7;        // [msb:lsb] = [12:6], val = 0
        unsigned char r_gdly_en                                                              : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0117h xdata g_rw_ctrl_0117h;    // Absolute Address = 6917h

union rw_ctrl_0118h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gdly_inc_en                                                          : 1;        // val = 0
        unsigned char r_init_gdly_bits_6_0                                                   : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0118h xdata g_rw_ctrl_0118h;    // Absolute Address = 6918h

union rw_ctrl_0119h
{
    unsigned char byte;
    struct
    {
        unsigned char r_init_gdly_bits_11_7                                                  : 5;        // [msb:lsb] = [11:7], val = 0
        unsigned char r_bk1_step_bits_2_0                                                    : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0119h xdata g_rw_ctrl_0119h;    // Absolute Address = 6919h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_011Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk1_step_bits_5_3                                                    : 3;        // [msb:lsb] = [5:3], val = 0
        unsigned char r_bk1_rep_bits_4_0                                                     : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_011Ah xdata g_rw_ctrl_011Ah;    // Absolute Address = 691Ah

union rw_ctrl_011Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk1_rep_bits_5                                                       : 1;        // val = 0
        unsigned char r_bk2_step                                                             : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_bk2_rep_bits_0                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_011Bh xdata g_rw_ctrl_011Bh;    // Absolute Address = 691Bh

union rw_ctrl_011Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk2_rep_bits_5_1                                                     : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_bk3_step_bits_2_0                                                    : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_011Ch xdata g_rw_ctrl_011Ch;    // Absolute Address = 691Ch

union rw_ctrl_011Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk3_step_bits_5_3                                                    : 3;        // [msb:lsb] = [5:3], val = 0
        unsigned char r_bk3_rep_bits_4_0                                                     : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_011Dh xdata g_rw_ctrl_011Dh;    // Absolute Address = 691Dh

union rw_ctrl_011Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk3_rep_bits_5                                                       : 1;        // val = 0
        unsigned char r_bk4_step                                                             : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_bk4_rep_bits_0                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_011Eh xdata g_rw_ctrl_011Eh;    // Absolute Address = 691Eh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_011Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk4_rep_bits_5_1                                                     : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_bk5_step_bits_2_0                                                    : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_011Fh xdata g_rw_ctrl_011Fh;    // Absolute Address = 691Fh

union rw_ctrl_0120h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk5_step_bits_5_3                                                    : 3;        // [msb:lsb] = [5:3], val = 0
        unsigned char r_bk5_rep_bits_4_0                                                     : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0120h xdata g_rw_ctrl_0120h;    // Absolute Address = 6920h

union rw_ctrl_0121h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk5_rep_bits_5                                                       : 1;        // val = 0
        unsigned char r_bk6_step                                                             : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_bk6_rep_bits_0                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0121h xdata g_rw_ctrl_0121h;    // Absolute Address = 6921h

union rw_ctrl_0122h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk6_rep_bits_5_1                                                     : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_bk7_step_bits_2_0                                                    : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0122h xdata g_rw_ctrl_0122h;    // Absolute Address = 6922h

union rw_ctrl_0123h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk7_step_bits_5_3                                                    : 3;        // [msb:lsb] = [5:3], val = 0
        unsigned char r_bk7_rep_bits_4_0                                                     : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0123h xdata g_rw_ctrl_0123h;    // Absolute Address = 6923h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0124h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk7_rep_bits_5                                                       : 1;        // val = 0
        unsigned char r_bk8_step                                                             : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_bk8_rep_bits_0                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0124h xdata g_rw_ctrl_0124h;    // Absolute Address = 6924h

union rw_ctrl_0125h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk8_rep_bits_5_1                                                     : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_bk9_step_bits_2_0                                                    : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0125h xdata g_rw_ctrl_0125h;    // Absolute Address = 6925h

union rw_ctrl_0126h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk9_step_bits_5_3                                                    : 3;        // [msb:lsb] = [5:3], val = 0
        unsigned char r_bk9_rep_bits_4_0                                                     : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0126h xdata g_rw_ctrl_0126h;    // Absolute Address = 6926h

union rw_ctrl_0127h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk9_rep_bits_5                                                       : 1;        // val = 0
        unsigned char r_bk10_step                                                            : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_bk10_rep_bits_0                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0127h xdata g_rw_ctrl_0127h;    // Absolute Address = 6927h

union rw_ctrl_0128h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk10_rep_bits_5_1                                                    : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_bk11_step_bits_2_0                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0128h xdata g_rw_ctrl_0128h;    // Absolute Address = 6928h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0129h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk11_step_bits_5_3                                                   : 3;        // [msb:lsb] = [5:3], val = 0
        unsigned char r_bk11_rep_bits_4_0                                                    : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0129h xdata g_rw_ctrl_0129h;    // Absolute Address = 6929h

union rw_ctrl_012Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk11_rep_bits_5                                                      : 1;        // val = 0
        unsigned char r_bk12_step                                                            : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_bk12_rep_bits_0                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_012Ah xdata g_rw_ctrl_012Ah;    // Absolute Address = 692Ah

union rw_ctrl_012Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk12_rep_bits_5_1                                                    : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_bk13_step_bits_2_0                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_012Bh xdata g_rw_ctrl_012Bh;    // Absolute Address = 692Bh

union rw_ctrl_012Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk13_step_bits_5_3                                                   : 3;        // [msb:lsb] = [5:3], val = 0
        unsigned char r_bk13_rep_bits_4_0                                                    : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_012Ch xdata g_rw_ctrl_012Ch;    // Absolute Address = 692Ch

union rw_ctrl_012Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk13_rep_bits_5                                                      : 1;        // val = 0
        unsigned char r_bk14_step                                                            : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_bk14_rep_bits_0                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_012Dh xdata g_rw_ctrl_012Dh;    // Absolute Address = 692Dh

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_012Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk14_rep_bits_5_1                                                    : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_bk15_step_bits_2_0                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_012Eh xdata g_rw_ctrl_012Eh;    // Absolute Address = 692Eh

union rw_ctrl_012Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk15_step_bits_5_3                                                   : 3;        // [msb:lsb] = [5:3], val = 0
        unsigned char r_bk15_rep_bits_4_0                                                    : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_012Fh xdata g_rw_ctrl_012Fh;    // Absolute Address = 692Fh

union rw_ctrl_0130h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk15_rep_bits_5                                                      : 1;        // val = 0
        unsigned char r_bk16_step                                                            : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_bk16_rep_bits_0                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0130h xdata g_rw_ctrl_0130h;    // Absolute Address = 6930h

union rw_ctrl_0131h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk16_rep_bits_5_1                                                    : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_bk17_step_bits_2_0                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0131h xdata g_rw_ctrl_0131h;    // Absolute Address = 6931h

union rw_ctrl_0132h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk17_step_bits_5_3                                                   : 3;        // [msb:lsb] = [5:3], val = 0
        unsigned char r_bk17_rep_bits_4_0                                                    : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0132h xdata g_rw_ctrl_0132h;    // Absolute Address = 6932h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0133h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk17_rep_bits_5                                                      : 1;        // val = 0
        unsigned char r_bk18_step                                                            : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_bk18_rep_bits_0                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0133h xdata g_rw_ctrl_0133h;    // Absolute Address = 6933h

union rw_ctrl_0134h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk18_rep_bits_5_1                                                    : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_bk19_step_bits_2_0                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0134h xdata g_rw_ctrl_0134h;    // Absolute Address = 6934h

union rw_ctrl_0135h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk19_step_bits_5_3                                                   : 3;        // [msb:lsb] = [5:3], val = 0
        unsigned char r_bk19_rep_bits_4_0                                                    : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_0135h xdata g_rw_ctrl_0135h;    // Absolute Address = 6935h

union rw_ctrl_0136h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk19_rep_bits_5                                                      : 1;        // val = 0
        unsigned char r_bk20_step                                                            : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_bk20_rep_bits_0                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0136h xdata g_rw_ctrl_0136h;    // Absolute Address = 6936h

union rw_ctrl_0137h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bk20_rep_bits_5_1                                                    : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_gck00_xdio_sel                                                       : 1;        // val = 0
        unsigned char r_gck01_xdio_sel                                                       : 1;        // val = 0
        unsigned char r_gck02_xdio_sel                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0137h xdata g_rw_ctrl_0137h;    // Absolute Address = 6937h

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_0138h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck03_xdio_sel                                                       : 1;        // val = 0
        unsigned char r_gck04_xdio_sel                                                       : 1;        // val = 0
        unsigned char r_gck05_xdio_sel                                                       : 1;        // val = 0
        unsigned char r_gck06_xdio_sel                                                       : 1;        // val = 0
        unsigned char r_gck07_xdio_sel                                                       : 1;        // val = 0
        unsigned char r_gck08_xdio_sel                                                       : 1;        // val = 0
        unsigned char r_gck09_xdio_sel                                                       : 1;        // val = 0
        unsigned char r_gck0a_xdio_sel                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0138h xdata g_rw_ctrl_0138h;    // Absolute Address = 6938h

union rw_ctrl_0139h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0b_xdio_sel                                                       : 1;        // val = 0
        unsigned char r_gck0c_xdio_sel                                                       : 1;        // val = 0
        unsigned char r_gck0d_xdio_sel                                                       : 1;        // val = 0
        unsigned char r_gck0e_xdio_sel                                                       : 1;        // val = 0
        unsigned char r_gck0f_xdio_sel                                                       : 1;        // val = 0
        unsigned char r_xdio_sel                                                             : 1;        // val = 0
        unsigned char r_gck00_stv2_chg_en                                                    : 1;        // val = 0
        unsigned char r_gck01_stv2_chg_en                                                    : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_0139h xdata g_rw_ctrl_0139h;    // Absolute Address = 6939h

union rw_ctrl_013Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck02_stv2_chg_en                                                    : 1;        // val = 0
        unsigned char r_gck03_stv2_chg_en                                                    : 1;        // val = 0
        unsigned char r_gck04_stv2_chg_en                                                    : 1;        // val = 0
        unsigned char r_gck05_stv2_chg_en                                                    : 1;        // val = 0
        unsigned char r_gck06_stv2_chg_en                                                    : 1;        // val = 0
        unsigned char r_gck07_stv2_chg_en                                                    : 1;        // val = 0
        unsigned char r_gck08_stv2_chg_en                                                    : 1;        // val = 0
        unsigned char r_gck09_stv2_chg_en                                                    : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_013Ah xdata g_rw_ctrl_013Ah;    // Absolute Address = 693Ah

union rw_ctrl_013Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gck0a_stv2_chg_en                                                    : 1;        // val = 0
        unsigned char r_gck0b_stv2_chg_en                                                    : 1;        // val = 0
        unsigned char r_gck0c_stv2_chg_en                                                    : 1;        // val = 0
        unsigned char r_gck0d_stv2_chg_en                                                    : 1;        // val = 0
        unsigned char r_gck0e_stv2_chg_en                                                    : 1;        // val = 0
        unsigned char r_gck0f_stv2_chg_en                                                    : 1;        // val = 0
        unsigned char r_stv2_cycle_bits_1_0                                                  : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_013Bh xdata g_rw_ctrl_013Bh;    // Absolute Address = 693Bh

union rw_ctrl_013Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_stv2_cycle_bits_3_2                                                  : 2;        // [msb:lsb] = [3:2], val = 0
        unsigned char r_stv2_step                                                            : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_stv2_frm_bits_1_0                                                    : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_ctrl_013Ch xdata g_rw_ctrl_013Ch;    // Absolute Address = 693Ch

//------------------------------------------------------------------------------------------------------------------------
union rw_ctrl_013Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_stv2_frm_bits_7_2                                                    : 6;        // [msb:lsb] = [7:2], val = 0
        unsigned char r_stv2_stchg_rst_en                                                    : 1;        // val = 0
        unsigned char r_debug_en                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_013Dh xdata g_rw_ctrl_013Dh;    // Absolute Address = 693Dh

union rw_ctrl_013Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_chksum_frm_no                                                        : 4;        // [msb:lsb] = [3:0], val = 13
        unsigned char r_dyn_polc_or_sqinv_sel                                                : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_mask_esd_flag_sel                                                    : 1;        // val = 0
        unsigned char r_line_clr_mux                                                         : 1;        // val = 0
    }bits;
};
extern volatile union rw_ctrl_013Fh xdata g_rw_ctrl_013Fh;    // Absolute Address = 693Fh

union rw_ctrl_0141h
{
    unsigned char byte;
    struct
    {
        unsigned char r_touch_sync_mode                                                      : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_touch_sync_level                                                     : 1;        // val = 0
        unsigned char reserved_4_by_tool                                                     : 1;
        unsigned char reserved_3_by_tool                                                     : 1;
        unsigned char reserved_2_by_tool                                                     : 1;
        unsigned char reserved_1_by_tool                                                     : 1;
        unsigned char reserved_0_by_tool                                                     : 1;
    }bits;
};
extern volatile union rw_ctrl_0141h xdata g_rw_ctrl_0141h;    // Absolute Address = 6941h


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_ctrl_0006h_gck00_st_v_line;                                           // [msb:lsb] = [8:1], val = 3, 	Absolute Address = 6806h
extern volatile uint8_t xdata g_rw_ctrl_0008h_gck00_end_v_line;                                          // [msb:lsb] = [10:3], val = 182, 	Absolute Address = 6808h
extern volatile uint8_t xdata g_rw_ctrl_000Ah_gck00_sh;                                                  // [msb:lsb] = [12:5], val = 15, 	Absolute Address = 680Ah
extern volatile uint8_t xdata g_rw_ctrl_000Bh_gck00_w;                                                   // [msb:lsb] = [7:0], val = 80, 	Absolute Address = 680Bh
extern volatile uint8_t xdata g_rw_ctrl_0013h_gck01_st_v_line;                                           // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 6813h

extern volatile uint8_t xdata g_rw_ctrl_0015h_gck01_end_v_line;                                          // [msb:lsb] = [13:6], val = 22, 	Absolute Address = 6815h
extern volatile uint8_t xdata g_rw_ctrl_0016h_gck01_sh;                                                  // [msb:lsb] = [7:0], val = 244, 	Absolute Address = 6816h
extern volatile uint8_t xdata g_rw_ctrl_0018h_gck01_w;                                                   // [msb:lsb] = [10:3], val = 10, 	Absolute Address = 6818h
extern volatile uint8_t xdata g_rw_ctrl_001Dh_gck02_frm_cycle;                                           // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 681Dh
extern volatile uint8_t xdata g_rw_ctrl_0021h_gck02_end_v_line;                                          // [msb:lsb] = [8:1], val = 255, 	Absolute Address = 6821h

extern volatile uint8_t xdata g_rw_ctrl_0023h_gck02_sh;                                                  // [msb:lsb] = [10:3], val = 12, 	Absolute Address = 6823h
extern volatile uint8_t xdata g_rw_ctrl_002Ch_gck03_st_v_line;                                           // [msb:lsb] = [9:2], val = 109, 	Absolute Address = 682Ch
extern volatile uint8_t xdata g_rw_ctrl_002Eh_gck03_end_v_line;                                          // [msb:lsb] = [11:4], val = 255, 	Absolute Address = 682Eh
extern volatile uint8_t xdata g_rw_ctrl_0031h_gck03_w;                                                   // [msb:lsb] = [8:1], val = 5, 	Absolute Address = 6831h
extern volatile uint8_t xdata g_rw_ctrl_0039h_gck04_st_v_line;                                           // [msb:lsb] = [12:5], val = 0, 	Absolute Address = 6839h

extern volatile uint8_t xdata g_rw_ctrl_003Ch_gck04_sh;                                                  // [msb:lsb] = [8:1], val = 45, 	Absolute Address = 683Ch
extern volatile uint8_t xdata g_rw_ctrl_003Eh_gck04_w;                                                   // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 683Eh
extern volatile uint8_t xdata g_rw_ctrl_0043h_gck05_frm_cycle;                                           // [msb:lsb] = [8:1], val = 0, 	Absolute Address = 6843h
extern volatile uint8_t xdata g_rw_ctrl_0044h_gck05_init;                                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6844h
extern volatile uint8_t xdata g_rw_ctrl_0045h_gck05_st_v_line;                                           // [msb:lsb] = [7:0], val = 4, 	Absolute Address = 6845h

extern volatile uint8_t xdata g_rw_ctrl_0047h_gck05_end_v_line;                                          // [msb:lsb] = [9:2], val = 255, 	Absolute Address = 6847h
extern volatile uint8_t xdata g_rw_ctrl_0049h_gck05_sh;                                                  // [msb:lsb] = [11:4], val = 12, 	Absolute Address = 6849h
extern volatile uint8_t xdata g_rw_ctrl_0052h_gck06_st_v_line;                                           // [msb:lsb] = [10:3], val = 182, 	Absolute Address = 6852h
extern volatile uint8_t xdata g_rw_ctrl_0054h_gck06_end_v_line;                                          // [msb:lsb] = [12:5], val = 127, 	Absolute Address = 6854h
extern volatile uint8_t xdata g_rw_ctrl_0057h_gck06_w;                                                   // [msb:lsb] = [9:2], val = 2, 	Absolute Address = 6857h

extern volatile uint8_t xdata g_rw_ctrl_005Fh_gck07_st_v_line;                                           // [msb:lsb] = [13:6], val = 0, 	Absolute Address = 685Fh
extern volatile uint8_t xdata g_rw_ctrl_0060h_gck07_end_v_line;                                          // [msb:lsb] = [7:0], val = 180, 	Absolute Address = 6860h
extern volatile uint8_t xdata g_rw_ctrl_0062h_gck07_sh;                                                  // [msb:lsb] = [9:2], val = 194, 	Absolute Address = 6862h
extern volatile uint8_t xdata g_rw_ctrl_0064h_gck07_w;                                                   // [msb:lsb] = [12:5], val = 15, 	Absolute Address = 6864h
extern volatile uint8_t xdata g_rw_ctrl_006Bh_gck08_st_v_line;                                           // [msb:lsb] = [8:1], val = 5, 	Absolute Address = 686Bh

extern volatile uint8_t xdata g_rw_ctrl_006Dh_gck08_end_v_line;                                          // [msb:lsb] = [10:3], val = 182, 	Absolute Address = 686Dh
extern volatile uint8_t xdata g_rw_ctrl_006Fh_gck08_sh;                                                  // [msb:lsb] = [12:5], val = 56, 	Absolute Address = 686Fh
extern volatile uint8_t xdata g_rw_ctrl_0070h_gck08_w;                                                   // [msb:lsb] = [7:0], val = 244, 	Absolute Address = 6870h
extern volatile uint8_t xdata g_rw_ctrl_0078h_gck09_st_v_line;                                           // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 6878h
extern volatile uint8_t xdata g_rw_ctrl_007Ah_gck09_end_v_line;                                          // [msb:lsb] = [13:6], val = 63, 	Absolute Address = 687Ah

extern volatile uint8_t xdata g_rw_ctrl_007Bh_gck09_sh;                                                  // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 687Bh
extern volatile uint8_t xdata g_rw_ctrl_007Dh_gck09_w;                                                   // [msb:lsb] = [10:3], val = 187, 	Absolute Address = 687Dh
extern volatile uint8_t xdata g_rw_ctrl_007Fh_gck_w_cs;                                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 687Fh
extern volatile uint8_t xdata g_rw_ctrl_0085h_gck_vblk_end_v_line;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6885h
extern volatile uint8_t xdata g_rw_ctrl_009Fh_28s_gck_sh1;                                               // [msb:lsb] = [10:3], val = 0, 	Absolute Address = 689Fh

extern volatile uint8_t xdata g_rw_ctrl_00A2h_28s_gck_sh3;                                               // [msb:lsb] = [8:1], val = 0, 	Absolute Address = 68A2h
extern volatile uint8_t xdata g_rw_ctrl_00A4h_28s_gck_w1;                                                // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 68A4h
extern volatile uint8_t xdata g_rw_ctrl_00A7h_28s_gck_w3;                                                // [msb:lsb] = [9:2], val = 0, 	Absolute Address = 68A7h
extern volatile uint8_t xdata g_rw_ctrl_00A9h_28s_ld_sh1;                                                // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 68A9h
extern volatile uint8_t xdata g_rw_ctrl_00ACh_28s_ld_sh3;                                                // [msb:lsb] = [9:2], val = 0, 	Absolute Address = 68ACh

extern volatile uint8_t xdata g_rw_ctrl_00AEh_28s_ld_w1;                                                 // [msb:lsb] = [12:5], val = 0, 	Absolute Address = 68AEh
extern volatile uint8_t xdata g_rw_ctrl_00AFh_28s_ld_w2;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 68AFh
extern volatile uint8_t xdata g_rw_ctrl_00B1h_28s_ld_w3;                                                 // [msb:lsb] = [10:3], val = 0, 	Absolute Address = 68B1h
extern volatile uint8_t xdata g_rw_ctrl_00BAh_gck0a_st_v_line;                                           // [msb:lsb] = [8:1], val = 38, 	Absolute Address = 68BAh
extern volatile uint8_t xdata g_rw_ctrl_00BCh_gck0a_end_v_line;                                          // [msb:lsb] = [10:3], val = 255, 	Absolute Address = 68BCh

extern volatile uint8_t xdata g_rw_ctrl_00BEh_gck0a_sh;                                                  // [msb:lsb] = [12:5], val = 6, 	Absolute Address = 68BEh
extern volatile uint8_t xdata g_rw_ctrl_00BFh_gck0a_w;                                                   // [msb:lsb] = [7:0], val = 244, 	Absolute Address = 68BFh
extern volatile uint8_t xdata g_rw_ctrl_00C7h_gck0b_st_v_line;                                           // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 68C7h
extern volatile uint8_t xdata g_rw_ctrl_00C9h_gck0b_end_v_line;                                          // [msb:lsb] = [13:6], val = 0, 	Absolute Address = 68C9h
extern volatile uint8_t xdata g_rw_ctrl_00CAh_gck0b_sh;                                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 68CAh

extern volatile uint8_t xdata g_rw_ctrl_00CCh_gck0b_w;                                                   // [msb:lsb] = [10:3], val = 0, 	Absolute Address = 68CCh
extern volatile uint8_t xdata g_rw_ctrl_00D1h_gck0c_frm_cycle;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 68D1h
extern volatile uint8_t xdata g_rw_ctrl_00D5h_gck0c_end_v_line;                                          // [msb:lsb] = [8:1], val = 0, 	Absolute Address = 68D5h
extern volatile uint8_t xdata g_rw_ctrl_00D7h_gck0c_sh;                                                  // [msb:lsb] = [10:3], val = 0, 	Absolute Address = 68D7h
extern volatile uint8_t xdata g_rw_ctrl_00E0h_gck0d_st_v_line;                                           // [msb:lsb] = [9:2], val = 0, 	Absolute Address = 68E0h

extern volatile uint8_t xdata g_rw_ctrl_00E2h_gck0d_end_v_line;                                          // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 68E2h
extern volatile uint8_t xdata g_rw_ctrl_00E5h_gck0d_w;                                                   // [msb:lsb] = [8:1], val = 0, 	Absolute Address = 68E5h
extern volatile uint8_t xdata g_rw_ctrl_00EDh_gck0e_st_v_line;                                           // [msb:lsb] = [12:5], val = 0, 	Absolute Address = 68EDh
extern volatile uint8_t xdata g_rw_ctrl_00F0h_gck0e_sh;                                                  // [msb:lsb] = [8:1], val = 0, 	Absolute Address = 68F0h
extern volatile uint8_t xdata g_rw_ctrl_00F2h_gck0e_w;                                                   // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 68F2h

extern volatile uint8_t xdata g_rw_ctrl_00F7h_gck0f_frm_cycle;                                           // [msb:lsb] = [8:1], val = 0, 	Absolute Address = 68F7h
extern volatile uint8_t xdata g_rw_ctrl_00F8h_gck0f_init;                                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 68F8h
extern volatile uint8_t xdata g_rw_ctrl_00F9h_gck0f_st_v_line;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 68F9h
extern volatile uint8_t xdata g_rw_ctrl_00FBh_gck0f_end_v_line;                                          // [msb:lsb] = [9:2], val = 0, 	Absolute Address = 68FBh
extern volatile uint8_t xdata g_rw_ctrl_00FDh_gck0f_sh;                                                  // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 68FDh

extern volatile uint8_t xdata g_rw_ctrl_0113h_28s_w1;                                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6913h
extern volatile uint8_t xdata g_rw_ctrl_0115h_28s_w2;                                                    // [msb:lsb] = [10:3], val = 0, 	Absolute Address = 6915h
extern volatile uint8_t xdata g_rw_ctrl_013Eh_chksum;                                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 693Eh
extern volatile uint8_t xdata g_rw_ctrl_0140h_touch_sync_hdly;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 6940h

#endif