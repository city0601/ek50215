#ifndef _LXL_GRAYLUT_H_
#define _LXL_GRAYLUT_H_
#include "reg_include.h"


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_lxl_graylut_0000h_LXL_LUT0_data_0;                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8800h
extern volatile uint8_t xdata g_rw_lxl_graylut_0001h_LXL_LUT0_data_1;                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8801h
extern volatile uint8_t xdata g_rw_lxl_graylut_0002h_LXL_LUT0_data_2;                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8802h
extern volatile uint8_t xdata g_rw_lxl_graylut_0003h_LXL_LUT0_data_3;                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8803h
extern volatile uint8_t xdata g_rw_lxl_graylut_0004h_LXL_LUT0_data_4;                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8804h

extern volatile uint8_t xdata g_rw_lxl_graylut_0005h_LXL_LUT0_data_5;                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8805h
extern volatile uint8_t xdata g_rw_lxl_graylut_0006h_LXL_LUT0_data_6;                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8806h
extern volatile uint8_t xdata g_rw_lxl_graylut_0007h_LXL_LUT0_data_7;                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8807h
extern volatile uint8_t xdata g_rw_lxl_graylut_0008h_LXL_LUT0_data_8;                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8808h
extern volatile uint8_t xdata g_rw_lxl_graylut_0009h_LXL_LUT0_data_9;                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8809h

extern volatile uint8_t xdata g_rw_lxl_graylut_000Ah_LXL_LUT0_data_10;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 880Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_000Bh_LXL_LUT0_data_11;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 880Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_000Ch_LXL_LUT0_data_12;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 880Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_000Dh_LXL_LUT0_data_13;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 880Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_000Eh_LXL_LUT0_data_14;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 880Eh

extern volatile uint8_t xdata g_rw_lxl_graylut_000Fh_LXL_LUT0_data_15;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 880Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0010h_LXL_LUT0_data_16;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8810h
extern volatile uint8_t xdata g_rw_lxl_graylut_0011h_LXL_LUT0_data_17;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8811h
extern volatile uint8_t xdata g_rw_lxl_graylut_0012h_LXL_LUT0_data_18;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8812h
extern volatile uint8_t xdata g_rw_lxl_graylut_0013h_LXL_LUT0_data_19;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8813h

extern volatile uint8_t xdata g_rw_lxl_graylut_0014h_LXL_LUT0_data_20;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8814h
extern volatile uint8_t xdata g_rw_lxl_graylut_0015h_LXL_LUT0_data_21;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8815h
extern volatile uint8_t xdata g_rw_lxl_graylut_0016h_LXL_LUT0_data_22;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8816h
extern volatile uint8_t xdata g_rw_lxl_graylut_0017h_LXL_LUT0_data_23;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8817h
extern volatile uint8_t xdata g_rw_lxl_graylut_0018h_LXL_LUT0_data_24;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8818h

extern volatile uint8_t xdata g_rw_lxl_graylut_0019h_LXL_LUT0_data_25;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8819h
extern volatile uint8_t xdata g_rw_lxl_graylut_001Ah_LXL_LUT0_data_26;                                   // [msb:lsb] = [7:0], val = 192, 	Absolute Address = 881Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_001Bh_LXL_LUT0_data_27;                                   // [msb:lsb] = [7:0], val = 15, 	Absolute Address = 881Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_001Ch_LXL_LUT0_data_28;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 881Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_001Dh_LXL_LUT0_data_29;                                   // [msb:lsb] = [7:0], val = 252, 	Absolute Address = 881Dh

extern volatile uint8_t xdata g_rw_lxl_graylut_001Eh_LXL_LUT0_data_30;                                   // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 881Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_001Fh_LXL_LUT0_data_31;                                   // [msb:lsb] = [7:0], val = 195, 	Absolute Address = 881Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0020h_LXL_LUT0_data_32;                                   // [msb:lsb] = [7:0], val = 15, 	Absolute Address = 8820h
extern volatile uint8_t xdata g_rw_lxl_graylut_0021h_LXL_LUT0_data_33;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 8821h
extern volatile uint8_t xdata g_rw_lxl_graylut_0022h_LXL_LUT0_data_34;                                   // [msb:lsb] = [7:0], val = 252, 	Absolute Address = 8822h

extern volatile uint8_t xdata g_rw_lxl_graylut_0023h_LXL_LUT0_data_35;                                   // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 8823h
extern volatile uint8_t xdata g_rw_lxl_graylut_0024h_LXL_LUT0_data_36;                                   // [msb:lsb] = [7:0], val = 195, 	Absolute Address = 8824h
extern volatile uint8_t xdata g_rw_lxl_graylut_0025h_LXL_LUT0_data_37;                                   // [msb:lsb] = [7:0], val = 15, 	Absolute Address = 8825h
extern volatile uint8_t xdata g_rw_lxl_graylut_0026h_LXL_LUT0_data_38;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 8826h
extern volatile uint8_t xdata g_rw_lxl_graylut_0027h_LXL_LUT0_data_39;                                   // [msb:lsb] = [7:0], val = 252, 	Absolute Address = 8827h

extern volatile uint8_t xdata g_rw_lxl_graylut_0028h_LXL_LUT0_data_40;                                   // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 8828h
extern volatile uint8_t xdata g_rw_lxl_graylut_0029h_LXL_LUT0_data_41;                                   // [msb:lsb] = [7:0], val = 195, 	Absolute Address = 8829h
extern volatile uint8_t xdata g_rw_lxl_graylut_002Ah_LXL_LUT0_data_42;                                   // [msb:lsb] = [7:0], val = 15, 	Absolute Address = 882Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_002Bh_LXL_LUT0_data_43;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 882Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_002Ch_LXL_LUT0_data_44;                                   // [msb:lsb] = [7:0], val = 251, 	Absolute Address = 882Ch

extern volatile uint8_t xdata g_rw_lxl_graylut_002Dh_LXL_LUT0_data_45;                                   // [msb:lsb] = [7:0], val = 220, 	Absolute Address = 882Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_002Eh_LXL_LUT0_data_46;                                   // [msb:lsb] = [7:0], val = 3, 	Absolute Address = 882Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_002Fh_LXL_LUT0_data_47;                                   // [msb:lsb] = [7:0], val = 207, 	Absolute Address = 882Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0030h_LXL_LUT0_data_48;                                   // [msb:lsb] = [7:0], val = 3, 	Absolute Address = 8830h
extern volatile uint8_t xdata g_rw_lxl_graylut_0031h_LXL_LUT0_data_49;                                   // [msb:lsb] = [7:0], val = 8, 	Absolute Address = 8831h

extern volatile uint8_t xdata g_rw_lxl_graylut_0032h_LXL_LUT0_data_50;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8832h
extern volatile uint8_t xdata g_rw_lxl_graylut_0033h_LXL_LUT0_data_51;                                   // [msb:lsb] = [7:0], val = 208, 	Absolute Address = 8833h
extern volatile uint8_t xdata g_rw_lxl_graylut_0034h_LXL_LUT0_data_52;                                   // [msb:lsb] = [7:0], val = 79, 	Absolute Address = 8834h
extern volatile uint8_t xdata g_rw_lxl_graylut_0035h_LXL_LUT0_data_53;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 8835h
extern volatile uint8_t xdata g_rw_lxl_graylut_0036h_LXL_LUT0_data_54;                                   // [msb:lsb] = [7:0], val = 252, 	Absolute Address = 8836h

extern volatile uint8_t xdata g_rw_lxl_graylut_0037h_LXL_LUT0_data_55;                                   // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 8837h
extern volatile uint8_t xdata g_rw_lxl_graylut_0038h_LXL_LUT0_data_56;                                   // [msb:lsb] = [7:0], val = 179, 	Absolute Address = 8838h
extern volatile uint8_t xdata g_rw_lxl_graylut_0039h_LXL_LUT0_data_57;                                   // [msb:lsb] = [7:0], val = 207, 	Absolute Address = 8839h
extern volatile uint8_t xdata g_rw_lxl_graylut_003Ah_LXL_LUT0_data_58;                                   // [msb:lsb] = [7:0], val = 62, 	Absolute Address = 883Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_003Bh_LXL_LUT0_data_59;                                   // [msb:lsb] = [7:0], val = 250, 	Absolute Address = 883Bh

extern volatile uint8_t xdata g_rw_lxl_graylut_003Ch_LXL_LUT0_data_60;                                   // [msb:lsb] = [7:0], val = 232, 	Absolute Address = 883Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_003Dh_LXL_LUT0_data_61;                                   // [msb:lsb] = [7:0], val = 163, 	Absolute Address = 883Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_003Eh_LXL_LUT0_data_62;                                   // [msb:lsb] = [7:0], val = 143, 	Absolute Address = 883Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_003Fh_LXL_LUT0_data_63;                                   // [msb:lsb] = [7:0], val = 62, 	Absolute Address = 883Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0040h_LXL_LUT0_data_64;                                   // [msb:lsb] = [7:0], val = 249, 	Absolute Address = 8840h

extern volatile uint8_t xdata g_rw_lxl_graylut_0041h_LXL_LUT0_data_65;                                   // [msb:lsb] = [7:0], val = 228, 	Absolute Address = 8841h
extern volatile uint8_t xdata g_rw_lxl_graylut_0042h_LXL_LUT0_data_66;                                   // [msb:lsb] = [7:0], val = 131, 	Absolute Address = 8842h
extern volatile uint8_t xdata g_rw_lxl_graylut_0043h_LXL_LUT0_data_67;                                   // [msb:lsb] = [7:0], val = 15, 	Absolute Address = 8843h
extern volatile uint8_t xdata g_rw_lxl_graylut_0044h_LXL_LUT0_data_68;                                   // [msb:lsb] = [7:0], val = 62, 	Absolute Address = 8844h
extern volatile uint8_t xdata g_rw_lxl_graylut_0045h_LXL_LUT0_data_69;                                   // [msb:lsb] = [7:0], val = 248, 	Absolute Address = 8845h

extern volatile uint8_t xdata g_rw_lxl_graylut_0046h_LXL_LUT0_data_70;                                   // [msb:lsb] = [7:0], val = 140, 	Absolute Address = 8846h
extern volatile uint8_t xdata g_rw_lxl_graylut_0047h_LXL_LUT0_data_71;                                   // [msb:lsb] = [7:0], val = 147, 	Absolute Address = 8847h
extern volatile uint8_t xdata g_rw_lxl_graylut_0048h_LXL_LUT0_data_72;                                   // [msb:lsb] = [7:0], val = 193, 	Absolute Address = 8848h
extern volatile uint8_t xdata g_rw_lxl_graylut_0049h_LXL_LUT0_data_73;                                   // [msb:lsb] = [7:0], val = 3, 	Absolute Address = 8849h
extern volatile uint8_t xdata g_rw_lxl_graylut_004Ah_LXL_LUT0_data_74;                                   // [msb:lsb] = [7:0], val = 8, 	Absolute Address = 884Ah

extern volatile uint8_t xdata g_rw_lxl_graylut_004Bh_LXL_LUT0_data_75;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 884Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_004Ch_LXL_LUT0_data_76;                                   // [msb:lsb] = [7:0], val = 176, 	Absolute Address = 884Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_004Dh_LXL_LUT0_data_77;                                   // [msb:lsb] = [7:0], val = 143, 	Absolute Address = 884Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_004Eh_LXL_LUT0_data_78;                                   // [msb:lsb] = [7:0], val = 62, 	Absolute Address = 884Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_004Fh_LXL_LUT0_data_79;                                   // [msb:lsb] = [7:0], val = 247, 	Absolute Address = 884Fh

extern volatile uint8_t xdata g_rw_lxl_graylut_0050h_LXL_LUT0_data_80;                                   // [msb:lsb] = [7:0], val = 216, 	Absolute Address = 8850h
extern volatile uint8_t xdata g_rw_lxl_graylut_0051h_LXL_LUT0_data_81;                                   // [msb:lsb] = [7:0], val = 99, 	Absolute Address = 8851h
extern volatile uint8_t xdata g_rw_lxl_graylut_0052h_LXL_LUT0_data_82;                                   // [msb:lsb] = [7:0], val = 15, 	Absolute Address = 8852h
extern volatile uint8_t xdata g_rw_lxl_graylut_0053h_LXL_LUT0_data_83;                                   // [msb:lsb] = [7:0], val = 61, 	Absolute Address = 8853h
extern volatile uint8_t xdata g_rw_lxl_graylut_0054h_LXL_LUT0_data_84;                                   // [msb:lsb] = [7:0], val = 244, 	Absolute Address = 8854h

extern volatile uint8_t xdata g_rw_lxl_graylut_0055h_LXL_LUT0_data_85;                                   // [msb:lsb] = [7:0], val = 208, 	Absolute Address = 8855h
extern volatile uint8_t xdata g_rw_lxl_graylut_0056h_LXL_LUT0_data_86;                                   // [msb:lsb] = [7:0], val = 67, 	Absolute Address = 8856h
extern volatile uint8_t xdata g_rw_lxl_graylut_0057h_LXL_LUT0_data_87;                                   // [msb:lsb] = [7:0], val = 15, 	Absolute Address = 8857h
extern volatile uint8_t xdata g_rw_lxl_graylut_0058h_LXL_LUT0_data_88;                                   // [msb:lsb] = [7:0], val = 61, 	Absolute Address = 8858h
extern volatile uint8_t xdata g_rw_lxl_graylut_0059h_LXL_LUT0_data_89;                                   // [msb:lsb] = [7:0], val = 243, 	Absolute Address = 8859h

extern volatile uint8_t xdata g_rw_lxl_graylut_005Ah_LXL_LUT0_data_90;                                   // [msb:lsb] = [7:0], val = 196, 	Absolute Address = 885Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_005Bh_LXL_LUT0_data_91;                                   // [msb:lsb] = [7:0], val = 3, 	Absolute Address = 885Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_005Ch_LXL_LUT0_data_92;                                   // [msb:lsb] = [7:0], val = 79, 	Absolute Address = 885Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_005Dh_LXL_LUT0_data_93;                                   // [msb:lsb] = [7:0], val = 57, 	Absolute Address = 885Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_005Eh_LXL_LUT0_data_94;                                   // [msb:lsb] = [7:0], val = 224, 	Absolute Address = 885Eh

extern volatile uint8_t xdata g_rw_lxl_graylut_005Fh_LXL_LUT0_data_95;                                   // [msb:lsb] = [7:0], val = 160, 	Absolute Address = 885Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0060h_LXL_LUT0_data_96;                                   // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 8860h
extern volatile uint8_t xdata g_rw_lxl_graylut_0061h_LXL_LUT0_data_97;                                   // [msb:lsb] = [7:0], val = 1, 	Absolute Address = 8861h
extern volatile uint8_t xdata g_rw_lxl_graylut_0062h_LXL_LUT0_data_98;                                   // [msb:lsb] = [7:0], val = 4, 	Absolute Address = 8862h
extern volatile uint8_t xdata g_rw_lxl_graylut_0063h_LXL_LUT0_data_99;                                   // [msb:lsb] = [7:0], val = 2, 	Absolute Address = 8863h

extern volatile uint8_t xdata g_rw_lxl_graylut_0064h_LXL_LUT0_data_100;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8864h
extern volatile uint8_t xdata g_rw_lxl_graylut_0065h_LXL_LUT0_data_101;                                  // [msb:lsb] = [7:0], val = 176, 	Absolute Address = 8865h
extern volatile uint8_t xdata g_rw_lxl_graylut_0066h_LXL_LUT0_data_102;                                  // [msb:lsb] = [7:0], val = 79, 	Absolute Address = 8866h
extern volatile uint8_t xdata g_rw_lxl_graylut_0067h_LXL_LUT0_data_103;                                  // [msb:lsb] = [7:0], val = 62, 	Absolute Address = 8867h
extern volatile uint8_t xdata g_rw_lxl_graylut_0068h_LXL_LUT0_data_104;                                  // [msb:lsb] = [7:0], val = 242, 	Absolute Address = 8868h

extern volatile uint8_t xdata g_rw_lxl_graylut_0069h_LXL_LUT0_data_105;                                  // [msb:lsb] = [7:0], val = 200, 	Absolute Address = 8869h
extern volatile uint8_t xdata g_rw_lxl_graylut_006Ah_LXL_LUT0_data_106;                                  // [msb:lsb] = [7:0], val = 35, 	Absolute Address = 886Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_006Bh_LXL_LUT0_data_107;                                  // [msb:lsb] = [7:0], val = 15, 	Absolute Address = 886Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_006Ch_LXL_LUT0_data_108;                                  // [msb:lsb] = [7:0], val = 60, 	Absolute Address = 886Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_006Dh_LXL_LUT0_data_109;                                  // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 886Dh

extern volatile uint8_t xdata g_rw_lxl_graylut_006Eh_LXL_LUT0_data_110;                                  // [msb:lsb] = [7:0], val = 184, 	Absolute Address = 886Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_006Fh_LXL_LUT0_data_111;                                  // [msb:lsb] = [7:0], val = 227, 	Absolute Address = 886Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0070h_LXL_LUT0_data_112;                                  // [msb:lsb] = [7:0], val = 142, 	Absolute Address = 8870h
extern volatile uint8_t xdata g_rw_lxl_graylut_0071h_LXL_LUT0_data_113;                                  // [msb:lsb] = [7:0], val = 59, 	Absolute Address = 8871h
extern volatile uint8_t xdata g_rw_lxl_graylut_0072h_LXL_LUT0_data_114;                                  // [msb:lsb] = [7:0], val = 236, 	Absolute Address = 8872h

extern volatile uint8_t xdata g_rw_lxl_graylut_0073h_LXL_LUT0_data_115;                                  // [msb:lsb] = [7:0], val = 156, 	Absolute Address = 8873h
extern volatile uint8_t xdata g_rw_lxl_graylut_0074h_LXL_LUT0_data_116;                                  // [msb:lsb] = [7:0], val = 51, 	Absolute Address = 8874h
extern volatile uint8_t xdata g_rw_lxl_graylut_0075h_LXL_LUT0_data_117;                                  // [msb:lsb] = [7:0], val = 78, 	Absolute Address = 8875h
extern volatile uint8_t xdata g_rw_lxl_graylut_0076h_LXL_LUT0_data_118;                                  // [msb:lsb] = [7:0], val = 55, 	Absolute Address = 8876h
extern volatile uint8_t xdata g_rw_lxl_graylut_0077h_LXL_LUT0_data_119;                                  // [msb:lsb] = [7:0], val = 65, 	Absolute Address = 8877h

extern volatile uint8_t xdata g_rw_lxl_graylut_0078h_LXL_LUT0_data_120;                                  // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 8878h
extern volatile uint8_t xdata g_rw_lxl_graylut_0079h_LXL_LUT0_data_121;                                  // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 8879h
extern volatile uint8_t xdata g_rw_lxl_graylut_007Ah_LXL_LUT0_data_122;                                  // [msb:lsb] = [7:0], val = 129, 	Absolute Address = 887Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_007Bh_LXL_LUT0_data_123;                                  // [msb:lsb] = [7:0], val = 2, 	Absolute Address = 887Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_007Ch_LXL_LUT0_data_124;                                  // [msb:lsb] = [7:0], val = 2, 	Absolute Address = 887Ch

extern volatile uint8_t xdata g_rw_lxl_graylut_007Dh_LXL_LUT0_data_125;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 887Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_007Eh_LXL_LUT0_data_126;                                  // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 887Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_007Fh_LXL_LUT0_data_127;                                  // [msb:lsb] = [7:0], val = 15, 	Absolute Address = 887Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0080h_LXL_LUT0_data_128;                                  // [msb:lsb] = [7:0], val = 60, 	Absolute Address = 8880h
extern volatile uint8_t xdata g_rw_lxl_graylut_0081h_LXL_LUT0_data_129;                                  // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 8881h

extern volatile uint8_t xdata g_rw_lxl_graylut_0082h_LXL_LUT0_data_130;                                  // [msb:lsb] = [7:0], val = 192, 	Absolute Address = 8882h
extern volatile uint8_t xdata g_rw_lxl_graylut_0083h_LXL_LUT0_data_131;                                  // [msb:lsb] = [7:0], val = 227, 	Absolute Address = 8883h
extern volatile uint8_t xdata g_rw_lxl_graylut_0084h_LXL_LUT0_data_132;                                  // [msb:lsb] = [7:0], val = 142, 	Absolute Address = 8884h
extern volatile uint8_t xdata g_rw_lxl_graylut_0085h_LXL_LUT0_data_133;                                  // [msb:lsb] = [7:0], val = 59, 	Absolute Address = 8885h
extern volatile uint8_t xdata g_rw_lxl_graylut_0086h_LXL_LUT0_data_134;                                  // [msb:lsb] = [7:0], val = 234, 	Absolute Address = 8886h

extern volatile uint8_t xdata g_rw_lxl_graylut_0087h_LXL_LUT0_data_135;                                  // [msb:lsb] = [7:0], val = 168, 	Absolute Address = 8887h
extern volatile uint8_t xdata g_rw_lxl_graylut_0088h_LXL_LUT0_data_136;                                  // [msb:lsb] = [7:0], val = 163, 	Absolute Address = 8888h
extern volatile uint8_t xdata g_rw_lxl_graylut_0089h_LXL_LUT0_data_137;                                  // [msb:lsb] = [7:0], val = 14, 	Absolute Address = 8889h
extern volatile uint8_t xdata g_rw_lxl_graylut_008Ah_LXL_LUT0_data_138;                                  // [msb:lsb] = [7:0], val = 58, 	Absolute Address = 888Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_008Bh_LXL_LUT0_data_139;                                  // [msb:lsb] = [7:0], val = 228, 	Absolute Address = 888Bh

extern volatile uint8_t xdata g_rw_lxl_graylut_008Ch_LXL_LUT0_data_140;                                  // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 888Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_008Dh_LXL_LUT0_data_141;                                  // [msb:lsb] = [7:0], val = 163, 	Absolute Address = 888Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_008Eh_LXL_LUT0_data_142;                                  // [msb:lsb] = [7:0], val = 13, 	Absolute Address = 888Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_008Fh_LXL_LUT0_data_143;                                  // [msb:lsb] = [7:0], val = 20, 	Absolute Address = 888Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0090h_LXL_LUT0_data_144;                                  // [msb:lsb] = [7:0], val = 40, 	Absolute Address = 8890h

extern volatile uint8_t xdata g_rw_lxl_graylut_0091h_LXL_LUT0_data_145;                                  // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 8891h
extern volatile uint8_t xdata g_rw_lxl_graylut_0092h_LXL_LUT0_data_146;                                  // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 8892h
extern volatile uint8_t xdata g_rw_lxl_graylut_0093h_LXL_LUT0_data_147;                                  // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 8893h
extern volatile uint8_t xdata g_rw_lxl_graylut_0094h_LXL_LUT0_data_148;                                  // [msb:lsb] = [7:0], val = 1, 	Absolute Address = 8894h
extern volatile uint8_t xdata g_rw_lxl_graylut_0095h_LXL_LUT0_data_149;                                  // [msb:lsb] = [7:0], val = 2, 	Absolute Address = 8895h

extern volatile uint8_t xdata g_rw_lxl_graylut_0096h_LXL_LUT0_data_150;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8896h
extern volatile uint8_t xdata g_rw_lxl_graylut_0097h_LXL_LUT0_data_151;                                  // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 8897h
extern volatile uint8_t xdata g_rw_lxl_graylut_0098h_LXL_LUT0_data_152;                                  // [msb:lsb] = [7:0], val = 207, 	Absolute Address = 8898h
extern volatile uint8_t xdata g_rw_lxl_graylut_0099h_LXL_LUT0_data_153;                                  // [msb:lsb] = [7:0], val = 61, 	Absolute Address = 8899h
extern volatile uint8_t xdata g_rw_lxl_graylut_009Ah_LXL_LUT0_data_154;                                  // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 889Ah

extern volatile uint8_t xdata g_rw_lxl_graylut_009Bh_LXL_LUT0_data_155;                                  // [msb:lsb] = [7:0], val = 176, 	Absolute Address = 889Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_009Ch_LXL_LUT0_data_156;                                  // [msb:lsb] = [7:0], val = 195, 	Absolute Address = 889Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_009Dh_LXL_LUT0_data_157;                                  // [msb:lsb] = [7:0], val = 78, 	Absolute Address = 889Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_009Eh_LXL_LUT0_data_158;                                  // [msb:lsb] = [7:0], val = 58, 	Absolute Address = 889Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_009Fh_LXL_LUT0_data_159;                                  // [msb:lsb] = [7:0], val = 231, 	Absolute Address = 889Fh

extern volatile uint8_t xdata g_rw_lxl_graylut_00A0h_LXL_LUT0_data_160;                                  // [msb:lsb] = [7:0], val = 164, 	Absolute Address = 88A0h
extern volatile uint8_t xdata g_rw_lxl_graylut_00A1h_LXL_LUT0_data_161;                                  // [msb:lsb] = [7:0], val = 99, 	Absolute Address = 88A1h
extern volatile uint8_t xdata g_rw_lxl_graylut_00A2h_LXL_LUT0_data_162;                                  // [msb:lsb] = [7:0], val = 142, 	Absolute Address = 88A2h
extern volatile uint8_t xdata g_rw_lxl_graylut_00A3h_LXL_LUT0_data_163;                                  // [msb:lsb] = [7:0], val = 56, 	Absolute Address = 88A3h
extern volatile uint8_t xdata g_rw_lxl_graylut_00A4h_LXL_LUT0_data_164;                                  // [msb:lsb] = [7:0], val = 221, 	Absolute Address = 88A4h

extern volatile uint8_t xdata g_rw_lxl_graylut_00A5h_LXL_LUT0_data_165;                                  // [msb:lsb] = [7:0], val = 100, 	Absolute Address = 88A5h
extern volatile uint8_t xdata g_rw_lxl_graylut_00A6h_LXL_LUT0_data_166;                                  // [msb:lsb] = [7:0], val = 83, 	Absolute Address = 88A6h
extern volatile uint8_t xdata g_rw_lxl_graylut_00A7h_LXL_LUT0_data_167;                                  // [msb:lsb] = [7:0], val = 69, 	Absolute Address = 88A7h
extern volatile uint8_t xdata g_rw_lxl_graylut_00A8h_LXL_LUT0_data_168;                                  // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 88A8h
extern volatile uint8_t xdata g_rw_lxl_graylut_00A9h_LXL_LUT0_data_169;                                  // [msb:lsb] = [7:0], val = 33, 	Absolute Address = 88A9h

extern volatile uint8_t xdata g_rw_lxl_graylut_00AAh_LXL_LUT0_data_170;                                  // [msb:lsb] = [7:0], val = 80, 	Absolute Address = 88AAh
extern volatile uint8_t xdata g_rw_lxl_graylut_00ABh_LXL_LUT0_data_171;                                  // [msb:lsb] = [7:0], val = 160, 	Absolute Address = 88ABh
extern volatile uint8_t xdata g_rw_lxl_graylut_00ACh_LXL_LUT0_data_172;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 88ACh
extern volatile uint8_t xdata g_rw_lxl_graylut_00ADh_LXL_LUT0_data_173;                                  // [msb:lsb] = [7:0], val = 1, 	Absolute Address = 88ADh
extern volatile uint8_t xdata g_rw_lxl_graylut_00AEh_LXL_LUT0_data_174;                                  // [msb:lsb] = [7:0], val = 2, 	Absolute Address = 88AEh

extern volatile uint8_t xdata g_rw_lxl_graylut_00AFh_LXL_LUT0_data_175;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 88AFh
extern volatile uint8_t xdata g_rw_lxl_graylut_00B0h_LXL_LUT0_data_176;                                  // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 88B0h
extern volatile uint8_t xdata g_rw_lxl_graylut_00B1h_LXL_LUT0_data_177;                                  // [msb:lsb] = [7:0], val = 143, 	Absolute Address = 88B1h
extern volatile uint8_t xdata g_rw_lxl_graylut_00B2h_LXL_LUT0_data_178;                                  // [msb:lsb] = [7:0], val = 61, 	Absolute Address = 88B2h
extern volatile uint8_t xdata g_rw_lxl_graylut_00B3h_LXL_LUT0_data_179;                                  // [msb:lsb] = [7:0], val = 243, 	Absolute Address = 88B3h

extern volatile uint8_t xdata g_rw_lxl_graylut_00B4h_LXL_LUT0_data_180;                                  // [msb:lsb] = [7:0], val = 192, 	Absolute Address = 88B4h
extern volatile uint8_t xdata g_rw_lxl_graylut_00B5h_LXL_LUT0_data_181;                                  // [msb:lsb] = [7:0], val = 195, 	Absolute Address = 88B5h
extern volatile uint8_t xdata g_rw_lxl_graylut_00B6h_LXL_LUT0_data_182;                                  // [msb:lsb] = [7:0], val = 142, 	Absolute Address = 88B6h
extern volatile uint8_t xdata g_rw_lxl_graylut_00B7h_LXL_LUT0_data_183;                                  // [msb:lsb] = [7:0], val = 58, 	Absolute Address = 88B7h
extern volatile uint8_t xdata g_rw_lxl_graylut_00B8h_LXL_LUT0_data_184;                                  // [msb:lsb] = [7:0], val = 236, 	Absolute Address = 88B8h

extern volatile uint8_t xdata g_rw_lxl_graylut_00B9h_LXL_LUT0_data_185;                                  // [msb:lsb] = [7:0], val = 160, 	Absolute Address = 88B9h
extern volatile uint8_t xdata g_rw_lxl_graylut_00BAh_LXL_LUT0_data_186;                                  // [msb:lsb] = [7:0], val = 99, 	Absolute Address = 88BAh
extern volatile uint8_t xdata g_rw_lxl_graylut_00BBh_LXL_LUT0_data_187;                                  // [msb:lsb] = [7:0], val = 142, 	Absolute Address = 88BBh
extern volatile uint8_t xdata g_rw_lxl_graylut_00BCh_LXL_LUT0_data_188;                                  // [msb:lsb] = [7:0], val = 56, 	Absolute Address = 88BCh
extern volatile uint8_t xdata g_rw_lxl_graylut_00BDh_LXL_LUT0_data_189;                                  // [msb:lsb] = [7:0], val = 211, 	Absolute Address = 88BDh

extern volatile uint8_t xdata g_rw_lxl_graylut_00BEh_LXL_LUT0_data_190;                                  // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 88BEh
extern volatile uint8_t xdata g_rw_lxl_graylut_00BFh_LXL_LUT0_data_191;                                  // [msb:lsb] = [7:0], val = 177, 	Absolute Address = 88BFh
extern volatile uint8_t xdata g_rw_lxl_graylut_00C0h_LXL_LUT0_data_192;                                  // [msb:lsb] = [7:0], val = 68, 	Absolute Address = 88C0h
extern volatile uint8_t xdata g_rw_lxl_graylut_00C1h_LXL_LUT0_data_193;                                  // [msb:lsb] = [7:0], val = 11, 	Absolute Address = 88C1h
extern volatile uint8_t xdata g_rw_lxl_graylut_00C2h_LXL_LUT0_data_194;                                  // [msb:lsb] = [7:0], val = 25, 	Absolute Address = 88C2h

extern volatile uint8_t xdata g_rw_lxl_graylut_00C3h_LXL_LUT0_data_195;                                  // [msb:lsb] = [7:0], val = 80, 	Absolute Address = 88C3h
extern volatile uint8_t xdata g_rw_lxl_graylut_00C4h_LXL_LUT0_data_196;                                  // [msb:lsb] = [7:0], val = 160, 	Absolute Address = 88C4h
extern volatile uint8_t xdata g_rw_lxl_graylut_00C5h_LXL_LUT0_data_197;                                  // [msb:lsb] = [7:0], val = 192, 	Absolute Address = 88C5h
extern volatile uint8_t xdata g_rw_lxl_graylut_00C6h_LXL_LUT0_data_198;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 88C6h
extern volatile uint8_t xdata g_rw_lxl_graylut_00C7h_LXL_LUT0_data_199;                                  // [msb:lsb] = [7:0], val = 1, 	Absolute Address = 88C7h

extern volatile uint8_t xdata g_rw_lxl_graylut_00C8h_LXL_LUT0_data_200;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 88C8h
extern volatile uint8_t xdata g_rw_lxl_graylut_00C9h_LXL_LUT0_data_201;                                  // [msb:lsb] = [7:0], val = 176, 	Absolute Address = 88C9h
extern volatile uint8_t xdata g_rw_lxl_graylut_00CAh_LXL_LUT0_data_202;                                  // [msb:lsb] = [7:0], val = 207, 	Absolute Address = 88CAh
extern volatile uint8_t xdata g_rw_lxl_graylut_00CBh_LXL_LUT0_data_203;                                  // [msb:lsb] = [7:0], val = 61, 	Absolute Address = 88CBh
extern volatile uint8_t xdata g_rw_lxl_graylut_00CCh_LXL_LUT0_data_204;                                  // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 88CCh

extern volatile uint8_t xdata g_rw_lxl_graylut_00CDh_LXL_LUT0_data_205;                                  // [msb:lsb] = [7:0], val = 176, 	Absolute Address = 88CDh
extern volatile uint8_t xdata g_rw_lxl_graylut_00CEh_LXL_LUT0_data_206;                                  // [msb:lsb] = [7:0], val = 99, 	Absolute Address = 88CEh
extern volatile uint8_t xdata g_rw_lxl_graylut_00CFh_LXL_LUT0_data_207;                                  // [msb:lsb] = [7:0], val = 142, 	Absolute Address = 88CFh
extern volatile uint8_t xdata g_rw_lxl_graylut_00D0h_LXL_LUT0_data_208;                                  // [msb:lsb] = [7:0], val = 55, 	Absolute Address = 88D0h
extern volatile uint8_t xdata g_rw_lxl_graylut_00D1h_LXL_LUT0_data_209;                                  // [msb:lsb] = [7:0], val = 222, 	Absolute Address = 88D1h

extern volatile uint8_t xdata g_rw_lxl_graylut_00D2h_LXL_LUT0_data_210;                                  // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 88D2h
extern volatile uint8_t xdata g_rw_lxl_graylut_00D3h_LXL_LUT0_data_211;                                  // [msb:lsb] = [7:0], val = 131, 	Absolute Address = 88D3h
extern volatile uint8_t xdata g_rw_lxl_graylut_00D4h_LXL_LUT0_data_212;                                  // [msb:lsb] = [7:0], val = 205, 	Absolute Address = 88D4h
extern volatile uint8_t xdata g_rw_lxl_graylut_00D5h_LXL_LUT0_data_213;                                  // [msb:lsb] = [7:0], val = 51, 	Absolute Address = 88D5h
extern volatile uint8_t xdata g_rw_lxl_graylut_00D6h_LXL_LUT0_data_214;                                  // [msb:lsb] = [7:0], val = 85, 	Absolute Address = 88D6h

extern volatile uint8_t xdata g_rw_lxl_graylut_00D7h_LXL_LUT0_data_215;                                  // [msb:lsb] = [7:0], val = 24, 	Absolute Address = 88D7h
extern volatile uint8_t xdata g_rw_lxl_graylut_00D8h_LXL_LUT0_data_216;                                  // [msb:lsb] = [7:0], val = 33, 	Absolute Address = 88D8h
extern volatile uint8_t xdata g_rw_lxl_graylut_00D9h_LXL_LUT0_data_217;                                  // [msb:lsb] = [7:0], val = 131, 	Absolute Address = 88D9h
extern volatile uint8_t xdata g_rw_lxl_graylut_00DAh_LXL_LUT0_data_218;                                  // [msb:lsb] = [7:0], val = 7, 	Absolute Address = 88DAh
extern volatile uint8_t xdata g_rw_lxl_graylut_00DBh_LXL_LUT0_data_219;                                  // [msb:lsb] = [7:0], val = 25, 	Absolute Address = 88DBh

extern volatile uint8_t xdata g_rw_lxl_graylut_00DCh_LXL_LUT0_data_220;                                  // [msb:lsb] = [7:0], val = 60, 	Absolute Address = 88DCh
extern volatile uint8_t xdata g_rw_lxl_graylut_00DDh_LXL_LUT0_data_221;                                  // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 88DDh
extern volatile uint8_t xdata g_rw_lxl_graylut_00DEh_LXL_LUT0_data_222;                                  // [msb:lsb] = [7:0], val = 192, 	Absolute Address = 88DEh
extern volatile uint8_t xdata g_rw_lxl_graylut_00DFh_LXL_LUT0_data_223;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 88DFh
extern volatile uint8_t xdata g_rw_lxl_graylut_00E0h_LXL_LUT0_data_224;                                  // [msb:lsb] = [7:0], val = 2, 	Absolute Address = 88E0h

extern volatile uint8_t xdata g_rw_lxl_graylut_00E1h_LXL_LUT0_data_225;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 88E1h
extern volatile uint8_t xdata g_rw_lxl_graylut_00E2h_LXL_LUT0_data_226;                                  // [msb:lsb] = [7:0], val = 208, 	Absolute Address = 88E2h
extern volatile uint8_t xdata g_rw_lxl_graylut_00E3h_LXL_LUT0_data_227;                                  // [msb:lsb] = [7:0], val = 207, 	Absolute Address = 88E3h
extern volatile uint8_t xdata g_rw_lxl_graylut_00E4h_LXL_LUT0_data_228;                                  // [msb:lsb] = [7:0], val = 61, 	Absolute Address = 88E4h
extern volatile uint8_t xdata g_rw_lxl_graylut_00E5h_LXL_LUT0_data_229;                                  // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 88E5h

extern volatile uint8_t xdata g_rw_lxl_graylut_00E6h_LXL_LUT0_data_230;                                  // [msb:lsb] = [7:0], val = 184, 	Absolute Address = 88E6h
extern volatile uint8_t xdata g_rw_lxl_graylut_00E7h_LXL_LUT0_data_231;                                  // [msb:lsb] = [7:0], val = 163, 	Absolute Address = 88E7h
extern volatile uint8_t xdata g_rw_lxl_graylut_00E8h_LXL_LUT0_data_232;                                  // [msb:lsb] = [7:0], val = 142, 	Absolute Address = 88E8h
extern volatile uint8_t xdata g_rw_lxl_graylut_00E9h_LXL_LUT0_data_233;                                  // [msb:lsb] = [7:0], val = 57, 	Absolute Address = 88E9h
extern volatile uint8_t xdata g_rw_lxl_graylut_00EAh_LXL_LUT0_data_234;                                  // [msb:lsb] = [7:0], val = 221, 	Absolute Address = 88EAh

extern volatile uint8_t xdata g_rw_lxl_graylut_00EBh_LXL_LUT0_data_235;                                  // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 88EBh
extern volatile uint8_t xdata g_rw_lxl_graylut_00ECh_LXL_LUT0_data_236;                                  // [msb:lsb] = [7:0], val = 227, 	Absolute Address = 88ECh
extern volatile uint8_t xdata g_rw_lxl_graylut_00EDh_LXL_LUT0_data_237;                                  // [msb:lsb] = [7:0], val = 204, 	Absolute Address = 88EDh
extern volatile uint8_t xdata g_rw_lxl_graylut_00EEh_LXL_LUT0_data_238;                                  // [msb:lsb] = [7:0], val = 20, 	Absolute Address = 88EEh
extern volatile uint8_t xdata g_rw_lxl_graylut_00EFh_LXL_LUT0_data_239;                                  // [msb:lsb] = [7:0], val = 65, 	Absolute Address = 88EFh

extern volatile uint8_t xdata g_rw_lxl_graylut_00F0h_LXL_LUT0_data_240;                                  // [msb:lsb] = [7:0], val = 212, 	Absolute Address = 88F0h
extern volatile uint8_t xdata g_rw_lxl_graylut_00F1h_LXL_LUT0_data_241;                                  // [msb:lsb] = [7:0], val = 208, 	Absolute Address = 88F1h
extern volatile uint8_t xdata g_rw_lxl_graylut_00F2h_LXL_LUT0_data_242;                                  // [msb:lsb] = [7:0], val = 2, 	Absolute Address = 88F2h
extern volatile uint8_t xdata g_rw_lxl_graylut_00F3h_LXL_LUT0_data_243;                                  // [msb:lsb] = [7:0], val = 10, 	Absolute Address = 88F3h
extern volatile uint8_t xdata g_rw_lxl_graylut_00F4h_LXL_LUT0_data_244;                                  // [msb:lsb] = [7:0], val = 15, 	Absolute Address = 88F4h

extern volatile uint8_t xdata g_rw_lxl_graylut_00F5h_LXL_LUT0_data_245;                                  // [msb:lsb] = [7:0], val = 52, 	Absolute Address = 88F5h
extern volatile uint8_t xdata g_rw_lxl_graylut_00F6h_LXL_LUT0_data_246;                                  // [msb:lsb] = [7:0], val = 160, 	Absolute Address = 88F6h
extern volatile uint8_t xdata g_rw_lxl_graylut_00F7h_LXL_LUT0_data_247;                                  // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 88F7h
extern volatile uint8_t xdata g_rw_lxl_graylut_00F8h_LXL_LUT0_data_248;                                  // [msb:lsb] = [7:0], val = 1, 	Absolute Address = 88F8h
extern volatile uint8_t xdata g_rw_lxl_graylut_00F9h_LXL_LUT0_data_249;                                  // [msb:lsb] = [7:0], val = 3, 	Absolute Address = 88F9h

extern volatile uint8_t xdata g_rw_lxl_graylut_00FAh_LXL_LUT0_data_250;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 88FAh
extern volatile uint8_t xdata g_rw_lxl_graylut_00FBh_LXL_LUT0_data_251;                                  // [msb:lsb] = [7:0], val = 224, 	Absolute Address = 88FBh
extern volatile uint8_t xdata g_rw_lxl_graylut_00FCh_LXL_LUT0_data_252;                                  // [msb:lsb] = [7:0], val = 207, 	Absolute Address = 88FCh
extern volatile uint8_t xdata g_rw_lxl_graylut_00FDh_LXL_LUT0_data_253;                                  // [msb:lsb] = [7:0], val = 62, 	Absolute Address = 88FDh
extern volatile uint8_t xdata g_rw_lxl_graylut_00FEh_LXL_LUT0_data_254;                                  // [msb:lsb] = [7:0], val = 251, 	Absolute Address = 88FEh

extern volatile uint8_t xdata g_rw_lxl_graylut_00FFh_LXL_LUT0_data_255;                                  // [msb:lsb] = [7:0], val = 200, 	Absolute Address = 88FFh
extern volatile uint8_t xdata g_rw_lxl_graylut_0100h_LXL_LUT0_data_256;                                  // [msb:lsb] = [7:0], val = 227, 	Absolute Address = 8900h
extern volatile uint8_t xdata g_rw_lxl_graylut_0101h_LXL_LUT0_data_257;                                  // [msb:lsb] = [7:0], val = 14, 	Absolute Address = 8901h
extern volatile uint8_t xdata g_rw_lxl_graylut_0102h_LXL_LUT0_data_258;                                  // [msb:lsb] = [7:0], val = 58, 	Absolute Address = 8902h
extern volatile uint8_t xdata g_rw_lxl_graylut_0103h_LXL_LUT0_data_259;                                  // [msb:lsb] = [7:0], val = 226, 	Absolute Address = 8903h

extern volatile uint8_t xdata g_rw_lxl_graylut_0104h_LXL_LUT0_data_260;                                  // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8904h
extern volatile uint8_t xdata g_rw_lxl_graylut_0105h_LXL_LUT0_data_261;                                  // [msb:lsb] = [7:0], val = 179, 	Absolute Address = 8905h
extern volatile uint8_t xdata g_rw_lxl_graylut_0106h_LXL_LUT0_data_262;                                  // [msb:lsb] = [7:0], val = 196, 	Absolute Address = 8906h
extern volatile uint8_t xdata g_rw_lxl_graylut_0107h_LXL_LUT0_data_263;                                  // [msb:lsb] = [7:0], val = 15, 	Absolute Address = 8907h
extern volatile uint8_t xdata g_rw_lxl_graylut_0108h_LXL_LUT0_data_264;                                  // [msb:lsb] = [7:0], val = 50, 	Absolute Address = 8908h

extern volatile uint8_t xdata g_rw_lxl_graylut_0109h_LXL_LUT0_data_265;                                  // [msb:lsb] = [7:0], val = 160, 	Absolute Address = 8909h
extern volatile uint8_t xdata g_rw_lxl_graylut_010Ah_LXL_LUT0_data_266;                                  // [msb:lsb] = [7:0], val = 48, 	Absolute Address = 890Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_010Bh_LXL_LUT0_data_267;                                  // [msb:lsb] = [7:0], val = 66, 	Absolute Address = 890Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_010Ch_LXL_LUT0_data_268;                                  // [msb:lsb] = [7:0], val = 3, 	Absolute Address = 890Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_010Dh_LXL_LUT0_data_269;                                  // [msb:lsb] = [7:0], val = 11, 	Absolute Address = 890Dh

extern volatile uint8_t xdata g_rw_lxl_graylut_010Eh_LXL_LUT0_data_270;                                  // [msb:lsb] = [7:0], val = 24, 	Absolute Address = 890Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_010Fh_LXL_LUT0_data_271;                                  // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 890Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0110h_LXL_LUT0_data_272;                                  // [msb:lsb] = [7:0], val = 192, 	Absolute Address = 8910h
extern volatile uint8_t xdata g_rw_lxl_graylut_0111h_LXL_LUT0_data_273;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8911h
extern volatile uint8_t xdata g_rw_lxl_graylut_0112h_LXL_LUT0_data_274;                                  // [msb:lsb] = [7:0], val = 3, 	Absolute Address = 8912h

extern volatile uint8_t xdata g_rw_lxl_graylut_0113h_LXL_LUT0_data_275;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8913h
extern volatile uint8_t xdata g_rw_lxl_graylut_0114h_LXL_LUT0_data_276;                                  // [msb:lsb] = [7:0], val = 224, 	Absolute Address = 8914h
extern volatile uint8_t xdata g_rw_lxl_graylut_0115h_LXL_LUT0_data_277;                                  // [msb:lsb] = [7:0], val = 143, 	Absolute Address = 8915h
extern volatile uint8_t xdata g_rw_lxl_graylut_0116h_LXL_LUT0_data_278;                                  // [msb:lsb] = [7:0], val = 62, 	Absolute Address = 8916h
extern volatile uint8_t xdata g_rw_lxl_graylut_0117h_LXL_LUT0_data_279;                                  // [msb:lsb] = [7:0], val = 246, 	Absolute Address = 8917h

extern volatile uint8_t xdata g_rw_lxl_graylut_0118h_LXL_LUT0_data_280;                                  // [msb:lsb] = [7:0], val = 196, 	Absolute Address = 8918h
extern volatile uint8_t xdata g_rw_lxl_graylut_0119h_LXL_LUT0_data_281;                                  // [msb:lsb] = [7:0], val = 131, 	Absolute Address = 8919h
extern volatile uint8_t xdata g_rw_lxl_graylut_011Ah_LXL_LUT0_data_282;                                  // [msb:lsb] = [7:0], val = 14, 	Absolute Address = 891Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_011Bh_LXL_LUT0_data_283;                                  // [msb:lsb] = [7:0], val = 57, 	Absolute Address = 891Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_011Ch_LXL_LUT0_data_284;                                  // [msb:lsb] = [7:0], val = 214, 	Absolute Address = 891Ch

extern volatile uint8_t xdata g_rw_lxl_graylut_011Dh_LXL_LUT0_data_285;                                  // [msb:lsb] = [7:0], val = 24, 	Absolute Address = 891Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_011Eh_LXL_LUT0_data_286;                                  // [msb:lsb] = [7:0], val = 193, 	Absolute Address = 891Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_011Fh_LXL_LUT0_data_287;                                  // [msb:lsb] = [7:0], val = 3, 	Absolute Address = 891Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0120h_LXL_LUT0_data_288;                                  // [msb:lsb] = [7:0], val = 10, 	Absolute Address = 8920h
extern volatile uint8_t xdata g_rw_lxl_graylut_0121h_LXL_LUT0_data_289;                                  // [msb:lsb] = [7:0], val = 35, 	Absolute Address = 8921h

extern volatile uint8_t xdata g_rw_lxl_graylut_0122h_LXL_LUT0_data_290;                                  // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 8922h
extern volatile uint8_t xdata g_rw_lxl_graylut_0123h_LXL_LUT0_data_291;                                  // [msb:lsb] = [7:0], val = 176, 	Absolute Address = 8923h
extern volatile uint8_t xdata g_rw_lxl_graylut_0124h_LXL_LUT0_data_292;                                  // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 8924h
extern volatile uint8_t xdata g_rw_lxl_graylut_0125h_LXL_LUT0_data_293;                                  // [msb:lsb] = [7:0], val = 2, 	Absolute Address = 8925h
extern volatile uint8_t xdata g_rw_lxl_graylut_0126h_LXL_LUT0_data_294;                                  // [msb:lsb] = [7:0], val = 7, 	Absolute Address = 8926h

extern volatile uint8_t xdata g_rw_lxl_graylut_0127h_LXL_LUT0_data_295;                                  // [msb:lsb] = [7:0], val = 12, 	Absolute Address = 8927h
extern volatile uint8_t xdata g_rw_lxl_graylut_0128h_LXL_LUT0_data_296;                                  // [msb:lsb] = [7:0], val = 32, 	Absolute Address = 8928h
extern volatile uint8_t xdata g_rw_lxl_graylut_0129h_LXL_LUT0_data_297;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 8929h
extern volatile uint8_t xdata g_rw_lxl_graylut_012Ah_LXL_LUT0_data_298;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 892Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_012Bh_LXL_LUT0_data_299;                                  // [msb:lsb] = [7:0], val = 1, 	Absolute Address = 892Bh

extern volatile uint8_t xdata g_rw_lxl_graylut_012Ch_LXL_LUT0_data_300;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 892Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_012Dh_LXL_LUT0_data_301;                                  // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 892Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_012Eh_LXL_LUT0_data_302;                                  // [msb:lsb] = [7:0], val = 15, 	Absolute Address = 892Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_012Fh_LXL_LUT0_data_303;                                  // [msb:lsb] = [7:0], val = 62, 	Absolute Address = 892Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0130h_LXL_LUT0_data_304;                                  // [msb:lsb] = [7:0], val = 244, 	Absolute Address = 8930h

extern volatile uint8_t xdata g_rw_lxl_graylut_0131h_LXL_LUT0_data_305;                                  // [msb:lsb] = [7:0], val = 176, 	Absolute Address = 8931h
extern volatile uint8_t xdata g_rw_lxl_graylut_0132h_LXL_LUT0_data_306;                                  // [msb:lsb] = [7:0], val = 131, 	Absolute Address = 8932h
extern volatile uint8_t xdata g_rw_lxl_graylut_0133h_LXL_LUT0_data_307;                                  // [msb:lsb] = [7:0], val = 14, 	Absolute Address = 8933h
extern volatile uint8_t xdata g_rw_lxl_graylut_0134h_LXL_LUT0_data_308;                                  // [msb:lsb] = [7:0], val = 54, 	Absolute Address = 8934h
extern volatile uint8_t xdata g_rw_lxl_graylut_0135h_LXL_LUT0_data_309;                                  // [msb:lsb] = [7:0], val = 60, 	Absolute Address = 8935h

extern volatile uint8_t xdata g_rw_lxl_graylut_0136h_LXL_LUT0_data_310;                                  // [msb:lsb] = [7:0], val = 220, 	Absolute Address = 8936h
extern volatile uint8_t xdata g_rw_lxl_graylut_0137h_LXL_LUT0_data_311;                                  // [msb:lsb] = [7:0], val = 48, 	Absolute Address = 8937h
extern volatile uint8_t xdata g_rw_lxl_graylut_0138h_LXL_LUT0_data_312;                                  // [msb:lsb] = [7:0], val = 66, 	Absolute Address = 8938h
extern volatile uint8_t xdata g_rw_lxl_graylut_0139h_LXL_LUT0_data_313;                                  // [msb:lsb] = [7:0], val = 8, 	Absolute Address = 8939h
extern volatile uint8_t xdata g_rw_lxl_graylut_013Ah_LXL_LUT0_data_314;                                  // [msb:lsb] = [7:0], val = 28, 	Absolute Address = 893Ah

extern volatile uint8_t xdata g_rw_lxl_graylut_013Bh_LXL_LUT0_data_315;                                  // [msb:lsb] = [7:0], val = 36, 	Absolute Address = 893Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_013Ch_LXL_LUT0_data_316;                                  // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 893Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_013Dh_LXL_LUT0_data_317;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 893Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_013Eh_LXL_LUT0_data_318;                                  // [msb:lsb] = [7:0], val = 1, 	Absolute Address = 893Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_013Fh_LXL_LUT0_data_319;                                  // [msb:lsb] = [7:0], val = 2, 	Absolute Address = 893Fh

extern volatile uint8_t xdata g_rw_lxl_graylut_0140h_LXL_LUT0_data_320;                                  // [msb:lsb] = [7:0], val = 4, 	Absolute Address = 8940h
extern volatile uint8_t xdata g_rw_lxl_graylut_0141h_LXL_LUT0_data_321;                                  // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8941h
extern volatile uint8_t xdata g_rw_lxl_graylut_0142h_LXL_LUT0_data_322;                                  // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 8942h
extern volatile uint8_t xdata g_rw_lxl_graylut_0143h_LXL_LUT0_data_323;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8943h
extern volatile uint8_t xdata g_rw_lxl_graylut_0144h_LXL_LUT0_data_324;                                  // [msb:lsb] = [7:0], val = 1, 	Absolute Address = 8944h

extern volatile uint8_t xdata g_rw_lxl_graylut_0145h_LXL_LUT0_data_325;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8945h
extern volatile uint8_t xdata g_rw_lxl_graylut_0146h_LXL_LUT0_data_326;                                  // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 8946h
extern volatile uint8_t xdata g_rw_lxl_graylut_0147h_LXL_LUT0_data_327;                                  // [msb:lsb] = [7:0], val = 143, 	Absolute Address = 8947h
extern volatile uint8_t xdata g_rw_lxl_graylut_0148h_LXL_LUT0_data_328;                                  // [msb:lsb] = [7:0], val = 61, 	Absolute Address = 8948h
extern volatile uint8_t xdata g_rw_lxl_graylut_0149h_LXL_LUT0_data_329;                                  // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 8949h

extern volatile uint8_t xdata g_rw_lxl_graylut_014Ah_LXL_LUT0_data_330;                                  // [msb:lsb] = [7:0], val = 184, 	Absolute Address = 894Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_014Bh_LXL_LUT0_data_331;                                  // [msb:lsb] = [7:0], val = 19, 	Absolute Address = 894Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_014Ch_LXL_LUT0_data_332;                                  // [msb:lsb] = [7:0], val = 78, 	Absolute Address = 894Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_014Dh_LXL_LUT0_data_333;                                  // [msb:lsb] = [7:0], val = 11, 	Absolute Address = 894Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_014Eh_LXL_LUT0_data_334;                                  // [msb:lsb] = [7:0], val = 40, 	Absolute Address = 894Eh

extern volatile uint8_t xdata g_rw_lxl_graylut_014Fh_LXL_LUT0_data_335;                                  // [msb:lsb] = [7:0], val = 140, 	Absolute Address = 894Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0150h_LXL_LUT0_data_336;                                  // [msb:lsb] = [7:0], val = 224, 	Absolute Address = 8950h
extern volatile uint8_t xdata g_rw_lxl_graylut_0151h_LXL_LUT0_data_337;                                  // [msb:lsb] = [7:0], val = 65, 	Absolute Address = 8951h
extern volatile uint8_t xdata g_rw_lxl_graylut_0152h_LXL_LUT0_data_338;                                  // [msb:lsb] = [7:0], val = 6, 	Absolute Address = 8952h
extern volatile uint8_t xdata g_rw_lxl_graylut_0153h_LXL_LUT0_data_339;                                  // [msb:lsb] = [7:0], val = 7, 	Absolute Address = 8953h

extern volatile uint8_t xdata g_rw_lxl_graylut_0154h_LXL_LUT0_data_340;                                  // [msb:lsb] = [7:0], val = 28, 	Absolute Address = 8954h
extern volatile uint8_t xdata g_rw_lxl_graylut_0155h_LXL_LUT0_data_341;                                  // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 8955h
extern volatile uint8_t xdata g_rw_lxl_graylut_0156h_LXL_LUT0_data_342;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 8956h
extern volatile uint8_t xdata g_rw_lxl_graylut_0157h_LXL_LUT0_data_343;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8957h
extern volatile uint8_t xdata g_rw_lxl_graylut_0158h_LXL_LUT0_data_344;                                  // [msb:lsb] = [7:0], val = 1, 	Absolute Address = 8958h

extern volatile uint8_t xdata g_rw_lxl_graylut_0159h_LXL_LUT0_data_345;                                  // [msb:lsb] = [7:0], val = 4, 	Absolute Address = 8959h
extern volatile uint8_t xdata g_rw_lxl_graylut_015Ah_LXL_LUT0_data_346;                                  // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 895Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_015Bh_LXL_LUT0_data_347;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 895Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_015Ch_LXL_LUT0_data_348;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 895Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_015Dh_LXL_LUT0_data_349;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 895Dh

extern volatile uint8_t xdata g_rw_lxl_graylut_015Eh_LXL_LUT0_data_350;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 895Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_015Fh_LXL_LUT0_data_351;                                  // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 895Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0160h_LXL_LUT0_data_352;                                  // [msb:lsb] = [7:0], val = 207, 	Absolute Address = 8960h
extern volatile uint8_t xdata g_rw_lxl_graylut_0161h_LXL_LUT0_data_353;                                  // [msb:lsb] = [7:0], val = 61, 	Absolute Address = 8961h
extern volatile uint8_t xdata g_rw_lxl_graylut_0162h_LXL_LUT0_data_354;                                  // [msb:lsb] = [7:0], val = 245, 	Absolute Address = 8962h

extern volatile uint8_t xdata g_rw_lxl_graylut_0163h_LXL_LUT0_data_355;                                  // [msb:lsb] = [7:0], val = 148, 	Absolute Address = 8963h
extern volatile uint8_t xdata g_rw_lxl_graylut_0164h_LXL_LUT0_data_356;                                  // [msb:lsb] = [7:0], val = 227, 	Absolute Address = 8964h
extern volatile uint8_t xdata g_rw_lxl_graylut_0165h_LXL_LUT0_data_357;                                  // [msb:lsb] = [7:0], val = 1, 	Absolute Address = 8965h
extern volatile uint8_t xdata g_rw_lxl_graylut_0166h_LXL_LUT0_data_358;                                  // [msb:lsb] = [7:0], val = 7, 	Absolute Address = 8966h
extern volatile uint8_t xdata g_rw_lxl_graylut_0167h_LXL_LUT0_data_359;                                  // [msb:lsb] = [7:0], val = 25, 	Absolute Address = 8967h

extern volatile uint8_t xdata g_rw_lxl_graylut_0168h_LXL_LUT0_data_360;                                  // [msb:lsb] = [7:0], val = 72, 	Absolute Address = 8968h
extern volatile uint8_t xdata g_rw_lxl_graylut_0169h_LXL_LUT0_data_361;                                  // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 8969h
extern volatile uint8_t xdata g_rw_lxl_graylut_016Ah_LXL_LUT0_data_362;                                  // [msb:lsb] = [7:0], val = 192, 	Absolute Address = 896Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_016Bh_LXL_LUT0_data_363;                                  // [msb:lsb] = [7:0], val = 1, 	Absolute Address = 896Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_016Ch_LXL_LUT0_data_364;                                  // [msb:lsb] = [7:0], val = 6, 	Absolute Address = 896Ch

extern volatile uint8_t xdata g_rw_lxl_graylut_016Dh_LXL_LUT0_data_365;                                  // [msb:lsb] = [7:0], val = 20, 	Absolute Address = 896Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_016Eh_LXL_LUT0_data_366;                                  // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 896Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_016Fh_LXL_LUT0_data_367;                                  // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 896Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0170h_LXL_LUT0_data_368;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8970h
extern volatile uint8_t xdata g_rw_lxl_graylut_0171h_LXL_LUT0_data_369;                                  // [msb:lsb] = [7:0], val = 1, 	Absolute Address = 8971h

extern volatile uint8_t xdata g_rw_lxl_graylut_0172h_LXL_LUT0_data_370;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8972h
extern volatile uint8_t xdata g_rw_lxl_graylut_0173h_LXL_LUT0_data_371;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8973h
extern volatile uint8_t xdata g_rw_lxl_graylut_0174h_LXL_LUT0_data_372;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8974h
extern volatile uint8_t xdata g_rw_lxl_graylut_0175h_LXL_LUT0_data_373;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8975h
extern volatile uint8_t xdata g_rw_lxl_graylut_0176h_LXL_LUT0_data_374;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8976h

extern volatile uint8_t xdata g_rw_lxl_graylut_0177h_LXL_LUT0_data_375;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8977h
extern volatile uint8_t xdata g_rw_lxl_graylut_0178h_LXL_LUT0_data_376;                                  // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 8978h
extern volatile uint8_t xdata g_rw_lxl_graylut_0179h_LXL_LUT0_data_377;                                  // [msb:lsb] = [7:0], val = 207, 	Absolute Address = 8979h
extern volatile uint8_t xdata g_rw_lxl_graylut_017Ah_LXL_LUT0_data_378;                                  // [msb:lsb] = [7:0], val = 60, 	Absolute Address = 897Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_017Bh_LXL_LUT0_data_379;                                  // [msb:lsb] = [7:0], val = 231, 	Absolute Address = 897Bh

extern volatile uint8_t xdata g_rw_lxl_graylut_017Ch_LXL_LUT0_data_380;                                  // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 897Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_017Dh_LXL_LUT0_data_381;                                  // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 897Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_017Eh_LXL_LUT0_data_382;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 897Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_017Fh_LXL_LUT0_data_383;                                  // [msb:lsb] = [7:0], val = 3, 	Absolute Address = 897Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0180h_LXL_LUT0_data_384;                                  // [msb:lsb] = [7:0], val = 11, 	Absolute Address = 8980h

extern volatile uint8_t xdata g_rw_lxl_graylut_0181h_LXL_LUT0_data_385;                                  // [msb:lsb] = [7:0], val = 32, 	Absolute Address = 8981h
extern volatile uint8_t xdata g_rw_lxl_graylut_0182h_LXL_LUT0_data_386;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 8982h
extern volatile uint8_t xdata g_rw_lxl_graylut_0183h_LXL_LUT0_data_387;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 8983h
extern volatile uint8_t xdata g_rw_lxl_graylut_0184h_LXL_LUT0_data_388;                                  // [msb:lsb] = [7:0], val = 1, 	Absolute Address = 8984h
extern volatile uint8_t xdata g_rw_lxl_graylut_0185h_LXL_LUT0_data_389;                                  // [msb:lsb] = [7:0], val = 4, 	Absolute Address = 8985h

extern volatile uint8_t xdata g_rw_lxl_graylut_0186h_LXL_LUT0_data_390;                                  // [msb:lsb] = [7:0], val = 4, 	Absolute Address = 8986h
extern volatile uint8_t xdata g_rw_lxl_graylut_0187h_LXL_LUT0_data_391;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8987h
extern volatile uint8_t xdata g_rw_lxl_graylut_0188h_LXL_LUT0_data_392;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8988h
extern volatile uint8_t xdata g_rw_lxl_graylut_0189h_LXL_LUT0_data_393;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8989h
extern volatile uint8_t xdata g_rw_lxl_graylut_018Ah_LXL_LUT0_data_394;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 898Ah

extern volatile uint8_t xdata g_rw_lxl_graylut_018Bh_LXL_LUT0_data_395;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 898Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_018Ch_LXL_LUT0_data_396;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 898Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_018Dh_LXL_LUT0_data_397;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 898Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_018Eh_LXL_LUT0_data_398;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 898Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_018Fh_LXL_LUT0_data_399;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 898Fh

extern volatile uint8_t xdata g_rw_lxl_graylut_0190h_LXL_LUT0_data_400;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8990h
extern volatile uint8_t xdata g_rw_lxl_graylut_0191h_LXL_LUT0_data_401;                                  // [msb:lsb] = [7:0], val = 32, 	Absolute Address = 8991h
extern volatile uint8_t xdata g_rw_lxl_graylut_0192h_LXL_LUT0_data_402;                                  // [msb:lsb] = [7:0], val = 15, 	Absolute Address = 8992h
extern volatile uint8_t xdata g_rw_lxl_graylut_0193h_LXL_LUT0_data_403;                                  // [msb:lsb] = [7:0], val = 58, 	Absolute Address = 8993h
extern volatile uint8_t xdata g_rw_lxl_graylut_0194h_LXL_LUT0_data_404;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8994h

extern volatile uint8_t xdata g_rw_lxl_graylut_0195h_LXL_LUT0_data_405;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8995h
extern volatile uint8_t xdata g_rw_lxl_graylut_0196h_LXL_LUT0_data_406;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8996h
extern volatile uint8_t xdata g_rw_lxl_graylut_0197h_LXL_LUT0_data_407;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8997h
extern volatile uint8_t xdata g_rw_lxl_graylut_0198h_LXL_LUT0_data_408;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8998h
extern volatile uint8_t xdata g_rw_lxl_graylut_0199h_LXL_LUT0_data_409;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8999h

extern volatile uint8_t xdata g_rw_lxl_graylut_019Ah_LXL_LUT0_data_410;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 899Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_019Bh_LXL_LUT0_data_411;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 899Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_019Ch_LXL_LUT0_data_412;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 899Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_019Dh_LXL_LUT0_data_413;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 899Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_019Eh_LXL_LUT0_data_414;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 899Eh

extern volatile uint8_t xdata g_rw_lxl_graylut_019Fh_LXL_LUT0_data_415;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 899Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_01A0h_LXL_LUT0_data_416;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89A0h
extern volatile uint8_t xdata g_rw_lxl_graylut_01A1h_LXL_LUT0_data_417;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89A1h
extern volatile uint8_t xdata g_rw_lxl_graylut_01A2h_LXL_LUT0_data_418;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89A2h
extern volatile uint8_t xdata g_rw_lxl_graylut_01A3h_LXL_LUT0_data_419;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89A3h

extern volatile uint8_t xdata g_rw_lxl_graylut_01A4h_LXL_LUT0_data_420;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89A4h
extern volatile uint8_t xdata g_rw_lxl_graylut_01A5h_LXL_LUT0_data_421;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89A5h
extern volatile uint8_t xdata g_rw_lxl_graylut_01A6h_LXL_LUT0_data_422;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89A6h
extern volatile uint8_t xdata g_rw_lxl_graylut_01A7h_LXL_LUT0_data_423;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89A7h
extern volatile uint8_t xdata g_rw_lxl_graylut_01A8h_LXL_LUT0_data_424;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89A8h

extern volatile uint8_t xdata g_rw_lxl_graylut_01A9h_LXL_LUT0_data_425;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89A9h
extern volatile uint8_t xdata g_rw_lxl_graylut_01AAh_LXL_LUT0_data_426;                                  // [msb:lsb] = [7:0], val = 160, 	Absolute Address = 89AAh
extern volatile uint8_t xdata g_rw_lxl_graylut_01ABh_LXL_LUT0_data_427;                                  // [msb:lsb] = [7:0], val = 14, 	Absolute Address = 89ABh
extern volatile uint8_t xdata g_rw_lxl_graylut_01ACh_LXL_LUT0_data_428;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89ACh
extern volatile uint8_t xdata g_rw_lxl_graylut_01ADh_LXL_LUT0_data_429;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89ADh

extern volatile uint8_t xdata g_rw_lxl_graylut_01AEh_LXL_LUT0_data_430;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89AEh
extern volatile uint8_t xdata g_rw_lxl_graylut_01AFh_LXL_LUT0_data_431;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89AFh
extern volatile uint8_t xdata g_rw_lxl_graylut_01B0h_LXL_LUT0_data_432;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89B0h
extern volatile uint8_t xdata g_rw_lxl_graylut_01B1h_LXL_LUT0_data_433;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89B1h
extern volatile uint8_t xdata g_rw_lxl_graylut_01B2h_LXL_LUT0_data_434;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89B2h

extern volatile uint8_t xdata g_rw_lxl_graylut_01B3h_LXL_LUT0_data_435;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89B3h
extern volatile uint8_t xdata g_rw_lxl_graylut_01B4h_LXL_LUT0_data_436;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89B4h
extern volatile uint8_t xdata g_rw_lxl_graylut_01B5h_LXL_LUT0_data_437;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89B5h
extern volatile uint8_t xdata g_rw_lxl_graylut_01B6h_LXL_LUT0_data_438;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89B6h
extern volatile uint8_t xdata g_rw_lxl_graylut_01B7h_LXL_LUT0_data_439;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89B7h

extern volatile uint8_t xdata g_rw_lxl_graylut_01B8h_LXL_LUT0_data_440;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89B8h
extern volatile uint8_t xdata g_rw_lxl_graylut_01B9h_LXL_LUT0_data_441;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89B9h
extern volatile uint8_t xdata g_rw_lxl_graylut_01BAh_LXL_LUT0_data_442;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89BAh
extern volatile uint8_t xdata g_rw_lxl_graylut_01BBh_LXL_LUT0_data_443;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89BBh
extern volatile uint8_t xdata g_rw_lxl_graylut_01BCh_LXL_LUT0_data_444;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89BCh

extern volatile uint8_t xdata g_rw_lxl_graylut_01BDh_LXL_LUT0_data_445;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89BDh
extern volatile uint8_t xdata g_rw_lxl_graylut_01BEh_LXL_LUT0_data_446;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89BEh
extern volatile uint8_t xdata g_rw_lxl_graylut_01BFh_LXL_LUT0_data_447;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89BFh
extern volatile uint8_t xdata g_rw_lxl_graylut_01C0h_LXL_LUT0_data_448;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89C0h
extern volatile uint8_t xdata g_rw_lxl_graylut_01C1h_LXL_LUT0_data_449;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89C1h

extern volatile uint8_t xdata g_rw_lxl_graylut_01C2h_LXL_LUT0_data_450;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89C2h
extern volatile uint8_t xdata g_rw_lxl_graylut_01C3h_LXL_LUT0_data_451;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89C3h
extern volatile uint8_t xdata g_rw_lxl_graylut_01C4h_LXL_LUT0_data_452;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89C4h
extern volatile uint8_t xdata g_rw_lxl_graylut_01C5h_LXL_LUT0_data_453;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89C5h
extern volatile uint8_t xdata g_rw_lxl_graylut_01C6h_LXL_LUT0_data_454;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89C6h

extern volatile uint8_t xdata g_rw_lxl_graylut_01C7h_LXL_LUT0_data_455;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89C7h
extern volatile uint8_t xdata g_rw_lxl_graylut_01C8h_LXL_LUT0_data_456;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89C8h
extern volatile uint8_t xdata g_rw_lxl_graylut_01C9h_LXL_LUT0_data_457;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89C9h
extern volatile uint8_t xdata g_rw_lxl_graylut_01CAh_LXL_LUT0_data_458;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89CAh
extern volatile uint8_t xdata g_rw_lxl_graylut_01CBh_LXL_LUT0_data_459;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89CBh

extern volatile uint8_t xdata g_rw_lxl_graylut_01CCh_LXL_LUT0_data_460;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89CCh
extern volatile uint8_t xdata g_rw_lxl_graylut_01CDh_LXL_LUT0_data_461;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89CDh
extern volatile uint8_t xdata g_rw_lxl_graylut_01CEh_LXL_LUT0_data_462;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89CEh
extern volatile uint8_t xdata g_rw_lxl_graylut_01CFh_LXL_LUT0_data_463;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89CFh
extern volatile uint8_t xdata g_rw_lxl_graylut_01D0h_LXL_LUT0_data_464;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89D0h

extern volatile uint8_t xdata g_rw_lxl_graylut_01D1h_LXL_LUT0_data_465;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89D1h
extern volatile uint8_t xdata g_rw_lxl_graylut_01D2h_LXL_LUT0_data_466;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89D2h
extern volatile uint8_t xdata g_rw_lxl_graylut_01D3h_LXL_LUT0_data_467;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89D3h
extern volatile uint8_t xdata g_rw_lxl_graylut_01D4h_LXL_LUT0_data_468;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89D4h
extern volatile uint8_t xdata g_rw_lxl_graylut_01D5h_LXL_LUT0_data_469;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89D5h

extern volatile uint8_t xdata g_rw_lxl_graylut_01D6h_LXL_LUT0_data_470;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89D6h
extern volatile uint8_t xdata g_rw_lxl_graylut_01D7h_LXL_LUT0_data_471;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89D7h
extern volatile uint8_t xdata g_rw_lxl_graylut_01D8h_LXL_LUT0_data_472;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89D8h
extern volatile uint8_t xdata g_rw_lxl_graylut_01D9h_LXL_LUT0_data_473;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89D9h
extern volatile uint8_t xdata g_rw_lxl_graylut_01DAh_LXL_LUT0_data_474;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89DAh

extern volatile uint8_t xdata g_rw_lxl_graylut_01DBh_LXL_LUT0_data_475;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89DBh
extern volatile uint8_t xdata g_rw_lxl_graylut_01DCh_LXL_LUT0_data_476;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89DCh
extern volatile uint8_t xdata g_rw_lxl_graylut_01DDh_LXL_LUT0_data_477;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89DDh
extern volatile uint8_t xdata g_rw_lxl_graylut_01DEh_LXL_LUT0_data_478;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89DEh
extern volatile uint8_t xdata g_rw_lxl_graylut_01DFh_LXL_LUT0_data_479;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89DFh

extern volatile uint8_t xdata g_rw_lxl_graylut_01E0h_LXL_LUT0_data_480;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89E0h
extern volatile uint8_t xdata g_rw_lxl_graylut_01E1h_LXL_LUT0_data_481;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89E1h
extern volatile uint8_t xdata g_rw_lxl_graylut_01E2h_LXL_LUT0_data_482;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89E2h
extern volatile uint8_t xdata g_rw_lxl_graylut_01E3h_LXL_LUT0_data_483;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89E3h
extern volatile uint8_t xdata g_rw_lxl_graylut_01E4h_LXL_LUT0_data_484;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89E4h

extern volatile uint8_t xdata g_rw_lxl_graylut_01E5h_LXL_LUT0_data_485;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89E5h
extern volatile uint8_t xdata g_rw_lxl_graylut_01E6h_LXL_LUT0_data_486;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89E6h
extern volatile uint8_t xdata g_rw_lxl_graylut_01E7h_LXL_LUT0_data_487;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89E7h
extern volatile uint8_t xdata g_rw_lxl_graylut_01E8h_LXL_LUT0_data_488;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89E8h
extern volatile uint8_t xdata g_rw_lxl_graylut_01E9h_LXL_LUT0_data_489;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89E9h

extern volatile uint8_t xdata g_rw_lxl_graylut_01EAh_LXL_LUT0_data_490;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89EAh
extern volatile uint8_t xdata g_rw_lxl_graylut_01EBh_LXL_LUT0_data_491;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89EBh
extern volatile uint8_t xdata g_rw_lxl_graylut_01ECh_LXL_LUT0_data_492;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89ECh
extern volatile uint8_t xdata g_rw_lxl_graylut_01EDh_LXL_LUT0_data_493;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89EDh
extern volatile uint8_t xdata g_rw_lxl_graylut_01EEh_LXL_LUT0_data_494;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89EEh

extern volatile uint8_t xdata g_rw_lxl_graylut_01EFh_LXL_LUT0_data_495;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89EFh
extern volatile uint8_t xdata g_rw_lxl_graylut_01F0h_LXL_LUT0_data_496;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89F0h
extern volatile uint8_t xdata g_rw_lxl_graylut_01F1h_LXL_LUT0_data_497;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89F1h
extern volatile uint8_t xdata g_rw_lxl_graylut_01F2h_LXL_LUT0_data_498;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89F2h
extern volatile uint8_t xdata g_rw_lxl_graylut_01F3h_LXL_LUT0_data_499;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89F3h

extern volatile uint8_t xdata g_rw_lxl_graylut_01F4h_LXL_LUT0_data_500;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89F4h
extern volatile uint8_t xdata g_rw_lxl_graylut_01F5h_LXL_LUT0_data_501;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89F5h
extern volatile uint8_t xdata g_rw_lxl_graylut_01F6h_LXL_LUT0_data_502;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89F6h
extern volatile uint8_t xdata g_rw_lxl_graylut_01F7h_LXL_LUT0_data_503;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89F7h
extern volatile uint8_t xdata g_rw_lxl_graylut_01F8h_LXL_LUT0_data_504;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89F8h

extern volatile uint8_t xdata g_rw_lxl_graylut_01F9h_LXL_LUT0_data_505;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89F9h
extern volatile uint8_t xdata g_rw_lxl_graylut_01FAh_LXL_LUT0_data_506;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89FAh
extern volatile uint8_t xdata g_rw_lxl_graylut_01FBh_LXL_LUT0_data_507;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89FBh
extern volatile uint8_t xdata g_rw_lxl_graylut_01FCh_LXL_LUT0_data_508;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89FCh
extern volatile uint8_t xdata g_rw_lxl_graylut_01FDh_LXL_LUT0_data_509;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89FDh

extern volatile uint8_t xdata g_rw_lxl_graylut_01FEh_LXL_LUT0_data_510;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89FEh
extern volatile uint8_t xdata g_rw_lxl_graylut_01FFh_LXL_LUT0_data_511;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 89FFh
extern volatile uint8_t xdata g_rw_lxl_graylut_0200h_LXL_LUT0_data_512;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A00h
extern volatile uint8_t xdata g_rw_lxl_graylut_0201h_LXL_LUT0_data_513;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A01h
extern volatile uint8_t xdata g_rw_lxl_graylut_0202h_LXL_LUT0_data_514;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A02h

extern volatile uint8_t xdata g_rw_lxl_graylut_0203h_LXL_LUT0_data_515;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A03h
extern volatile uint8_t xdata g_rw_lxl_graylut_0204h_LXL_LUT0_data_516;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A04h
extern volatile uint8_t xdata g_rw_lxl_graylut_0205h_LXL_LUT0_data_517;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A05h
extern volatile uint8_t xdata g_rw_lxl_graylut_0206h_LXL_LUT0_data_518;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A06h
extern volatile uint8_t xdata g_rw_lxl_graylut_0207h_LXL_LUT0_data_519;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A07h

extern volatile uint8_t xdata g_rw_lxl_graylut_0208h_LXL_LUT0_data_520;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A08h
extern volatile uint8_t xdata g_rw_lxl_graylut_0209h_LXL_LUT0_data_521;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A09h
extern volatile uint8_t xdata g_rw_lxl_graylut_020Ah_LXL_LUT0_data_522;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A0Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_020Bh_LXL_LUT0_data_523;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A0Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_020Ch_LXL_LUT0_data_524;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A0Ch

extern volatile uint8_t xdata g_rw_lxl_graylut_020Dh_LXL_LUT0_data_525;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A0Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_020Eh_LXL_LUT0_data_526;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A0Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_020Fh_LXL_LUT0_data_527;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A0Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0210h_LXL_LUT0_data_528;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A10h
extern volatile uint8_t xdata g_rw_lxl_graylut_0211h_LXL_LUT0_data_529;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A11h

extern volatile uint8_t xdata g_rw_lxl_graylut_0212h_LXL_LUT0_data_530;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A12h
extern volatile uint8_t xdata g_rw_lxl_graylut_0213h_LXL_LUT0_data_531;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A13h
extern volatile uint8_t xdata g_rw_lxl_graylut_0214h_LXL_LUT0_data_532;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A14h
extern volatile uint8_t xdata g_rw_lxl_graylut_0215h_LXL_LUT0_data_533;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A15h
extern volatile uint8_t xdata g_rw_lxl_graylut_0216h_LXL_LUT0_data_534;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A16h

extern volatile uint8_t xdata g_rw_lxl_graylut_0217h_LXL_LUT0_data_535;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A17h
extern volatile uint8_t xdata g_rw_lxl_graylut_0218h_LXL_LUT0_data_536;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A18h
extern volatile uint8_t xdata g_rw_lxl_graylut_0219h_LXL_LUT0_data_537;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A19h
extern volatile uint8_t xdata g_rw_lxl_graylut_021Ah_LXL_LUT0_data_538;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A1Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_021Bh_LXL_LUT0_data_539;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A1Bh

extern volatile uint8_t xdata g_rw_lxl_graylut_021Ch_LXL_LUT0_data_540;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A1Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_021Dh_LXL_LUT0_data_541;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A1Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_021Eh_LXL_LUT0_data_542;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A1Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_021Fh_LXL_LUT0_data_543;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A1Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0220h_LXL_LUT0_data_544;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A20h

extern volatile uint8_t xdata g_rw_lxl_graylut_0221h_LXL_LUT0_data_545;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A21h
extern volatile uint8_t xdata g_rw_lxl_graylut_0222h_LXL_LUT0_data_546;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A22h
extern volatile uint8_t xdata g_rw_lxl_graylut_0223h_LXL_LUT0_data_547;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A23h
extern volatile uint8_t xdata g_rw_lxl_graylut_0224h_LXL_LUT0_data_548;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A24h
extern volatile uint8_t xdata g_rw_lxl_graylut_0225h_LXL_LUT0_data_549;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A25h

extern volatile uint8_t xdata g_rw_lxl_graylut_0226h_LXL_LUT0_data_550;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A26h
extern volatile uint8_t xdata g_rw_lxl_graylut_0227h_LXL_LUT0_data_551;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A27h
extern volatile uint8_t xdata g_rw_lxl_graylut_0228h_LXL_LUT0_data_552;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A28h
extern volatile uint8_t xdata g_rw_lxl_graylut_0229h_LXL_LUT0_data_553;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A29h
extern volatile uint8_t xdata g_rw_lxl_graylut_022Ah_LXL_LUT0_data_554;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A2Ah

extern volatile uint8_t xdata g_rw_lxl_graylut_022Bh_LXL_LUT0_data_555;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A2Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_022Ch_LXL_LUT0_data_556;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A2Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_022Dh_LXL_LUT0_data_557;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A2Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_022Eh_LXL_LUT0_data_558;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A2Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_022Fh_LXL_LUT0_data_559;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A2Fh

extern volatile uint8_t xdata g_rw_lxl_graylut_0230h_LXL_LUT0_data_560;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A30h
extern volatile uint8_t xdata g_rw_lxl_graylut_0231h_LXL_LUT0_data_561;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A31h
extern volatile uint8_t xdata g_rw_lxl_graylut_0232h_LXL_LUT0_data_562;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A32h
extern volatile uint8_t xdata g_rw_lxl_graylut_0233h_LXL_LUT0_data_563;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A33h
extern volatile uint8_t xdata g_rw_lxl_graylut_0234h_LXL_LUT0_data_564;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A34h

extern volatile uint8_t xdata g_rw_lxl_graylut_0235h_LXL_LUT0_data_565;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A35h
extern volatile uint8_t xdata g_rw_lxl_graylut_0236h_LXL_LUT0_data_566;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A36h
extern volatile uint8_t xdata g_rw_lxl_graylut_0237h_LXL_LUT0_data_567;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A37h
extern volatile uint8_t xdata g_rw_lxl_graylut_0238h_LXL_LUT0_data_568;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A38h
extern volatile uint8_t xdata g_rw_lxl_graylut_0239h_LXL_LUT0_data_569;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A39h

extern volatile uint8_t xdata g_rw_lxl_graylut_023Ah_LXL_LUT0_data_570;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A3Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_023Bh_LXL_LUT0_data_571;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A3Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_023Ch_LXL_LUT0_data_572;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A3Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_023Dh_LXL_LUT0_data_573;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A3Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_023Eh_LXL_LUT0_data_574;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A3Eh

extern volatile uint8_t xdata g_rw_lxl_graylut_023Fh_LXL_LUT0_data_575;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A3Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0240h_LXL_LUT0_data_576;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A40h
extern volatile uint8_t xdata g_rw_lxl_graylut_0241h_LXL_LUT0_data_577;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A41h
extern volatile uint8_t xdata g_rw_lxl_graylut_0242h_LXL_LUT0_data_578;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A42h
extern volatile uint8_t xdata g_rw_lxl_graylut_0243h_LXL_LUT0_data_579;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A43h

extern volatile uint8_t xdata g_rw_lxl_graylut_0244h_LXL_LUT0_data_580;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A44h
extern volatile uint8_t xdata g_rw_lxl_graylut_0245h_LXL_LUT0_data_581;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A45h
extern volatile uint8_t xdata g_rw_lxl_graylut_0246h_LXL_LUT0_data_582;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A46h
extern volatile uint8_t xdata g_rw_lxl_graylut_0247h_LXL_LUT0_data_583;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A47h
extern volatile uint8_t xdata g_rw_lxl_graylut_0248h_LXL_LUT0_data_584;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A48h

extern volatile uint8_t xdata g_rw_lxl_graylut_0249h_LXL_LUT0_data_585;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A49h
extern volatile uint8_t xdata g_rw_lxl_graylut_024Ah_LXL_LUT0_data_586;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A4Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_024Bh_LXL_LUT0_data_587;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A4Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_024Ch_LXL_LUT0_data_588;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A4Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_024Dh_LXL_LUT0_data_589;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A4Dh

extern volatile uint8_t xdata g_rw_lxl_graylut_024Eh_LXL_LUT0_data_590;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A4Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_024Fh_LXL_LUT0_data_591;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A4Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0250h_LXL_LUT0_data_592;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A50h
extern volatile uint8_t xdata g_rw_lxl_graylut_0251h_LXL_LUT0_data_593;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A51h
extern volatile uint8_t xdata g_rw_lxl_graylut_0252h_LXL_LUT0_data_594;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A52h

extern volatile uint8_t xdata g_rw_lxl_graylut_0253h_LXL_LUT0_data_595;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A53h
extern volatile uint8_t xdata g_rw_lxl_graylut_0254h_LXL_LUT0_data_596;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A54h
extern volatile uint8_t xdata g_rw_lxl_graylut_0255h_LXL_LUT0_data_597;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A55h
extern volatile uint8_t xdata g_rw_lxl_graylut_0256h_LXL_LUT0_data_598;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A56h
extern volatile uint8_t xdata g_rw_lxl_graylut_0257h_LXL_LUT0_data_599;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A57h

extern volatile uint8_t xdata g_rw_lxl_graylut_0258h_LXL_LUT0_data_600;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A58h
extern volatile uint8_t xdata g_rw_lxl_graylut_0259h_LXL_LUT0_data_601;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A59h
extern volatile uint8_t xdata g_rw_lxl_graylut_025Ah_LXL_LUT0_data_602;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A5Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_025Bh_LXL_LUT0_data_603;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A5Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_025Ch_LXL_LUT0_data_604;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A5Ch

extern volatile uint8_t xdata g_rw_lxl_graylut_025Dh_LXL_LUT0_data_605;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A5Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_025Eh_LXL_LUT0_data_606;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A5Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_025Fh_LXL_LUT0_data_607;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A5Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0260h_LXL_LUT0_data_608;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A60h
extern volatile uint8_t xdata g_rw_lxl_graylut_0261h_LXL_LUT0_data_609;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A61h

extern volatile uint8_t xdata g_rw_lxl_graylut_0262h_LXL_LUT0_data_610;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A62h
extern volatile uint8_t xdata g_rw_lxl_graylut_0263h_LXL_LUT0_data_611;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A63h
extern volatile uint8_t xdata g_rw_lxl_graylut_0264h_LXL_LUT0_data_612;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A64h
extern volatile uint8_t xdata g_rw_lxl_graylut_0265h_LXL_LUT0_data_613;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A65h
extern volatile uint8_t xdata g_rw_lxl_graylut_0266h_LXL_LUT0_data_614;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A66h

extern volatile uint8_t xdata g_rw_lxl_graylut_0267h_LXL_LUT0_data_615;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A67h
extern volatile uint8_t xdata g_rw_lxl_graylut_0268h_LXL_LUT0_data_616;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A68h
extern volatile uint8_t xdata g_rw_lxl_graylut_0269h_LXL_LUT0_data_617;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A69h
extern volatile uint8_t xdata g_rw_lxl_graylut_026Ah_LXL_LUT0_data_618;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A6Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_026Bh_LXL_LUT0_data_619;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A6Bh

extern volatile uint8_t xdata g_rw_lxl_graylut_026Ch_LXL_LUT0_data_620;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A6Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_026Dh_LXL_LUT0_data_621;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A6Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_026Eh_LXL_LUT0_data_622;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A6Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_026Fh_LXL_LUT0_data_623;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A6Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0270h_LXL_LUT0_data_624;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A70h

extern volatile uint8_t xdata g_rw_lxl_graylut_0271h_LXL_LUT0_data_625;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A71h
extern volatile uint8_t xdata g_rw_lxl_graylut_0272h_LXL_LUT0_data_626;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A72h
extern volatile uint8_t xdata g_rw_lxl_graylut_0273h_LXL_LUT0_data_627;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A73h
extern volatile uint8_t xdata g_rw_lxl_graylut_0274h_LXL_LUT0_data_628;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A74h
extern volatile uint8_t xdata g_rw_lxl_graylut_0275h_LXL_LUT0_data_629;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A75h

extern volatile uint8_t xdata g_rw_lxl_graylut_0276h_LXL_LUT0_data_630;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A76h
extern volatile uint8_t xdata g_rw_lxl_graylut_0277h_LXL_LUT0_data_631;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A77h
extern volatile uint8_t xdata g_rw_lxl_graylut_0278h_LXL_LUT0_data_632;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A78h
extern volatile uint8_t xdata g_rw_lxl_graylut_0279h_LXL_LUT0_data_633;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A79h
extern volatile uint8_t xdata g_rw_lxl_graylut_027Ah_LXL_LUT0_data_634;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A7Ah

extern volatile uint8_t xdata g_rw_lxl_graylut_027Bh_LXL_LUT0_data_635;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A7Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_027Ch_LXL_LUT0_data_636;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A7Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_027Dh_LXL_LUT0_data_637;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A7Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_027Eh_LXL_LUT0_data_638;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A7Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_027Fh_LXL_LUT0_data_639;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A7Fh

extern volatile uint8_t xdata g_rw_lxl_graylut_0280h_LXL_LUT0_data_640;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A80h
extern volatile uint8_t xdata g_rw_lxl_graylut_0281h_LXL_LUT0_data_641;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A81h
extern volatile uint8_t xdata g_rw_lxl_graylut_0282h_LXL_LUT0_data_642;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A82h
extern volatile uint8_t xdata g_rw_lxl_graylut_0283h_LXL_LUT0_data_643;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A83h
extern volatile uint8_t xdata g_rw_lxl_graylut_0284h_LXL_LUT0_data_644;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A84h

extern volatile uint8_t xdata g_rw_lxl_graylut_0285h_LXL_LUT0_data_645;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A85h
extern volatile uint8_t xdata g_rw_lxl_graylut_0286h_LXL_LUT0_data_646;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A86h
extern volatile uint8_t xdata g_rw_lxl_graylut_0287h_LXL_LUT0_data_647;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A87h
extern volatile uint8_t xdata g_rw_lxl_graylut_0288h_LXL_LUT0_data_648;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A88h
extern volatile uint8_t xdata g_rw_lxl_graylut_0289h_LXL_LUT0_data_649;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A89h

extern volatile uint8_t xdata g_rw_lxl_graylut_028Ah_LXL_LUT0_data_650;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A8Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_028Bh_LXL_LUT0_data_651;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A8Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_028Ch_LXL_LUT0_data_652;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A8Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_028Dh_LXL_LUT0_data_653;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A8Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_028Eh_LXL_LUT0_data_654;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A8Eh

extern volatile uint8_t xdata g_rw_lxl_graylut_028Fh_LXL_LUT0_data_655;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A8Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0290h_LXL_LUT0_data_656;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A90h
extern volatile uint8_t xdata g_rw_lxl_graylut_0291h_LXL_LUT0_data_657;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A91h
extern volatile uint8_t xdata g_rw_lxl_graylut_0292h_LXL_LUT0_data_658;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A92h
extern volatile uint8_t xdata g_rw_lxl_graylut_0293h_LXL_LUT0_data_659;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A93h

extern volatile uint8_t xdata g_rw_lxl_graylut_0294h_LXL_LUT0_data_660;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A94h
extern volatile uint8_t xdata g_rw_lxl_graylut_0295h_LXL_LUT0_data_661;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A95h
extern volatile uint8_t xdata g_rw_lxl_graylut_0296h_LXL_LUT0_data_662;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A96h
extern volatile uint8_t xdata g_rw_lxl_graylut_0297h_LXL_LUT0_data_663;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A97h
extern volatile uint8_t xdata g_rw_lxl_graylut_0298h_LXL_LUT0_data_664;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A98h

extern volatile uint8_t xdata g_rw_lxl_graylut_0299h_LXL_LUT0_data_665;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A99h
extern volatile uint8_t xdata g_rw_lxl_graylut_029Ah_LXL_LUT0_data_666;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A9Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_029Bh_LXL_LUT0_data_667;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A9Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_029Ch_LXL_LUT0_data_668;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A9Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_029Dh_LXL_LUT0_data_669;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A9Dh

extern volatile uint8_t xdata g_rw_lxl_graylut_029Eh_LXL_LUT0_data_670;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A9Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_029Fh_LXL_LUT0_data_671;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8A9Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_02A0h_LXL_LUT0_data_672;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AA0h
extern volatile uint8_t xdata g_rw_lxl_graylut_02A1h_LXL_LUT0_data_673;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AA1h
extern volatile uint8_t xdata g_rw_lxl_graylut_02A2h_LXL_LUT0_data_674;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AA2h

extern volatile uint8_t xdata g_rw_lxl_graylut_02A3h_LXL_LUT0_data_675;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AA3h
extern volatile uint8_t xdata g_rw_lxl_graylut_02A4h_LXL_LUT0_data_676;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AA4h
extern volatile uint8_t xdata g_rw_lxl_graylut_02A5h_LXL_LUT0_data_677;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AA5h
extern volatile uint8_t xdata g_rw_lxl_graylut_02A6h_LXL_LUT0_data_678;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AA6h
extern volatile uint8_t xdata g_rw_lxl_graylut_02A7h_LXL_LUT0_data_679;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AA7h

extern volatile uint8_t xdata g_rw_lxl_graylut_02A8h_LXL_LUT0_data_680;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AA8h
extern volatile uint8_t xdata g_rw_lxl_graylut_02A9h_LXL_LUT0_data_681;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AA9h
extern volatile uint8_t xdata g_rw_lxl_graylut_02AAh_LXL_LUT0_data_682;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AAAh
extern volatile uint8_t xdata g_rw_lxl_graylut_02ABh_LXL_LUT0_data_683;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AABh
extern volatile uint8_t xdata g_rw_lxl_graylut_02ACh_LXL_LUT0_data_684;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AACh

extern volatile uint8_t xdata g_rw_lxl_graylut_02ADh_LXL_LUT0_data_685;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AADh
extern volatile uint8_t xdata g_rw_lxl_graylut_02AEh_LXL_LUT0_data_686;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AAEh
extern volatile uint8_t xdata g_rw_lxl_graylut_02AFh_LXL_LUT0_data_687;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AAFh
extern volatile uint8_t xdata g_rw_lxl_graylut_02B0h_LXL_LUT0_data_688;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AB0h
extern volatile uint8_t xdata g_rw_lxl_graylut_02B1h_LXL_LUT0_data_689;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AB1h

extern volatile uint8_t xdata g_rw_lxl_graylut_02B2h_LXL_LUT0_data_690;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AB2h
extern volatile uint8_t xdata g_rw_lxl_graylut_02B3h_LXL_LUT0_data_691;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AB3h
extern volatile uint8_t xdata g_rw_lxl_graylut_02B4h_LXL_LUT0_data_692;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AB4h
extern volatile uint8_t xdata g_rw_lxl_graylut_02B5h_LXL_LUT0_data_693;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AB5h
extern volatile uint8_t xdata g_rw_lxl_graylut_02B6h_LXL_LUT0_data_694;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AB6h

extern volatile uint8_t xdata g_rw_lxl_graylut_02B7h_LXL_LUT0_data_695;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AB7h
extern volatile uint8_t xdata g_rw_lxl_graylut_02B8h_LXL_LUT0_data_696;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AB8h
extern volatile uint8_t xdata g_rw_lxl_graylut_02B9h_LXL_LUT0_data_697;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AB9h
extern volatile uint8_t xdata g_rw_lxl_graylut_02BAh_LXL_LUT0_data_698;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ABAh
extern volatile uint8_t xdata g_rw_lxl_graylut_02BBh_LXL_LUT0_data_699;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ABBh

extern volatile uint8_t xdata g_rw_lxl_graylut_02BCh_LXL_LUT0_data_700;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ABCh
extern volatile uint8_t xdata g_rw_lxl_graylut_02BDh_LXL_LUT0_data_701;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ABDh
extern volatile uint8_t xdata g_rw_lxl_graylut_02BEh_LXL_LUT0_data_702;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ABEh
extern volatile uint8_t xdata g_rw_lxl_graylut_02BFh_LXL_LUT0_data_703;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ABFh
extern volatile uint8_t xdata g_rw_lxl_graylut_02C0h_LXL_LUT0_data_704;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AC0h

extern volatile uint8_t xdata g_rw_lxl_graylut_02C1h_LXL_LUT0_data_705;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AC1h
extern volatile uint8_t xdata g_rw_lxl_graylut_02C2h_LXL_LUT0_data_706;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AC2h
extern volatile uint8_t xdata g_rw_lxl_graylut_02C3h_LXL_LUT0_data_707;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AC3h
extern volatile uint8_t xdata g_rw_lxl_graylut_02C4h_LXL_LUT0_data_708;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AC4h
extern volatile uint8_t xdata g_rw_lxl_graylut_02C5h_LXL_LUT0_data_709;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AC5h

extern volatile uint8_t xdata g_rw_lxl_graylut_02C6h_LXL_LUT0_data_710;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AC6h
extern volatile uint8_t xdata g_rw_lxl_graylut_02C7h_LXL_LUT0_data_711;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AC7h
extern volatile uint8_t xdata g_rw_lxl_graylut_02C8h_LXL_LUT0_data_712;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AC8h
extern volatile uint8_t xdata g_rw_lxl_graylut_02C9h_LXL_LUT0_data_713;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AC9h
extern volatile uint8_t xdata g_rw_lxl_graylut_02CAh_LXL_LUT0_data_714;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ACAh

extern volatile uint8_t xdata g_rw_lxl_graylut_02CBh_LXL_LUT0_data_715;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ACBh
extern volatile uint8_t xdata g_rw_lxl_graylut_02CCh_LXL_LUT0_data_716;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ACCh
extern volatile uint8_t xdata g_rw_lxl_graylut_02CDh_LXL_LUT0_data_717;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ACDh
extern volatile uint8_t xdata g_rw_lxl_graylut_02CEh_LXL_LUT0_data_718;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ACEh
extern volatile uint8_t xdata g_rw_lxl_graylut_02CFh_LXL_LUT0_data_719;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ACFh

extern volatile uint8_t xdata g_rw_lxl_graylut_02D0h_LXL_LUT0_data_720;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AD0h
extern volatile uint8_t xdata g_rw_lxl_graylut_02D1h_LXL_LUT0_data_721;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AD1h
extern volatile uint8_t xdata g_rw_lxl_graylut_02D2h_LXL_LUT0_data_722;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AD2h
extern volatile uint8_t xdata g_rw_lxl_graylut_02D3h_LXL_LUT0_data_723;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AD3h
extern volatile uint8_t xdata g_rw_lxl_graylut_02D4h_LXL_LUT0_data_724;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AD4h

extern volatile uint8_t xdata g_rw_lxl_graylut_02D5h_LXL_LUT0_data_725;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AD5h
extern volatile uint8_t xdata g_rw_lxl_graylut_02D6h_LXL_LUT0_data_726;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AD6h
extern volatile uint8_t xdata g_rw_lxl_graylut_02D7h_LXL_LUT0_data_727;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AD7h
extern volatile uint8_t xdata g_rw_lxl_graylut_02D8h_LXL_LUT0_data_728;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AD8h
extern volatile uint8_t xdata g_rw_lxl_graylut_02D9h_LXL_LUT0_data_729;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AD9h

extern volatile uint8_t xdata g_rw_lxl_graylut_02DAh_LXL_LUT0_data_730;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ADAh
extern volatile uint8_t xdata g_rw_lxl_graylut_02DBh_LXL_LUT0_data_731;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ADBh
extern volatile uint8_t xdata g_rw_lxl_graylut_02DCh_LXL_LUT0_data_732;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ADCh
extern volatile uint8_t xdata g_rw_lxl_graylut_02DDh_LXL_LUT0_data_733;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ADDh
extern volatile uint8_t xdata g_rw_lxl_graylut_02DEh_LXL_LUT0_data_734;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ADEh

extern volatile uint8_t xdata g_rw_lxl_graylut_02DFh_LXL_LUT0_data_735;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8ADFh
extern volatile uint8_t xdata g_rw_lxl_graylut_02E0h_LXL_LUT0_data_736;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AE0h
extern volatile uint8_t xdata g_rw_lxl_graylut_02E1h_LXL_LUT0_data_737;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AE1h
extern volatile uint8_t xdata g_rw_lxl_graylut_02E2h_LXL_LUT0_data_738;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AE2h
extern volatile uint8_t xdata g_rw_lxl_graylut_02E3h_LXL_LUT0_data_739;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AE3h

extern volatile uint8_t xdata g_rw_lxl_graylut_02E4h_LXL_LUT0_data_740;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AE4h
extern volatile uint8_t xdata g_rw_lxl_graylut_02E5h_LXL_LUT0_data_741;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AE5h
extern volatile uint8_t xdata g_rw_lxl_graylut_02E6h_LXL_LUT0_data_742;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AE6h
extern volatile uint8_t xdata g_rw_lxl_graylut_02E7h_LXL_LUT0_data_743;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AE7h
extern volatile uint8_t xdata g_rw_lxl_graylut_02E8h_LXL_LUT0_data_744;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AE8h

extern volatile uint8_t xdata g_rw_lxl_graylut_02E9h_LXL_LUT0_data_745;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AE9h
extern volatile uint8_t xdata g_rw_lxl_graylut_02EAh_LXL_LUT0_data_746;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AEAh
extern volatile uint8_t xdata g_rw_lxl_graylut_02EBh_LXL_LUT0_data_747;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AEBh
extern volatile uint8_t xdata g_rw_lxl_graylut_02ECh_LXL_LUT0_data_748;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AECh
extern volatile uint8_t xdata g_rw_lxl_graylut_02EDh_LXL_LUT0_data_749;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AEDh

extern volatile uint8_t xdata g_rw_lxl_graylut_02EEh_LXL_LUT0_data_750;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AEEh
extern volatile uint8_t xdata g_rw_lxl_graylut_02EFh_LXL_LUT0_data_751;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AEFh
extern volatile uint8_t xdata g_rw_lxl_graylut_02F0h_LXL_LUT0_data_752;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AF0h
extern volatile uint8_t xdata g_rw_lxl_graylut_02F1h_LXL_LUT0_data_753;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AF1h
extern volatile uint8_t xdata g_rw_lxl_graylut_02F2h_LXL_LUT0_data_754;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AF2h

extern volatile uint8_t xdata g_rw_lxl_graylut_02F3h_LXL_LUT0_data_755;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AF3h
extern volatile uint8_t xdata g_rw_lxl_graylut_02F4h_LXL_LUT0_data_756;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AF4h
extern volatile uint8_t xdata g_rw_lxl_graylut_02F5h_LXL_LUT0_data_757;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AF5h
extern volatile uint8_t xdata g_rw_lxl_graylut_02F6h_LXL_LUT0_data_758;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AF6h
extern volatile uint8_t xdata g_rw_lxl_graylut_02F7h_LXL_LUT0_data_759;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AF7h

extern volatile uint8_t xdata g_rw_lxl_graylut_02F8h_LXL_LUT0_data_760;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AF8h
extern volatile uint8_t xdata g_rw_lxl_graylut_02F9h_LXL_LUT0_data_761;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AF9h
extern volatile uint8_t xdata g_rw_lxl_graylut_02FAh_LXL_LUT0_data_762;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AFAh
extern volatile uint8_t xdata g_rw_lxl_graylut_02FBh_LXL_LUT0_data_763;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AFBh
extern volatile uint8_t xdata g_rw_lxl_graylut_02FCh_LXL_LUT0_data_764;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AFCh

extern volatile uint8_t xdata g_rw_lxl_graylut_02FDh_LXL_LUT0_data_765;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AFDh
extern volatile uint8_t xdata g_rw_lxl_graylut_02FEh_LXL_LUT0_data_766;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AFEh
extern volatile uint8_t xdata g_rw_lxl_graylut_02FFh_LXL_LUT0_data_767;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8AFFh
extern volatile uint8_t xdata g_rw_lxl_graylut_0300h_LXL_LUT0_data_768;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B00h
extern volatile uint8_t xdata g_rw_lxl_graylut_0301h_LXL_LUT0_data_769;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B01h

extern volatile uint8_t xdata g_rw_lxl_graylut_0302h_LXL_LUT0_data_770;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B02h
extern volatile uint8_t xdata g_rw_lxl_graylut_0303h_LXL_LUT0_data_771;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B03h
extern volatile uint8_t xdata g_rw_lxl_graylut_0304h_LXL_LUT0_data_772;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B04h
extern volatile uint8_t xdata g_rw_lxl_graylut_0305h_LXL_LUT0_data_773;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B05h
extern volatile uint8_t xdata g_rw_lxl_graylut_0306h_LXL_LUT0_data_774;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B06h

extern volatile uint8_t xdata g_rw_lxl_graylut_0307h_LXL_LUT0_data_775;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B07h
extern volatile uint8_t xdata g_rw_lxl_graylut_0308h_LXL_LUT0_data_776;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B08h
extern volatile uint8_t xdata g_rw_lxl_graylut_0309h_LXL_LUT0_data_777;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B09h
extern volatile uint8_t xdata g_rw_lxl_graylut_030Ah_LXL_LUT0_data_778;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B0Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_030Bh_LXL_LUT0_data_779;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B0Bh

extern volatile uint8_t xdata g_rw_lxl_graylut_030Ch_LXL_LUT0_data_780;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B0Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_030Dh_LXL_LUT0_data_781;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B0Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_030Eh_LXL_LUT0_data_782;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B0Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_030Fh_LXL_LUT0_data_783;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B0Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0310h_LXL_LUT0_data_784;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B10h

extern volatile uint8_t xdata g_rw_lxl_graylut_0311h_LXL_LUT0_data_785;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B11h
extern volatile uint8_t xdata g_rw_lxl_graylut_0312h_LXL_LUT0_data_786;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B12h
extern volatile uint8_t xdata g_rw_lxl_graylut_0313h_LXL_LUT0_data_787;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B13h
extern volatile uint8_t xdata g_rw_lxl_graylut_0314h_LXL_LUT0_data_788;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B14h
extern volatile uint8_t xdata g_rw_lxl_graylut_0315h_LXL_LUT0_data_789;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B15h

extern volatile uint8_t xdata g_rw_lxl_graylut_0316h_LXL_LUT0_data_790;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B16h
extern volatile uint8_t xdata g_rw_lxl_graylut_0317h_LXL_LUT0_data_791;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B17h
extern volatile uint8_t xdata g_rw_lxl_graylut_0318h_LXL_LUT0_data_792;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B18h
extern volatile uint8_t xdata g_rw_lxl_graylut_0319h_LXL_LUT0_data_793;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B19h
extern volatile uint8_t xdata g_rw_lxl_graylut_031Ah_LXL_LUT0_data_794;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B1Ah

extern volatile uint8_t xdata g_rw_lxl_graylut_031Bh_LXL_LUT0_data_795;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B1Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_031Ch_LXL_LUT0_data_796;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B1Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_031Dh_LXL_LUT0_data_797;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B1Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_031Eh_LXL_LUT0_data_798;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B1Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_031Fh_LXL_LUT0_data_799;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B1Fh

extern volatile uint8_t xdata g_rw_lxl_graylut_0320h_LXL_LUT0_data_800;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B20h
extern volatile uint8_t xdata g_rw_lxl_graylut_0321h_LXL_LUT0_data_801;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B21h
extern volatile uint8_t xdata g_rw_lxl_graylut_0322h_LXL_LUT0_data_802;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B22h
extern volatile uint8_t xdata g_rw_lxl_graylut_0323h_LXL_LUT0_data_803;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B23h
extern volatile uint8_t xdata g_rw_lxl_graylut_0324h_LXL_LUT0_data_804;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B24h

extern volatile uint8_t xdata g_rw_lxl_graylut_0325h_LXL_LUT0_data_805;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B25h
extern volatile uint8_t xdata g_rw_lxl_graylut_0326h_LXL_LUT0_data_806;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B26h
extern volatile uint8_t xdata g_rw_lxl_graylut_0327h_LXL_LUT0_data_807;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B27h
extern volatile uint8_t xdata g_rw_lxl_graylut_0328h_LXL_LUT0_data_808;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B28h
extern volatile uint8_t xdata g_rw_lxl_graylut_0329h_LXL_LUT0_data_809;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B29h

extern volatile uint8_t xdata g_rw_lxl_graylut_032Ah_LXL_LUT0_data_810;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B2Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_032Bh_LXL_LUT0_data_811;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B2Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_032Ch_LXL_LUT0_data_812;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B2Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_032Dh_LXL_LUT0_data_813;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B2Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_032Eh_LXL_LUT0_data_814;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B2Eh

extern volatile uint8_t xdata g_rw_lxl_graylut_032Fh_LXL_LUT0_data_815;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B2Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0330h_LXL_LUT0_data_816;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B30h
extern volatile uint8_t xdata g_rw_lxl_graylut_0331h_LXL_LUT0_data_817;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B31h
extern volatile uint8_t xdata g_rw_lxl_graylut_0332h_LXL_LUT0_data_818;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B32h
extern volatile uint8_t xdata g_rw_lxl_graylut_0333h_LXL_LUT0_data_819;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B33h

extern volatile uint8_t xdata g_rw_lxl_graylut_0334h_LXL_LUT0_data_820;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B34h
extern volatile uint8_t xdata g_rw_lxl_graylut_0335h_LXL_LUT0_data_821;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B35h
extern volatile uint8_t xdata g_rw_lxl_graylut_0336h_LXL_LUT0_data_822;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B36h
extern volatile uint8_t xdata g_rw_lxl_graylut_0337h_LXL_LUT0_data_823;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B37h
extern volatile uint8_t xdata g_rw_lxl_graylut_0338h_LXL_LUT0_data_824;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B38h

extern volatile uint8_t xdata g_rw_lxl_graylut_0339h_LXL_LUT0_data_825;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B39h
extern volatile uint8_t xdata g_rw_lxl_graylut_033Ah_LXL_LUT0_data_826;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B3Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_033Bh_LXL_LUT0_data_827;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B3Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_033Ch_LXL_LUT0_data_828;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B3Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_033Dh_LXL_LUT0_data_829;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B3Dh

extern volatile uint8_t xdata g_rw_lxl_graylut_033Eh_LXL_LUT0_data_830;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B3Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_033Fh_LXL_LUT0_data_831;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B3Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0340h_LXL_LUT0_data_832;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B40h
extern volatile uint8_t xdata g_rw_lxl_graylut_0341h_LXL_LUT0_data_833;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B41h
extern volatile uint8_t xdata g_rw_lxl_graylut_0342h_LXL_LUT0_data_834;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B42h

extern volatile uint8_t xdata g_rw_lxl_graylut_0343h_LXL_LUT0_data_835;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B43h
extern volatile uint8_t xdata g_rw_lxl_graylut_0344h_LXL_LUT0_data_836;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B44h
extern volatile uint8_t xdata g_rw_lxl_graylut_0345h_LXL_LUT0_data_837;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B45h
extern volatile uint8_t xdata g_rw_lxl_graylut_0346h_LXL_LUT0_data_838;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B46h
extern volatile uint8_t xdata g_rw_lxl_graylut_0347h_LXL_LUT0_data_839;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B47h

extern volatile uint8_t xdata g_rw_lxl_graylut_0348h_LXL_LUT0_data_840;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B48h
extern volatile uint8_t xdata g_rw_lxl_graylut_0349h_LXL_LUT0_data_841;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B49h
extern volatile uint8_t xdata g_rw_lxl_graylut_034Ah_LXL_LUT0_data_842;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B4Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_034Bh_LXL_LUT0_data_843;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B4Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_034Ch_LXL_LUT0_data_844;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B4Ch

extern volatile uint8_t xdata g_rw_lxl_graylut_034Dh_LXL_LUT0_data_845;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B4Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_034Eh_LXL_LUT0_data_846;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B4Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_034Fh_LXL_LUT0_data_847;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B4Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0350h_LXL_LUT0_data_848;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B50h
extern volatile uint8_t xdata g_rw_lxl_graylut_0351h_LXL_LUT0_data_849;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B51h

extern volatile uint8_t xdata g_rw_lxl_graylut_0352h_LXL_LUT0_data_850;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B52h
extern volatile uint8_t xdata g_rw_lxl_graylut_0353h_LXL_LUT0_data_851;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B53h
extern volatile uint8_t xdata g_rw_lxl_graylut_0354h_LXL_LUT0_data_852;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B54h
extern volatile uint8_t xdata g_rw_lxl_graylut_0355h_LXL_LUT0_data_853;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B55h
extern volatile uint8_t xdata g_rw_lxl_graylut_0356h_LXL_LUT0_data_854;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B56h

extern volatile uint8_t xdata g_rw_lxl_graylut_0357h_LXL_LUT0_data_855;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B57h
extern volatile uint8_t xdata g_rw_lxl_graylut_0358h_LXL_LUT0_data_856;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B58h
extern volatile uint8_t xdata g_rw_lxl_graylut_0359h_LXL_LUT0_data_857;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B59h
extern volatile uint8_t xdata g_rw_lxl_graylut_035Ah_LXL_LUT0_data_858;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B5Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_035Bh_LXL_LUT0_data_859;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B5Bh

extern volatile uint8_t xdata g_rw_lxl_graylut_035Ch_LXL_LUT0_data_860;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B5Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_035Dh_LXL_LUT0_data_861;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B5Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_035Eh_LXL_LUT0_data_862;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B5Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_035Fh_LXL_LUT0_data_863;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B5Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0360h_LXL_LUT0_data_864;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B60h

extern volatile uint8_t xdata g_rw_lxl_graylut_0361h_LXL_LUT0_data_865;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B61h
extern volatile uint8_t xdata g_rw_lxl_graylut_0362h_LXL_LUT0_data_866;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B62h
extern volatile uint8_t xdata g_rw_lxl_graylut_0363h_LXL_LUT0_data_867;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B63h
extern volatile uint8_t xdata g_rw_lxl_graylut_0364h_LXL_LUT0_data_868;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B64h
extern volatile uint8_t xdata g_rw_lxl_graylut_0365h_LXL_LUT0_data_869;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B65h

extern volatile uint8_t xdata g_rw_lxl_graylut_0366h_LXL_LUT0_data_870;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B66h
extern volatile uint8_t xdata g_rw_lxl_graylut_0367h_LXL_LUT0_data_871;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B67h
extern volatile uint8_t xdata g_rw_lxl_graylut_0368h_LXL_LUT0_data_872;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B68h
extern volatile uint8_t xdata g_rw_lxl_graylut_0369h_LXL_LUT0_data_873;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B69h
extern volatile uint8_t xdata g_rw_lxl_graylut_036Ah_LXL_LUT0_data_874;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B6Ah

extern volatile uint8_t xdata g_rw_lxl_graylut_036Bh_LXL_LUT0_data_875;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B6Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_036Ch_LXL_LUT0_data_876;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B6Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_036Dh_LXL_LUT0_data_877;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B6Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_036Eh_LXL_LUT0_data_878;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B6Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_036Fh_LXL_LUT0_data_879;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B6Fh

extern volatile uint8_t xdata g_rw_lxl_graylut_0370h_LXL_LUT0_data_880;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B70h
extern volatile uint8_t xdata g_rw_lxl_graylut_0371h_LXL_LUT0_data_881;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B71h
extern volatile uint8_t xdata g_rw_lxl_graylut_0372h_LXL_LUT0_data_882;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B72h
extern volatile uint8_t xdata g_rw_lxl_graylut_0373h_LXL_LUT0_data_883;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B73h
extern volatile uint8_t xdata g_rw_lxl_graylut_0374h_LXL_LUT0_data_884;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B74h

extern volatile uint8_t xdata g_rw_lxl_graylut_0375h_LXL_LUT0_data_885;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B75h
extern volatile uint8_t xdata g_rw_lxl_graylut_0376h_LXL_LUT0_data_886;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B76h
extern volatile uint8_t xdata g_rw_lxl_graylut_0377h_LXL_LUT0_data_887;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B77h
extern volatile uint8_t xdata g_rw_lxl_graylut_0378h_LXL_LUT0_data_888;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B78h
extern volatile uint8_t xdata g_rw_lxl_graylut_0379h_LXL_LUT0_data_889;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B79h

extern volatile uint8_t xdata g_rw_lxl_graylut_037Ah_LXL_LUT0_data_890;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B7Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_037Bh_LXL_LUT0_data_891;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B7Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_037Ch_LXL_LUT0_data_892;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B7Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_037Dh_LXL_LUT0_data_893;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B7Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_037Eh_LXL_LUT0_data_894;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B7Eh

extern volatile uint8_t xdata g_rw_lxl_graylut_037Fh_LXL_LUT0_data_895;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B7Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0380h_LXL_LUT0_data_896;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B80h
extern volatile uint8_t xdata g_rw_lxl_graylut_0381h_LXL_LUT0_data_897;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B81h
extern volatile uint8_t xdata g_rw_lxl_graylut_0382h_LXL_LUT0_data_898;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B82h
extern volatile uint8_t xdata g_rw_lxl_graylut_0383h_LXL_LUT0_data_899;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B83h

extern volatile uint8_t xdata g_rw_lxl_graylut_0384h_LXL_LUT0_data_900;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B84h
extern volatile uint8_t xdata g_rw_lxl_graylut_0385h_LXL_LUT0_data_901;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B85h
extern volatile uint8_t xdata g_rw_lxl_graylut_0386h_LXL_LUT0_data_902;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B86h
extern volatile uint8_t xdata g_rw_lxl_graylut_0387h_LXL_LUT0_data_903;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B87h
extern volatile uint8_t xdata g_rw_lxl_graylut_0388h_LXL_LUT0_data_904;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B88h

extern volatile uint8_t xdata g_rw_lxl_graylut_0389h_LXL_LUT0_data_905;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B89h
extern volatile uint8_t xdata g_rw_lxl_graylut_038Ah_LXL_LUT0_data_906;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B8Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_038Bh_LXL_LUT0_data_907;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B8Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_038Ch_LXL_LUT0_data_908;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B8Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_038Dh_LXL_LUT0_data_909;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B8Dh

extern volatile uint8_t xdata g_rw_lxl_graylut_038Eh_LXL_LUT0_data_910;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B8Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_038Fh_LXL_LUT0_data_911;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B8Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0390h_LXL_LUT0_data_912;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B90h
extern volatile uint8_t xdata g_rw_lxl_graylut_0391h_LXL_LUT0_data_913;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B91h
extern volatile uint8_t xdata g_rw_lxl_graylut_0392h_LXL_LUT0_data_914;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B92h

extern volatile uint8_t xdata g_rw_lxl_graylut_0393h_LXL_LUT0_data_915;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B93h
extern volatile uint8_t xdata g_rw_lxl_graylut_0394h_LXL_LUT0_data_916;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B94h
extern volatile uint8_t xdata g_rw_lxl_graylut_0395h_LXL_LUT0_data_917;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B95h
extern volatile uint8_t xdata g_rw_lxl_graylut_0396h_LXL_LUT0_data_918;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B96h
extern volatile uint8_t xdata g_rw_lxl_graylut_0397h_LXL_LUT0_data_919;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B97h

extern volatile uint8_t xdata g_rw_lxl_graylut_0398h_LXL_LUT0_data_920;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B98h
extern volatile uint8_t xdata g_rw_lxl_graylut_0399h_LXL_LUT0_data_921;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B99h
extern volatile uint8_t xdata g_rw_lxl_graylut_039Ah_LXL_LUT0_data_922;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B9Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_039Bh_LXL_LUT0_data_923;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B9Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_039Ch_LXL_LUT0_data_924;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B9Ch

extern volatile uint8_t xdata g_rw_lxl_graylut_039Dh_LXL_LUT0_data_925;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B9Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_039Eh_LXL_LUT0_data_926;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B9Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_039Fh_LXL_LUT0_data_927;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8B9Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_03A0h_LXL_LUT0_data_928;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BA0h
extern volatile uint8_t xdata g_rw_lxl_graylut_03A1h_LXL_LUT0_data_929;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BA1h

extern volatile uint8_t xdata g_rw_lxl_graylut_03A2h_LXL_LUT0_data_930;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BA2h
extern volatile uint8_t xdata g_rw_lxl_graylut_03A3h_LXL_LUT0_data_931;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BA3h
extern volatile uint8_t xdata g_rw_lxl_graylut_03A4h_LXL_LUT0_data_932;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BA4h
extern volatile uint8_t xdata g_rw_lxl_graylut_03A5h_LXL_LUT0_data_933;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BA5h
extern volatile uint8_t xdata g_rw_lxl_graylut_03A6h_LXL_LUT0_data_934;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BA6h

extern volatile uint8_t xdata g_rw_lxl_graylut_03A7h_LXL_LUT0_data_935;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BA7h
extern volatile uint8_t xdata g_rw_lxl_graylut_03A8h_LXL_LUT0_data_936;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BA8h
extern volatile uint8_t xdata g_rw_lxl_graylut_03A9h_LXL_LUT0_data_937;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BA9h
extern volatile uint8_t xdata g_rw_lxl_graylut_03AAh_LXL_LUT0_data_938;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BAAh
extern volatile uint8_t xdata g_rw_lxl_graylut_03ABh_LXL_LUT0_data_939;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BABh

extern volatile uint8_t xdata g_rw_lxl_graylut_03ACh_LXL_LUT0_data_940;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BACh
extern volatile uint8_t xdata g_rw_lxl_graylut_03ADh_LXL_LUT0_data_941;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BADh
extern volatile uint8_t xdata g_rw_lxl_graylut_03AEh_LXL_LUT0_data_942;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BAEh
extern volatile uint8_t xdata g_rw_lxl_graylut_03AFh_LXL_LUT0_data_943;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BAFh
extern volatile uint8_t xdata g_rw_lxl_graylut_03B0h_LXL_LUT0_data_944;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BB0h

extern volatile uint8_t xdata g_rw_lxl_graylut_03B1h_LXL_LUT0_data_945;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BB1h
extern volatile uint8_t xdata g_rw_lxl_graylut_03B2h_LXL_LUT0_data_946;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BB2h
extern volatile uint8_t xdata g_rw_lxl_graylut_03B3h_LXL_LUT0_data_947;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BB3h
extern volatile uint8_t xdata g_rw_lxl_graylut_03B4h_LXL_LUT0_data_948;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BB4h
extern volatile uint8_t xdata g_rw_lxl_graylut_03B5h_LXL_LUT0_data_949;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BB5h

extern volatile uint8_t xdata g_rw_lxl_graylut_03B6h_LXL_LUT0_data_950;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BB6h
extern volatile uint8_t xdata g_rw_lxl_graylut_03B7h_LXL_LUT0_data_951;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BB7h
extern volatile uint8_t xdata g_rw_lxl_graylut_03B8h_LXL_LUT0_data_952;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BB8h
extern volatile uint8_t xdata g_rw_lxl_graylut_03B9h_LXL_LUT0_data_953;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BB9h
extern volatile uint8_t xdata g_rw_lxl_graylut_03BAh_LXL_LUT0_data_954;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BBAh

extern volatile uint8_t xdata g_rw_lxl_graylut_03BBh_LXL_LUT0_data_955;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BBBh
extern volatile uint8_t xdata g_rw_lxl_graylut_03BCh_LXL_LUT0_data_956;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BBCh
extern volatile uint8_t xdata g_rw_lxl_graylut_03BDh_LXL_LUT0_data_957;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BBDh
extern volatile uint8_t xdata g_rw_lxl_graylut_03BEh_LXL_LUT0_data_958;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BBEh
extern volatile uint8_t xdata g_rw_lxl_graylut_03BFh_LXL_LUT0_data_959;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BBFh

extern volatile uint8_t xdata g_rw_lxl_graylut_03C0h_LXL_LUT0_data_960;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BC0h
extern volatile uint8_t xdata g_rw_lxl_graylut_03C1h_LXL_LUT0_data_961;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BC1h
extern volatile uint8_t xdata g_rw_lxl_graylut_03C2h_LXL_LUT0_data_962;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BC2h
extern volatile uint8_t xdata g_rw_lxl_graylut_03C3h_LXL_LUT0_data_963;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BC3h
extern volatile uint8_t xdata g_rw_lxl_graylut_03C4h_LXL_LUT0_data_964;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BC4h

extern volatile uint8_t xdata g_rw_lxl_graylut_03C5h_LXL_LUT0_data_965;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BC5h
extern volatile uint8_t xdata g_rw_lxl_graylut_03C6h_LXL_LUT0_data_966;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BC6h
extern volatile uint8_t xdata g_rw_lxl_graylut_03C7h_LXL_LUT0_data_967;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BC7h
extern volatile uint8_t xdata g_rw_lxl_graylut_03C8h_LXL_LUT0_data_968;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BC8h
extern volatile uint8_t xdata g_rw_lxl_graylut_03C9h_LXL_LUT0_data_969;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BC9h

extern volatile uint8_t xdata g_rw_lxl_graylut_03CAh_LXL_LUT0_data_970;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BCAh
extern volatile uint8_t xdata g_rw_lxl_graylut_03CBh_LXL_LUT0_data_971;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BCBh
extern volatile uint8_t xdata g_rw_lxl_graylut_03CCh_LXL_LUT0_data_972;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BCCh
extern volatile uint8_t xdata g_rw_lxl_graylut_03CDh_LXL_LUT0_data_973;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BCDh
extern volatile uint8_t xdata g_rw_lxl_graylut_03CEh_LXL_LUT0_data_974;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BCEh

extern volatile uint8_t xdata g_rw_lxl_graylut_03CFh_LXL_LUT0_data_975;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BCFh
extern volatile uint8_t xdata g_rw_lxl_graylut_03D0h_LXL_LUT0_data_976;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BD0h
extern volatile uint8_t xdata g_rw_lxl_graylut_03D1h_LXL_LUT0_data_977;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BD1h
extern volatile uint8_t xdata g_rw_lxl_graylut_03D2h_LXL_LUT0_data_978;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BD2h
extern volatile uint8_t xdata g_rw_lxl_graylut_03D3h_LXL_LUT0_data_979;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BD3h

extern volatile uint8_t xdata g_rw_lxl_graylut_03D4h_LXL_LUT0_data_980;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BD4h
extern volatile uint8_t xdata g_rw_lxl_graylut_03D5h_LXL_LUT0_data_981;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BD5h
extern volatile uint8_t xdata g_rw_lxl_graylut_03D6h_LXL_LUT0_data_982;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BD6h
extern volatile uint8_t xdata g_rw_lxl_graylut_03D7h_LXL_LUT0_data_983;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BD7h
extern volatile uint8_t xdata g_rw_lxl_graylut_03D8h_LXL_LUT0_data_984;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BD8h

extern volatile uint8_t xdata g_rw_lxl_graylut_03D9h_LXL_LUT0_data_985;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BD9h
extern volatile uint8_t xdata g_rw_lxl_graylut_03DAh_LXL_LUT0_data_986;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BDAh
extern volatile uint8_t xdata g_rw_lxl_graylut_03DBh_LXL_LUT0_data_987;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BDBh
extern volatile uint8_t xdata g_rw_lxl_graylut_03DCh_LXL_LUT0_data_988;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BDCh
extern volatile uint8_t xdata g_rw_lxl_graylut_03DDh_LXL_LUT0_data_989;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BDDh

extern volatile uint8_t xdata g_rw_lxl_graylut_03DEh_LXL_LUT0_data_990;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BDEh
extern volatile uint8_t xdata g_rw_lxl_graylut_03DFh_LXL_LUT0_data_991;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BDFh
extern volatile uint8_t xdata g_rw_lxl_graylut_03E0h_LXL_LUT0_data_992;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BE0h
extern volatile uint8_t xdata g_rw_lxl_graylut_03E1h_LXL_LUT0_data_993;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BE1h
extern volatile uint8_t xdata g_rw_lxl_graylut_03E2h_LXL_LUT0_data_994;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BE2h

extern volatile uint8_t xdata g_rw_lxl_graylut_03E3h_LXL_LUT0_data_995;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BE3h
extern volatile uint8_t xdata g_rw_lxl_graylut_03E4h_LXL_LUT0_data_996;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BE4h
extern volatile uint8_t xdata g_rw_lxl_graylut_03E5h_LXL_LUT0_data_997;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BE5h
extern volatile uint8_t xdata g_rw_lxl_graylut_03E6h_LXL_LUT0_data_998;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BE6h
extern volatile uint8_t xdata g_rw_lxl_graylut_03E7h_LXL_LUT0_data_999;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BE7h

extern volatile uint8_t xdata g_rw_lxl_graylut_03E8h_LXL_LUT0_data_1000;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BE8h
extern volatile uint8_t xdata g_rw_lxl_graylut_03E9h_LXL_LUT0_data_1001;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BE9h
extern volatile uint8_t xdata g_rw_lxl_graylut_03EAh_LXL_LUT0_data_1002;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BEAh
extern volatile uint8_t xdata g_rw_lxl_graylut_03EBh_LXL_LUT0_data_1003;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BEBh
extern volatile uint8_t xdata g_rw_lxl_graylut_03ECh_LXL_LUT0_data_1004;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BECh

extern volatile uint8_t xdata g_rw_lxl_graylut_03EDh_LXL_LUT0_data_1005;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BEDh
extern volatile uint8_t xdata g_rw_lxl_graylut_03EEh_LXL_LUT0_data_1006;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BEEh
extern volatile uint8_t xdata g_rw_lxl_graylut_03EFh_LXL_LUT0_data_1007;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BEFh
extern volatile uint8_t xdata g_rw_lxl_graylut_03F0h_LXL_LUT0_data_1008;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BF0h
extern volatile uint8_t xdata g_rw_lxl_graylut_03F1h_LXL_LUT0_data_1009;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BF1h

extern volatile uint8_t xdata g_rw_lxl_graylut_03F2h_LXL_LUT0_data_1010;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BF2h
extern volatile uint8_t xdata g_rw_lxl_graylut_03F3h_LXL_LUT0_data_1011;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BF3h
extern volatile uint8_t xdata g_rw_lxl_graylut_03F4h_LXL_LUT0_data_1012;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BF4h
extern volatile uint8_t xdata g_rw_lxl_graylut_03F5h_LXL_LUT0_data_1013;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BF5h
extern volatile uint8_t xdata g_rw_lxl_graylut_03F6h_LXL_LUT0_data_1014;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BF6h

extern volatile uint8_t xdata g_rw_lxl_graylut_03F7h_LXL_LUT0_data_1015;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BF7h
extern volatile uint8_t xdata g_rw_lxl_graylut_03F8h_LXL_LUT0_data_1016;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BF8h
extern volatile uint8_t xdata g_rw_lxl_graylut_03F9h_LXL_LUT0_data_1017;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BF9h
extern volatile uint8_t xdata g_rw_lxl_graylut_03FAh_LXL_LUT0_data_1018;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BFAh
extern volatile uint8_t xdata g_rw_lxl_graylut_03FBh_LXL_LUT0_data_1019;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BFBh

extern volatile uint8_t xdata g_rw_lxl_graylut_03FCh_LXL_LUT0_data_1020;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BFCh
extern volatile uint8_t xdata g_rw_lxl_graylut_03FDh_LXL_LUT0_data_1021;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BFDh
extern volatile uint8_t xdata g_rw_lxl_graylut_03FEh_LXL_LUT0_data_1022;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BFEh
extern volatile uint8_t xdata g_rw_lxl_graylut_03FFh_LXL_LUT0_data_1023;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8BFFh
extern volatile uint8_t xdata g_rw_lxl_graylut_0400h_LXL_LUT0_data_1024;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C00h

extern volatile uint8_t xdata g_rw_lxl_graylut_0401h_LXL_LUT0_data_1025;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C01h
extern volatile uint8_t xdata g_rw_lxl_graylut_0402h_LXL_LUT0_data_1026;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C02h
extern volatile uint8_t xdata g_rw_lxl_graylut_0403h_LXL_LUT0_data_1027;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C03h
extern volatile uint8_t xdata g_rw_lxl_graylut_0404h_LXL_LUT0_data_1028;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C04h
extern volatile uint8_t xdata g_rw_lxl_graylut_0405h_LXL_LUT0_data_1029;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C05h

extern volatile uint8_t xdata g_rw_lxl_graylut_0406h_LXL_LUT0_data_1030;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C06h
extern volatile uint8_t xdata g_rw_lxl_graylut_0407h_LXL_LUT0_data_1031;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C07h
extern volatile uint8_t xdata g_rw_lxl_graylut_0408h_LXL_LUT0_data_1032;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C08h
extern volatile uint8_t xdata g_rw_lxl_graylut_0409h_LXL_LUT0_data_1033;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C09h
extern volatile uint8_t xdata g_rw_lxl_graylut_040Ah_LXL_LUT0_data_1034;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C0Ah

extern volatile uint8_t xdata g_rw_lxl_graylut_040Bh_LXL_LUT0_data_1035;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C0Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_040Ch_LXL_LUT0_data_1036;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C0Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_040Dh_LXL_LUT0_data_1037;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C0Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_040Eh_LXL_LUT0_data_1038;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C0Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_040Fh_LXL_LUT0_data_1039;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C0Fh

extern volatile uint8_t xdata g_rw_lxl_graylut_0410h_LXL_LUT0_data_1040;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C10h
extern volatile uint8_t xdata g_rw_lxl_graylut_0411h_LXL_LUT0_data_1041;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C11h
extern volatile uint8_t xdata g_rw_lxl_graylut_0412h_LXL_LUT0_data_1042;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C12h
extern volatile uint8_t xdata g_rw_lxl_graylut_0413h_LXL_LUT0_data_1043;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C13h
extern volatile uint8_t xdata g_rw_lxl_graylut_0414h_LXL_LUT0_data_1044;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C14h

extern volatile uint8_t xdata g_rw_lxl_graylut_0415h_LXL_LUT0_data_1045;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C15h
extern volatile uint8_t xdata g_rw_lxl_graylut_0416h_LXL_LUT0_data_1046;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C16h
extern volatile uint8_t xdata g_rw_lxl_graylut_0417h_LXL_LUT0_data_1047;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C17h
extern volatile uint8_t xdata g_rw_lxl_graylut_0418h_LXL_LUT0_data_1048;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C18h
extern volatile uint8_t xdata g_rw_lxl_graylut_0419h_LXL_LUT0_data_1049;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C19h

extern volatile uint8_t xdata g_rw_lxl_graylut_041Ah_LXL_LUT0_data_1050;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C1Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_041Bh_LXL_LUT0_data_1051;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C1Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_041Ch_LXL_LUT0_data_1052;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C1Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_041Dh_LXL_LUT0_data_1053;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C1Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_041Eh_LXL_LUT0_data_1054;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C1Eh

extern volatile uint8_t xdata g_rw_lxl_graylut_041Fh_LXL_LUT0_data_1055;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C1Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0420h_LXL_LUT0_data_1056;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C20h
extern volatile uint8_t xdata g_rw_lxl_graylut_0421h_LXL_LUT0_data_1057;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C21h
extern volatile uint8_t xdata g_rw_lxl_graylut_0422h_LXL_LUT0_data_1058;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C22h
extern volatile uint8_t xdata g_rw_lxl_graylut_0423h_LXL_LUT0_data_1059;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C23h

extern volatile uint8_t xdata g_rw_lxl_graylut_0424h_LXL_LUT0_data_1060;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C24h
extern volatile uint8_t xdata g_rw_lxl_graylut_0425h_LXL_LUT0_data_1061;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C25h
extern volatile uint8_t xdata g_rw_lxl_graylut_0426h_LXL_LUT0_data_1062;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C26h
extern volatile uint8_t xdata g_rw_lxl_graylut_0427h_LXL_LUT0_data_1063;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C27h
extern volatile uint8_t xdata g_rw_lxl_graylut_0428h_LXL_LUT0_data_1064;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C28h

extern volatile uint8_t xdata g_rw_lxl_graylut_0429h_LXL_LUT0_data_1065;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C29h
extern volatile uint8_t xdata g_rw_lxl_graylut_042Ah_LXL_LUT0_data_1066;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C2Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_042Bh_LXL_LUT0_data_1067;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C2Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_042Ch_LXL_LUT0_data_1068;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C2Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_042Dh_LXL_LUT0_data_1069;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C2Dh

extern volatile uint8_t xdata g_rw_lxl_graylut_042Eh_LXL_LUT0_data_1070;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C2Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_042Fh_LXL_LUT0_data_1071;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C2Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0430h_LXL_LUT0_data_1072;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C30h
extern volatile uint8_t xdata g_rw_lxl_graylut_0431h_LXL_LUT0_data_1073;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C31h
extern volatile uint8_t xdata g_rw_lxl_graylut_0432h_LXL_LUT0_data_1074;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C32h

extern volatile uint8_t xdata g_rw_lxl_graylut_0433h_LXL_LUT0_data_1075;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C33h
extern volatile uint8_t xdata g_rw_lxl_graylut_0434h_LXL_LUT0_data_1076;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C34h
extern volatile uint8_t xdata g_rw_lxl_graylut_0435h_LXL_LUT0_data_1077;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C35h
extern volatile uint8_t xdata g_rw_lxl_graylut_0436h_LXL_LUT0_data_1078;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C36h
extern volatile uint8_t xdata g_rw_lxl_graylut_0437h_LXL_LUT0_data_1079;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C37h

extern volatile uint8_t xdata g_rw_lxl_graylut_0438h_LXL_LUT0_data_1080;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C38h
extern volatile uint8_t xdata g_rw_lxl_graylut_0439h_LXL_LUT0_data_1081;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C39h
extern volatile uint8_t xdata g_rw_lxl_graylut_043Ah_LXL_LUT0_data_1082;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C3Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_043Bh_LXL_LUT0_data_1083;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C3Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_043Ch_LXL_LUT0_data_1084;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C3Ch

extern volatile uint8_t xdata g_rw_lxl_graylut_043Dh_LXL_LUT0_data_1085;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C3Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_043Eh_LXL_LUT0_data_1086;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C3Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_043Fh_LXL_LUT0_data_1087;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C3Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0440h_LXL_LUT0_data_1088;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C40h
extern volatile uint8_t xdata g_rw_lxl_graylut_0441h_LXL_LUT0_data_1089;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C41h

extern volatile uint8_t xdata g_rw_lxl_graylut_0442h_LXL_LUT0_data_1090;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C42h
extern volatile uint8_t xdata g_rw_lxl_graylut_0443h_LXL_LUT0_data_1091;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C43h
extern volatile uint8_t xdata g_rw_lxl_graylut_0444h_LXL_LUT0_data_1092;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C44h
extern volatile uint8_t xdata g_rw_lxl_graylut_0445h_LXL_LUT0_data_1093;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C45h
extern volatile uint8_t xdata g_rw_lxl_graylut_0446h_LXL_LUT0_data_1094;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C46h

extern volatile uint8_t xdata g_rw_lxl_graylut_0447h_LXL_LUT0_data_1095;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C47h
extern volatile uint8_t xdata g_rw_lxl_graylut_0448h_LXL_LUT0_data_1096;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C48h
extern volatile uint8_t xdata g_rw_lxl_graylut_0449h_LXL_LUT0_data_1097;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C49h
extern volatile uint8_t xdata g_rw_lxl_graylut_044Ah_LXL_LUT0_data_1098;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C4Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_044Bh_LXL_LUT0_data_1099;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C4Bh

extern volatile uint8_t xdata g_rw_lxl_graylut_044Ch_LXL_LUT0_data_1100;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C4Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_044Dh_LXL_LUT0_data_1101;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C4Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_044Eh_LXL_LUT0_data_1102;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C4Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_044Fh_LXL_LUT0_data_1103;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C4Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0450h_LXL_LUT0_data_1104;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C50h

extern volatile uint8_t xdata g_rw_lxl_graylut_0451h_LXL_LUT0_data_1105;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C51h
extern volatile uint8_t xdata g_rw_lxl_graylut_0452h_LXL_LUT0_data_1106;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C52h
extern volatile uint8_t xdata g_rw_lxl_graylut_0453h_LXL_LUT0_data_1107;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C53h
extern volatile uint8_t xdata g_rw_lxl_graylut_0454h_LXL_LUT0_data_1108;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C54h
extern volatile uint8_t xdata g_rw_lxl_graylut_0455h_LXL_LUT0_data_1109;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C55h

extern volatile uint8_t xdata g_rw_lxl_graylut_0456h_LXL_LUT0_data_1110;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C56h
extern volatile uint8_t xdata g_rw_lxl_graylut_0457h_LXL_LUT0_data_1111;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C57h
extern volatile uint8_t xdata g_rw_lxl_graylut_0458h_LXL_LUT0_data_1112;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C58h
extern volatile uint8_t xdata g_rw_lxl_graylut_0459h_LXL_LUT0_data_1113;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C59h
extern volatile uint8_t xdata g_rw_lxl_graylut_045Ah_LXL_LUT0_data_1114;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C5Ah

extern volatile uint8_t xdata g_rw_lxl_graylut_045Bh_LXL_LUT0_data_1115;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C5Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_045Ch_LXL_LUT0_data_1116;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C5Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_045Dh_LXL_LUT0_data_1117;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C5Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_045Eh_LXL_LUT0_data_1118;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C5Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_045Fh_LXL_LUT0_data_1119;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C5Fh

extern volatile uint8_t xdata g_rw_lxl_graylut_0460h_LXL_LUT0_data_1120;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C60h
extern volatile uint8_t xdata g_rw_lxl_graylut_0461h_LXL_LUT0_data_1121;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C61h
extern volatile uint8_t xdata g_rw_lxl_graylut_0462h_LXL_LUT0_data_1122;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C62h
extern volatile uint8_t xdata g_rw_lxl_graylut_0463h_LXL_LUT0_data_1123;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C63h
extern volatile uint8_t xdata g_rw_lxl_graylut_0464h_LXL_LUT0_data_1124;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C64h

extern volatile uint8_t xdata g_rw_lxl_graylut_0465h_LXL_LUT0_data_1125;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C65h
extern volatile uint8_t xdata g_rw_lxl_graylut_0466h_LXL_LUT0_data_1126;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C66h
extern volatile uint8_t xdata g_rw_lxl_graylut_0467h_LXL_LUT0_data_1127;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C67h
extern volatile uint8_t xdata g_rw_lxl_graylut_0468h_LXL_LUT0_data_1128;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C68h
extern volatile uint8_t xdata g_rw_lxl_graylut_0469h_LXL_LUT0_data_1129;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C69h

extern volatile uint8_t xdata g_rw_lxl_graylut_046Ah_LXL_LUT0_data_1130;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C6Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_046Bh_LXL_LUT0_data_1131;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C6Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_046Ch_LXL_LUT0_data_1132;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C6Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_046Dh_LXL_LUT0_data_1133;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C6Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_046Eh_LXL_LUT0_data_1134;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C6Eh

extern volatile uint8_t xdata g_rw_lxl_graylut_046Fh_LXL_LUT0_data_1135;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C6Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0470h_LXL_LUT0_data_1136;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C70h
extern volatile uint8_t xdata g_rw_lxl_graylut_0471h_LXL_LUT0_data_1137;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C71h
extern volatile uint8_t xdata g_rw_lxl_graylut_0472h_LXL_LUT0_data_1138;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C72h
extern volatile uint8_t xdata g_rw_lxl_graylut_0473h_LXL_LUT0_data_1139;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C73h

extern volatile uint8_t xdata g_rw_lxl_graylut_0474h_LXL_LUT0_data_1140;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C74h
extern volatile uint8_t xdata g_rw_lxl_graylut_0475h_LXL_LUT0_data_1141;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C75h
extern volatile uint8_t xdata g_rw_lxl_graylut_0476h_LXL_LUT0_data_1142;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C76h
extern volatile uint8_t xdata g_rw_lxl_graylut_0477h_LXL_LUT0_data_1143;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C77h
extern volatile uint8_t xdata g_rw_lxl_graylut_0478h_LXL_LUT0_data_1144;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C78h

extern volatile uint8_t xdata g_rw_lxl_graylut_0479h_LXL_LUT0_data_1145;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C79h
extern volatile uint8_t xdata g_rw_lxl_graylut_047Ah_LXL_LUT0_data_1146;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C7Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_047Bh_LXL_LUT0_data_1147;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C7Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_047Ch_LXL_LUT0_data_1148;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C7Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_047Dh_LXL_LUT0_data_1149;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C7Dh

extern volatile uint8_t xdata g_rw_lxl_graylut_047Eh_LXL_LUT0_data_1150;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C7Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_047Fh_LXL_LUT0_data_1151;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C7Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0480h_LXL_LUT0_data_1152;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C80h
extern volatile uint8_t xdata g_rw_lxl_graylut_0481h_LXL_LUT0_data_1153;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C81h
extern volatile uint8_t xdata g_rw_lxl_graylut_0482h_LXL_LUT0_data_1154;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C82h

extern volatile uint8_t xdata g_rw_lxl_graylut_0483h_LXL_LUT0_data_1155;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C83h
extern volatile uint8_t xdata g_rw_lxl_graylut_0484h_LXL_LUT0_data_1156;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C84h
extern volatile uint8_t xdata g_rw_lxl_graylut_0485h_LXL_LUT0_data_1157;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C85h
extern volatile uint8_t xdata g_rw_lxl_graylut_0486h_LXL_LUT0_data_1158;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C86h
extern volatile uint8_t xdata g_rw_lxl_graylut_0487h_LXL_LUT0_data_1159;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C87h

extern volatile uint8_t xdata g_rw_lxl_graylut_0488h_LXL_LUT0_data_1160;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C88h
extern volatile uint8_t xdata g_rw_lxl_graylut_0489h_LXL_LUT0_data_1161;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C89h
extern volatile uint8_t xdata g_rw_lxl_graylut_048Ah_LXL_LUT0_data_1162;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C8Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_048Bh_LXL_LUT0_data_1163;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C8Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_048Ch_LXL_LUT0_data_1164;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C8Ch

extern volatile uint8_t xdata g_rw_lxl_graylut_048Dh_LXL_LUT0_data_1165;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C8Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_048Eh_LXL_LUT0_data_1166;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C8Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_048Fh_LXL_LUT0_data_1167;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C8Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0490h_LXL_LUT0_data_1168;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C90h
extern volatile uint8_t xdata g_rw_lxl_graylut_0491h_LXL_LUT0_data_1169;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C91h

extern volatile uint8_t xdata g_rw_lxl_graylut_0492h_LXL_LUT0_data_1170;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C92h
extern volatile uint8_t xdata g_rw_lxl_graylut_0493h_LXL_LUT0_data_1171;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C93h
extern volatile uint8_t xdata g_rw_lxl_graylut_0494h_LXL_LUT0_data_1172;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C94h
extern volatile uint8_t xdata g_rw_lxl_graylut_0495h_LXL_LUT0_data_1173;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C95h
extern volatile uint8_t xdata g_rw_lxl_graylut_0496h_LXL_LUT0_data_1174;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C96h

extern volatile uint8_t xdata g_rw_lxl_graylut_0497h_LXL_LUT0_data_1175;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C97h
extern volatile uint8_t xdata g_rw_lxl_graylut_0498h_LXL_LUT0_data_1176;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C98h
extern volatile uint8_t xdata g_rw_lxl_graylut_0499h_LXL_LUT0_data_1177;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C99h
extern volatile uint8_t xdata g_rw_lxl_graylut_049Ah_LXL_LUT0_data_1178;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C9Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_049Bh_LXL_LUT0_data_1179;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C9Bh

extern volatile uint8_t xdata g_rw_lxl_graylut_049Ch_LXL_LUT0_data_1180;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C9Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_049Dh_LXL_LUT0_data_1181;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C9Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_049Eh_LXL_LUT0_data_1182;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C9Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_049Fh_LXL_LUT0_data_1183;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8C9Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_04A0h_LXL_LUT0_data_1184;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CA0h

extern volatile uint8_t xdata g_rw_lxl_graylut_04A1h_LXL_LUT0_data_1185;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CA1h
extern volatile uint8_t xdata g_rw_lxl_graylut_04A2h_LXL_LUT0_data_1186;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CA2h
extern volatile uint8_t xdata g_rw_lxl_graylut_04A3h_LXL_LUT0_data_1187;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CA3h
extern volatile uint8_t xdata g_rw_lxl_graylut_04A4h_LXL_LUT0_data_1188;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CA4h
extern volatile uint8_t xdata g_rw_lxl_graylut_04A5h_LXL_LUT0_data_1189;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CA5h

extern volatile uint8_t xdata g_rw_lxl_graylut_04A6h_LXL_LUT0_data_1190;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CA6h
extern volatile uint8_t xdata g_rw_lxl_graylut_04A7h_LXL_LUT0_data_1191;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CA7h
extern volatile uint8_t xdata g_rw_lxl_graylut_04A8h_LXL_LUT0_data_1192;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CA8h
extern volatile uint8_t xdata g_rw_lxl_graylut_04A9h_LXL_LUT0_data_1193;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CA9h
extern volatile uint8_t xdata g_rw_lxl_graylut_04AAh_LXL_LUT0_data_1194;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CAAh

extern volatile uint8_t xdata g_rw_lxl_graylut_04ABh_LXL_LUT0_data_1195;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CABh
extern volatile uint8_t xdata g_rw_lxl_graylut_04ACh_LXL_LUT0_data_1196;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CACh
extern volatile uint8_t xdata g_rw_lxl_graylut_04ADh_LXL_LUT0_data_1197;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CADh
extern volatile uint8_t xdata g_rw_lxl_graylut_04AEh_LXL_LUT0_data_1198;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CAEh
extern volatile uint8_t xdata g_rw_lxl_graylut_04AFh_LXL_LUT0_data_1199;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CAFh

extern volatile uint8_t xdata g_rw_lxl_graylut_04B0h_LXL_LUT0_data_1200;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CB0h
extern volatile uint8_t xdata g_rw_lxl_graylut_04B1h_LXL_LUT0_data_1201;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CB1h
extern volatile uint8_t xdata g_rw_lxl_graylut_04B2h_LXL_LUT0_data_1202;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CB2h
extern volatile uint8_t xdata g_rw_lxl_graylut_04B3h_LXL_LUT0_data_1203;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CB3h
extern volatile uint8_t xdata g_rw_lxl_graylut_04B4h_LXL_LUT0_data_1204;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CB4h

extern volatile uint8_t xdata g_rw_lxl_graylut_04B5h_LXL_LUT0_data_1205;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CB5h
extern volatile uint8_t xdata g_rw_lxl_graylut_04B6h_LXL_LUT0_data_1206;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CB6h
extern volatile uint8_t xdata g_rw_lxl_graylut_04B7h_LXL_LUT0_data_1207;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CB7h
extern volatile uint8_t xdata g_rw_lxl_graylut_04B8h_LXL_LUT0_data_1208;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CB8h
extern volatile uint8_t xdata g_rw_lxl_graylut_04B9h_LXL_LUT0_data_1209;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CB9h

extern volatile uint8_t xdata g_rw_lxl_graylut_04BAh_LXL_LUT0_data_1210;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CBAh
extern volatile uint8_t xdata g_rw_lxl_graylut_04BBh_LXL_LUT0_data_1211;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CBBh
extern volatile uint8_t xdata g_rw_lxl_graylut_04BCh_LXL_LUT0_data_1212;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CBCh
extern volatile uint8_t xdata g_rw_lxl_graylut_04BDh_LXL_LUT0_data_1213;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CBDh
extern volatile uint8_t xdata g_rw_lxl_graylut_04BEh_LXL_LUT0_data_1214;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CBEh

extern volatile uint8_t xdata g_rw_lxl_graylut_04BFh_LXL_LUT0_data_1215;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CBFh
extern volatile uint8_t xdata g_rw_lxl_graylut_04C0h_LXL_LUT0_data_1216;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CC0h
extern volatile uint8_t xdata g_rw_lxl_graylut_04C1h_LXL_LUT0_data_1217;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CC1h
extern volatile uint8_t xdata g_rw_lxl_graylut_04C2h_LXL_LUT0_data_1218;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CC2h
extern volatile uint8_t xdata g_rw_lxl_graylut_04C3h_LXL_LUT0_data_1219;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CC3h

extern volatile uint8_t xdata g_rw_lxl_graylut_04C4h_LXL_LUT0_data_1220;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CC4h
extern volatile uint8_t xdata g_rw_lxl_graylut_04C5h_LXL_LUT0_data_1221;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CC5h
extern volatile uint8_t xdata g_rw_lxl_graylut_04C6h_LXL_LUT0_data_1222;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CC6h
extern volatile uint8_t xdata g_rw_lxl_graylut_04C7h_LXL_LUT0_data_1223;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CC7h
extern volatile uint8_t xdata g_rw_lxl_graylut_04C8h_LXL_LUT0_data_1224;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CC8h

extern volatile uint8_t xdata g_rw_lxl_graylut_04C9h_LXL_LUT0_data_1225;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CC9h
extern volatile uint8_t xdata g_rw_lxl_graylut_04CAh_LXL_LUT0_data_1226;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CCAh
extern volatile uint8_t xdata g_rw_lxl_graylut_04CBh_LXL_LUT0_data_1227;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CCBh
extern volatile uint8_t xdata g_rw_lxl_graylut_04CCh_LXL_LUT0_data_1228;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CCCh
extern volatile uint8_t xdata g_rw_lxl_graylut_04CDh_LXL_LUT0_data_1229;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CCDh

extern volatile uint8_t xdata g_rw_lxl_graylut_04CEh_LXL_LUT0_data_1230;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CCEh
extern volatile uint8_t xdata g_rw_lxl_graylut_04CFh_LXL_LUT0_data_1231;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CCFh
extern volatile uint8_t xdata g_rw_lxl_graylut_04D0h_LXL_LUT0_data_1232;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CD0h
extern volatile uint8_t xdata g_rw_lxl_graylut_04D1h_LXL_LUT0_data_1233;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CD1h
extern volatile uint8_t xdata g_rw_lxl_graylut_04D2h_LXL_LUT0_data_1234;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CD2h

extern volatile uint8_t xdata g_rw_lxl_graylut_04D3h_LXL_LUT0_data_1235;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CD3h
extern volatile uint8_t xdata g_rw_lxl_graylut_04D4h_LXL_LUT0_data_1236;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CD4h
extern volatile uint8_t xdata g_rw_lxl_graylut_04D5h_LXL_LUT0_data_1237;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CD5h
extern volatile uint8_t xdata g_rw_lxl_graylut_04D6h_LXL_LUT0_data_1238;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CD6h
extern volatile uint8_t xdata g_rw_lxl_graylut_04D7h_LXL_LUT0_data_1239;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CD7h

extern volatile uint8_t xdata g_rw_lxl_graylut_04D8h_LXL_LUT0_data_1240;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CD8h
extern volatile uint8_t xdata g_rw_lxl_graylut_04D9h_LXL_LUT0_data_1241;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CD9h
extern volatile uint8_t xdata g_rw_lxl_graylut_04DAh_LXL_LUT0_data_1242;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CDAh
extern volatile uint8_t xdata g_rw_lxl_graylut_04DBh_LXL_LUT0_data_1243;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CDBh
extern volatile uint8_t xdata g_rw_lxl_graylut_04DCh_LXL_LUT0_data_1244;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CDCh

extern volatile uint8_t xdata g_rw_lxl_graylut_04DDh_LXL_LUT0_data_1245;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CDDh
extern volatile uint8_t xdata g_rw_lxl_graylut_04DEh_LXL_LUT0_data_1246;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CDEh
extern volatile uint8_t xdata g_rw_lxl_graylut_04DFh_LXL_LUT0_data_1247;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CDFh
extern volatile uint8_t xdata g_rw_lxl_graylut_04E0h_LXL_LUT0_data_1248;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CE0h
extern volatile uint8_t xdata g_rw_lxl_graylut_04E1h_LXL_LUT0_data_1249;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CE1h

extern volatile uint8_t xdata g_rw_lxl_graylut_04E2h_LXL_LUT0_data_1250;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CE2h
extern volatile uint8_t xdata g_rw_lxl_graylut_04E3h_LXL_LUT0_data_1251;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CE3h
extern volatile uint8_t xdata g_rw_lxl_graylut_04E4h_LXL_LUT0_data_1252;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CE4h
extern volatile uint8_t xdata g_rw_lxl_graylut_04E5h_LXL_LUT0_data_1253;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CE5h
extern volatile uint8_t xdata g_rw_lxl_graylut_04E6h_LXL_LUT0_data_1254;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CE6h

extern volatile uint8_t xdata g_rw_lxl_graylut_04E7h_LXL_LUT0_data_1255;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CE7h
extern volatile uint8_t xdata g_rw_lxl_graylut_04E8h_LXL_LUT0_data_1256;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CE8h
extern volatile uint8_t xdata g_rw_lxl_graylut_04E9h_LXL_LUT0_data_1257;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CE9h
extern volatile uint8_t xdata g_rw_lxl_graylut_04EAh_LXL_LUT0_data_1258;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CEAh
extern volatile uint8_t xdata g_rw_lxl_graylut_04EBh_LXL_LUT0_data_1259;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CEBh

extern volatile uint8_t xdata g_rw_lxl_graylut_04ECh_LXL_LUT0_data_1260;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CECh
extern volatile uint8_t xdata g_rw_lxl_graylut_04EDh_LXL_LUT0_data_1261;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CEDh
extern volatile uint8_t xdata g_rw_lxl_graylut_04EEh_LXL_LUT0_data_1262;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CEEh
extern volatile uint8_t xdata g_rw_lxl_graylut_04EFh_LXL_LUT0_data_1263;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CEFh
extern volatile uint8_t xdata g_rw_lxl_graylut_04F0h_LXL_LUT0_data_1264;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CF0h

extern volatile uint8_t xdata g_rw_lxl_graylut_04F1h_LXL_LUT0_data_1265;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CF1h
extern volatile uint8_t xdata g_rw_lxl_graylut_04F2h_LXL_LUT0_data_1266;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CF2h
extern volatile uint8_t xdata g_rw_lxl_graylut_04F3h_LXL_LUT0_data_1267;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CF3h
extern volatile uint8_t xdata g_rw_lxl_graylut_04F4h_LXL_LUT0_data_1268;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CF4h
extern volatile uint8_t xdata g_rw_lxl_graylut_04F5h_LXL_LUT0_data_1269;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CF5h

extern volatile uint8_t xdata g_rw_lxl_graylut_04F6h_LXL_LUT0_data_1270;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CF6h
extern volatile uint8_t xdata g_rw_lxl_graylut_04F7h_LXL_LUT0_data_1271;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CF7h
extern volatile uint8_t xdata g_rw_lxl_graylut_04F8h_LXL_LUT0_data_1272;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CF8h
extern volatile uint8_t xdata g_rw_lxl_graylut_04F9h_LXL_LUT0_data_1273;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CF9h
extern volatile uint8_t xdata g_rw_lxl_graylut_04FAh_LXL_LUT0_data_1274;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CFAh

extern volatile uint8_t xdata g_rw_lxl_graylut_04FBh_LXL_LUT0_data_1275;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CFBh
extern volatile uint8_t xdata g_rw_lxl_graylut_04FCh_LXL_LUT0_data_1276;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CFCh
extern volatile uint8_t xdata g_rw_lxl_graylut_04FDh_LXL_LUT0_data_1277;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CFDh
extern volatile uint8_t xdata g_rw_lxl_graylut_04FEh_LXL_LUT0_data_1278;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CFEh
extern volatile uint8_t xdata g_rw_lxl_graylut_04FFh_LXL_LUT0_data_1279;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8CFFh

extern volatile uint8_t xdata g_rw_lxl_graylut_0500h_LXL_LUT0_data_1280;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D00h
extern volatile uint8_t xdata g_rw_lxl_graylut_0501h_LXL_LUT0_data_1281;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D01h
extern volatile uint8_t xdata g_rw_lxl_graylut_0502h_LXL_LUT0_data_1282;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D02h
extern volatile uint8_t xdata g_rw_lxl_graylut_0503h_LXL_LUT0_data_1283;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D03h
extern volatile uint8_t xdata g_rw_lxl_graylut_0504h_LXL_LUT0_data_1284;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D04h

extern volatile uint8_t xdata g_rw_lxl_graylut_0505h_LXL_LUT0_data_1285;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D05h
extern volatile uint8_t xdata g_rw_lxl_graylut_0506h_LXL_LUT0_data_1286;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D06h
extern volatile uint8_t xdata g_rw_lxl_graylut_0507h_LXL_LUT0_data_1287;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D07h
extern volatile uint8_t xdata g_rw_lxl_graylut_0508h_LXL_LUT0_data_1288;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D08h
extern volatile uint8_t xdata g_rw_lxl_graylut_0509h_LXL_LUT0_data_1289;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D09h

extern volatile uint8_t xdata g_rw_lxl_graylut_050Ah_LXL_LUT0_data_1290;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D0Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_050Bh_LXL_LUT0_data_1291;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D0Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_050Ch_LXL_LUT0_data_1292;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D0Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_050Dh_LXL_LUT0_data_1293;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D0Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_050Eh_LXL_LUT0_data_1294;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D0Eh

extern volatile uint8_t xdata g_rw_lxl_graylut_050Fh_LXL_LUT0_data_1295;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D0Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0510h_LXL_LUT0_data_1296;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D10h
extern volatile uint8_t xdata g_rw_lxl_graylut_0511h_LXL_LUT0_data_1297;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D11h
extern volatile uint8_t xdata g_rw_lxl_graylut_0512h_LXL_LUT0_data_1298;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D12h
extern volatile uint8_t xdata g_rw_lxl_graylut_0513h_LXL_LUT0_data_1299;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D13h

extern volatile uint8_t xdata g_rw_lxl_graylut_0514h_LXL_LUT0_data_1300;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D14h
extern volatile uint8_t xdata g_rw_lxl_graylut_0515h_LXL_LUT0_data_1301;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D15h
extern volatile uint8_t xdata g_rw_lxl_graylut_0516h_LXL_LUT0_data_1302;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D16h
extern volatile uint8_t xdata g_rw_lxl_graylut_0517h_LXL_LUT0_data_1303;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D17h
extern volatile uint8_t xdata g_rw_lxl_graylut_0518h_LXL_LUT0_data_1304;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D18h

extern volatile uint8_t xdata g_rw_lxl_graylut_0519h_LXL_LUT0_data_1305;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D19h
extern volatile uint8_t xdata g_rw_lxl_graylut_051Ah_LXL_LUT0_data_1306;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D1Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_051Bh_LXL_LUT0_data_1307;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D1Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_051Ch_LXL_LUT0_data_1308;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D1Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_051Dh_LXL_LUT0_data_1309;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D1Dh

extern volatile uint8_t xdata g_rw_lxl_graylut_051Eh_LXL_LUT0_data_1310;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D1Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_051Fh_LXL_LUT0_data_1311;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D1Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0520h_LXL_LUT0_data_1312;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D20h
extern volatile uint8_t xdata g_rw_lxl_graylut_0521h_LXL_LUT0_data_1313;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D21h
extern volatile uint8_t xdata g_rw_lxl_graylut_0522h_LXL_LUT0_data_1314;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D22h

extern volatile uint8_t xdata g_rw_lxl_graylut_0523h_LXL_LUT0_data_1315;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D23h
extern volatile uint8_t xdata g_rw_lxl_graylut_0524h_LXL_LUT0_data_1316;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D24h
extern volatile uint8_t xdata g_rw_lxl_graylut_0525h_LXL_LUT0_data_1317;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D25h
extern volatile uint8_t xdata g_rw_lxl_graylut_0526h_LXL_LUT0_data_1318;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D26h
extern volatile uint8_t xdata g_rw_lxl_graylut_0527h_LXL_LUT0_data_1319;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D27h

extern volatile uint8_t xdata g_rw_lxl_graylut_0528h_LXL_LUT0_data_1320;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D28h
extern volatile uint8_t xdata g_rw_lxl_graylut_0529h_LXL_LUT0_data_1321;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D29h
extern volatile uint8_t xdata g_rw_lxl_graylut_052Ah_LXL_LUT0_data_1322;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D2Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_052Bh_LXL_LUT0_data_1323;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D2Bh
extern volatile uint8_t xdata g_rw_lxl_graylut_052Ch_LXL_LUT0_data_1324;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D2Ch

extern volatile uint8_t xdata g_rw_lxl_graylut_052Dh_LXL_LUT0_data_1325;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D2Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_052Eh_LXL_LUT0_data_1326;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D2Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_052Fh_LXL_LUT0_data_1327;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D2Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0530h_LXL_LUT0_data_1328;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D30h
extern volatile uint8_t xdata g_rw_lxl_graylut_0531h_LXL_LUT0_data_1329;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D31h

extern volatile uint8_t xdata g_rw_lxl_graylut_0532h_LXL_LUT0_data_1330;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D32h
extern volatile uint8_t xdata g_rw_lxl_graylut_0533h_LXL_LUT0_data_1331;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D33h
extern volatile uint8_t xdata g_rw_lxl_graylut_0534h_LXL_LUT0_data_1332;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D34h
extern volatile uint8_t xdata g_rw_lxl_graylut_0535h_LXL_LUT0_data_1333;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D35h
extern volatile uint8_t xdata g_rw_lxl_graylut_0536h_LXL_LUT0_data_1334;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D36h

extern volatile uint8_t xdata g_rw_lxl_graylut_0537h_LXL_LUT0_data_1335;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D37h
extern volatile uint8_t xdata g_rw_lxl_graylut_0538h_LXL_LUT0_data_1336;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D38h
extern volatile uint8_t xdata g_rw_lxl_graylut_0539h_LXL_LUT0_data_1337;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D39h
extern volatile uint8_t xdata g_rw_lxl_graylut_053Ah_LXL_LUT0_data_1338;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D3Ah
extern volatile uint8_t xdata g_rw_lxl_graylut_053Bh_LXL_LUT0_data_1339;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D3Bh

extern volatile uint8_t xdata g_rw_lxl_graylut_053Ch_LXL_LUT0_data_1340;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D3Ch
extern volatile uint8_t xdata g_rw_lxl_graylut_053Dh_LXL_LUT0_data_1341;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D3Dh
extern volatile uint8_t xdata g_rw_lxl_graylut_053Eh_LXL_LUT0_data_1342;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D3Eh
extern volatile uint8_t xdata g_rw_lxl_graylut_053Fh_LXL_LUT0_data_1343;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D3Fh
extern volatile uint8_t xdata g_rw_lxl_graylut_0540h_LXL_LUT0_data_1344;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D40h

extern volatile uint8_t xdata g_rw_lxl_graylut_0541h_LXL_LUT0_data_1345;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D41h
extern volatile uint8_t xdata g_rw_lxl_graylut_0542h_LXL_LUT0_data_1346;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D42h
extern volatile uint8_t xdata g_rw_lxl_graylut_0543h_LXL_LUT0_data_1347;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D43h
extern volatile uint8_t xdata g_rw_lxl_graylut_0544h_LXL_LUT0_data_1348;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D44h
extern volatile uint8_t xdata g_rw_lxl_graylut_0545h_LXL_LUT0_data_1349;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D45h

extern volatile uint8_t xdata g_rw_lxl_graylut_0546h_LXL_LUT0_data_1350;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D46h
extern volatile uint8_t xdata g_rw_lxl_graylut_0547h_LXL_LUT0_data_1351;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D47h
extern volatile uint8_t xdata g_rw_lxl_graylut_0548h_LXL_LUT0_data_1352;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D48h
extern volatile uint8_t xdata g_rw_lxl_graylut_0549h_LXL_LUT0_data_1353;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D49h
extern volatile uint8_t xdata g_rw_lxl_graylut_054Ah_LXL_LUT0_data_1354;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D4Ah

extern volatile uint8_t xdata g_rw_lxl_graylut_054Bh_LXL_LUT0_data_1355;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8D4Bh

#endif