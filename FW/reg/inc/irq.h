#ifndef _IRQ_H_
#define _IRQ_H_
#include "reg_include.h"

union rw_irq_0001h
{
    unsigned char byte;
    struct
    {
        unsigned char r_vx1_sym_irq_thr_bits_9_8                                             : 2;        // [msb:lsb] = [9:8], val = 0
        unsigned char r_prbs7_unlock_irq_thr_bits_5_0                                        : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_irq_0001h xdata g_rw_irq_0001h;    // Absolute Address = 4801h

union rw_irq_0002h
{
    unsigned char byte;
    struct
    {
        unsigned char r_prbs7_unlock_irq_thr_bits_9_6                                        : 4;        // [msb:lsb] = [9:6], val = 0
        unsigned char r_inproc_irq_de_cnt1_bits_3_0                                          : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_irq_0002h xdata g_rw_irq_0002h;    // Absolute Address = 4802h

union rw_irq_0005h
{
    unsigned char byte;
    struct
    {
        unsigned char r_inproc_irq_pix_cnt1_bits_11_8                                        : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_inproc_irq_de_cnt2_bits_3_0                                          : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_irq_0005h xdata g_rw_irq_0005h;    // Absolute Address = 4805h

union rw_irq_0008h
{
    unsigned char byte;
    struct
    {
        unsigned char r_inproc_irq_pix_cnt2_bits_11_8                                        : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_outproc_irq_de_cnt1_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_irq_0008h xdata g_rw_irq_0008h;    // Absolute Address = 4808h

union rw_irq_000Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_outproc_irq_pix_cnt1_bits_11_8                                       : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_outproc_irq_de_cnt2_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_irq_000Bh xdata g_rw_irq_000Bh;    // Absolute Address = 480Bh

//------------------------------------------------------------------------------------------------------------------------
union rw_irq_000Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_outproc_irq_pix_cnt2_bits_11_8                                       : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_frm_irq_cnt1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_irq_000Eh xdata g_rw_irq_000Eh;    // Absolute Address = 480Eh

union rw_irq_0011h
{
    unsigned char byte;
    struct
    {
        unsigned char r_frm_irq_cnt2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char reserved1                                                              : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_irq_0011h xdata g_rw_irq_0011h;    // Absolute Address = 4811h

union rw_irq_0012h
{
    unsigned char byte;
    struct
    {
        unsigned char r_patdet_sdpol_en                                                      : 1;        // val = 0
        unsigned char r_lvds_unlock_irq_en                                                   : 1;        // val = 0
        unsigned char r_vx1_unlock_irq_en                                                    : 1;        // val = 0
        unsigned char r_spll_unlock_irq_en                                                   : 1;        // val = 0
        unsigned char r_mpll_unlock_irq_en                                                   : 1;        // val = 0
        unsigned char r_p2p_unlock_irq_en                                                    : 1;        // val = 0
        unsigned char r_sym_unlock_en                                                        : 1;        // val = 0
        unsigned char r_bcc_lock_en                                                          : 1;        // val = 0
    }bits;
};
extern volatile union rw_irq_0012h xdata g_rw_irq_0012h;    // Absolute Address = 4812h

union rw_irq_0013h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bcc_busy_en                                                          : 1;        // val = 0
        unsigned char r_bcc_done_en                                                          : 1;        // val = 0
        unsigned char r_bcc_reg_finish_en                                                    : 1;        // val = 0
        unsigned char r_rom_acc_ne_en                                                        : 1;        // val = 0
        unsigned char r_rom_acc_pe_en                                                        : 1;        // val = 0
        unsigned char r_irq_pwm2spi_en                                                       : 1;        // val = 0
        unsigned char r_vsync_en                                                             : 1;        // val = 0
        unsigned char r_state_chg_en                                                         : 1;        // val = 0
    }bits;
};
extern volatile union rw_irq_0013h xdata g_rw_irq_0013h;    // Absolute Address = 4813h

union rw_irq_0014h
{
    unsigned char byte;
    struct
    {
        unsigned char r_reg_finish_en                                                        : 1;        // val = 0
        unsigned char r_lut_finish_en                                                        : 1;        // val = 0
        unsigned char r_tcon_finish_en                                                       : 1;        // val = 0
        unsigned char r_out_esd_mask_en                                                      : 1;        // val = 0
        unsigned char r_cmd_finish_en                                                        : 1;        // val = 0
        unsigned char r_i2c_idle_en                                                          : 1;        // val = 0
        unsigned char reserved_1_by_tool                                                     : 1;
        unsigned char reserved_0_by_tool                                                     : 1;
    }bits;
};
extern volatile union rw_irq_0014h xdata g_rw_irq_0014h;    // Absolute Address = 4814h

//------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_irq_0000h_vx1_sym_thr;                                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 4800h
extern volatile uint8_t xdata g_rw_irq_0003h_inde_cnt1;                                                  // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 4803h
extern volatile uint8_t xdata g_rw_irq_0004h_inpix_cnt1;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 4804h
extern volatile uint8_t xdata g_rw_irq_0006h_inde_cnt2;                                                  // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 4806h
extern volatile uint8_t xdata g_rw_irq_0007h_inpix_cnt2;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 4807h

extern volatile uint8_t xdata g_rw_irq_0009h_outde_cnt1;                                                 // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 4809h
extern volatile uint8_t xdata g_rw_irq_000Ah_outpix_cnt1;                                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 480Ah
extern volatile uint8_t xdata g_rw_irq_000Ch_outde_cnt2;                                                 // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 480Ch
extern volatile uint8_t xdata g_rw_irq_000Dh_outpix_cnt2;                                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 480Dh
extern volatile uint8_t xdata g_rw_irq_000Fh_frm_cnt1;                                                   // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 480Fh

extern volatile uint8_t xdata g_rw_irq_0010h_frm_cnt2;                                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 4810h

#endif