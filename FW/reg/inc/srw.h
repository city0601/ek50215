#ifndef _SRW_H_
#define _SRW_H_
#include "reg_include.h"

union rw_srw_000Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_lut_srw_update_sel                                                   : 1;        // val = 0
        unsigned char reserved_6_by_tool                                                     : 1;
        unsigned char reserved_5_by_tool                                                     : 1;
        unsigned char reserved_4_by_tool                                                     : 1;
        unsigned char reserved_3_by_tool                                                     : 1;
        unsigned char reserved_2_by_tool                                                     : 1;
        unsigned char reserved_1_by_tool                                                     : 1;
        unsigned char reserved_0_by_tool                                                     : 1;
    }bits;
};
extern volatile union rw_srw_000Dh xdata g_rw_srw_000Dh;    // Absolute Address = 000Dh

union rw_srw_000Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_lut_srw_wren                                                         : 1;        // val = 0
        unsigned char r_lut_srw_rd_en                                                        : 1;        // val = 0
        unsigned char reserved_5_by_tool                                                     : 1;
        unsigned char reserved_4_by_tool                                                     : 1;
        unsigned char reserved_3_by_tool                                                     : 1;
        unsigned char reserved_2_by_tool                                                     : 1;
        unsigned char reserved_1_by_tool                                                     : 1;
        unsigned char reserved_0_by_tool                                                     : 1;
    }bits;
};
extern volatile union rw_srw_000Eh xdata g_rw_srw_000Eh;    // Absolute Address = 000Eh


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_srw_0000h_reserved1;                                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 0000h
extern volatile uint8_t xdata g_rw_srw_0001h_lut_ip_sel;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 0001h
extern volatile uint8_t xdata g_rw_srw_0002h_lut_sram_sel;                                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 0002h
extern volatile uint8_t xdata g_rw_srw_0003h_lut_addr;                                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 0003h
extern volatile uint8_t xdata g_rw_srw_0004h_lut_addr;                                                   // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 0004h

extern volatile uint8_t xdata g_rw_srw_0005h_lut_wrdt;                                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 0005h
extern volatile uint8_t xdata g_rw_srw_0006h_lut_wrdt;                                                   // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 0006h
extern volatile uint8_t xdata g_rw_srw_0007h_lut_wrdt;                                                   // [msb:lsb] = [23:16], val = 0, 	Absolute Address = 0007h
extern volatile uint8_t xdata g_rw_srw_0008h_lut_wrdt;                                                   // [msb:lsb] = [31:24], val = 0, 	Absolute Address = 0008h
extern volatile uint8_t xdata g_rw_srw_0009h_lut_rddt_lth;                                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 0009h

extern volatile uint8_t xdata g_rw_srw_000Ah_lut_rddt_lth;                                               // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 000Ah
extern volatile uint8_t xdata g_rw_srw_000Bh_lut_rddt_lth;                                               // [msb:lsb] = [23:16], val = 0, 	Absolute Address = 000Bh
extern volatile uint8_t xdata g_rw_srw_000Ch_lut_rddt_lth;                                               // [msb:lsb] = [31:24], val = 0, 	Absolute Address = 000Ch

#endif