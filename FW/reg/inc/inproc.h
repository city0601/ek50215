#ifndef _INPROC_H_
#define _INPROC_H_
#include "reg_include.h"

union rw_inproc_0000h
{
    unsigned char byte;
    struct
    {
        unsigned char r_sellvds_h                                                            : 2;        // [msb:lsb] = [1:0], val = 2
        unsigned char r_sellvds_l                                                            : 2;        // [msb:lsb] = [1:0], val = 1
        unsigned char r_in_ch_swap                                                           : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_lvds_bit_width                                                       : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_inproc_0000h xdata g_rw_inproc_0000h;    // Absolute Address = 3000h

union rw_inproc_0001h
{
    unsigned char byte;
    struct
    {
        unsigned char r_in_port_ctrl                                                         : 4;        // [msb:lsb] = [3:0], val = 3
        unsigned char r_in_port_num                                                          : 1;        // val = 1
        unsigned char r_in_port_sel                                                          : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_inproc_0001h xdata g_rw_inproc_0001h;    // Absolute Address = 3001h

union rw_inproc_0002h
{
    unsigned char byte;
    struct
    {
        unsigned char r_6bit_type2_en                                                        : 1;        // val = 0
        unsigned char r_in_deskew_dis                                                        : 1;        // val = 0
        unsigned char r_vsync_inv                                                            : 1;        // val = 0
        unsigned char r_hsync_inv                                                            : 1;        // val = 0
        unsigned char r_lvds_debug_pair_sel                                                  : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_lvds_debug_port_sel_bits_0                                           : 1;        // val = 0
    }bits;
};
extern volatile union rw_inproc_0002h xdata g_rw_inproc_0002h;    // Absolute Address = 3002h

union rw_inproc_0003h
{
    unsigned char byte;
    struct
    {
        unsigned char r_lvds_debug_port_sel_bits_1                                           : 1;        // val = 0
        unsigned char r_de_pulse_min                                                         : 3;        // [msb:lsb] = [2:0], val = 4
        unsigned char r_hs_pulse_min                                                         : 3;        // [msb:lsb] = [2:0], val = 4
        unsigned char r_vs_pulse_min_bits_0                                                  : 1;        // val = 0
    }bits;
};
extern volatile union rw_inproc_0003h xdata g_rw_inproc_0003h;    // Absolute Address = 3003h

union rw_inproc_0004h
{
    unsigned char byte;
    struct
    {
        unsigned char r_vs_pulse_min_bits_2_1                                                : 2;        // [msb:lsb] = [2:1], val = 2
        unsigned char r_lvclk_freq_min_bits_5_0                                              : 6;        // [msb:lsb] = [5:0], val = 22
    }bits;
};
extern volatile union rw_inproc_0004h xdata g_rw_inproc_0004h;    // Absolute Address = 3004h

//------------------------------------------------------------------------------------------------------------------------
union rw_inproc_0005h
{
    unsigned char byte;
    struct
    {
        unsigned char r_lvclk_freq_min_bits_8_6                                              : 3;        // [msb:lsb] = [8:6], val = 2
        unsigned char r_lvclk_freq_max_bits_4_0                                              : 5;        // [msb:lsb] = [4:0], val = 12
    }bits;
};
extern volatile union rw_inproc_0005h xdata g_rw_inproc_0005h;    // Absolute Address = 3005h

union rw_inproc_0006h
{
    unsigned char byte;
    struct
    {
        unsigned char r_lvclk_freq_max_bits_10_5                                             : 6;        // [msb:lsb] = [10:5], val = 14
        unsigned char r_hfreq_det_en                                                         : 1;        // val = 0
        unsigned char r_lfreq_det_en                                                         : 1;        // val = 0
    }bits;
};
extern volatile union rw_inproc_0006h xdata g_rw_inproc_0006h;    // Absolute Address = 3006h

union rw_inproc_0007h
{
    unsigned char byte;
    struct
    {
        unsigned char r_clk_stop_det_en                                                      : 1;        // val = 1
        unsigned char r_sys_destop_en                                                        : 1;        // val = 1
        unsigned char r_window_period_bits_5_0                                               : 6;        // [msb:lsb] = [5:0], val = 63
    }bits;
};
extern volatile union rw_inproc_0007h xdata g_rw_inproc_0007h;    // Absolute Address = 3007h

union rw_inproc_0008h
{
    unsigned char byte;
    struct
    {
        unsigned char r_window_period_bits_10_6                                              : 5;        // [msb:lsb] = [10:6], val = 3
        unsigned char r_hfreq_deb_period_bits_2_0                                            : 3;        // [msb:lsb] = [2:0], val = 2
    }bits;
};
extern volatile union rw_inproc_0008h xdata g_rw_inproc_0008h;    // Absolute Address = 3008h

union rw_inproc_0009h
{
    unsigned char byte;
    struct
    {
        unsigned char r_hfreq_deb_period_bits_7_3                                            : 5;        // [msb:lsb] = [7:3], val = 1
        unsigned char r_lfreq_deb_period_bits_2_0                                            : 3;        // [msb:lsb] = [2:0], val = 2
    }bits;
};
extern volatile union rw_inproc_0009h xdata g_rw_inproc_0009h;    // Absolute Address = 3009h

//------------------------------------------------------------------------------------------------------------------------
union rw_inproc_000Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_lfreq_deb_period_bits_7_3                                            : 5;        // [msb:lsb] = [7:3], val = 1
        unsigned char r_stop_deb_period_bits_2_0                                             : 3;        // [msb:lsb] = [2:0], val = 2
    }bits;
};
extern volatile union rw_inproc_000Ah xdata g_rw_inproc_000Ah;    // Absolute Address = 300Ah

union rw_inproc_000Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_stop_deb_period_bits_7_3                                             : 5;        // [msb:lsb] = [7:3], val = 1
        unsigned char r_pstate_en                                                            : 1;        // val = 1
        unsigned char r_pstate_detect_region_bits_1_0                                        : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_inproc_000Bh xdata g_rw_inproc_000Bh;    // Absolute Address = 300Bh

union rw_inproc_000Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_pstate_detect_region_bits_15_10                                      : 6;        // [msb:lsb] = [15:10], val = 1
        unsigned char r_freq_det_en                                                          : 1;        // val = 0
        unsigned char r_always_det                                                           : 1;        // val = 0
    }bits;
};
extern volatile union rw_inproc_000Dh xdata g_rw_inproc_000Dh;    // Absolute Address = 300Dh

union rw_inproc_000Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_clk_det_thr                                                          : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_rx_unlock_det_en                                                     : 1;        // val = 0
        unsigned char r_hdisp_pg_bits_0                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_inproc_000Eh xdata g_rw_inproc_000Eh;    // Absolute Address = 300Eh

union rw_inproc_0010h
{
    unsigned char byte;
    struct
    {
        unsigned char r_hdisp_pg_bits_11_9                                                   : 3;        // [msb:lsb] = [11:9], val = 2
        unsigned char r_vdisp_pg_bits_4_0                                                    : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_inproc_0010h xdata g_rw_inproc_0010h;    // Absolute Address = 3010h

//------------------------------------------------------------------------------------------------------------------------
union rw_inproc_0013h
{
    unsigned char byte;
    struct
    {
        unsigned char r_disp_htot_0_bits_11_8                                                : 4;        // [msb:lsb] = [11:8], val = 4
        unsigned char r_disp_vtot_0_bits_3_0                                                 : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_inproc_0013h xdata g_rw_inproc_0013h;    // Absolute Address = 3013h

union rw_inproc_0015h
{
    unsigned char byte;
    struct
    {
        unsigned char r_disp_vtot_0_bits_12                                                  : 1;        // val = 0
        unsigned char r_disp_htot_1_bits_6_0                                                 : 7;        // [msb:lsb] = [6:0], val = 48
    }bits;
};
extern volatile union rw_inproc_0015h xdata g_rw_inproc_0015h;    // Absolute Address = 3015h

union rw_inproc_0016h
{
    unsigned char byte;
    struct
    {
        unsigned char r_disp_htot_1_bits_11_7                                                : 5;        // [msb:lsb] = [11:7], val = 9
        unsigned char r_disp_vtot_1_bits_2_0                                                 : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_inproc_0016h xdata g_rw_inproc_0016h;    // Absolute Address = 3016h

union rw_inproc_0018h
{
    unsigned char byte;
    struct
    {
        unsigned char r_disp_vtot_1_bits_12_11                                               : 2;        // [msb:lsb] = [12:11], val = 0
        unsigned char r_disp_vtot_tp_bits_5_0                                                : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_inproc_0018h xdata g_rw_inproc_0018h;    // Absolute Address = 3018h

union rw_inproc_0019h
{
    unsigned char byte;
    struct
    {
        unsigned char r_disp_vtot_tp_bits_12_6                                               : 7;        // [msb:lsb] = [12:6], val = 25
        unsigned char r_disp_vtot_bus_en_bits_0                                              : 1;        // val = 0
    }bits;
};
extern volatile union rw_inproc_0019h xdata g_rw_inproc_0019h;    // Absolute Address = 3019h

//------------------------------------------------------------------------------------------------------------------------
union rw_inproc_001Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_disp_vtot_bus_en_bits_12_9                                           : 4;        // [msb:lsb] = [12:9], val = 3
        unsigned char r_aging_eni_pr                                                         : 1;        // val = 0
        unsigned char r_aging_eni_r                                                          : 1;        // val = 0
        unsigned char r_mode2_pr                                                             : 1;        // val = 0
        unsigned char r_mode2_r                                                              : 1;        // val = 0
    }bits;
};
extern volatile union rw_inproc_001Bh xdata g_rw_inproc_001Bh;    // Absolute Address = 301Bh

union rw_inproc_001Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_bus_eni_pr                                                           : 1;        // val = 0
        unsigned char r_bus_eni_r                                                            : 1;        // val = 0
        unsigned char r_fcnt_rst_sel                                                         : 1;        // val = 0
        unsigned char r_fpat_en                                                              : 1;        // val = 1
        unsigned char r_testpatern_select                                                    : 1;        // val = 0
        unsigned char r_aging_8b_en                                                          : 1;        // val = 0
        unsigned char r_fast_agmode_en                                                       : 1;        // val = 0
        unsigned char r_init_frun_bits_0                                                     : 1;        // val = 1
    }bits;
};
extern volatile union rw_inproc_001Ch xdata g_rw_inproc_001Ch;    // Absolute Address = 301Ch

union rw_inproc_001Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_init_frun_bits_7_1                                                   : 7;        // [msb:lsb] = [7:1], val = 1
        unsigned char r_ext_8pix                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_inproc_001Dh xdata g_rw_inproc_001Dh;    // Absolute Address = 301Dh

union rw_inproc_001Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_boe_corestate                                                        : 1;        // val = 0
        unsigned char r_boe_corestate_aging                                                  : 1;        // val = 0
        unsigned char r_boe_corestate_flicker                                                : 1;        // val = 0
        unsigned char r_boe_corestate_vsync                                                  : 1;        // val = 0
        unsigned char r_boe_corestate_testmode                                               : 1;        // val = 0
        unsigned char r_aging_pe_to_frun                                                     : 1;        // val = 1
        unsigned char r_bright_wide_5120_sel                                                 : 1;        // val = 1
        unsigned char r_aging_fhd_disp                                                       : 1;        // val = 1
    }bits;
};
extern volatile union rw_inproc_001Eh xdata g_rw_inproc_001Eh;    // Absolute Address = 301Eh

union rw_inproc_001Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_aging_hd_disp                                                        : 1;        // val = 0
        unsigned char r_hdisp_bits_6_0                                                       : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_inproc_001Fh xdata g_rw_inproc_001Fh;    // Absolute Address = 301Fh

//------------------------------------------------------------------------------------------------------------------------
union rw_inproc_0020h
{
    unsigned char byte;
    struct
    {
        unsigned char r_hdisp_bits_11_7                                                      : 5;        // [msb:lsb] = [11:7], val = 10
        unsigned char r_htotal_min_lvclk_bits_2_0                                            : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_inproc_0020h xdata g_rw_inproc_0020h;    // Absolute Address = 3020h

union rw_inproc_0022h
{
    unsigned char byte;
    struct
    {
        unsigned char r_htotal_min_lvclk_bits_12_11                                          : 2;        // [msb:lsb] = [12:11], val = 0
        unsigned char r_htotal_max_lvclk_bits_5_0                                            : 6;        // [msb:lsb] = [5:0], val = 24
    }bits;
};
extern volatile union rw_inproc_0022h xdata g_rw_inproc_0022h;    // Absolute Address = 3022h

union rw_inproc_0023h
{
    unsigned char byte;
    struct
    {
        unsigned char r_htotal_max_lvclk_bits_12_6                                           : 7;        // [msb:lsb] = [12:6], val = 109
        unsigned char r_vdisp_bits_0                                                         : 1;        // val = 0
    }bits;
};
extern volatile union rw_inproc_0023h xdata g_rw_inproc_0023h;    // Absolute Address = 3023h

union rw_inproc_0025h
{
    unsigned char byte;
    struct
    {
        unsigned char r_vdisp_bits_12_9                                                      : 4;        // [msb:lsb] = [12:9], val = 2
        unsigned char r_vtotal_min_bits_3_0                                                  : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_inproc_0025h xdata g_rw_inproc_0025h;    // Absolute Address = 3025h

union rw_inproc_0027h
{
    unsigned char byte;
    struct
    {
        unsigned char r_vtotal_min_bits_12                                                   : 1;        // val = 0
        unsigned char r_vtotal_max_bits_6_0                                                  : 7;        // [msb:lsb] = [6:0], val = 64
    }bits;
};
extern volatile union rw_inproc_0027h xdata g_rw_inproc_0027h;    // Absolute Address = 3027h

//------------------------------------------------------------------------------------------------------------------------
union rw_inproc_0029h
{
    unsigned char byte;
    struct
    {
        unsigned char r_vtotal_max_bits_15                                                   : 1;        // val = 0
        unsigned char r_ext_vsync_sel                                                        : 1;        // val = 0
        unsigned char r_ext_hsync_sel                                                        : 1;        // val = 0
        unsigned char r_rst_pix_cnt_en                                                       : 1;        // val = 1
        unsigned char r_vsync_pos_bits_3_0                                                   : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_inproc_0029h xdata g_rw_inproc_0029h;    // Absolute Address = 3029h

union rw_inproc_002Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_vsync_pos_bits_7_4                                                   : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char r_vlimit_en                                                            : 1;        // val = 0
        unsigned char r_ex_vsync_refine                                                      : 1;        // val = 0
        unsigned char r_capture_keep                                                         : 1;        // val = 0
        unsigned char r_hsync_front_porch_bits_0                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_inproc_002Ah xdata g_rw_inproc_002Ah;    // Absolute Address = 302Ah

union rw_inproc_002Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_hsync_front_porch_bits_7_1                                           : 7;        // [msb:lsb] = [7:1], val = 15
        unsigned char r_hsync_trig_rd_en                                                     : 1;        // val = 0
    }bits;
};
extern volatile union rw_inproc_002Bh xdata g_rw_inproc_002Bh;    // Absolute Address = 302Bh

union rw_inproc_002Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_hsync_width_setting                                                  : 5;        // [msb:lsb] = [4:0], val = 10
        unsigned char r_short_de_block                                                       : 1;        // val = 0
        unsigned char r_hdisp_sft                                                            : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_inproc_002Ch xdata g_rw_inproc_002Ch;    // Absolute Address = 302Ch

union rw_inproc_002Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_line_cnt_no_large_vdisp_rst                                          : 1;        // val = 0
        unsigned char r_front_end_abnor_en                                                   : 1;        // val = 0
        unsigned char r_front_end_hact_min_bits_5_0                                          : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_inproc_002Dh xdata g_rw_inproc_002Dh;    // Absolute Address = 302Dh

//------------------------------------------------------------------------------------------------------------------------
union rw_inproc_002Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_front_end_hact_min_bits_11_6                                         : 6;        // [msb:lsb] = [11:6], val = 0
        unsigned char r_front_end_vact_min_bits_1_0                                          : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_inproc_002Eh xdata g_rw_inproc_002Eh;    // Absolute Address = 302Eh

union rw_inproc_0030h
{
    unsigned char byte;
    struct
    {
        unsigned char r_front_end_vact_min_bits_11_10                                        : 2;        // [msb:lsb] = [11:10], val = 0
        unsigned char r_rd_hdisp_bits_5_0                                                    : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_inproc_0030h xdata g_rw_inproc_0030h;    // Absolute Address = 3030h

union rw_inproc_0031h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rd_hdisp_bits_11_6                                                   : 6;        // [msb:lsb] = [11:6], val = 40
        unsigned char r_reg_hs_sel                                                           : 1;        // val = 0
        unsigned char r_reg_hs_dly_no_bits_0                                                 : 1;        // val = 0
    }bits;
};
extern volatile union rw_inproc_0031h xdata g_rw_inproc_0031h;    // Absolute Address = 3031h

union rw_inproc_0032h
{
    unsigned char byte;
    struct
    {
        unsigned char r_reg_hs_dly_no_bits_7_1                                               : 7;        // [msb:lsb] = [7:1], val = 0
        unsigned char r_reg_hs_width_bits_0                                                  : 1;        // val = 1
    }bits;
};
extern volatile union rw_inproc_0032h xdata g_rw_inproc_0032h;    // Absolute Address = 3032h

union rw_inproc_0033h
{
    unsigned char byte;
    struct
    {
        unsigned char r_reg_hs_width_bits_7_1                                                : 7;        // [msb:lsb] = [7:1], val = 2
        unsigned char r_port_num_bits_0                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_inproc_0033h xdata g_rw_inproc_0033h;    // Absolute Address = 3033h

//------------------------------------------------------------------------------------------------------------------------
union rw_inproc_0034h
{
    unsigned char byte;
    struct
    {
        unsigned char r_port_num_bits_1                                                      : 1;        // val = 0
        unsigned char r_frm_go_aging                                                         : 1;        // val = 0
        unsigned char r_frm_go_aging_cnt_bits_5_0                                            : 6;        // [msb:lsb] = [5:0], val = 15
    }bits;
};
extern volatile union rw_inproc_0034h xdata g_rw_inproc_0034h;    // Absolute Address = 3034h

union rw_inproc_0035h
{
    unsigned char byte;
    struct
    {
        unsigned char r_frm_go_aging_cnt_bits_6                                              : 1;        // val = 0
        unsigned char r_frun_cnt_start_sel                                                   : 1;        // val = 0
        unsigned char r_skip_1frame_nor_en                                                   : 1;        // val = 1
        unsigned char r_prbs_err_clr                                                         : 1;        // val = 1
        unsigned char r_prbs_cnt_ind_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_inproc_0035h xdata g_rw_inproc_0035h;    // Absolute Address = 3035h

union rw_inproc_0036h
{
    unsigned char byte;
    struct
    {
        unsigned char r_prbs_cnt_ind_bits_7_4                                                : 4;        // [msb:lsb] = [7:4], val = 7
        unsigned char r_lvds_bit_width_pr                                                    : 1;        // val = 0
        unsigned char r_inproc_rd_dbg_sel                                                    : 1;        // val = 0
        unsigned char r_bpc_sel                                                              : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_inproc_0036h xdata g_rw_inproc_0036h;    // Absolute Address = 3036h

union rw_inproc_0037h
{
    unsigned char byte;
    struct
    {
        unsigned char r_cap_frm_cnt                                                          : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_cap_line_cnt_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_inproc_0037h xdata g_rw_inproc_0037h;    // Absolute Address = 3037h

union rw_inproc_003Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_cap_pixel_cnt_bits_11_8                                              : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_htot_rec_sel                                                         : 1;        // val = 0
        unsigned char r_div_htot_rec_sel                                                     : 1;        // val = 0
        unsigned char r_htot_cmp_reg_min_bits_1_0                                            : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_inproc_003Ah xdata g_rw_inproc_003Ah;    // Absolute Address = 303Ah

//------------------------------------------------------------------------------------------------------------------------
union rw_inproc_003Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_htot_cmp_reg_min_bits_14_10                                          : 5;        // [msb:lsb] = [14:10], val = 0
        unsigned char r_htot_rec_bits_2_0                                                    : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_inproc_003Ch xdata g_rw_inproc_003Ch;    // Absolute Address = 303Ch

union rw_inproc_003Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_htot_rec_bits_14_11                                                  : 4;        // [msb:lsb] = [14:11], val = 0
        unsigned char r_htot_vair_bits_3_0                                                   : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_inproc_003Eh xdata g_rw_inproc_003Eh;    // Absolute Address = 303Eh

union rw_inproc_003Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_htot_vair_bits_7_4                                                   : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char r_capture_sel                                                          : 1;        // val = 0
        unsigned char r_no_hv_recover                                                        : 1;        // val = 0
        unsigned char r_lvclk_freq_min                                                       : 1;        // val = 0
        unsigned char r_lvclk_freq_max                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_inproc_003Fh xdata g_rw_inproc_003Fh;    // Absolute Address = 303Fh

union rw_inproc_0041h
{
    unsigned char byte;
    struct
    {
        unsigned char r_agingeni_bist_sel                                                    : 1;        // val = 0
        unsigned char r_esd_mask_corestate_en                                                : 1;        // val = 0
        unsigned char r_abn_htot_div2                                                        : 1;        // val = 0
        unsigned char r_de_long_stop                                                         : 1;        // val = 0
        unsigned char r_esd_det_period                                                       : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_front_end_abnor_en_sel                                               : 1;        // val = 0
        unsigned char r_esd_mask_sel                                                         : 1;        // val = 0
    }bits;
};
extern volatile union rw_inproc_0041h xdata g_rw_inproc_0041h;    // Absolute Address = 3041h


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_inproc_000Ch_pstate_detect_region;                                    // [msb:lsb] = [9:2], val = 119, 	Absolute Address = 300Ch
extern volatile uint8_t xdata g_rw_inproc_000Fh_hdisp_pg;                                                // [msb:lsb] = [8:1], val = 128, 	Absolute Address = 300Fh
extern volatile uint8_t xdata g_rw_inproc_0011h_vdisp_pg;                                                // [msb:lsb] = [12:5], val = 45, 	Absolute Address = 3011h
extern volatile uint8_t xdata g_rw_inproc_0012h_disp_htot_0;                                             // [msb:lsb] = [7:0], val = 176, 	Absolute Address = 3012h
extern volatile uint8_t xdata g_rw_inproc_0014h_disp_vtot_0;                                             // [msb:lsb] = [11:4], val = 100, 	Absolute Address = 3014h

extern volatile uint8_t xdata g_rw_inproc_0017h_disp_vtot_1;                                             // [msb:lsb] = [10:3], val = 200, 	Absolute Address = 3017h
extern volatile uint8_t xdata g_rw_inproc_001Ah_disp_vtot_bus_en;                                        // [msb:lsb] = [8:1], val = 32, 	Absolute Address = 301Ah
extern volatile uint8_t xdata g_rw_inproc_0021h_htotal_min_lvclk;                                        // [msb:lsb] = [10:3], val = 162, 	Absolute Address = 3021h
extern volatile uint8_t xdata g_rw_inproc_0024h_vdisp;                                                   // [msb:lsb] = [8:1], val = 208, 	Absolute Address = 3024h
extern volatile uint8_t xdata g_rw_inproc_0026h_vtotal_min;                                              // [msb:lsb] = [11:4], val = 90, 	Absolute Address = 3026h

extern volatile uint8_t xdata g_rw_inproc_0028h_vtotal_max;                                              // [msb:lsb] = [14:7], val = 31, 	Absolute Address = 3028h
extern volatile uint8_t xdata g_rw_inproc_002Fh_front_end_vact_min;                                      // [msb:lsb] = [9:2], val = 0, 	Absolute Address = 302Fh
extern volatile uint8_t xdata g_rw_inproc_0038h_cap_line_cnt;                                            // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 3038h
extern volatile uint8_t xdata g_rw_inproc_0039h_cap_pixel_cnt;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 3039h
extern volatile uint8_t xdata g_rw_inproc_003Bh_htot_cmp_reg_min;                                        // [msb:lsb] = [9:2], val = 0, 	Absolute Address = 303Bh

extern volatile uint8_t xdata g_rw_inproc_003Dh_htot_rec;                                                // [msb:lsb] = [10:3], val = 0, 	Absolute Address = 303Dh
extern volatile uint8_t xdata g_rw_inproc_0040h_hs_pin_width;                                            // [msb:lsb] = [7:0], val = 32, 	Absolute Address = 3040h
extern volatile uint8_t xdata g_rw_inproc_0042h_frun_dly;                                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 3042h

#endif