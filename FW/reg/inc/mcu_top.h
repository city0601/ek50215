#ifndef _MCU_TOP_H_
#define _MCU_TOP_H_
#include "reg_include.h"

union rw_mcu_top_0000h
{
    unsigned char byte;
    struct
    {
        unsigned char r_slv_dev                                                              : 7;        // [msb:lsb] = [6:0], val = 90
        unsigned char r_slv_grm                                                              : 1;        // val = 1
    }bits;
};
extern volatile union rw_mcu_top_0000h xdata g_rw_mcu_top_0000h;    // Absolute Address = 2000h

union rw_mcu_top_0001h
{
    unsigned char byte;
    struct
    {
        unsigned char r_ext_dev                                                              : 7;        // [msb:lsb] = [6:0], val = 0
        unsigned char r_mtr_grm                                                              : 1;        // val = 1
    }bits;
};
extern volatile union rw_mcu_top_0001h xdata g_rw_mcu_top_0001h;    // Absolute Address = 2001h

union rw_mcu_top_0002h
{
    unsigned char byte;
    struct
    {
        unsigned char r_vcom_delay_sel                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_i2c_wr_dly_tm                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_mcu_i2c_mst_clk                                                      : 3;        // [msb:lsb] = [2:0], val = 1
    }bits;
};
extern volatile union rw_mcu_top_0002h xdata g_rw_mcu_top_0002h;    // Absolute Address = 2002h

union rw_mcu_top_0003h
{
    unsigned char byte;
    struct
    {
        unsigned char r_i2cs_rdata_sel                                                       : 1;        // val = 0
        unsigned char mcu_i2c_ctrl                                                           : 1;        // val = 0
        unsigned char fw_i2c_access_vcom                                                     : 1;        // val = 0
        unsigned char vcm_i2cen                                                              : 1;        // val = 0
        unsigned char through_mode                                                           : 1;        // val = 0
        unsigned char r_i2c_mon_byte_mode                                                    : 1;        // val = 1
        unsigned char reserved1                                                              : 1;        // val = 1
        unsigned char reserved2                                                              : 1;        // val = 0
    }bits;
};
extern volatile union rw_mcu_top_0003h xdata g_rw_mcu_top_0003h;    // Absolute Address = 2003h

union rw_mcu_top_0004h
{
    unsigned char byte;
    struct
    {
        unsigned char r_clear                                                                : 1;        // val = 0
        unsigned char r_wp_n                                                                 : 1;        // val = 0
        unsigned char r_hold_n                                                               : 1;        // val = 0
        unsigned char r_sck_inv                                                              : 1;        // val = 0
        unsigned char r_si_lth_inv                                                           : 1;        // val = 0
        unsigned char r_pgm_t_set_ppg                                                        : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_mcu_top_0004h xdata g_rw_mcu_top_0004h;    // Absolute Address = 2004h

//------------------------------------------------------------------------------------------------------------------------
union rw_mcu_top_0006h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pgm_t_set_wr                                                         : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char r_nack_off                                                             : 1;        // val = 0
        unsigned char r_ready_det2_en                                                        : 1;        // val = 0
        unsigned char r_ready_det1_en                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_mcu_top_0006h xdata g_rw_mcu_top_0006h;    // Absolute Address = 2006h

union rw_mcu_top_0007h
{
    unsigned char byte;
    struct
    {
        unsigned char r_ready_mode                                                           : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_init_ready_sel                                                       : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_csot_crc_mcu                                                          : 1;       // 
        unsigned char reserved3                                                              : 3;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_mcu_top_0007h xdata g_rw_mcu_top_0007h;    // Absolute Address = 2007h

union rw_mcu_top_0009h
{
    unsigned char byte;
    struct
    {
        unsigned char r_exedid_dev_addr                                                      : 7;        // [msb:lsb] = [6:0], val = 0
        unsigned char reserved4                                                              : 1;        // val = 0
    }bits;
};
extern volatile union rw_mcu_top_0009h xdata g_rw_mcu_top_0009h;    // Absolute Address = 2009h

union rw_mcu_top_000Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_pmic_dev_adr                                                         : 7;        // [msb:lsb] = [6:0], val = 70
        unsigned char r_pmic_bus                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_mcu_top_000Ah xdata g_rw_mcu_top_000Ah;    // Absolute Address = 200Ah

union rw_mcu_top_0011h
{
    unsigned char byte;
    struct
    {
        unsigned char r_crc_en                                                               : 1;        // val = 0
        unsigned char r_hdr_dl_en                                                            : 1;        // val = 0
        unsigned char r_pmic_fun_en                                                          : 1;        // val = 0
        unsigned char r_pmic_gma_en                                                          : 1;        // val = 1
        unsigned char r_pmic_type                                                            : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_mcu_top_0011h xdata g_rw_mcu_top_0011h;    // Absolute Address = 2011h

//------------------------------------------------------------------------------------------------------------------------
union rw_mcu_top_0013h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pmic_i2c_dly_rim                                                     : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char reserved5                                                              : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_mcu_top_0013h xdata g_rw_mcu_top_0013h;    // Absolute Address = 2013h

union rw_mcu_top_0020h
{
    unsigned char byte;
    struct
    {
        unsigned char r_c_board_spi_freq_sel                                                 : 4;        // [msb:lsb] = [3:0], val = 1
        unsigned char r_x_board_spi_freq_sel                                                 : 4;        // [msb:lsb] = [3:0], val = 2
    }bits;
};
extern volatile union rw_mcu_top_0020h xdata g_rw_mcu_top_0020h;    // Absolute Address = 2020h

union rw_mcu_top_0027h
{
    unsigned char byte;
    struct
    {
        unsigned char ioa_i2c_path_ctrl                                                      : 1;        // val = 0
        unsigned char r_pmic_i2c_path                                                        : 1;        // val = 0
        unsigned char r_mcu_idle_en                                                          : 1;        // val = 1
        unsigned char r_mcu_spi_clk                                                          : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_auto_cal_flash_size_en                                               : 1;        // val = 0
        unsigned char reserved6                                                              : 1;        // val = 0
    }bits;
};
extern volatile union rw_mcu_top_0027h xdata g_rw_mcu_top_0027h;    // Absolute Address = 2027h

union rw_mcu_top_0035h
{
    unsigned char byte;
    struct
    {
        unsigned char r_init_restart                                                         : 1;        // val = 0
        unsigned char reserved7                                                              : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_mcu_top_0035h xdata g_rw_mcu_top_0035h;    // Absolute Address = 2035h

union rw_mcu_top_0036h
{
    unsigned char byte;
    struct
    {
        unsigned char i2c_slave_fsm_st                                                       : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_hdfailbypass                                                         : 1;        // val = 0
        unsigned char r_datafailbypass                                                       : 1;        // val = 0
        unsigned char restart_dl_flag                                                        : 1;        // val = 0
        unsigned char reserved8                                                              : 1;        // val = 0
    }bits;
};
extern volatile union rw_mcu_top_0036h xdata g_rw_mcu_top_0036h;    // Absolute Address = 2036h

//------------------------------------------------------------------------------------------------------------------------
union rw_mcu_top_0037h
{
    unsigned char byte;
    struct
    {
        unsigned char r_lut_srw_dev                                                          : 7;        // [msb:lsb] = [6:0], val = 97
        unsigned char r_csot_pmic_reg_mode                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_mcu_top_0037h xdata g_rw_mcu_top_0037h;    // Absolute Address = 2037h


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_mcu_top_0005h_pgm_t_unit;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2005h
extern volatile uint8_t xdata g_rw_mcu_top_0008h_init_ready_cnt;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2008h
extern volatile uint8_t xdata g_rw_mcu_top_000Bh_pmic_banka_start_adr;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 200Bh
extern volatile uint8_t xdata g_rw_mcu_top_000Ch_pmic_bankb_start_adr;                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 200Ch
extern volatile uint8_t xdata g_rw_mcu_top_000Dh_pmic_banka_len;                                         // [msb:lsb] = [7:0], val = 48, 	Absolute Address = 200Dh

extern volatile uint8_t xdata g_rw_mcu_top_000Eh_pmic_bankb_len;                                         // [msb:lsb] = [7:0], val = 48, 	Absolute Address = 200Eh
extern volatile uint8_t xdata g_rw_mcu_top_000Fh_pmic_header_len;                                        // [msb:lsb] = [7:0], val = 42, 	Absolute Address = 200Fh
extern volatile uint8_t xdata g_rw_mcu_top_0010h_pmic_gma_len;                                           // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 2010h
extern volatile uint8_t xdata g_rw_mcu_top_0012h_pmic_status_adr;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2012h
extern volatile uint8_t xdata g_rw_mcu_top_0014h_x_board_pmic_hdr_adr1;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2014h

extern volatile uint8_t xdata g_rw_mcu_top_0015h_x_board_pmic_hdr_adr2;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2015h
extern volatile uint8_t xdata g_rw_mcu_top_0016h_x_board_pmic_hdr_adr3;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2016h
extern volatile uint8_t xdata g_rw_mcu_top_0017h_x_board_pmic_gma_adr1;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2017h
extern volatile uint8_t xdata g_rw_mcu_top_0018h_x_board_pmic_gma_adr2;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2018h
extern volatile uint8_t xdata g_rw_mcu_top_0019h_x_board_pmic_gma_adr3;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2019h

extern volatile uint8_t xdata g_rw_mcu_top_001Ah_x_board_pmic_bankA_adr1;                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 201Ah
extern volatile uint8_t xdata g_rw_mcu_top_001Bh_x_board_pmic_bankA_adr2;                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 201Bh
extern volatile uint8_t xdata g_rw_mcu_top_001Ch_x_board_pmic_bankA_adr3;                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 201Ch
extern volatile uint8_t xdata g_rw_mcu_top_001Dh_x_board_pmic_bankB_adr1;                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 201Dh
extern volatile uint8_t xdata g_rw_mcu_top_001Eh_x_board_pmic_bankB_adr2;                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 201Eh

extern volatile uint8_t xdata g_rw_mcu_top_001Fh_x_board_pmic_bankB_adr3;                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 201Fh
extern volatile uint8_t xdata g_rw_mcu_top_0021h_x_board_DGC_adr1;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2021h
extern volatile uint8_t xdata g_rw_mcu_top_0022h_x_board_DGC_adr2;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2022h
extern volatile uint8_t xdata g_rw_mcu_top_0023h_x_board_DGC_adr3;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2023h
extern volatile uint8_t xdata g_rw_mcu_top_0024h_x_board_VCOM_adr1;                                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2024h

extern volatile uint8_t xdata g_rw_mcu_top_0025h_x_board_VCOM_adr2;                                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2025h
extern volatile uint8_t xdata g_rw_mcu_top_0026h_x_board_VCOM_adr3;                                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2026h
extern volatile uint8_t xdata g_rw_mcu_top_0028h_i2c_over_fw_addr1;                                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2028h
extern volatile uint8_t xdata g_rw_mcu_top_0029h_i2c_over_fw_addr2;                                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2029h
extern volatile uint8_t xdata g_rw_mcu_top_002Ah_i2c_target_num;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 202Ah

extern volatile uint8_t xdata g_rw_mcu_top_002Bh_i2c_over_fw_dev;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 202Bh
extern volatile uint8_t xdata g_rw_mcu_top_002Ch_i2c_over_data1;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 202Ch
extern volatile uint8_t xdata g_rw_mcu_top_002Dh_i2c_over_data2;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 202Dh
extern volatile uint8_t xdata g_rw_mcu_top_002Eh_i2c_over_data3;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 202Eh
extern volatile uint8_t xdata g_rw_mcu_top_002Fh_i2c_over_data4;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 202Fh

extern volatile uint8_t xdata g_rw_mcu_top_0030h_i2c_over_data5;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2030h
extern volatile uint8_t xdata g_rw_mcu_top_0031h_i2c_over_data6;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2031h
extern volatile uint8_t xdata g_rw_mcu_top_0032h_i2c_over_data7;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2032h
extern volatile uint8_t xdata g_rw_mcu_top_0033h_i2c_over_data8;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2033h
extern volatile uint8_t xdata g_rw_mcu_top_0034h_i2c_over_cnt;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2034h

extern volatile uint8_t xdata g_rw_mcu_top_0038h_vendor_id;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2038h
extern volatile uint8_t xdata g_rw_mcu_top_0039h_tcon_id1;                                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2039h
extern volatile uint8_t xdata g_rw_mcu_top_003Ah_tcon_id2;                                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 203Ah
extern volatile uint8_t xdata g_rw_mcu_top_003Bh_mid;                                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 203Bh
extern volatile uint8_t xdata g_rw_mcu_top_003Ch_did1;                                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 203Ch

extern volatile uint8_t xdata g_rw_mcu_top_003Dh_did2;                                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 203Dh

#endif