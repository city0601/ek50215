#ifndef _PDF_H_
#define _PDF_H_
#include "reg_include.h"

union rw_pdf_0000h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp00_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp00_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp00_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp00_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0000h xdata g_rw_pdf_0000h;    // Absolute Address = 4000h

union rw_pdf_0003h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp00_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp00_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0003h xdata g_rw_pdf_0003h;    // Absolute Address = 4003h

union rw_pdf_0004h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp00_mask_array_sel_bits_4                                           : 1;        // val = 0
        unsigned char r_gp00_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp00_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0004h xdata g_rw_pdf_0004h;    // Absolute Address = 4004h

union rw_pdf_0005h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp01_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp01_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp01_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp01_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0005h xdata g_rw_pdf_0005h;    // Absolute Address = 4005h

union rw_pdf_0008h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp01_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp01_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_pdf_0008h xdata g_rw_pdf_0008h;    // Absolute Address = 4008h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0009h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp01_mask_array_sel_bits_4                                           : 1;        // val = 0
        unsigned char r_gp01_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp01_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0009h xdata g_rw_pdf_0009h;    // Absolute Address = 4009h

union rw_pdf_000Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp02_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp02_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp02_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp02_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_000Ah xdata g_rw_pdf_000Ah;    // Absolute Address = 400Ah

union rw_pdf_000Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp02_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp02_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 2
    }bits;
};
extern volatile union rw_pdf_000Dh xdata g_rw_pdf_000Dh;    // Absolute Address = 400Dh

union rw_pdf_000Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp02_mask_array_sel_bits_4                                           : 1;        // val = 0
        unsigned char r_gp02_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp02_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_000Eh xdata g_rw_pdf_000Eh;    // Absolute Address = 400Eh

union rw_pdf_000Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp03_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp03_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp03_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp03_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_000Fh xdata g_rw_pdf_000Fh;    // Absolute Address = 400Fh

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0012h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp03_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp03_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 3
    }bits;
};
extern volatile union rw_pdf_0012h xdata g_rw_pdf_0012h;    // Absolute Address = 4012h

union rw_pdf_0013h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp03_mask_array_sel_bits_4                                           : 1;        // val = 0
        unsigned char r_gp03_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp03_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0013h xdata g_rw_pdf_0013h;    // Absolute Address = 4013h

union rw_pdf_0014h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp04_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp04_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp04_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp04_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0014h xdata g_rw_pdf_0014h;    // Absolute Address = 4014h

union rw_pdf_0017h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp04_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp04_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 4
    }bits;
};
extern volatile union rw_pdf_0017h xdata g_rw_pdf_0017h;    // Absolute Address = 4017h

union rw_pdf_0018h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp04_mask_array_sel_bits_4                                           : 1;        // val = 0
        unsigned char r_gp04_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp04_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0018h xdata g_rw_pdf_0018h;    // Absolute Address = 4018h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0019h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp05_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp05_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp05_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp05_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0019h xdata g_rw_pdf_0019h;    // Absolute Address = 4019h

union rw_pdf_001Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp05_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp05_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_pdf_001Ch xdata g_rw_pdf_001Ch;    // Absolute Address = 401Ch

union rw_pdf_001Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp05_mask_array_sel_bits_4                                           : 1;        // val = 0
        unsigned char r_gp05_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp05_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_001Dh xdata g_rw_pdf_001Dh;    // Absolute Address = 401Dh

union rw_pdf_001Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp06_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp06_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp06_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp06_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_001Eh xdata g_rw_pdf_001Eh;    // Absolute Address = 401Eh

union rw_pdf_0021h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp06_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp06_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 6
    }bits;
};
extern volatile union rw_pdf_0021h xdata g_rw_pdf_0021h;    // Absolute Address = 4021h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0022h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp06_mask_array_sel_bits_4                                           : 1;        // val = 0
        unsigned char r_gp06_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp06_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0022h xdata g_rw_pdf_0022h;    // Absolute Address = 4022h

union rw_pdf_0023h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp07_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp07_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp07_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp07_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0023h xdata g_rw_pdf_0023h;    // Absolute Address = 4023h

union rw_pdf_0026h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp07_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp07_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 7
    }bits;
};
extern volatile union rw_pdf_0026h xdata g_rw_pdf_0026h;    // Absolute Address = 4026h

union rw_pdf_0027h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp07_mask_array_sel_bits_4                                           : 1;        // val = 0
        unsigned char r_gp07_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp07_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0027h xdata g_rw_pdf_0027h;    // Absolute Address = 4027h

union rw_pdf_0028h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp08_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp08_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp08_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp08_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0028h xdata g_rw_pdf_0028h;    // Absolute Address = 4028h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_002Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp08_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp08_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_pdf_002Bh xdata g_rw_pdf_002Bh;    // Absolute Address = 402Bh

union rw_pdf_002Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp08_mask_array_sel_bits_4                                           : 1;        // val = 0
        unsigned char r_gp08_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp08_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_002Ch xdata g_rw_pdf_002Ch;    // Absolute Address = 402Ch

union rw_pdf_002Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp09_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp09_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp09_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp09_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_002Dh xdata g_rw_pdf_002Dh;    // Absolute Address = 402Dh

union rw_pdf_0030h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp09_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp09_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 9
    }bits;
};
extern volatile union rw_pdf_0030h xdata g_rw_pdf_0030h;    // Absolute Address = 4030h

union rw_pdf_0031h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp09_mask_array_sel_bits_4                                           : 1;        // val = 0
        unsigned char r_gp09_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp09_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0031h xdata g_rw_pdf_0031h;    // Absolute Address = 4031h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0032h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp10_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp10_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp10_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gp10_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0032h xdata g_rw_pdf_0032h;    // Absolute Address = 4032h

union rw_pdf_0035h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp10_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp10_pat_no3_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0035h xdata g_rw_pdf_0035h;    // Absolute Address = 4035h

union rw_pdf_0038h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp10_pat_no4_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp10_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 10
    }bits;
};
extern volatile union rw_pdf_0038h xdata g_rw_pdf_0038h;    // Absolute Address = 4038h

union rw_pdf_0039h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp10_mask_array_sel_bits_4                                           : 1;        // val = 0
        unsigned char r_gp10_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp10_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0039h xdata g_rw_pdf_0039h;    // Absolute Address = 4039h

union rw_pdf_003Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp11_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp11_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp11_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_gp11_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_003Ah xdata g_rw_pdf_003Ah;    // Absolute Address = 403Ah

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_003Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp11_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp11_pat_no3_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_003Dh xdata g_rw_pdf_003Dh;    // Absolute Address = 403Dh

union rw_pdf_0040h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp11_pat_no4_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp11_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 11
    }bits;
};
extern volatile union rw_pdf_0040h xdata g_rw_pdf_0040h;    // Absolute Address = 4040h

union rw_pdf_0041h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp11_mask_array_sel_bits_4                                           : 1;        // val = 0
        unsigned char r_gp11_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp11_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0041h xdata g_rw_pdf_0041h;    // Absolute Address = 4041h

union rw_pdf_0042h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp12_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp12_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 1
        unsigned char r_gp12_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0042h xdata g_rw_pdf_0042h;    // Absolute Address = 4042h

union rw_pdf_0045h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gp12_pat_no3_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 10
    }bits;
};
extern volatile union rw_pdf_0045h xdata g_rw_pdf_0045h;    // Absolute Address = 4045h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0048h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_pat_no4_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp12_pat_no5_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0048h xdata g_rw_pdf_0048h;    // Absolute Address = 4048h

union rw_pdf_004Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_pat_no6_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gp12_pat_no7_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 10
    }bits;
};
extern volatile union rw_pdf_004Bh xdata g_rw_pdf_004Bh;    // Absolute Address = 404Bh

union rw_pdf_004Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_pat_no8_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp12_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 12
    }bits;
};
extern volatile union rw_pdf_004Eh xdata g_rw_pdf_004Eh;    // Absolute Address = 404Eh

union rw_pdf_004Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_mask_array_sel_bits_4                                           : 1;        // val = 0
        unsigned char r_gp12_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp12_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_004Fh xdata g_rw_pdf_004Fh;    // Absolute Address = 404Fh

union rw_pdf_0050h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp13_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp13_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp13_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp13_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0050h xdata g_rw_pdf_0050h;    // Absolute Address = 4050h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0053h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp13_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp13_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 13
    }bits;
};
extern volatile union rw_pdf_0053h xdata g_rw_pdf_0053h;    // Absolute Address = 4053h

union rw_pdf_0054h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp13_mask_array_sel_bits_4                                           : 1;        // val = 0
        unsigned char r_gp13_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp13_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0054h xdata g_rw_pdf_0054h;    // Absolute Address = 4054h

union rw_pdf_0055h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp14_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp14_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp14_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp14_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0055h xdata g_rw_pdf_0055h;    // Absolute Address = 4055h

union rw_pdf_0058h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp14_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp14_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 14
    }bits;
};
extern volatile union rw_pdf_0058h xdata g_rw_pdf_0058h;    // Absolute Address = 4058h

union rw_pdf_0059h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp14_mask_array_sel_bits_4                                           : 1;        // val = 0
        unsigned char r_gp14_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp14_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0059h xdata g_rw_pdf_0059h;    // Absolute Address = 4059h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_005Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp15_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp15_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp15_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp15_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_005Ah xdata g_rw_pdf_005Ah;    // Absolute Address = 405Ah

union rw_pdf_005Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp15_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp15_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_005Dh xdata g_rw_pdf_005Dh;    // Absolute Address = 405Dh

union rw_pdf_005Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp15_mask_array_sel_bits_4                                           : 1;        // val = 0
        unsigned char r_gp15_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp15_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_005Eh xdata g_rw_pdf_005Eh;    // Absolute Address = 405Eh

union rw_pdf_005Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp16_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp16_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp16_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp16_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_005Fh xdata g_rw_pdf_005Fh;    // Absolute Address = 405Fh

union rw_pdf_0062h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp16_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp16_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0062h xdata g_rw_pdf_0062h;    // Absolute Address = 4062h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0063h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp16_mask_array_sel_bits_4                                           : 1;        // val = 1
        unsigned char r_gp16_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp16_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0063h xdata g_rw_pdf_0063h;    // Absolute Address = 4063h

union rw_pdf_0064h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp17_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp17_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp17_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp17_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0064h xdata g_rw_pdf_0064h;    // Absolute Address = 4064h

union rw_pdf_0067h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp17_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp17_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_pdf_0067h xdata g_rw_pdf_0067h;    // Absolute Address = 4067h

union rw_pdf_0068h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp17_mask_array_sel_bits_4                                           : 1;        // val = 1
        unsigned char r_gp17_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp17_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0068h xdata g_rw_pdf_0068h;    // Absolute Address = 4068h

union rw_pdf_0069h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp18_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp18_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp18_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp18_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0069h xdata g_rw_pdf_0069h;    // Absolute Address = 4069h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_006Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp18_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp18_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 2
    }bits;
};
extern volatile union rw_pdf_006Ch xdata g_rw_pdf_006Ch;    // Absolute Address = 406Ch

union rw_pdf_006Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp18_mask_array_sel_bits_4                                           : 1;        // val = 1
        unsigned char r_gp18_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp18_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_006Dh xdata g_rw_pdf_006Dh;    // Absolute Address = 406Dh

union rw_pdf_006Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp19_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp19_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp19_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp19_pat_no1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_006Eh xdata g_rw_pdf_006Eh;    // Absolute Address = 406Eh

union rw_pdf_0071h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp19_pat_no2_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 10
        unsigned char r_gp19_mask_array_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 3
    }bits;
};
extern volatile union rw_pdf_0071h xdata g_rw_pdf_0071h;    // Absolute Address = 4071h

union rw_pdf_0072h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp19_mask_array_sel_bits_4                                           : 1;        // val = 1
        unsigned char r_gp19_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_gp19_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0072h xdata g_rw_pdf_0072h;    // Absolute Address = 4072h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0073h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp20_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp20_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp20_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp20_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0073h xdata g_rw_pdf_0073h;    // Absolute Address = 4073h

union rw_pdf_0074h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp20_mask_array_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char r_gp20_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0074h xdata g_rw_pdf_0074h;    // Absolute Address = 4074h

union rw_pdf_0075h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp21_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp21_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp21_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp21_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0075h xdata g_rw_pdf_0075h;    // Absolute Address = 4075h

union rw_pdf_0076h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp21_mask_array_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 1
        unsigned char r_gp21_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0076h xdata g_rw_pdf_0076h;    // Absolute Address = 4076h

union rw_pdf_0077h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp22_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp22_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp22_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp22_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0077h xdata g_rw_pdf_0077h;    // Absolute Address = 4077h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0078h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp22_mask_array_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 2
        unsigned char r_gp22_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0078h xdata g_rw_pdf_0078h;    // Absolute Address = 4078h

union rw_pdf_0079h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp23_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp23_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp23_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp23_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0079h xdata g_rw_pdf_0079h;    // Absolute Address = 4079h

union rw_pdf_007Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp23_mask_array_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 3
        unsigned char r_gp23_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_007Ah xdata g_rw_pdf_007Ah;    // Absolute Address = 407Ah

union rw_pdf_007Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp24_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp24_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp24_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp24_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_pdf_007Bh xdata g_rw_pdf_007Bh;    // Absolute Address = 407Bh

union rw_pdf_007Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp24_mask_array_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 4
        unsigned char r_gp24_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_007Ch xdata g_rw_pdf_007Ch;    // Absolute Address = 407Ch

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_007Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp25_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp25_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp25_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp25_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_pdf_007Dh xdata g_rw_pdf_007Dh;    // Absolute Address = 407Dh

union rw_pdf_007Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp25_mask_array_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 5
        unsigned char r_gp25_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_007Eh xdata g_rw_pdf_007Eh;    // Absolute Address = 407Eh

union rw_pdf_007Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp26_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp26_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp26_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp26_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_pdf_007Fh xdata g_rw_pdf_007Fh;    // Absolute Address = 407Fh

union rw_pdf_0080h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp26_mask_array_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 6
        unsigned char r_gp26_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0080h xdata g_rw_pdf_0080h;    // Absolute Address = 4080h

union rw_pdf_0081h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp27_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp27_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp27_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp27_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0081h xdata g_rw_pdf_0081h;    // Absolute Address = 4081h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0082h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp27_mask_array_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 7
        unsigned char r_gp27_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0082h xdata g_rw_pdf_0082h;    // Absolute Address = 4082h

union rw_pdf_0083h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp28_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp28_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp28_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp28_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0083h xdata g_rw_pdf_0083h;    // Absolute Address = 4083h

union rw_pdf_0084h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp28_mask_array_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 8
        unsigned char r_gp28_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0084h xdata g_rw_pdf_0084h;    // Absolute Address = 4084h

union rw_pdf_0085h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp29_pat_en                                                          : 1;        // val = 1
        unsigned char r_gp29_mask_sel                                                        : 1;        // val = 0
        unsigned char r_gp29_patsize_sel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp29_pix_color_sel                                                   : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0085h xdata g_rw_pdf_0085h;    // Absolute Address = 4085h

union rw_pdf_0086h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp29_mask_array_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 9
        unsigned char r_gp29_gray_comp_sel                                                   : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0086h xdata g_rw_pdf_0086h;    // Absolute Address = 4086h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0088h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp00_acc_hgh_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 15
        unsigned char r_gp01_acc_hgh_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0088h xdata g_rw_pdf_0088h;    // Absolute Address = 4088h

union rw_pdf_008Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp02_acc_hgh_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 15
        unsigned char r_gp03_acc_hgh_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_008Bh xdata g_rw_pdf_008Bh;    // Absolute Address = 408Bh

union rw_pdf_008Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp04_acc_hgh_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 15
        unsigned char r_gp05_acc_hgh_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_008Eh xdata g_rw_pdf_008Eh;    // Absolute Address = 408Eh

union rw_pdf_0091h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp06_acc_hgh_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 15
        unsigned char r_gp07_acc_hgh_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0091h xdata g_rw_pdf_0091h;    // Absolute Address = 4091h

union rw_pdf_0094h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp08_acc_hgh_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 15
        unsigned char r_gp09_acc_hgh_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0094h xdata g_rw_pdf_0094h;    // Absolute Address = 4094h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0097h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp10_acc_hgh_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 15
        unsigned char r_gp11_acc_hgh_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_0097h xdata g_rw_pdf_0097h;    // Absolute Address = 4097h

union rw_pdf_009Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_acc_hgh_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 15
        unsigned char r_gp13_acc_hgh_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_009Ah xdata g_rw_pdf_009Ah;    // Absolute Address = 409Ah

union rw_pdf_009Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp14_acc_hgh_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 15
        unsigned char r_gp15_acc_hgh_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_009Dh xdata g_rw_pdf_009Dh;    // Absolute Address = 409Dh

union rw_pdf_00A0h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp16_acc_hgh_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 15
        unsigned char r_gp17_acc_hgh_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_00A0h xdata g_rw_pdf_00A0h;    // Absolute Address = 40A0h

union rw_pdf_00A3h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp18_acc_hgh_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 15
        unsigned char r_gp19_acc_hgh_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_00A3h xdata g_rw_pdf_00A3h;    // Absolute Address = 40A3h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_00A6h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp20_acc_hgh_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 15
        unsigned char r_gp21_acc_hgh_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_00A6h xdata g_rw_pdf_00A6h;    // Absolute Address = 40A6h

union rw_pdf_00A9h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp22_acc_hgh_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 15
        unsigned char r_gp23_acc_hgh_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_00A9h xdata g_rw_pdf_00A9h;    // Absolute Address = 40A9h

union rw_pdf_00ACh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp24_acc_hgh_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 15
        unsigned char r_gp25_acc_hgh_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_00ACh xdata g_rw_pdf_00ACh;    // Absolute Address = 40ACh

union rw_pdf_00AFh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp26_acc_hgh_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 15
        unsigned char r_gp27_acc_hgh_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_00AFh xdata g_rw_pdf_00AFh;    // Absolute Address = 40AFh

union rw_pdf_00B2h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp28_acc_hgh_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 15
        unsigned char r_gp29_acc_hgh_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_pdf_00B2h xdata g_rw_pdf_00B2h;    // Absolute Address = 40B2h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_00B5h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp00_acc_low_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gp01_acc_low_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_pdf_00B5h xdata g_rw_pdf_00B5h;    // Absolute Address = 40B5h

union rw_pdf_00B8h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp02_acc_low_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gp03_acc_low_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_pdf_00B8h xdata g_rw_pdf_00B8h;    // Absolute Address = 40B8h

union rw_pdf_00BBh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp04_acc_low_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gp05_acc_low_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_pdf_00BBh xdata g_rw_pdf_00BBh;    // Absolute Address = 40BBh

union rw_pdf_00BEh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp06_acc_low_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gp07_acc_low_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_pdf_00BEh xdata g_rw_pdf_00BEh;    // Absolute Address = 40BEh

union rw_pdf_00C1h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp08_acc_low_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gp09_acc_low_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_pdf_00C1h xdata g_rw_pdf_00C1h;    // Absolute Address = 40C1h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_00C4h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp10_acc_low_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gp11_acc_low_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_pdf_00C4h xdata g_rw_pdf_00C4h;    // Absolute Address = 40C4h

union rw_pdf_00C7h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_acc_low_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gp13_acc_low_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_pdf_00C7h xdata g_rw_pdf_00C7h;    // Absolute Address = 40C7h

union rw_pdf_00CAh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp14_acc_low_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gp15_acc_low_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_pdf_00CAh xdata g_rw_pdf_00CAh;    // Absolute Address = 40CAh

union rw_pdf_00CDh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp16_acc_low_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gp17_acc_low_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_pdf_00CDh xdata g_rw_pdf_00CDh;    // Absolute Address = 40CDh

union rw_pdf_00D0h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp18_acc_low_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gp19_acc_low_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_pdf_00D0h xdata g_rw_pdf_00D0h;    // Absolute Address = 40D0h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_00D3h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp20_acc_low_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gp21_acc_low_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_pdf_00D3h xdata g_rw_pdf_00D3h;    // Absolute Address = 40D3h

union rw_pdf_00D6h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp22_acc_low_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gp23_acc_low_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_pdf_00D6h xdata g_rw_pdf_00D6h;    // Absolute Address = 40D6h

union rw_pdf_00D9h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp24_acc_low_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gp25_acc_low_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_pdf_00D9h xdata g_rw_pdf_00D9h;    // Absolute Address = 40D9h

union rw_pdf_00DCh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp26_acc_low_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gp27_acc_low_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_pdf_00DCh xdata g_rw_pdf_00DCh;    // Absolute Address = 40DCh

union rw_pdf_00DFh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp28_acc_low_th_bits_11_8                                            : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_gp29_acc_low_th_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_pdf_00DFh xdata g_rw_pdf_00DFh;    // Absolute Address = 40DFh

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_00F1h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pix_color_en0                                                        : 7;        // [msb:lsb] = [6:0], val = 1
        unsigned char r_pix_color_en1_bits_0                                                 : 1;        // val = 1
    }bits;
};
extern volatile union rw_pdf_00F1h xdata g_rw_pdf_00F1h;    // Absolute Address = 40F1h

union rw_pdf_00F2h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pix_color_en1_bits_6_1                                               : 6;        // [msb:lsb] = [6:1], val = 43
        unsigned char r_pix_color_en2_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_pdf_00F2h xdata g_rw_pdf_00F2h;    // Absolute Address = 40F2h

union rw_pdf_00F3h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pix_color_en2_bits_6_2                                               : 5;        // [msb:lsb] = [6:2], val = 31
        unsigned char r_pix_color_en3_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_00F3h xdata g_rw_pdf_00F3h;    // Absolute Address = 40F3h

union rw_pdf_00F4h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pix_color_en3_bits_6_3                                               : 4;        // [msb:lsb] = [6:3], val = 4
        unsigned char r_pix_color_en4_bits_3_0                                               : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_pdf_00F4h xdata g_rw_pdf_00F4h;    // Absolute Address = 40F4h

union rw_pdf_00F5h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pix_color_en4_bits_6_4                                               : 3;        // [msb:lsb] = [6:4], val = 2
        unsigned char r_pix_color_en5_bits_4_0                                               : 5;        // [msb:lsb] = [4:0], val = 8
    }bits;
};
extern volatile union rw_pdf_00F5h xdata g_rw_pdf_00F5h;    // Absolute Address = 40F5h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_00F6h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pix_color_en5_bits_6_5                                               : 2;        // [msb:lsb] = [6:5], val = 0
        unsigned char r_pix_color_en6_bits_5_0                                               : 6;        // [msb:lsb] = [5:0], val = 11
    }bits;
};
extern volatile union rw_pdf_00F6h xdata g_rw_pdf_00F6h;    // Absolute Address = 40F6h

union rw_pdf_00F7h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pix_color_en6_bits_6                                                 : 1;        // val = 0
        unsigned char r_pix_color_en7                                                        : 7;        // [msb:lsb] = [6:0], val = 127
    }bits;
};
extern volatile union rw_pdf_00F7h xdata g_rw_pdf_00F7h;    // Absolute Address = 40F7h

union rw_pdf_00F8h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pix_color_en8                                                        : 7;        // [msb:lsb] = [6:0], val = 1
        unsigned char r_pix_color_en9_bits_0                                                 : 1;        // val = 1
    }bits;
};
extern volatile union rw_pdf_00F8h xdata g_rw_pdf_00F8h;    // Absolute Address = 40F8h

union rw_pdf_00F9h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pix_color_en9_bits_6_1                                               : 6;        // [msb:lsb] = [6:1], val = 43
        unsigned char r_pix_color_en10_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_pdf_00F9h xdata g_rw_pdf_00F9h;    // Absolute Address = 40F9h

union rw_pdf_00FAh
{
    unsigned char byte;
    struct
    {
        unsigned char r_pix_color_en10_bits_6_2                                              : 5;        // [msb:lsb] = [6:2], val = 31
        unsigned char r_pix_color_en11_bits_2_0                                              : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_00FAh xdata g_rw_pdf_00FAh;    // Absolute Address = 40FAh

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_00FBh
{
    unsigned char byte;
    struct
    {
        unsigned char r_pix_color_en11_bits_6_3                                              : 4;        // [msb:lsb] = [6:3], val = 4
        unsigned char r_pix_color_en12_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_pdf_00FBh xdata g_rw_pdf_00FBh;    // Absolute Address = 40FBh

union rw_pdf_00FCh
{
    unsigned char byte;
    struct
    {
        unsigned char r_pix_color_en12_bits_6_4                                              : 3;        // [msb:lsb] = [6:4], val = 2
        unsigned char r_pix_color_en13_bits_4_0                                              : 5;        // [msb:lsb] = [4:0], val = 8
    }bits;
};
extern volatile union rw_pdf_00FCh xdata g_rw_pdf_00FCh;    // Absolute Address = 40FCh

union rw_pdf_00FDh
{
    unsigned char byte;
    struct
    {
        unsigned char r_pix_color_en13_bits_6_5                                              : 2;        // [msb:lsb] = [6:5], val = 0
        unsigned char r_pix_color_en14_bits_5_0                                              : 6;        // [msb:lsb] = [5:0], val = 11
    }bits;
};
extern volatile union rw_pdf_00FDh xdata g_rw_pdf_00FDh;    // Absolute Address = 40FDh

union rw_pdf_00FEh
{
    unsigned char byte;
    struct
    {
        unsigned char r_pix_color_en14_bits_6                                                : 1;        // val = 0
        unsigned char r_pix_color_en15                                                       : 7;        // [msb:lsb] = [6:0], val = 127
    }bits;
};
extern volatile union rw_pdf_00FEh xdata g_rw_pdf_00FEh;    // Absolute Address = 40FEh

union rw_pdf_00FFh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp00_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp01_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp02_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp03_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_pdf_00FFh xdata g_rw_pdf_00FFh;    // Absolute Address = 40FFh

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0100h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp04_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp05_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp06_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp07_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_pdf_0100h xdata g_rw_pdf_0100h;    // Absolute Address = 4100h

union rw_pdf_0101h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp08_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp09_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp10_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp11_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_pdf_0101h xdata g_rw_pdf_0101h;    // Absolute Address = 4101h

union rw_pdf_0102h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp13_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp14_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp15_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_pdf_0102h xdata g_rw_pdf_0102h;    // Absolute Address = 4102h

union rw_pdf_0103h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp16_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp17_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp18_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp19_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_pdf_0103h xdata g_rw_pdf_0103h;    // Absolute Address = 4103h

union rw_pdf_0104h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp20_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp21_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp22_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp23_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_pdf_0104h xdata g_rw_pdf_0104h;    // Absolute Address = 4104h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0105h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp24_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp25_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp26_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp27_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_pdf_0105h xdata g_rw_pdf_0105h;    // Absolute Address = 4105h

union rw_pdf_0106h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp28_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp29_drv_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_gp00_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 1
        unsigned char r_gp01_pol_sel_bits_0                                                  : 1;        // val = 1
    }bits;
};
extern volatile union rw_pdf_0106h xdata g_rw_pdf_0106h;    // Absolute Address = 4106h

union rw_pdf_0107h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp01_pol_sel_bits_2_1                                                : 2;        // [msb:lsb] = [2:1], val = 0
        unsigned char r_gp02_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 2
        unsigned char r_gp03_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 3
    }bits;
};
extern volatile union rw_pdf_0107h xdata g_rw_pdf_0107h;    // Absolute Address = 4107h

union rw_pdf_0108h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp04_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 1
        unsigned char r_gp05_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 2
        unsigned char r_gp06_pol_sel_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_pdf_0108h xdata g_rw_pdf_0108h;    // Absolute Address = 4108h

union rw_pdf_0109h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp06_pol_sel_bits_2                                                  : 1;        // val = 0
        unsigned char r_gp07_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 1
        unsigned char r_gp08_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 2
        unsigned char r_gp09_pol_sel_bits_0                                                  : 1;        // val = 1
    }bits;
};
extern volatile union rw_pdf_0109h xdata g_rw_pdf_0109h;    // Absolute Address = 4109h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_010Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp09_pol_sel_bits_2_1                                                : 2;        // [msb:lsb] = [2:1], val = 1
        unsigned char r_gp10_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 1
        unsigned char r_gp11_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 2
    }bits;
};
extern volatile union rw_pdf_010Ah xdata g_rw_pdf_010Ah;    // Absolute Address = 410Ah

union rw_pdf_010Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 3
        unsigned char r_gp13_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 1
        unsigned char r_gp14_pol_sel_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_pdf_010Bh xdata g_rw_pdf_010Bh;    // Absolute Address = 410Bh

union rw_pdf_010Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp14_pol_sel_bits_2                                                  : 1;        // val = 0
        unsigned char r_gp15_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 3
        unsigned char r_gp16_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 1
        unsigned char r_gp17_pol_sel_bits_0                                                  : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_010Ch xdata g_rw_pdf_010Ch;    // Absolute Address = 410Ch

union rw_pdf_010Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp17_pol_sel_bits_2_1                                                : 2;        // [msb:lsb] = [2:1], val = 1
        unsigned char r_gp18_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 3
        unsigned char r_gp19_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 1
    }bits;
};
extern volatile union rw_pdf_010Dh xdata g_rw_pdf_010Dh;    // Absolute Address = 410Dh

union rw_pdf_010Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp20_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 2
        unsigned char r_gp21_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 3
        unsigned char r_gp22_pol_sel_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_pdf_010Eh xdata g_rw_pdf_010Eh;    // Absolute Address = 410Eh

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_010Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp22_pol_sel_bits_2                                                  : 1;        // val = 0
        unsigned char r_gp23_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 2
        unsigned char r_gp24_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 3
        unsigned char r_gp25_pol_sel_bits_0                                                  : 1;        // val = 1
    }bits;
};
extern volatile union rw_pdf_010Fh xdata g_rw_pdf_010Fh;    // Absolute Address = 410Fh

union rw_pdf_0110h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp25_pol_sel_bits_2_1                                                : 2;        // [msb:lsb] = [2:1], val = 0
        unsigned char r_gp26_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 2
        unsigned char r_gp27_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 3
    }bits;
};
extern volatile union rw_pdf_0110h xdata g_rw_pdf_0110h;    // Absolute Address = 4110h

union rw_pdf_0111h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp28_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 1
        unsigned char r_gp29_pol_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 1
        unsigned char r_gp00_pri_num_sel_bits_1_0                                            : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0111h xdata g_rw_pdf_0111h;    // Absolute Address = 4111h

union rw_pdf_0112h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp00_pri_num_sel_bits_4_2                                            : 3;        // [msb:lsb] = [4:2], val = 0
        unsigned char r_gp01_pri_num_sel                                                     : 5;        // [msb:lsb] = [4:0], val = 1
    }bits;
};
extern volatile union rw_pdf_0112h xdata g_rw_pdf_0112h;    // Absolute Address = 4112h

union rw_pdf_0113h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp02_pri_num_sel                                                     : 5;        // [msb:lsb] = [4:0], val = 2
        unsigned char r_gp03_pri_num_sel_bits_2_0                                            : 3;        // [msb:lsb] = [2:0], val = 3
    }bits;
};
extern volatile union rw_pdf_0113h xdata g_rw_pdf_0113h;    // Absolute Address = 4113h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0114h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp03_pri_num_sel_bits_4_3                                            : 2;        // [msb:lsb] = [4:3], val = 0
        unsigned char r_gp04_pri_num_sel                                                     : 5;        // [msb:lsb] = [4:0], val = 4
        unsigned char r_gp05_pri_num_sel_bits_0                                              : 1;        // val = 1
    }bits;
};
extern volatile union rw_pdf_0114h xdata g_rw_pdf_0114h;    // Absolute Address = 4114h

union rw_pdf_0115h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp05_pri_num_sel_bits_4_1                                            : 4;        // [msb:lsb] = [4:1], val = 2
        unsigned char r_gp06_pri_num_sel_bits_3_0                                            : 4;        // [msb:lsb] = [3:0], val = 6
    }bits;
};
extern volatile union rw_pdf_0115h xdata g_rw_pdf_0115h;    // Absolute Address = 4115h

union rw_pdf_0116h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp06_pri_num_sel_bits_4                                              : 1;        // val = 0
        unsigned char r_gp07_pri_num_sel                                                     : 5;        // [msb:lsb] = [4:0], val = 7
        unsigned char r_gp08_pri_num_sel_bits_1_0                                            : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0116h xdata g_rw_pdf_0116h;    // Absolute Address = 4116h

union rw_pdf_0117h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp08_pri_num_sel_bits_4_2                                            : 3;        // [msb:lsb] = [4:2], val = 2
        unsigned char r_gp09_pri_num_sel                                                     : 5;        // [msb:lsb] = [4:0], val = 9
    }bits;
};
extern volatile union rw_pdf_0117h xdata g_rw_pdf_0117h;    // Absolute Address = 4117h

union rw_pdf_0118h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp10_pri_num_sel                                                     : 5;        // [msb:lsb] = [4:0], val = 10
        unsigned char r_gp11_pri_num_sel_bits_2_0                                            : 3;        // [msb:lsb] = [2:0], val = 3
    }bits;
};
extern volatile union rw_pdf_0118h xdata g_rw_pdf_0118h;    // Absolute Address = 4118h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0119h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp11_pri_num_sel_bits_4_3                                            : 2;        // [msb:lsb] = [4:3], val = 1
        unsigned char r_gp12_pri_num_sel                                                     : 5;        // [msb:lsb] = [4:0], val = 12
        unsigned char r_gp13_pri_num_sel_bits_0                                              : 1;        // val = 1
    }bits;
};
extern volatile union rw_pdf_0119h xdata g_rw_pdf_0119h;    // Absolute Address = 4119h

union rw_pdf_011Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp13_pri_num_sel_bits_4_1                                            : 4;        // [msb:lsb] = [4:1], val = 6
        unsigned char r_gp14_pri_num_sel_bits_3_0                                            : 4;        // [msb:lsb] = [3:0], val = 14
    }bits;
};
extern volatile union rw_pdf_011Ah xdata g_rw_pdf_011Ah;    // Absolute Address = 411Ah

union rw_pdf_011Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp14_pri_num_sel_bits_4                                              : 1;        // val = 0
        unsigned char r_gp15_pri_num_sel                                                     : 5;        // [msb:lsb] = [4:0], val = 15
        unsigned char r_gp16_pri_num_sel_bits_1_0                                            : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_pdf_011Bh xdata g_rw_pdf_011Bh;    // Absolute Address = 411Bh

union rw_pdf_011Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp16_pri_num_sel_bits_4_2                                            : 3;        // [msb:lsb] = [4:2], val = 4
        unsigned char r_gp17_pri_num_sel                                                     : 5;        // [msb:lsb] = [4:0], val = 17
    }bits;
};
extern volatile union rw_pdf_011Ch xdata g_rw_pdf_011Ch;    // Absolute Address = 411Ch

union rw_pdf_011Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp18_pri_num_sel                                                     : 5;        // [msb:lsb] = [4:0], val = 18
        unsigned char r_gp19_pri_num_sel_bits_2_0                                            : 3;        // [msb:lsb] = [2:0], val = 3
    }bits;
};
extern volatile union rw_pdf_011Dh xdata g_rw_pdf_011Dh;    // Absolute Address = 411Dh

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_011Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp19_pri_num_sel_bits_4_3                                            : 2;        // [msb:lsb] = [4:3], val = 2
        unsigned char r_gp20_pri_num_sel                                                     : 5;        // [msb:lsb] = [4:0], val = 20
        unsigned char r_gp21_pri_num_sel_bits_0                                              : 1;        // val = 1
    }bits;
};
extern volatile union rw_pdf_011Eh xdata g_rw_pdf_011Eh;    // Absolute Address = 411Eh

union rw_pdf_011Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp21_pri_num_sel_bits_4_1                                            : 4;        // [msb:lsb] = [4:1], val = 10
        unsigned char r_gp22_pri_num_sel_bits_3_0                                            : 4;        // [msb:lsb] = [3:0], val = 6
    }bits;
};
extern volatile union rw_pdf_011Fh xdata g_rw_pdf_011Fh;    // Absolute Address = 411Fh

union rw_pdf_0120h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp22_pri_num_sel_bits_4                                              : 1;        // val = 1
        unsigned char r_gp23_pri_num_sel                                                     : 5;        // [msb:lsb] = [4:0], val = 23
        unsigned char r_gp24_pri_num_sel_bits_1_0                                            : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0120h xdata g_rw_pdf_0120h;    // Absolute Address = 4120h

union rw_pdf_0121h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp24_pri_num_sel_bits_4_2                                            : 3;        // [msb:lsb] = [4:2], val = 6
        unsigned char r_gp25_pri_num_sel                                                     : 5;        // [msb:lsb] = [4:0], val = 25
    }bits;
};
extern volatile union rw_pdf_0121h xdata g_rw_pdf_0121h;    // Absolute Address = 4121h

union rw_pdf_0122h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp26_pri_num_sel                                                     : 5;        // [msb:lsb] = [4:0], val = 26
        unsigned char r_gp27_pri_num_sel_bits_2_0                                            : 3;        // [msb:lsb] = [2:0], val = 3
    }bits;
};
extern volatile union rw_pdf_0122h xdata g_rw_pdf_0122h;    // Absolute Address = 4122h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0123h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp27_pri_num_sel_bits_4_3                                            : 2;        // [msb:lsb] = [4:3], val = 3
        unsigned char r_gp28_pri_num_sel                                                     : 5;        // [msb:lsb] = [4:0], val = 28
        unsigned char r_gp29_pri_num_sel_bits_0                                              : 1;        // val = 1
    }bits;
};
extern volatile union rw_pdf_0123h xdata g_rw_pdf_0123h;    // Absolute Address = 4123h

union rw_pdf_0124h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp29_pri_num_sel_bits_4_1                                            : 4;        // [msb:lsb] = [4:1], val = 14
        unsigned char r_gp10_region_en                                                       : 1;        // val = 0
        unsigned char r_gp11_region_en                                                       : 1;        // val = 0
        unsigned char r_gp12_region_en                                                       : 1;        // val = 0
        unsigned char r_gp13_region_en                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_0124h xdata g_rw_pdf_0124h;    // Absolute Address = 4124h

union rw_pdf_0125h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp14_region_en                                                       : 1;        // val = 0
        unsigned char r_gp15_region_en                                                       : 1;        // val = 0
        unsigned char r_gp16_region_en                                                       : 1;        // val = 0
        unsigned char r_gp17_region_en                                                       : 1;        // val = 0
        unsigned char r_gp18_region_en                                                       : 1;        // val = 0
        unsigned char r_gp19_region_en                                                       : 1;        // val = 0
        unsigned char r_gp20_region_en                                                       : 1;        // val = 1
        unsigned char r_gp21_region_en                                                       : 1;        // val = 1
    }bits;
};
extern volatile union rw_pdf_0125h xdata g_rw_pdf_0125h;    // Absolute Address = 4125h

union rw_pdf_0126h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp22_region_en                                                       : 1;        // val = 1
        unsigned char r_gp23_region_en                                                       : 1;        // val = 1
        unsigned char r_gp24_region_en                                                       : 1;        // val = 1
        unsigned char r_gp25_region_en                                                       : 1;        // val = 1
        unsigned char r_gp26_region_en                                                       : 1;        // val = 1
        unsigned char r_gp27_region_en                                                       : 1;        // val = 1
        unsigned char r_gp28_region_en                                                       : 1;        // val = 1
        unsigned char r_gp29_region_en                                                       : 1;        // val = 1
    }bits;
};
extern volatile union rw_pdf_0126h xdata g_rw_pdf_0126h;    // Absolute Address = 4126h

union rw_pdf_0127h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x00_pos                                                       : 7;        // [msb:lsb] = [6:0], val = 1
        unsigned char r_region_y00_pos_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_0127h xdata g_rw_pdf_0127h;    // Absolute Address = 4127h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0128h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y00_pos_bits_5_1                                              : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_region_leng00_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_pdf_0128h xdata g_rw_pdf_0128h;    // Absolute Address = 4128h

union rw_pdf_0129h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_leng00_bits_6_3                                               : 4;        // [msb:lsb] = [6:3], val = 15
        unsigned char r_region_x01_pos_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_pdf_0129h xdata g_rw_pdf_0129h;    // Absolute Address = 4129h

union rw_pdf_012Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x01_pos_bits_6_4                                              : 3;        // [msb:lsb] = [6:4], val = 0
        unsigned char r_region_y01_pos_bits_4_0                                              : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_pdf_012Ah xdata g_rw_pdf_012Ah;    // Absolute Address = 412Ah

union rw_pdf_012Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y01_pos_bits_5                                                : 1;        // val = 0
        unsigned char r_region_leng01                                                        : 7;        // [msb:lsb] = [6:0], val = 127
    }bits;
};
extern volatile union rw_pdf_012Bh xdata g_rw_pdf_012Bh;    // Absolute Address = 412Bh

union rw_pdf_012Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x02_pos                                                       : 7;        // [msb:lsb] = [6:0], val = 1
        unsigned char r_region_y02_pos_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_012Ch xdata g_rw_pdf_012Ch;    // Absolute Address = 412Ch

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_012Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y02_pos_bits_5_1                                              : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_region_leng02_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_pdf_012Dh xdata g_rw_pdf_012Dh;    // Absolute Address = 412Dh

union rw_pdf_012Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_leng02_bits_6_3                                               : 4;        // [msb:lsb] = [6:3], val = 15
        unsigned char r_region_x03_pos_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_pdf_012Eh xdata g_rw_pdf_012Eh;    // Absolute Address = 412Eh

union rw_pdf_012Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x03_pos_bits_6_4                                              : 3;        // [msb:lsb] = [6:4], val = 0
        unsigned char r_region_y03_pos_bits_4_0                                              : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_pdf_012Fh xdata g_rw_pdf_012Fh;    // Absolute Address = 412Fh

union rw_pdf_0130h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y03_pos_bits_5                                                : 1;        // val = 0
        unsigned char r_region_leng03                                                        : 7;        // [msb:lsb] = [6:0], val = 127
    }bits;
};
extern volatile union rw_pdf_0130h xdata g_rw_pdf_0130h;    // Absolute Address = 4130h

union rw_pdf_0131h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x04_pos                                                       : 7;        // [msb:lsb] = [6:0], val = 1
        unsigned char r_region_y04_pos_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_0131h xdata g_rw_pdf_0131h;    // Absolute Address = 4131h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0132h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y04_pos_bits_5_1                                              : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_region_leng04_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_pdf_0132h xdata g_rw_pdf_0132h;    // Absolute Address = 4132h

union rw_pdf_0133h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_leng04_bits_6_3                                               : 4;        // [msb:lsb] = [6:3], val = 15
        unsigned char r_region_x05_pos_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_pdf_0133h xdata g_rw_pdf_0133h;    // Absolute Address = 4133h

union rw_pdf_0134h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x05_pos_bits_6_4                                              : 3;        // [msb:lsb] = [6:4], val = 0
        unsigned char r_region_y05_pos_bits_4_0                                              : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0134h xdata g_rw_pdf_0134h;    // Absolute Address = 4134h

union rw_pdf_0135h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y05_pos_bits_5                                                : 1;        // val = 0
        unsigned char r_region_leng05                                                        : 7;        // [msb:lsb] = [6:0], val = 127
    }bits;
};
extern volatile union rw_pdf_0135h xdata g_rw_pdf_0135h;    // Absolute Address = 4135h

union rw_pdf_0136h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x06_pos                                                       : 7;        // [msb:lsb] = [6:0], val = 1
        unsigned char r_region_y06_pos_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_0136h xdata g_rw_pdf_0136h;    // Absolute Address = 4136h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0137h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y06_pos_bits_5_1                                              : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_region_leng06_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_pdf_0137h xdata g_rw_pdf_0137h;    // Absolute Address = 4137h

union rw_pdf_0138h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_leng06_bits_6_3                                               : 4;        // [msb:lsb] = [6:3], val = 15
        unsigned char r_region_x07_pos_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_pdf_0138h xdata g_rw_pdf_0138h;    // Absolute Address = 4138h

union rw_pdf_0139h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x07_pos_bits_6_4                                              : 3;        // [msb:lsb] = [6:4], val = 0
        unsigned char r_region_y07_pos_bits_4_0                                              : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0139h xdata g_rw_pdf_0139h;    // Absolute Address = 4139h

union rw_pdf_013Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y07_pos_bits_5                                                : 1;        // val = 0
        unsigned char r_region_leng07                                                        : 7;        // [msb:lsb] = [6:0], val = 127
    }bits;
};
extern volatile union rw_pdf_013Ah xdata g_rw_pdf_013Ah;    // Absolute Address = 413Ah

union rw_pdf_013Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x08_pos                                                       : 7;        // [msb:lsb] = [6:0], val = 1
        unsigned char r_region_y08_pos_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_013Bh xdata g_rw_pdf_013Bh;    // Absolute Address = 413Bh

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_013Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y08_pos_bits_5_1                                              : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_region_leng08_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_pdf_013Ch xdata g_rw_pdf_013Ch;    // Absolute Address = 413Ch

union rw_pdf_013Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_leng08_bits_6_3                                               : 4;        // [msb:lsb] = [6:3], val = 15
        unsigned char r_region_x09_pos_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_pdf_013Dh xdata g_rw_pdf_013Dh;    // Absolute Address = 413Dh

union rw_pdf_013Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x09_pos_bits_6_4                                              : 3;        // [msb:lsb] = [6:4], val = 0
        unsigned char r_region_y09_pos_bits_4_0                                              : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_pdf_013Eh xdata g_rw_pdf_013Eh;    // Absolute Address = 413Eh

union rw_pdf_013Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y09_pos_bits_5                                                : 1;        // val = 0
        unsigned char r_region_leng09                                                        : 7;        // [msb:lsb] = [6:0], val = 127
    }bits;
};
extern volatile union rw_pdf_013Fh xdata g_rw_pdf_013Fh;    // Absolute Address = 413Fh

union rw_pdf_0140h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x10_pos                                                       : 7;        // [msb:lsb] = [6:0], val = 1
        unsigned char r_region_y10_pos_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_0140h xdata g_rw_pdf_0140h;    // Absolute Address = 4140h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0141h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y10_pos_bits_5_1                                              : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_region_leng10_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_pdf_0141h xdata g_rw_pdf_0141h;    // Absolute Address = 4141h

union rw_pdf_0142h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_leng10_bits_6_3                                               : 4;        // [msb:lsb] = [6:3], val = 15
        unsigned char r_region_x11_pos_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_pdf_0142h xdata g_rw_pdf_0142h;    // Absolute Address = 4142h

union rw_pdf_0143h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x11_pos_bits_6_4                                              : 3;        // [msb:lsb] = [6:4], val = 0
        unsigned char r_region_y11_pos_bits_4_0                                              : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0143h xdata g_rw_pdf_0143h;    // Absolute Address = 4143h

union rw_pdf_0144h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y11_pos_bits_5                                                : 1;        // val = 0
        unsigned char r_region_leng11                                                        : 7;        // [msb:lsb] = [6:0], val = 127
    }bits;
};
extern volatile union rw_pdf_0144h xdata g_rw_pdf_0144h;    // Absolute Address = 4144h

union rw_pdf_0145h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x12_pos                                                       : 7;        // [msb:lsb] = [6:0], val = 1
        unsigned char r_region_y12_pos_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_0145h xdata g_rw_pdf_0145h;    // Absolute Address = 4145h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0146h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y12_pos_bits_5_1                                              : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_region_leng12_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_pdf_0146h xdata g_rw_pdf_0146h;    // Absolute Address = 4146h

union rw_pdf_0147h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_leng12_bits_6_3                                               : 4;        // [msb:lsb] = [6:3], val = 15
        unsigned char r_region_x13_pos_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_pdf_0147h xdata g_rw_pdf_0147h;    // Absolute Address = 4147h

union rw_pdf_0148h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x13_pos_bits_6_4                                              : 3;        // [msb:lsb] = [6:4], val = 0
        unsigned char r_region_y13_pos_bits_4_0                                              : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0148h xdata g_rw_pdf_0148h;    // Absolute Address = 4148h

union rw_pdf_0149h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y13_pos_bits_5                                                : 1;        // val = 0
        unsigned char r_region_leng13                                                        : 7;        // [msb:lsb] = [6:0], val = 127
    }bits;
};
extern volatile union rw_pdf_0149h xdata g_rw_pdf_0149h;    // Absolute Address = 4149h

union rw_pdf_014Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x14_pos                                                       : 7;        // [msb:lsb] = [6:0], val = 1
        unsigned char r_region_y14_pos_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_014Ah xdata g_rw_pdf_014Ah;    // Absolute Address = 414Ah

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_014Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y14_pos_bits_5_1                                              : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_region_leng14_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_pdf_014Bh xdata g_rw_pdf_014Bh;    // Absolute Address = 414Bh

union rw_pdf_014Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_leng14_bits_6_3                                               : 4;        // [msb:lsb] = [6:3], val = 15
        unsigned char r_region_x15_pos_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_pdf_014Ch xdata g_rw_pdf_014Ch;    // Absolute Address = 414Ch

union rw_pdf_014Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x15_pos_bits_6_4                                              : 3;        // [msb:lsb] = [6:4], val = 0
        unsigned char r_region_y15_pos_bits_4_0                                              : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_pdf_014Dh xdata g_rw_pdf_014Dh;    // Absolute Address = 414Dh

union rw_pdf_014Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y15_pos_bits_5                                                : 1;        // val = 0
        unsigned char r_region_leng15                                                        : 7;        // [msb:lsb] = [6:0], val = 127
    }bits;
};
extern volatile union rw_pdf_014Eh xdata g_rw_pdf_014Eh;    // Absolute Address = 414Eh

union rw_pdf_014Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x16_pos                                                       : 7;        // [msb:lsb] = [6:0], val = 1
        unsigned char r_region_y16_pos_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_014Fh xdata g_rw_pdf_014Fh;    // Absolute Address = 414Fh

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0150h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y16_pos_bits_5_1                                              : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_region_leng16_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_pdf_0150h xdata g_rw_pdf_0150h;    // Absolute Address = 4150h

union rw_pdf_0151h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_leng16_bits_6_3                                               : 4;        // [msb:lsb] = [6:3], val = 15
        unsigned char r_region_x17_pos_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_pdf_0151h xdata g_rw_pdf_0151h;    // Absolute Address = 4151h

union rw_pdf_0152h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x17_pos_bits_6_4                                              : 3;        // [msb:lsb] = [6:4], val = 0
        unsigned char r_region_y17_pos_bits_4_0                                              : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0152h xdata g_rw_pdf_0152h;    // Absolute Address = 4152h

union rw_pdf_0153h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y17_pos_bits_5                                                : 1;        // val = 0
        unsigned char r_region_leng17                                                        : 7;        // [msb:lsb] = [6:0], val = 127
    }bits;
};
extern volatile union rw_pdf_0153h xdata g_rw_pdf_0153h;    // Absolute Address = 4153h

union rw_pdf_0154h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x18_pos                                                       : 7;        // [msb:lsb] = [6:0], val = 1
        unsigned char r_region_y18_pos_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_0154h xdata g_rw_pdf_0154h;    // Absolute Address = 4154h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0155h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y18_pos_bits_5_1                                              : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_region_leng18_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_pdf_0155h xdata g_rw_pdf_0155h;    // Absolute Address = 4155h

union rw_pdf_0156h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_leng18_bits_6_3                                               : 4;        // [msb:lsb] = [6:3], val = 15
        unsigned char r_region_x19_pos_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_pdf_0156h xdata g_rw_pdf_0156h;    // Absolute Address = 4156h

union rw_pdf_0157h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_x19_pos_bits_6_4                                              : 3;        // [msb:lsb] = [6:4], val = 0
        unsigned char r_region_y19_pos_bits_4_0                                              : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0157h xdata g_rw_pdf_0157h;    // Absolute Address = 4157h

union rw_pdf_0158h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_y19_pos_bits_5                                                : 1;        // val = 0
        unsigned char r_region_leng19                                                        : 7;        // [msb:lsb] = [6:0], val = 127
    }bits;
};
extern volatile union rw_pdf_0158h xdata g_rw_pdf_0158h;    // Absolute Address = 4158h

union rw_pdf_0159h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp10_4x4_multi_en                                                    : 1;        // val = 1
        unsigned char r_gp11_4x4_multi_en                                                    : 1;        // val = 1
        unsigned char r_gp12_8x8_multi_en                                                    : 1;        // val = 1
        unsigned char r_gp10_logic_comb_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0159h xdata g_rw_pdf_0159h;    // Absolute Address = 4159h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_015Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp11_logic_comb_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char r_gp12_logic_comb_sel_bits_2_0                                         : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_015Ah xdata g_rw_pdf_015Ah;    // Absolute Address = 415Ah

union rw_pdf_015Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_logic_comb_sel_bits_4_3                                         : 2;        // [msb:lsb] = [4:3], val = 0
        unsigned char r_gp13_logic_comb_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char r_gp14_logic_comb_sel_bits_0                                           : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_015Bh xdata g_rw_pdf_015Bh;    // Absolute Address = 415Bh

union rw_pdf_015Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp14_logic_comb_sel_bits_4_1                                         : 4;        // [msb:lsb] = [4:1], val = 0
        unsigned char r_gp15_logic_comb_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_pdf_015Ch xdata g_rw_pdf_015Ch;    // Absolute Address = 415Ch

union rw_pdf_015Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp15_logic_comb_sel_bits_4                                           : 1;        // val = 0
        unsigned char r_gp16_logic_comb_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char r_gp17_logic_comb_sel_bits_1_0                                         : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_pdf_015Dh xdata g_rw_pdf_015Dh;    // Absolute Address = 415Dh

union rw_pdf_015Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp17_logic_comb_sel_bits_4_2                                         : 3;        // [msb:lsb] = [4:2], val = 0
        unsigned char r_gp18_logic_comb_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 19
    }bits;
};
extern volatile union rw_pdf_015Eh xdata g_rw_pdf_015Eh;    // Absolute Address = 415Eh

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_015Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp19_logic_comb_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 19
        unsigned char r_gp20_logic_comb_sel_bits_2_0                                         : 3;        // [msb:lsb] = [2:0], val = 5
    }bits;
};
extern volatile union rw_pdf_015Fh xdata g_rw_pdf_015Fh;    // Absolute Address = 415Fh

union rw_pdf_0160h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp20_logic_comb_sel_bits_4_3                                         : 2;        // [msb:lsb] = [4:3], val = 2
        unsigned char r_gp21_logic_comb_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 21
        unsigned char r_gp22_logic_comb_sel_bits_0                                           : 1;        // val = 1
    }bits;
};
extern volatile union rw_pdf_0160h xdata g_rw_pdf_0160h;    // Absolute Address = 4160h

union rw_pdf_0161h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp22_logic_comb_sel_bits_4_1                                         : 4;        // [msb:lsb] = [4:1], val = 11
        unsigned char r_gp23_logic_comb_sel_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 7
    }bits;
};
extern volatile union rw_pdf_0161h xdata g_rw_pdf_0161h;    // Absolute Address = 4161h

union rw_pdf_0162h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp23_logic_comb_sel_bits_4                                           : 1;        // val = 1
        unsigned char r_gp24_logic_comb_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 25
        unsigned char r_gp25_logic_comb_sel_bits_1_0                                         : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_pdf_0162h xdata g_rw_pdf_0162h;    // Absolute Address = 4162h

union rw_pdf_0163h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp25_logic_comb_sel_bits_4_2                                         : 3;        // [msb:lsb] = [4:2], val = 6
        unsigned char r_gp26_logic_comb_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 27
    }bits;
};
extern volatile union rw_pdf_0163h xdata g_rw_pdf_0163h;    // Absolute Address = 4163h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0164h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp27_logic_comb_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 27
        unsigned char r_gp28_logic_comb_sel_bits_2_0                                         : 3;        // [msb:lsb] = [2:0], val = 5
    }bits;
};
extern volatile union rw_pdf_0164h xdata g_rw_pdf_0164h;    // Absolute Address = 4164h

union rw_pdf_0165h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp28_logic_comb_sel_bits_4_3                                         : 2;        // [msb:lsb] = [4:3], val = 3
        unsigned char r_gp29_logic_comb_sel                                                  : 5;        // [msb:lsb] = [4:0], val = 29
        unsigned char r_frame_deb_bits_0                                                     : 1;        // val = 1
    }bits;
};
extern volatile union rw_pdf_0165h xdata g_rw_pdf_0165h;    // Absolute Address = 4165h

union rw_pdf_0166h
{
    unsigned char byte;
    struct
    {
        unsigned char r_frame_deb_bits_7_1                                                   : 7;        // [msb:lsb] = [7:1], val = 0
        unsigned char r_shift_acc_globe_bits_0                                               : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_0166h xdata g_rw_pdf_0166h;    // Absolute Address = 4166h

union rw_pdf_0167h
{
    unsigned char byte;
    struct
    {
        unsigned char r_shift_acc_globe_bits_4_1                                             : 4;        // [msb:lsb] = [4:1], val = 3
        unsigned char r_shift_acc_local_bits_3_0                                             : 4;        // [msb:lsb] = [3:0], val = 6
    }bits;
};
extern volatile union rw_pdf_0167h xdata g_rw_pdf_0167h;    // Absolute Address = 4167h

union rw_pdf_0168h
{
    unsigned char byte;
    struct
    {
        unsigned char r_shift_acc_local_bits_4                                               : 1;        // val = 0
        unsigned char r_pat_globe_en                                                         : 1;        // val = 0
        unsigned char r_pat_debug                                                            : 1;        // val = 0
        unsigned char r_tot_hit_sel                                                          : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_lbuf_addr_set_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0168h xdata g_rw_pdf_0168h;    // Absolute Address = 4168h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0169h
{
    unsigned char byte;
    struct
    {
        unsigned char r_lbuf_addr_set_bits_3                                                 : 1;        // val = 0
        unsigned char r_truncate_bit9                                                        : 1;        // val = 0
        unsigned char r_truncate_bit6                                                        : 1;        // val = 0
        unsigned char r_hit_debug_sel                                                        : 1;        // val = 0
        unsigned char r_pri_en                                                               : 1;        // val = 1
        unsigned char r_gp12_pat_no9_bits_2_0                                                : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_pdf_0169h xdata g_rw_pdf_0169h;    // Absolute Address = 4169h

union rw_pdf_016Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_pat_no9_bits_11                                                 : 1;        // val = 0
        unsigned char r_gp12_pat_no10_bits_6_0                                               : 7;        // [msb:lsb] = [6:0], val = 63
    }bits;
};
extern volatile union rw_pdf_016Bh xdata g_rw_pdf_016Bh;    // Absolute Address = 416Bh

union rw_pdf_016Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_pat_no10_bits_11_7                                              : 5;        // [msb:lsb] = [11:7], val = 0
        unsigned char r_gp12_pat_no11_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 2
    }bits;
};
extern volatile union rw_pdf_016Ch xdata g_rw_pdf_016Ch;    // Absolute Address = 416Ch

union rw_pdf_016Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_pat_no11_bits_11                                                : 1;        // val = 1
        unsigned char r_gp12_pat_no12_bits_6_0                                               : 7;        // [msb:lsb] = [6:0], val = 42
    }bits;
};
extern volatile union rw_pdf_016Eh xdata g_rw_pdf_016Eh;    // Absolute Address = 416Eh

union rw_pdf_016Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_pat_no12_bits_11_7                                              : 5;        // [msb:lsb] = [11:7], val = 21
        unsigned char r_gp12_pat_no13_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_pdf_016Fh xdata g_rw_pdf_016Fh;    // Absolute Address = 416Fh

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0171h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_pat_no13_bits_11                                                : 1;        // val = 0
        unsigned char r_gp12_pat_no14_bits_6_0                                               : 7;        // [msb:lsb] = [6:0], val = 63
    }bits;
};
extern volatile union rw_pdf_0171h xdata g_rw_pdf_0171h;    // Absolute Address = 4171h

union rw_pdf_0172h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_pat_no14_bits_11_7                                              : 5;        // [msb:lsb] = [11:7], val = 0
        unsigned char r_gp12_pat_no15_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 2
    }bits;
};
extern volatile union rw_pdf_0172h xdata g_rw_pdf_0172h;    // Absolute Address = 4172h

union rw_pdf_0174h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_pat_no15_bits_11                                                : 1;        // val = 1
        unsigned char r_gp12_pat_no16_bits_6_0                                               : 7;        // [msb:lsb] = [6:0], val = 42
    }bits;
};
extern volatile union rw_pdf_0174h xdata g_rw_pdf_0174h;    // Absolute Address = 4174h

union rw_pdf_0175h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_pat_no16_bits_11_7                                              : 5;        // [msb:lsb] = [11:7], val = 21
        unsigned char reserve1                                                               : 1;        // val = 0
        unsigned char r_gp00_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp01_hysteresis_en                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_0175h xdata g_rw_pdf_0175h;    // Absolute Address = 4175h

union rw_pdf_0176h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp02_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp03_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp04_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp05_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp06_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp07_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp08_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp09_hysteresis_en                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_0176h xdata g_rw_pdf_0176h;    // Absolute Address = 4176h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0177h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp10_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp11_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp12_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp13_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp14_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp15_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp16_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp17_hysteresis_en                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_0177h xdata g_rw_pdf_0177h;    // Absolute Address = 4177h

union rw_pdf_0178h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp18_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp19_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp20_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp21_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp22_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp23_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp24_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp25_hysteresis_en                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_0178h xdata g_rw_pdf_0178h;    // Absolute Address = 4178h

union rw_pdf_0179h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp26_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp27_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp28_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gp29_hysteresis_en                                                   : 1;        // val = 0
        unsigned char r_gray0_hysteresis_en                                                  : 1;        // val = 0
        unsigned char r_gray1_hysteresis_en                                                  : 1;        // val = 0
        unsigned char r_gray2_hysteresis_en                                                  : 1;        // val = 0
        unsigned char r_gray3_hysteresis_en                                                  : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_0179h xdata g_rw_pdf_0179h;    // Absolute Address = 4179h

union rw_pdf_017Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gray4_hysteresis_en                                                  : 1;        // val = 0
        unsigned char r_gray5_hysteresis_en                                                  : 1;        // val = 0
        unsigned char r_gray6_hysteresis_en                                                  : 1;        // val = 0
        unsigned char r_gray7_hysteresis_en                                                  : 1;        // val = 0
        unsigned char r_gray_hck_hys_en                                                      : 1;        // val = 0
        unsigned char r_gray_hck_hys_deb_sel                                                 : 1;        // val = 0
        unsigned char r_hgh_gray_th0_a_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_pdf_017Ah xdata g_rw_pdf_017Ah;    // Absolute Address = 417Ah

union rw_pdf_017Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_hgh_gray_th0_a_bits_7_2                                              : 6;        // [msb:lsb] = [7:2], val = 2
        unsigned char r_hgh_gray_th1_a_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_pdf_017Bh xdata g_rw_pdf_017Bh;    // Absolute Address = 417Bh

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_017Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_hgh_gray_th1_a_bits_7_2                                              : 6;        // [msb:lsb] = [7:2], val = 2
        unsigned char r_hgh_gray_th2_a_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_pdf_017Ch xdata g_rw_pdf_017Ch;    // Absolute Address = 417Ch

union rw_pdf_017Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_hgh_gray_th2_a_bits_7_2                                              : 6;        // [msb:lsb] = [7:2], val = 2
        unsigned char r_hgh_gray_th3_a_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_pdf_017Dh xdata g_rw_pdf_017Dh;    // Absolute Address = 417Dh

union rw_pdf_017Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_hgh_gray_th3_a_bits_7_2                                              : 6;        // [msb:lsb] = [7:2], val = 2
        unsigned char r_hgh_gray_th4_a_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_pdf_017Eh xdata g_rw_pdf_017Eh;    // Absolute Address = 417Eh

union rw_pdf_017Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_hgh_gray_th4_a_bits_7_2                                              : 6;        // [msb:lsb] = [7:2], val = 2
        unsigned char r_hgh_gray_th5_a_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_pdf_017Fh xdata g_rw_pdf_017Fh;    // Absolute Address = 417Fh

union rw_pdf_0180h
{
    unsigned char byte;
    struct
    {
        unsigned char r_hgh_gray_th5_a_bits_7_2                                              : 6;        // [msb:lsb] = [7:2], val = 2
        unsigned char r_hgh_gray_th6_a_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_pdf_0180h xdata g_rw_pdf_0180h;    // Absolute Address = 4180h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0181h
{
    unsigned char byte;
    struct
    {
        unsigned char r_hgh_gray_th6_a_bits_7_2                                              : 6;        // [msb:lsb] = [7:2], val = 2
        unsigned char r_hgh_gray_th7_a_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_pdf_0181h xdata g_rw_pdf_0181h;    // Absolute Address = 4181h

union rw_pdf_0182h
{
    unsigned char byte;
    struct
    {
        unsigned char r_hgh_gray_th7_a_bits_7_2                                              : 6;        // [msb:lsb] = [7:2], val = 2
        unsigned char r_low_gray_th0_b_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_pdf_0182h xdata g_rw_pdf_0182h;    // Absolute Address = 4182h

union rw_pdf_0183h
{
    unsigned char byte;
    struct
    {
        unsigned char r_low_gray_th0_b_bits_7_2                                              : 6;        // [msb:lsb] = [7:2], val = 2
        unsigned char r_low_gray_th1_b_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_pdf_0183h xdata g_rw_pdf_0183h;    // Absolute Address = 4183h

union rw_pdf_0184h
{
    unsigned char byte;
    struct
    {
        unsigned char r_low_gray_th1_b_bits_7_2                                              : 6;        // [msb:lsb] = [7:2], val = 2
        unsigned char r_low_gray_th2_b_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_pdf_0184h xdata g_rw_pdf_0184h;    // Absolute Address = 4184h

union rw_pdf_0185h
{
    unsigned char byte;
    struct
    {
        unsigned char r_low_gray_th2_b_bits_7_2                                              : 6;        // [msb:lsb] = [7:2], val = 2
        unsigned char r_low_gray_th3_b_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_pdf_0185h xdata g_rw_pdf_0185h;    // Absolute Address = 4185h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0186h
{
    unsigned char byte;
    struct
    {
        unsigned char r_low_gray_th3_b_bits_7_2                                              : 6;        // [msb:lsb] = [7:2], val = 2
        unsigned char r_low_gray_th4_b_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_pdf_0186h xdata g_rw_pdf_0186h;    // Absolute Address = 4186h

union rw_pdf_0187h
{
    unsigned char byte;
    struct
    {
        unsigned char r_low_gray_th4_b_bits_7_2                                              : 6;        // [msb:lsb] = [7:2], val = 2
        unsigned char r_low_gray_th5_b_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_pdf_0187h xdata g_rw_pdf_0187h;    // Absolute Address = 4187h

union rw_pdf_0188h
{
    unsigned char byte;
    struct
    {
        unsigned char r_low_gray_th5_b_bits_7_2                                              : 6;        // [msb:lsb] = [7:2], val = 2
        unsigned char r_low_gray_th6_b_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_pdf_0188h xdata g_rw_pdf_0188h;    // Absolute Address = 4188h

union rw_pdf_0189h
{
    unsigned char byte;
    struct
    {
        unsigned char r_low_gray_th6_b_bits_7_2                                              : 6;        // [msb:lsb] = [7:2], val = 2
        unsigned char r_low_gray_th7_b_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_pdf_0189h xdata g_rw_pdf_0189h;    // Absolute Address = 4189h

union rw_pdf_018Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_low_gray_th7_b_bits_7_2                                              : 6;        // [msb:lsb] = [7:2], val = 2
        unsigned char r_area_opt_sel                                                         : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_pdf_018Ah xdata g_rw_pdf_018Ah;    // Absolute Address = 418Ah

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_018Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width00_bits_8                                                : 1;        // val = 0
        unsigned char r_region_width01_bits_6_0                                              : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_pdf_018Ch xdata g_rw_pdf_018Ch;    // Absolute Address = 418Ch

union rw_pdf_018Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width01_bits_8_7                                              : 2;        // [msb:lsb] = [8:7], val = 0
        unsigned char r_region_width02_bits_5_0                                              : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_pdf_018Dh xdata g_rw_pdf_018Dh;    // Absolute Address = 418Dh

union rw_pdf_018Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width02_bits_8_6                                              : 3;        // [msb:lsb] = [8:6], val = 0
        unsigned char r_region_width03_bits_4_0                                              : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_pdf_018Eh xdata g_rw_pdf_018Eh;    // Absolute Address = 418Eh

union rw_pdf_018Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width03_bits_8_5                                              : 4;        // [msb:lsb] = [8:5], val = 0
        unsigned char r_region_width04_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_pdf_018Fh xdata g_rw_pdf_018Fh;    // Absolute Address = 418Fh

union rw_pdf_0190h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width04_bits_8_4                                              : 5;        // [msb:lsb] = [8:4], val = 0
        unsigned char r_region_width05_bits_2_0                                              : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0190h xdata g_rw_pdf_0190h;    // Absolute Address = 4190h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0191h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width05_bits_8_3                                              : 6;        // [msb:lsb] = [8:3], val = 0
        unsigned char r_region_width06_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0191h xdata g_rw_pdf_0191h;    // Absolute Address = 4191h

union rw_pdf_0192h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width06_bits_8_2                                              : 7;        // [msb:lsb] = [8:2], val = 0
        unsigned char r_region_width07_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_0192h xdata g_rw_pdf_0192h;    // Absolute Address = 4192h

union rw_pdf_0195h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width08_bits_8                                                : 1;        // val = 0
        unsigned char r_region_width09_bits_6_0                                              : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0195h xdata g_rw_pdf_0195h;    // Absolute Address = 4195h

union rw_pdf_0196h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width09_bits_8_7                                              : 2;        // [msb:lsb] = [8:7], val = 0
        unsigned char r_region_width10_bits_5_0                                              : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0196h xdata g_rw_pdf_0196h;    // Absolute Address = 4196h

union rw_pdf_0197h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width10_bits_8_6                                              : 3;        // [msb:lsb] = [8:6], val = 0
        unsigned char r_region_width11_bits_4_0                                              : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0197h xdata g_rw_pdf_0197h;    // Absolute Address = 4197h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_0198h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width11_bits_8_5                                              : 4;        // [msb:lsb] = [8:5], val = 0
        unsigned char r_region_width12_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0198h xdata g_rw_pdf_0198h;    // Absolute Address = 4198h

union rw_pdf_0199h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width12_bits_8_4                                              : 5;        // [msb:lsb] = [8:4], val = 0
        unsigned char r_region_width13_bits_2_0                                              : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_0199h xdata g_rw_pdf_0199h;    // Absolute Address = 4199h

union rw_pdf_019Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width13_bits_8_3                                              : 6;        // [msb:lsb] = [8:3], val = 0
        unsigned char r_region_width14_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_pdf_019Ah xdata g_rw_pdf_019Ah;    // Absolute Address = 419Ah

union rw_pdf_019Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width14_bits_8_2                                              : 7;        // [msb:lsb] = [8:2], val = 0
        unsigned char r_region_width15_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_019Bh xdata g_rw_pdf_019Bh;    // Absolute Address = 419Bh

union rw_pdf_019Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width16_bits_8                                                : 1;        // val = 0
        unsigned char r_region_width17_bits_6_0                                              : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_pdf_019Eh xdata g_rw_pdf_019Eh;    // Absolute Address = 419Eh

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_019Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width17_bits_8_7                                              : 2;        // [msb:lsb] = [8:7], val = 0
        unsigned char r_region_width18_bits_5_0                                              : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_pdf_019Fh xdata g_rw_pdf_019Fh;    // Absolute Address = 419Fh

union rw_pdf_01A0h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width18_bits_8_6                                              : 3;        // [msb:lsb] = [8:6], val = 0
        unsigned char r_region_width19_bits_4_0                                              : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_pdf_01A0h xdata g_rw_pdf_01A0h;    // Absolute Address = 41A0h

union rw_pdf_01A1h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region_width19_bits_8_5                                              : 4;        // [msb:lsb] = [8:5], val = 0
        unsigned char r_region2_leng00                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_region2_leng01_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_01A1h xdata g_rw_pdf_01A1h;    // Absolute Address = 41A1h

union rw_pdf_01A2h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region2_leng01_bits_2_1                                              : 2;        // [msb:lsb] = [2:1], val = 0
        unsigned char r_region2_leng02                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_region2_leng03                                                       : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_01A2h xdata g_rw_pdf_01A2h;    // Absolute Address = 41A2h

union rw_pdf_01A3h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region2_leng04                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_region2_leng05                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_region2_leng06_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_pdf_01A3h xdata g_rw_pdf_01A3h;    // Absolute Address = 41A3h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_01A4h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region2_leng06_bits_2                                                : 1;        // val = 0
        unsigned char r_region2_leng07                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_region2_leng08                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_region2_leng09_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_01A4h xdata g_rw_pdf_01A4h;    // Absolute Address = 41A4h

union rw_pdf_01A5h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region2_leng09_bits_2_1                                              : 2;        // [msb:lsb] = [2:1], val = 0
        unsigned char r_region2_leng10                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_region2_leng11                                                       : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_01A5h xdata g_rw_pdf_01A5h;    // Absolute Address = 41A5h

union rw_pdf_01A6h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region2_leng12                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_region2_leng13                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_region2_leng14_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_pdf_01A6h xdata g_rw_pdf_01A6h;    // Absolute Address = 41A6h

union rw_pdf_01A7h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region2_leng14_bits_2                                                : 1;        // val = 0
        unsigned char r_region2_leng15                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_region2_leng16                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_region2_leng17_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_pdf_01A7h xdata g_rw_pdf_01A7h;    // Absolute Address = 41A7h

union rw_pdf_01A8h
{
    unsigned char byte;
    struct
    {
        unsigned char r_region2_leng17_bits_2_1                                              : 2;        // [msb:lsb] = [2:1], val = 0
        unsigned char r_region2_leng18                                                       : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_region2_leng19                                                       : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_pdf_01A8h xdata g_rw_pdf_01A8h;    // Absolute Address = 41A8h

//------------------------------------------------------------------------------------------------------------------------
union rw_pdf_01A9h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gp12_0_4x4_multi_en                                                  : 1;        // val = 0
        unsigned char r_gp12_1_4x4_multi_en                                                  : 1;        // val = 0
        unsigned char r_2x1_4x1_sel                                                          : 1;        // val = 0
        unsigned char reserved_4_by_tool                                                     : 1;
        unsigned char reserved_3_by_tool                                                     : 1;
        unsigned char reserved_2_by_tool                                                     : 1;
        unsigned char reserved_1_by_tool                                                     : 1;
        unsigned char reserved_0_by_tool                                                     : 1;
    }bits;
};
extern volatile union rw_pdf_01A9h xdata g_rw_pdf_01A9h;    // Absolute Address = 41A9h


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_pdf_0001h_gp00_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 4001h
extern volatile uint8_t xdata g_rw_pdf_0002h_gp00_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 4002h
extern volatile uint8_t xdata g_rw_pdf_0006h_gp01_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 4006h
extern volatile uint8_t xdata g_rw_pdf_0007h_gp01_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 4007h
extern volatile uint8_t xdata g_rw_pdf_000Bh_gp02_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 400Bh

extern volatile uint8_t xdata g_rw_pdf_000Ch_gp02_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 400Ch
extern volatile uint8_t xdata g_rw_pdf_0010h_gp03_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 4010h
extern volatile uint8_t xdata g_rw_pdf_0011h_gp03_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 4011h
extern volatile uint8_t xdata g_rw_pdf_0015h_gp04_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 4015h
extern volatile uint8_t xdata g_rw_pdf_0016h_gp04_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 4016h

extern volatile uint8_t xdata g_rw_pdf_001Ah_gp05_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 401Ah
extern volatile uint8_t xdata g_rw_pdf_001Bh_gp05_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 401Bh
extern volatile uint8_t xdata g_rw_pdf_001Fh_gp06_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 401Fh
extern volatile uint8_t xdata g_rw_pdf_0020h_gp06_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 4020h
extern volatile uint8_t xdata g_rw_pdf_0024h_gp07_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 4024h

extern volatile uint8_t xdata g_rw_pdf_0025h_gp07_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 4025h
extern volatile uint8_t xdata g_rw_pdf_0029h_gp08_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 4029h
extern volatile uint8_t xdata g_rw_pdf_002Ah_gp08_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 402Ah
extern volatile uint8_t xdata g_rw_pdf_002Eh_gp09_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 402Eh
extern volatile uint8_t xdata g_rw_pdf_002Fh_gp09_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 402Fh

extern volatile uint8_t xdata g_rw_pdf_0033h_gp10_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 4033h
extern volatile uint8_t xdata g_rw_pdf_0034h_gp10_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 4034h
extern volatile uint8_t xdata g_rw_pdf_0036h_gp10_pat_no3;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 4036h
extern volatile uint8_t xdata g_rw_pdf_0037h_gp10_pat_no4;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 4037h
extern volatile uint8_t xdata g_rw_pdf_003Bh_gp11_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 403Bh

extern volatile uint8_t xdata g_rw_pdf_003Ch_gp11_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 403Ch
extern volatile uint8_t xdata g_rw_pdf_003Eh_gp11_pat_no3;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 403Eh
extern volatile uint8_t xdata g_rw_pdf_003Fh_gp11_pat_no4;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 403Fh
extern volatile uint8_t xdata g_rw_pdf_0043h_gp12_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 4043h
extern volatile uint8_t xdata g_rw_pdf_0044h_gp12_pat_no2;                                               // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 4044h

extern volatile uint8_t xdata g_rw_pdf_0046h_gp12_pat_no3;                                               // [msb:lsb] = [11:4], val = 170, 	Absolute Address = 4046h
extern volatile uint8_t xdata g_rw_pdf_0047h_gp12_pat_no4;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 4047h
extern volatile uint8_t xdata g_rw_pdf_0049h_gp12_pat_no5;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 4049h
extern volatile uint8_t xdata g_rw_pdf_004Ah_gp12_pat_no6;                                               // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 404Ah
extern volatile uint8_t xdata g_rw_pdf_004Ch_gp12_pat_no7;                                               // [msb:lsb] = [11:4], val = 170, 	Absolute Address = 404Ch

extern volatile uint8_t xdata g_rw_pdf_004Dh_gp12_pat_no8;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 404Dh
extern volatile uint8_t xdata g_rw_pdf_0051h_gp13_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 4051h
extern volatile uint8_t xdata g_rw_pdf_0052h_gp13_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 4052h
extern volatile uint8_t xdata g_rw_pdf_0056h_gp14_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 4056h
extern volatile uint8_t xdata g_rw_pdf_0057h_gp14_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 4057h

extern volatile uint8_t xdata g_rw_pdf_005Bh_gp15_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 405Bh
extern volatile uint8_t xdata g_rw_pdf_005Ch_gp15_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 405Ch
extern volatile uint8_t xdata g_rw_pdf_0060h_gp16_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 4060h
extern volatile uint8_t xdata g_rw_pdf_0061h_gp16_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 4061h
extern volatile uint8_t xdata g_rw_pdf_0065h_gp17_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 4065h

extern volatile uint8_t xdata g_rw_pdf_0066h_gp17_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 4066h
extern volatile uint8_t xdata g_rw_pdf_006Ah_gp18_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 406Ah
extern volatile uint8_t xdata g_rw_pdf_006Bh_gp18_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 406Bh
extern volatile uint8_t xdata g_rw_pdf_006Fh_gp19_pat_no1;                                               // [msb:lsb] = [11:4], val = 3, 	Absolute Address = 406Fh
extern volatile uint8_t xdata g_rw_pdf_0070h_gp19_pat_no2;                                               // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 4070h

extern volatile uint8_t xdata g_rw_pdf_0087h_gp00_acc_hgh_th;                                            // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 4087h
extern volatile uint8_t xdata g_rw_pdf_0089h_gp01_acc_hgh_th;                                            // [msb:lsb] = [11:4], val = 255, 	Absolute Address = 4089h
extern volatile uint8_t xdata g_rw_pdf_008Ah_gp02_acc_hgh_th;                                            // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 408Ah
extern volatile uint8_t xdata g_rw_pdf_008Ch_gp03_acc_hgh_th;                                            // [msb:lsb] = [11:4], val = 255, 	Absolute Address = 408Ch
extern volatile uint8_t xdata g_rw_pdf_008Dh_gp04_acc_hgh_th;                                            // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 408Dh

extern volatile uint8_t xdata g_rw_pdf_008Fh_gp05_acc_hgh_th;                                            // [msb:lsb] = [11:4], val = 255, 	Absolute Address = 408Fh
extern volatile uint8_t xdata g_rw_pdf_0090h_gp06_acc_hgh_th;                                            // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 4090h
extern volatile uint8_t xdata g_rw_pdf_0092h_gp07_acc_hgh_th;                                            // [msb:lsb] = [11:4], val = 255, 	Absolute Address = 4092h
extern volatile uint8_t xdata g_rw_pdf_0093h_gp08_acc_hgh_th;                                            // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 4093h
extern volatile uint8_t xdata g_rw_pdf_0095h_gp09_acc_hgh_th;                                            // [msb:lsb] = [11:4], val = 255, 	Absolute Address = 4095h

extern volatile uint8_t xdata g_rw_pdf_0096h_gp10_acc_hgh_th;                                            // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 4096h
extern volatile uint8_t xdata g_rw_pdf_0098h_gp11_acc_hgh_th;                                            // [msb:lsb] = [11:4], val = 255, 	Absolute Address = 4098h
extern volatile uint8_t xdata g_rw_pdf_0099h_gp12_acc_hgh_th;                                            // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 4099h
extern volatile uint8_t xdata g_rw_pdf_009Bh_gp13_acc_hgh_th;                                            // [msb:lsb] = [11:4], val = 255, 	Absolute Address = 409Bh
extern volatile uint8_t xdata g_rw_pdf_009Ch_gp14_acc_hgh_th;                                            // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 409Ch

extern volatile uint8_t xdata g_rw_pdf_009Eh_gp15_acc_hgh_th;                                            // [msb:lsb] = [11:4], val = 255, 	Absolute Address = 409Eh
extern volatile uint8_t xdata g_rw_pdf_009Fh_gp16_acc_hgh_th;                                            // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 409Fh
extern volatile uint8_t xdata g_rw_pdf_00A1h_gp17_acc_hgh_th;                                            // [msb:lsb] = [11:4], val = 255, 	Absolute Address = 40A1h
extern volatile uint8_t xdata g_rw_pdf_00A2h_gp18_acc_hgh_th;                                            // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 40A2h
extern volatile uint8_t xdata g_rw_pdf_00A4h_gp19_acc_hgh_th;                                            // [msb:lsb] = [11:4], val = 255, 	Absolute Address = 40A4h

extern volatile uint8_t xdata g_rw_pdf_00A5h_gp20_acc_hgh_th;                                            // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 40A5h
extern volatile uint8_t xdata g_rw_pdf_00A7h_gp21_acc_hgh_th;                                            // [msb:lsb] = [11:4], val = 255, 	Absolute Address = 40A7h
extern volatile uint8_t xdata g_rw_pdf_00A8h_gp22_acc_hgh_th;                                            // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 40A8h
extern volatile uint8_t xdata g_rw_pdf_00AAh_gp23_acc_hgh_th;                                            // [msb:lsb] = [11:4], val = 255, 	Absolute Address = 40AAh
extern volatile uint8_t xdata g_rw_pdf_00ABh_gp24_acc_hgh_th;                                            // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 40ABh

extern volatile uint8_t xdata g_rw_pdf_00ADh_gp25_acc_hgh_th;                                            // [msb:lsb] = [11:4], val = 255, 	Absolute Address = 40ADh
extern volatile uint8_t xdata g_rw_pdf_00AEh_gp26_acc_hgh_th;                                            // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 40AEh
extern volatile uint8_t xdata g_rw_pdf_00B0h_gp27_acc_hgh_th;                                            // [msb:lsb] = [11:4], val = 255, 	Absolute Address = 40B0h
extern volatile uint8_t xdata g_rw_pdf_00B1h_gp28_acc_hgh_th;                                            // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 40B1h
extern volatile uint8_t xdata g_rw_pdf_00B3h_gp29_acc_hgh_th;                                            // [msb:lsb] = [11:4], val = 255, 	Absolute Address = 40B3h

extern volatile uint8_t xdata g_rw_pdf_00B4h_gp00_acc_low_th;                                            // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 40B4h
extern volatile uint8_t xdata g_rw_pdf_00B6h_gp01_acc_low_th;                                            // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 40B6h
extern volatile uint8_t xdata g_rw_pdf_00B7h_gp02_acc_low_th;                                            // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 40B7h
extern volatile uint8_t xdata g_rw_pdf_00B9h_gp03_acc_low_th;                                            // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 40B9h
extern volatile uint8_t xdata g_rw_pdf_00BAh_gp04_acc_low_th;                                            // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 40BAh

extern volatile uint8_t xdata g_rw_pdf_00BCh_gp05_acc_low_th;                                            // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 40BCh
extern volatile uint8_t xdata g_rw_pdf_00BDh_gp06_acc_low_th;                                            // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 40BDh
extern volatile uint8_t xdata g_rw_pdf_00BFh_gp07_acc_low_th;                                            // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 40BFh
extern volatile uint8_t xdata g_rw_pdf_00C0h_gp08_acc_low_th;                                            // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 40C0h
extern volatile uint8_t xdata g_rw_pdf_00C2h_gp09_acc_low_th;                                            // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 40C2h

extern volatile uint8_t xdata g_rw_pdf_00C3h_gp10_acc_low_th;                                            // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 40C3h
extern volatile uint8_t xdata g_rw_pdf_00C5h_gp11_acc_low_th;                                            // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 40C5h
extern volatile uint8_t xdata g_rw_pdf_00C6h_gp12_acc_low_th;                                            // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 40C6h
extern volatile uint8_t xdata g_rw_pdf_00C8h_gp13_acc_low_th;                                            // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 40C8h
extern volatile uint8_t xdata g_rw_pdf_00C9h_gp14_acc_low_th;                                            // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 40C9h

extern volatile uint8_t xdata g_rw_pdf_00CBh_gp15_acc_low_th;                                            // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 40CBh
extern volatile uint8_t xdata g_rw_pdf_00CCh_gp16_acc_low_th;                                            // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 40CCh
extern volatile uint8_t xdata g_rw_pdf_00CEh_gp17_acc_low_th;                                            // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 40CEh
extern volatile uint8_t xdata g_rw_pdf_00CFh_gp18_acc_low_th;                                            // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 40CFh
extern volatile uint8_t xdata g_rw_pdf_00D1h_gp19_acc_low_th;                                            // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 40D1h

extern volatile uint8_t xdata g_rw_pdf_00D2h_gp20_acc_low_th;                                            // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 40D2h
extern volatile uint8_t xdata g_rw_pdf_00D4h_gp21_acc_low_th;                                            // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 40D4h
extern volatile uint8_t xdata g_rw_pdf_00D5h_gp22_acc_low_th;                                            // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 40D5h
extern volatile uint8_t xdata g_rw_pdf_00D7h_gp23_acc_low_th;                                            // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 40D7h
extern volatile uint8_t xdata g_rw_pdf_00D8h_gp24_acc_low_th;                                            // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 40D8h

extern volatile uint8_t xdata g_rw_pdf_00DAh_gp25_acc_low_th;                                            // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 40DAh
extern volatile uint8_t xdata g_rw_pdf_00DBh_gp26_acc_low_th;                                            // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 40DBh
extern volatile uint8_t xdata g_rw_pdf_00DDh_gp27_acc_low_th;                                            // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 40DDh
extern volatile uint8_t xdata g_rw_pdf_00DEh_gp28_acc_low_th;                                            // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 40DEh
extern volatile uint8_t xdata g_rw_pdf_00E0h_gp29_acc_low_th;                                            // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 40E0h

extern volatile uint8_t xdata g_rw_pdf_00E1h_hgh_gray_th0;                                               // [msb:lsb] = [7:0], val = 100, 	Absolute Address = 40E1h
extern volatile uint8_t xdata g_rw_pdf_00E2h_hgh_gray_th1;                                               // [msb:lsb] = [7:0], val = 101, 	Absolute Address = 40E2h
extern volatile uint8_t xdata g_rw_pdf_00E3h_hgh_gray_th2;                                               // [msb:lsb] = [7:0], val = 102, 	Absolute Address = 40E3h
extern volatile uint8_t xdata g_rw_pdf_00E4h_hgh_gray_th3;                                               // [msb:lsb] = [7:0], val = 103, 	Absolute Address = 40E4h
extern volatile uint8_t xdata g_rw_pdf_00E5h_hgh_gray_th4;                                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 40E5h

extern volatile uint8_t xdata g_rw_pdf_00E6h_hgh_gray_th5;                                               // [msb:lsb] = [7:0], val = 105, 	Absolute Address = 40E6h
extern volatile uint8_t xdata g_rw_pdf_00E7h_hgh_gray_th6;                                               // [msb:lsb] = [7:0], val = 106, 	Absolute Address = 40E7h
extern volatile uint8_t xdata g_rw_pdf_00E8h_hgh_gray_th7;                                               // [msb:lsb] = [7:0], val = 107, 	Absolute Address = 40E8h
extern volatile uint8_t xdata g_rw_pdf_00E9h_low_gray_th0;                                               // [msb:lsb] = [7:0], val = 20, 	Absolute Address = 40E9h
extern volatile uint8_t xdata g_rw_pdf_00EAh_low_gray_th1;                                               // [msb:lsb] = [7:0], val = 21, 	Absolute Address = 40EAh

extern volatile uint8_t xdata g_rw_pdf_00EBh_low_gray_th2;                                               // [msb:lsb] = [7:0], val = 22, 	Absolute Address = 40EBh
extern volatile uint8_t xdata g_rw_pdf_00ECh_low_gray_th3;                                               // [msb:lsb] = [7:0], val = 23, 	Absolute Address = 40ECh
extern volatile uint8_t xdata g_rw_pdf_00EDh_low_gray_th4;                                               // [msb:lsb] = [7:0], val = 24, 	Absolute Address = 40EDh
extern volatile uint8_t xdata g_rw_pdf_00EEh_low_gray_th5;                                               // [msb:lsb] = [7:0], val = 25, 	Absolute Address = 40EEh
extern volatile uint8_t xdata g_rw_pdf_00EFh_low_gray_th6;                                               // [msb:lsb] = [7:0], val = 26, 	Absolute Address = 40EFh

extern volatile uint8_t xdata g_rw_pdf_00F0h_low_gray_th7;                                               // [msb:lsb] = [7:0], val = 27, 	Absolute Address = 40F0h
extern volatile uint8_t xdata g_rw_pdf_016Ah_gp12_pat_no9;                                               // [msb:lsb] = [10:3], val = 7, 	Absolute Address = 416Ah
extern volatile uint8_t xdata g_rw_pdf_016Dh_gp12_pat_no11;                                              // [msb:lsb] = [10:3], val = 85, 	Absolute Address = 416Dh
extern volatile uint8_t xdata g_rw_pdf_0170h_gp12_pat_no13;                                              // [msb:lsb] = [10:3], val = 7, 	Absolute Address = 4170h
extern volatile uint8_t xdata g_rw_pdf_0173h_gp12_pat_no15;                                              // [msb:lsb] = [10:3], val = 85, 	Absolute Address = 4173h

extern volatile uint8_t xdata g_rw_pdf_018Bh_region_width00;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 418Bh
extern volatile uint8_t xdata g_rw_pdf_0193h_region_width07;                                             // [msb:lsb] = [8:1], val = 0, 	Absolute Address = 4193h
extern volatile uint8_t xdata g_rw_pdf_0194h_region_width08;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 4194h
extern volatile uint8_t xdata g_rw_pdf_019Ch_region_width15;                                             // [msb:lsb] = [8:1], val = 0, 	Absolute Address = 419Ch
extern volatile uint8_t xdata g_rw_pdf_019Dh_region_width16;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 419Dh


#endif