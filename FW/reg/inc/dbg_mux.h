#ifndef _DBG_MUX_H_
#define _DBG_MUX_H_
#include "reg_include.h"

union rw_dbg_mux_000Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_mcu_top_dbgo_sel                                                     : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_dbgo_separate_sel_en                                                 : 1;        // val = 0
        unsigned char r_romscan_en                                                           : 1;        // val = 0
    }bits;
};
extern volatile union rw_dbg_mux_000Ah xdata g_rw_dbg_mux_000Ah;    // Absolute Address = F80Ah

union rw_dbg_mux_000Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_inproc_dbgo_sel                                                      : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char reserved1                                                              : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_dbg_mux_000Bh xdata g_rw_dbg_mux_000Bh;    // Absolute Address = F80Bh

union rw_dbg_mux_000Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_aging_dbgo_sel                                                       : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char reserved2                                                              : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_dbg_mux_000Ch xdata g_rw_dbg_mux_000Ch;    // Absolute Address = F80Ch

union rw_dbg_mux_000Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_dataflow_dbgo_sel                                                    : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char reserved3                                                              : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_dbg_mux_000Dh xdata g_rw_dbg_mux_000Dh;    // Absolute Address = F80Dh

union rw_dbg_mux_000Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_patdet_dbgo_sel                                                      : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char reserved4                                                              : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_dbg_mux_000Eh xdata g_rw_dbg_mux_000Eh;    // Absolute Address = F80Eh

//------------------------------------------------------------------------------------------------------------------------
union rw_dbg_mux_000Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_vrr_dbgo_sel                                                         : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char reserved5                                                              : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_dbg_mux_000Fh xdata g_rw_dbg_mux_000Fh;    // Absolute Address = F80Fh

union rw_dbg_mux_0010h
{
    unsigned char byte;
    struct
    {
        unsigned char r_frc_dbgo_sel                                                         : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char reserved6                                                              : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_dbg_mux_0010h xdata g_rw_dbg_mux_0010h;    // Absolute Address = F810h

union rw_dbg_mux_0011h
{
    unsigned char byte;
    struct
    {
        unsigned char r_lxlod_dbgo_sel                                                       : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char reserved7                                                              : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_dbg_mux_0011h xdata g_rw_dbg_mux_0011h;    // Absolute Address = F811h

union rw_dbg_mux_0012h
{
    unsigned char byte;
    struct
    {
        unsigned char r_outproc_dbgo_sel                                                     : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char reserved8                                                              : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_dbg_mux_0012h xdata g_rw_dbg_mux_0012h;    // Absolute Address = F812h

union rw_dbg_mux_0013h
{
    unsigned char byte;
    struct
    {
        unsigned char r_p2p_dbgo_sel                                                         : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char reserved9                                                              : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_dbg_mux_0013h xdata g_rw_dbg_mux_0013h;    // Absolute Address = F813h

//------------------------------------------------------------------------------------------------------------------------
union rw_dbg_mux_0014h
{
    unsigned char byte;
    struct
    {
        unsigned char r_ctrl_signal_dbgo_sel                                                 : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char reserved10                                                             : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_dbg_mux_0014h xdata g_rw_dbg_mux_0014h;    // Absolute Address = F814h

union rw_dbg_mux_0015h
{
    unsigned char byte;
    struct
    {
        unsigned char r_aip_proc_dbgo_sel                                                    : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char reserved11                                                             : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_dbg_mux_0015h xdata g_rw_dbg_mux_0015h;    // Absolute Address = F815h

union rw_dbg_mux_0016h
{
    unsigned char byte;
    struct
    {
        unsigned char r_ip_dbgo_sel                                                          : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char reserved12                                                             : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_dbg_mux_0016h xdata g_rw_dbg_mux_0016h;    // Absolute Address = F816h

union rw_dbg_mux_0017h
{
    unsigned char byte;
    struct
    {
        unsigned char r_sys_dbgo_sel                                                         : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char reserved13                                                             : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_dbg_mux_0017h xdata g_rw_dbg_mux_0017h;    // Absolute Address = F817h

union rw_dbg_mux_0018h
{
    unsigned char byte;
    struct
    {
        unsigned char r_aip_dbg_sel                                                          : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char reserved14                                                             : 1;        // val = 0
        unsigned char r_tx_clk_bypin_sel_sscg                                                : 1;        // val = 0
        unsigned char r_tx_clk_bypin_sel_sys                                                 : 1;        // val = 0
        unsigned char r_tx_clk_bypin_sel_sys2                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_dbg_mux_0018h xdata g_rw_dbg_mux_0018h;    // Absolute Address = F818h

//------------------------------------------------------------------------------------------------------------------------
union rw_dbg_mux_001Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_dbg_reserve3                                                         : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_efuse_unlock_key                                                     : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_dbg_mux_001Ch xdata g_rw_dbg_mux_001Ch;    // Absolute Address = F81Ch

union rw_dbg_mux_001Fh
{
    unsigned char byte;
    struct
    {
        unsigned char rx_chksum_flag_i                                                       : 1;        // val = 0
        unsigned char rx_chksum_event_i                                                      : 1;        // val = 0
        unsigned char mil_chksum_flag_i                                                      : 1;        // val = 0
        unsigned char mil_chksum_event_i                                                     : 1;        // val = 0
        unsigned char ctrl_chksum_flag_i                                                     : 1;        // val = 0
        unsigned char mbist_done_g_bits_2_0                                                  : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_dbg_mux_001Fh xdata g_rw_dbg_mux_001Fh;    // Absolute Address = F81Fh

union rw_dbg_mux_0020h
{
    unsigned char byte;
    struct
    {
        unsigned char mbist_done_g_bits_8_3                                                  : 6;        // [msb:lsb] = [8:3], val = 0
        unsigned char mbist_any_fail                                                         : 1;        // val = 0
        unsigned char mbist_all_done                                                         : 1;        // val = 0
    }bits;
};
extern volatile union rw_dbg_mux_0020h xdata g_rw_dbg_mux_0020h;    // Absolute Address = F820h

union rw_dbg_mux_0021h
{
    unsigned char byte;
    struct
    {
        unsigned char mbist_fail_g                                                           : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char reserved15                                                             : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_dbg_mux_0021h xdata g_rw_dbg_mux_0021h;    // Absolute Address = F821h


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_dbg_mux_0000h_sw_rst;                                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F800h
extern volatile uint8_t xdata g_rw_dbg_mux_0001h_manual_rstn;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F801h
extern volatile uint8_t xdata g_rw_dbg_mux_0002h_manual_mcu_rstn;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F802h
extern volatile uint8_t xdata g_rw_dbg_mux_0003h_manual_rx_rstn;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F803h
extern volatile uint8_t xdata g_rw_dbg_mux_0004h_manual_dclk_rstn;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F804h

extern volatile uint8_t xdata g_rw_dbg_mux_0005h_manual_ctrl_rstn;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F805h
extern volatile uint8_t xdata g_rw_dbg_mux_0006h_manual_rdclk_rstn;                                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F806h
extern volatile uint8_t xdata g_rw_dbg_mux_0007h_manual_mclk_rstn;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F807h
extern volatile uint8_t xdata g_rw_dbg_mux_0008h_test_mode;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F808h
extern volatile uint8_t xdata g_rw_dbg_mux_0009h_mbist_start;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F809h

extern volatile uint8_t xdata g_rw_dbg_mux_0019h_dbg_reserve0;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F819h
extern volatile uint8_t xdata g_rw_dbg_mux_001Ah_dbg_reserve1;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F81Ah
extern volatile uint8_t xdata g_rw_dbg_mux_001Bh_dbg_reserve2;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F81Bh
extern volatile uint8_t xdata g_rw_dbg_mux_001Dh_rx_chksum_dt_i;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F81Dh
extern volatile uint8_t xdata g_rw_dbg_mux_001Eh_rx_chksum_dt_i;                                         // [msb:lsb] = [15:8], val = 0, 	Absolute Address = F81Eh

extern volatile uint8_t xdata g_rw_dbg_mux_0022h_rom_misr_dataout;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F822h
extern volatile uint8_t xdata g_rw_dbg_mux_0023h_rom_misr_dataout;                                       // [msb:lsb] = [15:8], val = 0, 	Absolute Address = F823h
extern volatile uint8_t xdata g_rw_dbg_mux_0024h_rom_misr_dataout;                                       // [msb:lsb] = [23:16], val = 0, 	Absolute Address = F824h
extern volatile uint8_t xdata g_rw_dbg_mux_0025h_rom_misr_dataout;                                       // [msb:lsb] = [31:24], val = 0, 	Absolute Address = F825h
extern volatile uint8_t xdata g_rw_dbg_mux_0026h_rom_misr_dataout;                                       // [msb:lsb] = [39:32], val = 0, 	Absolute Address = F826h

extern volatile uint8_t xdata g_rw_dbg_mux_0027h_rom_misr_dataout;                                       // [msb:lsb] = [47:40], val = 0, 	Absolute Address = F827h
extern volatile uint8_t xdata g_rw_dbg_mux_0028h_rom_misr_dataout;                                       // [msb:lsb] = [55:48], val = 0, 	Absolute Address = F828h
extern volatile uint8_t xdata g_rw_dbg_mux_0029h_rom_misr_dataout;                                       // [msb:lsb] = [63:56], val = 0, 	Absolute Address = F829h
extern volatile uint8_t xdata g_rw_dbg_mux_002Ah_mcu_top_dbgr_1;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F82Ah
extern volatile uint8_t xdata g_rw_dbg_mux_002Bh_mcu_top_dbgr_2;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F82Bh

extern volatile uint8_t xdata g_rw_dbg_mux_002Ch_mcu_top_dbgr_3;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F82Ch
extern volatile uint8_t xdata g_rw_dbg_mux_002Dh_mcu_top_dbgr_4;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F82Dh
extern volatile uint8_t xdata g_rw_dbg_mux_002Eh_mcu_top_dbgr_5;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F82Eh
extern volatile uint8_t xdata g_rw_dbg_mux_002Fh_mcu_top_dbgr_6;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F82Fh
extern volatile uint8_t xdata g_rw_dbg_mux_0030h_mcu_top_dbgr_7;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F830h

extern volatile uint8_t xdata g_rw_dbg_mux_0031h_mcu_top_dbgr_8;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F831h
extern volatile uint8_t xdata g_rw_dbg_mux_0032h_mcu_top_dbgr_9;                                         // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F832h
extern volatile uint8_t xdata g_rw_dbg_mux_0033h_mcu_top_dbgr_10;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F833h
extern volatile uint8_t xdata g_rw_dbg_mux_0034h_mcu_top_dbgr_11;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F834h
extern volatile uint8_t xdata g_rw_dbg_mux_0035h_mcu_top_dbgr_12;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F835h

extern volatile uint8_t xdata g_rw_dbg_mux_0036h_mcu_top_dbgr_13;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F836h
extern volatile uint8_t xdata g_rw_dbg_mux_0037h_mcu_top_dbgr_14;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F837h
extern volatile uint8_t xdata g_rw_dbg_mux_0038h_mcu_top_dbgr_15;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F838h
extern volatile uint8_t xdata g_rw_dbg_mux_0039h_mcu_top_dbgr_16;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F839h
extern volatile uint8_t xdata g_rw_dbg_mux_003Ah_mcu_top_dbgr_17;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F83Ah

extern volatile uint8_t xdata g_rw_dbg_mux_003Bh_mcu_top_dbgr_18;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F83Bh
extern volatile uint8_t xdata g_rw_dbg_mux_003Ch_mcu_top_dbgr_19;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F83Ch
extern volatile uint8_t xdata g_rw_dbg_mux_003Dh_mcu_top_dbgr_20;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F83Dh
extern volatile uint8_t xdata g_rw_dbg_mux_003Eh_mcu_top_dbgr_21;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F83Eh
extern volatile uint8_t xdata g_rw_dbg_mux_003Fh_mcu_top_dbgr_22;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F83Fh

extern volatile uint8_t xdata g_rw_dbg_mux_0040h_mcu_top_dbgr_23;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F840h
extern volatile uint8_t xdata g_rw_dbg_mux_0041h_mcu_top_dbgr_24;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F841h
extern volatile uint8_t xdata g_rw_dbg_mux_0042h_indbgr_0;                                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F842h
extern volatile uint8_t xdata g_rw_dbg_mux_0043h_indbgr_1;                                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F843h
extern volatile uint8_t xdata g_rw_dbg_mux_0044h_indbgr_2;                                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F844h

extern volatile uint8_t xdata g_rw_dbg_mux_0045h_indbgr_3;                                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F845h
extern volatile uint8_t xdata g_rw_dbg_mux_0046h_indbgr_4;                                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F846h
extern volatile uint8_t xdata g_rw_dbg_mux_0047h_indbgr_5;                                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F847h
extern volatile uint8_t xdata g_rw_dbg_mux_0048h_indbgr_6;                                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F848h
extern volatile uint8_t xdata g_rw_dbg_mux_0049h_indbgr_7;                                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F849h

extern volatile uint8_t xdata g_rw_dbg_mux_004Ah_indbgr_8;                                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F84Ah
extern volatile uint8_t xdata g_rw_dbg_mux_004Bh_indbgr_9;                                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F84Bh
extern volatile uint8_t xdata g_rw_dbg_mux_004Ch_indbgr_10;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F84Ch
extern volatile uint8_t xdata g_rw_dbg_mux_004Dh_indbgr_11;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F84Dh
extern volatile uint8_t xdata g_rw_dbg_mux_004Eh_indbgr_12;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F84Eh

extern volatile uint8_t xdata g_rw_dbg_mux_004Fh_indbgr_13;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F84Fh
extern volatile uint8_t xdata g_rw_dbg_mux_0050h_indbgr_14;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F850h
extern volatile uint8_t xdata g_rw_dbg_mux_0051h_indbgr_15;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F851h
extern volatile uint8_t xdata g_rw_dbg_mux_0052h_indbgr_16;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F852h
extern volatile uint8_t xdata g_rw_dbg_mux_0053h_indbgr_17;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F853h

extern volatile uint8_t xdata g_rw_dbg_mux_0054h_aging_dbgr_0;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F854h
extern volatile uint8_t xdata g_rw_dbg_mux_0055h_aging_dbgr_1;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F855h
extern volatile uint8_t xdata g_rw_dbg_mux_0056h_aging_dbgr_2;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F856h
extern volatile uint8_t xdata g_rw_dbg_mux_0057h_aging_dbgr_3;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F857h
extern volatile uint8_t xdata g_rw_dbg_mux_0058h_aging_dbgr_4;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F858h

extern volatile uint8_t xdata g_rw_dbg_mux_0059h_aging_dbgr_5;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F859h
extern volatile uint8_t xdata g_rw_dbg_mux_005Ah_aging_dbgr_6;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F85Ah
extern volatile uint8_t xdata g_rw_dbg_mux_005Bh_aging_dbgr_7;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F85Bh
extern volatile uint8_t xdata g_rw_dbg_mux_005Ch_patdet_dbgr_0;                                          // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F85Ch
extern volatile uint8_t xdata g_rw_dbg_mux_005Dh_dataflow_dbgr_0;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F85Dh

extern volatile uint8_t xdata g_rw_dbg_mux_005Eh_dataflow_dbgr_1;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F85Eh
extern volatile uint8_t xdata g_rw_dbg_mux_005Fh_dataflow_dbgr_2;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F85Fh
extern volatile uint8_t xdata g_rw_dbg_mux_0060h_dataflow_dbgr_3;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F860h
extern volatile uint8_t xdata g_rw_dbg_mux_0061h_dataflow_dbgr_4;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F861h
extern volatile uint8_t xdata g_rw_dbg_mux_0062h_dataflow_dbgr_5;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F862h

extern volatile uint8_t xdata g_rw_dbg_mux_0063h_dataflow_dbgr_6;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F863h
extern volatile uint8_t xdata g_rw_dbg_mux_0064h_dataflow_dbgr_7;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F864h
extern volatile uint8_t xdata g_rw_dbg_mux_0065h_dataflow_dbgr_8;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F865h
extern volatile uint8_t xdata g_rw_dbg_mux_0066h_dataflow_dbgr_9;                                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F866h
extern volatile uint8_t xdata g_rw_dbg_mux_0067h_dataflow_dbgr_10;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F867h

extern volatile uint8_t xdata g_rw_dbg_mux_0068h_dataflow_dbgr_11;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F868h
extern volatile uint8_t xdata g_rw_dbg_mux_0069h_dataflow_dbgr_12;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F869h
extern volatile uint8_t xdata g_rw_dbg_mux_006Ah_dataflow_dbgr_13;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F86Ah
extern volatile uint8_t xdata g_rw_dbg_mux_006Bh_dataflow_dbgr_14;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F86Bh
extern volatile uint8_t xdata g_rw_dbg_mux_006Ch_dataflow_dbgr_15;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F86Ch

extern volatile uint8_t xdata g_rw_dbg_mux_006Dh_dataflow_dbgr_16;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F86Dh
extern volatile uint8_t xdata g_rw_dbg_mux_006Eh_outdbgr_0;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F86Eh
extern volatile uint8_t xdata g_rw_dbg_mux_006Fh_outdbgr_1;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F86Fh
extern volatile uint8_t xdata g_rw_dbg_mux_0070h_outdbgr_2;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F870h
extern volatile uint8_t xdata g_rw_dbg_mux_0071h_outdbgr_3;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F871h

extern volatile uint8_t xdata g_rw_dbg_mux_0072h_outdbgr_4;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F872h
extern volatile uint8_t xdata g_rw_dbg_mux_0073h_outdbgr_5;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F873h
extern volatile uint8_t xdata g_rw_dbg_mux_0074h_outdbgr_6;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F874h
extern volatile uint8_t xdata g_rw_dbg_mux_0075h_outdbgr_7;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F875h
extern volatile uint8_t xdata g_rw_dbg_mux_0076h_p2p_dbgr_0;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F876h

extern volatile uint8_t xdata g_rw_dbg_mux_0077h_p2p_dbgr_1;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F877h
extern volatile uint8_t xdata g_rw_dbg_mux_0078h_p2p_dbgr_2;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F878h
extern volatile uint8_t xdata g_rw_dbg_mux_0079h_p2p_dbgr_3;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F879h
extern volatile uint8_t xdata g_rw_dbg_mux_007Ah_p2p_dbgr_4;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F87Ah
extern volatile uint8_t xdata g_rw_dbg_mux_007Bh_p2p_dbgr_5;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F87Bh

extern volatile uint8_t xdata g_rw_dbg_mux_007Ch_p2p_dbgr_6;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F87Ch
extern volatile uint8_t xdata g_rw_dbg_mux_007Dh_p2p_dbgr_7;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F87Dh
extern volatile uint8_t xdata g_rw_dbg_mux_007Eh_ctrl_signal_dbgr_0;                                     // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F87Eh
extern volatile uint8_t xdata g_rw_dbg_mux_007Fh_ctrl_signal_dbgr_1;                                     // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F87Fh
extern volatile uint8_t xdata g_rw_dbg_mux_0080h_ctrl_signal_dbgr_2;                                     // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F880h

extern volatile uint8_t xdata g_rw_dbg_mux_0081h_ctrl_signal_dbgr_3;                                     // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F881h
extern volatile uint8_t xdata g_rw_dbg_mux_0082h_ctrl_signal_dbgr_4;                                     // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F882h
extern volatile uint8_t xdata g_rw_dbg_mux_0083h_ctrl_signal_dbgr_5;                                     // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F883h
extern volatile uint8_t xdata g_rw_dbg_mux_0084h_ctrl_signal_dbgr_6;                                     // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F884h
extern volatile uint8_t xdata g_rw_dbg_mux_0085h_ctrl_signal_dbgr_7;                                     // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F885h

extern volatile uint8_t xdata g_rw_dbg_mux_0086h_aip_dbgr_0;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F886h
extern volatile uint8_t xdata g_rw_dbg_mux_0087h_aip_dbgr_1;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F887h
extern volatile uint8_t xdata g_rw_dbg_mux_0088h_aip_dbgr_2;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F888h
extern volatile uint8_t xdata g_rw_dbg_mux_0089h_aip_dbgr_3;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F889h
extern volatile uint8_t xdata g_rw_dbg_mux_008Ah_aip_dbgr_4;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F88Ah

extern volatile uint8_t xdata g_rw_dbg_mux_008Bh_ro_dbgr_reserve0;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F88Bh
extern volatile uint8_t xdata g_rw_dbg_mux_008Ch_ro_dbgr_reserve1;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = F88Ch

#endif