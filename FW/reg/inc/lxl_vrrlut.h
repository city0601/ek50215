#ifndef _LXL_VRRLUT_H_
#define _LXL_VRRLUT_H_
#include "reg_include.h"


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0000h_LXL_LUT0_data_0;                                     // [msb:lsb] = [7:0], val = 90, 	Absolute Address = 9400h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0001h_LXL_LUT0_data_1;                                     // [msb:lsb] = [7:0], val = 85, 	Absolute Address = 9401h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0002h_LXL_LUT0_data_2;                                     // [msb:lsb] = [7:0], val = 45, 	Absolute Address = 9402h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0003h_LXL_LUT0_data_3;                                     // [msb:lsb] = [7:0], val = 42, 	Absolute Address = 9403h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0004h_LXL_LUT0_data_4;                                     // [msb:lsb] = [7:0], val = 30, 	Absolute Address = 9404h

extern volatile uint8_t xdata g_rw_lxl_vrrlut_0005h_LXL_LUT0_data_5;                                     // [msb:lsb] = [7:0], val = 28, 	Absolute Address = 9405h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0006h_LXL_LUT0_data_6;                                     // [msb:lsb] = [7:0], val = 22, 	Absolute Address = 9406h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0007h_LXL_LUT0_data_7;                                     // [msb:lsb] = [7:0], val = 149, 	Absolute Address = 9407h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0008h_LXL_LUT0_data_8;                                     // [msb:lsb] = [7:0], val = 18, 	Absolute Address = 9408h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0009h_LXL_LUT0_data_9;                                     // [msb:lsb] = [7:0], val = 17, 	Absolute Address = 9409h

extern volatile uint8_t xdata g_rw_lxl_vrrlut_000Ah_LXL_LUT0_data_10;                                    // [msb:lsb] = [7:0], val = 15, 	Absolute Address = 940Ah
extern volatile uint8_t xdata g_rw_lxl_vrrlut_000Bh_LXL_LUT0_data_11;                                    // [msb:lsb] = [7:0], val = 14, 	Absolute Address = 940Bh
extern volatile uint8_t xdata g_rw_lxl_vrrlut_000Ch_LXL_LUT0_data_12;                                    // [msb:lsb] = [7:0], val = 12, 	Absolute Address = 940Ch
extern volatile uint8_t xdata g_rw_lxl_vrrlut_000Dh_LXL_LUT0_data_13;                                    // [msb:lsb] = [7:0], val = 231, 	Absolute Address = 940Dh
extern volatile uint8_t xdata g_rw_lxl_vrrlut_000Eh_LXL_LUT0_data_14;                                    // [msb:lsb] = [7:0], val = 11, 	Absolute Address = 940Eh

extern volatile uint8_t xdata g_rw_lxl_vrrlut_000Fh_LXL_LUT0_data_15;                                    // [msb:lsb] = [7:0], val = 74, 	Absolute Address = 940Fh
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0010h_LXL_LUT0_data_16;                                    // [msb:lsb] = [7:0], val = 10, 	Absolute Address = 9410h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0011h_LXL_LUT0_data_17;                                    // [msb:lsb] = [7:0], val = 9, 	Absolute Address = 9411h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0012h_LXL_LUT0_data_18;                                    // [msb:lsb] = [7:0], val = 9, 	Absolute Address = 9412h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0013h_LXL_LUT0_data_19;                                    // [msb:lsb] = [7:0], val = 8, 	Absolute Address = 9413h

extern volatile uint8_t xdata g_rw_lxl_vrrlut_0014h_LXL_LUT0_data_20;                                    // [msb:lsb] = [7:0], val = 8, 	Absolute Address = 9414h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0015h_LXL_LUT0_data_21;                                    // [msb:lsb] = [7:0], val = 54, 	Absolute Address = 9415h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0016h_LXL_LUT0_data_22;                                    // [msb:lsb] = [7:0], val = 7, 	Absolute Address = 9416h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0017h_LXL_LUT0_data_23;                                    // [msb:lsb] = [7:0], val = 135, 	Absolute Address = 9417h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0018h_LXL_LUT0_data_24;                                    // [msb:lsb] = [7:0], val = 6, 	Absolute Address = 9418h

extern volatile uint8_t xdata g_rw_lxl_vrrlut_0019h_LXL_LUT0_data_25;                                    // [msb:lsb] = [7:0], val = 242, 	Absolute Address = 9419h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_001Ah_LXL_LUT0_data_26;                                    // [msb:lsb] = [7:0], val = 6, 	Absolute Address = 941Ah
extern volatile uint8_t xdata g_rw_lxl_vrrlut_001Bh_LXL_LUT0_data_27;                                    // [msb:lsb] = [7:0], val = 115, 	Absolute Address = 941Bh
extern volatile uint8_t xdata g_rw_lxl_vrrlut_001Ch_LXL_LUT0_data_28;                                    // [msb:lsb] = [7:0], val = 6, 	Absolute Address = 941Ch
extern volatile uint8_t xdata g_rw_lxl_vrrlut_001Dh_LXL_LUT0_data_29;                                    // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 941Dh

extern volatile uint8_t xdata g_rw_lxl_vrrlut_001Eh_LXL_LUT0_data_30;                                    // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 941Eh
extern volatile uint8_t xdata g_rw_lxl_vrrlut_001Fh_LXL_LUT0_data_31;                                    // [msb:lsb] = [7:0], val = 165, 	Absolute Address = 941Fh
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0020h_LXL_LUT0_data_32;                                    // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 9420h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0021h_LXL_LUT0_data_33;                                    // [msb:lsb] = [7:0], val = 80, 	Absolute Address = 9421h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0022h_LXL_LUT0_data_34;                                    // [msb:lsb] = [7:0], val = 5, 	Absolute Address = 9422h

extern volatile uint8_t xdata g_rw_lxl_vrrlut_0023h_LXL_LUT0_data_35;                                    // [msb:lsb] = [7:0], val = 4, 	Absolute Address = 9423h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0024h_LXL_LUT0_data_36;                                    // [msb:lsb] = [7:0], val = 22, 	Absolute Address = 9424h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0025h_LXL_LUT0_data_37;                                    // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 9425h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0026h_LXL_LUT0_data_38;                                    // [msb:lsb] = [7:0], val = 4, 	Absolute Address = 9426h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0027h_LXL_LUT0_data_39;                                    // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 9427h

extern volatile uint8_t xdata g_rw_lxl_vrrlut_0028h_LXL_LUT0_data_40;                                    // [msb:lsb] = [7:0], val = 32, 	Absolute Address = 9428h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0029h_LXL_LUT0_data_41;                                    // [msb:lsb] = [7:0], val = 14, 	Absolute Address = 9429h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_002Ah_LXL_LUT0_data_42;                                    // [msb:lsb] = [7:0], val = 84, 	Absolute Address = 942Ah
extern volatile uint8_t xdata g_rw_lxl_vrrlut_002Bh_LXL_LUT0_data_43;                                    // [msb:lsb] = [7:0], val = 177, 	Absolute Address = 942Bh
extern volatile uint8_t xdata g_rw_lxl_vrrlut_002Ch_LXL_LUT0_data_44;                                    // [msb:lsb] = [7:0], val = 29, 	Absolute Address = 942Ch

extern volatile uint8_t xdata g_rw_lxl_vrrlut_002Dh_LXL_LUT0_data_45;                                    // [msb:lsb] = [7:0], val = 122, 	Absolute Address = 942Dh
extern volatile uint8_t xdata g_rw_lxl_vrrlut_002Eh_LXL_LUT0_data_46;                                    // [msb:lsb] = [7:0], val = 2, 	Absolute Address = 942Eh
extern volatile uint8_t xdata g_rw_lxl_vrrlut_002Fh_LXL_LUT0_data_47;                                    // [msb:lsb] = [7:0], val = 51, 	Absolute Address = 942Fh
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0030h_LXL_LUT0_data_48;                                    // [msb:lsb] = [7:0], val = 252, 	Absolute Address = 9430h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0031h_LXL_LUT0_data_49;                                    // [msb:lsb] = [7:0], val = 3, 	Absolute Address = 9431h

extern volatile uint8_t xdata g_rw_lxl_vrrlut_0032h_LXL_LUT0_data_50;                                    // [msb:lsb] = [7:0], val = 78, 	Absolute Address = 9432h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0033h_LXL_LUT0_data_51;                                    // [msb:lsb] = [7:0], val = 217, 	Absolute Address = 9433h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0034h_LXL_LUT0_data_52;                                    // [msb:lsb] = [7:0], val = 245, 	Absolute Address = 9434h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0035h_LXL_LUT0_data_53;                                    // [msb:lsb] = [7:0], val = 109, 	Absolute Address = 9435h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0036h_LXL_LUT0_data_54;                                    // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 9436h

extern volatile uint8_t xdata g_rw_lxl_vrrlut_0037h_LXL_LUT0_data_55;                                    // [msb:lsb] = [7:0], val = 248, 	Absolute Address = 9437h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0038h_LXL_LUT0_data_56;                                    // [msb:lsb] = [7:0], val = 148, 	Absolute Address = 9438h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0039h_LXL_LUT0_data_57;                                    // [msb:lsb] = [7:0], val = 170, 	Absolute Address = 9439h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_003Ah_LXL_LUT0_data_58;                                    // [msb:lsb] = [7:0], val = 202, 	Absolute Address = 943Ah
extern volatile uint8_t xdata g_rw_lxl_vrrlut_003Bh_LXL_LUT0_data_59;                                    // [msb:lsb] = [7:0], val = 192, 	Absolute Address = 943Bh

extern volatile uint8_t xdata g_rw_lxl_vrrlut_003Ch_LXL_LUT0_data_60;                                    // [msb:lsb] = [7:0], val = 121, 	Absolute Address = 943Ch
extern volatile uint8_t xdata g_rw_lxl_vrrlut_003Dh_LXL_LUT0_data_61;                                    // [msb:lsb] = [7:0], val = 13, 	Absolute Address = 943Dh
extern volatile uint8_t xdata g_rw_lxl_vrrlut_003Eh_LXL_LUT0_data_62;                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 943Eh
extern volatile uint8_t xdata g_rw_lxl_vrrlut_003Fh_LXL_LUT0_data_63;                                    // [msb:lsb] = [7:0], val = 48, 	Absolute Address = 943Fh
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0040h_LXL_LUT0_data_64;                                    // [msb:lsb] = [7:0], val = 48, 	Absolute Address = 9440h

extern volatile uint8_t xdata g_rw_lxl_vrrlut_0041h_LXL_LUT0_data_65;                                    // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 9441h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0042h_LXL_LUT0_data_66;                                    // [msb:lsb] = [7:0], val = 80, 	Absolute Address = 9442h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0043h_LXL_LUT0_data_67;                                    // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 9443h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0044h_LXL_LUT0_data_68;                                    // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 9444h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0045h_LXL_LUT0_data_69;                                    // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9445h

extern volatile uint8_t xdata g_rw_lxl_vrrlut_0046h_LXL_LUT0_data_70;                                    // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 9446h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0047h_LXL_LUT0_data_71;                                    // [msb:lsb] = [7:0], val = 160, 	Absolute Address = 9447h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0048h_LXL_LUT0_data_72;                                    // [msb:lsb] = [7:0], val = 176, 	Absolute Address = 9448h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_0049h_LXL_LUT0_data_73;                                    // [msb:lsb] = [7:0], val = 192, 	Absolute Address = 9449h
extern volatile uint8_t xdata g_rw_lxl_vrrlut_004Ah_LXL_LUT0_data_74;                                    // [msb:lsb] = [7:0], val = 208, 	Absolute Address = 944Ah

extern volatile uint8_t xdata g_rw_lxl_vrrlut_004Bh_LXL_LUT0_data_75;                                    // [msb:lsb] = [7:0], val = 224, 	Absolute Address = 944Bh
extern volatile uint8_t xdata g_rw_lxl_vrrlut_004Ch_LXL_LUT0_data_76;                                    // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 944Ch
extern volatile uint8_t xdata g_rw_lxl_vrrlut_004Dh_LXL_LUT0_data_77;                                    // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 944Dh
extern volatile uint8_t xdata g_rw_lxl_vrrlut_004Eh_LXL_LUT0_data_78;                                    // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 944Eh
extern volatile uint8_t xdata g_rw_lxl_vrrlut_004Fh_LXL_LUT0_data_79;                                    // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 944Fh

extern volatile uint8_t xdata g_rw_lxl_vrrlut_0050h_LXL_LUT0_data_80;                                    // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 9450h

#endif