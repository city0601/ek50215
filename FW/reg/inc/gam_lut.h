#ifndef _GAM_LUT_H_
#define _GAM_LUT_H_
#include "reg_include.h"


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_gam_lut_0000h_GAM_LUT_0;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7800h
extern volatile uint8_t xdata g_rw_gam_lut_0001h_GAM_LUT_1;                                              // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7801h
extern volatile uint8_t xdata g_rw_gam_lut_0002h_GAM_LUT_2;                                              // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7802h
extern volatile uint8_t xdata g_rw_gam_lut_0003h_GAM_LUT_3;                                              // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7803h
extern volatile uint8_t xdata g_rw_gam_lut_0004h_GAM_LUT_4;                                              // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7804h

extern volatile uint8_t xdata g_rw_gam_lut_0005h_GAM_LUT_5;                                              // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7805h
extern volatile uint8_t xdata g_rw_gam_lut_0006h_GAM_LUT_6;                                              // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7806h
extern volatile uint8_t xdata g_rw_gam_lut_0007h_GAM_LUT_7;                                              // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7807h
extern volatile uint8_t xdata g_rw_gam_lut_0008h_GAM_LUT_8;                                              // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7808h
extern volatile uint8_t xdata g_rw_gam_lut_0009h_GAM_LUT_9;                                              // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7809h

extern volatile uint8_t xdata g_rw_gam_lut_000Ah_GAM_LUT_10;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 780Ah
extern volatile uint8_t xdata g_rw_gam_lut_000Bh_GAM_LUT_11;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 780Bh
extern volatile uint8_t xdata g_rw_gam_lut_000Ch_GAM_LUT_12;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 780Ch
extern volatile uint8_t xdata g_rw_gam_lut_000Dh_GAM_LUT_13;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 780Dh
extern volatile uint8_t xdata g_rw_gam_lut_000Eh_GAM_LUT_14;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 780Eh

extern volatile uint8_t xdata g_rw_gam_lut_000Fh_GAM_LUT_15;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 780Fh
extern volatile uint8_t xdata g_rw_gam_lut_0010h_GAM_LUT_16;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7810h
extern volatile uint8_t xdata g_rw_gam_lut_0011h_GAM_LUT_17;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7811h
extern volatile uint8_t xdata g_rw_gam_lut_0012h_GAM_LUT_18;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7812h
extern volatile uint8_t xdata g_rw_gam_lut_0013h_GAM_LUT_19;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7813h

extern volatile uint8_t xdata g_rw_gam_lut_0014h_GAM_LUT_20;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7814h
extern volatile uint8_t xdata g_rw_gam_lut_0015h_GAM_LUT_21;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7815h
extern volatile uint8_t xdata g_rw_gam_lut_0016h_GAM_LUT_22;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7816h
extern volatile uint8_t xdata g_rw_gam_lut_0017h_GAM_LUT_23;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7817h
extern volatile uint8_t xdata g_rw_gam_lut_0018h_GAM_LUT_24;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7818h

extern volatile uint8_t xdata g_rw_gam_lut_0019h_GAM_LUT_25;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7819h
extern volatile uint8_t xdata g_rw_gam_lut_001Ah_GAM_LUT_26;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 781Ah
extern volatile uint8_t xdata g_rw_gam_lut_001Bh_GAM_LUT_27;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 781Bh
extern volatile uint8_t xdata g_rw_gam_lut_001Ch_GAM_LUT_28;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 781Ch
extern volatile uint8_t xdata g_rw_gam_lut_001Dh_GAM_LUT_29;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 781Dh

extern volatile uint8_t xdata g_rw_gam_lut_001Eh_GAM_LUT_30;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 781Eh
extern volatile uint8_t xdata g_rw_gam_lut_001Fh_GAM_LUT_31;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 781Fh
extern volatile uint8_t xdata g_rw_gam_lut_0020h_GAM_LUT_32;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7820h
extern volatile uint8_t xdata g_rw_gam_lut_0021h_GAM_LUT_33;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7821h
extern volatile uint8_t xdata g_rw_gam_lut_0022h_GAM_LUT_34;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7822h

extern volatile uint8_t xdata g_rw_gam_lut_0023h_GAM_LUT_35;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7823h
extern volatile uint8_t xdata g_rw_gam_lut_0024h_GAM_LUT_36;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7824h
extern volatile uint8_t xdata g_rw_gam_lut_0025h_GAM_LUT_37;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7825h
extern volatile uint8_t xdata g_rw_gam_lut_0026h_GAM_LUT_38;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7826h
extern volatile uint8_t xdata g_rw_gam_lut_0027h_GAM_LUT_39;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7827h

extern volatile uint8_t xdata g_rw_gam_lut_0028h_GAM_LUT_40;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7828h
extern volatile uint8_t xdata g_rw_gam_lut_0029h_GAM_LUT_41;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7829h
extern volatile uint8_t xdata g_rw_gam_lut_002Ah_GAM_LUT_42;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 782Ah
extern volatile uint8_t xdata g_rw_gam_lut_002Bh_GAM_LUT_43;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 782Bh
extern volatile uint8_t xdata g_rw_gam_lut_002Ch_GAM_LUT_44;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 782Ch

extern volatile uint8_t xdata g_rw_gam_lut_002Dh_GAM_LUT_45;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 782Dh
extern volatile uint8_t xdata g_rw_gam_lut_002Eh_GAM_LUT_46;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 782Eh
extern volatile uint8_t xdata g_rw_gam_lut_002Fh_GAM_LUT_47;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 782Fh
extern volatile uint8_t xdata g_rw_gam_lut_0030h_GAM_LUT_48;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7830h
extern volatile uint8_t xdata g_rw_gam_lut_0031h_GAM_LUT_49;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7831h

extern volatile uint8_t xdata g_rw_gam_lut_0032h_GAM_LUT_50;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7832h
extern volatile uint8_t xdata g_rw_gam_lut_0033h_GAM_LUT_51;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7833h
extern volatile uint8_t xdata g_rw_gam_lut_0034h_GAM_LUT_52;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7834h
extern volatile uint8_t xdata g_rw_gam_lut_0035h_GAM_LUT_53;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7835h
extern volatile uint8_t xdata g_rw_gam_lut_0036h_GAM_LUT_54;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7836h

extern volatile uint8_t xdata g_rw_gam_lut_0037h_GAM_LUT_55;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7837h
extern volatile uint8_t xdata g_rw_gam_lut_0038h_GAM_LUT_56;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7838h
extern volatile uint8_t xdata g_rw_gam_lut_0039h_GAM_LUT_57;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7839h
extern volatile uint8_t xdata g_rw_gam_lut_003Ah_GAM_LUT_58;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 783Ah
extern volatile uint8_t xdata g_rw_gam_lut_003Bh_GAM_LUT_59;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 783Bh

extern volatile uint8_t xdata g_rw_gam_lut_003Ch_GAM_LUT_60;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 783Ch
extern volatile uint8_t xdata g_rw_gam_lut_003Dh_GAM_LUT_61;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 783Dh
extern volatile uint8_t xdata g_rw_gam_lut_003Eh_GAM_LUT_62;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 783Eh
extern volatile uint8_t xdata g_rw_gam_lut_003Fh_GAM_LUT_63;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 783Fh
extern volatile uint8_t xdata g_rw_gam_lut_0040h_GAM_LUT_64;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7840h

extern volatile uint8_t xdata g_rw_gam_lut_0041h_GAM_LUT_65;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7841h
extern volatile uint8_t xdata g_rw_gam_lut_0042h_GAM_LUT_66;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7842h
extern volatile uint8_t xdata g_rw_gam_lut_0043h_GAM_LUT_67;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7843h
extern volatile uint8_t xdata g_rw_gam_lut_0044h_GAM_LUT_68;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7844h
extern volatile uint8_t xdata g_rw_gam_lut_0045h_GAM_LUT_69;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7845h

extern volatile uint8_t xdata g_rw_gam_lut_0046h_GAM_LUT_70;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7846h
extern volatile uint8_t xdata g_rw_gam_lut_0047h_GAM_LUT_71;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7847h
extern volatile uint8_t xdata g_rw_gam_lut_0048h_GAM_LUT_72;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7848h
extern volatile uint8_t xdata g_rw_gam_lut_0049h_GAM_LUT_73;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7849h
extern volatile uint8_t xdata g_rw_gam_lut_004Ah_GAM_LUT_74;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 784Ah

extern volatile uint8_t xdata g_rw_gam_lut_004Bh_GAM_LUT_75;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 784Bh
extern volatile uint8_t xdata g_rw_gam_lut_004Ch_GAM_LUT_76;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 784Ch
extern volatile uint8_t xdata g_rw_gam_lut_004Dh_GAM_LUT_77;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 784Dh
extern volatile uint8_t xdata g_rw_gam_lut_004Eh_GAM_LUT_78;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 784Eh
extern volatile uint8_t xdata g_rw_gam_lut_004Fh_GAM_LUT_79;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 784Fh

extern volatile uint8_t xdata g_rw_gam_lut_0050h_GAM_LUT_80;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7850h
extern volatile uint8_t xdata g_rw_gam_lut_0051h_GAM_LUT_81;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7851h
extern volatile uint8_t xdata g_rw_gam_lut_0052h_GAM_LUT_82;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7852h
extern volatile uint8_t xdata g_rw_gam_lut_0053h_GAM_LUT_83;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7853h
extern volatile uint8_t xdata g_rw_gam_lut_0054h_GAM_LUT_84;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7854h

extern volatile uint8_t xdata g_rw_gam_lut_0055h_GAM_LUT_85;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7855h
extern volatile uint8_t xdata g_rw_gam_lut_0056h_GAM_LUT_86;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7856h
extern volatile uint8_t xdata g_rw_gam_lut_0057h_GAM_LUT_87;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7857h
extern volatile uint8_t xdata g_rw_gam_lut_0058h_GAM_LUT_88;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7858h
extern volatile uint8_t xdata g_rw_gam_lut_0059h_GAM_LUT_89;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7859h

extern volatile uint8_t xdata g_rw_gam_lut_005Ah_GAM_LUT_90;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 785Ah
extern volatile uint8_t xdata g_rw_gam_lut_005Bh_GAM_LUT_91;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 785Bh
extern volatile uint8_t xdata g_rw_gam_lut_005Ch_GAM_LUT_92;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 785Ch
extern volatile uint8_t xdata g_rw_gam_lut_005Dh_GAM_LUT_93;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 785Dh
extern volatile uint8_t xdata g_rw_gam_lut_005Eh_GAM_LUT_94;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 785Eh

extern volatile uint8_t xdata g_rw_gam_lut_005Fh_GAM_LUT_95;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 785Fh
extern volatile uint8_t xdata g_rw_gam_lut_0060h_GAM_LUT_96;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7860h
extern volatile uint8_t xdata g_rw_gam_lut_0061h_GAM_LUT_97;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7861h
extern volatile uint8_t xdata g_rw_gam_lut_0062h_GAM_LUT_98;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7862h
extern volatile uint8_t xdata g_rw_gam_lut_0063h_GAM_LUT_99;                                             // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7863h

extern volatile uint8_t xdata g_rw_gam_lut_0064h_GAM_LUT_100;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7864h
extern volatile uint8_t xdata g_rw_gam_lut_0065h_GAM_LUT_101;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7865h
extern volatile uint8_t xdata g_rw_gam_lut_0066h_GAM_LUT_102;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7866h
extern volatile uint8_t xdata g_rw_gam_lut_0067h_GAM_LUT_103;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7867h
extern volatile uint8_t xdata g_rw_gam_lut_0068h_GAM_LUT_104;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7868h

extern volatile uint8_t xdata g_rw_gam_lut_0069h_GAM_LUT_105;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7869h
extern volatile uint8_t xdata g_rw_gam_lut_006Ah_GAM_LUT_106;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 786Ah
extern volatile uint8_t xdata g_rw_gam_lut_006Bh_GAM_LUT_107;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 786Bh
extern volatile uint8_t xdata g_rw_gam_lut_006Ch_GAM_LUT_108;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 786Ch
extern volatile uint8_t xdata g_rw_gam_lut_006Dh_GAM_LUT_109;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 786Dh

extern volatile uint8_t xdata g_rw_gam_lut_006Eh_GAM_LUT_110;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 786Eh
extern volatile uint8_t xdata g_rw_gam_lut_006Fh_GAM_LUT_111;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 786Fh
extern volatile uint8_t xdata g_rw_gam_lut_0070h_GAM_LUT_112;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7870h
extern volatile uint8_t xdata g_rw_gam_lut_0071h_GAM_LUT_113;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7871h
extern volatile uint8_t xdata g_rw_gam_lut_0072h_GAM_LUT_114;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7872h

extern volatile uint8_t xdata g_rw_gam_lut_0073h_GAM_LUT_115;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7873h
extern volatile uint8_t xdata g_rw_gam_lut_0074h_GAM_LUT_116;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7874h
extern volatile uint8_t xdata g_rw_gam_lut_0075h_GAM_LUT_117;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7875h
extern volatile uint8_t xdata g_rw_gam_lut_0076h_GAM_LUT_118;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7876h
extern volatile uint8_t xdata g_rw_gam_lut_0077h_GAM_LUT_119;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7877h

extern volatile uint8_t xdata g_rw_gam_lut_0078h_GAM_LUT_120;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7878h
extern volatile uint8_t xdata g_rw_gam_lut_0079h_GAM_LUT_121;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7879h
extern volatile uint8_t xdata g_rw_gam_lut_007Ah_GAM_LUT_122;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 787Ah
extern volatile uint8_t xdata g_rw_gam_lut_007Bh_GAM_LUT_123;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 787Bh
extern volatile uint8_t xdata g_rw_gam_lut_007Ch_GAM_LUT_124;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 787Ch

extern volatile uint8_t xdata g_rw_gam_lut_007Dh_GAM_LUT_125;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 787Dh
extern volatile uint8_t xdata g_rw_gam_lut_007Eh_GAM_LUT_126;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 787Eh
extern volatile uint8_t xdata g_rw_gam_lut_007Fh_GAM_LUT_127;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 787Fh
extern volatile uint8_t xdata g_rw_gam_lut_0080h_GAM_LUT_128;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7880h
extern volatile uint8_t xdata g_rw_gam_lut_0081h_GAM_LUT_129;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7881h

extern volatile uint8_t xdata g_rw_gam_lut_0082h_GAM_LUT_130;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7882h
extern volatile uint8_t xdata g_rw_gam_lut_0083h_GAM_LUT_131;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7883h
extern volatile uint8_t xdata g_rw_gam_lut_0084h_GAM_LUT_132;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7884h
extern volatile uint8_t xdata g_rw_gam_lut_0085h_GAM_LUT_133;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7885h
extern volatile uint8_t xdata g_rw_gam_lut_0086h_GAM_LUT_134;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7886h

extern volatile uint8_t xdata g_rw_gam_lut_0087h_GAM_LUT_135;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7887h
extern volatile uint8_t xdata g_rw_gam_lut_0088h_GAM_LUT_136;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7888h
extern volatile uint8_t xdata g_rw_gam_lut_0089h_GAM_LUT_137;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7889h
extern volatile uint8_t xdata g_rw_gam_lut_008Ah_GAM_LUT_138;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 788Ah
extern volatile uint8_t xdata g_rw_gam_lut_008Bh_GAM_LUT_139;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 788Bh

extern volatile uint8_t xdata g_rw_gam_lut_008Ch_GAM_LUT_140;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 788Ch
extern volatile uint8_t xdata g_rw_gam_lut_008Dh_GAM_LUT_141;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 788Dh
extern volatile uint8_t xdata g_rw_gam_lut_008Eh_GAM_LUT_142;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 788Eh
extern volatile uint8_t xdata g_rw_gam_lut_008Fh_GAM_LUT_143;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 788Fh
extern volatile uint8_t xdata g_rw_gam_lut_0090h_GAM_LUT_144;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7890h

extern volatile uint8_t xdata g_rw_gam_lut_0091h_GAM_LUT_145;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7891h
extern volatile uint8_t xdata g_rw_gam_lut_0092h_GAM_LUT_146;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7892h
extern volatile uint8_t xdata g_rw_gam_lut_0093h_GAM_LUT_147;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7893h
extern volatile uint8_t xdata g_rw_gam_lut_0094h_GAM_LUT_148;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7894h
extern volatile uint8_t xdata g_rw_gam_lut_0095h_GAM_LUT_149;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7895h

extern volatile uint8_t xdata g_rw_gam_lut_0096h_GAM_LUT_150;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7896h
extern volatile uint8_t xdata g_rw_gam_lut_0097h_GAM_LUT_151;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7897h
extern volatile uint8_t xdata g_rw_gam_lut_0098h_GAM_LUT_152;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7898h
extern volatile uint8_t xdata g_rw_gam_lut_0099h_GAM_LUT_153;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7899h
extern volatile uint8_t xdata g_rw_gam_lut_009Ah_GAM_LUT_154;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 789Ah

extern volatile uint8_t xdata g_rw_gam_lut_009Bh_GAM_LUT_155;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 789Bh
extern volatile uint8_t xdata g_rw_gam_lut_009Ch_GAM_LUT_156;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 789Ch
extern volatile uint8_t xdata g_rw_gam_lut_009Dh_GAM_LUT_157;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 789Dh
extern volatile uint8_t xdata g_rw_gam_lut_009Eh_GAM_LUT_158;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 789Eh
extern volatile uint8_t xdata g_rw_gam_lut_009Fh_GAM_LUT_159;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 789Fh

extern volatile uint8_t xdata g_rw_gam_lut_00A0h_GAM_LUT_160;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78A0h
extern volatile uint8_t xdata g_rw_gam_lut_00A1h_GAM_LUT_161;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78A1h
extern volatile uint8_t xdata g_rw_gam_lut_00A2h_GAM_LUT_162;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78A2h
extern volatile uint8_t xdata g_rw_gam_lut_00A3h_GAM_LUT_163;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78A3h
extern volatile uint8_t xdata g_rw_gam_lut_00A4h_GAM_LUT_164;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78A4h

extern volatile uint8_t xdata g_rw_gam_lut_00A5h_GAM_LUT_165;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78A5h
extern volatile uint8_t xdata g_rw_gam_lut_00A6h_GAM_LUT_166;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78A6h
extern volatile uint8_t xdata g_rw_gam_lut_00A7h_GAM_LUT_167;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78A7h
extern volatile uint8_t xdata g_rw_gam_lut_00A8h_GAM_LUT_168;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78A8h
extern volatile uint8_t xdata g_rw_gam_lut_00A9h_GAM_LUT_169;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78A9h

extern volatile uint8_t xdata g_rw_gam_lut_00AAh_GAM_LUT_170;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78AAh
extern volatile uint8_t xdata g_rw_gam_lut_00ABh_GAM_LUT_171;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78ABh
extern volatile uint8_t xdata g_rw_gam_lut_00ACh_GAM_LUT_172;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78ACh
extern volatile uint8_t xdata g_rw_gam_lut_00ADh_GAM_LUT_173;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78ADh
extern volatile uint8_t xdata g_rw_gam_lut_00AEh_GAM_LUT_174;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78AEh

extern volatile uint8_t xdata g_rw_gam_lut_00AFh_GAM_LUT_175;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78AFh
extern volatile uint8_t xdata g_rw_gam_lut_00B0h_GAM_LUT_176;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78B0h
extern volatile uint8_t xdata g_rw_gam_lut_00B1h_GAM_LUT_177;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78B1h
extern volatile uint8_t xdata g_rw_gam_lut_00B2h_GAM_LUT_178;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78B2h
extern volatile uint8_t xdata g_rw_gam_lut_00B3h_GAM_LUT_179;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78B3h

extern volatile uint8_t xdata g_rw_gam_lut_00B4h_GAM_LUT_180;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78B4h
extern volatile uint8_t xdata g_rw_gam_lut_00B5h_GAM_LUT_181;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78B5h
extern volatile uint8_t xdata g_rw_gam_lut_00B6h_GAM_LUT_182;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78B6h
extern volatile uint8_t xdata g_rw_gam_lut_00B7h_GAM_LUT_183;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78B7h
extern volatile uint8_t xdata g_rw_gam_lut_00B8h_GAM_LUT_184;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78B8h

extern volatile uint8_t xdata g_rw_gam_lut_00B9h_GAM_LUT_185;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78B9h
extern volatile uint8_t xdata g_rw_gam_lut_00BAh_GAM_LUT_186;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78BAh
extern volatile uint8_t xdata g_rw_gam_lut_00BBh_GAM_LUT_187;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78BBh
extern volatile uint8_t xdata g_rw_gam_lut_00BCh_GAM_LUT_188;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78BCh
extern volatile uint8_t xdata g_rw_gam_lut_00BDh_GAM_LUT_189;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78BDh

extern volatile uint8_t xdata g_rw_gam_lut_00BEh_GAM_LUT_190;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78BEh
extern volatile uint8_t xdata g_rw_gam_lut_00BFh_GAM_LUT_191;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78BFh
extern volatile uint8_t xdata g_rw_gam_lut_00C0h_GAM_LUT_192;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78C0h
extern volatile uint8_t xdata g_rw_gam_lut_00C1h_GAM_LUT_193;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78C1h
extern volatile uint8_t xdata g_rw_gam_lut_00C2h_GAM_LUT_194;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78C2h

extern volatile uint8_t xdata g_rw_gam_lut_00C3h_GAM_LUT_195;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78C3h
extern volatile uint8_t xdata g_rw_gam_lut_00C4h_GAM_LUT_196;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78C4h
extern volatile uint8_t xdata g_rw_gam_lut_00C5h_GAM_LUT_197;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78C5h
extern volatile uint8_t xdata g_rw_gam_lut_00C6h_GAM_LUT_198;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78C6h
extern volatile uint8_t xdata g_rw_gam_lut_00C7h_GAM_LUT_199;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78C7h

extern volatile uint8_t xdata g_rw_gam_lut_00C8h_GAM_LUT_200;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78C8h
extern volatile uint8_t xdata g_rw_gam_lut_00C9h_GAM_LUT_201;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78C9h
extern volatile uint8_t xdata g_rw_gam_lut_00CAh_GAM_LUT_202;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78CAh
extern volatile uint8_t xdata g_rw_gam_lut_00CBh_GAM_LUT_203;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78CBh
extern volatile uint8_t xdata g_rw_gam_lut_00CCh_GAM_LUT_204;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78CCh

extern volatile uint8_t xdata g_rw_gam_lut_00CDh_GAM_LUT_205;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78CDh
extern volatile uint8_t xdata g_rw_gam_lut_00CEh_GAM_LUT_206;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78CEh
extern volatile uint8_t xdata g_rw_gam_lut_00CFh_GAM_LUT_207;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78CFh
extern volatile uint8_t xdata g_rw_gam_lut_00D0h_GAM_LUT_208;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78D0h
extern volatile uint8_t xdata g_rw_gam_lut_00D1h_GAM_LUT_209;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78D1h

extern volatile uint8_t xdata g_rw_gam_lut_00D2h_GAM_LUT_210;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78D2h
extern volatile uint8_t xdata g_rw_gam_lut_00D3h_GAM_LUT_211;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78D3h
extern volatile uint8_t xdata g_rw_gam_lut_00D4h_GAM_LUT_212;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78D4h
extern volatile uint8_t xdata g_rw_gam_lut_00D5h_GAM_LUT_213;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78D5h
extern volatile uint8_t xdata g_rw_gam_lut_00D6h_GAM_LUT_214;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78D6h

extern volatile uint8_t xdata g_rw_gam_lut_00D7h_GAM_LUT_215;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78D7h
extern volatile uint8_t xdata g_rw_gam_lut_00D8h_GAM_LUT_216;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78D8h
extern volatile uint8_t xdata g_rw_gam_lut_00D9h_GAM_LUT_217;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78D9h
extern volatile uint8_t xdata g_rw_gam_lut_00DAh_GAM_LUT_218;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78DAh
extern volatile uint8_t xdata g_rw_gam_lut_00DBh_GAM_LUT_219;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78DBh

extern volatile uint8_t xdata g_rw_gam_lut_00DCh_GAM_LUT_220;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78DCh
extern volatile uint8_t xdata g_rw_gam_lut_00DDh_GAM_LUT_221;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78DDh
extern volatile uint8_t xdata g_rw_gam_lut_00DEh_GAM_LUT_222;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78DEh
extern volatile uint8_t xdata g_rw_gam_lut_00DFh_GAM_LUT_223;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78DFh
extern volatile uint8_t xdata g_rw_gam_lut_00E0h_GAM_LUT_224;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78E0h

extern volatile uint8_t xdata g_rw_gam_lut_00E1h_GAM_LUT_225;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78E1h
extern volatile uint8_t xdata g_rw_gam_lut_00E2h_GAM_LUT_226;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78E2h
extern volatile uint8_t xdata g_rw_gam_lut_00E3h_GAM_LUT_227;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78E3h
extern volatile uint8_t xdata g_rw_gam_lut_00E4h_GAM_LUT_228;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78E4h
extern volatile uint8_t xdata g_rw_gam_lut_00E5h_GAM_LUT_229;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78E5h

extern volatile uint8_t xdata g_rw_gam_lut_00E6h_GAM_LUT_230;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78E6h
extern volatile uint8_t xdata g_rw_gam_lut_00E7h_GAM_LUT_231;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78E7h
extern volatile uint8_t xdata g_rw_gam_lut_00E8h_GAM_LUT_232;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78E8h
extern volatile uint8_t xdata g_rw_gam_lut_00E9h_GAM_LUT_233;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78E9h
extern volatile uint8_t xdata g_rw_gam_lut_00EAh_GAM_LUT_234;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78EAh

extern volatile uint8_t xdata g_rw_gam_lut_00EBh_GAM_LUT_235;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78EBh
extern volatile uint8_t xdata g_rw_gam_lut_00ECh_GAM_LUT_236;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78ECh
extern volatile uint8_t xdata g_rw_gam_lut_00EDh_GAM_LUT_237;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78EDh
extern volatile uint8_t xdata g_rw_gam_lut_00EEh_GAM_LUT_238;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78EEh
extern volatile uint8_t xdata g_rw_gam_lut_00EFh_GAM_LUT_239;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78EFh

extern volatile uint8_t xdata g_rw_gam_lut_00F0h_GAM_LUT_240;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78F0h
extern volatile uint8_t xdata g_rw_gam_lut_00F1h_GAM_LUT_241;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78F1h
extern volatile uint8_t xdata g_rw_gam_lut_00F2h_GAM_LUT_242;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78F2h
extern volatile uint8_t xdata g_rw_gam_lut_00F3h_GAM_LUT_243;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78F3h
extern volatile uint8_t xdata g_rw_gam_lut_00F4h_GAM_LUT_244;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78F4h

extern volatile uint8_t xdata g_rw_gam_lut_00F5h_GAM_LUT_245;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78F5h
extern volatile uint8_t xdata g_rw_gam_lut_00F6h_GAM_LUT_246;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78F6h
extern volatile uint8_t xdata g_rw_gam_lut_00F7h_GAM_LUT_247;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78F7h
extern volatile uint8_t xdata g_rw_gam_lut_00F8h_GAM_LUT_248;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78F8h
extern volatile uint8_t xdata g_rw_gam_lut_00F9h_GAM_LUT_249;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78F9h

extern volatile uint8_t xdata g_rw_gam_lut_00FAh_GAM_LUT_250;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78FAh
extern volatile uint8_t xdata g_rw_gam_lut_00FBh_GAM_LUT_251;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78FBh
extern volatile uint8_t xdata g_rw_gam_lut_00FCh_GAM_LUT_252;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 78FCh
extern volatile uint8_t xdata g_rw_gam_lut_00FDh_GAM_LUT_253;                                            // [msb:lsb] = [7:0], val = 32, 	Absolute Address = 78FDh
extern volatile uint8_t xdata g_rw_gam_lut_00FEh_GAM_LUT_254;                                            // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 78FEh

extern volatile uint8_t xdata g_rw_gam_lut_00FFh_GAM_LUT_255;                                            // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 78FFh
extern volatile uint8_t xdata g_rw_gam_lut_0100h_GAM_LUT_256;                                            // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 7900h
extern volatile uint8_t xdata g_rw_gam_lut_0101h_GAM_LUT_257;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7901h
extern volatile uint8_t xdata g_rw_gam_lut_0102h_GAM_LUT_258;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7902h
extern volatile uint8_t xdata g_rw_gam_lut_0103h_GAM_LUT_259;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7903h

extern volatile uint8_t xdata g_rw_gam_lut_0104h_GAM_LUT_260;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7904h
extern volatile uint8_t xdata g_rw_gam_lut_0105h_GAM_LUT_261;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7905h
extern volatile uint8_t xdata g_rw_gam_lut_0106h_GAM_LUT_262;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7906h
extern volatile uint8_t xdata g_rw_gam_lut_0107h_GAM_LUT_263;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7907h
extern volatile uint8_t xdata g_rw_gam_lut_0108h_GAM_LUT_264;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7908h

extern volatile uint8_t xdata g_rw_gam_lut_0109h_GAM_LUT_265;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7909h
extern volatile uint8_t xdata g_rw_gam_lut_010Ah_GAM_LUT_266;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 790Ah
extern volatile uint8_t xdata g_rw_gam_lut_010Bh_GAM_LUT_267;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 790Bh
extern volatile uint8_t xdata g_rw_gam_lut_010Ch_GAM_LUT_268;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 790Ch
extern volatile uint8_t xdata g_rw_gam_lut_010Dh_GAM_LUT_269;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 790Dh

extern volatile uint8_t xdata g_rw_gam_lut_010Eh_GAM_LUT_270;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 790Eh
extern volatile uint8_t xdata g_rw_gam_lut_010Fh_GAM_LUT_271;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 790Fh
extern volatile uint8_t xdata g_rw_gam_lut_0110h_GAM_LUT_272;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7910h
extern volatile uint8_t xdata g_rw_gam_lut_0111h_GAM_LUT_273;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7911h
extern volatile uint8_t xdata g_rw_gam_lut_0112h_GAM_LUT_274;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7912h

extern volatile uint8_t xdata g_rw_gam_lut_0113h_GAM_LUT_275;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7913h
extern volatile uint8_t xdata g_rw_gam_lut_0114h_GAM_LUT_276;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7914h
extern volatile uint8_t xdata g_rw_gam_lut_0115h_GAM_LUT_277;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7915h
extern volatile uint8_t xdata g_rw_gam_lut_0116h_GAM_LUT_278;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7916h
extern volatile uint8_t xdata g_rw_gam_lut_0117h_GAM_LUT_279;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7917h

extern volatile uint8_t xdata g_rw_gam_lut_0118h_GAM_LUT_280;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7918h
extern volatile uint8_t xdata g_rw_gam_lut_0119h_GAM_LUT_281;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7919h
extern volatile uint8_t xdata g_rw_gam_lut_011Ah_GAM_LUT_282;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 791Ah
extern volatile uint8_t xdata g_rw_gam_lut_011Bh_GAM_LUT_283;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 791Bh
extern volatile uint8_t xdata g_rw_gam_lut_011Ch_GAM_LUT_284;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 791Ch

extern volatile uint8_t xdata g_rw_gam_lut_011Dh_GAM_LUT_285;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 791Dh
extern volatile uint8_t xdata g_rw_gam_lut_011Eh_GAM_LUT_286;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 791Eh
extern volatile uint8_t xdata g_rw_gam_lut_011Fh_GAM_LUT_287;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 791Fh
extern volatile uint8_t xdata g_rw_gam_lut_0120h_GAM_LUT_288;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7920h
extern volatile uint8_t xdata g_rw_gam_lut_0121h_GAM_LUT_289;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7921h

extern volatile uint8_t xdata g_rw_gam_lut_0122h_GAM_LUT_290;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7922h
extern volatile uint8_t xdata g_rw_gam_lut_0123h_GAM_LUT_291;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7923h
extern volatile uint8_t xdata g_rw_gam_lut_0124h_GAM_LUT_292;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7924h
extern volatile uint8_t xdata g_rw_gam_lut_0125h_GAM_LUT_293;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7925h
extern volatile uint8_t xdata g_rw_gam_lut_0126h_GAM_LUT_294;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7926h

extern volatile uint8_t xdata g_rw_gam_lut_0127h_GAM_LUT_295;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7927h
extern volatile uint8_t xdata g_rw_gam_lut_0128h_GAM_LUT_296;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7928h
extern volatile uint8_t xdata g_rw_gam_lut_0129h_GAM_LUT_297;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7929h
extern volatile uint8_t xdata g_rw_gam_lut_012Ah_GAM_LUT_298;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 792Ah
extern volatile uint8_t xdata g_rw_gam_lut_012Bh_GAM_LUT_299;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 792Bh

extern volatile uint8_t xdata g_rw_gam_lut_012Ch_GAM_LUT_300;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 792Ch
extern volatile uint8_t xdata g_rw_gam_lut_012Dh_GAM_LUT_301;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 792Dh
extern volatile uint8_t xdata g_rw_gam_lut_012Eh_GAM_LUT_302;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 792Eh
extern volatile uint8_t xdata g_rw_gam_lut_012Fh_GAM_LUT_303;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 792Fh
extern volatile uint8_t xdata g_rw_gam_lut_0130h_GAM_LUT_304;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7930h

extern volatile uint8_t xdata g_rw_gam_lut_0131h_GAM_LUT_305;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7931h
extern volatile uint8_t xdata g_rw_gam_lut_0132h_GAM_LUT_306;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7932h
extern volatile uint8_t xdata g_rw_gam_lut_0133h_GAM_LUT_307;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7933h
extern volatile uint8_t xdata g_rw_gam_lut_0134h_GAM_LUT_308;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7934h
extern volatile uint8_t xdata g_rw_gam_lut_0135h_GAM_LUT_309;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7935h

extern volatile uint8_t xdata g_rw_gam_lut_0136h_GAM_LUT_310;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7936h
extern volatile uint8_t xdata g_rw_gam_lut_0137h_GAM_LUT_311;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7937h
extern volatile uint8_t xdata g_rw_gam_lut_0138h_GAM_LUT_312;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7938h
extern volatile uint8_t xdata g_rw_gam_lut_0139h_GAM_LUT_313;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7939h
extern volatile uint8_t xdata g_rw_gam_lut_013Ah_GAM_LUT_314;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 793Ah

extern volatile uint8_t xdata g_rw_gam_lut_013Bh_GAM_LUT_315;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 793Bh
extern volatile uint8_t xdata g_rw_gam_lut_013Ch_GAM_LUT_316;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 793Ch
extern volatile uint8_t xdata g_rw_gam_lut_013Dh_GAM_LUT_317;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 793Dh
extern volatile uint8_t xdata g_rw_gam_lut_013Eh_GAM_LUT_318;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 793Eh
extern volatile uint8_t xdata g_rw_gam_lut_013Fh_GAM_LUT_319;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 793Fh

extern volatile uint8_t xdata g_rw_gam_lut_0140h_GAM_LUT_320;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7940h
extern volatile uint8_t xdata g_rw_gam_lut_0141h_GAM_LUT_321;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7941h
extern volatile uint8_t xdata g_rw_gam_lut_0142h_GAM_LUT_322;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7942h
extern volatile uint8_t xdata g_rw_gam_lut_0143h_GAM_LUT_323;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7943h
extern volatile uint8_t xdata g_rw_gam_lut_0144h_GAM_LUT_324;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7944h

extern volatile uint8_t xdata g_rw_gam_lut_0145h_GAM_LUT_325;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7945h
extern volatile uint8_t xdata g_rw_gam_lut_0146h_GAM_LUT_326;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7946h
extern volatile uint8_t xdata g_rw_gam_lut_0147h_GAM_LUT_327;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7947h
extern volatile uint8_t xdata g_rw_gam_lut_0148h_GAM_LUT_328;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7948h
extern volatile uint8_t xdata g_rw_gam_lut_0149h_GAM_LUT_329;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7949h

extern volatile uint8_t xdata g_rw_gam_lut_014Ah_GAM_LUT_330;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 794Ah
extern volatile uint8_t xdata g_rw_gam_lut_014Bh_GAM_LUT_331;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 794Bh
extern volatile uint8_t xdata g_rw_gam_lut_014Ch_GAM_LUT_332;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 794Ch
extern volatile uint8_t xdata g_rw_gam_lut_014Dh_GAM_LUT_333;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 794Dh
extern volatile uint8_t xdata g_rw_gam_lut_014Eh_GAM_LUT_334;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 794Eh

extern volatile uint8_t xdata g_rw_gam_lut_014Fh_GAM_LUT_335;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 794Fh
extern volatile uint8_t xdata g_rw_gam_lut_0150h_GAM_LUT_336;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7950h
extern volatile uint8_t xdata g_rw_gam_lut_0151h_GAM_LUT_337;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7951h
extern volatile uint8_t xdata g_rw_gam_lut_0152h_GAM_LUT_338;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7952h
extern volatile uint8_t xdata g_rw_gam_lut_0153h_GAM_LUT_339;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7953h

extern volatile uint8_t xdata g_rw_gam_lut_0154h_GAM_LUT_340;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7954h
extern volatile uint8_t xdata g_rw_gam_lut_0155h_GAM_LUT_341;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7955h
extern volatile uint8_t xdata g_rw_gam_lut_0156h_GAM_LUT_342;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7956h
extern volatile uint8_t xdata g_rw_gam_lut_0157h_GAM_LUT_343;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7957h
extern volatile uint8_t xdata g_rw_gam_lut_0158h_GAM_LUT_344;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7958h

extern volatile uint8_t xdata g_rw_gam_lut_0159h_GAM_LUT_345;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7959h
extern volatile uint8_t xdata g_rw_gam_lut_015Ah_GAM_LUT_346;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 795Ah
extern volatile uint8_t xdata g_rw_gam_lut_015Bh_GAM_LUT_347;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 795Bh
extern volatile uint8_t xdata g_rw_gam_lut_015Ch_GAM_LUT_348;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 795Ch
extern volatile uint8_t xdata g_rw_gam_lut_015Dh_GAM_LUT_349;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 795Dh

extern volatile uint8_t xdata g_rw_gam_lut_015Eh_GAM_LUT_350;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 795Eh
extern volatile uint8_t xdata g_rw_gam_lut_015Fh_GAM_LUT_351;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 795Fh
extern volatile uint8_t xdata g_rw_gam_lut_0160h_GAM_LUT_352;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7960h
extern volatile uint8_t xdata g_rw_gam_lut_0161h_GAM_LUT_353;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7961h
extern volatile uint8_t xdata g_rw_gam_lut_0162h_GAM_LUT_354;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7962h

extern volatile uint8_t xdata g_rw_gam_lut_0163h_GAM_LUT_355;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7963h
extern volatile uint8_t xdata g_rw_gam_lut_0164h_GAM_LUT_356;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7964h
extern volatile uint8_t xdata g_rw_gam_lut_0165h_GAM_LUT_357;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7965h
extern volatile uint8_t xdata g_rw_gam_lut_0166h_GAM_LUT_358;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7966h
extern volatile uint8_t xdata g_rw_gam_lut_0167h_GAM_LUT_359;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7967h

extern volatile uint8_t xdata g_rw_gam_lut_0168h_GAM_LUT_360;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7968h
extern volatile uint8_t xdata g_rw_gam_lut_0169h_GAM_LUT_361;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7969h
extern volatile uint8_t xdata g_rw_gam_lut_016Ah_GAM_LUT_362;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 796Ah
extern volatile uint8_t xdata g_rw_gam_lut_016Bh_GAM_LUT_363;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 796Bh
extern volatile uint8_t xdata g_rw_gam_lut_016Ch_GAM_LUT_364;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 796Ch

extern volatile uint8_t xdata g_rw_gam_lut_016Dh_GAM_LUT_365;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 796Dh
extern volatile uint8_t xdata g_rw_gam_lut_016Eh_GAM_LUT_366;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 796Eh
extern volatile uint8_t xdata g_rw_gam_lut_016Fh_GAM_LUT_367;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 796Fh
extern volatile uint8_t xdata g_rw_gam_lut_0170h_GAM_LUT_368;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7970h
extern volatile uint8_t xdata g_rw_gam_lut_0171h_GAM_LUT_369;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7971h

extern volatile uint8_t xdata g_rw_gam_lut_0172h_GAM_LUT_370;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7972h
extern volatile uint8_t xdata g_rw_gam_lut_0173h_GAM_LUT_371;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7973h
extern volatile uint8_t xdata g_rw_gam_lut_0174h_GAM_LUT_372;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7974h
extern volatile uint8_t xdata g_rw_gam_lut_0175h_GAM_LUT_373;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7975h
extern volatile uint8_t xdata g_rw_gam_lut_0176h_GAM_LUT_374;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7976h

extern volatile uint8_t xdata g_rw_gam_lut_0177h_GAM_LUT_375;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7977h
extern volatile uint8_t xdata g_rw_gam_lut_0178h_GAM_LUT_376;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7978h
extern volatile uint8_t xdata g_rw_gam_lut_0179h_GAM_LUT_377;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7979h
extern volatile uint8_t xdata g_rw_gam_lut_017Ah_GAM_LUT_378;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 797Ah
extern volatile uint8_t xdata g_rw_gam_lut_017Bh_GAM_LUT_379;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 797Bh

extern volatile uint8_t xdata g_rw_gam_lut_017Ch_GAM_LUT_380;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 797Ch
extern volatile uint8_t xdata g_rw_gam_lut_017Dh_GAM_LUT_381;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 797Dh
extern volatile uint8_t xdata g_rw_gam_lut_017Eh_GAM_LUT_382;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 797Eh
extern volatile uint8_t xdata g_rw_gam_lut_017Fh_GAM_LUT_383;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 797Fh
extern volatile uint8_t xdata g_rw_gam_lut_0180h_GAM_LUT_384;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7980h

extern volatile uint8_t xdata g_rw_gam_lut_0181h_GAM_LUT_385;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7981h
extern volatile uint8_t xdata g_rw_gam_lut_0182h_GAM_LUT_386;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7982h
extern volatile uint8_t xdata g_rw_gam_lut_0183h_GAM_LUT_387;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7983h
extern volatile uint8_t xdata g_rw_gam_lut_0184h_GAM_LUT_388;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7984h
extern volatile uint8_t xdata g_rw_gam_lut_0185h_GAM_LUT_389;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7985h

extern volatile uint8_t xdata g_rw_gam_lut_0186h_GAM_LUT_390;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7986h
extern volatile uint8_t xdata g_rw_gam_lut_0187h_GAM_LUT_391;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7987h
extern volatile uint8_t xdata g_rw_gam_lut_0188h_GAM_LUT_392;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7988h
extern volatile uint8_t xdata g_rw_gam_lut_0189h_GAM_LUT_393;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7989h
extern volatile uint8_t xdata g_rw_gam_lut_018Ah_GAM_LUT_394;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 798Ah

extern volatile uint8_t xdata g_rw_gam_lut_018Bh_GAM_LUT_395;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 798Bh
extern volatile uint8_t xdata g_rw_gam_lut_018Ch_GAM_LUT_396;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 798Ch
extern volatile uint8_t xdata g_rw_gam_lut_018Dh_GAM_LUT_397;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 798Dh
extern volatile uint8_t xdata g_rw_gam_lut_018Eh_GAM_LUT_398;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 798Eh
extern volatile uint8_t xdata g_rw_gam_lut_018Fh_GAM_LUT_399;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 798Fh

extern volatile uint8_t xdata g_rw_gam_lut_0190h_GAM_LUT_400;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7990h
extern volatile uint8_t xdata g_rw_gam_lut_0191h_GAM_LUT_401;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7991h
extern volatile uint8_t xdata g_rw_gam_lut_0192h_GAM_LUT_402;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7992h
extern volatile uint8_t xdata g_rw_gam_lut_0193h_GAM_LUT_403;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7993h
extern volatile uint8_t xdata g_rw_gam_lut_0194h_GAM_LUT_404;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7994h

extern volatile uint8_t xdata g_rw_gam_lut_0195h_GAM_LUT_405;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7995h
extern volatile uint8_t xdata g_rw_gam_lut_0196h_GAM_LUT_406;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7996h
extern volatile uint8_t xdata g_rw_gam_lut_0197h_GAM_LUT_407;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7997h
extern volatile uint8_t xdata g_rw_gam_lut_0198h_GAM_LUT_408;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7998h
extern volatile uint8_t xdata g_rw_gam_lut_0199h_GAM_LUT_409;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7999h

extern volatile uint8_t xdata g_rw_gam_lut_019Ah_GAM_LUT_410;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 799Ah
extern volatile uint8_t xdata g_rw_gam_lut_019Bh_GAM_LUT_411;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 799Bh
extern volatile uint8_t xdata g_rw_gam_lut_019Ch_GAM_LUT_412;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 799Ch
extern volatile uint8_t xdata g_rw_gam_lut_019Dh_GAM_LUT_413;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 799Dh
extern volatile uint8_t xdata g_rw_gam_lut_019Eh_GAM_LUT_414;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 799Eh

extern volatile uint8_t xdata g_rw_gam_lut_019Fh_GAM_LUT_415;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 799Fh
extern volatile uint8_t xdata g_rw_gam_lut_01A0h_GAM_LUT_416;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79A0h
extern volatile uint8_t xdata g_rw_gam_lut_01A1h_GAM_LUT_417;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79A1h
extern volatile uint8_t xdata g_rw_gam_lut_01A2h_GAM_LUT_418;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79A2h
extern volatile uint8_t xdata g_rw_gam_lut_01A3h_GAM_LUT_419;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79A3h

extern volatile uint8_t xdata g_rw_gam_lut_01A4h_GAM_LUT_420;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79A4h
extern volatile uint8_t xdata g_rw_gam_lut_01A5h_GAM_LUT_421;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79A5h
extern volatile uint8_t xdata g_rw_gam_lut_01A6h_GAM_LUT_422;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79A6h
extern volatile uint8_t xdata g_rw_gam_lut_01A7h_GAM_LUT_423;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79A7h
extern volatile uint8_t xdata g_rw_gam_lut_01A8h_GAM_LUT_424;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79A8h

extern volatile uint8_t xdata g_rw_gam_lut_01A9h_GAM_LUT_425;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79A9h
extern volatile uint8_t xdata g_rw_gam_lut_01AAh_GAM_LUT_426;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79AAh
extern volatile uint8_t xdata g_rw_gam_lut_01ABh_GAM_LUT_427;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79ABh
extern volatile uint8_t xdata g_rw_gam_lut_01ACh_GAM_LUT_428;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79ACh
extern volatile uint8_t xdata g_rw_gam_lut_01ADh_GAM_LUT_429;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79ADh

extern volatile uint8_t xdata g_rw_gam_lut_01AEh_GAM_LUT_430;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79AEh
extern volatile uint8_t xdata g_rw_gam_lut_01AFh_GAM_LUT_431;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79AFh
extern volatile uint8_t xdata g_rw_gam_lut_01B0h_GAM_LUT_432;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79B0h
extern volatile uint8_t xdata g_rw_gam_lut_01B1h_GAM_LUT_433;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79B1h
extern volatile uint8_t xdata g_rw_gam_lut_01B2h_GAM_LUT_434;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79B2h

extern volatile uint8_t xdata g_rw_gam_lut_01B3h_GAM_LUT_435;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79B3h
extern volatile uint8_t xdata g_rw_gam_lut_01B4h_GAM_LUT_436;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79B4h
extern volatile uint8_t xdata g_rw_gam_lut_01B5h_GAM_LUT_437;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79B5h
extern volatile uint8_t xdata g_rw_gam_lut_01B6h_GAM_LUT_438;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79B6h
extern volatile uint8_t xdata g_rw_gam_lut_01B7h_GAM_LUT_439;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79B7h

extern volatile uint8_t xdata g_rw_gam_lut_01B8h_GAM_LUT_440;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79B8h
extern volatile uint8_t xdata g_rw_gam_lut_01B9h_GAM_LUT_441;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79B9h
extern volatile uint8_t xdata g_rw_gam_lut_01BAh_GAM_LUT_442;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79BAh
extern volatile uint8_t xdata g_rw_gam_lut_01BBh_GAM_LUT_443;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79BBh
extern volatile uint8_t xdata g_rw_gam_lut_01BCh_GAM_LUT_444;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79BCh

extern volatile uint8_t xdata g_rw_gam_lut_01BDh_GAM_LUT_445;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79BDh
extern volatile uint8_t xdata g_rw_gam_lut_01BEh_GAM_LUT_446;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79BEh
extern volatile uint8_t xdata g_rw_gam_lut_01BFh_GAM_LUT_447;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79BFh
extern volatile uint8_t xdata g_rw_gam_lut_01C0h_GAM_LUT_448;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79C0h
extern volatile uint8_t xdata g_rw_gam_lut_01C1h_GAM_LUT_449;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79C1h

extern volatile uint8_t xdata g_rw_gam_lut_01C2h_GAM_LUT_450;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79C2h
extern volatile uint8_t xdata g_rw_gam_lut_01C3h_GAM_LUT_451;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79C3h
extern volatile uint8_t xdata g_rw_gam_lut_01C4h_GAM_LUT_452;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79C4h
extern volatile uint8_t xdata g_rw_gam_lut_01C5h_GAM_LUT_453;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79C5h
extern volatile uint8_t xdata g_rw_gam_lut_01C6h_GAM_LUT_454;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79C6h

extern volatile uint8_t xdata g_rw_gam_lut_01C7h_GAM_LUT_455;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79C7h
extern volatile uint8_t xdata g_rw_gam_lut_01C8h_GAM_LUT_456;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79C8h
extern volatile uint8_t xdata g_rw_gam_lut_01C9h_GAM_LUT_457;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79C9h
extern volatile uint8_t xdata g_rw_gam_lut_01CAh_GAM_LUT_458;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79CAh
extern volatile uint8_t xdata g_rw_gam_lut_01CBh_GAM_LUT_459;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79CBh

extern volatile uint8_t xdata g_rw_gam_lut_01CCh_GAM_LUT_460;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79CCh
extern volatile uint8_t xdata g_rw_gam_lut_01CDh_GAM_LUT_461;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79CDh
extern volatile uint8_t xdata g_rw_gam_lut_01CEh_GAM_LUT_462;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79CEh
extern volatile uint8_t xdata g_rw_gam_lut_01CFh_GAM_LUT_463;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79CFh
extern volatile uint8_t xdata g_rw_gam_lut_01D0h_GAM_LUT_464;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79D0h

extern volatile uint8_t xdata g_rw_gam_lut_01D1h_GAM_LUT_465;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79D1h
extern volatile uint8_t xdata g_rw_gam_lut_01D2h_GAM_LUT_466;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79D2h
extern volatile uint8_t xdata g_rw_gam_lut_01D3h_GAM_LUT_467;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79D3h
extern volatile uint8_t xdata g_rw_gam_lut_01D4h_GAM_LUT_468;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79D4h
extern volatile uint8_t xdata g_rw_gam_lut_01D5h_GAM_LUT_469;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79D5h

extern volatile uint8_t xdata g_rw_gam_lut_01D6h_GAM_LUT_470;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79D6h
extern volatile uint8_t xdata g_rw_gam_lut_01D7h_GAM_LUT_471;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79D7h
extern volatile uint8_t xdata g_rw_gam_lut_01D8h_GAM_LUT_472;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79D8h
extern volatile uint8_t xdata g_rw_gam_lut_01D9h_GAM_LUT_473;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79D9h
extern volatile uint8_t xdata g_rw_gam_lut_01DAh_GAM_LUT_474;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79DAh

extern volatile uint8_t xdata g_rw_gam_lut_01DBh_GAM_LUT_475;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79DBh
extern volatile uint8_t xdata g_rw_gam_lut_01DCh_GAM_LUT_476;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79DCh
extern volatile uint8_t xdata g_rw_gam_lut_01DDh_GAM_LUT_477;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79DDh
extern volatile uint8_t xdata g_rw_gam_lut_01DEh_GAM_LUT_478;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79DEh
extern volatile uint8_t xdata g_rw_gam_lut_01DFh_GAM_LUT_479;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79DFh

extern volatile uint8_t xdata g_rw_gam_lut_01E0h_GAM_LUT_480;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79E0h
extern volatile uint8_t xdata g_rw_gam_lut_01E1h_GAM_LUT_481;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79E1h
extern volatile uint8_t xdata g_rw_gam_lut_01E2h_GAM_LUT_482;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79E2h
extern volatile uint8_t xdata g_rw_gam_lut_01E3h_GAM_LUT_483;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79E3h
extern volatile uint8_t xdata g_rw_gam_lut_01E4h_GAM_LUT_484;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79E4h

extern volatile uint8_t xdata g_rw_gam_lut_01E5h_GAM_LUT_485;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79E5h
extern volatile uint8_t xdata g_rw_gam_lut_01E6h_GAM_LUT_486;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79E6h
extern volatile uint8_t xdata g_rw_gam_lut_01E7h_GAM_LUT_487;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79E7h
extern volatile uint8_t xdata g_rw_gam_lut_01E8h_GAM_LUT_488;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79E8h
extern volatile uint8_t xdata g_rw_gam_lut_01E9h_GAM_LUT_489;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79E9h

extern volatile uint8_t xdata g_rw_gam_lut_01EAh_GAM_LUT_490;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79EAh
extern volatile uint8_t xdata g_rw_gam_lut_01EBh_GAM_LUT_491;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79EBh
extern volatile uint8_t xdata g_rw_gam_lut_01ECh_GAM_LUT_492;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79ECh
extern volatile uint8_t xdata g_rw_gam_lut_01EDh_GAM_LUT_493;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79EDh
extern volatile uint8_t xdata g_rw_gam_lut_01EEh_GAM_LUT_494;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79EEh

extern volatile uint8_t xdata g_rw_gam_lut_01EFh_GAM_LUT_495;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79EFh
extern volatile uint8_t xdata g_rw_gam_lut_01F0h_GAM_LUT_496;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79F0h
extern volatile uint8_t xdata g_rw_gam_lut_01F1h_GAM_LUT_497;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79F1h
extern volatile uint8_t xdata g_rw_gam_lut_01F2h_GAM_LUT_498;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79F2h
extern volatile uint8_t xdata g_rw_gam_lut_01F3h_GAM_LUT_499;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79F3h

extern volatile uint8_t xdata g_rw_gam_lut_01F4h_GAM_LUT_500;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79F4h
extern volatile uint8_t xdata g_rw_gam_lut_01F5h_GAM_LUT_501;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79F5h
extern volatile uint8_t xdata g_rw_gam_lut_01F6h_GAM_LUT_502;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79F6h
extern volatile uint8_t xdata g_rw_gam_lut_01F7h_GAM_LUT_503;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79F7h
extern volatile uint8_t xdata g_rw_gam_lut_01F8h_GAM_LUT_504;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79F8h

extern volatile uint8_t xdata g_rw_gam_lut_01F9h_GAM_LUT_505;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79F9h
extern volatile uint8_t xdata g_rw_gam_lut_01FAh_GAM_LUT_506;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79FAh
extern volatile uint8_t xdata g_rw_gam_lut_01FBh_GAM_LUT_507;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79FBh
extern volatile uint8_t xdata g_rw_gam_lut_01FCh_GAM_LUT_508;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79FCh
extern volatile uint8_t xdata g_rw_gam_lut_01FDh_GAM_LUT_509;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79FDh

extern volatile uint8_t xdata g_rw_gam_lut_01FEh_GAM_LUT_510;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 79FEh
extern volatile uint8_t xdata g_rw_gam_lut_01FFh_GAM_LUT_511;                                            // [msb:lsb] = [7:0], val = 32, 	Absolute Address = 79FFh
extern volatile uint8_t xdata g_rw_gam_lut_0200h_GAM_LUT_512;                                            // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 7A00h
extern volatile uint8_t xdata g_rw_gam_lut_0201h_GAM_LUT_513;                                            // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 7A01h
extern volatile uint8_t xdata g_rw_gam_lut_0202h_GAM_LUT_514;                                            // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 7A02h

extern volatile uint8_t xdata g_rw_gam_lut_0203h_GAM_LUT_515;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7A03h
extern volatile uint8_t xdata g_rw_gam_lut_0204h_GAM_LUT_516;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7A04h
extern volatile uint8_t xdata g_rw_gam_lut_0205h_GAM_LUT_517;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A05h
extern volatile uint8_t xdata g_rw_gam_lut_0206h_GAM_LUT_518;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A06h
extern volatile uint8_t xdata g_rw_gam_lut_0207h_GAM_LUT_519;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A07h

extern volatile uint8_t xdata g_rw_gam_lut_0208h_GAM_LUT_520;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A08h
extern volatile uint8_t xdata g_rw_gam_lut_0209h_GAM_LUT_521;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A09h
extern volatile uint8_t xdata g_rw_gam_lut_020Ah_GAM_LUT_522;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A0Ah
extern volatile uint8_t xdata g_rw_gam_lut_020Bh_GAM_LUT_523;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A0Bh
extern volatile uint8_t xdata g_rw_gam_lut_020Ch_GAM_LUT_524;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A0Ch

extern volatile uint8_t xdata g_rw_gam_lut_020Dh_GAM_LUT_525;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A0Dh
extern volatile uint8_t xdata g_rw_gam_lut_020Eh_GAM_LUT_526;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A0Eh
extern volatile uint8_t xdata g_rw_gam_lut_020Fh_GAM_LUT_527;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A0Fh
extern volatile uint8_t xdata g_rw_gam_lut_0210h_GAM_LUT_528;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A10h
extern volatile uint8_t xdata g_rw_gam_lut_0211h_GAM_LUT_529;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A11h

extern volatile uint8_t xdata g_rw_gam_lut_0212h_GAM_LUT_530;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A12h
extern volatile uint8_t xdata g_rw_gam_lut_0213h_GAM_LUT_531;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A13h
extern volatile uint8_t xdata g_rw_gam_lut_0214h_GAM_LUT_532;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A14h
extern volatile uint8_t xdata g_rw_gam_lut_0215h_GAM_LUT_533;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A15h
extern volatile uint8_t xdata g_rw_gam_lut_0216h_GAM_LUT_534;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A16h

extern volatile uint8_t xdata g_rw_gam_lut_0217h_GAM_LUT_535;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A17h
extern volatile uint8_t xdata g_rw_gam_lut_0218h_GAM_LUT_536;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A18h
extern volatile uint8_t xdata g_rw_gam_lut_0219h_GAM_LUT_537;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A19h
extern volatile uint8_t xdata g_rw_gam_lut_021Ah_GAM_LUT_538;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A1Ah
extern volatile uint8_t xdata g_rw_gam_lut_021Bh_GAM_LUT_539;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A1Bh

extern volatile uint8_t xdata g_rw_gam_lut_021Ch_GAM_LUT_540;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A1Ch
extern volatile uint8_t xdata g_rw_gam_lut_021Dh_GAM_LUT_541;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A1Dh
extern volatile uint8_t xdata g_rw_gam_lut_021Eh_GAM_LUT_542;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A1Eh
extern volatile uint8_t xdata g_rw_gam_lut_021Fh_GAM_LUT_543;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A1Fh
extern volatile uint8_t xdata g_rw_gam_lut_0220h_GAM_LUT_544;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A20h

extern volatile uint8_t xdata g_rw_gam_lut_0221h_GAM_LUT_545;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A21h
extern volatile uint8_t xdata g_rw_gam_lut_0222h_GAM_LUT_546;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A22h
extern volatile uint8_t xdata g_rw_gam_lut_0223h_GAM_LUT_547;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A23h
extern volatile uint8_t xdata g_rw_gam_lut_0224h_GAM_LUT_548;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A24h
extern volatile uint8_t xdata g_rw_gam_lut_0225h_GAM_LUT_549;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A25h

extern volatile uint8_t xdata g_rw_gam_lut_0226h_GAM_LUT_550;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A26h
extern volatile uint8_t xdata g_rw_gam_lut_0227h_GAM_LUT_551;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A27h
extern volatile uint8_t xdata g_rw_gam_lut_0228h_GAM_LUT_552;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A28h
extern volatile uint8_t xdata g_rw_gam_lut_0229h_GAM_LUT_553;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A29h
extern volatile uint8_t xdata g_rw_gam_lut_022Ah_GAM_LUT_554;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A2Ah

extern volatile uint8_t xdata g_rw_gam_lut_022Bh_GAM_LUT_555;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A2Bh
extern volatile uint8_t xdata g_rw_gam_lut_022Ch_GAM_LUT_556;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A2Ch
extern volatile uint8_t xdata g_rw_gam_lut_022Dh_GAM_LUT_557;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A2Dh
extern volatile uint8_t xdata g_rw_gam_lut_022Eh_GAM_LUT_558;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A2Eh
extern volatile uint8_t xdata g_rw_gam_lut_022Fh_GAM_LUT_559;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A2Fh

extern volatile uint8_t xdata g_rw_gam_lut_0230h_GAM_LUT_560;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A30h
extern volatile uint8_t xdata g_rw_gam_lut_0231h_GAM_LUT_561;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A31h
extern volatile uint8_t xdata g_rw_gam_lut_0232h_GAM_LUT_562;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A32h
extern volatile uint8_t xdata g_rw_gam_lut_0233h_GAM_LUT_563;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A33h
extern volatile uint8_t xdata g_rw_gam_lut_0234h_GAM_LUT_564;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A34h

extern volatile uint8_t xdata g_rw_gam_lut_0235h_GAM_LUT_565;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A35h
extern volatile uint8_t xdata g_rw_gam_lut_0236h_GAM_LUT_566;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A36h
extern volatile uint8_t xdata g_rw_gam_lut_0237h_GAM_LUT_567;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A37h
extern volatile uint8_t xdata g_rw_gam_lut_0238h_GAM_LUT_568;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A38h
extern volatile uint8_t xdata g_rw_gam_lut_0239h_GAM_LUT_569;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A39h

extern volatile uint8_t xdata g_rw_gam_lut_023Ah_GAM_LUT_570;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A3Ah
extern volatile uint8_t xdata g_rw_gam_lut_023Bh_GAM_LUT_571;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A3Bh
extern volatile uint8_t xdata g_rw_gam_lut_023Ch_GAM_LUT_572;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A3Ch
extern volatile uint8_t xdata g_rw_gam_lut_023Dh_GAM_LUT_573;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A3Dh
extern volatile uint8_t xdata g_rw_gam_lut_023Eh_GAM_LUT_574;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A3Eh

extern volatile uint8_t xdata g_rw_gam_lut_023Fh_GAM_LUT_575;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A3Fh
extern volatile uint8_t xdata g_rw_gam_lut_0240h_GAM_LUT_576;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A40h
extern volatile uint8_t xdata g_rw_gam_lut_0241h_GAM_LUT_577;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A41h
extern volatile uint8_t xdata g_rw_gam_lut_0242h_GAM_LUT_578;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A42h
extern volatile uint8_t xdata g_rw_gam_lut_0243h_GAM_LUT_579;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A43h

extern volatile uint8_t xdata g_rw_gam_lut_0244h_GAM_LUT_580;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A44h
extern volatile uint8_t xdata g_rw_gam_lut_0245h_GAM_LUT_581;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A45h
extern volatile uint8_t xdata g_rw_gam_lut_0246h_GAM_LUT_582;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A46h
extern volatile uint8_t xdata g_rw_gam_lut_0247h_GAM_LUT_583;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A47h
extern volatile uint8_t xdata g_rw_gam_lut_0248h_GAM_LUT_584;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A48h

extern volatile uint8_t xdata g_rw_gam_lut_0249h_GAM_LUT_585;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A49h
extern volatile uint8_t xdata g_rw_gam_lut_024Ah_GAM_LUT_586;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A4Ah
extern volatile uint8_t xdata g_rw_gam_lut_024Bh_GAM_LUT_587;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A4Bh
extern volatile uint8_t xdata g_rw_gam_lut_024Ch_GAM_LUT_588;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A4Ch
extern volatile uint8_t xdata g_rw_gam_lut_024Dh_GAM_LUT_589;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A4Dh

extern volatile uint8_t xdata g_rw_gam_lut_024Eh_GAM_LUT_590;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A4Eh
extern volatile uint8_t xdata g_rw_gam_lut_024Fh_GAM_LUT_591;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A4Fh
extern volatile uint8_t xdata g_rw_gam_lut_0250h_GAM_LUT_592;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A50h
extern volatile uint8_t xdata g_rw_gam_lut_0251h_GAM_LUT_593;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A51h
extern volatile uint8_t xdata g_rw_gam_lut_0252h_GAM_LUT_594;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A52h

extern volatile uint8_t xdata g_rw_gam_lut_0253h_GAM_LUT_595;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A53h
extern volatile uint8_t xdata g_rw_gam_lut_0254h_GAM_LUT_596;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A54h
extern volatile uint8_t xdata g_rw_gam_lut_0255h_GAM_LUT_597;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A55h
extern volatile uint8_t xdata g_rw_gam_lut_0256h_GAM_LUT_598;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A56h
extern volatile uint8_t xdata g_rw_gam_lut_0257h_GAM_LUT_599;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A57h

extern volatile uint8_t xdata g_rw_gam_lut_0258h_GAM_LUT_600;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A58h
extern volatile uint8_t xdata g_rw_gam_lut_0259h_GAM_LUT_601;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A59h
extern volatile uint8_t xdata g_rw_gam_lut_025Ah_GAM_LUT_602;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A5Ah
extern volatile uint8_t xdata g_rw_gam_lut_025Bh_GAM_LUT_603;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A5Bh
extern volatile uint8_t xdata g_rw_gam_lut_025Ch_GAM_LUT_604;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A5Ch

extern volatile uint8_t xdata g_rw_gam_lut_025Dh_GAM_LUT_605;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A5Dh
extern volatile uint8_t xdata g_rw_gam_lut_025Eh_GAM_LUT_606;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A5Eh
extern volatile uint8_t xdata g_rw_gam_lut_025Fh_GAM_LUT_607;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A5Fh
extern volatile uint8_t xdata g_rw_gam_lut_0260h_GAM_LUT_608;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A60h
extern volatile uint8_t xdata g_rw_gam_lut_0261h_GAM_LUT_609;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A61h

extern volatile uint8_t xdata g_rw_gam_lut_0262h_GAM_LUT_610;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A62h
extern volatile uint8_t xdata g_rw_gam_lut_0263h_GAM_LUT_611;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A63h
extern volatile uint8_t xdata g_rw_gam_lut_0264h_GAM_LUT_612;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A64h
extern volatile uint8_t xdata g_rw_gam_lut_0265h_GAM_LUT_613;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A65h
extern volatile uint8_t xdata g_rw_gam_lut_0266h_GAM_LUT_614;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A66h

extern volatile uint8_t xdata g_rw_gam_lut_0267h_GAM_LUT_615;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A67h
extern volatile uint8_t xdata g_rw_gam_lut_0268h_GAM_LUT_616;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A68h
extern volatile uint8_t xdata g_rw_gam_lut_0269h_GAM_LUT_617;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A69h
extern volatile uint8_t xdata g_rw_gam_lut_026Ah_GAM_LUT_618;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A6Ah
extern volatile uint8_t xdata g_rw_gam_lut_026Bh_GAM_LUT_619;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A6Bh

extern volatile uint8_t xdata g_rw_gam_lut_026Ch_GAM_LUT_620;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A6Ch
extern volatile uint8_t xdata g_rw_gam_lut_026Dh_GAM_LUT_621;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A6Dh
extern volatile uint8_t xdata g_rw_gam_lut_026Eh_GAM_LUT_622;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A6Eh
extern volatile uint8_t xdata g_rw_gam_lut_026Fh_GAM_LUT_623;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A6Fh
extern volatile uint8_t xdata g_rw_gam_lut_0270h_GAM_LUT_624;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A70h

extern volatile uint8_t xdata g_rw_gam_lut_0271h_GAM_LUT_625;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A71h
extern volatile uint8_t xdata g_rw_gam_lut_0272h_GAM_LUT_626;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A72h
extern volatile uint8_t xdata g_rw_gam_lut_0273h_GAM_LUT_627;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A73h
extern volatile uint8_t xdata g_rw_gam_lut_0274h_GAM_LUT_628;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A74h
extern volatile uint8_t xdata g_rw_gam_lut_0275h_GAM_LUT_629;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A75h

extern volatile uint8_t xdata g_rw_gam_lut_0276h_GAM_LUT_630;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A76h
extern volatile uint8_t xdata g_rw_gam_lut_0277h_GAM_LUT_631;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A77h
extern volatile uint8_t xdata g_rw_gam_lut_0278h_GAM_LUT_632;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A78h
extern volatile uint8_t xdata g_rw_gam_lut_0279h_GAM_LUT_633;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A79h
extern volatile uint8_t xdata g_rw_gam_lut_027Ah_GAM_LUT_634;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A7Ah

extern volatile uint8_t xdata g_rw_gam_lut_027Bh_GAM_LUT_635;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A7Bh
extern volatile uint8_t xdata g_rw_gam_lut_027Ch_GAM_LUT_636;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A7Ch
extern volatile uint8_t xdata g_rw_gam_lut_027Dh_GAM_LUT_637;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A7Dh
extern volatile uint8_t xdata g_rw_gam_lut_027Eh_GAM_LUT_638;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A7Eh
extern volatile uint8_t xdata g_rw_gam_lut_027Fh_GAM_LUT_639;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A7Fh

extern volatile uint8_t xdata g_rw_gam_lut_0280h_GAM_LUT_640;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A80h
extern volatile uint8_t xdata g_rw_gam_lut_0281h_GAM_LUT_641;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A81h
extern volatile uint8_t xdata g_rw_gam_lut_0282h_GAM_LUT_642;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A82h
extern volatile uint8_t xdata g_rw_gam_lut_0283h_GAM_LUT_643;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A83h
extern volatile uint8_t xdata g_rw_gam_lut_0284h_GAM_LUT_644;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A84h

extern volatile uint8_t xdata g_rw_gam_lut_0285h_GAM_LUT_645;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A85h
extern volatile uint8_t xdata g_rw_gam_lut_0286h_GAM_LUT_646;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A86h
extern volatile uint8_t xdata g_rw_gam_lut_0287h_GAM_LUT_647;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A87h
extern volatile uint8_t xdata g_rw_gam_lut_0288h_GAM_LUT_648;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A88h
extern volatile uint8_t xdata g_rw_gam_lut_0289h_GAM_LUT_649;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A89h

extern volatile uint8_t xdata g_rw_gam_lut_028Ah_GAM_LUT_650;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A8Ah
extern volatile uint8_t xdata g_rw_gam_lut_028Bh_GAM_LUT_651;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A8Bh
extern volatile uint8_t xdata g_rw_gam_lut_028Ch_GAM_LUT_652;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A8Ch
extern volatile uint8_t xdata g_rw_gam_lut_028Dh_GAM_LUT_653;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A8Dh
extern volatile uint8_t xdata g_rw_gam_lut_028Eh_GAM_LUT_654;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A8Eh

extern volatile uint8_t xdata g_rw_gam_lut_028Fh_GAM_LUT_655;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A8Fh
extern volatile uint8_t xdata g_rw_gam_lut_0290h_GAM_LUT_656;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A90h
extern volatile uint8_t xdata g_rw_gam_lut_0291h_GAM_LUT_657;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A91h
extern volatile uint8_t xdata g_rw_gam_lut_0292h_GAM_LUT_658;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A92h
extern volatile uint8_t xdata g_rw_gam_lut_0293h_GAM_LUT_659;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A93h

extern volatile uint8_t xdata g_rw_gam_lut_0294h_GAM_LUT_660;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A94h
extern volatile uint8_t xdata g_rw_gam_lut_0295h_GAM_LUT_661;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A95h
extern volatile uint8_t xdata g_rw_gam_lut_0296h_GAM_LUT_662;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A96h
extern volatile uint8_t xdata g_rw_gam_lut_0297h_GAM_LUT_663;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A97h
extern volatile uint8_t xdata g_rw_gam_lut_0298h_GAM_LUT_664;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A98h

extern volatile uint8_t xdata g_rw_gam_lut_0299h_GAM_LUT_665;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A99h
extern volatile uint8_t xdata g_rw_gam_lut_029Ah_GAM_LUT_666;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A9Ah
extern volatile uint8_t xdata g_rw_gam_lut_029Bh_GAM_LUT_667;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A9Bh
extern volatile uint8_t xdata g_rw_gam_lut_029Ch_GAM_LUT_668;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A9Ch
extern volatile uint8_t xdata g_rw_gam_lut_029Dh_GAM_LUT_669;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A9Dh

extern volatile uint8_t xdata g_rw_gam_lut_029Eh_GAM_LUT_670;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A9Eh
extern volatile uint8_t xdata g_rw_gam_lut_029Fh_GAM_LUT_671;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7A9Fh
extern volatile uint8_t xdata g_rw_gam_lut_02A0h_GAM_LUT_672;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AA0h
extern volatile uint8_t xdata g_rw_gam_lut_02A1h_GAM_LUT_673;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AA1h
extern volatile uint8_t xdata g_rw_gam_lut_02A2h_GAM_LUT_674;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AA2h

extern volatile uint8_t xdata g_rw_gam_lut_02A3h_GAM_LUT_675;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AA3h
extern volatile uint8_t xdata g_rw_gam_lut_02A4h_GAM_LUT_676;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AA4h
extern volatile uint8_t xdata g_rw_gam_lut_02A5h_GAM_LUT_677;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AA5h
extern volatile uint8_t xdata g_rw_gam_lut_02A6h_GAM_LUT_678;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AA6h
extern volatile uint8_t xdata g_rw_gam_lut_02A7h_GAM_LUT_679;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AA7h

extern volatile uint8_t xdata g_rw_gam_lut_02A8h_GAM_LUT_680;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AA8h
extern volatile uint8_t xdata g_rw_gam_lut_02A9h_GAM_LUT_681;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AA9h
extern volatile uint8_t xdata g_rw_gam_lut_02AAh_GAM_LUT_682;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AAAh
extern volatile uint8_t xdata g_rw_gam_lut_02ABh_GAM_LUT_683;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AABh
extern volatile uint8_t xdata g_rw_gam_lut_02ACh_GAM_LUT_684;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AACh

extern volatile uint8_t xdata g_rw_gam_lut_02ADh_GAM_LUT_685;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AADh
extern volatile uint8_t xdata g_rw_gam_lut_02AEh_GAM_LUT_686;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AAEh
extern volatile uint8_t xdata g_rw_gam_lut_02AFh_GAM_LUT_687;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AAFh
extern volatile uint8_t xdata g_rw_gam_lut_02B0h_GAM_LUT_688;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AB0h
extern volatile uint8_t xdata g_rw_gam_lut_02B1h_GAM_LUT_689;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AB1h

extern volatile uint8_t xdata g_rw_gam_lut_02B2h_GAM_LUT_690;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AB2h
extern volatile uint8_t xdata g_rw_gam_lut_02B3h_GAM_LUT_691;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AB3h
extern volatile uint8_t xdata g_rw_gam_lut_02B4h_GAM_LUT_692;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AB4h
extern volatile uint8_t xdata g_rw_gam_lut_02B5h_GAM_LUT_693;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AB5h
extern volatile uint8_t xdata g_rw_gam_lut_02B6h_GAM_LUT_694;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AB6h

extern volatile uint8_t xdata g_rw_gam_lut_02B7h_GAM_LUT_695;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AB7h
extern volatile uint8_t xdata g_rw_gam_lut_02B8h_GAM_LUT_696;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AB8h
extern volatile uint8_t xdata g_rw_gam_lut_02B9h_GAM_LUT_697;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AB9h
extern volatile uint8_t xdata g_rw_gam_lut_02BAh_GAM_LUT_698;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ABAh
extern volatile uint8_t xdata g_rw_gam_lut_02BBh_GAM_LUT_699;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ABBh

extern volatile uint8_t xdata g_rw_gam_lut_02BCh_GAM_LUT_700;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ABCh
extern volatile uint8_t xdata g_rw_gam_lut_02BDh_GAM_LUT_701;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ABDh
extern volatile uint8_t xdata g_rw_gam_lut_02BEh_GAM_LUT_702;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ABEh
extern volatile uint8_t xdata g_rw_gam_lut_02BFh_GAM_LUT_703;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ABFh
extern volatile uint8_t xdata g_rw_gam_lut_02C0h_GAM_LUT_704;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AC0h

extern volatile uint8_t xdata g_rw_gam_lut_02C1h_GAM_LUT_705;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AC1h
extern volatile uint8_t xdata g_rw_gam_lut_02C2h_GAM_LUT_706;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AC2h
extern volatile uint8_t xdata g_rw_gam_lut_02C3h_GAM_LUT_707;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AC3h
extern volatile uint8_t xdata g_rw_gam_lut_02C4h_GAM_LUT_708;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AC4h
extern volatile uint8_t xdata g_rw_gam_lut_02C5h_GAM_LUT_709;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AC5h

extern volatile uint8_t xdata g_rw_gam_lut_02C6h_GAM_LUT_710;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AC6h
extern volatile uint8_t xdata g_rw_gam_lut_02C7h_GAM_LUT_711;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AC7h
extern volatile uint8_t xdata g_rw_gam_lut_02C8h_GAM_LUT_712;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AC8h
extern volatile uint8_t xdata g_rw_gam_lut_02C9h_GAM_LUT_713;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AC9h
extern volatile uint8_t xdata g_rw_gam_lut_02CAh_GAM_LUT_714;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ACAh

extern volatile uint8_t xdata g_rw_gam_lut_02CBh_GAM_LUT_715;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ACBh
extern volatile uint8_t xdata g_rw_gam_lut_02CCh_GAM_LUT_716;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ACCh
extern volatile uint8_t xdata g_rw_gam_lut_02CDh_GAM_LUT_717;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ACDh
extern volatile uint8_t xdata g_rw_gam_lut_02CEh_GAM_LUT_718;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ACEh
extern volatile uint8_t xdata g_rw_gam_lut_02CFh_GAM_LUT_719;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ACFh

extern volatile uint8_t xdata g_rw_gam_lut_02D0h_GAM_LUT_720;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AD0h
extern volatile uint8_t xdata g_rw_gam_lut_02D1h_GAM_LUT_721;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AD1h
extern volatile uint8_t xdata g_rw_gam_lut_02D2h_GAM_LUT_722;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AD2h
extern volatile uint8_t xdata g_rw_gam_lut_02D3h_GAM_LUT_723;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AD3h
extern volatile uint8_t xdata g_rw_gam_lut_02D4h_GAM_LUT_724;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AD4h

extern volatile uint8_t xdata g_rw_gam_lut_02D5h_GAM_LUT_725;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AD5h
extern volatile uint8_t xdata g_rw_gam_lut_02D6h_GAM_LUT_726;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AD6h
extern volatile uint8_t xdata g_rw_gam_lut_02D7h_GAM_LUT_727;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AD7h
extern volatile uint8_t xdata g_rw_gam_lut_02D8h_GAM_LUT_728;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AD8h
extern volatile uint8_t xdata g_rw_gam_lut_02D9h_GAM_LUT_729;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AD9h

extern volatile uint8_t xdata g_rw_gam_lut_02DAh_GAM_LUT_730;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ADAh
extern volatile uint8_t xdata g_rw_gam_lut_02DBh_GAM_LUT_731;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ADBh
extern volatile uint8_t xdata g_rw_gam_lut_02DCh_GAM_LUT_732;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ADCh
extern volatile uint8_t xdata g_rw_gam_lut_02DDh_GAM_LUT_733;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ADDh
extern volatile uint8_t xdata g_rw_gam_lut_02DEh_GAM_LUT_734;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ADEh

extern volatile uint8_t xdata g_rw_gam_lut_02DFh_GAM_LUT_735;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7ADFh
extern volatile uint8_t xdata g_rw_gam_lut_02E0h_GAM_LUT_736;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AE0h
extern volatile uint8_t xdata g_rw_gam_lut_02E1h_GAM_LUT_737;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AE1h
extern volatile uint8_t xdata g_rw_gam_lut_02E2h_GAM_LUT_738;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AE2h
extern volatile uint8_t xdata g_rw_gam_lut_02E3h_GAM_LUT_739;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AE3h

extern volatile uint8_t xdata g_rw_gam_lut_02E4h_GAM_LUT_740;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AE4h
extern volatile uint8_t xdata g_rw_gam_lut_02E5h_GAM_LUT_741;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AE5h
extern volatile uint8_t xdata g_rw_gam_lut_02E6h_GAM_LUT_742;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AE6h
extern volatile uint8_t xdata g_rw_gam_lut_02E7h_GAM_LUT_743;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AE7h
extern volatile uint8_t xdata g_rw_gam_lut_02E8h_GAM_LUT_744;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AE8h

extern volatile uint8_t xdata g_rw_gam_lut_02E9h_GAM_LUT_745;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AE9h
extern volatile uint8_t xdata g_rw_gam_lut_02EAh_GAM_LUT_746;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AEAh
extern volatile uint8_t xdata g_rw_gam_lut_02EBh_GAM_LUT_747;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AEBh
extern volatile uint8_t xdata g_rw_gam_lut_02ECh_GAM_LUT_748;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AECh
extern volatile uint8_t xdata g_rw_gam_lut_02EDh_GAM_LUT_749;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AEDh

extern volatile uint8_t xdata g_rw_gam_lut_02EEh_GAM_LUT_750;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AEEh
extern volatile uint8_t xdata g_rw_gam_lut_02EFh_GAM_LUT_751;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AEFh
extern volatile uint8_t xdata g_rw_gam_lut_02F0h_GAM_LUT_752;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AF0h
extern volatile uint8_t xdata g_rw_gam_lut_02F1h_GAM_LUT_753;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AF1h
extern volatile uint8_t xdata g_rw_gam_lut_02F2h_GAM_LUT_754;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AF2h

extern volatile uint8_t xdata g_rw_gam_lut_02F3h_GAM_LUT_755;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AF3h
extern volatile uint8_t xdata g_rw_gam_lut_02F4h_GAM_LUT_756;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AF4h
extern volatile uint8_t xdata g_rw_gam_lut_02F5h_GAM_LUT_757;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AF5h
extern volatile uint8_t xdata g_rw_gam_lut_02F6h_GAM_LUT_758;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AF6h
extern volatile uint8_t xdata g_rw_gam_lut_02F7h_GAM_LUT_759;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AF7h

extern volatile uint8_t xdata g_rw_gam_lut_02F8h_GAM_LUT_760;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AF8h
extern volatile uint8_t xdata g_rw_gam_lut_02F9h_GAM_LUT_761;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AF9h
extern volatile uint8_t xdata g_rw_gam_lut_02FAh_GAM_LUT_762;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AFAh
extern volatile uint8_t xdata g_rw_gam_lut_02FBh_GAM_LUT_763;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AFBh
extern volatile uint8_t xdata g_rw_gam_lut_02FCh_GAM_LUT_764;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AFCh

extern volatile uint8_t xdata g_rw_gam_lut_02FDh_GAM_LUT_765;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AFDh
extern volatile uint8_t xdata g_rw_gam_lut_02FEh_GAM_LUT_766;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AFEh
extern volatile uint8_t xdata g_rw_gam_lut_02FFh_GAM_LUT_767;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7AFFh
extern volatile uint8_t xdata g_rw_gam_lut_0300h_GAM_LUT_768;                                            // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 7B00h
extern volatile uint8_t xdata g_rw_gam_lut_0301h_GAM_LUT_769;                                            // [msb:lsb] = [7:0], val = 32, 	Absolute Address = 7B01h

extern volatile uint8_t xdata g_rw_gam_lut_0302h_GAM_LUT_770;                                            // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 7B02h
extern volatile uint8_t xdata g_rw_gam_lut_0303h_GAM_LUT_771;                                            // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 7B03h
extern volatile uint8_t xdata g_rw_gam_lut_0304h_GAM_LUT_772;                                            // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 7B04h
extern volatile uint8_t xdata g_rw_gam_lut_0305h_GAM_LUT_773;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B05h
extern volatile uint8_t xdata g_rw_gam_lut_0306h_GAM_LUT_774;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B06h

extern volatile uint8_t xdata g_rw_gam_lut_0307h_GAM_LUT_775;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B07h
extern volatile uint8_t xdata g_rw_gam_lut_0308h_GAM_LUT_776;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B08h
extern volatile uint8_t xdata g_rw_gam_lut_0309h_GAM_LUT_777;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B09h
extern volatile uint8_t xdata g_rw_gam_lut_030Ah_GAM_LUT_778;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B0Ah
extern volatile uint8_t xdata g_rw_gam_lut_030Bh_GAM_LUT_779;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B0Bh

extern volatile uint8_t xdata g_rw_gam_lut_030Ch_GAM_LUT_780;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B0Ch
extern volatile uint8_t xdata g_rw_gam_lut_030Dh_GAM_LUT_781;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B0Dh
extern volatile uint8_t xdata g_rw_gam_lut_030Eh_GAM_LUT_782;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B0Eh
extern volatile uint8_t xdata g_rw_gam_lut_030Fh_GAM_LUT_783;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B0Fh
extern volatile uint8_t xdata g_rw_gam_lut_0310h_GAM_LUT_784;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B10h

extern volatile uint8_t xdata g_rw_gam_lut_0311h_GAM_LUT_785;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B11h
extern volatile uint8_t xdata g_rw_gam_lut_0312h_GAM_LUT_786;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B12h
extern volatile uint8_t xdata g_rw_gam_lut_0313h_GAM_LUT_787;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B13h
extern volatile uint8_t xdata g_rw_gam_lut_0314h_GAM_LUT_788;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B14h
extern volatile uint8_t xdata g_rw_gam_lut_0315h_GAM_LUT_789;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B15h

extern volatile uint8_t xdata g_rw_gam_lut_0316h_GAM_LUT_790;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B16h
extern volatile uint8_t xdata g_rw_gam_lut_0317h_GAM_LUT_791;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B17h
extern volatile uint8_t xdata g_rw_gam_lut_0318h_GAM_LUT_792;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B18h
extern volatile uint8_t xdata g_rw_gam_lut_0319h_GAM_LUT_793;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B19h
extern volatile uint8_t xdata g_rw_gam_lut_031Ah_GAM_LUT_794;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B1Ah

extern volatile uint8_t xdata g_rw_gam_lut_031Bh_GAM_LUT_795;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B1Bh
extern volatile uint8_t xdata g_rw_gam_lut_031Ch_GAM_LUT_796;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B1Ch
extern volatile uint8_t xdata g_rw_gam_lut_031Dh_GAM_LUT_797;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B1Dh
extern volatile uint8_t xdata g_rw_gam_lut_031Eh_GAM_LUT_798;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B1Eh
extern volatile uint8_t xdata g_rw_gam_lut_031Fh_GAM_LUT_799;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B1Fh

extern volatile uint8_t xdata g_rw_gam_lut_0320h_GAM_LUT_800;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B20h
extern volatile uint8_t xdata g_rw_gam_lut_0321h_GAM_LUT_801;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B21h
extern volatile uint8_t xdata g_rw_gam_lut_0322h_GAM_LUT_802;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B22h
extern volatile uint8_t xdata g_rw_gam_lut_0323h_GAM_LUT_803;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B23h
extern volatile uint8_t xdata g_rw_gam_lut_0324h_GAM_LUT_804;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B24h

extern volatile uint8_t xdata g_rw_gam_lut_0325h_GAM_LUT_805;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B25h
extern volatile uint8_t xdata g_rw_gam_lut_0326h_GAM_LUT_806;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B26h
extern volatile uint8_t xdata g_rw_gam_lut_0327h_GAM_LUT_807;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B27h
extern volatile uint8_t xdata g_rw_gam_lut_0328h_GAM_LUT_808;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B28h
extern volatile uint8_t xdata g_rw_gam_lut_0329h_GAM_LUT_809;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B29h

extern volatile uint8_t xdata g_rw_gam_lut_032Ah_GAM_LUT_810;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B2Ah
extern volatile uint8_t xdata g_rw_gam_lut_032Bh_GAM_LUT_811;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B2Bh
extern volatile uint8_t xdata g_rw_gam_lut_032Ch_GAM_LUT_812;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B2Ch
extern volatile uint8_t xdata g_rw_gam_lut_032Dh_GAM_LUT_813;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B2Dh
extern volatile uint8_t xdata g_rw_gam_lut_032Eh_GAM_LUT_814;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B2Eh

extern volatile uint8_t xdata g_rw_gam_lut_032Fh_GAM_LUT_815;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B2Fh
extern volatile uint8_t xdata g_rw_gam_lut_0330h_GAM_LUT_816;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B30h
extern volatile uint8_t xdata g_rw_gam_lut_0331h_GAM_LUT_817;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B31h
extern volatile uint8_t xdata g_rw_gam_lut_0332h_GAM_LUT_818;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B32h
extern volatile uint8_t xdata g_rw_gam_lut_0333h_GAM_LUT_819;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B33h

extern volatile uint8_t xdata g_rw_gam_lut_0334h_GAM_LUT_820;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B34h
extern volatile uint8_t xdata g_rw_gam_lut_0335h_GAM_LUT_821;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B35h
extern volatile uint8_t xdata g_rw_gam_lut_0336h_GAM_LUT_822;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B36h
extern volatile uint8_t xdata g_rw_gam_lut_0337h_GAM_LUT_823;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B37h
extern volatile uint8_t xdata g_rw_gam_lut_0338h_GAM_LUT_824;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B38h

extern volatile uint8_t xdata g_rw_gam_lut_0339h_GAM_LUT_825;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B39h
extern volatile uint8_t xdata g_rw_gam_lut_033Ah_GAM_LUT_826;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B3Ah
extern volatile uint8_t xdata g_rw_gam_lut_033Bh_GAM_LUT_827;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B3Bh
extern volatile uint8_t xdata g_rw_gam_lut_033Ch_GAM_LUT_828;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B3Ch
extern volatile uint8_t xdata g_rw_gam_lut_033Dh_GAM_LUT_829;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B3Dh

extern volatile uint8_t xdata g_rw_gam_lut_033Eh_GAM_LUT_830;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B3Eh
extern volatile uint8_t xdata g_rw_gam_lut_033Fh_GAM_LUT_831;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B3Fh
extern volatile uint8_t xdata g_rw_gam_lut_0340h_GAM_LUT_832;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B40h
extern volatile uint8_t xdata g_rw_gam_lut_0341h_GAM_LUT_833;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B41h
extern volatile uint8_t xdata g_rw_gam_lut_0342h_GAM_LUT_834;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B42h

extern volatile uint8_t xdata g_rw_gam_lut_0343h_GAM_LUT_835;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B43h
extern volatile uint8_t xdata g_rw_gam_lut_0344h_GAM_LUT_836;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B44h
extern volatile uint8_t xdata g_rw_gam_lut_0345h_GAM_LUT_837;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B45h
extern volatile uint8_t xdata g_rw_gam_lut_0346h_GAM_LUT_838;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B46h
extern volatile uint8_t xdata g_rw_gam_lut_0347h_GAM_LUT_839;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B47h

extern volatile uint8_t xdata g_rw_gam_lut_0348h_GAM_LUT_840;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B48h
extern volatile uint8_t xdata g_rw_gam_lut_0349h_GAM_LUT_841;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B49h
extern volatile uint8_t xdata g_rw_gam_lut_034Ah_GAM_LUT_842;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B4Ah
extern volatile uint8_t xdata g_rw_gam_lut_034Bh_GAM_LUT_843;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B4Bh
extern volatile uint8_t xdata g_rw_gam_lut_034Ch_GAM_LUT_844;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B4Ch

extern volatile uint8_t xdata g_rw_gam_lut_034Dh_GAM_LUT_845;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B4Dh
extern volatile uint8_t xdata g_rw_gam_lut_034Eh_GAM_LUT_846;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B4Eh
extern volatile uint8_t xdata g_rw_gam_lut_034Fh_GAM_LUT_847;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B4Fh
extern volatile uint8_t xdata g_rw_gam_lut_0350h_GAM_LUT_848;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B50h
extern volatile uint8_t xdata g_rw_gam_lut_0351h_GAM_LUT_849;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B51h

extern volatile uint8_t xdata g_rw_gam_lut_0352h_GAM_LUT_850;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B52h
extern volatile uint8_t xdata g_rw_gam_lut_0353h_GAM_LUT_851;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B53h
extern volatile uint8_t xdata g_rw_gam_lut_0354h_GAM_LUT_852;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B54h
extern volatile uint8_t xdata g_rw_gam_lut_0355h_GAM_LUT_853;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B55h
extern volatile uint8_t xdata g_rw_gam_lut_0356h_GAM_LUT_854;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B56h

extern volatile uint8_t xdata g_rw_gam_lut_0357h_GAM_LUT_855;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B57h
extern volatile uint8_t xdata g_rw_gam_lut_0358h_GAM_LUT_856;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B58h
extern volatile uint8_t xdata g_rw_gam_lut_0359h_GAM_LUT_857;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B59h
extern volatile uint8_t xdata g_rw_gam_lut_035Ah_GAM_LUT_858;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B5Ah
extern volatile uint8_t xdata g_rw_gam_lut_035Bh_GAM_LUT_859;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B5Bh

extern volatile uint8_t xdata g_rw_gam_lut_035Ch_GAM_LUT_860;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B5Ch
extern volatile uint8_t xdata g_rw_gam_lut_035Dh_GAM_LUT_861;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B5Dh
extern volatile uint8_t xdata g_rw_gam_lut_035Eh_GAM_LUT_862;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B5Eh
extern volatile uint8_t xdata g_rw_gam_lut_035Fh_GAM_LUT_863;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B5Fh
extern volatile uint8_t xdata g_rw_gam_lut_0360h_GAM_LUT_864;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B60h

extern volatile uint8_t xdata g_rw_gam_lut_0361h_GAM_LUT_865;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B61h
extern volatile uint8_t xdata g_rw_gam_lut_0362h_GAM_LUT_866;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B62h
extern volatile uint8_t xdata g_rw_gam_lut_0363h_GAM_LUT_867;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B63h
extern volatile uint8_t xdata g_rw_gam_lut_0364h_GAM_LUT_868;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B64h
extern volatile uint8_t xdata g_rw_gam_lut_0365h_GAM_LUT_869;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B65h

extern volatile uint8_t xdata g_rw_gam_lut_0366h_GAM_LUT_870;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B66h
extern volatile uint8_t xdata g_rw_gam_lut_0367h_GAM_LUT_871;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B67h
extern volatile uint8_t xdata g_rw_gam_lut_0368h_GAM_LUT_872;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B68h
extern volatile uint8_t xdata g_rw_gam_lut_0369h_GAM_LUT_873;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B69h
extern volatile uint8_t xdata g_rw_gam_lut_036Ah_GAM_LUT_874;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B6Ah

extern volatile uint8_t xdata g_rw_gam_lut_036Bh_GAM_LUT_875;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B6Bh
extern volatile uint8_t xdata g_rw_gam_lut_036Ch_GAM_LUT_876;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B6Ch
extern volatile uint8_t xdata g_rw_gam_lut_036Dh_GAM_LUT_877;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B6Dh
extern volatile uint8_t xdata g_rw_gam_lut_036Eh_GAM_LUT_878;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B6Eh
extern volatile uint8_t xdata g_rw_gam_lut_036Fh_GAM_LUT_879;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B6Fh

extern volatile uint8_t xdata g_rw_gam_lut_0370h_GAM_LUT_880;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B70h
extern volatile uint8_t xdata g_rw_gam_lut_0371h_GAM_LUT_881;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B71h
extern volatile uint8_t xdata g_rw_gam_lut_0372h_GAM_LUT_882;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B72h
extern volatile uint8_t xdata g_rw_gam_lut_0373h_GAM_LUT_883;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B73h
extern volatile uint8_t xdata g_rw_gam_lut_0374h_GAM_LUT_884;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B74h

extern volatile uint8_t xdata g_rw_gam_lut_0375h_GAM_LUT_885;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B75h
extern volatile uint8_t xdata g_rw_gam_lut_0376h_GAM_LUT_886;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B76h
extern volatile uint8_t xdata g_rw_gam_lut_0377h_GAM_LUT_887;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B77h
extern volatile uint8_t xdata g_rw_gam_lut_0378h_GAM_LUT_888;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B78h
extern volatile uint8_t xdata g_rw_gam_lut_0379h_GAM_LUT_889;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B79h

extern volatile uint8_t xdata g_rw_gam_lut_037Ah_GAM_LUT_890;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B7Ah
extern volatile uint8_t xdata g_rw_gam_lut_037Bh_GAM_LUT_891;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B7Bh
extern volatile uint8_t xdata g_rw_gam_lut_037Ch_GAM_LUT_892;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B7Ch
extern volatile uint8_t xdata g_rw_gam_lut_037Dh_GAM_LUT_893;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B7Dh
extern volatile uint8_t xdata g_rw_gam_lut_037Eh_GAM_LUT_894;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B7Eh

extern volatile uint8_t xdata g_rw_gam_lut_037Fh_GAM_LUT_895;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B7Fh
extern volatile uint8_t xdata g_rw_gam_lut_0380h_GAM_LUT_896;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B80h
extern volatile uint8_t xdata g_rw_gam_lut_0381h_GAM_LUT_897;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B81h
extern volatile uint8_t xdata g_rw_gam_lut_0382h_GAM_LUT_898;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B82h
extern volatile uint8_t xdata g_rw_gam_lut_0383h_GAM_LUT_899;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B83h

extern volatile uint8_t xdata g_rw_gam_lut_0384h_GAM_LUT_900;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B84h
extern volatile uint8_t xdata g_rw_gam_lut_0385h_GAM_LUT_901;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B85h
extern volatile uint8_t xdata g_rw_gam_lut_0386h_GAM_LUT_902;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B86h
extern volatile uint8_t xdata g_rw_gam_lut_0387h_GAM_LUT_903;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B87h
extern volatile uint8_t xdata g_rw_gam_lut_0388h_GAM_LUT_904;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B88h

extern volatile uint8_t xdata g_rw_gam_lut_0389h_GAM_LUT_905;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B89h
extern volatile uint8_t xdata g_rw_gam_lut_038Ah_GAM_LUT_906;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B8Ah
extern volatile uint8_t xdata g_rw_gam_lut_038Bh_GAM_LUT_907;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B8Bh
extern volatile uint8_t xdata g_rw_gam_lut_038Ch_GAM_LUT_908;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B8Ch
extern volatile uint8_t xdata g_rw_gam_lut_038Dh_GAM_LUT_909;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B8Dh

extern volatile uint8_t xdata g_rw_gam_lut_038Eh_GAM_LUT_910;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B8Eh
extern volatile uint8_t xdata g_rw_gam_lut_038Fh_GAM_LUT_911;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B8Fh
extern volatile uint8_t xdata g_rw_gam_lut_0390h_GAM_LUT_912;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B90h
extern volatile uint8_t xdata g_rw_gam_lut_0391h_GAM_LUT_913;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B91h
extern volatile uint8_t xdata g_rw_gam_lut_0392h_GAM_LUT_914;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B92h

extern volatile uint8_t xdata g_rw_gam_lut_0393h_GAM_LUT_915;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B93h
extern volatile uint8_t xdata g_rw_gam_lut_0394h_GAM_LUT_916;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B94h
extern volatile uint8_t xdata g_rw_gam_lut_0395h_GAM_LUT_917;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B95h
extern volatile uint8_t xdata g_rw_gam_lut_0396h_GAM_LUT_918;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B96h
extern volatile uint8_t xdata g_rw_gam_lut_0397h_GAM_LUT_919;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B97h

extern volatile uint8_t xdata g_rw_gam_lut_0398h_GAM_LUT_920;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B98h
extern volatile uint8_t xdata g_rw_gam_lut_0399h_GAM_LUT_921;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B99h
extern volatile uint8_t xdata g_rw_gam_lut_039Ah_GAM_LUT_922;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B9Ah
extern volatile uint8_t xdata g_rw_gam_lut_039Bh_GAM_LUT_923;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B9Bh
extern volatile uint8_t xdata g_rw_gam_lut_039Ch_GAM_LUT_924;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B9Ch

extern volatile uint8_t xdata g_rw_gam_lut_039Dh_GAM_LUT_925;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B9Dh
extern volatile uint8_t xdata g_rw_gam_lut_039Eh_GAM_LUT_926;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B9Eh
extern volatile uint8_t xdata g_rw_gam_lut_039Fh_GAM_LUT_927;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7B9Fh
extern volatile uint8_t xdata g_rw_gam_lut_03A0h_GAM_LUT_928;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BA0h
extern volatile uint8_t xdata g_rw_gam_lut_03A1h_GAM_LUT_929;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BA1h

extern volatile uint8_t xdata g_rw_gam_lut_03A2h_GAM_LUT_930;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BA2h
extern volatile uint8_t xdata g_rw_gam_lut_03A3h_GAM_LUT_931;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BA3h
extern volatile uint8_t xdata g_rw_gam_lut_03A4h_GAM_LUT_932;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BA4h
extern volatile uint8_t xdata g_rw_gam_lut_03A5h_GAM_LUT_933;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BA5h
extern volatile uint8_t xdata g_rw_gam_lut_03A6h_GAM_LUT_934;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BA6h

extern volatile uint8_t xdata g_rw_gam_lut_03A7h_GAM_LUT_935;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BA7h
extern volatile uint8_t xdata g_rw_gam_lut_03A8h_GAM_LUT_936;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BA8h
extern volatile uint8_t xdata g_rw_gam_lut_03A9h_GAM_LUT_937;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BA9h
extern volatile uint8_t xdata g_rw_gam_lut_03AAh_GAM_LUT_938;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BAAh
extern volatile uint8_t xdata g_rw_gam_lut_03ABh_GAM_LUT_939;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BABh

extern volatile uint8_t xdata g_rw_gam_lut_03ACh_GAM_LUT_940;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BACh
extern volatile uint8_t xdata g_rw_gam_lut_03ADh_GAM_LUT_941;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BADh
extern volatile uint8_t xdata g_rw_gam_lut_03AEh_GAM_LUT_942;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BAEh
extern volatile uint8_t xdata g_rw_gam_lut_03AFh_GAM_LUT_943;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BAFh
extern volatile uint8_t xdata g_rw_gam_lut_03B0h_GAM_LUT_944;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BB0h

extern volatile uint8_t xdata g_rw_gam_lut_03B1h_GAM_LUT_945;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BB1h
extern volatile uint8_t xdata g_rw_gam_lut_03B2h_GAM_LUT_946;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BB2h
extern volatile uint8_t xdata g_rw_gam_lut_03B3h_GAM_LUT_947;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BB3h
extern volatile uint8_t xdata g_rw_gam_lut_03B4h_GAM_LUT_948;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BB4h
extern volatile uint8_t xdata g_rw_gam_lut_03B5h_GAM_LUT_949;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BB5h

extern volatile uint8_t xdata g_rw_gam_lut_03B6h_GAM_LUT_950;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BB6h
extern volatile uint8_t xdata g_rw_gam_lut_03B7h_GAM_LUT_951;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BB7h
extern volatile uint8_t xdata g_rw_gam_lut_03B8h_GAM_LUT_952;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BB8h
extern volatile uint8_t xdata g_rw_gam_lut_03B9h_GAM_LUT_953;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BB9h
extern volatile uint8_t xdata g_rw_gam_lut_03BAh_GAM_LUT_954;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BBAh

extern volatile uint8_t xdata g_rw_gam_lut_03BBh_GAM_LUT_955;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BBBh
extern volatile uint8_t xdata g_rw_gam_lut_03BCh_GAM_LUT_956;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BBCh
extern volatile uint8_t xdata g_rw_gam_lut_03BDh_GAM_LUT_957;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BBDh
extern volatile uint8_t xdata g_rw_gam_lut_03BEh_GAM_LUT_958;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BBEh
extern volatile uint8_t xdata g_rw_gam_lut_03BFh_GAM_LUT_959;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BBFh

extern volatile uint8_t xdata g_rw_gam_lut_03C0h_GAM_LUT_960;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BC0h
extern volatile uint8_t xdata g_rw_gam_lut_03C1h_GAM_LUT_961;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BC1h
extern volatile uint8_t xdata g_rw_gam_lut_03C2h_GAM_LUT_962;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BC2h
extern volatile uint8_t xdata g_rw_gam_lut_03C3h_GAM_LUT_963;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BC3h
extern volatile uint8_t xdata g_rw_gam_lut_03C4h_GAM_LUT_964;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BC4h

extern volatile uint8_t xdata g_rw_gam_lut_03C5h_GAM_LUT_965;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BC5h
extern volatile uint8_t xdata g_rw_gam_lut_03C6h_GAM_LUT_966;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BC6h
extern volatile uint8_t xdata g_rw_gam_lut_03C7h_GAM_LUT_967;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BC7h
extern volatile uint8_t xdata g_rw_gam_lut_03C8h_GAM_LUT_968;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BC8h
extern volatile uint8_t xdata g_rw_gam_lut_03C9h_GAM_LUT_969;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BC9h

extern volatile uint8_t xdata g_rw_gam_lut_03CAh_GAM_LUT_970;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BCAh
extern volatile uint8_t xdata g_rw_gam_lut_03CBh_GAM_LUT_971;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BCBh
extern volatile uint8_t xdata g_rw_gam_lut_03CCh_GAM_LUT_972;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BCCh
extern volatile uint8_t xdata g_rw_gam_lut_03CDh_GAM_LUT_973;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BCDh
extern volatile uint8_t xdata g_rw_gam_lut_03CEh_GAM_LUT_974;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BCEh

extern volatile uint8_t xdata g_rw_gam_lut_03CFh_GAM_LUT_975;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BCFh
extern volatile uint8_t xdata g_rw_gam_lut_03D0h_GAM_LUT_976;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BD0h
extern volatile uint8_t xdata g_rw_gam_lut_03D1h_GAM_LUT_977;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BD1h
extern volatile uint8_t xdata g_rw_gam_lut_03D2h_GAM_LUT_978;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BD2h
extern volatile uint8_t xdata g_rw_gam_lut_03D3h_GAM_LUT_979;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BD3h

extern volatile uint8_t xdata g_rw_gam_lut_03D4h_GAM_LUT_980;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BD4h
extern volatile uint8_t xdata g_rw_gam_lut_03D5h_GAM_LUT_981;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BD5h
extern volatile uint8_t xdata g_rw_gam_lut_03D6h_GAM_LUT_982;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BD6h
extern volatile uint8_t xdata g_rw_gam_lut_03D7h_GAM_LUT_983;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BD7h
extern volatile uint8_t xdata g_rw_gam_lut_03D8h_GAM_LUT_984;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BD8h

extern volatile uint8_t xdata g_rw_gam_lut_03D9h_GAM_LUT_985;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BD9h
extern volatile uint8_t xdata g_rw_gam_lut_03DAh_GAM_LUT_986;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BDAh
extern volatile uint8_t xdata g_rw_gam_lut_03DBh_GAM_LUT_987;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BDBh
extern volatile uint8_t xdata g_rw_gam_lut_03DCh_GAM_LUT_988;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BDCh
extern volatile uint8_t xdata g_rw_gam_lut_03DDh_GAM_LUT_989;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BDDh

extern volatile uint8_t xdata g_rw_gam_lut_03DEh_GAM_LUT_990;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BDEh
extern volatile uint8_t xdata g_rw_gam_lut_03DFh_GAM_LUT_991;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BDFh
extern volatile uint8_t xdata g_rw_gam_lut_03E0h_GAM_LUT_992;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BE0h
extern volatile uint8_t xdata g_rw_gam_lut_03E1h_GAM_LUT_993;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BE1h
extern volatile uint8_t xdata g_rw_gam_lut_03E2h_GAM_LUT_994;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BE2h

extern volatile uint8_t xdata g_rw_gam_lut_03E3h_GAM_LUT_995;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BE3h
extern volatile uint8_t xdata g_rw_gam_lut_03E4h_GAM_LUT_996;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BE4h
extern volatile uint8_t xdata g_rw_gam_lut_03E5h_GAM_LUT_997;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BE5h
extern volatile uint8_t xdata g_rw_gam_lut_03E6h_GAM_LUT_998;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BE6h
extern volatile uint8_t xdata g_rw_gam_lut_03E7h_GAM_LUT_999;                                            // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BE7h

extern volatile uint8_t xdata g_rw_gam_lut_03E8h_GAM_LUT_1000;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BE8h
extern volatile uint8_t xdata g_rw_gam_lut_03E9h_GAM_LUT_1001;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BE9h
extern volatile uint8_t xdata g_rw_gam_lut_03EAh_GAM_LUT_1002;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BEAh
extern volatile uint8_t xdata g_rw_gam_lut_03EBh_GAM_LUT_1003;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BEBh
extern volatile uint8_t xdata g_rw_gam_lut_03ECh_GAM_LUT_1004;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BECh

extern volatile uint8_t xdata g_rw_gam_lut_03EDh_GAM_LUT_1005;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BEDh
extern volatile uint8_t xdata g_rw_gam_lut_03EEh_GAM_LUT_1006;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BEEh
extern volatile uint8_t xdata g_rw_gam_lut_03EFh_GAM_LUT_1007;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BEFh
extern volatile uint8_t xdata g_rw_gam_lut_03F0h_GAM_LUT_1008;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BF0h
extern volatile uint8_t xdata g_rw_gam_lut_03F1h_GAM_LUT_1009;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BF1h

extern volatile uint8_t xdata g_rw_gam_lut_03F2h_GAM_LUT_1010;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BF2h
extern volatile uint8_t xdata g_rw_gam_lut_03F3h_GAM_LUT_1011;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BF3h
extern volatile uint8_t xdata g_rw_gam_lut_03F4h_GAM_LUT_1012;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BF4h
extern volatile uint8_t xdata g_rw_gam_lut_03F5h_GAM_LUT_1013;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BF5h
extern volatile uint8_t xdata g_rw_gam_lut_03F6h_GAM_LUT_1014;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BF6h

extern volatile uint8_t xdata g_rw_gam_lut_03F7h_GAM_LUT_1015;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BF7h
extern volatile uint8_t xdata g_rw_gam_lut_03F8h_GAM_LUT_1016;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BF8h
extern volatile uint8_t xdata g_rw_gam_lut_03F9h_GAM_LUT_1017;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BF9h
extern volatile uint8_t xdata g_rw_gam_lut_03FAh_GAM_LUT_1018;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BFAh
extern volatile uint8_t xdata g_rw_gam_lut_03FBh_GAM_LUT_1019;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BFBh

extern volatile uint8_t xdata g_rw_gam_lut_03FCh_GAM_LUT_1020;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BFCh
extern volatile uint8_t xdata g_rw_gam_lut_03FDh_GAM_LUT_1021;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BFDh
extern volatile uint8_t xdata g_rw_gam_lut_03FEh_GAM_LUT_1022;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BFEh
extern volatile uint8_t xdata g_rw_gam_lut_03FFh_GAM_LUT_1023;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7BFFh
extern volatile uint8_t xdata g_rw_gam_lut_0400h_GAM_LUT_1024;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C00h

extern volatile uint8_t xdata g_rw_gam_lut_0401h_GAM_LUT_1025;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C01h
extern volatile uint8_t xdata g_rw_gam_lut_0402h_GAM_LUT_1026;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C02h
extern volatile uint8_t xdata g_rw_gam_lut_0403h_GAM_LUT_1027;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C03h
extern volatile uint8_t xdata g_rw_gam_lut_0404h_GAM_LUT_1028;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C04h
extern volatile uint8_t xdata g_rw_gam_lut_0405h_GAM_LUT_1029;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C05h

extern volatile uint8_t xdata g_rw_gam_lut_0406h_GAM_LUT_1030;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C06h
extern volatile uint8_t xdata g_rw_gam_lut_0407h_GAM_LUT_1031;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C07h
extern volatile uint8_t xdata g_rw_gam_lut_0408h_GAM_LUT_1032;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C08h
extern volatile uint8_t xdata g_rw_gam_lut_0409h_GAM_LUT_1033;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C09h
extern volatile uint8_t xdata g_rw_gam_lut_040Ah_GAM_LUT_1034;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C0Ah

extern volatile uint8_t xdata g_rw_gam_lut_040Bh_GAM_LUT_1035;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C0Bh
extern volatile uint8_t xdata g_rw_gam_lut_040Ch_GAM_LUT_1036;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C0Ch
extern volatile uint8_t xdata g_rw_gam_lut_040Dh_GAM_LUT_1037;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C0Dh
extern volatile uint8_t xdata g_rw_gam_lut_040Eh_GAM_LUT_1038;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C0Eh
extern volatile uint8_t xdata g_rw_gam_lut_040Fh_GAM_LUT_1039;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C0Fh

extern volatile uint8_t xdata g_rw_gam_lut_0410h_GAM_LUT_1040;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C10h
extern volatile uint8_t xdata g_rw_gam_lut_0411h_GAM_LUT_1041;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C11h
extern volatile uint8_t xdata g_rw_gam_lut_0412h_GAM_LUT_1042;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C12h
extern volatile uint8_t xdata g_rw_gam_lut_0413h_GAM_LUT_1043;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C13h
extern volatile uint8_t xdata g_rw_gam_lut_0414h_GAM_LUT_1044;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C14h

extern volatile uint8_t xdata g_rw_gam_lut_0415h_GAM_LUT_1045;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C15h
extern volatile uint8_t xdata g_rw_gam_lut_0416h_GAM_LUT_1046;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C16h
extern volatile uint8_t xdata g_rw_gam_lut_0417h_GAM_LUT_1047;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C17h
extern volatile uint8_t xdata g_rw_gam_lut_0418h_GAM_LUT_1048;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C18h
extern volatile uint8_t xdata g_rw_gam_lut_0419h_GAM_LUT_1049;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C19h

extern volatile uint8_t xdata g_rw_gam_lut_041Ah_GAM_LUT_1050;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C1Ah
extern volatile uint8_t xdata g_rw_gam_lut_041Bh_GAM_LUT_1051;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C1Bh
extern volatile uint8_t xdata g_rw_gam_lut_041Ch_GAM_LUT_1052;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C1Ch
extern volatile uint8_t xdata g_rw_gam_lut_041Dh_GAM_LUT_1053;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C1Dh
extern volatile uint8_t xdata g_rw_gam_lut_041Eh_GAM_LUT_1054;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C1Eh

extern volatile uint8_t xdata g_rw_gam_lut_041Fh_GAM_LUT_1055;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C1Fh
extern volatile uint8_t xdata g_rw_gam_lut_0420h_GAM_LUT_1056;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C20h
extern volatile uint8_t xdata g_rw_gam_lut_0421h_GAM_LUT_1057;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C21h
extern volatile uint8_t xdata g_rw_gam_lut_0422h_GAM_LUT_1058;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C22h
extern volatile uint8_t xdata g_rw_gam_lut_0423h_GAM_LUT_1059;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C23h

extern volatile uint8_t xdata g_rw_gam_lut_0424h_GAM_LUT_1060;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C24h
extern volatile uint8_t xdata g_rw_gam_lut_0425h_GAM_LUT_1061;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C25h
extern volatile uint8_t xdata g_rw_gam_lut_0426h_GAM_LUT_1062;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C26h
extern volatile uint8_t xdata g_rw_gam_lut_0427h_GAM_LUT_1063;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C27h
extern volatile uint8_t xdata g_rw_gam_lut_0428h_GAM_LUT_1064;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C28h

extern volatile uint8_t xdata g_rw_gam_lut_0429h_GAM_LUT_1065;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C29h
extern volatile uint8_t xdata g_rw_gam_lut_042Ah_GAM_LUT_1066;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C2Ah
extern volatile uint8_t xdata g_rw_gam_lut_042Bh_GAM_LUT_1067;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C2Bh
extern volatile uint8_t xdata g_rw_gam_lut_042Ch_GAM_LUT_1068;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C2Ch
extern volatile uint8_t xdata g_rw_gam_lut_042Dh_GAM_LUT_1069;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C2Dh

extern volatile uint8_t xdata g_rw_gam_lut_042Eh_GAM_LUT_1070;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C2Eh
extern volatile uint8_t xdata g_rw_gam_lut_042Fh_GAM_LUT_1071;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C2Fh
extern volatile uint8_t xdata g_rw_gam_lut_0430h_GAM_LUT_1072;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C30h
extern volatile uint8_t xdata g_rw_gam_lut_0431h_GAM_LUT_1073;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C31h
extern volatile uint8_t xdata g_rw_gam_lut_0432h_GAM_LUT_1074;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C32h

extern volatile uint8_t xdata g_rw_gam_lut_0433h_GAM_LUT_1075;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C33h
extern volatile uint8_t xdata g_rw_gam_lut_0434h_GAM_LUT_1076;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C34h
extern volatile uint8_t xdata g_rw_gam_lut_0435h_GAM_LUT_1077;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C35h
extern volatile uint8_t xdata g_rw_gam_lut_0436h_GAM_LUT_1078;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C36h
extern volatile uint8_t xdata g_rw_gam_lut_0437h_GAM_LUT_1079;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C37h

extern volatile uint8_t xdata g_rw_gam_lut_0438h_GAM_LUT_1080;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C38h
extern volatile uint8_t xdata g_rw_gam_lut_0439h_GAM_LUT_1081;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C39h
extern volatile uint8_t xdata g_rw_gam_lut_043Ah_GAM_LUT_1082;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C3Ah
extern volatile uint8_t xdata g_rw_gam_lut_043Bh_GAM_LUT_1083;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C3Bh
extern volatile uint8_t xdata g_rw_gam_lut_043Ch_GAM_LUT_1084;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C3Ch

extern volatile uint8_t xdata g_rw_gam_lut_043Dh_GAM_LUT_1085;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C3Dh
extern volatile uint8_t xdata g_rw_gam_lut_043Eh_GAM_LUT_1086;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C3Eh
extern volatile uint8_t xdata g_rw_gam_lut_043Fh_GAM_LUT_1087;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C3Fh
extern volatile uint8_t xdata g_rw_gam_lut_0440h_GAM_LUT_1088;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C40h
extern volatile uint8_t xdata g_rw_gam_lut_0441h_GAM_LUT_1089;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C41h

extern volatile uint8_t xdata g_rw_gam_lut_0442h_GAM_LUT_1090;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C42h
extern volatile uint8_t xdata g_rw_gam_lut_0443h_GAM_LUT_1091;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C43h
extern volatile uint8_t xdata g_rw_gam_lut_0444h_GAM_LUT_1092;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C44h
extern volatile uint8_t xdata g_rw_gam_lut_0445h_GAM_LUT_1093;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C45h
extern volatile uint8_t xdata g_rw_gam_lut_0446h_GAM_LUT_1094;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C46h

extern volatile uint8_t xdata g_rw_gam_lut_0447h_GAM_LUT_1095;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C47h
extern volatile uint8_t xdata g_rw_gam_lut_0448h_GAM_LUT_1096;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C48h
extern volatile uint8_t xdata g_rw_gam_lut_0449h_GAM_LUT_1097;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C49h
extern volatile uint8_t xdata g_rw_gam_lut_044Ah_GAM_LUT_1098;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C4Ah
extern volatile uint8_t xdata g_rw_gam_lut_044Bh_GAM_LUT_1099;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C4Bh

extern volatile uint8_t xdata g_rw_gam_lut_044Ch_GAM_LUT_1100;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C4Ch
extern volatile uint8_t xdata g_rw_gam_lut_044Dh_GAM_LUT_1101;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C4Dh
extern volatile uint8_t xdata g_rw_gam_lut_044Eh_GAM_LUT_1102;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C4Eh
extern volatile uint8_t xdata g_rw_gam_lut_044Fh_GAM_LUT_1103;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C4Fh
extern volatile uint8_t xdata g_rw_gam_lut_0450h_GAM_LUT_1104;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C50h

extern volatile uint8_t xdata g_rw_gam_lut_0451h_GAM_LUT_1105;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C51h
extern volatile uint8_t xdata g_rw_gam_lut_0452h_GAM_LUT_1106;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C52h
extern volatile uint8_t xdata g_rw_gam_lut_0453h_GAM_LUT_1107;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C53h
extern volatile uint8_t xdata g_rw_gam_lut_0454h_GAM_LUT_1108;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C54h
extern volatile uint8_t xdata g_rw_gam_lut_0455h_GAM_LUT_1109;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C55h

extern volatile uint8_t xdata g_rw_gam_lut_0456h_GAM_LUT_1110;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C56h
extern volatile uint8_t xdata g_rw_gam_lut_0457h_GAM_LUT_1111;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C57h
extern volatile uint8_t xdata g_rw_gam_lut_0458h_GAM_LUT_1112;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C58h
extern volatile uint8_t xdata g_rw_gam_lut_0459h_GAM_LUT_1113;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C59h
extern volatile uint8_t xdata g_rw_gam_lut_045Ah_GAM_LUT_1114;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C5Ah

extern volatile uint8_t xdata g_rw_gam_lut_045Bh_GAM_LUT_1115;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C5Bh
extern volatile uint8_t xdata g_rw_gam_lut_045Ch_GAM_LUT_1116;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C5Ch
extern volatile uint8_t xdata g_rw_gam_lut_045Dh_GAM_LUT_1117;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C5Dh
extern volatile uint8_t xdata g_rw_gam_lut_045Eh_GAM_LUT_1118;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C5Eh
extern volatile uint8_t xdata g_rw_gam_lut_045Fh_GAM_LUT_1119;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C5Fh

extern volatile uint8_t xdata g_rw_gam_lut_0460h_GAM_LUT_1120;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C60h
extern volatile uint8_t xdata g_rw_gam_lut_0461h_GAM_LUT_1121;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C61h
extern volatile uint8_t xdata g_rw_gam_lut_0462h_GAM_LUT_1122;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C62h
extern volatile uint8_t xdata g_rw_gam_lut_0463h_GAM_LUT_1123;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C63h
extern volatile uint8_t xdata g_rw_gam_lut_0464h_GAM_LUT_1124;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C64h

extern volatile uint8_t xdata g_rw_gam_lut_0465h_GAM_LUT_1125;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C65h
extern volatile uint8_t xdata g_rw_gam_lut_0466h_GAM_LUT_1126;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C66h
extern volatile uint8_t xdata g_rw_gam_lut_0467h_GAM_LUT_1127;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C67h
extern volatile uint8_t xdata g_rw_gam_lut_0468h_GAM_LUT_1128;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C68h
extern volatile uint8_t xdata g_rw_gam_lut_0469h_GAM_LUT_1129;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C69h

extern volatile uint8_t xdata g_rw_gam_lut_046Ah_GAM_LUT_1130;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C6Ah
extern volatile uint8_t xdata g_rw_gam_lut_046Bh_GAM_LUT_1131;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C6Bh
extern volatile uint8_t xdata g_rw_gam_lut_046Ch_GAM_LUT_1132;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C6Ch
extern volatile uint8_t xdata g_rw_gam_lut_046Dh_GAM_LUT_1133;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C6Dh
extern volatile uint8_t xdata g_rw_gam_lut_046Eh_GAM_LUT_1134;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C6Eh

extern volatile uint8_t xdata g_rw_gam_lut_046Fh_GAM_LUT_1135;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C6Fh
extern volatile uint8_t xdata g_rw_gam_lut_0470h_GAM_LUT_1136;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C70h
extern volatile uint8_t xdata g_rw_gam_lut_0471h_GAM_LUT_1137;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C71h
extern volatile uint8_t xdata g_rw_gam_lut_0472h_GAM_LUT_1138;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C72h
extern volatile uint8_t xdata g_rw_gam_lut_0473h_GAM_LUT_1139;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C73h

extern volatile uint8_t xdata g_rw_gam_lut_0474h_GAM_LUT_1140;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C74h
extern volatile uint8_t xdata g_rw_gam_lut_0475h_GAM_LUT_1141;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C75h
extern volatile uint8_t xdata g_rw_gam_lut_0476h_GAM_LUT_1142;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C76h
extern volatile uint8_t xdata g_rw_gam_lut_0477h_GAM_LUT_1143;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C77h
extern volatile uint8_t xdata g_rw_gam_lut_0478h_GAM_LUT_1144;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C78h

extern volatile uint8_t xdata g_rw_gam_lut_0479h_GAM_LUT_1145;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C79h
extern volatile uint8_t xdata g_rw_gam_lut_047Ah_GAM_LUT_1146;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C7Ah
extern volatile uint8_t xdata g_rw_gam_lut_047Bh_GAM_LUT_1147;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C7Bh
extern volatile uint8_t xdata g_rw_gam_lut_047Ch_GAM_LUT_1148;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C7Ch
extern volatile uint8_t xdata g_rw_gam_lut_047Dh_GAM_LUT_1149;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C7Dh

extern volatile uint8_t xdata g_rw_gam_lut_047Eh_GAM_LUT_1150;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C7Eh
extern volatile uint8_t xdata g_rw_gam_lut_047Fh_GAM_LUT_1151;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C7Fh
extern volatile uint8_t xdata g_rw_gam_lut_0480h_GAM_LUT_1152;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C80h
extern volatile uint8_t xdata g_rw_gam_lut_0481h_GAM_LUT_1153;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C81h
extern volatile uint8_t xdata g_rw_gam_lut_0482h_GAM_LUT_1154;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C82h

extern volatile uint8_t xdata g_rw_gam_lut_0483h_GAM_LUT_1155;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C83h
extern volatile uint8_t xdata g_rw_gam_lut_0484h_GAM_LUT_1156;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C84h
extern volatile uint8_t xdata g_rw_gam_lut_0485h_GAM_LUT_1157;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C85h
extern volatile uint8_t xdata g_rw_gam_lut_0486h_GAM_LUT_1158;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C86h
extern volatile uint8_t xdata g_rw_gam_lut_0487h_GAM_LUT_1159;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C87h

extern volatile uint8_t xdata g_rw_gam_lut_0488h_GAM_LUT_1160;                                           // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 7C88h

#endif