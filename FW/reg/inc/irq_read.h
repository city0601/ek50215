#ifndef _IRQ_READ_H_
#define _IRQ_READ_H_
#include "reg_include.h"

union rw_irq_read_0000h
{
    unsigned char byte;
    struct
    {
        unsigned char bcc_lock_ev                                                            : 1;        // val = 0
        unsigned char bcc_busy_ev                                                            : 1;        // val = 0
        unsigned char bcc_done_ev                                                            : 1;        // val = 0
        unsigned char bcc_reg_finish_ev                                                      : 1;        // val = 0
        unsigned char reserved1                                                              : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_irq_read_0000h xdata g_rw_irq_read_0000h;    // Absolute Address = 4900h

union rw_irq_read_0001h
{
    unsigned char byte;
    struct
    {
        unsigned char vsync_ev                                                               : 1;        // val = 0
        unsigned char state_chg_ev                                                           : 1;        // val = 0
        unsigned char reg_finish_ev                                                          : 1;        // val = 0
        unsigned char lut_finish_ev                                                          : 1;        // val = 0
        unsigned char tcon_finish_ev                                                         : 1;        // val = 0
        unsigned char i2c_idle_ev                                                            : 1;        // val = 0
        unsigned char patdet_sdpol_ev                                                        : 1;        // val = 0
        unsigned char pwm2spi_ev                                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_irq_read_0001h xdata g_rw_irq_read_0001h;    // Absolute Address = 4901h

union rw_irq_read_0002h
{
    unsigned char byte;
    struct
    {
        unsigned char lvds_unlock_irq_ev                                                     : 1;        // val = 0
        unsigned char vx1_unlock_irq_ev                                                      : 1;        // val = 0
        unsigned char spll_unlock_irq_ev                                                     : 1;        // val = 0
        unsigned char mpll_unlock_irq_ev                                                     : 1;        // val = 0
        unsigned char p2p_unlock_irq_ev                                                      : 1;        // val = 0
        unsigned char sym_unlock_ev                                                          : 1;        // val = 0
        unsigned char rom_acc_ne_ev                                                          : 1;        // val = 0
        unsigned char rom_acc_pe_ev                                                          : 1;        // val = 0
    }bits;
};
extern volatile union rw_irq_read_0002h xdata g_rw_irq_read_0002h;    // Absolute Address = 4902h

union rw_irq_read_0003h
{
    unsigned char byte;
    struct
    {
        unsigned char inproc_irq_ev1                                                         : 1;        // val = 0
        unsigned char inproc_irq_ev2                                                         : 1;        // val = 0
        unsigned char outproc_irq_ev1                                                        : 1;        // val = 0
        unsigned char outproc_irq_ev2                                                        : 1;        // val = 0
        unsigned char frm_irq_ev1                                                            : 1;        // val = 0
        unsigned char frm_irq_ev2                                                            : 1;        // val = 0
        unsigned char cmd_finish_ev                                                          : 1;        // val = 0
        unsigned char out_esd_mask_ev                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_irq_read_0003h xdata g_rw_irq_read_0003h;    // Absolute Address = 4903h

union rw_irq_read_000Ch
{
    unsigned char byte;
    struct
    {
        unsigned char irq_bcc_st                                                             : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char irq_spidma_st                                                          : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char irq_rom_acc_st                                                         : 1;        // val = 0
        unsigned char irq_unlock_st                                                          : 1;        // val = 0
    }bits;
};
extern volatile union rw_irq_read_000Ch xdata g_rw_irq_read_000Ch;    // Absolute Address = 490Ch

//------------------------------------------------------------------------------------------------------------------------
union rw_irq_read_000Dh
{
    unsigned char byte;
    struct
    {
        unsigned char irq_unlock_st_bits_4_0                                                 : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char irq_inproc_st                                                          : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_irq_read_000Dh xdata g_rw_irq_read_000Dh;    // Absolute Address = 490Dh


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_irq_read_0004h_reserved2;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 4904h
extern volatile uint8_t xdata g_rw_irq_read_0005h_reserved2;                                             // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 4905h
extern volatile uint8_t xdata g_rw_irq_read_0006h_reserved2;                                             // [msb:lsb] = [23:16], val = 0, 	Absolute Address = 4906h
extern volatile uint8_t xdata g_rw_irq_read_0007h_reserved2;                                             // [msb:lsb] = [31:24], val = 0, 	Absolute Address = 4907h
extern volatile uint8_t xdata g_rw_irq_read_0008h_reserved2;                                             // [msb:lsb] = [39:32], val = 0, 	Absolute Address = 4908h

extern volatile uint8_t xdata g_rw_irq_read_0009h_irq_patdet_sdpol;                                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 4909h
extern volatile uint8_t xdata g_rw_irq_read_000Ah_irq_patdet_sdpol;                                      // [msb:lsb] = [15:8], val = 0, 	Absolute Address = 490Ah
extern volatile uint8_t xdata g_rw_irq_read_000Bh_irq_patdet_sdpol;                                      // [msb:lsb] = [23:16], val = 0, 	Absolute Address = 490Bh

#endif