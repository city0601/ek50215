#ifndef _NPAT_H_
#define _NPAT_H_
#include "reg_include.h"

union rw_npat_0000h
{
    unsigned char byte;
    struct
    {
        unsigned char r_disp_time_sel                                                        : 1;        // val = 0
        unsigned char r_f_pat_st                                                             : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_f_pat_end_bits_0                                                     : 1;        // val = 1
    }bits;
};
extern volatile union rw_npat_0000h xdata g_rw_npat_0000h;    // Absolute Address = 2800h

union rw_npat_0001h
{
    unsigned char byte;
    struct
    {
        unsigned char r_f_pat_end_bits_5_1                                                   : 5;        // [msb:lsb] = [5:1], val = 21
        unsigned char r_f_frame_num_bits_2_0                                                 : 3;        // [msb:lsb] = [2:0], val = 4
    }bits;
};
extern volatile union rw_npat_0001h xdata g_rw_npat_0001h;    // Absolute Address = 2801h

union rw_npat_0002h
{
    unsigned char byte;
    struct
    {
        unsigned char r_f_frame_num_bits_7_3                                                 : 5;        // [msb:lsb] = [7:3], val = 7
        unsigned char r_display_time_bits_2_0                                                : 3;        // [msb:lsb] = [2:0], val = 1
    }bits;
};
extern volatile union rw_npat_0002h xdata g_rw_npat_0002h;    // Absolute Address = 2802h

union rw_npat_0003h
{
    unsigned char byte;
    struct
    {
        unsigned char r_display_time_bits_4_3                                                : 2;        // [msb:lsb] = [4:3], val = 0
        unsigned char r_spat_idx_st                                                          : 6;        // [msb:lsb] = [5:0], val = 50
    }bits;
};
extern volatile union rw_npat_0003h xdata g_rw_npat_0003h;    // Absolute Address = 2803h

union rw_npat_0007h
{
    unsigned char byte;
    struct
    {
        unsigned char r_abnml_en_sel                                                         : 1;        // val = 0
        unsigned char r_gray_div8_en                                                         : 1;        // val = 0
        unsigned char r_abnml_degen_sel                                                      : 1;        // val = 0
        unsigned char r_gray_level_sel                                                       : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_init_frun_vs_en                                                      : 1;        // val = 0
        unsigned char r_init_aging_vs_en                                                     : 1;        // val = 0
        unsigned char r_vblk_vs_en                                                           : 1;        // val = 0
    }bits;
};
extern volatile union rw_npat_0007h xdata g_rw_npat_0007h;    // Absolute Address = 2807h

//------------------------------------------------------------------------------------------------------------------------
union rw_npat_0008h
{
    unsigned char byte;
    struct
    {
        unsigned char r_ramp_color                                                           : 3;        // [msb:lsb] = [2:0], val = 7
        unsigned char r_bar_s                                                                : 4;        // [msb:lsb] = [3:0], val = 1
        unsigned char r_bar_w_bits_0                                                         : 1;        // val = 0
    }bits;
};
extern volatile union rw_npat_0008h xdata g_rw_npat_0008h;    // Absolute Address = 2808h

union rw_npat_0009h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bar_w_bits_5_1                                                       : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_bar_r_bits_2_0                                                       : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_npat_0009h xdata g_rw_npat_0009h;    // Absolute Address = 2809h

union rw_npat_000Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_bar_r_bits_9_3                                                       : 7;        // [msb:lsb] = [9:3], val = 96
        unsigned char r_bar_g_bits_0                                                         : 1;        // val = 0
    }bits;
};
extern volatile union rw_npat_000Ah xdata g_rw_npat_000Ah;    // Absolute Address = 280Ah

union rw_npat_000Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_bar_g_bits_9                                                         : 1;        // val = 1
        unsigned char r_bar_b_bits_6_0                                                       : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_npat_000Ch xdata g_rw_npat_000Ch;    // Absolute Address = 280Ch

union rw_npat_000Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_bar_b_bits_9_7                                                       : 3;        // [msb:lsb] = [9:7], val = 6
        unsigned char r_gc_sel                                                               : 1;        // val = 0
        unsigned char r_gc_pat_y_phase                                                       : 1;        // val = 0
        unsigned char r_gc_pat_x_step                                                        : 1;        // val = 0
        unsigned char r_gc_pat_y_step                                                        : 1;        // val = 0
        unsigned char r_chk_sel_bits_0                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_npat_000Dh xdata g_rw_npat_000Dh;    // Absolute Address = 280Dh

//------------------------------------------------------------------------------------------------------------------------
union rw_npat_000Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_chk_sel_bits_1                                                       : 1;        // val = 0
        unsigned char r_chk_r_bits_6_0                                                       : 7;        // [msb:lsb] = [6:0], val = 127
    }bits;
};
extern volatile union rw_npat_000Eh xdata g_rw_npat_000Eh;    // Absolute Address = 280Eh

union rw_npat_000Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_chk_r_bits_9_7                                                       : 3;        // [msb:lsb] = [9:7], val = 7
        unsigned char r_chk_g_bits_4_0                                                       : 5;        // [msb:lsb] = [4:0], val = 31
    }bits;
};
extern volatile union rw_npat_000Fh xdata g_rw_npat_000Fh;    // Absolute Address = 280Fh

union rw_npat_0010h
{
    unsigned char byte;
    struct
    {
        unsigned char r_chk_g_bits_9_5                                                       : 5;        // [msb:lsb] = [9:5], val = 31
        unsigned char r_chk_b_bits_2_0                                                       : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_npat_0010h xdata g_rw_npat_0010h;    // Absolute Address = 2810h

union rw_npat_0011h
{
    unsigned char byte;
    struct
    {
        unsigned char r_chk_b_bits_9_3                                                       : 7;        // [msb:lsb] = [9:3], val = 127
        unsigned char r_arb_f1_num_bits_0                                                    : 1;        // val = 1
    }bits;
};
extern volatile union rw_npat_0011h xdata g_rw_npat_0011h;    // Absolute Address = 2811h

union rw_npat_0012h
{
    unsigned char byte;
    struct
    {
        unsigned char r_arb_f1_num_bits_7_1                                                  : 7;        // [msb:lsb] = [7:1], val = 127
        unsigned char r_arb_f3_num_bits_0                                                    : 1;        // val = 1
    }bits;
};
extern volatile union rw_npat_0012h xdata g_rw_npat_0012h;    // Absolute Address = 2812h

//------------------------------------------------------------------------------------------------------------------------
union rw_npat_0013h
{
    unsigned char byte;
    struct
    {
        unsigned char r_arb_f3_num_bits_7_1                                                  : 7;        // [msb:lsb] = [7:1], val = 63
        unsigned char r_arb_f4_num_bits_0                                                    : 1;        // val = 1
    }bits;
};
extern volatile union rw_npat_0013h xdata g_rw_npat_0013h;    // Absolute Address = 2813h

union rw_npat_0014h
{
    unsigned char byte;
    struct
    {
        unsigned char r_arb_f4_num_bits_7_1                                                  : 7;        // [msb:lsb] = [7:1], val = 31
        unsigned char r_sstrph_sel                                                           : 1;        // val = 0
    }bits;
};
extern volatile union rw_npat_0014h xdata g_rw_npat_0014h;    // Absolute Address = 2814h

union rw_npat_0015h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_pat_x_phase                                                       : 1;        // val = 0
        unsigned char r_pat_x_sel                                                            : 1;        // val = 0
        unsigned char r_slave_obey                                                           : 1;        // val = 0
        unsigned char r_f_fix_pat                                                            : 1;        // val = 0
        unsigned char r_hsc_s                                                                : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_npat_0015h xdata g_rw_npat_0015h;    // Absolute Address = 2815h

union rw_npat_0016h
{
    unsigned char byte;
    struct
    {
        unsigned char r_debug_sel                                                            : 1;        // val = 0
        unsigned char r_htot_rec_sel                                                         : 1;        // val = 0
        unsigned char r_div_htot_rec_sel                                                     : 1;        // val = 0
        unsigned char r_htot_cmp_reg_min_bits_4_0                                            : 5;        // [msb:lsb] = [4:0], val = 28
    }bits;
};
extern volatile union rw_npat_0016h xdata g_rw_npat_0016h;    // Absolute Address = 2816h

union rw_npat_0017h
{
    unsigned char byte;
    struct
    {
        unsigned char r_htot_cmp_reg_min_bits_11_5                                           : 7;        // [msb:lsb] = [11:5], val = 31
        unsigned char r_htot_rec_bits_0                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_npat_0017h xdata g_rw_npat_0017h;    // Absolute Address = 2817h

//------------------------------------------------------------------------------------------------------------------------
union rw_npat_0019h
{
    unsigned char byte;
    struct
    {
        unsigned char r_htot_rec_bits_11_9                                                   : 3;        // [msb:lsb] = [11:9], val = 2
        unsigned char r_htot_vair_bits_4_0                                                   : 5;        // [msb:lsb] = [4:0], val = 20
    }bits;
};
extern volatile union rw_npat_0019h xdata g_rw_npat_0019h;    // Absolute Address = 2819h

union rw_npat_001Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_htot_vair_bits_7_5                                                   : 3;        // [msb:lsb] = [7:5], val = 0
        unsigned char r_vact_ofs_bits_4_0                                                    : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_npat_001Ah xdata g_rw_npat_001Ah;    // Absolute Address = 281Ah

union rw_npat_001Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_vact_ofs_bits_7_5                                                    : 3;        // [msb:lsb] = [7:5], val = 0
        unsigned char r_n_vcnt_sel                                                           : 1;        // val = 0
        unsigned char r_prbs_fcyc_bits_3_0                                                   : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_npat_001Bh xdata g_rw_npat_001Bh;    // Absolute Address = 281Bh

union rw_npat_001Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_prbs_fcyc_bits_7_4                                                   : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char r_bist1_format_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_npat_001Ch xdata g_rw_npat_001Ch;    // Absolute Address = 281Ch

union rw_npat_001Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist1_format_bits_5_4                                                : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_bist2_format                                                         : 6;        // [msb:lsb] = [5:0], val = 1
    }bits;
};
extern volatile union rw_npat_001Dh xdata g_rw_npat_001Dh;    // Absolute Address = 281Dh

//------------------------------------------------------------------------------------------------------------------------
union rw_npat_001Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist3_format                                                         : 6;        // [msb:lsb] = [5:0], val = 2
        unsigned char r_bist4_format_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_npat_001Eh xdata g_rw_npat_001Eh;    // Absolute Address = 281Eh

union rw_npat_001Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist4_format_bits_5_2                                                : 4;        // [msb:lsb] = [5:2], val = 0
        unsigned char r_bist5_format_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 4
    }bits;
};
extern volatile union rw_npat_001Fh xdata g_rw_npat_001Fh;    // Absolute Address = 281Fh

union rw_npat_0020h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist5_format_bits_5_4                                                : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_bist6_format                                                         : 6;        // [msb:lsb] = [5:0], val = 5
    }bits;
};
extern volatile union rw_npat_0020h xdata g_rw_npat_0020h;    // Absolute Address = 2820h

union rw_npat_0021h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist7_format                                                         : 6;        // [msb:lsb] = [5:0], val = 6
        unsigned char r_bist8_format_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_npat_0021h xdata g_rw_npat_0021h;    // Absolute Address = 2821h

union rw_npat_0022h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist8_format_bits_5_2                                                : 4;        // [msb:lsb] = [5:2], val = 1
        unsigned char r_bist9_format_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_npat_0022h xdata g_rw_npat_0022h;    // Absolute Address = 2822h

//------------------------------------------------------------------------------------------------------------------------
union rw_npat_0023h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist9_format_bits_5_4                                                : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_bist10_format                                                        : 6;        // [msb:lsb] = [5:0], val = 9
    }bits;
};
extern volatile union rw_npat_0023h xdata g_rw_npat_0023h;    // Absolute Address = 2823h

union rw_npat_0024h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist11_format                                                        : 6;        // [msb:lsb] = [5:0], val = 10
        unsigned char r_bist12_format_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_npat_0024h xdata g_rw_npat_0024h;    // Absolute Address = 2824h

union rw_npat_0025h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist12_format_bits_5_2                                               : 4;        // [msb:lsb] = [5:2], val = 2
        unsigned char r_bist13_format_bits_3_0                                               : 4;        // [msb:lsb] = [3:0], val = 12
    }bits;
};
extern volatile union rw_npat_0025h xdata g_rw_npat_0025h;    // Absolute Address = 2825h

union rw_npat_0026h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist13_format_bits_5_4                                               : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_bist14_format                                                        : 6;        // [msb:lsb] = [5:0], val = 13
    }bits;
};
extern volatile union rw_npat_0026h xdata g_rw_npat_0026h;    // Absolute Address = 2826h

union rw_npat_0027h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist15_format                                                        : 6;        // [msb:lsb] = [5:0], val = 14
        unsigned char r_bist16_format_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_npat_0027h xdata g_rw_npat_0027h;    // Absolute Address = 2827h

//------------------------------------------------------------------------------------------------------------------------
union rw_npat_0028h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist16_format_bits_5_2                                               : 4;        // [msb:lsb] = [5:2], val = 3
        unsigned char r_bist1_display_time_bits_3_0                                          : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_npat_0028h xdata g_rw_npat_0028h;    // Absolute Address = 2828h

union rw_npat_0029h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist1_display_time_bits_4                                            : 1;        // val = 0
        unsigned char r_bist2_display_time                                                   : 5;        // [msb:lsb] = [4:0], val = 1
        unsigned char r_bist3_display_time_bits_1_0                                          : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_npat_0029h xdata g_rw_npat_0029h;    // Absolute Address = 2829h

union rw_npat_002Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist3_display_time_bits_4_2                                          : 3;        // [msb:lsb] = [4:2], val = 0
        unsigned char r_bist4_display_time                                                   : 5;        // [msb:lsb] = [4:0], val = 1
    }bits;
};
extern volatile union rw_npat_002Ah xdata g_rw_npat_002Ah;    // Absolute Address = 282Ah

union rw_npat_002Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist5_display_time                                                   : 5;        // [msb:lsb] = [4:0], val = 1
        unsigned char r_bist6_display_time_bits_2_0                                          : 3;        // [msb:lsb] = [2:0], val = 1
    }bits;
};
extern volatile union rw_npat_002Bh xdata g_rw_npat_002Bh;    // Absolute Address = 282Bh

union rw_npat_002Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist6_display_time_bits_4_3                                          : 2;        // [msb:lsb] = [4:3], val = 0
        unsigned char r_bist7_display_time                                                   : 5;        // [msb:lsb] = [4:0], val = 1
        unsigned char r_bist8_display_time_bits_0                                            : 1;        // val = 1
    }bits;
};
extern volatile union rw_npat_002Ch xdata g_rw_npat_002Ch;    // Absolute Address = 282Ch

//------------------------------------------------------------------------------------------------------------------------
union rw_npat_002Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist8_display_time_bits_4_1                                          : 4;        // [msb:lsb] = [4:1], val = 0
        unsigned char r_bist9_display_time_bits_3_0                                          : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_npat_002Dh xdata g_rw_npat_002Dh;    // Absolute Address = 282Dh

union rw_npat_002Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist9_display_time_bits_4                                            : 1;        // val = 0
        unsigned char r_bist10_display_time                                                  : 5;        // [msb:lsb] = [4:0], val = 1
        unsigned char r_bist11_display_time_bits_1_0                                         : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_npat_002Eh xdata g_rw_npat_002Eh;    // Absolute Address = 282Eh

union rw_npat_002Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist11_display_time_bits_4_2                                         : 3;        // [msb:lsb] = [4:2], val = 0
        unsigned char r_bist12_display_time                                                  : 5;        // [msb:lsb] = [4:0], val = 1
    }bits;
};
extern volatile union rw_npat_002Fh xdata g_rw_npat_002Fh;    // Absolute Address = 282Fh

union rw_npat_0030h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist13_display_time                                                  : 5;        // [msb:lsb] = [4:0], val = 1
        unsigned char r_bist14_display_time_bits_2_0                                         : 3;        // [msb:lsb] = [2:0], val = 1
    }bits;
};
extern volatile union rw_npat_0030h xdata g_rw_npat_0030h;    // Absolute Address = 2830h

union rw_npat_0031h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist14_display_time_bits_4_3                                         : 2;        // [msb:lsb] = [4:3], val = 0
        unsigned char r_bist15_display_time                                                  : 5;        // [msb:lsb] = [4:0], val = 1
        unsigned char r_bist16_display_time_bits_0                                           : 1;        // val = 1
    }bits;
};
extern volatile union rw_npat_0031h xdata g_rw_npat_0031h;    // Absolute Address = 2831h

//------------------------------------------------------------------------------------------------------------------------
union rw_npat_0032h
{
    unsigned char byte;
    struct
    {
        unsigned char r_bist16_display_time_bits_4_1                                         : 4;        // [msb:lsb] = [4:1], val = 0
        unsigned char r_gc_a_r_bits_3_0                                                      : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_npat_0032h xdata g_rw_npat_0032h;    // Absolute Address = 2832h

union rw_npat_0033h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_a_r_bits_9_4                                                      : 6;        // [msb:lsb] = [9:4], val = 48
        unsigned char r_gc_a_g_bits_1_0                                                      : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_npat_0033h xdata g_rw_npat_0033h;    // Absolute Address = 2833h

union rw_npat_0036h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_a_b_bits_9_8                                                      : 2;        // [msb:lsb] = [9:8], val = 3
        unsigned char r_gc_b_r_bits_5_0                                                      : 6;        // [msb:lsb] = [5:0], val = 24
    }bits;
};
extern volatile union rw_npat_0036h xdata g_rw_npat_0036h;    // Absolute Address = 2836h

union rw_npat_0037h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_b_r_bits_9_6                                                      : 4;        // [msb:lsb] = [9:6], val = 9
        unsigned char r_gc_b_g_bits_3_0                                                      : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_npat_0037h xdata g_rw_npat_0037h;    // Absolute Address = 2837h

union rw_npat_0038h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_b_g_bits_9_4                                                      : 6;        // [msb:lsb] = [9:4], val = 37
        unsigned char r_gc_b_b_bits_1_0                                                      : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_npat_0038h xdata g_rw_npat_0038h;    // Absolute Address = 2838h

//------------------------------------------------------------------------------------------------------------------------
union rw_npat_003Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_c_r_bits_9_8                                                      : 2;        // [msb:lsb] = [9:8], val = 2
        unsigned char r_gc_c_g_bits_5_0                                                      : 6;        // [msb:lsb] = [5:0], val = 8
    }bits;
};
extern volatile union rw_npat_003Bh xdata g_rw_npat_003Bh;    // Absolute Address = 283Bh

union rw_npat_003Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_c_g_bits_9_6                                                      : 4;        // [msb:lsb] = [9:6], val = 11
        unsigned char r_gc_c_b_bits_3_0                                                      : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_npat_003Ch xdata g_rw_npat_003Ch;    // Absolute Address = 283Ch

union rw_npat_003Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_c_b_bits_9_4                                                      : 6;        // [msb:lsb] = [9:4], val = 44
        unsigned char r_gc_d_r_bits_1_0                                                      : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_npat_003Dh xdata g_rw_npat_003Dh;    // Absolute Address = 283Dh

union rw_npat_0040h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_d_g_bits_9_8                                                      : 2;        // [msb:lsb] = [9:8], val = 2
        unsigned char r_gc_d_b_bits_5_0                                                      : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_npat_0040h xdata g_rw_npat_0040h;    // Absolute Address = 2840h

union rw_npat_0041h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_d_b_bits_9_6                                                      : 4;        // [msb:lsb] = [9:6], val = 8
        unsigned char r_gc_e_r_bits_3_0                                                      : 4;        // [msb:lsb] = [3:0], val = 12
    }bits;
};
extern volatile union rw_npat_0041h xdata g_rw_npat_0041h;    // Absolute Address = 2841h

//------------------------------------------------------------------------------------------------------------------------
union rw_npat_0042h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_e_r_bits_9_4                                                      : 6;        // [msb:lsb] = [9:4], val = 12
        unsigned char r_gc_e_g_bits_1_0                                                      : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_npat_0042h xdata g_rw_npat_0042h;    // Absolute Address = 2842h

union rw_npat_0045h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_e_b_bits_9_8                                                      : 2;        // [msb:lsb] = [9:8], val = 0
        unsigned char r_gc_f_r_bits_5_0                                                      : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_npat_0045h xdata g_rw_npat_0045h;    // Absolute Address = 2845h

union rw_npat_0046h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_f_r_bits_9_6                                                      : 4;        // [msb:lsb] = [9:6], val = 0
        unsigned char r_gc_f_g_bits_3_0                                                      : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_npat_0046h xdata g_rw_npat_0046h;    // Absolute Address = 2846h

union rw_npat_0047h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_f_g_bits_9_4                                                      : 6;        // [msb:lsb] = [9:4], val = 0
        unsigned char r_gc_f_b_bits_1_0                                                      : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_npat_0047h xdata g_rw_npat_0047h;    // Absolute Address = 2847h

union rw_npat_004Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_l_r_bits_9_8                                                      : 2;        // [msb:lsb] = [9:8], val = 0
        unsigned char r_gc_l_g_bits_5_0                                                      : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_npat_004Ah xdata g_rw_npat_004Ah;    // Absolute Address = 284Ah

//------------------------------------------------------------------------------------------------------------------------
union rw_npat_004Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_l_g_bits_9_6                                                      : 4;        // [msb:lsb] = [9:6], val = 0
        unsigned char r_gc_l_b_bits_3_0                                                      : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_npat_004Bh xdata g_rw_npat_004Bh;    // Absolute Address = 284Bh

union rw_npat_004Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_l_b_bits_9_4                                                      : 6;        // [msb:lsb] = [9:4], val = 0
        unsigned char r_gc_bg_r_bits_1_0                                                     : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_npat_004Ch xdata g_rw_npat_004Ch;    // Absolute Address = 284Ch

union rw_npat_004Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_bg_g_bits_9_8                                                     : 2;        // [msb:lsb] = [9:8], val = 3
        unsigned char r_gc_bg_b_bits_5_0                                                     : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_npat_004Fh xdata g_rw_npat_004Fh;    // Absolute Address = 284Fh

union rw_npat_0050h
{
    unsigned char byte;
    struct
    {
        unsigned char r_gc_bg_b_bits_9_6                                                     : 4;        // [msb:lsb] = [9:6], val = 12
        unsigned char r_arb_a_r_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_npat_0050h xdata g_rw_npat_0050h;    // Absolute Address = 2850h

union rw_npat_0051h
{
    unsigned char byte;
    struct
    {
        unsigned char r_arb_a_r_bits_9_4                                                     : 6;        // [msb:lsb] = [9:4], val = 7
        unsigned char r_arb_a_g_bits_1_0                                                     : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_npat_0051h xdata g_rw_npat_0051h;    // Absolute Address = 2851h

//------------------------------------------------------------------------------------------------------------------------
union rw_npat_0054h
{
    unsigned char byte;
    struct
    {
        unsigned char r_arb_a_b_bits_9_8                                                     : 2;        // [msb:lsb] = [9:8], val = 0
        unsigned char r_arb_b_r_bits_5_0                                                     : 6;        // [msb:lsb] = [5:0], val = 63
    }bits;
};
extern volatile union rw_npat_0054h xdata g_rw_npat_0054h;    // Absolute Address = 2854h

union rw_npat_0055h
{
    unsigned char byte;
    struct
    {
        unsigned char r_arb_b_r_bits_9_6                                                     : 4;        // [msb:lsb] = [9:6], val = 0
        unsigned char r_arb_b_g_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_npat_0055h xdata g_rw_npat_0055h;    // Absolute Address = 2855h

union rw_npat_0056h
{
    unsigned char byte;
    struct
    {
        unsigned char r_arb_b_g_bits_9_4                                                     : 6;        // [msb:lsb] = [9:4], val = 3
        unsigned char r_arb_b_b_bits_1_0                                                     : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_npat_0056h xdata g_rw_npat_0056h;    // Absolute Address = 2856h

union rw_npat_0059h
{
    unsigned char byte;
    struct
    {
        unsigned char r_arb_c_r_bits_9_8                                                     : 2;        // [msb:lsb] = [9:8], val = 1
        unsigned char r_arb_c_g_bits_5_0                                                     : 6;        // [msb:lsb] = [5:0], val = 63
    }bits;
};
extern volatile union rw_npat_0059h xdata g_rw_npat_0059h;    // Absolute Address = 2859h

union rw_npat_005Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_arb_c_g_bits_9_6                                                     : 4;        // [msb:lsb] = [9:6], val = 7
        unsigned char r_arb_c_b_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_npat_005Ah xdata g_rw_npat_005Ah;    // Absolute Address = 285Ah

//------------------------------------------------------------------------------------------------------------------------
union rw_npat_005Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_arb_c_b_bits_9_4                                                     : 6;        // [msb:lsb] = [9:4], val = 31
        unsigned char r_arb_d_r_bits_1_0                                                     : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_npat_005Bh xdata g_rw_npat_005Bh;    // Absolute Address = 285Bh

union rw_npat_005Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_arb_d_g_bits_9_8                                                     : 2;        // [msb:lsb] = [9:8], val = 0
        unsigned char r_arb_d_b_bits_5_0                                                     : 6;        // [msb:lsb] = [5:0], val = 63
    }bits;
};
extern volatile union rw_npat_005Eh xdata g_rw_npat_005Eh;    // Absolute Address = 285Eh

union rw_npat_005Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_arb_d_b_bits_9_6                                                     : 4;        // [msb:lsb] = [9:6], val = 3
        unsigned char r_arb_e_r_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_npat_005Fh xdata g_rw_npat_005Fh;    // Absolute Address = 285Fh

union rw_npat_0060h
{
    unsigned char byte;
    struct
    {
        unsigned char r_arb_e_r_bits_9_4                                                     : 6;        // [msb:lsb] = [9:4], val = 63
        unsigned char r_arb_e_g_bits_1_0                                                     : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_npat_0060h xdata g_rw_npat_0060h;    // Absolute Address = 2860h

union rw_npat_0063h
{
    unsigned char byte;
    struct
    {
        unsigned char r_arb_e_b_bits_9_8                                                     : 2;        // [msb:lsb] = [9:8], val = 3
        unsigned char r_arb_f_r_bits_5_0                                                     : 6;        // [msb:lsb] = [5:0], val = 31
    }bits;
};
extern volatile union rw_npat_0063h xdata g_rw_npat_0063h;    // Absolute Address = 2863h

//------------------------------------------------------------------------------------------------------------------------
union rw_npat_0064h
{
    unsigned char byte;
    struct
    {
        unsigned char r_arb_f_r_bits_9_6                                                     : 4;        // [msb:lsb] = [9:6], val = 0
        unsigned char r_arb_f_g_bits_3_0                                                     : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_npat_0064h xdata g_rw_npat_0064h;    // Absolute Address = 2864h

union rw_npat_0065h
{
    unsigned char byte;
    struct
    {
        unsigned char r_arb_f_g_bits_9_4                                                     : 6;        // [msb:lsb] = [9:4], val = 1
        unsigned char r_arb_f_b_bits_1_0                                                     : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_npat_0065h xdata g_rw_npat_0065h;    // Absolute Address = 2865h


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_npat_0004h_delta_vgray;                                               // [msb:lsb] = [7:0], val = 61, 	Absolute Address = 2804h
extern volatile uint8_t xdata g_rw_npat_0005h_delta_hgray;                                               // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 2805h
extern volatile uint8_t xdata g_rw_npat_0006h_patx_slope;                                                // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 2806h
extern volatile uint8_t xdata g_rw_npat_000Bh_bar_g;                                                     // [msb:lsb] = [8:1], val = 128, 	Absolute Address = 280Bh
extern volatile uint8_t xdata g_rw_npat_0018h_htot_rec;                                                  // [msb:lsb] = [8:1], val = 38, 	Absolute Address = 2818h

extern volatile uint8_t xdata g_rw_npat_0034h_gc_a_g;                                                    // [msb:lsb] = [9:2], val = 192, 	Absolute Address = 2834h
extern volatile uint8_t xdata g_rw_npat_0035h_gc_a_b;                                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2835h
extern volatile uint8_t xdata g_rw_npat_0039h_gc_b_b;                                                    // [msb:lsb] = [9:2], val = 150, 	Absolute Address = 2839h
extern volatile uint8_t xdata g_rw_npat_003Ah_gc_c_r;                                                    // [msb:lsb] = [7:0], val = 200, 	Absolute Address = 283Ah
extern volatile uint8_t xdata g_rw_npat_003Eh_gc_d_r;                                                    // [msb:lsb] = [9:2], val = 128, 	Absolute Address = 283Eh

extern volatile uint8_t xdata g_rw_npat_003Fh_gc_d_g;                                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 283Fh
extern volatile uint8_t xdata g_rw_npat_0043h_gc_e_g;                                                    // [msb:lsb] = [9:2], val = 51, 	Absolute Address = 2843h
extern volatile uint8_t xdata g_rw_npat_0044h_gc_e_b;                                                    // [msb:lsb] = [7:0], val = 204, 	Absolute Address = 2844h
extern volatile uint8_t xdata g_rw_npat_0048h_gc_f_b;                                                    // [msb:lsb] = [9:2], val = 0, 	Absolute Address = 2848h
extern volatile uint8_t xdata g_rw_npat_0049h_gc_l_r;                                                    // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2849h

extern volatile uint8_t xdata g_rw_npat_004Dh_gc_bg_r;                                                   // [msb:lsb] = [9:2], val = 192, 	Absolute Address = 284Dh
extern volatile uint8_t xdata g_rw_npat_004Eh_gc_bg_g;                                                   // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 284Eh
extern volatile uint8_t xdata g_rw_npat_0052h_arb_a_g;                                                   // [msb:lsb] = [9:2], val = 31, 	Absolute Address = 2852h
extern volatile uint8_t xdata g_rw_npat_0053h_arb_a_b;                                                   // [msb:lsb] = [7:0], val = 127, 	Absolute Address = 2853h
extern volatile uint8_t xdata g_rw_npat_0057h_arb_b_b;                                                   // [msb:lsb] = [9:2], val = 15, 	Absolute Address = 2857h

extern volatile uint8_t xdata g_rw_npat_0058h_arb_c_r;                                                   // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 2858h
extern volatile uint8_t xdata g_rw_npat_005Ch_arb_d_r;                                                   // [msb:lsb] = [9:2], val = 63, 	Absolute Address = 285Ch
extern volatile uint8_t xdata g_rw_npat_005Dh_arb_d_g;                                                   // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 285Dh
extern volatile uint8_t xdata g_rw_npat_0061h_arb_e_g;                                                   // [msb:lsb] = [9:2], val = 255, 	Absolute Address = 2861h
extern volatile uint8_t xdata g_rw_npat_0062h_arb_e_b;                                                   // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 2862h

extern volatile uint8_t xdata g_rw_npat_0066h_arb_f_b;                                                   // [msb:lsb] = [9:2], val = 7, 	Absolute Address = 2866h
extern volatile uint8_t xdata g_rw_npat_0067h_freerun_r0;                                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2867h
extern volatile uint8_t xdata g_rw_npat_0068h_freerun_g0;                                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2868h
extern volatile uint8_t xdata g_rw_npat_0069h_freerun_b0;                                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 2869h
extern volatile uint8_t xdata g_rw_npat_006Ah_freerun_r1;                                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 286Ah

extern volatile uint8_t xdata g_rw_npat_006Bh_freerun_g1;                                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 286Bh
extern volatile uint8_t xdata g_rw_npat_006Ch_freerun_b1;                                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 286Ch

#endif