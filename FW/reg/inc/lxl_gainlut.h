#ifndef _LXL_GAINLUT_H_
#define _LXL_GAINLUT_H_
#include "reg_include.h"


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_lxl_gainlut_0000h_LXL_LUT1_data_0;                                    // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9000h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0001h_LXL_LUT1_data_1;                                    // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9001h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0002h_LXL_LUT1_data_2;                                    // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9002h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0003h_LXL_LUT1_data_3;                                    // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9003h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0004h_LXL_LUT1_data_4;                                    // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9004h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0005h_LXL_LUT1_data_5;                                    // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9005h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0006h_LXL_LUT1_data_6;                                    // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9006h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0007h_LXL_LUT1_data_7;                                    // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9007h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0008h_LXL_LUT1_data_8;                                    // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9008h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0009h_LXL_LUT1_data_9;                                    // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9009h

extern volatile uint8_t xdata g_rw_lxl_gainlut_000Ah_LXL_LUT1_data_10;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 900Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_000Bh_LXL_LUT1_data_11;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 900Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_000Ch_LXL_LUT1_data_12;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 900Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_000Dh_LXL_LUT1_data_13;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 900Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_000Eh_LXL_LUT1_data_14;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 900Eh

extern volatile uint8_t xdata g_rw_lxl_gainlut_000Fh_LXL_LUT1_data_15;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 900Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0010h_LXL_LUT1_data_16;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9010h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0011h_LXL_LUT1_data_17;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9011h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0012h_LXL_LUT1_data_18;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9012h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0013h_LXL_LUT1_data_19;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9013h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0014h_LXL_LUT1_data_20;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9014h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0015h_LXL_LUT1_data_21;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9015h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0016h_LXL_LUT1_data_22;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9016h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0017h_LXL_LUT1_data_23;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9017h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0018h_LXL_LUT1_data_24;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9018h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0019h_LXL_LUT1_data_25;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9019h
extern volatile uint8_t xdata g_rw_lxl_gainlut_001Ah_LXL_LUT1_data_26;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 901Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_001Bh_LXL_LUT1_data_27;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 901Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_001Ch_LXL_LUT1_data_28;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 901Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_001Dh_LXL_LUT1_data_29;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 901Dh

extern volatile uint8_t xdata g_rw_lxl_gainlut_001Eh_LXL_LUT1_data_30;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 901Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_001Fh_LXL_LUT1_data_31;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 901Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0020h_LXL_LUT1_data_32;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9020h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0021h_LXL_LUT1_data_33;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9021h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0022h_LXL_LUT1_data_34;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9022h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0023h_LXL_LUT1_data_35;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9023h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0024h_LXL_LUT1_data_36;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9024h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0025h_LXL_LUT1_data_37;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9025h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0026h_LXL_LUT1_data_38;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9026h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0027h_LXL_LUT1_data_39;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9027h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0028h_LXL_LUT1_data_40;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9028h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0029h_LXL_LUT1_data_41;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9029h
extern volatile uint8_t xdata g_rw_lxl_gainlut_002Ah_LXL_LUT1_data_42;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 902Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_002Bh_LXL_LUT1_data_43;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 902Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_002Ch_LXL_LUT1_data_44;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 902Ch

extern volatile uint8_t xdata g_rw_lxl_gainlut_002Dh_LXL_LUT1_data_45;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 902Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_002Eh_LXL_LUT1_data_46;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 902Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_002Fh_LXL_LUT1_data_47;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 902Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0030h_LXL_LUT1_data_48;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9030h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0031h_LXL_LUT1_data_49;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9031h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0032h_LXL_LUT1_data_50;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9032h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0033h_LXL_LUT1_data_51;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9033h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0034h_LXL_LUT1_data_52;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9034h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0035h_LXL_LUT1_data_53;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9035h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0036h_LXL_LUT1_data_54;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9036h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0037h_LXL_LUT1_data_55;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9037h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0038h_LXL_LUT1_data_56;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9038h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0039h_LXL_LUT1_data_57;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9039h
extern volatile uint8_t xdata g_rw_lxl_gainlut_003Ah_LXL_LUT1_data_58;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 903Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_003Bh_LXL_LUT1_data_59;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 903Bh

extern volatile uint8_t xdata g_rw_lxl_gainlut_003Ch_LXL_LUT1_data_60;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 903Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_003Dh_LXL_LUT1_data_61;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 903Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_003Eh_LXL_LUT1_data_62;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 903Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_003Fh_LXL_LUT1_data_63;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 903Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0040h_LXL_LUT1_data_64;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9040h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0041h_LXL_LUT1_data_65;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9041h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0042h_LXL_LUT1_data_66;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9042h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0043h_LXL_LUT1_data_67;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9043h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0044h_LXL_LUT1_data_68;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9044h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0045h_LXL_LUT1_data_69;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9045h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0046h_LXL_LUT1_data_70;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9046h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0047h_LXL_LUT1_data_71;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9047h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0048h_LXL_LUT1_data_72;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9048h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0049h_LXL_LUT1_data_73;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9049h
extern volatile uint8_t xdata g_rw_lxl_gainlut_004Ah_LXL_LUT1_data_74;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 904Ah

extern volatile uint8_t xdata g_rw_lxl_gainlut_004Bh_LXL_LUT1_data_75;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 904Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_004Ch_LXL_LUT1_data_76;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 904Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_004Dh_LXL_LUT1_data_77;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 904Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_004Eh_LXL_LUT1_data_78;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 904Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_004Fh_LXL_LUT1_data_79;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 904Fh

extern volatile uint8_t xdata g_rw_lxl_gainlut_0050h_LXL_LUT1_data_80;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9050h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0051h_LXL_LUT1_data_81;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9051h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0052h_LXL_LUT1_data_82;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9052h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0053h_LXL_LUT1_data_83;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9053h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0054h_LXL_LUT1_data_84;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9054h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0055h_LXL_LUT1_data_85;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9055h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0056h_LXL_LUT1_data_86;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9056h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0057h_LXL_LUT1_data_87;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9057h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0058h_LXL_LUT1_data_88;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9058h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0059h_LXL_LUT1_data_89;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9059h

extern volatile uint8_t xdata g_rw_lxl_gainlut_005Ah_LXL_LUT1_data_90;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 905Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_005Bh_LXL_LUT1_data_91;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 905Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_005Ch_LXL_LUT1_data_92;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 905Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_005Dh_LXL_LUT1_data_93;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 905Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_005Eh_LXL_LUT1_data_94;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 905Eh

extern volatile uint8_t xdata g_rw_lxl_gainlut_005Fh_LXL_LUT1_data_95;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 905Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0060h_LXL_LUT1_data_96;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9060h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0061h_LXL_LUT1_data_97;                                   // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9061h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0062h_LXL_LUT1_data_98;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9062h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0063h_LXL_LUT1_data_99;                                   // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9063h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0064h_LXL_LUT1_data_100;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9064h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0065h_LXL_LUT1_data_101;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9065h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0066h_LXL_LUT1_data_102;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9066h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0067h_LXL_LUT1_data_103;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9067h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0068h_LXL_LUT1_data_104;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9068h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0069h_LXL_LUT1_data_105;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9069h
extern volatile uint8_t xdata g_rw_lxl_gainlut_006Ah_LXL_LUT1_data_106;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 906Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_006Bh_LXL_LUT1_data_107;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 906Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_006Ch_LXL_LUT1_data_108;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 906Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_006Dh_LXL_LUT1_data_109;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 906Dh

extern volatile uint8_t xdata g_rw_lxl_gainlut_006Eh_LXL_LUT1_data_110;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 906Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_006Fh_LXL_LUT1_data_111;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 906Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0070h_LXL_LUT1_data_112;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9070h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0071h_LXL_LUT1_data_113;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9071h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0072h_LXL_LUT1_data_114;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9072h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0073h_LXL_LUT1_data_115;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9073h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0074h_LXL_LUT1_data_116;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9074h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0075h_LXL_LUT1_data_117;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9075h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0076h_LXL_LUT1_data_118;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9076h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0077h_LXL_LUT1_data_119;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9077h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0078h_LXL_LUT1_data_120;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9078h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0079h_LXL_LUT1_data_121;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9079h
extern volatile uint8_t xdata g_rw_lxl_gainlut_007Ah_LXL_LUT1_data_122;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 907Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_007Bh_LXL_LUT1_data_123;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 907Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_007Ch_LXL_LUT1_data_124;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 907Ch

extern volatile uint8_t xdata g_rw_lxl_gainlut_007Dh_LXL_LUT1_data_125;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 907Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_007Eh_LXL_LUT1_data_126;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 907Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_007Fh_LXL_LUT1_data_127;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 907Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0080h_LXL_LUT1_data_128;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9080h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0081h_LXL_LUT1_data_129;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9081h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0082h_LXL_LUT1_data_130;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9082h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0083h_LXL_LUT1_data_131;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9083h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0084h_LXL_LUT1_data_132;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9084h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0085h_LXL_LUT1_data_133;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9085h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0086h_LXL_LUT1_data_134;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9086h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0087h_LXL_LUT1_data_135;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9087h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0088h_LXL_LUT1_data_136;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9088h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0089h_LXL_LUT1_data_137;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9089h
extern volatile uint8_t xdata g_rw_lxl_gainlut_008Ah_LXL_LUT1_data_138;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 908Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_008Bh_LXL_LUT1_data_139;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 908Bh

extern volatile uint8_t xdata g_rw_lxl_gainlut_008Ch_LXL_LUT1_data_140;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 908Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_008Dh_LXL_LUT1_data_141;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 908Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_008Eh_LXL_LUT1_data_142;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 908Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_008Fh_LXL_LUT1_data_143;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 908Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0090h_LXL_LUT1_data_144;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9090h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0091h_LXL_LUT1_data_145;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9091h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0092h_LXL_LUT1_data_146;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9092h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0093h_LXL_LUT1_data_147;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9093h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0094h_LXL_LUT1_data_148;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9094h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0095h_LXL_LUT1_data_149;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9095h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0096h_LXL_LUT1_data_150;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9096h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0097h_LXL_LUT1_data_151;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9097h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0098h_LXL_LUT1_data_152;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9098h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0099h_LXL_LUT1_data_153;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9099h
extern volatile uint8_t xdata g_rw_lxl_gainlut_009Ah_LXL_LUT1_data_154;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 909Ah

extern volatile uint8_t xdata g_rw_lxl_gainlut_009Bh_LXL_LUT1_data_155;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 909Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_009Ch_LXL_LUT1_data_156;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 909Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_009Dh_LXL_LUT1_data_157;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 909Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_009Eh_LXL_LUT1_data_158;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 909Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_009Fh_LXL_LUT1_data_159;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 909Fh

extern volatile uint8_t xdata g_rw_lxl_gainlut_00A0h_LXL_LUT1_data_160;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90A0h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00A1h_LXL_LUT1_data_161;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90A1h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00A2h_LXL_LUT1_data_162;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90A2h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00A3h_LXL_LUT1_data_163;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90A3h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00A4h_LXL_LUT1_data_164;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90A4h

extern volatile uint8_t xdata g_rw_lxl_gainlut_00A5h_LXL_LUT1_data_165;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90A5h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00A6h_LXL_LUT1_data_166;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90A6h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00A7h_LXL_LUT1_data_167;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90A7h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00A8h_LXL_LUT1_data_168;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90A8h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00A9h_LXL_LUT1_data_169;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90A9h

extern volatile uint8_t xdata g_rw_lxl_gainlut_00AAh_LXL_LUT1_data_170;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90AAh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00ABh_LXL_LUT1_data_171;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90ABh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00ACh_LXL_LUT1_data_172;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90ACh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00ADh_LXL_LUT1_data_173;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90ADh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00AEh_LXL_LUT1_data_174;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90AEh

extern volatile uint8_t xdata g_rw_lxl_gainlut_00AFh_LXL_LUT1_data_175;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90AFh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00B0h_LXL_LUT1_data_176;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90B0h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00B1h_LXL_LUT1_data_177;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90B1h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00B2h_LXL_LUT1_data_178;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90B2h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00B3h_LXL_LUT1_data_179;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90B3h

extern volatile uint8_t xdata g_rw_lxl_gainlut_00B4h_LXL_LUT1_data_180;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90B4h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00B5h_LXL_LUT1_data_181;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90B5h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00B6h_LXL_LUT1_data_182;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90B6h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00B7h_LXL_LUT1_data_183;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90B7h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00B8h_LXL_LUT1_data_184;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90B8h

extern volatile uint8_t xdata g_rw_lxl_gainlut_00B9h_LXL_LUT1_data_185;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90B9h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00BAh_LXL_LUT1_data_186;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90BAh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00BBh_LXL_LUT1_data_187;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90BBh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00BCh_LXL_LUT1_data_188;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90BCh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00BDh_LXL_LUT1_data_189;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90BDh

extern volatile uint8_t xdata g_rw_lxl_gainlut_00BEh_LXL_LUT1_data_190;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90BEh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00BFh_LXL_LUT1_data_191;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90BFh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00C0h_LXL_LUT1_data_192;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90C0h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00C1h_LXL_LUT1_data_193;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90C1h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00C2h_LXL_LUT1_data_194;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90C2h

extern volatile uint8_t xdata g_rw_lxl_gainlut_00C3h_LXL_LUT1_data_195;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90C3h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00C4h_LXL_LUT1_data_196;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90C4h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00C5h_LXL_LUT1_data_197;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90C5h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00C6h_LXL_LUT1_data_198;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90C6h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00C7h_LXL_LUT1_data_199;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90C7h

extern volatile uint8_t xdata g_rw_lxl_gainlut_00C8h_LXL_LUT1_data_200;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90C8h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00C9h_LXL_LUT1_data_201;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90C9h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00CAh_LXL_LUT1_data_202;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90CAh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00CBh_LXL_LUT1_data_203;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90CBh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00CCh_LXL_LUT1_data_204;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90CCh

extern volatile uint8_t xdata g_rw_lxl_gainlut_00CDh_LXL_LUT1_data_205;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90CDh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00CEh_LXL_LUT1_data_206;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90CEh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00CFh_LXL_LUT1_data_207;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90CFh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00D0h_LXL_LUT1_data_208;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90D0h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00D1h_LXL_LUT1_data_209;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90D1h

extern volatile uint8_t xdata g_rw_lxl_gainlut_00D2h_LXL_LUT1_data_210;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90D2h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00D3h_LXL_LUT1_data_211;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90D3h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00D4h_LXL_LUT1_data_212;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90D4h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00D5h_LXL_LUT1_data_213;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90D5h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00D6h_LXL_LUT1_data_214;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90D6h

extern volatile uint8_t xdata g_rw_lxl_gainlut_00D7h_LXL_LUT1_data_215;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90D7h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00D8h_LXL_LUT1_data_216;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90D8h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00D9h_LXL_LUT1_data_217;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90D9h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00DAh_LXL_LUT1_data_218;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90DAh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00DBh_LXL_LUT1_data_219;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90DBh

extern volatile uint8_t xdata g_rw_lxl_gainlut_00DCh_LXL_LUT1_data_220;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90DCh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00DDh_LXL_LUT1_data_221;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90DDh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00DEh_LXL_LUT1_data_222;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90DEh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00DFh_LXL_LUT1_data_223;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90DFh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00E0h_LXL_LUT1_data_224;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90E0h

extern volatile uint8_t xdata g_rw_lxl_gainlut_00E1h_LXL_LUT1_data_225;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90E1h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00E2h_LXL_LUT1_data_226;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90E2h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00E3h_LXL_LUT1_data_227;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90E3h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00E4h_LXL_LUT1_data_228;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90E4h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00E5h_LXL_LUT1_data_229;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90E5h

extern volatile uint8_t xdata g_rw_lxl_gainlut_00E6h_LXL_LUT1_data_230;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90E6h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00E7h_LXL_LUT1_data_231;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90E7h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00E8h_LXL_LUT1_data_232;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90E8h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00E9h_LXL_LUT1_data_233;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90E9h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00EAh_LXL_LUT1_data_234;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90EAh

extern volatile uint8_t xdata g_rw_lxl_gainlut_00EBh_LXL_LUT1_data_235;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90EBh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00ECh_LXL_LUT1_data_236;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90ECh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00EDh_LXL_LUT1_data_237;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90EDh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00EEh_LXL_LUT1_data_238;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90EEh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00EFh_LXL_LUT1_data_239;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90EFh

extern volatile uint8_t xdata g_rw_lxl_gainlut_00F0h_LXL_LUT1_data_240;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90F0h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00F1h_LXL_LUT1_data_241;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90F1h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00F2h_LXL_LUT1_data_242;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90F2h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00F3h_LXL_LUT1_data_243;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90F3h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00F4h_LXL_LUT1_data_244;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90F4h

extern volatile uint8_t xdata g_rw_lxl_gainlut_00F5h_LXL_LUT1_data_245;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90F5h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00F6h_LXL_LUT1_data_246;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90F6h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00F7h_LXL_LUT1_data_247;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90F7h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00F8h_LXL_LUT1_data_248;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90F8h
extern volatile uint8_t xdata g_rw_lxl_gainlut_00F9h_LXL_LUT1_data_249;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90F9h

extern volatile uint8_t xdata g_rw_lxl_gainlut_00FAh_LXL_LUT1_data_250;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90FAh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00FBh_LXL_LUT1_data_251;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90FBh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00FCh_LXL_LUT1_data_252;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90FCh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00FDh_LXL_LUT1_data_253;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 90FDh
extern volatile uint8_t xdata g_rw_lxl_gainlut_00FEh_LXL_LUT1_data_254;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90FEh

extern volatile uint8_t xdata g_rw_lxl_gainlut_00FFh_LXL_LUT1_data_255;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 90FFh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0100h_LXL_LUT1_data_256;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9100h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0101h_LXL_LUT1_data_257;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9101h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0102h_LXL_LUT1_data_258;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9102h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0103h_LXL_LUT1_data_259;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9103h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0104h_LXL_LUT1_data_260;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9104h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0105h_LXL_LUT1_data_261;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9105h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0106h_LXL_LUT1_data_262;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9106h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0107h_LXL_LUT1_data_263;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9107h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0108h_LXL_LUT1_data_264;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9108h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0109h_LXL_LUT1_data_265;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9109h
extern volatile uint8_t xdata g_rw_lxl_gainlut_010Ah_LXL_LUT1_data_266;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 910Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_010Bh_LXL_LUT1_data_267;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 910Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_010Ch_LXL_LUT1_data_268;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 910Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_010Dh_LXL_LUT1_data_269;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 910Dh

extern volatile uint8_t xdata g_rw_lxl_gainlut_010Eh_LXL_LUT1_data_270;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 910Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_010Fh_LXL_LUT1_data_271;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 910Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0110h_LXL_LUT1_data_272;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9110h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0111h_LXL_LUT1_data_273;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9111h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0112h_LXL_LUT1_data_274;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9112h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0113h_LXL_LUT1_data_275;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9113h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0114h_LXL_LUT1_data_276;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9114h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0115h_LXL_LUT1_data_277;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9115h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0116h_LXL_LUT1_data_278;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9116h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0117h_LXL_LUT1_data_279;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9117h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0118h_LXL_LUT1_data_280;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9118h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0119h_LXL_LUT1_data_281;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9119h
extern volatile uint8_t xdata g_rw_lxl_gainlut_011Ah_LXL_LUT1_data_282;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 911Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_011Bh_LXL_LUT1_data_283;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 911Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_011Ch_LXL_LUT1_data_284;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 911Ch

extern volatile uint8_t xdata g_rw_lxl_gainlut_011Dh_LXL_LUT1_data_285;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 911Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_011Eh_LXL_LUT1_data_286;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 911Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_011Fh_LXL_LUT1_data_287;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 911Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0120h_LXL_LUT1_data_288;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9120h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0121h_LXL_LUT1_data_289;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9121h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0122h_LXL_LUT1_data_290;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9122h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0123h_LXL_LUT1_data_291;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9123h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0124h_LXL_LUT1_data_292;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9124h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0125h_LXL_LUT1_data_293;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9125h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0126h_LXL_LUT1_data_294;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9126h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0127h_LXL_LUT1_data_295;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9127h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0128h_LXL_LUT1_data_296;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9128h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0129h_LXL_LUT1_data_297;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9129h
extern volatile uint8_t xdata g_rw_lxl_gainlut_012Ah_LXL_LUT1_data_298;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 912Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_012Bh_LXL_LUT1_data_299;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 912Bh

extern volatile uint8_t xdata g_rw_lxl_gainlut_012Ch_LXL_LUT1_data_300;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 912Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_012Dh_LXL_LUT1_data_301;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 912Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_012Eh_LXL_LUT1_data_302;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 912Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_012Fh_LXL_LUT1_data_303;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 912Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0130h_LXL_LUT1_data_304;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9130h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0131h_LXL_LUT1_data_305;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9131h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0132h_LXL_LUT1_data_306;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9132h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0133h_LXL_LUT1_data_307;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9133h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0134h_LXL_LUT1_data_308;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9134h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0135h_LXL_LUT1_data_309;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9135h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0136h_LXL_LUT1_data_310;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9136h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0137h_LXL_LUT1_data_311;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9137h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0138h_LXL_LUT1_data_312;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9138h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0139h_LXL_LUT1_data_313;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9139h
extern volatile uint8_t xdata g_rw_lxl_gainlut_013Ah_LXL_LUT1_data_314;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 913Ah

extern volatile uint8_t xdata g_rw_lxl_gainlut_013Bh_LXL_LUT1_data_315;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 913Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_013Ch_LXL_LUT1_data_316;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 913Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_013Dh_LXL_LUT1_data_317;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 913Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_013Eh_LXL_LUT1_data_318;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 913Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_013Fh_LXL_LUT1_data_319;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 913Fh

extern volatile uint8_t xdata g_rw_lxl_gainlut_0140h_LXL_LUT1_data_320;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9140h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0141h_LXL_LUT1_data_321;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9141h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0142h_LXL_LUT1_data_322;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9142h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0143h_LXL_LUT1_data_323;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9143h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0144h_LXL_LUT1_data_324;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9144h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0145h_LXL_LUT1_data_325;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9145h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0146h_LXL_LUT1_data_326;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9146h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0147h_LXL_LUT1_data_327;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9147h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0148h_LXL_LUT1_data_328;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9148h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0149h_LXL_LUT1_data_329;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9149h

extern volatile uint8_t xdata g_rw_lxl_gainlut_014Ah_LXL_LUT1_data_330;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 914Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_014Bh_LXL_LUT1_data_331;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 914Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_014Ch_LXL_LUT1_data_332;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 914Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_014Dh_LXL_LUT1_data_333;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 914Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_014Eh_LXL_LUT1_data_334;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 914Eh

extern volatile uint8_t xdata g_rw_lxl_gainlut_014Fh_LXL_LUT1_data_335;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 914Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0150h_LXL_LUT1_data_336;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9150h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0151h_LXL_LUT1_data_337;                                  // [msb:lsb] = [7:0], val = 63, 	Absolute Address = 9151h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0152h_LXL_LUT1_data_338;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9152h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0153h_LXL_LUT1_data_339;                                  // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9153h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0154h_LXL_LUT1_data_340;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9154h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0155h_LXL_LUT1_data_341;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9155h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0156h_LXL_LUT1_data_342;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9156h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0157h_LXL_LUT1_data_343;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9157h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0158h_LXL_LUT1_data_344;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9158h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0159h_LXL_LUT1_data_345;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9159h
extern volatile uint8_t xdata g_rw_lxl_gainlut_015Ah_LXL_LUT1_data_346;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 915Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_015Bh_LXL_LUT1_data_347;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 915Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_015Ch_LXL_LUT1_data_348;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 915Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_015Dh_LXL_LUT1_data_349;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 915Dh

extern volatile uint8_t xdata g_rw_lxl_gainlut_015Eh_LXL_LUT1_data_350;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 915Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_015Fh_LXL_LUT1_data_351;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 915Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0160h_LXL_LUT1_data_352;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9160h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0161h_LXL_LUT1_data_353;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9161h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0162h_LXL_LUT1_data_354;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9162h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0163h_LXL_LUT1_data_355;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9163h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0164h_LXL_LUT1_data_356;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9164h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0165h_LXL_LUT1_data_357;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9165h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0166h_LXL_LUT1_data_358;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9166h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0167h_LXL_LUT1_data_359;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9167h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0168h_LXL_LUT1_data_360;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9168h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0169h_LXL_LUT1_data_361;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9169h
extern volatile uint8_t xdata g_rw_lxl_gainlut_016Ah_LXL_LUT1_data_362;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 916Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_016Bh_LXL_LUT1_data_363;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 916Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_016Ch_LXL_LUT1_data_364;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 916Ch

extern volatile uint8_t xdata g_rw_lxl_gainlut_016Dh_LXL_LUT1_data_365;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 916Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_016Eh_LXL_LUT1_data_366;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 916Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_016Fh_LXL_LUT1_data_367;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 916Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0170h_LXL_LUT1_data_368;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9170h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0171h_LXL_LUT1_data_369;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9171h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0172h_LXL_LUT1_data_370;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9172h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0173h_LXL_LUT1_data_371;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9173h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0174h_LXL_LUT1_data_372;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9174h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0175h_LXL_LUT1_data_373;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9175h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0176h_LXL_LUT1_data_374;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9176h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0177h_LXL_LUT1_data_375;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9177h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0178h_LXL_LUT1_data_376;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9178h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0179h_LXL_LUT1_data_377;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9179h
extern volatile uint8_t xdata g_rw_lxl_gainlut_017Ah_LXL_LUT1_data_378;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 917Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_017Bh_LXL_LUT1_data_379;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 917Bh

extern volatile uint8_t xdata g_rw_lxl_gainlut_017Ch_LXL_LUT1_data_380;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 917Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_017Dh_LXL_LUT1_data_381;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 917Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_017Eh_LXL_LUT1_data_382;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 917Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_017Fh_LXL_LUT1_data_383;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 917Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0180h_LXL_LUT1_data_384;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9180h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0181h_LXL_LUT1_data_385;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9181h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0182h_LXL_LUT1_data_386;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9182h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0183h_LXL_LUT1_data_387;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9183h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0184h_LXL_LUT1_data_388;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9184h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0185h_LXL_LUT1_data_389;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9185h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0186h_LXL_LUT1_data_390;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9186h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0187h_LXL_LUT1_data_391;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9187h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0188h_LXL_LUT1_data_392;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9188h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0189h_LXL_LUT1_data_393;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9189h
extern volatile uint8_t xdata g_rw_lxl_gainlut_018Ah_LXL_LUT1_data_394;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 918Ah

extern volatile uint8_t xdata g_rw_lxl_gainlut_018Bh_LXL_LUT1_data_395;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 918Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_018Ch_LXL_LUT1_data_396;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 918Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_018Dh_LXL_LUT1_data_397;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 918Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_018Eh_LXL_LUT1_data_398;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 918Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_018Fh_LXL_LUT1_data_399;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 918Fh

extern volatile uint8_t xdata g_rw_lxl_gainlut_0190h_LXL_LUT1_data_400;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9190h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0191h_LXL_LUT1_data_401;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9191h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0192h_LXL_LUT1_data_402;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9192h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0193h_LXL_LUT1_data_403;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9193h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0194h_LXL_LUT1_data_404;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9194h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0195h_LXL_LUT1_data_405;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9195h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0196h_LXL_LUT1_data_406;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9196h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0197h_LXL_LUT1_data_407;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9197h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0198h_LXL_LUT1_data_408;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9198h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0199h_LXL_LUT1_data_409;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9199h

extern volatile uint8_t xdata g_rw_lxl_gainlut_019Ah_LXL_LUT1_data_410;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 919Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_019Bh_LXL_LUT1_data_411;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 919Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_019Ch_LXL_LUT1_data_412;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 919Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_019Dh_LXL_LUT1_data_413;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 919Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_019Eh_LXL_LUT1_data_414;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 919Eh

extern volatile uint8_t xdata g_rw_lxl_gainlut_019Fh_LXL_LUT1_data_415;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 919Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01A0h_LXL_LUT1_data_416;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91A0h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01A1h_LXL_LUT1_data_417;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91A1h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01A2h_LXL_LUT1_data_418;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91A2h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01A3h_LXL_LUT1_data_419;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91A3h

extern volatile uint8_t xdata g_rw_lxl_gainlut_01A4h_LXL_LUT1_data_420;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91A4h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01A5h_LXL_LUT1_data_421;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91A5h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01A6h_LXL_LUT1_data_422;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91A6h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01A7h_LXL_LUT1_data_423;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91A7h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01A8h_LXL_LUT1_data_424;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91A8h

extern volatile uint8_t xdata g_rw_lxl_gainlut_01A9h_LXL_LUT1_data_425;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91A9h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01AAh_LXL_LUT1_data_426;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91AAh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01ABh_LXL_LUT1_data_427;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91ABh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01ACh_LXL_LUT1_data_428;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91ACh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01ADh_LXL_LUT1_data_429;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91ADh

extern volatile uint8_t xdata g_rw_lxl_gainlut_01AEh_LXL_LUT1_data_430;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91AEh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01AFh_LXL_LUT1_data_431;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91AFh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01B0h_LXL_LUT1_data_432;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91B0h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01B1h_LXL_LUT1_data_433;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91B1h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01B2h_LXL_LUT1_data_434;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91B2h

extern volatile uint8_t xdata g_rw_lxl_gainlut_01B3h_LXL_LUT1_data_435;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91B3h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01B4h_LXL_LUT1_data_436;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91B4h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01B5h_LXL_LUT1_data_437;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91B5h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01B6h_LXL_LUT1_data_438;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91B6h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01B7h_LXL_LUT1_data_439;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91B7h

extern volatile uint8_t xdata g_rw_lxl_gainlut_01B8h_LXL_LUT1_data_440;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91B8h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01B9h_LXL_LUT1_data_441;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91B9h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01BAh_LXL_LUT1_data_442;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91BAh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01BBh_LXL_LUT1_data_443;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91BBh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01BCh_LXL_LUT1_data_444;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91BCh

extern volatile uint8_t xdata g_rw_lxl_gainlut_01BDh_LXL_LUT1_data_445;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91BDh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01BEh_LXL_LUT1_data_446;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91BEh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01BFh_LXL_LUT1_data_447;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91BFh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01C0h_LXL_LUT1_data_448;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91C0h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01C1h_LXL_LUT1_data_449;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91C1h

extern volatile uint8_t xdata g_rw_lxl_gainlut_01C2h_LXL_LUT1_data_450;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91C2h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01C3h_LXL_LUT1_data_451;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91C3h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01C4h_LXL_LUT1_data_452;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91C4h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01C5h_LXL_LUT1_data_453;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91C5h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01C6h_LXL_LUT1_data_454;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91C6h

extern volatile uint8_t xdata g_rw_lxl_gainlut_01C7h_LXL_LUT1_data_455;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91C7h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01C8h_LXL_LUT1_data_456;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91C8h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01C9h_LXL_LUT1_data_457;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91C9h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01CAh_LXL_LUT1_data_458;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91CAh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01CBh_LXL_LUT1_data_459;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91CBh

extern volatile uint8_t xdata g_rw_lxl_gainlut_01CCh_LXL_LUT1_data_460;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91CCh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01CDh_LXL_LUT1_data_461;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91CDh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01CEh_LXL_LUT1_data_462;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91CEh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01CFh_LXL_LUT1_data_463;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91CFh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01D0h_LXL_LUT1_data_464;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91D0h

extern volatile uint8_t xdata g_rw_lxl_gainlut_01D1h_LXL_LUT1_data_465;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91D1h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01D2h_LXL_LUT1_data_466;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91D2h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01D3h_LXL_LUT1_data_467;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91D3h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01D4h_LXL_LUT1_data_468;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91D4h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01D5h_LXL_LUT1_data_469;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91D5h

extern volatile uint8_t xdata g_rw_lxl_gainlut_01D6h_LXL_LUT1_data_470;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91D6h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01D7h_LXL_LUT1_data_471;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91D7h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01D8h_LXL_LUT1_data_472;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91D8h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01D9h_LXL_LUT1_data_473;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91D9h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01DAh_LXL_LUT1_data_474;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91DAh

extern volatile uint8_t xdata g_rw_lxl_gainlut_01DBh_LXL_LUT1_data_475;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91DBh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01DCh_LXL_LUT1_data_476;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91DCh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01DDh_LXL_LUT1_data_477;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91DDh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01DEh_LXL_LUT1_data_478;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91DEh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01DFh_LXL_LUT1_data_479;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91DFh

extern volatile uint8_t xdata g_rw_lxl_gainlut_01E0h_LXL_LUT1_data_480;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91E0h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01E1h_LXL_LUT1_data_481;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91E1h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01E2h_LXL_LUT1_data_482;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91E2h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01E3h_LXL_LUT1_data_483;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91E3h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01E4h_LXL_LUT1_data_484;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91E4h

extern volatile uint8_t xdata g_rw_lxl_gainlut_01E5h_LXL_LUT1_data_485;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91E5h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01E6h_LXL_LUT1_data_486;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91E6h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01E7h_LXL_LUT1_data_487;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91E7h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01E8h_LXL_LUT1_data_488;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91E8h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01E9h_LXL_LUT1_data_489;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91E9h

extern volatile uint8_t xdata g_rw_lxl_gainlut_01EAh_LXL_LUT1_data_490;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91EAh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01EBh_LXL_LUT1_data_491;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91EBh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01ECh_LXL_LUT1_data_492;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91ECh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01EDh_LXL_LUT1_data_493;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91EDh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01EEh_LXL_LUT1_data_494;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91EEh

extern volatile uint8_t xdata g_rw_lxl_gainlut_01EFh_LXL_LUT1_data_495;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91EFh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01F0h_LXL_LUT1_data_496;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91F0h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01F1h_LXL_LUT1_data_497;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91F1h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01F2h_LXL_LUT1_data_498;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91F2h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01F3h_LXL_LUT1_data_499;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91F3h

extern volatile uint8_t xdata g_rw_lxl_gainlut_01F4h_LXL_LUT1_data_500;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91F4h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01F5h_LXL_LUT1_data_501;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91F5h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01F6h_LXL_LUT1_data_502;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91F6h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01F7h_LXL_LUT1_data_503;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91F7h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01F8h_LXL_LUT1_data_504;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91F8h

extern volatile uint8_t xdata g_rw_lxl_gainlut_01F9h_LXL_LUT1_data_505;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91F9h
extern volatile uint8_t xdata g_rw_lxl_gainlut_01FAh_LXL_LUT1_data_506;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91FAh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01FBh_LXL_LUT1_data_507;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91FBh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01FCh_LXL_LUT1_data_508;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91FCh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01FDh_LXL_LUT1_data_509;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91FDh

extern volatile uint8_t xdata g_rw_lxl_gainlut_01FEh_LXL_LUT1_data_510;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91FEh
extern volatile uint8_t xdata g_rw_lxl_gainlut_01FFh_LXL_LUT1_data_511;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 91FFh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0200h_LXL_LUT1_data_512;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9200h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0201h_LXL_LUT1_data_513;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9201h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0202h_LXL_LUT1_data_514;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9202h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0203h_LXL_LUT1_data_515;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9203h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0204h_LXL_LUT1_data_516;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9204h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0205h_LXL_LUT1_data_517;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9205h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0206h_LXL_LUT1_data_518;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9206h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0207h_LXL_LUT1_data_519;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9207h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0208h_LXL_LUT1_data_520;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9208h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0209h_LXL_LUT1_data_521;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9209h
extern volatile uint8_t xdata g_rw_lxl_gainlut_020Ah_LXL_LUT1_data_522;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 920Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_020Bh_LXL_LUT1_data_523;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 920Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_020Ch_LXL_LUT1_data_524;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 920Ch

extern volatile uint8_t xdata g_rw_lxl_gainlut_020Dh_LXL_LUT1_data_525;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 920Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_020Eh_LXL_LUT1_data_526;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 920Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_020Fh_LXL_LUT1_data_527;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 920Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0210h_LXL_LUT1_data_528;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9210h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0211h_LXL_LUT1_data_529;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9211h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0212h_LXL_LUT1_data_530;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9212h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0213h_LXL_LUT1_data_531;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9213h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0214h_LXL_LUT1_data_532;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9214h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0215h_LXL_LUT1_data_533;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9215h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0216h_LXL_LUT1_data_534;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9216h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0217h_LXL_LUT1_data_535;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9217h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0218h_LXL_LUT1_data_536;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9218h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0219h_LXL_LUT1_data_537;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9219h
extern volatile uint8_t xdata g_rw_lxl_gainlut_021Ah_LXL_LUT1_data_538;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 921Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_021Bh_LXL_LUT1_data_539;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 921Bh

extern volatile uint8_t xdata g_rw_lxl_gainlut_021Ch_LXL_LUT1_data_540;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 921Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_021Dh_LXL_LUT1_data_541;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 921Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_021Eh_LXL_LUT1_data_542;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 921Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_021Fh_LXL_LUT1_data_543;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 921Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0220h_LXL_LUT1_data_544;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9220h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0221h_LXL_LUT1_data_545;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9221h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0222h_LXL_LUT1_data_546;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9222h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0223h_LXL_LUT1_data_547;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9223h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0224h_LXL_LUT1_data_548;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9224h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0225h_LXL_LUT1_data_549;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9225h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0226h_LXL_LUT1_data_550;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9226h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0227h_LXL_LUT1_data_551;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9227h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0228h_LXL_LUT1_data_552;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9228h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0229h_LXL_LUT1_data_553;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9229h
extern volatile uint8_t xdata g_rw_lxl_gainlut_022Ah_LXL_LUT1_data_554;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 922Ah

extern volatile uint8_t xdata g_rw_lxl_gainlut_022Bh_LXL_LUT1_data_555;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 922Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_022Ch_LXL_LUT1_data_556;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 922Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_022Dh_LXL_LUT1_data_557;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 922Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_022Eh_LXL_LUT1_data_558;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 922Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_022Fh_LXL_LUT1_data_559;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 922Fh

extern volatile uint8_t xdata g_rw_lxl_gainlut_0230h_LXL_LUT1_data_560;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9230h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0231h_LXL_LUT1_data_561;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9231h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0232h_LXL_LUT1_data_562;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9232h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0233h_LXL_LUT1_data_563;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9233h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0234h_LXL_LUT1_data_564;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9234h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0235h_LXL_LUT1_data_565;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9235h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0236h_LXL_LUT1_data_566;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9236h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0237h_LXL_LUT1_data_567;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9237h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0238h_LXL_LUT1_data_568;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9238h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0239h_LXL_LUT1_data_569;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9239h

extern volatile uint8_t xdata g_rw_lxl_gainlut_023Ah_LXL_LUT1_data_570;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 923Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_023Bh_LXL_LUT1_data_571;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 923Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_023Ch_LXL_LUT1_data_572;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 923Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_023Dh_LXL_LUT1_data_573;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 923Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_023Eh_LXL_LUT1_data_574;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 923Eh

extern volatile uint8_t xdata g_rw_lxl_gainlut_023Fh_LXL_LUT1_data_575;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 923Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0240h_LXL_LUT1_data_576;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9240h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0241h_LXL_LUT1_data_577;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9241h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0242h_LXL_LUT1_data_578;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9242h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0243h_LXL_LUT1_data_579;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9243h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0244h_LXL_LUT1_data_580;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9244h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0245h_LXL_LUT1_data_581;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9245h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0246h_LXL_LUT1_data_582;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9246h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0247h_LXL_LUT1_data_583;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9247h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0248h_LXL_LUT1_data_584;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9248h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0249h_LXL_LUT1_data_585;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9249h
extern volatile uint8_t xdata g_rw_lxl_gainlut_024Ah_LXL_LUT1_data_586;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 924Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_024Bh_LXL_LUT1_data_587;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 924Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_024Ch_LXL_LUT1_data_588;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 924Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_024Dh_LXL_LUT1_data_589;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 924Dh

extern volatile uint8_t xdata g_rw_lxl_gainlut_024Eh_LXL_LUT1_data_590;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 924Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_024Fh_LXL_LUT1_data_591;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 924Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0250h_LXL_LUT1_data_592;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9250h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0251h_LXL_LUT1_data_593;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9251h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0252h_LXL_LUT1_data_594;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9252h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0253h_LXL_LUT1_data_595;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9253h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0254h_LXL_LUT1_data_596;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9254h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0255h_LXL_LUT1_data_597;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9255h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0256h_LXL_LUT1_data_598;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9256h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0257h_LXL_LUT1_data_599;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9257h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0258h_LXL_LUT1_data_600;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9258h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0259h_LXL_LUT1_data_601;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9259h
extern volatile uint8_t xdata g_rw_lxl_gainlut_025Ah_LXL_LUT1_data_602;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 925Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_025Bh_LXL_LUT1_data_603;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 925Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_025Ch_LXL_LUT1_data_604;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 925Ch

extern volatile uint8_t xdata g_rw_lxl_gainlut_025Dh_LXL_LUT1_data_605;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 925Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_025Eh_LXL_LUT1_data_606;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 925Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_025Fh_LXL_LUT1_data_607;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 925Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0260h_LXL_LUT1_data_608;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9260h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0261h_LXL_LUT1_data_609;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9261h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0262h_LXL_LUT1_data_610;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9262h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0263h_LXL_LUT1_data_611;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9263h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0264h_LXL_LUT1_data_612;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9264h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0265h_LXL_LUT1_data_613;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9265h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0266h_LXL_LUT1_data_614;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9266h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0267h_LXL_LUT1_data_615;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9267h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0268h_LXL_LUT1_data_616;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9268h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0269h_LXL_LUT1_data_617;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9269h
extern volatile uint8_t xdata g_rw_lxl_gainlut_026Ah_LXL_LUT1_data_618;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 926Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_026Bh_LXL_LUT1_data_619;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 926Bh

extern volatile uint8_t xdata g_rw_lxl_gainlut_026Ch_LXL_LUT1_data_620;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 926Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_026Dh_LXL_LUT1_data_621;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 926Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_026Eh_LXL_LUT1_data_622;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 926Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_026Fh_LXL_LUT1_data_623;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 926Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0270h_LXL_LUT1_data_624;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9270h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0271h_LXL_LUT1_data_625;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9271h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0272h_LXL_LUT1_data_626;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9272h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0273h_LXL_LUT1_data_627;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9273h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0274h_LXL_LUT1_data_628;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9274h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0275h_LXL_LUT1_data_629;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9275h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0276h_LXL_LUT1_data_630;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9276h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0277h_LXL_LUT1_data_631;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9277h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0278h_LXL_LUT1_data_632;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9278h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0279h_LXL_LUT1_data_633;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9279h
extern volatile uint8_t xdata g_rw_lxl_gainlut_027Ah_LXL_LUT1_data_634;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 927Ah

extern volatile uint8_t xdata g_rw_lxl_gainlut_027Bh_LXL_LUT1_data_635;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 927Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_027Ch_LXL_LUT1_data_636;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 927Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_027Dh_LXL_LUT1_data_637;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 927Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_027Eh_LXL_LUT1_data_638;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 927Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_027Fh_LXL_LUT1_data_639;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 927Fh

extern volatile uint8_t xdata g_rw_lxl_gainlut_0280h_LXL_LUT1_data_640;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9280h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0281h_LXL_LUT1_data_641;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9281h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0282h_LXL_LUT1_data_642;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9282h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0283h_LXL_LUT1_data_643;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9283h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0284h_LXL_LUT1_data_644;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9284h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0285h_LXL_LUT1_data_645;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9285h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0286h_LXL_LUT1_data_646;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9286h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0287h_LXL_LUT1_data_647;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9287h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0288h_LXL_LUT1_data_648;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9288h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0289h_LXL_LUT1_data_649;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9289h

extern volatile uint8_t xdata g_rw_lxl_gainlut_028Ah_LXL_LUT1_data_650;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 928Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_028Bh_LXL_LUT1_data_651;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 928Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_028Ch_LXL_LUT1_data_652;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 928Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_028Dh_LXL_LUT1_data_653;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 928Dh
extern volatile uint8_t xdata g_rw_lxl_gainlut_028Eh_LXL_LUT1_data_654;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 928Eh

extern volatile uint8_t xdata g_rw_lxl_gainlut_028Fh_LXL_LUT1_data_655;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 928Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_0290h_LXL_LUT1_data_656;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9290h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0291h_LXL_LUT1_data_657;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9291h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0292h_LXL_LUT1_data_658;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9292h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0293h_LXL_LUT1_data_659;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9293h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0294h_LXL_LUT1_data_660;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9294h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0295h_LXL_LUT1_data_661;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9295h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0296h_LXL_LUT1_data_662;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9296h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0297h_LXL_LUT1_data_663;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9297h
extern volatile uint8_t xdata g_rw_lxl_gainlut_0298h_LXL_LUT1_data_664;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9298h

extern volatile uint8_t xdata g_rw_lxl_gainlut_0299h_LXL_LUT1_data_665;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9299h
extern volatile uint8_t xdata g_rw_lxl_gainlut_029Ah_LXL_LUT1_data_666;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 929Ah
extern volatile uint8_t xdata g_rw_lxl_gainlut_029Bh_LXL_LUT1_data_667;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 929Bh
extern volatile uint8_t xdata g_rw_lxl_gainlut_029Ch_LXL_LUT1_data_668;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 929Ch
extern volatile uint8_t xdata g_rw_lxl_gainlut_029Dh_LXL_LUT1_data_669;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 929Dh

extern volatile uint8_t xdata g_rw_lxl_gainlut_029Eh_LXL_LUT1_data_670;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 929Eh
extern volatile uint8_t xdata g_rw_lxl_gainlut_029Fh_LXL_LUT1_data_671;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 929Fh
extern volatile uint8_t xdata g_rw_lxl_gainlut_02A0h_LXL_LUT1_data_672;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 92A0h
extern volatile uint8_t xdata g_rw_lxl_gainlut_02A1h_LXL_LUT1_data_673;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 92A1h
extern volatile uint8_t xdata g_rw_lxl_gainlut_02A2h_LXL_LUT1_data_674;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 92A2h

extern volatile uint8_t xdata g_rw_lxl_gainlut_02A3h_LXL_LUT1_data_675;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 92A3h
extern volatile uint8_t xdata g_rw_lxl_gainlut_02A4h_LXL_LUT1_data_676;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 92A4h
extern volatile uint8_t xdata g_rw_lxl_gainlut_02A5h_LXL_LUT1_data_677;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 92A5h
extern volatile uint8_t xdata g_rw_lxl_gainlut_02A6h_LXL_LUT1_data_678;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 92A6h
extern volatile uint8_t xdata g_rw_lxl_gainlut_02A7h_LXL_LUT1_data_679;                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 92A7h


#endif