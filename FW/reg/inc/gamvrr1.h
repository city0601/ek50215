#ifndef _GAMVRR1_H_
#define _GAMVRR1_H_
#include "reg_include.h"


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_gamvrr1_0000h_GAM_VRR_LUT_OFST12_1_new_data_0;                        // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8000h
extern volatile uint8_t xdata g_rw_gamvrr1_0001h_GAM_VRR_LUT_OFST12_1_new_data_1;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8001h
extern volatile uint8_t xdata g_rw_gamvrr1_0002h_GAM_VRR_LUT_OFST12_1_new_data_2;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8002h
extern volatile uint8_t xdata g_rw_gamvrr1_0003h_GAM_VRR_LUT_OFST12_1_new_data_3;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8003h
extern volatile uint8_t xdata g_rw_gamvrr1_0004h_GAM_VRR_LUT_OFST12_1_new_data_4;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8004h

extern volatile uint8_t xdata g_rw_gamvrr1_0005h_GAM_VRR_LUT_OFST12_1_new_data_5;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8005h
extern volatile uint8_t xdata g_rw_gamvrr1_0006h_GAM_VRR_LUT_OFST12_1_new_data_6;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8006h
extern volatile uint8_t xdata g_rw_gamvrr1_0007h_GAM_VRR_LUT_OFST12_1_new_data_7;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8007h
extern volatile uint8_t xdata g_rw_gamvrr1_0008h_GAM_VRR_LUT_OFST12_1_new_data_8;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8008h
extern volatile uint8_t xdata g_rw_gamvrr1_0009h_GAM_VRR_LUT_OFST12_1_new_data_9;                        // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8009h

extern volatile uint8_t xdata g_rw_gamvrr1_000Ah_GAM_VRR_LUT_OFST12_1_new_data_10;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 800Ah
extern volatile uint8_t xdata g_rw_gamvrr1_000Bh_GAM_VRR_LUT_OFST12_1_new_data_11;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 800Bh
extern volatile uint8_t xdata g_rw_gamvrr1_000Ch_GAM_VRR_LUT_OFST12_1_new_data_12;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 800Ch
extern volatile uint8_t xdata g_rw_gamvrr1_000Dh_GAM_VRR_LUT_OFST12_1_new_data_13;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 800Dh
extern volatile uint8_t xdata g_rw_gamvrr1_000Eh_GAM_VRR_LUT_OFST12_1_new_data_14;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 800Eh

extern volatile uint8_t xdata g_rw_gamvrr1_000Fh_GAM_VRR_LUT_OFST12_1_new_data_15;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 800Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0010h_GAM_VRR_LUT_OFST12_1_new_data_16;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8010h
extern volatile uint8_t xdata g_rw_gamvrr1_0011h_GAM_VRR_LUT_OFST12_1_new_data_17;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8011h
extern volatile uint8_t xdata g_rw_gamvrr1_0012h_GAM_VRR_LUT_OFST12_1_new_data_18;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8012h
extern volatile uint8_t xdata g_rw_gamvrr1_0013h_GAM_VRR_LUT_OFST12_1_new_data_19;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8013h

extern volatile uint8_t xdata g_rw_gamvrr1_0014h_GAM_VRR_LUT_OFST12_1_new_data_20;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8014h
extern volatile uint8_t xdata g_rw_gamvrr1_0015h_GAM_VRR_LUT_OFST12_1_new_data_21;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8015h
extern volatile uint8_t xdata g_rw_gamvrr1_0016h_GAM_VRR_LUT_OFST12_1_new_data_22;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8016h
extern volatile uint8_t xdata g_rw_gamvrr1_0017h_GAM_VRR_LUT_OFST12_1_new_data_23;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8017h
extern volatile uint8_t xdata g_rw_gamvrr1_0018h_GAM_VRR_LUT_OFST12_1_new_data_24;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8018h

extern volatile uint8_t xdata g_rw_gamvrr1_0019h_GAM_VRR_LUT_OFST12_1_new_data_25;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8019h
extern volatile uint8_t xdata g_rw_gamvrr1_001Ah_GAM_VRR_LUT_OFST12_1_new_data_26;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 801Ah
extern volatile uint8_t xdata g_rw_gamvrr1_001Bh_GAM_VRR_LUT_OFST12_1_new_data_27;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 801Bh
extern volatile uint8_t xdata g_rw_gamvrr1_001Ch_GAM_VRR_LUT_OFST12_1_new_data_28;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 801Ch
extern volatile uint8_t xdata g_rw_gamvrr1_001Dh_GAM_VRR_LUT_OFST12_1_new_data_29;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 801Dh

extern volatile uint8_t xdata g_rw_gamvrr1_001Eh_GAM_VRR_LUT_OFST12_1_new_data_30;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 801Eh
extern volatile uint8_t xdata g_rw_gamvrr1_001Fh_GAM_VRR_LUT_OFST12_1_new_data_31;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 801Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0020h_GAM_VRR_LUT_OFST12_1_new_data_32;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8020h
extern volatile uint8_t xdata g_rw_gamvrr1_0021h_GAM_VRR_LUT_OFST12_1_new_data_33;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8021h
extern volatile uint8_t xdata g_rw_gamvrr1_0022h_GAM_VRR_LUT_OFST12_1_new_data_34;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8022h

extern volatile uint8_t xdata g_rw_gamvrr1_0023h_GAM_VRR_LUT_OFST12_1_new_data_35;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8023h
extern volatile uint8_t xdata g_rw_gamvrr1_0024h_GAM_VRR_LUT_OFST12_1_new_data_36;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8024h
extern volatile uint8_t xdata g_rw_gamvrr1_0025h_GAM_VRR_LUT_OFST12_1_new_data_37;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8025h
extern volatile uint8_t xdata g_rw_gamvrr1_0026h_GAM_VRR_LUT_OFST12_1_new_data_38;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8026h
extern volatile uint8_t xdata g_rw_gamvrr1_0027h_GAM_VRR_LUT_OFST12_1_new_data_39;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8027h

extern volatile uint8_t xdata g_rw_gamvrr1_0028h_GAM_VRR_LUT_OFST12_1_new_data_40;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8028h
extern volatile uint8_t xdata g_rw_gamvrr1_0029h_GAM_VRR_LUT_OFST12_1_new_data_41;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8029h
extern volatile uint8_t xdata g_rw_gamvrr1_002Ah_GAM_VRR_LUT_OFST12_1_new_data_42;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 802Ah
extern volatile uint8_t xdata g_rw_gamvrr1_002Bh_GAM_VRR_LUT_OFST12_1_new_data_43;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 802Bh
extern volatile uint8_t xdata g_rw_gamvrr1_002Ch_GAM_VRR_LUT_OFST12_1_new_data_44;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 802Ch

extern volatile uint8_t xdata g_rw_gamvrr1_002Dh_GAM_VRR_LUT_OFST12_1_new_data_45;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 802Dh
extern volatile uint8_t xdata g_rw_gamvrr1_002Eh_GAM_VRR_LUT_OFST12_1_new_data_46;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 802Eh
extern volatile uint8_t xdata g_rw_gamvrr1_002Fh_GAM_VRR_LUT_OFST12_1_new_data_47;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 802Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0030h_GAM_VRR_LUT_OFST12_1_new_data_48;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8030h
extern volatile uint8_t xdata g_rw_gamvrr1_0031h_GAM_VRR_LUT_OFST12_1_new_data_49;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8031h

extern volatile uint8_t xdata g_rw_gamvrr1_0032h_GAM_VRR_LUT_OFST12_1_new_data_50;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8032h
extern volatile uint8_t xdata g_rw_gamvrr1_0033h_GAM_VRR_LUT_OFST12_1_new_data_51;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8033h
extern volatile uint8_t xdata g_rw_gamvrr1_0034h_GAM_VRR_LUT_OFST12_1_new_data_52;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8034h
extern volatile uint8_t xdata g_rw_gamvrr1_0035h_GAM_VRR_LUT_OFST12_1_new_data_53;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8035h
extern volatile uint8_t xdata g_rw_gamvrr1_0036h_GAM_VRR_LUT_OFST12_1_new_data_54;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8036h

extern volatile uint8_t xdata g_rw_gamvrr1_0037h_GAM_VRR_LUT_OFST12_1_new_data_55;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8037h
extern volatile uint8_t xdata g_rw_gamvrr1_0038h_GAM_VRR_LUT_OFST12_1_new_data_56;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8038h
extern volatile uint8_t xdata g_rw_gamvrr1_0039h_GAM_VRR_LUT_OFST12_1_new_data_57;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8039h
extern volatile uint8_t xdata g_rw_gamvrr1_003Ah_GAM_VRR_LUT_OFST12_1_new_data_58;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 803Ah
extern volatile uint8_t xdata g_rw_gamvrr1_003Bh_GAM_VRR_LUT_OFST12_1_new_data_59;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 803Bh

extern volatile uint8_t xdata g_rw_gamvrr1_003Ch_GAM_VRR_LUT_OFST12_1_new_data_60;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 803Ch
extern volatile uint8_t xdata g_rw_gamvrr1_003Dh_GAM_VRR_LUT_OFST12_1_new_data_61;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 803Dh
extern volatile uint8_t xdata g_rw_gamvrr1_003Eh_GAM_VRR_LUT_OFST12_1_new_data_62;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 803Eh
extern volatile uint8_t xdata g_rw_gamvrr1_003Fh_GAM_VRR_LUT_OFST12_1_new_data_63;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 803Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0040h_GAM_VRR_LUT_OFST12_1_new_data_64;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8040h

extern volatile uint8_t xdata g_rw_gamvrr1_0041h_GAM_VRR_LUT_OFST12_1_new_data_65;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8041h
extern volatile uint8_t xdata g_rw_gamvrr1_0042h_GAM_VRR_LUT_OFST12_1_new_data_66;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8042h
extern volatile uint8_t xdata g_rw_gamvrr1_0043h_GAM_VRR_LUT_OFST12_1_new_data_67;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8043h
extern volatile uint8_t xdata g_rw_gamvrr1_0044h_GAM_VRR_LUT_OFST12_1_new_data_68;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8044h
extern volatile uint8_t xdata g_rw_gamvrr1_0045h_GAM_VRR_LUT_OFST12_1_new_data_69;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8045h

extern volatile uint8_t xdata g_rw_gamvrr1_0046h_GAM_VRR_LUT_OFST12_1_new_data_70;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8046h
extern volatile uint8_t xdata g_rw_gamvrr1_0047h_GAM_VRR_LUT_OFST12_1_new_data_71;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8047h
extern volatile uint8_t xdata g_rw_gamvrr1_0048h_GAM_VRR_LUT_OFST12_1_new_data_72;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8048h
extern volatile uint8_t xdata g_rw_gamvrr1_0049h_GAM_VRR_LUT_OFST12_1_new_data_73;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8049h
extern volatile uint8_t xdata g_rw_gamvrr1_004Ah_GAM_VRR_LUT_OFST12_1_new_data_74;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 804Ah

extern volatile uint8_t xdata g_rw_gamvrr1_004Bh_GAM_VRR_LUT_OFST12_1_new_data_75;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 804Bh
extern volatile uint8_t xdata g_rw_gamvrr1_004Ch_GAM_VRR_LUT_OFST12_1_new_data_76;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 804Ch
extern volatile uint8_t xdata g_rw_gamvrr1_004Dh_GAM_VRR_LUT_OFST12_1_new_data_77;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 804Dh
extern volatile uint8_t xdata g_rw_gamvrr1_004Eh_GAM_VRR_LUT_OFST12_1_new_data_78;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 804Eh
extern volatile uint8_t xdata g_rw_gamvrr1_004Fh_GAM_VRR_LUT_OFST12_1_new_data_79;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 804Fh

extern volatile uint8_t xdata g_rw_gamvrr1_0050h_GAM_VRR_LUT_OFST12_1_new_data_80;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8050h
extern volatile uint8_t xdata g_rw_gamvrr1_0051h_GAM_VRR_LUT_OFST12_1_new_data_81;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8051h
extern volatile uint8_t xdata g_rw_gamvrr1_0052h_GAM_VRR_LUT_OFST12_1_new_data_82;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8052h
extern volatile uint8_t xdata g_rw_gamvrr1_0053h_GAM_VRR_LUT_OFST12_1_new_data_83;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8053h
extern volatile uint8_t xdata g_rw_gamvrr1_0054h_GAM_VRR_LUT_OFST12_1_new_data_84;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8054h

extern volatile uint8_t xdata g_rw_gamvrr1_0055h_GAM_VRR_LUT_OFST12_1_new_data_85;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8055h
extern volatile uint8_t xdata g_rw_gamvrr1_0056h_GAM_VRR_LUT_OFST12_1_new_data_86;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8056h
extern volatile uint8_t xdata g_rw_gamvrr1_0057h_GAM_VRR_LUT_OFST12_1_new_data_87;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8057h
extern volatile uint8_t xdata g_rw_gamvrr1_0058h_GAM_VRR_LUT_OFST12_1_new_data_88;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8058h
extern volatile uint8_t xdata g_rw_gamvrr1_0059h_GAM_VRR_LUT_OFST12_1_new_data_89;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8059h

extern volatile uint8_t xdata g_rw_gamvrr1_005Ah_GAM_VRR_LUT_OFST12_1_new_data_90;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 805Ah
extern volatile uint8_t xdata g_rw_gamvrr1_005Bh_GAM_VRR_LUT_OFST12_1_new_data_91;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 805Bh
extern volatile uint8_t xdata g_rw_gamvrr1_005Ch_GAM_VRR_LUT_OFST12_1_new_data_92;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 805Ch
extern volatile uint8_t xdata g_rw_gamvrr1_005Dh_GAM_VRR_LUT_OFST12_1_new_data_93;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 805Dh
extern volatile uint8_t xdata g_rw_gamvrr1_005Eh_GAM_VRR_LUT_OFST12_1_new_data_94;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 805Eh

extern volatile uint8_t xdata g_rw_gamvrr1_005Fh_GAM_VRR_LUT_OFST12_1_new_data_95;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 805Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0060h_GAM_VRR_LUT_OFST12_1_new_data_96;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8060h
extern volatile uint8_t xdata g_rw_gamvrr1_0061h_GAM_VRR_LUT_OFST12_1_new_data_97;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8061h
extern volatile uint8_t xdata g_rw_gamvrr1_0062h_GAM_VRR_LUT_OFST12_1_new_data_98;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8062h
extern volatile uint8_t xdata g_rw_gamvrr1_0063h_GAM_VRR_LUT_OFST12_1_new_data_99;                       // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8063h

extern volatile uint8_t xdata g_rw_gamvrr1_0064h_GAM_VRR_LUT_OFST12_1_new_data_100;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8064h
extern volatile uint8_t xdata g_rw_gamvrr1_0065h_GAM_VRR_LUT_OFST12_1_new_data_101;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8065h
extern volatile uint8_t xdata g_rw_gamvrr1_0066h_GAM_VRR_LUT_OFST12_1_new_data_102;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8066h
extern volatile uint8_t xdata g_rw_gamvrr1_0067h_GAM_VRR_LUT_OFST12_1_new_data_103;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8067h
extern volatile uint8_t xdata g_rw_gamvrr1_0068h_GAM_VRR_LUT_OFST12_1_new_data_104;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8068h

extern volatile uint8_t xdata g_rw_gamvrr1_0069h_GAM_VRR_LUT_OFST12_1_new_data_105;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8069h
extern volatile uint8_t xdata g_rw_gamvrr1_006Ah_GAM_VRR_LUT_OFST12_1_new_data_106;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 806Ah
extern volatile uint8_t xdata g_rw_gamvrr1_006Bh_GAM_VRR_LUT_OFST12_1_new_data_107;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 806Bh
extern volatile uint8_t xdata g_rw_gamvrr1_006Ch_GAM_VRR_LUT_OFST12_1_new_data_108;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 806Ch
extern volatile uint8_t xdata g_rw_gamvrr1_006Dh_GAM_VRR_LUT_OFST12_1_new_data_109;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 806Dh

extern volatile uint8_t xdata g_rw_gamvrr1_006Eh_GAM_VRR_LUT_OFST12_1_new_data_110;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 806Eh
extern volatile uint8_t xdata g_rw_gamvrr1_006Fh_GAM_VRR_LUT_OFST12_1_new_data_111;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 806Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0070h_GAM_VRR_LUT_OFST12_1_new_data_112;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8070h
extern volatile uint8_t xdata g_rw_gamvrr1_0071h_GAM_VRR_LUT_OFST12_1_new_data_113;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8071h
extern volatile uint8_t xdata g_rw_gamvrr1_0072h_GAM_VRR_LUT_OFST12_1_new_data_114;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8072h

extern volatile uint8_t xdata g_rw_gamvrr1_0073h_GAM_VRR_LUT_OFST12_1_new_data_115;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8073h
extern volatile uint8_t xdata g_rw_gamvrr1_0074h_GAM_VRR_LUT_OFST12_1_new_data_116;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8074h
extern volatile uint8_t xdata g_rw_gamvrr1_0075h_GAM_VRR_LUT_OFST12_1_new_data_117;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8075h
extern volatile uint8_t xdata g_rw_gamvrr1_0076h_GAM_VRR_LUT_OFST12_1_new_data_118;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8076h
extern volatile uint8_t xdata g_rw_gamvrr1_0077h_GAM_VRR_LUT_OFST12_1_new_data_119;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8077h

extern volatile uint8_t xdata g_rw_gamvrr1_0078h_GAM_VRR_LUT_OFST12_1_new_data_120;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8078h
extern volatile uint8_t xdata g_rw_gamvrr1_0079h_GAM_VRR_LUT_OFST12_1_new_data_121;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8079h
extern volatile uint8_t xdata g_rw_gamvrr1_007Ah_GAM_VRR_LUT_OFST12_1_new_data_122;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 807Ah
extern volatile uint8_t xdata g_rw_gamvrr1_007Bh_GAM_VRR_LUT_OFST12_1_new_data_123;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 807Bh
extern volatile uint8_t xdata g_rw_gamvrr1_007Ch_GAM_VRR_LUT_OFST12_1_new_data_124;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 807Ch

extern volatile uint8_t xdata g_rw_gamvrr1_007Dh_GAM_VRR_LUT_OFST12_1_new_data_125;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 807Dh
extern volatile uint8_t xdata g_rw_gamvrr1_007Eh_GAM_VRR_LUT_OFST12_1_new_data_126;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 807Eh
extern volatile uint8_t xdata g_rw_gamvrr1_007Fh_GAM_VRR_LUT_OFST12_1_new_data_127;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 807Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0080h_GAM_VRR_LUT_OFST12_1_new_data_128;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8080h
extern volatile uint8_t xdata g_rw_gamvrr1_0081h_GAM_VRR_LUT_OFST12_1_new_data_129;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8081h

extern volatile uint8_t xdata g_rw_gamvrr1_0082h_GAM_VRR_LUT_OFST12_1_new_data_130;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8082h
extern volatile uint8_t xdata g_rw_gamvrr1_0083h_GAM_VRR_LUT_OFST12_1_new_data_131;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8083h
extern volatile uint8_t xdata g_rw_gamvrr1_0084h_GAM_VRR_LUT_OFST12_1_new_data_132;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8084h
extern volatile uint8_t xdata g_rw_gamvrr1_0085h_GAM_VRR_LUT_OFST12_1_new_data_133;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8085h
extern volatile uint8_t xdata g_rw_gamvrr1_0086h_GAM_VRR_LUT_OFST12_1_new_data_134;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8086h

extern volatile uint8_t xdata g_rw_gamvrr1_0087h_GAM_VRR_LUT_OFST12_1_new_data_135;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8087h
extern volatile uint8_t xdata g_rw_gamvrr1_0088h_GAM_VRR_LUT_OFST12_1_new_data_136;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8088h
extern volatile uint8_t xdata g_rw_gamvrr1_0089h_GAM_VRR_LUT_OFST12_1_new_data_137;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8089h
extern volatile uint8_t xdata g_rw_gamvrr1_008Ah_GAM_VRR_LUT_OFST12_1_new_data_138;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 808Ah
extern volatile uint8_t xdata g_rw_gamvrr1_008Bh_GAM_VRR_LUT_OFST12_1_new_data_139;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 808Bh

extern volatile uint8_t xdata g_rw_gamvrr1_008Ch_GAM_VRR_LUT_OFST12_1_new_data_140;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 808Ch
extern volatile uint8_t xdata g_rw_gamvrr1_008Dh_GAM_VRR_LUT_OFST12_1_new_data_141;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 808Dh
extern volatile uint8_t xdata g_rw_gamvrr1_008Eh_GAM_VRR_LUT_OFST12_1_new_data_142;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 808Eh
extern volatile uint8_t xdata g_rw_gamvrr1_008Fh_GAM_VRR_LUT_OFST12_1_new_data_143;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 808Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0090h_GAM_VRR_LUT_OFST12_1_new_data_144;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8090h

extern volatile uint8_t xdata g_rw_gamvrr1_0091h_GAM_VRR_LUT_OFST12_1_new_data_145;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8091h
extern volatile uint8_t xdata g_rw_gamvrr1_0092h_GAM_VRR_LUT_OFST12_1_new_data_146;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8092h
extern volatile uint8_t xdata g_rw_gamvrr1_0093h_GAM_VRR_LUT_OFST12_1_new_data_147;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8093h
extern volatile uint8_t xdata g_rw_gamvrr1_0094h_GAM_VRR_LUT_OFST12_1_new_data_148;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8094h
extern volatile uint8_t xdata g_rw_gamvrr1_0095h_GAM_VRR_LUT_OFST12_1_new_data_149;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8095h

extern volatile uint8_t xdata g_rw_gamvrr1_0096h_GAM_VRR_LUT_OFST12_1_new_data_150;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8096h
extern volatile uint8_t xdata g_rw_gamvrr1_0097h_GAM_VRR_LUT_OFST12_1_new_data_151;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8097h
extern volatile uint8_t xdata g_rw_gamvrr1_0098h_GAM_VRR_LUT_OFST12_1_new_data_152;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8098h
extern volatile uint8_t xdata g_rw_gamvrr1_0099h_GAM_VRR_LUT_OFST12_1_new_data_153;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8099h
extern volatile uint8_t xdata g_rw_gamvrr1_009Ah_GAM_VRR_LUT_OFST12_1_new_data_154;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 809Ah

extern volatile uint8_t xdata g_rw_gamvrr1_009Bh_GAM_VRR_LUT_OFST12_1_new_data_155;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 809Bh
extern volatile uint8_t xdata g_rw_gamvrr1_009Ch_GAM_VRR_LUT_OFST12_1_new_data_156;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 809Ch
extern volatile uint8_t xdata g_rw_gamvrr1_009Dh_GAM_VRR_LUT_OFST12_1_new_data_157;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 809Dh
extern volatile uint8_t xdata g_rw_gamvrr1_009Eh_GAM_VRR_LUT_OFST12_1_new_data_158;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 809Eh
extern volatile uint8_t xdata g_rw_gamvrr1_009Fh_GAM_VRR_LUT_OFST12_1_new_data_159;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 809Fh

extern volatile uint8_t xdata g_rw_gamvrr1_00A0h_GAM_VRR_LUT_OFST12_1_new_data_160;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80A0h
extern volatile uint8_t xdata g_rw_gamvrr1_00A1h_GAM_VRR_LUT_OFST12_1_new_data_161;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80A1h
extern volatile uint8_t xdata g_rw_gamvrr1_00A2h_GAM_VRR_LUT_OFST12_1_new_data_162;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80A2h
extern volatile uint8_t xdata g_rw_gamvrr1_00A3h_GAM_VRR_LUT_OFST12_1_new_data_163;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80A3h
extern volatile uint8_t xdata g_rw_gamvrr1_00A4h_GAM_VRR_LUT_OFST12_1_new_data_164;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80A4h

extern volatile uint8_t xdata g_rw_gamvrr1_00A5h_GAM_VRR_LUT_OFST12_1_new_data_165;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80A5h
extern volatile uint8_t xdata g_rw_gamvrr1_00A6h_GAM_VRR_LUT_OFST12_1_new_data_166;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80A6h
extern volatile uint8_t xdata g_rw_gamvrr1_00A7h_GAM_VRR_LUT_OFST12_1_new_data_167;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80A7h
extern volatile uint8_t xdata g_rw_gamvrr1_00A8h_GAM_VRR_LUT_OFST12_1_new_data_168;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80A8h
extern volatile uint8_t xdata g_rw_gamvrr1_00A9h_GAM_VRR_LUT_OFST12_1_new_data_169;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80A9h

extern volatile uint8_t xdata g_rw_gamvrr1_00AAh_GAM_VRR_LUT_OFST12_1_new_data_170;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80AAh
extern volatile uint8_t xdata g_rw_gamvrr1_00ABh_GAM_VRR_LUT_OFST12_1_new_data_171;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80ABh
extern volatile uint8_t xdata g_rw_gamvrr1_00ACh_GAM_VRR_LUT_OFST12_1_new_data_172;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80ACh
extern volatile uint8_t xdata g_rw_gamvrr1_00ADh_GAM_VRR_LUT_OFST12_1_new_data_173;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80ADh
extern volatile uint8_t xdata g_rw_gamvrr1_00AEh_GAM_VRR_LUT_OFST12_1_new_data_174;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80AEh

extern volatile uint8_t xdata g_rw_gamvrr1_00AFh_GAM_VRR_LUT_OFST12_1_new_data_175;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80AFh
extern volatile uint8_t xdata g_rw_gamvrr1_00B0h_GAM_VRR_LUT_OFST12_1_new_data_176;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80B0h
extern volatile uint8_t xdata g_rw_gamvrr1_00B1h_GAM_VRR_LUT_OFST12_1_new_data_177;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80B1h
extern volatile uint8_t xdata g_rw_gamvrr1_00B2h_GAM_VRR_LUT_OFST12_1_new_data_178;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80B2h
extern volatile uint8_t xdata g_rw_gamvrr1_00B3h_GAM_VRR_LUT_OFST12_1_new_data_179;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80B3h

extern volatile uint8_t xdata g_rw_gamvrr1_00B4h_GAM_VRR_LUT_OFST12_1_new_data_180;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80B4h
extern volatile uint8_t xdata g_rw_gamvrr1_00B5h_GAM_VRR_LUT_OFST12_1_new_data_181;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80B5h
extern volatile uint8_t xdata g_rw_gamvrr1_00B6h_GAM_VRR_LUT_OFST12_1_new_data_182;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80B6h
extern volatile uint8_t xdata g_rw_gamvrr1_00B7h_GAM_VRR_LUT_OFST12_1_new_data_183;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80B7h
extern volatile uint8_t xdata g_rw_gamvrr1_00B8h_GAM_VRR_LUT_OFST12_1_new_data_184;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80B8h

extern volatile uint8_t xdata g_rw_gamvrr1_00B9h_GAM_VRR_LUT_OFST12_1_new_data_185;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80B9h
extern volatile uint8_t xdata g_rw_gamvrr1_00BAh_GAM_VRR_LUT_OFST12_1_new_data_186;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80BAh
extern volatile uint8_t xdata g_rw_gamvrr1_00BBh_GAM_VRR_LUT_OFST12_1_new_data_187;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80BBh
extern volatile uint8_t xdata g_rw_gamvrr1_00BCh_GAM_VRR_LUT_OFST12_1_new_data_188;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80BCh
extern volatile uint8_t xdata g_rw_gamvrr1_00BDh_GAM_VRR_LUT_OFST12_1_new_data_189;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80BDh

extern volatile uint8_t xdata g_rw_gamvrr1_00BEh_GAM_VRR_LUT_OFST12_1_new_data_190;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80BEh
extern volatile uint8_t xdata g_rw_gamvrr1_00BFh_GAM_VRR_LUT_OFST12_1_new_data_191;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80BFh
extern volatile uint8_t xdata g_rw_gamvrr1_00C0h_GAM_VRR_LUT_OFST12_1_new_data_192;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80C0h
extern volatile uint8_t xdata g_rw_gamvrr1_00C1h_GAM_VRR_LUT_OFST12_1_new_data_193;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80C1h
extern volatile uint8_t xdata g_rw_gamvrr1_00C2h_GAM_VRR_LUT_OFST12_1_new_data_194;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80C2h

extern volatile uint8_t xdata g_rw_gamvrr1_00C3h_GAM_VRR_LUT_OFST12_1_new_data_195;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80C3h
extern volatile uint8_t xdata g_rw_gamvrr1_00C4h_GAM_VRR_LUT_OFST12_1_new_data_196;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80C4h
extern volatile uint8_t xdata g_rw_gamvrr1_00C5h_GAM_VRR_LUT_OFST12_1_new_data_197;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80C5h
extern volatile uint8_t xdata g_rw_gamvrr1_00C6h_GAM_VRR_LUT_OFST12_1_new_data_198;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80C6h
extern volatile uint8_t xdata g_rw_gamvrr1_00C7h_GAM_VRR_LUT_OFST12_1_new_data_199;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80C7h

extern volatile uint8_t xdata g_rw_gamvrr1_00C8h_GAM_VRR_LUT_OFST12_1_new_data_200;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80C8h
extern volatile uint8_t xdata g_rw_gamvrr1_00C9h_GAM_VRR_LUT_OFST12_1_new_data_201;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80C9h
extern volatile uint8_t xdata g_rw_gamvrr1_00CAh_GAM_VRR_LUT_OFST12_1_new_data_202;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80CAh
extern volatile uint8_t xdata g_rw_gamvrr1_00CBh_GAM_VRR_LUT_OFST12_1_new_data_203;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80CBh
extern volatile uint8_t xdata g_rw_gamvrr1_00CCh_GAM_VRR_LUT_OFST12_1_new_data_204;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80CCh

extern volatile uint8_t xdata g_rw_gamvrr1_00CDh_GAM_VRR_LUT_OFST12_1_new_data_205;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80CDh
extern volatile uint8_t xdata g_rw_gamvrr1_00CEh_GAM_VRR_LUT_OFST12_1_new_data_206;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80CEh
extern volatile uint8_t xdata g_rw_gamvrr1_00CFh_GAM_VRR_LUT_OFST12_1_new_data_207;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80CFh
extern volatile uint8_t xdata g_rw_gamvrr1_00D0h_GAM_VRR_LUT_OFST12_1_new_data_208;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80D0h
extern volatile uint8_t xdata g_rw_gamvrr1_00D1h_GAM_VRR_LUT_OFST12_1_new_data_209;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80D1h

extern volatile uint8_t xdata g_rw_gamvrr1_00D2h_GAM_VRR_LUT_OFST12_1_new_data_210;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80D2h
extern volatile uint8_t xdata g_rw_gamvrr1_00D3h_GAM_VRR_LUT_OFST12_1_new_data_211;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80D3h
extern volatile uint8_t xdata g_rw_gamvrr1_00D4h_GAM_VRR_LUT_OFST12_1_new_data_212;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80D4h
extern volatile uint8_t xdata g_rw_gamvrr1_00D5h_GAM_VRR_LUT_OFST12_1_new_data_213;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80D5h
extern volatile uint8_t xdata g_rw_gamvrr1_00D6h_GAM_VRR_LUT_OFST12_1_new_data_214;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80D6h

extern volatile uint8_t xdata g_rw_gamvrr1_00D7h_GAM_VRR_LUT_OFST12_1_new_data_215;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80D7h
extern volatile uint8_t xdata g_rw_gamvrr1_00D8h_GAM_VRR_LUT_OFST12_1_new_data_216;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80D8h
extern volatile uint8_t xdata g_rw_gamvrr1_00D9h_GAM_VRR_LUT_OFST12_1_new_data_217;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80D9h
extern volatile uint8_t xdata g_rw_gamvrr1_00DAh_GAM_VRR_LUT_OFST12_1_new_data_218;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80DAh
extern volatile uint8_t xdata g_rw_gamvrr1_00DBh_GAM_VRR_LUT_OFST12_1_new_data_219;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80DBh

extern volatile uint8_t xdata g_rw_gamvrr1_00DCh_GAM_VRR_LUT_OFST12_1_new_data_220;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80DCh
extern volatile uint8_t xdata g_rw_gamvrr1_00DDh_GAM_VRR_LUT_OFST12_1_new_data_221;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80DDh
extern volatile uint8_t xdata g_rw_gamvrr1_00DEh_GAM_VRR_LUT_OFST12_1_new_data_222;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80DEh
extern volatile uint8_t xdata g_rw_gamvrr1_00DFh_GAM_VRR_LUT_OFST12_1_new_data_223;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80DFh
extern volatile uint8_t xdata g_rw_gamvrr1_00E0h_GAM_VRR_LUT_OFST12_1_new_data_224;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80E0h

extern volatile uint8_t xdata g_rw_gamvrr1_00E1h_GAM_VRR_LUT_OFST12_1_new_data_225;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80E1h
extern volatile uint8_t xdata g_rw_gamvrr1_00E2h_GAM_VRR_LUT_OFST12_1_new_data_226;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80E2h
extern volatile uint8_t xdata g_rw_gamvrr1_00E3h_GAM_VRR_LUT_OFST12_1_new_data_227;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80E3h
extern volatile uint8_t xdata g_rw_gamvrr1_00E4h_GAM_VRR_LUT_OFST12_1_new_data_228;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80E4h
extern volatile uint8_t xdata g_rw_gamvrr1_00E5h_GAM_VRR_LUT_OFST12_1_new_data_229;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80E5h

extern volatile uint8_t xdata g_rw_gamvrr1_00E6h_GAM_VRR_LUT_OFST12_1_new_data_230;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80E6h
extern volatile uint8_t xdata g_rw_gamvrr1_00E7h_GAM_VRR_LUT_OFST12_1_new_data_231;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80E7h
extern volatile uint8_t xdata g_rw_gamvrr1_00E8h_GAM_VRR_LUT_OFST12_1_new_data_232;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80E8h
extern volatile uint8_t xdata g_rw_gamvrr1_00E9h_GAM_VRR_LUT_OFST12_1_new_data_233;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80E9h
extern volatile uint8_t xdata g_rw_gamvrr1_00EAh_GAM_VRR_LUT_OFST12_1_new_data_234;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80EAh

extern volatile uint8_t xdata g_rw_gamvrr1_00EBh_GAM_VRR_LUT_OFST12_1_new_data_235;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80EBh
extern volatile uint8_t xdata g_rw_gamvrr1_00ECh_GAM_VRR_LUT_OFST12_1_new_data_236;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80ECh
extern volatile uint8_t xdata g_rw_gamvrr1_00EDh_GAM_VRR_LUT_OFST12_1_new_data_237;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80EDh
extern volatile uint8_t xdata g_rw_gamvrr1_00EEh_GAM_VRR_LUT_OFST12_1_new_data_238;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80EEh
extern volatile uint8_t xdata g_rw_gamvrr1_00EFh_GAM_VRR_LUT_OFST12_1_new_data_239;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80EFh

extern volatile uint8_t xdata g_rw_gamvrr1_00F0h_GAM_VRR_LUT_OFST12_1_new_data_240;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80F0h
extern volatile uint8_t xdata g_rw_gamvrr1_00F1h_GAM_VRR_LUT_OFST12_1_new_data_241;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80F1h
extern volatile uint8_t xdata g_rw_gamvrr1_00F2h_GAM_VRR_LUT_OFST12_1_new_data_242;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80F2h
extern volatile uint8_t xdata g_rw_gamvrr1_00F3h_GAM_VRR_LUT_OFST12_1_new_data_243;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80F3h
extern volatile uint8_t xdata g_rw_gamvrr1_00F4h_GAM_VRR_LUT_OFST12_1_new_data_244;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80F4h

extern volatile uint8_t xdata g_rw_gamvrr1_00F5h_GAM_VRR_LUT_OFST12_1_new_data_245;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80F5h
extern volatile uint8_t xdata g_rw_gamvrr1_00F6h_GAM_VRR_LUT_OFST12_1_new_data_246;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80F6h
extern volatile uint8_t xdata g_rw_gamvrr1_00F7h_GAM_VRR_LUT_OFST12_1_new_data_247;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80F7h
extern volatile uint8_t xdata g_rw_gamvrr1_00F8h_GAM_VRR_LUT_OFST12_1_new_data_248;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80F8h
extern volatile uint8_t xdata g_rw_gamvrr1_00F9h_GAM_VRR_LUT_OFST12_1_new_data_249;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80F9h

extern volatile uint8_t xdata g_rw_gamvrr1_00FAh_GAM_VRR_LUT_OFST12_1_new_data_250;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80FAh
extern volatile uint8_t xdata g_rw_gamvrr1_00FBh_GAM_VRR_LUT_OFST12_1_new_data_251;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80FBh
extern volatile uint8_t xdata g_rw_gamvrr1_00FCh_GAM_VRR_LUT_OFST12_1_new_data_252;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 80FCh
extern volatile uint8_t xdata g_rw_gamvrr1_00FDh_GAM_VRR_LUT_OFST12_1_new_data_253;                      // [msb:lsb] = [7:0], val = 32, 	Absolute Address = 80FDh
extern volatile uint8_t xdata g_rw_gamvrr1_00FEh_GAM_VRR_LUT_OFST12_1_new_data_254;                      // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 80FEh

extern volatile uint8_t xdata g_rw_gamvrr1_00FFh_GAM_VRR_LUT_OFST12_1_new_data_255;                      // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 80FFh
extern volatile uint8_t xdata g_rw_gamvrr1_0100h_GAM_VRR_LUT_OFST12_1_new_data_256;                      // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 8100h
extern volatile uint8_t xdata g_rw_gamvrr1_0101h_GAM_VRR_LUT_OFST12_1_new_data_257;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8101h
extern volatile uint8_t xdata g_rw_gamvrr1_0102h_GAM_VRR_LUT_OFST12_1_new_data_258;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8102h
extern volatile uint8_t xdata g_rw_gamvrr1_0103h_GAM_VRR_LUT_OFST12_1_new_data_259;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8103h

extern volatile uint8_t xdata g_rw_gamvrr1_0104h_GAM_VRR_LUT_OFST12_1_new_data_260;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8104h
extern volatile uint8_t xdata g_rw_gamvrr1_0105h_GAM_VRR_LUT_OFST12_1_new_data_261;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8105h
extern volatile uint8_t xdata g_rw_gamvrr1_0106h_GAM_VRR_LUT_OFST12_1_new_data_262;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8106h
extern volatile uint8_t xdata g_rw_gamvrr1_0107h_GAM_VRR_LUT_OFST12_1_new_data_263;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8107h
extern volatile uint8_t xdata g_rw_gamvrr1_0108h_GAM_VRR_LUT_OFST12_1_new_data_264;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8108h

extern volatile uint8_t xdata g_rw_gamvrr1_0109h_GAM_VRR_LUT_OFST12_1_new_data_265;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8109h
extern volatile uint8_t xdata g_rw_gamvrr1_010Ah_GAM_VRR_LUT_OFST12_1_new_data_266;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 810Ah
extern volatile uint8_t xdata g_rw_gamvrr1_010Bh_GAM_VRR_LUT_OFST12_1_new_data_267;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 810Bh
extern volatile uint8_t xdata g_rw_gamvrr1_010Ch_GAM_VRR_LUT_OFST12_1_new_data_268;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 810Ch
extern volatile uint8_t xdata g_rw_gamvrr1_010Dh_GAM_VRR_LUT_OFST12_1_new_data_269;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 810Dh

extern volatile uint8_t xdata g_rw_gamvrr1_010Eh_GAM_VRR_LUT_OFST12_1_new_data_270;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 810Eh
extern volatile uint8_t xdata g_rw_gamvrr1_010Fh_GAM_VRR_LUT_OFST12_1_new_data_271;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 810Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0110h_GAM_VRR_LUT_OFST12_1_new_data_272;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8110h
extern volatile uint8_t xdata g_rw_gamvrr1_0111h_GAM_VRR_LUT_OFST12_1_new_data_273;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8111h
extern volatile uint8_t xdata g_rw_gamvrr1_0112h_GAM_VRR_LUT_OFST12_1_new_data_274;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8112h

extern volatile uint8_t xdata g_rw_gamvrr1_0113h_GAM_VRR_LUT_OFST12_1_new_data_275;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8113h
extern volatile uint8_t xdata g_rw_gamvrr1_0114h_GAM_VRR_LUT_OFST12_1_new_data_276;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8114h
extern volatile uint8_t xdata g_rw_gamvrr1_0115h_GAM_VRR_LUT_OFST12_1_new_data_277;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8115h
extern volatile uint8_t xdata g_rw_gamvrr1_0116h_GAM_VRR_LUT_OFST12_1_new_data_278;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8116h
extern volatile uint8_t xdata g_rw_gamvrr1_0117h_GAM_VRR_LUT_OFST12_1_new_data_279;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8117h

extern volatile uint8_t xdata g_rw_gamvrr1_0118h_GAM_VRR_LUT_OFST12_1_new_data_280;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8118h
extern volatile uint8_t xdata g_rw_gamvrr1_0119h_GAM_VRR_LUT_OFST12_1_new_data_281;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8119h
extern volatile uint8_t xdata g_rw_gamvrr1_011Ah_GAM_VRR_LUT_OFST12_1_new_data_282;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 811Ah
extern volatile uint8_t xdata g_rw_gamvrr1_011Bh_GAM_VRR_LUT_OFST12_1_new_data_283;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 811Bh
extern volatile uint8_t xdata g_rw_gamvrr1_011Ch_GAM_VRR_LUT_OFST12_1_new_data_284;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 811Ch

extern volatile uint8_t xdata g_rw_gamvrr1_011Dh_GAM_VRR_LUT_OFST12_1_new_data_285;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 811Dh
extern volatile uint8_t xdata g_rw_gamvrr1_011Eh_GAM_VRR_LUT_OFST12_1_new_data_286;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 811Eh
extern volatile uint8_t xdata g_rw_gamvrr1_011Fh_GAM_VRR_LUT_OFST12_1_new_data_287;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 811Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0120h_GAM_VRR_LUT_OFST12_1_new_data_288;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8120h
extern volatile uint8_t xdata g_rw_gamvrr1_0121h_GAM_VRR_LUT_OFST12_1_new_data_289;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8121h

extern volatile uint8_t xdata g_rw_gamvrr1_0122h_GAM_VRR_LUT_OFST12_1_new_data_290;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8122h
extern volatile uint8_t xdata g_rw_gamvrr1_0123h_GAM_VRR_LUT_OFST12_1_new_data_291;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8123h
extern volatile uint8_t xdata g_rw_gamvrr1_0124h_GAM_VRR_LUT_OFST12_1_new_data_292;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8124h
extern volatile uint8_t xdata g_rw_gamvrr1_0125h_GAM_VRR_LUT_OFST12_1_new_data_293;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8125h
extern volatile uint8_t xdata g_rw_gamvrr1_0126h_GAM_VRR_LUT_OFST12_1_new_data_294;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8126h

extern volatile uint8_t xdata g_rw_gamvrr1_0127h_GAM_VRR_LUT_OFST12_1_new_data_295;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8127h
extern volatile uint8_t xdata g_rw_gamvrr1_0128h_GAM_VRR_LUT_OFST12_1_new_data_296;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8128h
extern volatile uint8_t xdata g_rw_gamvrr1_0129h_GAM_VRR_LUT_OFST12_1_new_data_297;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8129h
extern volatile uint8_t xdata g_rw_gamvrr1_012Ah_GAM_VRR_LUT_OFST12_1_new_data_298;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 812Ah
extern volatile uint8_t xdata g_rw_gamvrr1_012Bh_GAM_VRR_LUT_OFST12_1_new_data_299;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 812Bh

extern volatile uint8_t xdata g_rw_gamvrr1_012Ch_GAM_VRR_LUT_OFST12_1_new_data_300;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 812Ch
extern volatile uint8_t xdata g_rw_gamvrr1_012Dh_GAM_VRR_LUT_OFST12_1_new_data_301;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 812Dh
extern volatile uint8_t xdata g_rw_gamvrr1_012Eh_GAM_VRR_LUT_OFST12_1_new_data_302;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 812Eh
extern volatile uint8_t xdata g_rw_gamvrr1_012Fh_GAM_VRR_LUT_OFST12_1_new_data_303;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 812Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0130h_GAM_VRR_LUT_OFST12_1_new_data_304;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8130h

extern volatile uint8_t xdata g_rw_gamvrr1_0131h_GAM_VRR_LUT_OFST12_1_new_data_305;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8131h
extern volatile uint8_t xdata g_rw_gamvrr1_0132h_GAM_VRR_LUT_OFST12_1_new_data_306;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8132h
extern volatile uint8_t xdata g_rw_gamvrr1_0133h_GAM_VRR_LUT_OFST12_1_new_data_307;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8133h
extern volatile uint8_t xdata g_rw_gamvrr1_0134h_GAM_VRR_LUT_OFST12_1_new_data_308;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8134h
extern volatile uint8_t xdata g_rw_gamvrr1_0135h_GAM_VRR_LUT_OFST12_1_new_data_309;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8135h

extern volatile uint8_t xdata g_rw_gamvrr1_0136h_GAM_VRR_LUT_OFST12_1_new_data_310;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8136h
extern volatile uint8_t xdata g_rw_gamvrr1_0137h_GAM_VRR_LUT_OFST12_1_new_data_311;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8137h
extern volatile uint8_t xdata g_rw_gamvrr1_0138h_GAM_VRR_LUT_OFST12_1_new_data_312;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8138h
extern volatile uint8_t xdata g_rw_gamvrr1_0139h_GAM_VRR_LUT_OFST12_1_new_data_313;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8139h
extern volatile uint8_t xdata g_rw_gamvrr1_013Ah_GAM_VRR_LUT_OFST12_1_new_data_314;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 813Ah

extern volatile uint8_t xdata g_rw_gamvrr1_013Bh_GAM_VRR_LUT_OFST12_1_new_data_315;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 813Bh
extern volatile uint8_t xdata g_rw_gamvrr1_013Ch_GAM_VRR_LUT_OFST12_1_new_data_316;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 813Ch
extern volatile uint8_t xdata g_rw_gamvrr1_013Dh_GAM_VRR_LUT_OFST12_1_new_data_317;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 813Dh
extern volatile uint8_t xdata g_rw_gamvrr1_013Eh_GAM_VRR_LUT_OFST12_1_new_data_318;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 813Eh
extern volatile uint8_t xdata g_rw_gamvrr1_013Fh_GAM_VRR_LUT_OFST12_1_new_data_319;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 813Fh

extern volatile uint8_t xdata g_rw_gamvrr1_0140h_GAM_VRR_LUT_OFST12_1_new_data_320;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8140h
extern volatile uint8_t xdata g_rw_gamvrr1_0141h_GAM_VRR_LUT_OFST12_1_new_data_321;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8141h
extern volatile uint8_t xdata g_rw_gamvrr1_0142h_GAM_VRR_LUT_OFST12_1_new_data_322;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8142h
extern volatile uint8_t xdata g_rw_gamvrr1_0143h_GAM_VRR_LUT_OFST12_1_new_data_323;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8143h
extern volatile uint8_t xdata g_rw_gamvrr1_0144h_GAM_VRR_LUT_OFST12_1_new_data_324;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8144h

extern volatile uint8_t xdata g_rw_gamvrr1_0145h_GAM_VRR_LUT_OFST12_1_new_data_325;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8145h
extern volatile uint8_t xdata g_rw_gamvrr1_0146h_GAM_VRR_LUT_OFST12_1_new_data_326;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8146h
extern volatile uint8_t xdata g_rw_gamvrr1_0147h_GAM_VRR_LUT_OFST12_1_new_data_327;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8147h
extern volatile uint8_t xdata g_rw_gamvrr1_0148h_GAM_VRR_LUT_OFST12_1_new_data_328;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8148h
extern volatile uint8_t xdata g_rw_gamvrr1_0149h_GAM_VRR_LUT_OFST12_1_new_data_329;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8149h

extern volatile uint8_t xdata g_rw_gamvrr1_014Ah_GAM_VRR_LUT_OFST12_1_new_data_330;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 814Ah
extern volatile uint8_t xdata g_rw_gamvrr1_014Bh_GAM_VRR_LUT_OFST12_1_new_data_331;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 814Bh
extern volatile uint8_t xdata g_rw_gamvrr1_014Ch_GAM_VRR_LUT_OFST12_1_new_data_332;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 814Ch
extern volatile uint8_t xdata g_rw_gamvrr1_014Dh_GAM_VRR_LUT_OFST12_1_new_data_333;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 814Dh
extern volatile uint8_t xdata g_rw_gamvrr1_014Eh_GAM_VRR_LUT_OFST12_1_new_data_334;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 814Eh

extern volatile uint8_t xdata g_rw_gamvrr1_014Fh_GAM_VRR_LUT_OFST12_1_new_data_335;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 814Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0150h_GAM_VRR_LUT_OFST12_1_new_data_336;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8150h
extern volatile uint8_t xdata g_rw_gamvrr1_0151h_GAM_VRR_LUT_OFST12_1_new_data_337;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8151h
extern volatile uint8_t xdata g_rw_gamvrr1_0152h_GAM_VRR_LUT_OFST12_1_new_data_338;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8152h
extern volatile uint8_t xdata g_rw_gamvrr1_0153h_GAM_VRR_LUT_OFST12_1_new_data_339;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8153h

extern volatile uint8_t xdata g_rw_gamvrr1_0154h_GAM_VRR_LUT_OFST12_1_new_data_340;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8154h
extern volatile uint8_t xdata g_rw_gamvrr1_0155h_GAM_VRR_LUT_OFST12_1_new_data_341;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8155h
extern volatile uint8_t xdata g_rw_gamvrr1_0156h_GAM_VRR_LUT_OFST12_1_new_data_342;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8156h
extern volatile uint8_t xdata g_rw_gamvrr1_0157h_GAM_VRR_LUT_OFST12_1_new_data_343;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8157h
extern volatile uint8_t xdata g_rw_gamvrr1_0158h_GAM_VRR_LUT_OFST12_1_new_data_344;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8158h

extern volatile uint8_t xdata g_rw_gamvrr1_0159h_GAM_VRR_LUT_OFST12_1_new_data_345;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8159h
extern volatile uint8_t xdata g_rw_gamvrr1_015Ah_GAM_VRR_LUT_OFST12_1_new_data_346;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 815Ah
extern volatile uint8_t xdata g_rw_gamvrr1_015Bh_GAM_VRR_LUT_OFST12_1_new_data_347;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 815Bh
extern volatile uint8_t xdata g_rw_gamvrr1_015Ch_GAM_VRR_LUT_OFST12_1_new_data_348;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 815Ch
extern volatile uint8_t xdata g_rw_gamvrr1_015Dh_GAM_VRR_LUT_OFST12_1_new_data_349;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 815Dh

extern volatile uint8_t xdata g_rw_gamvrr1_015Eh_GAM_VRR_LUT_OFST12_1_new_data_350;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 815Eh
extern volatile uint8_t xdata g_rw_gamvrr1_015Fh_GAM_VRR_LUT_OFST12_1_new_data_351;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 815Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0160h_GAM_VRR_LUT_OFST12_1_new_data_352;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8160h
extern volatile uint8_t xdata g_rw_gamvrr1_0161h_GAM_VRR_LUT_OFST12_1_new_data_353;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8161h
extern volatile uint8_t xdata g_rw_gamvrr1_0162h_GAM_VRR_LUT_OFST12_1_new_data_354;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8162h

extern volatile uint8_t xdata g_rw_gamvrr1_0163h_GAM_VRR_LUT_OFST12_1_new_data_355;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8163h
extern volatile uint8_t xdata g_rw_gamvrr1_0164h_GAM_VRR_LUT_OFST12_1_new_data_356;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8164h
extern volatile uint8_t xdata g_rw_gamvrr1_0165h_GAM_VRR_LUT_OFST12_1_new_data_357;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8165h
extern volatile uint8_t xdata g_rw_gamvrr1_0166h_GAM_VRR_LUT_OFST12_1_new_data_358;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8166h
extern volatile uint8_t xdata g_rw_gamvrr1_0167h_GAM_VRR_LUT_OFST12_1_new_data_359;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8167h

extern volatile uint8_t xdata g_rw_gamvrr1_0168h_GAM_VRR_LUT_OFST12_1_new_data_360;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8168h
extern volatile uint8_t xdata g_rw_gamvrr1_0169h_GAM_VRR_LUT_OFST12_1_new_data_361;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8169h
extern volatile uint8_t xdata g_rw_gamvrr1_016Ah_GAM_VRR_LUT_OFST12_1_new_data_362;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 816Ah
extern volatile uint8_t xdata g_rw_gamvrr1_016Bh_GAM_VRR_LUT_OFST12_1_new_data_363;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 816Bh
extern volatile uint8_t xdata g_rw_gamvrr1_016Ch_GAM_VRR_LUT_OFST12_1_new_data_364;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 816Ch

extern volatile uint8_t xdata g_rw_gamvrr1_016Dh_GAM_VRR_LUT_OFST12_1_new_data_365;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 816Dh
extern volatile uint8_t xdata g_rw_gamvrr1_016Eh_GAM_VRR_LUT_OFST12_1_new_data_366;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 816Eh
extern volatile uint8_t xdata g_rw_gamvrr1_016Fh_GAM_VRR_LUT_OFST12_1_new_data_367;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 816Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0170h_GAM_VRR_LUT_OFST12_1_new_data_368;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8170h
extern volatile uint8_t xdata g_rw_gamvrr1_0171h_GAM_VRR_LUT_OFST12_1_new_data_369;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8171h

extern volatile uint8_t xdata g_rw_gamvrr1_0172h_GAM_VRR_LUT_OFST12_1_new_data_370;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8172h
extern volatile uint8_t xdata g_rw_gamvrr1_0173h_GAM_VRR_LUT_OFST12_1_new_data_371;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8173h
extern volatile uint8_t xdata g_rw_gamvrr1_0174h_GAM_VRR_LUT_OFST12_1_new_data_372;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8174h
extern volatile uint8_t xdata g_rw_gamvrr1_0175h_GAM_VRR_LUT_OFST12_1_new_data_373;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8175h
extern volatile uint8_t xdata g_rw_gamvrr1_0176h_GAM_VRR_LUT_OFST12_1_new_data_374;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8176h

extern volatile uint8_t xdata g_rw_gamvrr1_0177h_GAM_VRR_LUT_OFST12_1_new_data_375;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8177h
extern volatile uint8_t xdata g_rw_gamvrr1_0178h_GAM_VRR_LUT_OFST12_1_new_data_376;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8178h
extern volatile uint8_t xdata g_rw_gamvrr1_0179h_GAM_VRR_LUT_OFST12_1_new_data_377;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8179h
extern volatile uint8_t xdata g_rw_gamvrr1_017Ah_GAM_VRR_LUT_OFST12_1_new_data_378;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 817Ah
extern volatile uint8_t xdata g_rw_gamvrr1_017Bh_GAM_VRR_LUT_OFST12_1_new_data_379;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 817Bh

extern volatile uint8_t xdata g_rw_gamvrr1_017Ch_GAM_VRR_LUT_OFST12_1_new_data_380;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 817Ch
extern volatile uint8_t xdata g_rw_gamvrr1_017Dh_GAM_VRR_LUT_OFST12_1_new_data_381;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 817Dh
extern volatile uint8_t xdata g_rw_gamvrr1_017Eh_GAM_VRR_LUT_OFST12_1_new_data_382;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 817Eh
extern volatile uint8_t xdata g_rw_gamvrr1_017Fh_GAM_VRR_LUT_OFST12_1_new_data_383;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 817Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0180h_GAM_VRR_LUT_OFST12_1_new_data_384;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8180h

extern volatile uint8_t xdata g_rw_gamvrr1_0181h_GAM_VRR_LUT_OFST12_1_new_data_385;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8181h
extern volatile uint8_t xdata g_rw_gamvrr1_0182h_GAM_VRR_LUT_OFST12_1_new_data_386;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8182h
extern volatile uint8_t xdata g_rw_gamvrr1_0183h_GAM_VRR_LUT_OFST12_1_new_data_387;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8183h
extern volatile uint8_t xdata g_rw_gamvrr1_0184h_GAM_VRR_LUT_OFST12_1_new_data_388;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8184h
extern volatile uint8_t xdata g_rw_gamvrr1_0185h_GAM_VRR_LUT_OFST12_1_new_data_389;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8185h

extern volatile uint8_t xdata g_rw_gamvrr1_0186h_GAM_VRR_LUT_OFST12_1_new_data_390;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8186h
extern volatile uint8_t xdata g_rw_gamvrr1_0187h_GAM_VRR_LUT_OFST12_1_new_data_391;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8187h
extern volatile uint8_t xdata g_rw_gamvrr1_0188h_GAM_VRR_LUT_OFST12_1_new_data_392;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8188h
extern volatile uint8_t xdata g_rw_gamvrr1_0189h_GAM_VRR_LUT_OFST12_1_new_data_393;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8189h
extern volatile uint8_t xdata g_rw_gamvrr1_018Ah_GAM_VRR_LUT_OFST12_1_new_data_394;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 818Ah

extern volatile uint8_t xdata g_rw_gamvrr1_018Bh_GAM_VRR_LUT_OFST12_1_new_data_395;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 818Bh
extern volatile uint8_t xdata g_rw_gamvrr1_018Ch_GAM_VRR_LUT_OFST12_1_new_data_396;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 818Ch
extern volatile uint8_t xdata g_rw_gamvrr1_018Dh_GAM_VRR_LUT_OFST12_1_new_data_397;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 818Dh
extern volatile uint8_t xdata g_rw_gamvrr1_018Eh_GAM_VRR_LUT_OFST12_1_new_data_398;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 818Eh
extern volatile uint8_t xdata g_rw_gamvrr1_018Fh_GAM_VRR_LUT_OFST12_1_new_data_399;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 818Fh

extern volatile uint8_t xdata g_rw_gamvrr1_0190h_GAM_VRR_LUT_OFST12_1_new_data_400;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8190h
extern volatile uint8_t xdata g_rw_gamvrr1_0191h_GAM_VRR_LUT_OFST12_1_new_data_401;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8191h
extern volatile uint8_t xdata g_rw_gamvrr1_0192h_GAM_VRR_LUT_OFST12_1_new_data_402;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8192h
extern volatile uint8_t xdata g_rw_gamvrr1_0193h_GAM_VRR_LUT_OFST12_1_new_data_403;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8193h
extern volatile uint8_t xdata g_rw_gamvrr1_0194h_GAM_VRR_LUT_OFST12_1_new_data_404;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8194h

extern volatile uint8_t xdata g_rw_gamvrr1_0195h_GAM_VRR_LUT_OFST12_1_new_data_405;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8195h
extern volatile uint8_t xdata g_rw_gamvrr1_0196h_GAM_VRR_LUT_OFST12_1_new_data_406;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8196h
extern volatile uint8_t xdata g_rw_gamvrr1_0197h_GAM_VRR_LUT_OFST12_1_new_data_407;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8197h
extern volatile uint8_t xdata g_rw_gamvrr1_0198h_GAM_VRR_LUT_OFST12_1_new_data_408;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8198h
extern volatile uint8_t xdata g_rw_gamvrr1_0199h_GAM_VRR_LUT_OFST12_1_new_data_409;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8199h

extern volatile uint8_t xdata g_rw_gamvrr1_019Ah_GAM_VRR_LUT_OFST12_1_new_data_410;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 819Ah
extern volatile uint8_t xdata g_rw_gamvrr1_019Bh_GAM_VRR_LUT_OFST12_1_new_data_411;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 819Bh
extern volatile uint8_t xdata g_rw_gamvrr1_019Ch_GAM_VRR_LUT_OFST12_1_new_data_412;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 819Ch
extern volatile uint8_t xdata g_rw_gamvrr1_019Dh_GAM_VRR_LUT_OFST12_1_new_data_413;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 819Dh
extern volatile uint8_t xdata g_rw_gamvrr1_019Eh_GAM_VRR_LUT_OFST12_1_new_data_414;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 819Eh

extern volatile uint8_t xdata g_rw_gamvrr1_019Fh_GAM_VRR_LUT_OFST12_1_new_data_415;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 819Fh
extern volatile uint8_t xdata g_rw_gamvrr1_01A0h_GAM_VRR_LUT_OFST12_1_new_data_416;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81A0h
extern volatile uint8_t xdata g_rw_gamvrr1_01A1h_GAM_VRR_LUT_OFST12_1_new_data_417;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81A1h
extern volatile uint8_t xdata g_rw_gamvrr1_01A2h_GAM_VRR_LUT_OFST12_1_new_data_418;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81A2h
extern volatile uint8_t xdata g_rw_gamvrr1_01A3h_GAM_VRR_LUT_OFST12_1_new_data_419;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81A3h

extern volatile uint8_t xdata g_rw_gamvrr1_01A4h_GAM_VRR_LUT_OFST12_1_new_data_420;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81A4h
extern volatile uint8_t xdata g_rw_gamvrr1_01A5h_GAM_VRR_LUT_OFST12_1_new_data_421;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81A5h
extern volatile uint8_t xdata g_rw_gamvrr1_01A6h_GAM_VRR_LUT_OFST12_1_new_data_422;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81A6h
extern volatile uint8_t xdata g_rw_gamvrr1_01A7h_GAM_VRR_LUT_OFST12_1_new_data_423;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81A7h
extern volatile uint8_t xdata g_rw_gamvrr1_01A8h_GAM_VRR_LUT_OFST12_1_new_data_424;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81A8h

extern volatile uint8_t xdata g_rw_gamvrr1_01A9h_GAM_VRR_LUT_OFST12_1_new_data_425;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81A9h
extern volatile uint8_t xdata g_rw_gamvrr1_01AAh_GAM_VRR_LUT_OFST12_1_new_data_426;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81AAh
extern volatile uint8_t xdata g_rw_gamvrr1_01ABh_GAM_VRR_LUT_OFST12_1_new_data_427;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81ABh
extern volatile uint8_t xdata g_rw_gamvrr1_01ACh_GAM_VRR_LUT_OFST12_1_new_data_428;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81ACh
extern volatile uint8_t xdata g_rw_gamvrr1_01ADh_GAM_VRR_LUT_OFST12_1_new_data_429;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81ADh

extern volatile uint8_t xdata g_rw_gamvrr1_01AEh_GAM_VRR_LUT_OFST12_1_new_data_430;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81AEh
extern volatile uint8_t xdata g_rw_gamvrr1_01AFh_GAM_VRR_LUT_OFST12_1_new_data_431;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81AFh
extern volatile uint8_t xdata g_rw_gamvrr1_01B0h_GAM_VRR_LUT_OFST12_1_new_data_432;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81B0h
extern volatile uint8_t xdata g_rw_gamvrr1_01B1h_GAM_VRR_LUT_OFST12_1_new_data_433;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81B1h
extern volatile uint8_t xdata g_rw_gamvrr1_01B2h_GAM_VRR_LUT_OFST12_1_new_data_434;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81B2h

extern volatile uint8_t xdata g_rw_gamvrr1_01B3h_GAM_VRR_LUT_OFST12_1_new_data_435;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81B3h
extern volatile uint8_t xdata g_rw_gamvrr1_01B4h_GAM_VRR_LUT_OFST12_1_new_data_436;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81B4h
extern volatile uint8_t xdata g_rw_gamvrr1_01B5h_GAM_VRR_LUT_OFST12_1_new_data_437;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81B5h
extern volatile uint8_t xdata g_rw_gamvrr1_01B6h_GAM_VRR_LUT_OFST12_1_new_data_438;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81B6h
extern volatile uint8_t xdata g_rw_gamvrr1_01B7h_GAM_VRR_LUT_OFST12_1_new_data_439;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81B7h

extern volatile uint8_t xdata g_rw_gamvrr1_01B8h_GAM_VRR_LUT_OFST12_1_new_data_440;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81B8h
extern volatile uint8_t xdata g_rw_gamvrr1_01B9h_GAM_VRR_LUT_OFST12_1_new_data_441;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81B9h
extern volatile uint8_t xdata g_rw_gamvrr1_01BAh_GAM_VRR_LUT_OFST12_1_new_data_442;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81BAh
extern volatile uint8_t xdata g_rw_gamvrr1_01BBh_GAM_VRR_LUT_OFST12_1_new_data_443;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81BBh
extern volatile uint8_t xdata g_rw_gamvrr1_01BCh_GAM_VRR_LUT_OFST12_1_new_data_444;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81BCh

extern volatile uint8_t xdata g_rw_gamvrr1_01BDh_GAM_VRR_LUT_OFST12_1_new_data_445;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81BDh
extern volatile uint8_t xdata g_rw_gamvrr1_01BEh_GAM_VRR_LUT_OFST12_1_new_data_446;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81BEh
extern volatile uint8_t xdata g_rw_gamvrr1_01BFh_GAM_VRR_LUT_OFST12_1_new_data_447;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81BFh
extern volatile uint8_t xdata g_rw_gamvrr1_01C0h_GAM_VRR_LUT_OFST12_1_new_data_448;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81C0h
extern volatile uint8_t xdata g_rw_gamvrr1_01C1h_GAM_VRR_LUT_OFST12_1_new_data_449;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81C1h

extern volatile uint8_t xdata g_rw_gamvrr1_01C2h_GAM_VRR_LUT_OFST12_1_new_data_450;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81C2h
extern volatile uint8_t xdata g_rw_gamvrr1_01C3h_GAM_VRR_LUT_OFST12_1_new_data_451;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81C3h
extern volatile uint8_t xdata g_rw_gamvrr1_01C4h_GAM_VRR_LUT_OFST12_1_new_data_452;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81C4h
extern volatile uint8_t xdata g_rw_gamvrr1_01C5h_GAM_VRR_LUT_OFST12_1_new_data_453;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81C5h
extern volatile uint8_t xdata g_rw_gamvrr1_01C6h_GAM_VRR_LUT_OFST12_1_new_data_454;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81C6h

extern volatile uint8_t xdata g_rw_gamvrr1_01C7h_GAM_VRR_LUT_OFST12_1_new_data_455;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81C7h
extern volatile uint8_t xdata g_rw_gamvrr1_01C8h_GAM_VRR_LUT_OFST12_1_new_data_456;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81C8h
extern volatile uint8_t xdata g_rw_gamvrr1_01C9h_GAM_VRR_LUT_OFST12_1_new_data_457;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81C9h
extern volatile uint8_t xdata g_rw_gamvrr1_01CAh_GAM_VRR_LUT_OFST12_1_new_data_458;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81CAh
extern volatile uint8_t xdata g_rw_gamvrr1_01CBh_GAM_VRR_LUT_OFST12_1_new_data_459;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81CBh

extern volatile uint8_t xdata g_rw_gamvrr1_01CCh_GAM_VRR_LUT_OFST12_1_new_data_460;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81CCh
extern volatile uint8_t xdata g_rw_gamvrr1_01CDh_GAM_VRR_LUT_OFST12_1_new_data_461;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81CDh
extern volatile uint8_t xdata g_rw_gamvrr1_01CEh_GAM_VRR_LUT_OFST12_1_new_data_462;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81CEh
extern volatile uint8_t xdata g_rw_gamvrr1_01CFh_GAM_VRR_LUT_OFST12_1_new_data_463;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81CFh
extern volatile uint8_t xdata g_rw_gamvrr1_01D0h_GAM_VRR_LUT_OFST12_1_new_data_464;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81D0h

extern volatile uint8_t xdata g_rw_gamvrr1_01D1h_GAM_VRR_LUT_OFST12_1_new_data_465;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81D1h
extern volatile uint8_t xdata g_rw_gamvrr1_01D2h_GAM_VRR_LUT_OFST12_1_new_data_466;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81D2h
extern volatile uint8_t xdata g_rw_gamvrr1_01D3h_GAM_VRR_LUT_OFST12_1_new_data_467;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81D3h
extern volatile uint8_t xdata g_rw_gamvrr1_01D4h_GAM_VRR_LUT_OFST12_1_new_data_468;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81D4h
extern volatile uint8_t xdata g_rw_gamvrr1_01D5h_GAM_VRR_LUT_OFST12_1_new_data_469;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81D5h

extern volatile uint8_t xdata g_rw_gamvrr1_01D6h_GAM_VRR_LUT_OFST12_1_new_data_470;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81D6h
extern volatile uint8_t xdata g_rw_gamvrr1_01D7h_GAM_VRR_LUT_OFST12_1_new_data_471;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81D7h
extern volatile uint8_t xdata g_rw_gamvrr1_01D8h_GAM_VRR_LUT_OFST12_1_new_data_472;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81D8h
extern volatile uint8_t xdata g_rw_gamvrr1_01D9h_GAM_VRR_LUT_OFST12_1_new_data_473;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81D9h
extern volatile uint8_t xdata g_rw_gamvrr1_01DAh_GAM_VRR_LUT_OFST12_1_new_data_474;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81DAh

extern volatile uint8_t xdata g_rw_gamvrr1_01DBh_GAM_VRR_LUT_OFST12_1_new_data_475;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81DBh
extern volatile uint8_t xdata g_rw_gamvrr1_01DCh_GAM_VRR_LUT_OFST12_1_new_data_476;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81DCh
extern volatile uint8_t xdata g_rw_gamvrr1_01DDh_GAM_VRR_LUT_OFST12_1_new_data_477;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81DDh
extern volatile uint8_t xdata g_rw_gamvrr1_01DEh_GAM_VRR_LUT_OFST12_1_new_data_478;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81DEh
extern volatile uint8_t xdata g_rw_gamvrr1_01DFh_GAM_VRR_LUT_OFST12_1_new_data_479;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81DFh

extern volatile uint8_t xdata g_rw_gamvrr1_01E0h_GAM_VRR_LUT_OFST12_1_new_data_480;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81E0h
extern volatile uint8_t xdata g_rw_gamvrr1_01E1h_GAM_VRR_LUT_OFST12_1_new_data_481;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81E1h
extern volatile uint8_t xdata g_rw_gamvrr1_01E2h_GAM_VRR_LUT_OFST12_1_new_data_482;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81E2h
extern volatile uint8_t xdata g_rw_gamvrr1_01E3h_GAM_VRR_LUT_OFST12_1_new_data_483;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81E3h
extern volatile uint8_t xdata g_rw_gamvrr1_01E4h_GAM_VRR_LUT_OFST12_1_new_data_484;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81E4h

extern volatile uint8_t xdata g_rw_gamvrr1_01E5h_GAM_VRR_LUT_OFST12_1_new_data_485;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81E5h
extern volatile uint8_t xdata g_rw_gamvrr1_01E6h_GAM_VRR_LUT_OFST12_1_new_data_486;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81E6h
extern volatile uint8_t xdata g_rw_gamvrr1_01E7h_GAM_VRR_LUT_OFST12_1_new_data_487;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81E7h
extern volatile uint8_t xdata g_rw_gamvrr1_01E8h_GAM_VRR_LUT_OFST12_1_new_data_488;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81E8h
extern volatile uint8_t xdata g_rw_gamvrr1_01E9h_GAM_VRR_LUT_OFST12_1_new_data_489;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81E9h

extern volatile uint8_t xdata g_rw_gamvrr1_01EAh_GAM_VRR_LUT_OFST12_1_new_data_490;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81EAh
extern volatile uint8_t xdata g_rw_gamvrr1_01EBh_GAM_VRR_LUT_OFST12_1_new_data_491;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81EBh
extern volatile uint8_t xdata g_rw_gamvrr1_01ECh_GAM_VRR_LUT_OFST12_1_new_data_492;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81ECh
extern volatile uint8_t xdata g_rw_gamvrr1_01EDh_GAM_VRR_LUT_OFST12_1_new_data_493;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81EDh
extern volatile uint8_t xdata g_rw_gamvrr1_01EEh_GAM_VRR_LUT_OFST12_1_new_data_494;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81EEh

extern volatile uint8_t xdata g_rw_gamvrr1_01EFh_GAM_VRR_LUT_OFST12_1_new_data_495;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81EFh
extern volatile uint8_t xdata g_rw_gamvrr1_01F0h_GAM_VRR_LUT_OFST12_1_new_data_496;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81F0h
extern volatile uint8_t xdata g_rw_gamvrr1_01F1h_GAM_VRR_LUT_OFST12_1_new_data_497;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81F1h
extern volatile uint8_t xdata g_rw_gamvrr1_01F2h_GAM_VRR_LUT_OFST12_1_new_data_498;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81F2h
extern volatile uint8_t xdata g_rw_gamvrr1_01F3h_GAM_VRR_LUT_OFST12_1_new_data_499;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81F3h

extern volatile uint8_t xdata g_rw_gamvrr1_01F4h_GAM_VRR_LUT_OFST12_1_new_data_500;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81F4h
extern volatile uint8_t xdata g_rw_gamvrr1_01F5h_GAM_VRR_LUT_OFST12_1_new_data_501;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81F5h
extern volatile uint8_t xdata g_rw_gamvrr1_01F6h_GAM_VRR_LUT_OFST12_1_new_data_502;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81F6h
extern volatile uint8_t xdata g_rw_gamvrr1_01F7h_GAM_VRR_LUT_OFST12_1_new_data_503;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81F7h
extern volatile uint8_t xdata g_rw_gamvrr1_01F8h_GAM_VRR_LUT_OFST12_1_new_data_504;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81F8h

extern volatile uint8_t xdata g_rw_gamvrr1_01F9h_GAM_VRR_LUT_OFST12_1_new_data_505;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81F9h
extern volatile uint8_t xdata g_rw_gamvrr1_01FAh_GAM_VRR_LUT_OFST12_1_new_data_506;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81FAh
extern volatile uint8_t xdata g_rw_gamvrr1_01FBh_GAM_VRR_LUT_OFST12_1_new_data_507;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81FBh
extern volatile uint8_t xdata g_rw_gamvrr1_01FCh_GAM_VRR_LUT_OFST12_1_new_data_508;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81FCh
extern volatile uint8_t xdata g_rw_gamvrr1_01FDh_GAM_VRR_LUT_OFST12_1_new_data_509;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81FDh

extern volatile uint8_t xdata g_rw_gamvrr1_01FEh_GAM_VRR_LUT_OFST12_1_new_data_510;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 81FEh
extern volatile uint8_t xdata g_rw_gamvrr1_01FFh_GAM_VRR_LUT_OFST12_1_new_data_511;                      // [msb:lsb] = [7:0], val = 32, 	Absolute Address = 81FFh
extern volatile uint8_t xdata g_rw_gamvrr1_0200h_GAM_VRR_LUT_OFST12_1_new_data_512;                      // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 8200h
extern volatile uint8_t xdata g_rw_gamvrr1_0201h_GAM_VRR_LUT_OFST12_1_new_data_513;                      // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 8201h
extern volatile uint8_t xdata g_rw_gamvrr1_0202h_GAM_VRR_LUT_OFST12_1_new_data_514;                      // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 8202h

extern volatile uint8_t xdata g_rw_gamvrr1_0203h_GAM_VRR_LUT_OFST12_1_new_data_515;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8203h
extern volatile uint8_t xdata g_rw_gamvrr1_0204h_GAM_VRR_LUT_OFST12_1_new_data_516;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8204h
extern volatile uint8_t xdata g_rw_gamvrr1_0205h_GAM_VRR_LUT_OFST12_1_new_data_517;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8205h
extern volatile uint8_t xdata g_rw_gamvrr1_0206h_GAM_VRR_LUT_OFST12_1_new_data_518;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8206h
extern volatile uint8_t xdata g_rw_gamvrr1_0207h_GAM_VRR_LUT_OFST12_1_new_data_519;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8207h

extern volatile uint8_t xdata g_rw_gamvrr1_0208h_GAM_VRR_LUT_OFST12_1_new_data_520;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8208h
extern volatile uint8_t xdata g_rw_gamvrr1_0209h_GAM_VRR_LUT_OFST12_1_new_data_521;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8209h
extern volatile uint8_t xdata g_rw_gamvrr1_020Ah_GAM_VRR_LUT_OFST12_1_new_data_522;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 820Ah
extern volatile uint8_t xdata g_rw_gamvrr1_020Bh_GAM_VRR_LUT_OFST12_1_new_data_523;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 820Bh
extern volatile uint8_t xdata g_rw_gamvrr1_020Ch_GAM_VRR_LUT_OFST12_1_new_data_524;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 820Ch

extern volatile uint8_t xdata g_rw_gamvrr1_020Dh_GAM_VRR_LUT_OFST12_1_new_data_525;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 820Dh
extern volatile uint8_t xdata g_rw_gamvrr1_020Eh_GAM_VRR_LUT_OFST12_1_new_data_526;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 820Eh
extern volatile uint8_t xdata g_rw_gamvrr1_020Fh_GAM_VRR_LUT_OFST12_1_new_data_527;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 820Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0210h_GAM_VRR_LUT_OFST12_1_new_data_528;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8210h
extern volatile uint8_t xdata g_rw_gamvrr1_0211h_GAM_VRR_LUT_OFST12_1_new_data_529;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8211h

extern volatile uint8_t xdata g_rw_gamvrr1_0212h_GAM_VRR_LUT_OFST12_1_new_data_530;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8212h
extern volatile uint8_t xdata g_rw_gamvrr1_0213h_GAM_VRR_LUT_OFST12_1_new_data_531;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8213h
extern volatile uint8_t xdata g_rw_gamvrr1_0214h_GAM_VRR_LUT_OFST12_1_new_data_532;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8214h
extern volatile uint8_t xdata g_rw_gamvrr1_0215h_GAM_VRR_LUT_OFST12_1_new_data_533;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8215h
extern volatile uint8_t xdata g_rw_gamvrr1_0216h_GAM_VRR_LUT_OFST12_1_new_data_534;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8216h

extern volatile uint8_t xdata g_rw_gamvrr1_0217h_GAM_VRR_LUT_OFST12_1_new_data_535;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8217h
extern volatile uint8_t xdata g_rw_gamvrr1_0218h_GAM_VRR_LUT_OFST12_1_new_data_536;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8218h
extern volatile uint8_t xdata g_rw_gamvrr1_0219h_GAM_VRR_LUT_OFST12_1_new_data_537;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8219h
extern volatile uint8_t xdata g_rw_gamvrr1_021Ah_GAM_VRR_LUT_OFST12_1_new_data_538;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 821Ah
extern volatile uint8_t xdata g_rw_gamvrr1_021Bh_GAM_VRR_LUT_OFST12_1_new_data_539;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 821Bh

extern volatile uint8_t xdata g_rw_gamvrr1_021Ch_GAM_VRR_LUT_OFST12_1_new_data_540;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 821Ch
extern volatile uint8_t xdata g_rw_gamvrr1_021Dh_GAM_VRR_LUT_OFST12_1_new_data_541;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 821Dh
extern volatile uint8_t xdata g_rw_gamvrr1_021Eh_GAM_VRR_LUT_OFST12_1_new_data_542;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 821Eh
extern volatile uint8_t xdata g_rw_gamvrr1_021Fh_GAM_VRR_LUT_OFST12_1_new_data_543;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 821Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0220h_GAM_VRR_LUT_OFST12_1_new_data_544;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8220h

extern volatile uint8_t xdata g_rw_gamvrr1_0221h_GAM_VRR_LUT_OFST12_1_new_data_545;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8221h
extern volatile uint8_t xdata g_rw_gamvrr1_0222h_GAM_VRR_LUT_OFST12_1_new_data_546;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8222h
extern volatile uint8_t xdata g_rw_gamvrr1_0223h_GAM_VRR_LUT_OFST12_1_new_data_547;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8223h
extern volatile uint8_t xdata g_rw_gamvrr1_0224h_GAM_VRR_LUT_OFST12_1_new_data_548;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8224h
extern volatile uint8_t xdata g_rw_gamvrr1_0225h_GAM_VRR_LUT_OFST12_1_new_data_549;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8225h

extern volatile uint8_t xdata g_rw_gamvrr1_0226h_GAM_VRR_LUT_OFST12_1_new_data_550;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8226h
extern volatile uint8_t xdata g_rw_gamvrr1_0227h_GAM_VRR_LUT_OFST12_1_new_data_551;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8227h
extern volatile uint8_t xdata g_rw_gamvrr1_0228h_GAM_VRR_LUT_OFST12_1_new_data_552;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8228h
extern volatile uint8_t xdata g_rw_gamvrr1_0229h_GAM_VRR_LUT_OFST12_1_new_data_553;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8229h
extern volatile uint8_t xdata g_rw_gamvrr1_022Ah_GAM_VRR_LUT_OFST12_1_new_data_554;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 822Ah

extern volatile uint8_t xdata g_rw_gamvrr1_022Bh_GAM_VRR_LUT_OFST12_1_new_data_555;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 822Bh
extern volatile uint8_t xdata g_rw_gamvrr1_022Ch_GAM_VRR_LUT_OFST12_1_new_data_556;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 822Ch
extern volatile uint8_t xdata g_rw_gamvrr1_022Dh_GAM_VRR_LUT_OFST12_1_new_data_557;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 822Dh
extern volatile uint8_t xdata g_rw_gamvrr1_022Eh_GAM_VRR_LUT_OFST12_1_new_data_558;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 822Eh
extern volatile uint8_t xdata g_rw_gamvrr1_022Fh_GAM_VRR_LUT_OFST12_1_new_data_559;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 822Fh

extern volatile uint8_t xdata g_rw_gamvrr1_0230h_GAM_VRR_LUT_OFST12_1_new_data_560;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8230h
extern volatile uint8_t xdata g_rw_gamvrr1_0231h_GAM_VRR_LUT_OFST12_1_new_data_561;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8231h
extern volatile uint8_t xdata g_rw_gamvrr1_0232h_GAM_VRR_LUT_OFST12_1_new_data_562;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8232h
extern volatile uint8_t xdata g_rw_gamvrr1_0233h_GAM_VRR_LUT_OFST12_1_new_data_563;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8233h
extern volatile uint8_t xdata g_rw_gamvrr1_0234h_GAM_VRR_LUT_OFST12_1_new_data_564;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8234h

extern volatile uint8_t xdata g_rw_gamvrr1_0235h_GAM_VRR_LUT_OFST12_1_new_data_565;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8235h
extern volatile uint8_t xdata g_rw_gamvrr1_0236h_GAM_VRR_LUT_OFST12_1_new_data_566;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8236h
extern volatile uint8_t xdata g_rw_gamvrr1_0237h_GAM_VRR_LUT_OFST12_1_new_data_567;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8237h
extern volatile uint8_t xdata g_rw_gamvrr1_0238h_GAM_VRR_LUT_OFST12_1_new_data_568;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8238h
extern volatile uint8_t xdata g_rw_gamvrr1_0239h_GAM_VRR_LUT_OFST12_1_new_data_569;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8239h

extern volatile uint8_t xdata g_rw_gamvrr1_023Ah_GAM_VRR_LUT_OFST12_1_new_data_570;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 823Ah
extern volatile uint8_t xdata g_rw_gamvrr1_023Bh_GAM_VRR_LUT_OFST12_1_new_data_571;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 823Bh
extern volatile uint8_t xdata g_rw_gamvrr1_023Ch_GAM_VRR_LUT_OFST12_1_new_data_572;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 823Ch
extern volatile uint8_t xdata g_rw_gamvrr1_023Dh_GAM_VRR_LUT_OFST12_1_new_data_573;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 823Dh
extern volatile uint8_t xdata g_rw_gamvrr1_023Eh_GAM_VRR_LUT_OFST12_1_new_data_574;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 823Eh

extern volatile uint8_t xdata g_rw_gamvrr1_023Fh_GAM_VRR_LUT_OFST12_1_new_data_575;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 823Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0240h_GAM_VRR_LUT_OFST12_1_new_data_576;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8240h
extern volatile uint8_t xdata g_rw_gamvrr1_0241h_GAM_VRR_LUT_OFST12_1_new_data_577;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8241h
extern volatile uint8_t xdata g_rw_gamvrr1_0242h_GAM_VRR_LUT_OFST12_1_new_data_578;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8242h
extern volatile uint8_t xdata g_rw_gamvrr1_0243h_GAM_VRR_LUT_OFST12_1_new_data_579;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8243h

extern volatile uint8_t xdata g_rw_gamvrr1_0244h_GAM_VRR_LUT_OFST12_1_new_data_580;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8244h
extern volatile uint8_t xdata g_rw_gamvrr1_0245h_GAM_VRR_LUT_OFST12_1_new_data_581;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8245h
extern volatile uint8_t xdata g_rw_gamvrr1_0246h_GAM_VRR_LUT_OFST12_1_new_data_582;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8246h
extern volatile uint8_t xdata g_rw_gamvrr1_0247h_GAM_VRR_LUT_OFST12_1_new_data_583;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8247h
extern volatile uint8_t xdata g_rw_gamvrr1_0248h_GAM_VRR_LUT_OFST12_1_new_data_584;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8248h

extern volatile uint8_t xdata g_rw_gamvrr1_0249h_GAM_VRR_LUT_OFST12_1_new_data_585;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8249h
extern volatile uint8_t xdata g_rw_gamvrr1_024Ah_GAM_VRR_LUT_OFST12_1_new_data_586;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 824Ah
extern volatile uint8_t xdata g_rw_gamvrr1_024Bh_GAM_VRR_LUT_OFST12_1_new_data_587;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 824Bh
extern volatile uint8_t xdata g_rw_gamvrr1_024Ch_GAM_VRR_LUT_OFST12_1_new_data_588;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 824Ch
extern volatile uint8_t xdata g_rw_gamvrr1_024Dh_GAM_VRR_LUT_OFST12_1_new_data_589;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 824Dh

extern volatile uint8_t xdata g_rw_gamvrr1_024Eh_GAM_VRR_LUT_OFST12_1_new_data_590;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 824Eh
extern volatile uint8_t xdata g_rw_gamvrr1_024Fh_GAM_VRR_LUT_OFST12_1_new_data_591;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 824Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0250h_GAM_VRR_LUT_OFST12_1_new_data_592;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8250h
extern volatile uint8_t xdata g_rw_gamvrr1_0251h_GAM_VRR_LUT_OFST12_1_new_data_593;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8251h
extern volatile uint8_t xdata g_rw_gamvrr1_0252h_GAM_VRR_LUT_OFST12_1_new_data_594;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8252h

extern volatile uint8_t xdata g_rw_gamvrr1_0253h_GAM_VRR_LUT_OFST12_1_new_data_595;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8253h
extern volatile uint8_t xdata g_rw_gamvrr1_0254h_GAM_VRR_LUT_OFST12_1_new_data_596;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8254h
extern volatile uint8_t xdata g_rw_gamvrr1_0255h_GAM_VRR_LUT_OFST12_1_new_data_597;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8255h
extern volatile uint8_t xdata g_rw_gamvrr1_0256h_GAM_VRR_LUT_OFST12_1_new_data_598;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8256h
extern volatile uint8_t xdata g_rw_gamvrr1_0257h_GAM_VRR_LUT_OFST12_1_new_data_599;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8257h

extern volatile uint8_t xdata g_rw_gamvrr1_0258h_GAM_VRR_LUT_OFST12_1_new_data_600;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8258h
extern volatile uint8_t xdata g_rw_gamvrr1_0259h_GAM_VRR_LUT_OFST12_1_new_data_601;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8259h
extern volatile uint8_t xdata g_rw_gamvrr1_025Ah_GAM_VRR_LUT_OFST12_1_new_data_602;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 825Ah
extern volatile uint8_t xdata g_rw_gamvrr1_025Bh_GAM_VRR_LUT_OFST12_1_new_data_603;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 825Bh
extern volatile uint8_t xdata g_rw_gamvrr1_025Ch_GAM_VRR_LUT_OFST12_1_new_data_604;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 825Ch

extern volatile uint8_t xdata g_rw_gamvrr1_025Dh_GAM_VRR_LUT_OFST12_1_new_data_605;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 825Dh
extern volatile uint8_t xdata g_rw_gamvrr1_025Eh_GAM_VRR_LUT_OFST12_1_new_data_606;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 825Eh
extern volatile uint8_t xdata g_rw_gamvrr1_025Fh_GAM_VRR_LUT_OFST12_1_new_data_607;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 825Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0260h_GAM_VRR_LUT_OFST12_1_new_data_608;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8260h
extern volatile uint8_t xdata g_rw_gamvrr1_0261h_GAM_VRR_LUT_OFST12_1_new_data_609;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8261h

extern volatile uint8_t xdata g_rw_gamvrr1_0262h_GAM_VRR_LUT_OFST12_1_new_data_610;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8262h
extern volatile uint8_t xdata g_rw_gamvrr1_0263h_GAM_VRR_LUT_OFST12_1_new_data_611;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8263h
extern volatile uint8_t xdata g_rw_gamvrr1_0264h_GAM_VRR_LUT_OFST12_1_new_data_612;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8264h
extern volatile uint8_t xdata g_rw_gamvrr1_0265h_GAM_VRR_LUT_OFST12_1_new_data_613;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8265h
extern volatile uint8_t xdata g_rw_gamvrr1_0266h_GAM_VRR_LUT_OFST12_1_new_data_614;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8266h

extern volatile uint8_t xdata g_rw_gamvrr1_0267h_GAM_VRR_LUT_OFST12_1_new_data_615;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8267h
extern volatile uint8_t xdata g_rw_gamvrr1_0268h_GAM_VRR_LUT_OFST12_1_new_data_616;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8268h
extern volatile uint8_t xdata g_rw_gamvrr1_0269h_GAM_VRR_LUT_OFST12_1_new_data_617;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8269h
extern volatile uint8_t xdata g_rw_gamvrr1_026Ah_GAM_VRR_LUT_OFST12_1_new_data_618;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 826Ah
extern volatile uint8_t xdata g_rw_gamvrr1_026Bh_GAM_VRR_LUT_OFST12_1_new_data_619;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 826Bh

extern volatile uint8_t xdata g_rw_gamvrr1_026Ch_GAM_VRR_LUT_OFST12_1_new_data_620;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 826Ch
extern volatile uint8_t xdata g_rw_gamvrr1_026Dh_GAM_VRR_LUT_OFST12_1_new_data_621;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 826Dh
extern volatile uint8_t xdata g_rw_gamvrr1_026Eh_GAM_VRR_LUT_OFST12_1_new_data_622;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 826Eh
extern volatile uint8_t xdata g_rw_gamvrr1_026Fh_GAM_VRR_LUT_OFST12_1_new_data_623;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 826Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0270h_GAM_VRR_LUT_OFST12_1_new_data_624;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8270h

extern volatile uint8_t xdata g_rw_gamvrr1_0271h_GAM_VRR_LUT_OFST12_1_new_data_625;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8271h
extern volatile uint8_t xdata g_rw_gamvrr1_0272h_GAM_VRR_LUT_OFST12_1_new_data_626;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8272h
extern volatile uint8_t xdata g_rw_gamvrr1_0273h_GAM_VRR_LUT_OFST12_1_new_data_627;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8273h
extern volatile uint8_t xdata g_rw_gamvrr1_0274h_GAM_VRR_LUT_OFST12_1_new_data_628;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8274h
extern volatile uint8_t xdata g_rw_gamvrr1_0275h_GAM_VRR_LUT_OFST12_1_new_data_629;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8275h

extern volatile uint8_t xdata g_rw_gamvrr1_0276h_GAM_VRR_LUT_OFST12_1_new_data_630;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8276h
extern volatile uint8_t xdata g_rw_gamvrr1_0277h_GAM_VRR_LUT_OFST12_1_new_data_631;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8277h
extern volatile uint8_t xdata g_rw_gamvrr1_0278h_GAM_VRR_LUT_OFST12_1_new_data_632;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8278h
extern volatile uint8_t xdata g_rw_gamvrr1_0279h_GAM_VRR_LUT_OFST12_1_new_data_633;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8279h
extern volatile uint8_t xdata g_rw_gamvrr1_027Ah_GAM_VRR_LUT_OFST12_1_new_data_634;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 827Ah

extern volatile uint8_t xdata g_rw_gamvrr1_027Bh_GAM_VRR_LUT_OFST12_1_new_data_635;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 827Bh
extern volatile uint8_t xdata g_rw_gamvrr1_027Ch_GAM_VRR_LUT_OFST12_1_new_data_636;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 827Ch
extern volatile uint8_t xdata g_rw_gamvrr1_027Dh_GAM_VRR_LUT_OFST12_1_new_data_637;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 827Dh
extern volatile uint8_t xdata g_rw_gamvrr1_027Eh_GAM_VRR_LUT_OFST12_1_new_data_638;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 827Eh
extern volatile uint8_t xdata g_rw_gamvrr1_027Fh_GAM_VRR_LUT_OFST12_1_new_data_639;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 827Fh

extern volatile uint8_t xdata g_rw_gamvrr1_0280h_GAM_VRR_LUT_OFST12_1_new_data_640;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8280h
extern volatile uint8_t xdata g_rw_gamvrr1_0281h_GAM_VRR_LUT_OFST12_1_new_data_641;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8281h
extern volatile uint8_t xdata g_rw_gamvrr1_0282h_GAM_VRR_LUT_OFST12_1_new_data_642;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8282h
extern volatile uint8_t xdata g_rw_gamvrr1_0283h_GAM_VRR_LUT_OFST12_1_new_data_643;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8283h
extern volatile uint8_t xdata g_rw_gamvrr1_0284h_GAM_VRR_LUT_OFST12_1_new_data_644;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8284h

extern volatile uint8_t xdata g_rw_gamvrr1_0285h_GAM_VRR_LUT_OFST12_1_new_data_645;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8285h
extern volatile uint8_t xdata g_rw_gamvrr1_0286h_GAM_VRR_LUT_OFST12_1_new_data_646;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8286h
extern volatile uint8_t xdata g_rw_gamvrr1_0287h_GAM_VRR_LUT_OFST12_1_new_data_647;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8287h
extern volatile uint8_t xdata g_rw_gamvrr1_0288h_GAM_VRR_LUT_OFST12_1_new_data_648;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8288h
extern volatile uint8_t xdata g_rw_gamvrr1_0289h_GAM_VRR_LUT_OFST12_1_new_data_649;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8289h

extern volatile uint8_t xdata g_rw_gamvrr1_028Ah_GAM_VRR_LUT_OFST12_1_new_data_650;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 828Ah
extern volatile uint8_t xdata g_rw_gamvrr1_028Bh_GAM_VRR_LUT_OFST12_1_new_data_651;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 828Bh
extern volatile uint8_t xdata g_rw_gamvrr1_028Ch_GAM_VRR_LUT_OFST12_1_new_data_652;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 828Ch
extern volatile uint8_t xdata g_rw_gamvrr1_028Dh_GAM_VRR_LUT_OFST12_1_new_data_653;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 828Dh
extern volatile uint8_t xdata g_rw_gamvrr1_028Eh_GAM_VRR_LUT_OFST12_1_new_data_654;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 828Eh

extern volatile uint8_t xdata g_rw_gamvrr1_028Fh_GAM_VRR_LUT_OFST12_1_new_data_655;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 828Fh
extern volatile uint8_t xdata g_rw_gamvrr1_0290h_GAM_VRR_LUT_OFST12_1_new_data_656;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8290h
extern volatile uint8_t xdata g_rw_gamvrr1_0291h_GAM_VRR_LUT_OFST12_1_new_data_657;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8291h
extern volatile uint8_t xdata g_rw_gamvrr1_0292h_GAM_VRR_LUT_OFST12_1_new_data_658;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8292h
extern volatile uint8_t xdata g_rw_gamvrr1_0293h_GAM_VRR_LUT_OFST12_1_new_data_659;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8293h

extern volatile uint8_t xdata g_rw_gamvrr1_0294h_GAM_VRR_LUT_OFST12_1_new_data_660;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8294h
extern volatile uint8_t xdata g_rw_gamvrr1_0295h_GAM_VRR_LUT_OFST12_1_new_data_661;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8295h
extern volatile uint8_t xdata g_rw_gamvrr1_0296h_GAM_VRR_LUT_OFST12_1_new_data_662;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8296h
extern volatile uint8_t xdata g_rw_gamvrr1_0297h_GAM_VRR_LUT_OFST12_1_new_data_663;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8297h
extern volatile uint8_t xdata g_rw_gamvrr1_0298h_GAM_VRR_LUT_OFST12_1_new_data_664;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8298h

extern volatile uint8_t xdata g_rw_gamvrr1_0299h_GAM_VRR_LUT_OFST12_1_new_data_665;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8299h
extern volatile uint8_t xdata g_rw_gamvrr1_029Ah_GAM_VRR_LUT_OFST12_1_new_data_666;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 829Ah
extern volatile uint8_t xdata g_rw_gamvrr1_029Bh_GAM_VRR_LUT_OFST12_1_new_data_667;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 829Bh
extern volatile uint8_t xdata g_rw_gamvrr1_029Ch_GAM_VRR_LUT_OFST12_1_new_data_668;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 829Ch
extern volatile uint8_t xdata g_rw_gamvrr1_029Dh_GAM_VRR_LUT_OFST12_1_new_data_669;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 829Dh

extern volatile uint8_t xdata g_rw_gamvrr1_029Eh_GAM_VRR_LUT_OFST12_1_new_data_670;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 829Eh
extern volatile uint8_t xdata g_rw_gamvrr1_029Fh_GAM_VRR_LUT_OFST12_1_new_data_671;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 829Fh
extern volatile uint8_t xdata g_rw_gamvrr1_02A0h_GAM_VRR_LUT_OFST12_1_new_data_672;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82A0h
extern volatile uint8_t xdata g_rw_gamvrr1_02A1h_GAM_VRR_LUT_OFST12_1_new_data_673;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82A1h
extern volatile uint8_t xdata g_rw_gamvrr1_02A2h_GAM_VRR_LUT_OFST12_1_new_data_674;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82A2h

extern volatile uint8_t xdata g_rw_gamvrr1_02A3h_GAM_VRR_LUT_OFST12_1_new_data_675;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82A3h
extern volatile uint8_t xdata g_rw_gamvrr1_02A4h_GAM_VRR_LUT_OFST12_1_new_data_676;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82A4h
extern volatile uint8_t xdata g_rw_gamvrr1_02A5h_GAM_VRR_LUT_OFST12_1_new_data_677;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82A5h
extern volatile uint8_t xdata g_rw_gamvrr1_02A6h_GAM_VRR_LUT_OFST12_1_new_data_678;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82A6h
extern volatile uint8_t xdata g_rw_gamvrr1_02A7h_GAM_VRR_LUT_OFST12_1_new_data_679;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82A7h

extern volatile uint8_t xdata g_rw_gamvrr1_02A8h_GAM_VRR_LUT_OFST12_1_new_data_680;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82A8h
extern volatile uint8_t xdata g_rw_gamvrr1_02A9h_GAM_VRR_LUT_OFST12_1_new_data_681;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82A9h
extern volatile uint8_t xdata g_rw_gamvrr1_02AAh_GAM_VRR_LUT_OFST12_1_new_data_682;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82AAh
extern volatile uint8_t xdata g_rw_gamvrr1_02ABh_GAM_VRR_LUT_OFST12_1_new_data_683;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82ABh
extern volatile uint8_t xdata g_rw_gamvrr1_02ACh_GAM_VRR_LUT_OFST12_1_new_data_684;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82ACh

extern volatile uint8_t xdata g_rw_gamvrr1_02ADh_GAM_VRR_LUT_OFST12_1_new_data_685;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82ADh
extern volatile uint8_t xdata g_rw_gamvrr1_02AEh_GAM_VRR_LUT_OFST12_1_new_data_686;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82AEh
extern volatile uint8_t xdata g_rw_gamvrr1_02AFh_GAM_VRR_LUT_OFST12_1_new_data_687;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82AFh
extern volatile uint8_t xdata g_rw_gamvrr1_02B0h_GAM_VRR_LUT_OFST12_1_new_data_688;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82B0h
extern volatile uint8_t xdata g_rw_gamvrr1_02B1h_GAM_VRR_LUT_OFST12_1_new_data_689;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82B1h

extern volatile uint8_t xdata g_rw_gamvrr1_02B2h_GAM_VRR_LUT_OFST12_1_new_data_690;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82B2h
extern volatile uint8_t xdata g_rw_gamvrr1_02B3h_GAM_VRR_LUT_OFST12_1_new_data_691;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82B3h
extern volatile uint8_t xdata g_rw_gamvrr1_02B4h_GAM_VRR_LUT_OFST12_1_new_data_692;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82B4h
extern volatile uint8_t xdata g_rw_gamvrr1_02B5h_GAM_VRR_LUT_OFST12_1_new_data_693;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82B5h
extern volatile uint8_t xdata g_rw_gamvrr1_02B6h_GAM_VRR_LUT_OFST12_1_new_data_694;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82B6h

extern volatile uint8_t xdata g_rw_gamvrr1_02B7h_GAM_VRR_LUT_OFST12_1_new_data_695;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82B7h
extern volatile uint8_t xdata g_rw_gamvrr1_02B8h_GAM_VRR_LUT_OFST12_1_new_data_696;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82B8h
extern volatile uint8_t xdata g_rw_gamvrr1_02B9h_GAM_VRR_LUT_OFST12_1_new_data_697;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82B9h
extern volatile uint8_t xdata g_rw_gamvrr1_02BAh_GAM_VRR_LUT_OFST12_1_new_data_698;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82BAh
extern volatile uint8_t xdata g_rw_gamvrr1_02BBh_GAM_VRR_LUT_OFST12_1_new_data_699;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82BBh

extern volatile uint8_t xdata g_rw_gamvrr1_02BCh_GAM_VRR_LUT_OFST12_1_new_data_700;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82BCh
extern volatile uint8_t xdata g_rw_gamvrr1_02BDh_GAM_VRR_LUT_OFST12_1_new_data_701;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82BDh
extern volatile uint8_t xdata g_rw_gamvrr1_02BEh_GAM_VRR_LUT_OFST12_1_new_data_702;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82BEh
extern volatile uint8_t xdata g_rw_gamvrr1_02BFh_GAM_VRR_LUT_OFST12_1_new_data_703;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82BFh
extern volatile uint8_t xdata g_rw_gamvrr1_02C0h_GAM_VRR_LUT_OFST12_1_new_data_704;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82C0h

extern volatile uint8_t xdata g_rw_gamvrr1_02C1h_GAM_VRR_LUT_OFST12_1_new_data_705;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82C1h
extern volatile uint8_t xdata g_rw_gamvrr1_02C2h_GAM_VRR_LUT_OFST12_1_new_data_706;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82C2h
extern volatile uint8_t xdata g_rw_gamvrr1_02C3h_GAM_VRR_LUT_OFST12_1_new_data_707;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82C3h
extern volatile uint8_t xdata g_rw_gamvrr1_02C4h_GAM_VRR_LUT_OFST12_1_new_data_708;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82C4h
extern volatile uint8_t xdata g_rw_gamvrr1_02C5h_GAM_VRR_LUT_OFST12_1_new_data_709;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82C5h

extern volatile uint8_t xdata g_rw_gamvrr1_02C6h_GAM_VRR_LUT_OFST12_1_new_data_710;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82C6h
extern volatile uint8_t xdata g_rw_gamvrr1_02C7h_GAM_VRR_LUT_OFST12_1_new_data_711;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82C7h
extern volatile uint8_t xdata g_rw_gamvrr1_02C8h_GAM_VRR_LUT_OFST12_1_new_data_712;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82C8h
extern volatile uint8_t xdata g_rw_gamvrr1_02C9h_GAM_VRR_LUT_OFST12_1_new_data_713;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82C9h
extern volatile uint8_t xdata g_rw_gamvrr1_02CAh_GAM_VRR_LUT_OFST12_1_new_data_714;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82CAh

extern volatile uint8_t xdata g_rw_gamvrr1_02CBh_GAM_VRR_LUT_OFST12_1_new_data_715;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82CBh
extern volatile uint8_t xdata g_rw_gamvrr1_02CCh_GAM_VRR_LUT_OFST12_1_new_data_716;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82CCh
extern volatile uint8_t xdata g_rw_gamvrr1_02CDh_GAM_VRR_LUT_OFST12_1_new_data_717;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82CDh
extern volatile uint8_t xdata g_rw_gamvrr1_02CEh_GAM_VRR_LUT_OFST12_1_new_data_718;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82CEh
extern volatile uint8_t xdata g_rw_gamvrr1_02CFh_GAM_VRR_LUT_OFST12_1_new_data_719;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82CFh

extern volatile uint8_t xdata g_rw_gamvrr1_02D0h_GAM_VRR_LUT_OFST12_1_new_data_720;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82D0h
extern volatile uint8_t xdata g_rw_gamvrr1_02D1h_GAM_VRR_LUT_OFST12_1_new_data_721;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82D1h
extern volatile uint8_t xdata g_rw_gamvrr1_02D2h_GAM_VRR_LUT_OFST12_1_new_data_722;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82D2h
extern volatile uint8_t xdata g_rw_gamvrr1_02D3h_GAM_VRR_LUT_OFST12_1_new_data_723;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82D3h
extern volatile uint8_t xdata g_rw_gamvrr1_02D4h_GAM_VRR_LUT_OFST12_1_new_data_724;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82D4h

extern volatile uint8_t xdata g_rw_gamvrr1_02D5h_GAM_VRR_LUT_OFST12_1_new_data_725;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82D5h
extern volatile uint8_t xdata g_rw_gamvrr1_02D6h_GAM_VRR_LUT_OFST12_1_new_data_726;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82D6h
extern volatile uint8_t xdata g_rw_gamvrr1_02D7h_GAM_VRR_LUT_OFST12_1_new_data_727;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82D7h
extern volatile uint8_t xdata g_rw_gamvrr1_02D8h_GAM_VRR_LUT_OFST12_1_new_data_728;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82D8h
extern volatile uint8_t xdata g_rw_gamvrr1_02D9h_GAM_VRR_LUT_OFST12_1_new_data_729;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82D9h

extern volatile uint8_t xdata g_rw_gamvrr1_02DAh_GAM_VRR_LUT_OFST12_1_new_data_730;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82DAh
extern volatile uint8_t xdata g_rw_gamvrr1_02DBh_GAM_VRR_LUT_OFST12_1_new_data_731;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82DBh
extern volatile uint8_t xdata g_rw_gamvrr1_02DCh_GAM_VRR_LUT_OFST12_1_new_data_732;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82DCh
extern volatile uint8_t xdata g_rw_gamvrr1_02DDh_GAM_VRR_LUT_OFST12_1_new_data_733;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82DDh
extern volatile uint8_t xdata g_rw_gamvrr1_02DEh_GAM_VRR_LUT_OFST12_1_new_data_734;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82DEh

extern volatile uint8_t xdata g_rw_gamvrr1_02DFh_GAM_VRR_LUT_OFST12_1_new_data_735;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82DFh
extern volatile uint8_t xdata g_rw_gamvrr1_02E0h_GAM_VRR_LUT_OFST12_1_new_data_736;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82E0h
extern volatile uint8_t xdata g_rw_gamvrr1_02E1h_GAM_VRR_LUT_OFST12_1_new_data_737;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82E1h
extern volatile uint8_t xdata g_rw_gamvrr1_02E2h_GAM_VRR_LUT_OFST12_1_new_data_738;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82E2h
extern volatile uint8_t xdata g_rw_gamvrr1_02E3h_GAM_VRR_LUT_OFST12_1_new_data_739;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82E3h

extern volatile uint8_t xdata g_rw_gamvrr1_02E4h_GAM_VRR_LUT_OFST12_1_new_data_740;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82E4h
extern volatile uint8_t xdata g_rw_gamvrr1_02E5h_GAM_VRR_LUT_OFST12_1_new_data_741;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82E5h
extern volatile uint8_t xdata g_rw_gamvrr1_02E6h_GAM_VRR_LUT_OFST12_1_new_data_742;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82E6h
extern volatile uint8_t xdata g_rw_gamvrr1_02E7h_GAM_VRR_LUT_OFST12_1_new_data_743;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82E7h
extern volatile uint8_t xdata g_rw_gamvrr1_02E8h_GAM_VRR_LUT_OFST12_1_new_data_744;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82E8h

extern volatile uint8_t xdata g_rw_gamvrr1_02E9h_GAM_VRR_LUT_OFST12_1_new_data_745;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82E9h
extern volatile uint8_t xdata g_rw_gamvrr1_02EAh_GAM_VRR_LUT_OFST12_1_new_data_746;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82EAh
extern volatile uint8_t xdata g_rw_gamvrr1_02EBh_GAM_VRR_LUT_OFST12_1_new_data_747;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82EBh
extern volatile uint8_t xdata g_rw_gamvrr1_02ECh_GAM_VRR_LUT_OFST12_1_new_data_748;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82ECh
extern volatile uint8_t xdata g_rw_gamvrr1_02EDh_GAM_VRR_LUT_OFST12_1_new_data_749;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82EDh

extern volatile uint8_t xdata g_rw_gamvrr1_02EEh_GAM_VRR_LUT_OFST12_1_new_data_750;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82EEh
extern volatile uint8_t xdata g_rw_gamvrr1_02EFh_GAM_VRR_LUT_OFST12_1_new_data_751;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82EFh
extern volatile uint8_t xdata g_rw_gamvrr1_02F0h_GAM_VRR_LUT_OFST12_1_new_data_752;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82F0h
extern volatile uint8_t xdata g_rw_gamvrr1_02F1h_GAM_VRR_LUT_OFST12_1_new_data_753;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82F1h
extern volatile uint8_t xdata g_rw_gamvrr1_02F2h_GAM_VRR_LUT_OFST12_1_new_data_754;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82F2h

extern volatile uint8_t xdata g_rw_gamvrr1_02F3h_GAM_VRR_LUT_OFST12_1_new_data_755;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82F3h
extern volatile uint8_t xdata g_rw_gamvrr1_02F4h_GAM_VRR_LUT_OFST12_1_new_data_756;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82F4h
extern volatile uint8_t xdata g_rw_gamvrr1_02F5h_GAM_VRR_LUT_OFST12_1_new_data_757;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82F5h
extern volatile uint8_t xdata g_rw_gamvrr1_02F6h_GAM_VRR_LUT_OFST12_1_new_data_758;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82F6h
extern volatile uint8_t xdata g_rw_gamvrr1_02F7h_GAM_VRR_LUT_OFST12_1_new_data_759;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82F7h

extern volatile uint8_t xdata g_rw_gamvrr1_02F8h_GAM_VRR_LUT_OFST12_1_new_data_760;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82F8h
extern volatile uint8_t xdata g_rw_gamvrr1_02F9h_GAM_VRR_LUT_OFST12_1_new_data_761;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82F9h
extern volatile uint8_t xdata g_rw_gamvrr1_02FAh_GAM_VRR_LUT_OFST12_1_new_data_762;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82FAh
extern volatile uint8_t xdata g_rw_gamvrr1_02FBh_GAM_VRR_LUT_OFST12_1_new_data_763;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82FBh
extern volatile uint8_t xdata g_rw_gamvrr1_02FCh_GAM_VRR_LUT_OFST12_1_new_data_764;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82FCh

extern volatile uint8_t xdata g_rw_gamvrr1_02FDh_GAM_VRR_LUT_OFST12_1_new_data_765;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82FDh
extern volatile uint8_t xdata g_rw_gamvrr1_02FEh_GAM_VRR_LUT_OFST12_1_new_data_766;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82FEh
extern volatile uint8_t xdata g_rw_gamvrr1_02FFh_GAM_VRR_LUT_OFST12_1_new_data_767;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 82FFh
extern volatile uint8_t xdata g_rw_gamvrr1_0300h_GAM_VRR_LUT_OFST12_1_new_data_768;                      // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 8300h
extern volatile uint8_t xdata g_rw_gamvrr1_0301h_GAM_VRR_LUT_OFST12_1_new_data_769;                      // [msb:lsb] = [7:0], val = 32, 	Absolute Address = 8301h

extern volatile uint8_t xdata g_rw_gamvrr1_0302h_GAM_VRR_LUT_OFST12_1_new_data_770;                      // [msb:lsb] = [7:0], val = 64, 	Absolute Address = 8302h
extern volatile uint8_t xdata g_rw_gamvrr1_0303h_GAM_VRR_LUT_OFST12_1_new_data_771;                      // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 8303h
extern volatile uint8_t xdata g_rw_gamvrr1_0304h_GAM_VRR_LUT_OFST12_1_new_data_772;                      // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 8304h
extern volatile uint8_t xdata g_rw_gamvrr1_0305h_GAM_VRR_LUT_OFST12_1_new_data_773;                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 8305h

#endif