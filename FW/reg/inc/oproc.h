#ifndef _OPROC_H_
#define _OPROC_H_
#include "reg_include.h"

union rw_oproc_0000h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tri_gate_en                                                          : 1;        // val = 0
        unsigned char r_hsd_en                                                               : 1;        // val = 0
        unsigned char r_fix_ctrl                                                             : 1;        // val = 0
        unsigned char r_de_ext1_en                                                           : 1;        // val = 1
        unsigned char r_de_ext2_en                                                           : 1;        // val = 0
        unsigned char r_de_ext3_en                                                           : 1;        // val = 0
        unsigned char r_wr_swap_en                                                           : 1;        // val = 0
        unsigned char r_g1_dmy_sft_bits_0                                                    : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_0000h xdata g_rw_oproc_0000h;    // Absolute Address = 5800h

union rw_oproc_0001h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_dmy_sft_bits_2_1                                                  : 2;        // [msb:lsb] = [2:1], val = 0
        unsigned char r_g2_dmy_sft                                                           : 3;        // [msb:lsb] = [2:0], val = 1
        unsigned char r_g3_dmy_sft                                                           : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0001h xdata g_rw_oproc_0001h;    // Absolute Address = 5801h

union rw_oproc_0002h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g4_dmy_sft                                                           : 3;        // [msb:lsb] = [2:0], val = 1
        unsigned char r_g5_dmy_sft                                                           : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_g6_dmy_sft_bits_1_0                                                  : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_oproc_0002h xdata g_rw_oproc_0002h;    // Absolute Address = 5802h

union rw_oproc_0003h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g6_dmy_sft_bits_2                                                    : 1;        // val = 0
        unsigned char r_g7_dmy_sft                                                           : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_g8_dmy_sft                                                           : 3;        // [msb:lsb] = [2:0], val = 1
        unsigned char r_g9_dmy_sft_bits_0                                                    : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_0003h xdata g_rw_oproc_0003h;    // Absolute Address = 5803h

union rw_oproc_0004h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g9_dmy_sft_bits_2_1                                                  : 2;        // [msb:lsb] = [2:1], val = 0
        unsigned char r_g10_dmy_sft                                                          : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_g11_dmy_sft                                                          : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0004h xdata g_rw_oproc_0004h;    // Absolute Address = 5804h

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_0005h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g12_dmy_sft                                                          : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_rev_f                                                                : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_init_rev_val                                                         : 1;        // val = 0
        unsigned char r_wr_rev_en                                                            : 1;        // val = 0
        unsigned char r_rd_rev_en                                                            : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_0005h xdata g_rw_oproc_0005h;    // Absolute Address = 5805h

union rw_oproc_0006h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pre_line_typ                                                         : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_preline_no                                                           : 4;        // [msb:lsb] = [3:0], val = 2
        unsigned char r_dmy_gray_sel                                                         : 1;        // val = 0
        unsigned char r_lbuf_rd_hdisp_bits_0                                                 : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_0006h xdata g_rw_oproc_0006h;    // Absolute Address = 5806h

union rw_oproc_0008h
{
    unsigned char byte;
    struct
    {
        unsigned char r_lbuf_rd_hdisp_bits_11_9                                              : 3;        // [msb:lsb] = [11:9], val = 5
        unsigned char r_line_map_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_line_map_prog_en                                                     : 1;        // val = 0
        unsigned char r_line_map_prog_bits_0                                                 : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_0008h xdata g_rw_oproc_0008h;    // Absolute Address = 5808h

union rw_oproc_0009h
{
    unsigned char byte;
    struct
    {
        unsigned char r_line_map_prog_bits_7_1                                               : 7;        // [msb:lsb] = [7:1], val = 0
        unsigned char r_dmy_gray_bits_0                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_0009h xdata g_rw_oproc_0009h;    // Absolute Address = 5809h

union rw_oproc_000Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_dmy_gray_bits_7_1                                                    : 7;        // [msb:lsb] = [7:1], val = 0
        unsigned char r_rbcolor_swap_a                                                       : 1;        // val = 1
    }bits;
};
extern volatile union rw_oproc_000Ah xdata g_rw_oproc_000Ah;    // Absolute Address = 580Ah

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_000Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_rbcolor_swap_b                                                       : 1;        // val = 1
        unsigned char r_odd_even_swap_a                                                      : 1;        // val = 0
        unsigned char r_odd_even_swap_b                                                      : 1;        // val = 0
        unsigned char r_rgcolor_swap_a                                                       : 1;        // val = 0
        unsigned char r_rgcolor_swap_b                                                       : 1;        // val = 0
        unsigned char r_inc_dec_a                                                            : 1;        // val = 0
        unsigned char r_inc_dec_b                                                            : 1;        // val = 0
        unsigned char r_a_start_pix_bits_0                                                   : 1;        // val = 1
    }bits;
};
extern volatile union rw_oproc_000Bh xdata g_rw_oproc_000Bh;    // Absolute Address = 580Bh

union rw_oproc_000Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_a_start_pix_bits_11_9                                                : 3;        // [msb:lsb] = [11:9], val = 7
        unsigned char r_b_start_pix_bits_4_0                                                 : 5;        // [msb:lsb] = [4:0], val = 1
    }bits;
};
extern volatile union rw_oproc_000Dh xdata g_rw_oproc_000Dh;    // Absolute Address = 580Dh

union rw_oproc_000Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_b_start_pix_bits_11_5                                                : 7;        // [msb:lsb] = [11:5], val = 40
        unsigned char r_lbuf_addr_max_bits_0                                                 : 1;        // val = 1
    }bits;
};
extern volatile union rw_oproc_000Eh xdata g_rw_oproc_000Eh;    // Absolute Address = 580Eh

union rw_oproc_0010h
{
    unsigned char byte;
    struct
    {
        unsigned char r_lbuf_addr_max_bits_11_9                                              : 3;        // [msb:lsb] = [11:9], val = 5
        unsigned char r_obit_sel                                                             : 2;        // [msb:lsb] = [1:0], val = 1
        unsigned char r_mask_black_en                                                        : 1;        // val = 0
        unsigned char r_tx_port_bits_1_0                                                     : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_oproc_0010h xdata g_rw_oproc_0010h;    // Absolute Address = 5810h

union rw_oproc_0011h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_port_bits_3_2                                                     : 2;        // [msb:lsb] = [3:2], val = 0
        unsigned char r_tx_pair                                                              : 1;        // val = 0
        unsigned char r_vblk_gray_bits_4_0                                                   : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0011h xdata g_rw_oproc_0011h;    // Absolute Address = 5811h

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_0012h
{
    unsigned char byte;
    struct
    {
        unsigned char r_vblk_gray_bits_9_5                                                   : 5;        // [msb:lsb] = [9:5], val = 0
        unsigned char r_rd_line_byrev_en                                                     : 1;        // val = 0
        unsigned char r_eco_xdio_sel                                                         : 1;        // val = 0
        unsigned char r_dmy_gray_ne_en                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_0012h xdata g_rw_oproc_0012h;    // Absolute Address = 5812h

union rw_oproc_0013h
{
    unsigned char byte;
    struct
    {
        unsigned char r_def_gate_en                                                          : 1;        // val = 0
        unsigned char r_def_gate_val                                                         : 3;        // [msb:lsb] = [2:0], val = 5
        unsigned char r_htot_rec_sel                                                         : 1;        // val = 0
        unsigned char r_chrg_zig                                                             : 1;        // val = 0
        unsigned char r_chrb_zig                                                             : 1;        // val = 0
        unsigned char r_chwb                                                                 : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_0013h xdata g_rw_oproc_0013h;    // Absolute Address = 5813h

union rw_oproc_0014h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map0_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 6
        unsigned char r_g1_map0_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 7
    }bits;
};
extern volatile union rw_oproc_0014h xdata g_rw_oproc_0014h;    // Absolute Address = 5814h

union rw_oproc_0015h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map0_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 8
        unsigned char r_r1_map1_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 6
    }bits;
};
extern volatile union rw_oproc_0015h xdata g_rw_oproc_0015h;    // Absolute Address = 5815h

union rw_oproc_0016h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map1_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 7
        unsigned char r_b1_map1_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_oproc_0016h xdata g_rw_oproc_0016h;    // Absolute Address = 5816h

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_0017h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map2_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 6
        unsigned char r_g1_map2_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 7
    }bits;
};
extern volatile union rw_oproc_0017h xdata g_rw_oproc_0017h;    // Absolute Address = 5817h

union rw_oproc_0018h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map2_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 8
        unsigned char r_r1_map3_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 6
    }bits;
};
extern volatile union rw_oproc_0018h xdata g_rw_oproc_0018h;    // Absolute Address = 5818h

union rw_oproc_0019h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map3_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 7
        unsigned char r_b1_map3_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_oproc_0019h xdata g_rw_oproc_0019h;    // Absolute Address = 5819h

union rw_oproc_001Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map4_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 6
        unsigned char r_g1_map4_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 7
    }bits;
};
extern volatile union rw_oproc_001Ah xdata g_rw_oproc_001Ah;    // Absolute Address = 581Ah

union rw_oproc_001Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map4_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 8
        unsigned char r_r1_map5_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 6
    }bits;
};
extern volatile union rw_oproc_001Bh xdata g_rw_oproc_001Bh;    // Absolute Address = 581Bh

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_001Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map5_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 7
        unsigned char r_b1_map5_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_oproc_001Ch xdata g_rw_oproc_001Ch;    // Absolute Address = 581Ch

union rw_oproc_001Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map6_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 6
        unsigned char r_g1_map6_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 7
    }bits;
};
extern volatile union rw_oproc_001Dh xdata g_rw_oproc_001Dh;    // Absolute Address = 581Dh

union rw_oproc_001Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map6_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 8
        unsigned char r_r1_map7_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 6
    }bits;
};
extern volatile union rw_oproc_001Eh xdata g_rw_oproc_001Eh;    // Absolute Address = 581Eh

union rw_oproc_001Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map7_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 7
        unsigned char r_b1_map7_pos_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_oproc_001Fh xdata g_rw_oproc_001Fh;    // Absolute Address = 581Fh

union rw_oproc_0020h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map0_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 6
        unsigned char r_g1_map0_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 7
    }bits;
};
extern volatile union rw_oproc_0020h xdata g_rw_oproc_0020h;    // Absolute Address = 5820h

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_0021h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map0_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 8
        unsigned char r_r1_map1_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 6
    }bits;
};
extern volatile union rw_oproc_0021h xdata g_rw_oproc_0021h;    // Absolute Address = 5821h

union rw_oproc_0022h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map1_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 7
        unsigned char r_b1_map1_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_oproc_0022h xdata g_rw_oproc_0022h;    // Absolute Address = 5822h

union rw_oproc_0023h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map2_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 6
        unsigned char r_g1_map2_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 7
    }bits;
};
extern volatile union rw_oproc_0023h xdata g_rw_oproc_0023h;    // Absolute Address = 5823h

union rw_oproc_0024h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map2_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 8
        unsigned char r_r1_map3_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 6
    }bits;
};
extern volatile union rw_oproc_0024h xdata g_rw_oproc_0024h;    // Absolute Address = 5824h

union rw_oproc_0025h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map3_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 7
        unsigned char r_b1_map3_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_oproc_0025h xdata g_rw_oproc_0025h;    // Absolute Address = 5825h

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_0026h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map4_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 6
        unsigned char r_g1_map4_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 7
    }bits;
};
extern volatile union rw_oproc_0026h xdata g_rw_oproc_0026h;    // Absolute Address = 5826h

union rw_oproc_0027h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map4_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 8
        unsigned char r_r1_map5_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 6
    }bits;
};
extern volatile union rw_oproc_0027h xdata g_rw_oproc_0027h;    // Absolute Address = 5827h

union rw_oproc_0028h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map5_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 7
        unsigned char r_b1_map5_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_oproc_0028h xdata g_rw_oproc_0028h;    // Absolute Address = 5828h

union rw_oproc_0029h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map6_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 6
        unsigned char r_g1_map6_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 7
    }bits;
};
extern volatile union rw_oproc_0029h xdata g_rw_oproc_0029h;    // Absolute Address = 5829h

union rw_oproc_002Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map6_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 8
        unsigned char r_r1_map7_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 6
    }bits;
};
extern volatile union rw_oproc_002Ah xdata g_rw_oproc_002Ah;    // Absolute Address = 582Ah

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_002Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map7_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 7
        unsigned char r_b1_map7_neg_ns                                                       : 4;        // [msb:lsb] = [3:0], val = 8
    }bits;
};
extern volatile union rw_oproc_002Bh xdata g_rw_oproc_002Bh;    // Absolute Address = 582Bh

union rw_oproc_002Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_pol_int_sel                                                          : 1;        // val = 0
        unsigned char r_pol1_frm_inv                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol1_pol_mode                                                        : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol1_m_mode_bits_0                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_002Ch xdata g_rw_oproc_002Ch;    // Absolute Address = 582Ch

union rw_oproc_002Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_pol1_m_mode_bits_2_1                                                 : 2;        // [msb:lsb] = [2:1], val = 0
        unsigned char r_frame_inv_num_bits_5_0                                               : 6;        // [msb:lsb] = [5:0], val = 16
    }bits;
};
extern volatile union rw_oproc_002Dh xdata g_rw_oproc_002Dh;    // Absolute Address = 582Dh

union rw_oproc_002Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_frame_inv_num_bits_12_6                                              : 7;        // [msb:lsb] = [12:6], val = 26
        unsigned char r_pol_col_sel_bits_0                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_002Eh xdata g_rw_oproc_002Eh;    // Absolute Address = 582Eh

union rw_oproc_002Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_pol_col_sel_bits_1                                                   : 1;        // val = 0
        unsigned char r_pol1_init_pol_val                                                    : 1;        // val = 0
        unsigned char r_long_pol_en                                                          : 1;        // val = 0
        unsigned char r_pol_28s_en                                                           : 1;        // val = 0
        unsigned char r_htot_vair_bits_3_0                                                   : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_oproc_002Fh xdata g_rw_oproc_002Fh;    // Absolute Address = 582Fh

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_0030h
{
    unsigned char byte;
    struct
    {
        unsigned char r_htot_vair_bits_7_4                                                   : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char r_dmy_sft_en_sel                                                       : 1;        // val = 0
        unsigned char r_rd_xdio_sel                                                          : 1;        // val = 1
        unsigned char r_htot_rec_bits_1_0                                                    : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0030h xdata g_rw_oproc_0030h;    // Absolute Address = 5830h

union rw_oproc_0032h
{
    unsigned char byte;
    struct
    {
        unsigned char r_htot_rec_bits_13_10                                                  : 4;        // [msb:lsb] = [13:10], val = 0
        unsigned char r_sq_inv_sel                                                           : 1;        // val = 0
        unsigned char r_pol1_dyn_en                                                          : 1;        // val = 0
        unsigned char r_pre_dmy1_gray_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0032h xdata g_rw_oproc_0032h;    // Absolute Address = 5832h

union rw_oproc_0033h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pre_dmy1_gray_bits_7_2                                               : 6;        // [msb:lsb] = [7:2], val = 0
        unsigned char r_pre_dmy2_gray_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0033h xdata g_rw_oproc_0033h;    // Absolute Address = 5833h

union rw_oproc_0034h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pre_dmy2_gray_bits_7_2                                               : 6;        // [msb:lsb] = [7:2], val = 0
        unsigned char r_pre_dmy3_gray_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0034h xdata g_rw_oproc_0034h;    // Absolute Address = 5834h

union rw_oproc_0035h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pre_dmy3_gray_bits_7_2                                               : 6;        // [msb:lsb] = [7:2], val = 0
        unsigned char r_pre_dmy4_gray_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0035h xdata g_rw_oproc_0035h;    // Absolute Address = 5835h

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_0036h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pre_dmy4_gray_bits_7_2                                               : 6;        // [msb:lsb] = [7:2], val = 0
        unsigned char r_pre_dmy1_gray_no                                                     : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0036h xdata g_rw_oproc_0036h;    // Absolute Address = 5836h

union rw_oproc_0037h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pre_dmy2_gray_no                                                     : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_pre_dmy3_gray_no                                                     : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_pre_dmy4_gray_no                                                     : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_pre_dmy5_gray_no                                                     : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0037h xdata g_rw_oproc_0037h;    // Absolute Address = 5837h

union rw_oproc_0038h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pre_dmy6_gray_no                                                     : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_pre_dmy7_gray_no                                                     : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_pol2_frm_inv                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_pol2_pol_mode_bits_0                                                 : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_0038h xdata g_rw_oproc_0038h;    // Absolute Address = 5838h

union rw_oproc_0039h
{
    unsigned char byte;
    struct
    {
        unsigned char r_pol2_pol_mode_bits_2_1                                               : 2;        // [msb:lsb] = [2:1], val = 0
        unsigned char r_pol2_m_mode                                                          : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_frame_inv_num2_bits_2_0                                              : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0039h xdata g_rw_oproc_0039h;    // Absolute Address = 5839h

union rw_oproc_003Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_frame_inv_num2_bits_12_11                                            : 2;        // [msb:lsb] = [12:11], val = 0
        unsigned char r_pol2_init_pol_val                                                    : 1;        // val = 0
        unsigned char r_pol_28s_sel                                                          : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_pol2_dyn_en                                                          : 1;        // val = 0
        unsigned char r_htot_cmp_reg_min_bits_1_0                                            : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_003Bh xdata g_rw_oproc_003Bh;    // Absolute Address = 583Bh

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_003Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_htot_cmp_reg_min_bits_13_10                                          : 4;        // [msb:lsb] = [13:10], val = 0
        unsigned char r_div_htot_rec_sel                                                     : 1;        // val = 0
        unsigned char r_rev_frm_sel                                                          : 1;        // val = 0
        unsigned char r_prepol_sel                                                           : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_003Dh xdata g_rw_oproc_003Dh;    // Absolute Address = 583Dh

union rw_oproc_003Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_hsd_new_type_en                                                      : 1;        // val = 0
        unsigned char r_abnor_clr_off                                                        : 1;        // val = 1
        unsigned char r_bk_pol                                                               : 1;        // val = 0
        unsigned char r_dym_sft_num_bits_4_0                                                 : 5;        // [msb:lsb] = [4:0], val = 30
    }bits;
};
extern volatile union rw_oproc_003Eh xdata g_rw_oproc_003Eh;    // Absolute Address = 583Eh

union rw_oproc_0040h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rbcolor_swap_c                                                       : 1;        // val = 0
        unsigned char r_rbcolor_swap_d                                                       : 1;        // val = 0
        unsigned char r_rbcolor_swap_e                                                       : 1;        // val = 0
        unsigned char r_rbcolor_swap_f                                                       : 1;        // val = 0
        unsigned char r_rbcolor_swap_g                                                       : 1;        // val = 0
        unsigned char r_rbcolor_swap_h                                                       : 1;        // val = 0
        unsigned char r_odd_even_swap_c                                                      : 1;        // val = 0
        unsigned char r_odd_even_swap_d                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_0040h xdata g_rw_oproc_0040h;    // Absolute Address = 5840h

union rw_oproc_0041h
{
    unsigned char byte;
    struct
    {
        unsigned char r_odd_even_swap_e                                                      : 1;        // val = 0
        unsigned char r_odd_even_swap_f                                                      : 1;        // val = 0
        unsigned char r_odd_even_swap_g                                                      : 1;        // val = 0
        unsigned char r_odd_even_swap_h                                                      : 1;        // val = 0
        unsigned char r_rgcolor_swap_c                                                       : 1;        // val = 0
        unsigned char r_rgcolor_swap_d                                                       : 1;        // val = 0
        unsigned char r_rgcolor_swap_e                                                       : 1;        // val = 0
        unsigned char r_rgcolor_swap_f                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_0041h xdata g_rw_oproc_0041h;    // Absolute Address = 5841h

union rw_oproc_0042h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rgcolor_swap_g                                                       : 1;        // val = 0
        unsigned char r_rgcolor_swap_h                                                       : 1;        // val = 0
        unsigned char r_inc_dec_c                                                            : 1;        // val = 0
        unsigned char r_inc_dec_d                                                            : 1;        // val = 0
        unsigned char r_inc_dec_e                                                            : 1;        // val = 0
        unsigned char r_inc_dec_f                                                            : 1;        // val = 0
        unsigned char r_inc_dec_g                                                            : 1;        // val = 0
        unsigned char r_inc_dec_h                                                            : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_0042h xdata g_rw_oproc_0042h;    // Absolute Address = 5842h

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_0044h
{
    unsigned char byte;
    struct
    {
        unsigned char r_c_start_pix_bits_11_8                                                : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_d_start_pix_bits_3_0                                                 : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0044h xdata g_rw_oproc_0044h;    // Absolute Address = 5844h

union rw_oproc_0047h
{
    unsigned char byte;
    struct
    {
        unsigned char r_e_start_pix_bits_11_8                                                : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_f_start_pix_bits_3_0                                                 : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0047h xdata g_rw_oproc_0047h;    // Absolute Address = 5847h

union rw_oproc_004Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_g_start_pix_bits_11_8                                                : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_h_start_pix_bits_3_0                                                 : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_004Ah xdata g_rw_oproc_004Ah;    // Absolute Address = 584Ah

union rw_oproc_004Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_pdi_6b                                                               : 1;        // val = 0
        unsigned char r_port_len_bits_6_0                                                    : 7;        // [msb:lsb] = [6:0], val = 95
    }bits;
};
extern volatile union rw_oproc_004Ch xdata g_rw_oproc_004Ch;    // Absolute Address = 584Ch

union rw_oproc_004Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_port_len_bits_11_7                                                   : 5;        // [msb:lsb] = [11:7], val = 3
        unsigned char r_6b_odd_even_swap_a                                                   : 1;        // val = 0
        unsigned char r_6b_odd_even_swap_b                                                   : 1;        // val = 0
        unsigned char r_6b_odd_even_swap_c                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_004Dh xdata g_rw_oproc_004Dh;    // Absolute Address = 584Dh

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_004Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_6b_odd_even_swap_d                                                   : 1;        // val = 0
        unsigned char r_6b_odd_even_swap_e                                                   : 1;        // val = 0
        unsigned char r_6b_odd_even_swap_f                                                   : 1;        // val = 0
        unsigned char r_6b_odd_even_swap_g                                                   : 1;        // val = 0
        unsigned char r_6b_odd_even_swap_h                                                   : 1;        // val = 0
        unsigned char r_6b_inc_dec_a                                                         : 1;        // val = 0
        unsigned char r_6b_inc_dec_b                                                         : 1;        // val = 0
        unsigned char r_6b_inc_dec_c                                                         : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_004Eh xdata g_rw_oproc_004Eh;    // Absolute Address = 584Eh

union rw_oproc_004Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_6b_inc_dec_d                                                         : 1;        // val = 0
        unsigned char r_6b_inc_dec_e                                                         : 1;        // val = 0
        unsigned char r_6b_inc_dec_f                                                         : 1;        // val = 0
        unsigned char r_6b_inc_dec_g                                                         : 1;        // val = 0
        unsigned char r_6b_inc_dec_h                                                         : 1;        // val = 0
        unsigned char r_lxl_test_en                                                          : 1;        // val = 0
        unsigned char r_lxl_test_dt_sel                                                      : 1;        // val = 0
        unsigned char r_vline_sel_0                                                          : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_004Fh xdata g_rw_oproc_004Fh;    // Absolute Address = 584Fh

union rw_oproc_0050h
{
    unsigned char byte;
    struct
    {
        unsigned char r_vline_sel_1                                                          : 1;        // val = 0
        unsigned char r_vline_sel_2                                                          : 1;        // val = 0
        unsigned char r_vline_sel_3                                                          : 1;        // val = 0
        unsigned char r_measure_dt_0_bits_4_0                                                : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0050h xdata g_rw_oproc_0050h;    // Absolute Address = 5850h

union rw_oproc_0051h
{
    unsigned char byte;
    struct
    {
        unsigned char r_measure_dt_0_bits_9_5                                                : 5;        // [msb:lsb] = [9:5], val = 0
        unsigned char r_measure_dt_1_bits_2_0                                                : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0051h xdata g_rw_oproc_0051h;    // Absolute Address = 5851h

union rw_oproc_0052h
{
    unsigned char byte;
    struct
    {
        unsigned char r_measure_dt_1_bits_9_3                                                : 7;        // [msb:lsb] = [9:3], val = 0
        unsigned char r_measure_dt_2_bits_0                                                  : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_0052h xdata g_rw_oproc_0052h;    // Absolute Address = 5852h

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_0054h
{
    unsigned char byte;
    struct
    {
        unsigned char r_measure_dt_2_bits_9                                                  : 1;        // val = 0
        unsigned char r_measure_dt_3_bits_6_0                                                : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0054h xdata g_rw_oproc_0054h;    // Absolute Address = 5854h

union rw_oproc_0055h
{
    unsigned char byte;
    struct
    {
        unsigned char r_measure_dt_3_bits_9_7                                                : 3;        // [msb:lsb] = [9:7], val = 0
        unsigned char r_measure_dt_4_bits_4_0                                                : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0055h xdata g_rw_oproc_0055h;    // Absolute Address = 5855h

union rw_oproc_0056h
{
    unsigned char byte;
    struct
    {
        unsigned char r_measure_dt_4_bits_9_5                                                : 5;        // [msb:lsb] = [9:5], val = 0
        unsigned char r_measure_dt_5_bits_2_0                                                : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0056h xdata g_rw_oproc_0056h;    // Absolute Address = 5856h

union rw_oproc_0057h
{
    unsigned char byte;
    struct
    {
        unsigned char r_measure_dt_5_bits_9_3                                                : 7;        // [msb:lsb] = [9:3], val = 0
        unsigned char r_measure_dt_6_bits_0                                                  : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_0057h xdata g_rw_oproc_0057h;    // Absolute Address = 5857h

union rw_oproc_0059h
{
    unsigned char byte;
    struct
    {
        unsigned char r_measure_dt_6_bits_9                                                  : 1;        // val = 0
        unsigned char r_measure_dt_7_bits_6_0                                                : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0059h xdata g_rw_oproc_0059h;    // Absolute Address = 5859h

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_005Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_measure_dt_7_bits_9_7                                                : 3;        // [msb:lsb] = [9:7], val = 0
        unsigned char r_ch_debug_en                                                          : 1;        // val = 0
        unsigned char r_test_data_r_bits_3_0                                                 : 4;        // [msb:lsb] = [3:0], val = 4
    }bits;
};
extern volatile union rw_oproc_005Ah xdata g_rw_oproc_005Ah;    // Absolute Address = 585Ah

union rw_oproc_005Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_test_data_r_bits_9_4                                                 : 6;        // [msb:lsb] = [9:4], val = 6
        unsigned char r_test_data_g_bits_1_0                                                 : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_005Bh xdata g_rw_oproc_005Bh;    // Absolute Address = 585Bh

union rw_oproc_005Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_test_data_b_bits_9_8                                                 : 2;        // [msb:lsb] = [9:8], val = 0
        unsigned char r_test_a_ch_en                                                         : 1;        // val = 0
        unsigned char r_test_b_ch_en                                                         : 1;        // val = 0
        unsigned char r_test_c_ch_en                                                         : 1;        // val = 0
        unsigned char r_test_d_ch_en                                                         : 1;        // val = 0
        unsigned char r_test_e_ch_en                                                         : 1;        // val = 0
        unsigned char r_test_f_ch_en                                                         : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_005Eh xdata g_rw_oproc_005Eh;    // Absolute Address = 585Eh

union rw_oproc_005Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_test_g_ch_en                                                         : 1;        // val = 0
        unsigned char r_test_h_ch_en                                                         : 1;        // val = 0
        unsigned char r_chwb_opt                                                             : 1;        // val = 0
        unsigned char r_dym_sft_sel                                                          : 1;        // val = 0
        unsigned char r_tri_gate12_en                                                        : 1;        // val = 1
        unsigned char r_fix_htot                                                             : 1;        // val = 1
        unsigned char r_fix_htot_ctrl_sel                                                    : 1;        // val = 0
        unsigned char r_dym_sft_en                                                           : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_005Fh xdata g_rw_oproc_005Fh;    // Absolute Address = 585Fh

union rw_oproc_0060h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tp_sen                                                               : 1;        // val = 0
        unsigned char r_tp_en                                                                : 1;        // val = 0
        unsigned char r_tpl_data_st_bits_5_0                                                 : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0060h xdata g_rw_oproc_0060h;    // Absolute Address = 5860h

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_0063h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tph_data_st1_bits_13_8                                               : 6;        // [msb:lsb] = [13:8], val = 0
        unsigned char r_tph_data_st2_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0063h xdata g_rw_oproc_0063h;    // Absolute Address = 5863h

union rw_oproc_0065h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tph_data_st2_bits_13_10                                              : 4;        // [msb:lsb] = [13:10], val = 0
        unsigned char r_htot_ctrl_rec_bits_3_0                                               : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0065h xdata g_rw_oproc_0065h;    // Absolute Address = 5865h

union rw_oproc_0067h
{
    unsigned char byte;
    struct
    {
        unsigned char r_htot_ctrl_rec_bits_13_12                                             : 2;        // [msb:lsb] = [13:12], val = 0
        unsigned char r_fix_htot_data_sel                                                    : 1;        // val = 0
        unsigned char r_fix_htot_div2_ctrl                                                   : 1;        // val = 0
        unsigned char r_fix_htot_div2_data                                                   : 1;        // val = 0
        unsigned char r_de_ext4_en                                                           : 1;        // val = 0
        unsigned char r_de_ext5_en                                                           : 1;        // val = 0
        unsigned char r_tch966_en                                                            : 1;        // val = 0
    }bits;
};
extern volatile union rw_oproc_0067h xdata g_rw_oproc_0067h;    // Absolute Address = 5867h

union rw_oproc_0069h
{
    unsigned char byte;
    struct
    {
        unsigned char r_driv1_point_bits_11_8                                                : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_driv2_point_bits_3_0                                                 : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0069h xdata g_rw_oproc_0069h;    // Absolute Address = 5869h

union rw_oproc_006Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_driv3_point_bits_11_8                                                : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_driv4_point_bits_3_0                                                 : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_006Ch xdata g_rw_oproc_006Ch;    // Absolute Address = 586Ch

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_006Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_driv5_point_bits_11_8                                                : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_driv6_point_bits_3_0                                                 : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_006Fh xdata g_rw_oproc_006Fh;    // Absolute Address = 586Fh

union rw_oproc_0072h
{
    unsigned char byte;
    struct
    {
        unsigned char r_driv7_point_bits_11_8                                                : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_driv8_point_bits_3_0                                                 : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0072h xdata g_rw_oproc_0072h;    // Absolute Address = 5872h

union rw_oproc_0075h
{
    unsigned char byte;
    struct
    {
        unsigned char r_driv9_point_bits_11_8                                                : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_driv10_point_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0075h xdata g_rw_oproc_0075h;    // Absolute Address = 5875h

union rw_oproc_0078h
{
    unsigned char byte;
    struct
    {
        unsigned char r_driv11_point_bits_11_8                                               : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_driv12_point_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0078h xdata g_rw_oproc_0078h;    // Absolute Address = 5878h

union rw_oproc_007Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_end_point_bits_11_8                                                  : 4;        // [msb:lsb] = [11:8], val = 0
        unsigned char r_r1_driv1_sel_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_007Bh xdata g_rw_oproc_007Bh;    // Absolute Address = 587Bh

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_007Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_driv1_sel_bits_5_4                                                : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_g1_driv1_sel                                                         : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_oproc_007Ch xdata g_rw_oproc_007Ch;    // Absolute Address = 587Ch

union rw_oproc_007Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_driv1_sel                                                         : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_r1_driv2_sel_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_007Dh xdata g_rw_oproc_007Dh;    // Absolute Address = 587Dh

union rw_oproc_007Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_driv2_sel_bits_5_2                                                : 4;        // [msb:lsb] = [5:2], val = 0
        unsigned char r_g1_driv2_sel_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_007Eh xdata g_rw_oproc_007Eh;    // Absolute Address = 587Eh

union rw_oproc_007Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_driv2_sel_bits_5_4                                                : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_b1_driv2_sel                                                         : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_oproc_007Fh xdata g_rw_oproc_007Fh;    // Absolute Address = 587Fh

union rw_oproc_0080h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_driv3_sel                                                         : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_g1_driv3_sel_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0080h xdata g_rw_oproc_0080h;    // Absolute Address = 5880h

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_0081h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_driv3_sel_bits_5_2                                                : 4;        // [msb:lsb] = [5:2], val = 0
        unsigned char r_b1_driv3_sel_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0081h xdata g_rw_oproc_0081h;    // Absolute Address = 5881h

union rw_oproc_0082h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_driv3_sel_bits_5_4                                                : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_r1_driv4_sel                                                         : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0082h xdata g_rw_oproc_0082h;    // Absolute Address = 5882h

union rw_oproc_0083h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_driv4_sel                                                         : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_b1_driv4_sel_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0083h xdata g_rw_oproc_0083h;    // Absolute Address = 5883h

union rw_oproc_0084h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_driv4_sel_bits_5_2                                                : 4;        // [msb:lsb] = [5:2], val = 0
        unsigned char r_r1_driv5_sel_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0084h xdata g_rw_oproc_0084h;    // Absolute Address = 5884h

union rw_oproc_0085h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_driv5_sel_bits_5_4                                                : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_g1_driv5_sel                                                         : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0085h xdata g_rw_oproc_0085h;    // Absolute Address = 5885h

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_0086h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_driv5_sel                                                         : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_r1_driv6_sel_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0086h xdata g_rw_oproc_0086h;    // Absolute Address = 5886h

union rw_oproc_0087h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_driv6_sel_bits_5_2                                                : 4;        // [msb:lsb] = [5:2], val = 0
        unsigned char r_g1_driv6_sel_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0087h xdata g_rw_oproc_0087h;    // Absolute Address = 5887h

union rw_oproc_0088h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_driv6_sel_bits_5_4                                                : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_b1_driv6_sel                                                         : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0088h xdata g_rw_oproc_0088h;    // Absolute Address = 5888h

union rw_oproc_0089h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_driv7_sel                                                         : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_g1_driv7_sel_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0089h xdata g_rw_oproc_0089h;    // Absolute Address = 5889h

union rw_oproc_008Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_driv7_sel_bits_5_2                                                : 4;        // [msb:lsb] = [5:2], val = 0
        unsigned char r_b1_driv7_sel_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_008Ah xdata g_rw_oproc_008Ah;    // Absolute Address = 588Ah

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_008Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_driv7_sel_bits_5_4                                                : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_r1_driv8_sel                                                         : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_oproc_008Bh xdata g_rw_oproc_008Bh;    // Absolute Address = 588Bh

union rw_oproc_008Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_driv8_sel                                                         : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_b1_driv8_sel_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_008Ch xdata g_rw_oproc_008Ch;    // Absolute Address = 588Ch

union rw_oproc_008Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_driv8_sel_bits_5_2                                                : 4;        // [msb:lsb] = [5:2], val = 0
        unsigned char r_r1_driv9_sel_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_008Dh xdata g_rw_oproc_008Dh;    // Absolute Address = 588Dh

union rw_oproc_008Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_driv9_sel_bits_5_4                                                : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_g1_driv9_sel                                                         : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_oproc_008Eh xdata g_rw_oproc_008Eh;    // Absolute Address = 588Eh

union rw_oproc_008Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_driv9_sel                                                         : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_r1_driv10_sel_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_008Fh xdata g_rw_oproc_008Fh;    // Absolute Address = 588Fh

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_0090h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_driv10_sel_bits_5_2                                               : 4;        // [msb:lsb] = [5:2], val = 0
        unsigned char r_g1_driv10_sel_bits_3_0                                               : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0090h xdata g_rw_oproc_0090h;    // Absolute Address = 5890h

union rw_oproc_0091h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_driv10_sel_bits_5_4                                               : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_b1_driv10_sel                                                        : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0091h xdata g_rw_oproc_0091h;    // Absolute Address = 5891h

union rw_oproc_0092h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_driv11_sel                                                        : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_g1_driv11_sel_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0092h xdata g_rw_oproc_0092h;    // Absolute Address = 5892h

union rw_oproc_0093h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_driv11_sel_bits_5_2                                               : 4;        // [msb:lsb] = [5:2], val = 0
        unsigned char r_b1_driv11_sel_bits_3_0                                               : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0093h xdata g_rw_oproc_0093h;    // Absolute Address = 5893h

union rw_oproc_0094h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_driv11_sel_bits_5_4                                               : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_r1_driv12_sel                                                        : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0094h xdata g_rw_oproc_0094h;    // Absolute Address = 5894h

//------------------------------------------------------------------------------------------------------------------------
union rw_oproc_0095h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_driv12_sel                                                        : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_b1_driv12_sel_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_oproc_0095h xdata g_rw_oproc_0095h;    // Absolute Address = 5895h

union rw_oproc_0096h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_driv12_sel_bits_5_2                                               : 4;        // [msb:lsb] = [5:2], val = 0
        unsigned char reserved_3_by_tool                                                     : 1;
        unsigned char reserved_2_by_tool                                                     : 1;
        unsigned char reserved_1_by_tool                                                     : 1;
        unsigned char reserved_0_by_tool                                                     : 1;
    }bits;
};
extern volatile union rw_oproc_0096h xdata g_rw_oproc_0096h;    // Absolute Address = 5896h


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_oproc_0007h_lbuf_rd_hdisp;                                            // [msb:lsb] = [8:1], val = 0, 	Absolute Address = 5807h
extern volatile uint8_t xdata g_rw_oproc_000Ch_a_start_pix;                                              // [msb:lsb] = [8:1], val = 255, 	Absolute Address = 580Ch
extern volatile uint8_t xdata g_rw_oproc_000Fh_lbuf_addr_max;                                            // [msb:lsb] = [8:1], val = 11, 	Absolute Address = 580Fh
extern volatile uint8_t xdata g_rw_oproc_0031h_htot_rec;                                                 // [msb:lsb] = [9:2], val = 0, 	Absolute Address = 5831h
extern volatile uint8_t xdata g_rw_oproc_003Ah_frame_inv_num2;                                           // [msb:lsb] = [10:3], val = 210, 	Absolute Address = 583Ah

extern volatile uint8_t xdata g_rw_oproc_003Ch_htot_cmp_reg_min;                                         // [msb:lsb] = [9:2], val = 150, 	Absolute Address = 583Ch
extern volatile uint8_t xdata g_rw_oproc_003Fh_dym_sft_num;                                              // [msb:lsb] = [12:5], val = 49, 	Absolute Address = 583Fh
extern volatile uint8_t xdata g_rw_oproc_0043h_c_start_pix;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 5843h
extern volatile uint8_t xdata g_rw_oproc_0045h_d_start_pix;                                              // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 5845h
extern volatile uint8_t xdata g_rw_oproc_0046h_e_start_pix;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 5846h

extern volatile uint8_t xdata g_rw_oproc_0048h_f_start_pix;                                              // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 5848h
extern volatile uint8_t xdata g_rw_oproc_0049h_g_start_pix;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 5849h
extern volatile uint8_t xdata g_rw_oproc_004Bh_h_start_pix;                                              // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 584Bh
extern volatile uint8_t xdata g_rw_oproc_0053h_measure_dt_2;                                             // [msb:lsb] = [8:1], val = 0, 	Absolute Address = 5853h
extern volatile uint8_t xdata g_rw_oproc_0058h_measure_dt_6;                                             // [msb:lsb] = [8:1], val = 0, 	Absolute Address = 5858h

extern volatile uint8_t xdata g_rw_oproc_005Ch_test_data_g;                                              // [msb:lsb] = [9:2], val = 25, 	Absolute Address = 585Ch
extern volatile uint8_t xdata g_rw_oproc_005Dh_test_data_b;                                              // [msb:lsb] = [7:0], val = 100, 	Absolute Address = 585Dh
extern volatile uint8_t xdata g_rw_oproc_0061h_tpl_data_st;                                              // [msb:lsb] = [13:6], val = 0, 	Absolute Address = 5861h
extern volatile uint8_t xdata g_rw_oproc_0062h_tph_data_st1;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 5862h
extern volatile uint8_t xdata g_rw_oproc_0064h_tph_data_st2;                                             // [msb:lsb] = [9:2], val = 0, 	Absolute Address = 5864h

extern volatile uint8_t xdata g_rw_oproc_0066h_htot_ctrl_rec;                                            // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 5866h
extern volatile uint8_t xdata g_rw_oproc_0068h_driv1_point;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 5868h
extern volatile uint8_t xdata g_rw_oproc_006Ah_driv2_point;                                              // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 586Ah
extern volatile uint8_t xdata g_rw_oproc_006Bh_driv3_point;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 586Bh
extern volatile uint8_t xdata g_rw_oproc_006Dh_driv4_point;                                              // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 586Dh

extern volatile uint8_t xdata g_rw_oproc_006Eh_driv5_point;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 586Eh
extern volatile uint8_t xdata g_rw_oproc_0070h_driv6_point;                                              // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 5870h
extern volatile uint8_t xdata g_rw_oproc_0071h_driv7_point;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 5871h
extern volatile uint8_t xdata g_rw_oproc_0073h_driv8_point;                                              // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 5873h
extern volatile uint8_t xdata g_rw_oproc_0074h_driv9_point;                                              // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 5874h

extern volatile uint8_t xdata g_rw_oproc_0076h_driv10_point;                                             // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 5876h
extern volatile uint8_t xdata g_rw_oproc_0077h_driv11_point;                                             // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 5877h
extern volatile uint8_t xdata g_rw_oproc_0079h_driv12_point;                                             // [msb:lsb] = [11:4], val = 0, 	Absolute Address = 5879h
extern volatile uint8_t xdata g_rw_oproc_007Ah_end_point;                                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 587Ah

#endif