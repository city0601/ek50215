#ifndef _FRC_SET_MODE3_H_
#define _FRC_SET_MODE3_H_
#include "reg_include.h"


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_frc_set_mode3_0000h_FRC_LUT_0_data_0;                                 // [msb:lsb] = [7:0], val = 127, 	Absolute Address = 9800h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0001h_FRC_LUT_0_data_1;                                 // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9801h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0002h_FRC_LUT_0_data_2;                                 // [msb:lsb] = [7:0], val = 224, 	Absolute Address = 9802h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0003h_FRC_LUT_0_data_3;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9803h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0004h_FRC_LUT_0_data_4;                                 // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9804h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0005h_FRC_LUT_0_data_5;                                 // [msb:lsb] = [7:0], val = 126, 	Absolute Address = 9805h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0006h_FRC_LUT_0_data_6;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9806h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0007h_FRC_LUT_0_data_7;                                 // [msb:lsb] = [7:0], val = 224, 	Absolute Address = 9807h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0008h_FRC_LUT_0_data_8;                                 // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9808h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0009h_FRC_LUT_0_data_9;                                 // [msb:lsb] = [7:0], val = 224, 	Absolute Address = 9809h

extern volatile uint8_t xdata g_rw_frc_set_mode3_000Ah_FRC_LUT_0_data_10;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 980Ah
extern volatile uint8_t xdata g_rw_frc_set_mode3_000Bh_FRC_LUT_0_data_11;                                // [msb:lsb] = [7:0], val = 126, 	Absolute Address = 980Bh
extern volatile uint8_t xdata g_rw_frc_set_mode3_000Ch_FRC_LUT_0_data_12;                                // [msb:lsb] = [7:0], val = 224, 	Absolute Address = 980Ch
extern volatile uint8_t xdata g_rw_frc_set_mode3_000Dh_FRC_LUT_0_data_13;                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 980Dh
extern volatile uint8_t xdata g_rw_frc_set_mode3_000Eh_FRC_LUT_0_data_14;                                // [msb:lsb] = [7:0], val = 126, 	Absolute Address = 980Eh

extern volatile uint8_t xdata g_rw_frc_set_mode3_000Fh_FRC_LUT_0_data_15;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 980Fh
extern volatile uint8_t xdata g_rw_frc_set_mode3_0010h_FRC_LUT_0_data_16;                                // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 9810h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0011h_FRC_LUT_0_data_17;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9811h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0012h_FRC_LUT_0_data_18;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9812h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0013h_FRC_LUT_0_data_19;                                // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 9813h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0014h_FRC_LUT_0_data_20;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9814h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0015h_FRC_LUT_0_data_21;                                // [msb:lsb] = [7:0], val = 105, 	Absolute Address = 9815h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0016h_FRC_LUT_0_data_22;                                // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 9816h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0017h_FRC_LUT_0_data_23;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9817h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0018h_FRC_LUT_0_data_24;                                // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 9818h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0019h_FRC_LUT_0_data_25;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9819h
extern volatile uint8_t xdata g_rw_frc_set_mode3_001Ah_FRC_LUT_0_data_26;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 981Ah
extern volatile uint8_t xdata g_rw_frc_set_mode3_001Bh_FRC_LUT_0_data_27;                                // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 981Bh
extern volatile uint8_t xdata g_rw_frc_set_mode3_001Ch_FRC_LUT_0_data_28;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 981Ch
extern volatile uint8_t xdata g_rw_frc_set_mode3_001Dh_FRC_LUT_0_data_29;                                // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 981Dh

extern volatile uint8_t xdata g_rw_frc_set_mode3_001Eh_FRC_LUT_0_data_30;                                // [msb:lsb] = [7:0], val = 105, 	Absolute Address = 981Eh
extern volatile uint8_t xdata g_rw_frc_set_mode3_001Fh_FRC_LUT_0_data_31;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 981Fh
extern volatile uint8_t xdata g_rw_frc_set_mode3_0020h_FRC_LUT_0_data_32;                                // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 9820h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0021h_FRC_LUT_0_data_33;                                // [msb:lsb] = [7:0], val = 132, 	Absolute Address = 9821h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0022h_FRC_LUT_0_data_34;                                // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 9822h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0023h_FRC_LUT_0_data_35;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9823h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0024h_FRC_LUT_0_data_36;                                // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 9824h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0025h_FRC_LUT_0_data_37;                                // [msb:lsb] = [7:0], val = 97, 	Absolute Address = 9825h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0026h_FRC_LUT_0_data_38;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9826h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0027h_FRC_LUT_0_data_39;                                // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 9827h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0028h_FRC_LUT_0_data_40;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9828h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0029h_FRC_LUT_0_data_41;                                // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 9829h
extern volatile uint8_t xdata g_rw_frc_set_mode3_002Ah_FRC_LUT_0_data_42;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 982Ah
extern volatile uint8_t xdata g_rw_frc_set_mode3_002Bh_FRC_LUT_0_data_43;                                // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 982Bh
extern volatile uint8_t xdata g_rw_frc_set_mode3_002Ch_FRC_LUT_0_data_44;                                // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 982Ch

extern volatile uint8_t xdata g_rw_frc_set_mode3_002Dh_FRC_LUT_0_data_45;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 982Dh
extern volatile uint8_t xdata g_rw_frc_set_mode3_002Eh_FRC_LUT_0_data_46;                                // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 982Eh
extern volatile uint8_t xdata g_rw_frc_set_mode3_002Fh_FRC_LUT_0_data_47;                                // [msb:lsb] = [7:0], val = 132, 	Absolute Address = 982Fh
extern volatile uint8_t xdata g_rw_frc_set_mode3_0030h_FRC_LUT_0_data_48;                                // [msb:lsb] = [7:0], val = 1, 	Absolute Address = 9830h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0031h_FRC_LUT_0_data_49;                                // [msb:lsb] = [7:0], val = 150, 	Absolute Address = 9831h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0032h_FRC_LUT_0_data_50;                                // [msb:lsb] = [7:0], val = 124, 	Absolute Address = 9832h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0033h_FRC_LUT_0_data_51;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9833h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0034h_FRC_LUT_0_data_52;                                // [msb:lsb] = [7:0], val = 150, 	Absolute Address = 9834h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0035h_FRC_LUT_0_data_53;                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9835h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0036h_FRC_LUT_0_data_54;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9836h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0037h_FRC_LUT_0_data_55;                                // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 9837h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0038h_FRC_LUT_0_data_56;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9838h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0039h_FRC_LUT_0_data_57;                                // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 9839h
extern volatile uint8_t xdata g_rw_frc_set_mode3_003Ah_FRC_LUT_0_data_58;                                // [msb:lsb] = [7:0], val = 150, 	Absolute Address = 983Ah
extern volatile uint8_t xdata g_rw_frc_set_mode3_003Bh_FRC_LUT_0_data_59;                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 983Bh

extern volatile uint8_t xdata g_rw_frc_set_mode3_003Ch_FRC_LUT_0_data_60;                                // [msb:lsb] = [7:0], val = 108, 	Absolute Address = 983Ch
extern volatile uint8_t xdata g_rw_frc_set_mode3_003Dh_FRC_LUT_0_data_61;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 983Dh
extern volatile uint8_t xdata g_rw_frc_set_mode3_003Eh_FRC_LUT_0_data_62;                                // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 983Eh
extern volatile uint8_t xdata g_rw_frc_set_mode3_003Fh_FRC_LUT_0_data_63;                                // [msb:lsb] = [7:0], val = 150, 	Absolute Address = 983Fh
extern volatile uint8_t xdata g_rw_frc_set_mode3_0040h_FRC_LUT_0_data_64;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9840h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0041h_FRC_LUT_0_data_65;                                // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 9841h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0042h_FRC_LUT_0_data_66;                                // [msb:lsb] = [7:0], val = 22, 	Absolute Address = 9842h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0043h_FRC_LUT_0_data_67;                                // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 9843h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0044h_FRC_LUT_0_data_68;                                // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 9844h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0045h_FRC_LUT_0_data_69;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9845h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0046h_FRC_LUT_0_data_70;                                // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 9846h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0047h_FRC_LUT_0_data_71;                                // [msb:lsb] = [7:0], val = 86, 	Absolute Address = 9847h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0048h_FRC_LUT_0_data_72;                                // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 9848h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0049h_FRC_LUT_0_data_73;                                // [msb:lsb] = [7:0], val = 86, 	Absolute Address = 9849h
extern volatile uint8_t xdata g_rw_frc_set_mode3_004Ah_FRC_LUT_0_data_74;                                // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 984Ah

extern volatile uint8_t xdata g_rw_frc_set_mode3_004Bh_FRC_LUT_0_data_75;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 984Bh
extern volatile uint8_t xdata g_rw_frc_set_mode3_004Ch_FRC_LUT_0_data_76;                                // [msb:lsb] = [7:0], val = 6, 	Absolute Address = 984Ch
extern volatile uint8_t xdata g_rw_frc_set_mode3_004Dh_FRC_LUT_0_data_77;                                // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 984Dh
extern volatile uint8_t xdata g_rw_frc_set_mode3_004Eh_FRC_LUT_0_data_78;                                // [msb:lsb] = [7:0], val = 129, 	Absolute Address = 984Eh
extern volatile uint8_t xdata g_rw_frc_set_mode3_004Fh_FRC_LUT_0_data_79;                                // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 984Fh

extern volatile uint8_t xdata g_rw_frc_set_mode3_0050h_FRC_LUT_0_data_80;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9850h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0051h_FRC_LUT_0_data_81;                                // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 9851h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0052h_FRC_LUT_0_data_82;                                // [msb:lsb] = [7:0], val = 80, 	Absolute Address = 9852h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0053h_FRC_LUT_0_data_83;                                // [msb:lsb] = [7:0], val = 232, 	Absolute Address = 9853h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0054h_FRC_LUT_0_data_84;                                // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 9854h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0055h_FRC_LUT_0_data_85;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9855h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0056h_FRC_LUT_0_data_86;                                // [msb:lsb] = [7:0], val = 232, 	Absolute Address = 9856h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0057h_FRC_LUT_0_data_87;                                // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 9857h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0058h_FRC_LUT_0_data_88;                                // [msb:lsb] = [7:0], val = 232, 	Absolute Address = 9858h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0059h_FRC_LUT_0_data_89;                                // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 9859h

extern volatile uint8_t xdata g_rw_frc_set_mode3_005Ah_FRC_LUT_0_data_90;                                // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 985Ah
extern volatile uint8_t xdata g_rw_frc_set_mode3_005Bh_FRC_LUT_0_data_91;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 985Bh
extern volatile uint8_t xdata g_rw_frc_set_mode3_005Ch_FRC_LUT_0_data_92;                                // [msb:lsb] = [7:0], val = 80, 	Absolute Address = 985Ch
extern volatile uint8_t xdata g_rw_frc_set_mode3_005Dh_FRC_LUT_0_data_93;                                // [msb:lsb] = [7:0], val = 232, 	Absolute Address = 985Dh
extern volatile uint8_t xdata g_rw_frc_set_mode3_005Eh_FRC_LUT_0_data_94;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 985Eh

extern volatile uint8_t xdata g_rw_frc_set_mode3_005Fh_FRC_LUT_0_data_95;                                // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 985Fh
extern volatile uint8_t xdata g_rw_frc_set_mode3_0060h_FRC_LUT_0_data_96;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9860h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0061h_FRC_LUT_0_data_97;                                // [msb:lsb] = [7:0], val = 72, 	Absolute Address = 9861h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0062h_FRC_LUT_0_data_98;                                // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9862h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0063h_FRC_LUT_0_data_99;                                // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 9863h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0064h_FRC_LUT_0_data_100;                               // [msb:lsb] = [7:0], val = 76, 	Absolute Address = 9864h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0065h_FRC_LUT_0_data_101;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9865h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0066h_FRC_LUT_0_data_102;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 9866h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0067h_FRC_LUT_0_data_103;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9867h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0068h_FRC_LUT_0_data_104;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 9868h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0069h_FRC_LUT_0_data_105;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9869h
extern volatile uint8_t xdata g_rw_frc_set_mode3_006Ah_FRC_LUT_0_data_106;                               // [msb:lsb] = [7:0], val = 76, 	Absolute Address = 986Ah
extern volatile uint8_t xdata g_rw_frc_set_mode3_006Bh_FRC_LUT_0_data_107;                               // [msb:lsb] = [7:0], val = 129, 	Absolute Address = 986Bh
extern volatile uint8_t xdata g_rw_frc_set_mode3_006Ch_FRC_LUT_0_data_108;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 986Ch
extern volatile uint8_t xdata g_rw_frc_set_mode3_006Dh_FRC_LUT_0_data_109;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 986Dh

extern volatile uint8_t xdata g_rw_frc_set_mode3_006Eh_FRC_LUT_0_data_110;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 986Eh
extern volatile uint8_t xdata g_rw_frc_set_mode3_006Fh_FRC_LUT_0_data_111;                               // [msb:lsb] = [7:0], val = 72, 	Absolute Address = 986Fh
extern volatile uint8_t xdata g_rw_frc_set_mode3_0070h_FRC_LUT_0_data_112;                               // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 9870h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0071h_FRC_LUT_0_data_113;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 9871h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0072h_FRC_LUT_0_data_114;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9872h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0073h_FRC_LUT_0_data_115;                               // [msb:lsb] = [7:0], val = 102, 	Absolute Address = 9873h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0074h_FRC_LUT_0_data_116;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 9874h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0075h_FRC_LUT_0_data_117;                               // [msb:lsb] = [7:0], val = 148, 	Absolute Address = 9875h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0076h_FRC_LUT_0_data_118;                               // [msb:lsb] = [7:0], val = 102, 	Absolute Address = 9876h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0077h_FRC_LUT_0_data_119;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9877h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0078h_FRC_LUT_0_data_120;                               // [msb:lsb] = [7:0], val = 102, 	Absolute Address = 9878h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0079h_FRC_LUT_0_data_121;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9879h
extern volatile uint8_t xdata g_rw_frc_set_mode3_007Ah_FRC_LUT_0_data_122;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 987Ah
extern volatile uint8_t xdata g_rw_frc_set_mode3_007Bh_FRC_LUT_0_data_123;                               // [msb:lsb] = [7:0], val = 149, 	Absolute Address = 987Bh
extern volatile uint8_t xdata g_rw_frc_set_mode3_007Ch_FRC_LUT_0_data_124;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 987Ch

extern volatile uint8_t xdata g_rw_frc_set_mode3_007Dh_FRC_LUT_0_data_125;                               // [msb:lsb] = [7:0], val = 102, 	Absolute Address = 987Dh
extern volatile uint8_t xdata g_rw_frc_set_mode3_007Eh_FRC_LUT_0_data_126;                               // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 987Eh
extern volatile uint8_t xdata g_rw_frc_set_mode3_007Fh_FRC_LUT_0_data_127;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 987Fh
extern volatile uint8_t xdata g_rw_frc_set_mode3_0080h_FRC_LUT_0_data_128;                               // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 9880h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0081h_FRC_LUT_0_data_129;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9881h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0082h_FRC_LUT_0_data_130;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9882h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0083h_FRC_LUT_0_data_131;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9883h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0084h_FRC_LUT_0_data_132;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9884h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0085h_FRC_LUT_0_data_133;                               // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 9885h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0086h_FRC_LUT_0_data_134;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9886h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0087h_FRC_LUT_0_data_135;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9887h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0088h_FRC_LUT_0_data_136;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9888h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0089h_FRC_LUT_0_data_137;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9889h
extern volatile uint8_t xdata g_rw_frc_set_mode3_008Ah_FRC_LUT_0_data_138;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 988Ah
extern volatile uint8_t xdata g_rw_frc_set_mode3_008Bh_FRC_LUT_0_data_139;                               // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 988Bh

extern volatile uint8_t xdata g_rw_frc_set_mode3_008Ch_FRC_LUT_0_data_140;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 988Ch
extern volatile uint8_t xdata g_rw_frc_set_mode3_008Dh_FRC_LUT_0_data_141;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 988Dh
extern volatile uint8_t xdata g_rw_frc_set_mode3_008Eh_FRC_LUT_0_data_142;                               // [msb:lsb] = [7:0], val = 120, 	Absolute Address = 988Eh
extern volatile uint8_t xdata g_rw_frc_set_mode3_008Fh_FRC_LUT_0_data_143;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 988Fh
extern volatile uint8_t xdata g_rw_frc_set_mode3_0090h_FRC_LUT_0_data_144;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 9890h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0091h_FRC_LUT_0_data_145;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9891h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0092h_FRC_LUT_0_data_146;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9892h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0093h_FRC_LUT_0_data_147;                               // [msb:lsb] = [7:0], val = 118, 	Absolute Address = 9893h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0094h_FRC_LUT_0_data_148;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9894h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0095h_FRC_LUT_0_data_149;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 9895h

extern volatile uint8_t xdata g_rw_frc_set_mode3_0096h_FRC_LUT_0_data_150;                               // [msb:lsb] = [7:0], val = 118, 	Absolute Address = 9896h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0097h_FRC_LUT_0_data_151;                               // [msb:lsb] = [7:0], val = 129, 	Absolute Address = 9897h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0098h_FRC_LUT_0_data_152;                               // [msb:lsb] = [7:0], val = 118, 	Absolute Address = 9898h
extern volatile uint8_t xdata g_rw_frc_set_mode3_0099h_FRC_LUT_0_data_153;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 9899h
extern volatile uint8_t xdata g_rw_frc_set_mode3_009Ah_FRC_LUT_0_data_154;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 989Ah

extern volatile uint8_t xdata g_rw_frc_set_mode3_009Bh_FRC_LUT_0_data_155;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 989Bh
extern volatile uint8_t xdata g_rw_frc_set_mode3_009Ch_FRC_LUT_0_data_156;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 989Ch
extern volatile uint8_t xdata g_rw_frc_set_mode3_009Dh_FRC_LUT_0_data_157;                               // [msb:lsb] = [7:0], val = 118, 	Absolute Address = 989Dh
extern volatile uint8_t xdata g_rw_frc_set_mode3_009Eh_FRC_LUT_0_data_158;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 989Eh
extern volatile uint8_t xdata g_rw_frc_set_mode3_009Fh_FRC_LUT_0_data_159;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 989Fh

extern volatile uint8_t xdata g_rw_frc_set_mode3_00A0h_FRC_LUT_0_data_160;                               // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 98A0h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00A1h_FRC_LUT_0_data_161;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98A1h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00A2h_FRC_LUT_0_data_162;                               // [msb:lsb] = [7:0], val = 126, 	Absolute Address = 98A2h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00A3h_FRC_LUT_0_data_163;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98A3h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00A4h_FRC_LUT_0_data_164;                               // [msb:lsb] = [7:0], val = 132, 	Absolute Address = 98A4h

extern volatile uint8_t xdata g_rw_frc_set_mode3_00A5h_FRC_LUT_0_data_165;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 98A5h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00A6h_FRC_LUT_0_data_166;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98A6h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00A7h_FRC_LUT_0_data_167;                               // [msb:lsb] = [7:0], val = 126, 	Absolute Address = 98A7h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00A8h_FRC_LUT_0_data_168;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98A8h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00A9h_FRC_LUT_0_data_169;                               // [msb:lsb] = [7:0], val = 127, 	Absolute Address = 98A9h

extern volatile uint8_t xdata g_rw_frc_set_mode3_00AAh_FRC_LUT_0_data_170;                               // [msb:lsb] = [7:0], val = 148, 	Absolute Address = 98AAh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00ABh_FRC_LUT_0_data_171;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 98ABh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00ACh_FRC_LUT_0_data_172;                               // [msb:lsb] = [7:0], val = 126, 	Absolute Address = 98ACh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00ADh_FRC_LUT_0_data_173;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98ADh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00AEh_FRC_LUT_0_data_174;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 98AEh

extern volatile uint8_t xdata g_rw_frc_set_mode3_00AFh_FRC_LUT_0_data_175;                               // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 98AFh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00B0h_FRC_LUT_0_data_176;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 98B0h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00B1h_FRC_LUT_0_data_177;                               // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 98B1h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00B2h_FRC_LUT_0_data_178;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 98B2h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00B3h_FRC_LUT_0_data_179;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98B3h

extern volatile uint8_t xdata g_rw_frc_set_mode3_00B4h_FRC_LUT_0_data_180;                               // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 98B4h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00B5h_FRC_LUT_0_data_181;                               // [msb:lsb] = [7:0], val = 4, 	Absolute Address = 98B5h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00B6h_FRC_LUT_0_data_182;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98B6h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00B7h_FRC_LUT_0_data_183;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 98B7h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00B8h_FRC_LUT_0_data_184;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98B8h

extern volatile uint8_t xdata g_rw_frc_set_mode3_00B9h_FRC_LUT_0_data_185;                               // [msb:lsb] = [7:0], val = 105, 	Absolute Address = 98B9h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00BAh_FRC_LUT_0_data_186;                               // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 98BAh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00BBh_FRC_LUT_0_data_187;                               // [msb:lsb] = [7:0], val = 4, 	Absolute Address = 98BBh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00BCh_FRC_LUT_0_data_188;                               // [msb:lsb] = [7:0], val = 104, 	Absolute Address = 98BCh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00BDh_FRC_LUT_0_data_189;                               // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 98BDh

extern volatile uint8_t xdata g_rw_frc_set_mode3_00BEh_FRC_LUT_0_data_190;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 98BEh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00BFh_FRC_LUT_0_data_191;                               // [msb:lsb] = [7:0], val = 240, 	Absolute Address = 98BFh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00C0h_FRC_LUT_0_data_192;                               // [msb:lsb] = [7:0], val = 192, 	Absolute Address = 98C0h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00C1h_FRC_LUT_0_data_193;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 98C1h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00C2h_FRC_LUT_0_data_194;                               // [msb:lsb] = [7:0], val = 97, 	Absolute Address = 98C2h

extern volatile uint8_t xdata g_rw_frc_set_mode3_00C3h_FRC_LUT_0_data_195;                               // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 98C3h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00C4h_FRC_LUT_0_data_196;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 98C4h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00C5h_FRC_LUT_0_data_197;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98C5h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00C6h_FRC_LUT_0_data_198;                               // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 98C6h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00C7h_FRC_LUT_0_data_199;                               // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 98C7h

extern volatile uint8_t xdata g_rw_frc_set_mode3_00C8h_FRC_LUT_0_data_200;                               // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 98C8h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00C9h_FRC_LUT_0_data_201;                               // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 98C9h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00CAh_FRC_LUT_0_data_202;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 98CAh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00CBh_FRC_LUT_0_data_203;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98CBh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00CCh_FRC_LUT_0_data_204;                               // [msb:lsb] = [7:0], val = 97, 	Absolute Address = 98CCh

extern volatile uint8_t xdata g_rw_frc_set_mode3_00CDh_FRC_LUT_0_data_205;                               // [msb:lsb] = [7:0], val = 152, 	Absolute Address = 98CDh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00CEh_FRC_LUT_0_data_206;                               // [msb:lsb] = [7:0], val = 192, 	Absolute Address = 98CEh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00CFh_FRC_LUT_0_data_207;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 98CFh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00D0h_FRC_LUT_0_data_208;                               // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 98D0h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00D1h_FRC_LUT_0_data_209;                               // [msb:lsb] = [7:0], val = 102, 	Absolute Address = 98D1h

extern volatile uint8_t xdata g_rw_frc_set_mode3_00D2h_FRC_LUT_0_data_210;                               // [msb:lsb] = [7:0], val = 16, 	Absolute Address = 98D2h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00D3h_FRC_LUT_0_data_211;                               // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 98D3h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00D4h_FRC_LUT_0_data_212;                               // [msb:lsb] = [7:0], val = 102, 	Absolute Address = 98D4h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00D5h_FRC_LUT_0_data_213;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98D5h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00D6h_FRC_LUT_0_data_214;                               // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 98D6h

extern volatile uint8_t xdata g_rw_frc_set_mode3_00D7h_FRC_LUT_0_data_215;                               // [msb:lsb] = [7:0], val = 80, 	Absolute Address = 98D7h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00D8h_FRC_LUT_0_data_216;                               // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 98D8h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00D9h_FRC_LUT_0_data_217;                               // [msb:lsb] = [7:0], val = 80, 	Absolute Address = 98D9h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00DAh_FRC_LUT_0_data_218;                               // [msb:lsb] = [7:0], val = 118, 	Absolute Address = 98DAh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00DBh_FRC_LUT_0_data_219;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98DBh

extern volatile uint8_t xdata g_rw_frc_set_mode3_00DCh_FRC_LUT_0_data_220;                               // [msb:lsb] = [7:0], val = 17, 	Absolute Address = 98DCh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00DDh_FRC_LUT_0_data_221;                               // [msb:lsb] = [7:0], val = 136, 	Absolute Address = 98DDh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00DEh_FRC_LUT_0_data_222;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98DEh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00DFh_FRC_LUT_0_data_223;                               // [msb:lsb] = [7:0], val = 118, 	Absolute Address = 98DFh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00E0h_FRC_LUT_0_data_224;                               // [msb:lsb] = [7:0], val = 230, 	Absolute Address = 98E0h

extern volatile uint8_t xdata g_rw_frc_set_mode3_00E1h_FRC_LUT_0_data_225;                               // [msb:lsb] = [7:0], val = 8, 	Absolute Address = 98E1h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00E2h_FRC_LUT_0_data_226;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98E2h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00E3h_FRC_LUT_0_data_227;                               // [msb:lsb] = [7:0], val = 116, 	Absolute Address = 98E3h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00E4h_FRC_LUT_0_data_228;                               // [msb:lsb] = [7:0], val = 8, 	Absolute Address = 98E4h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00E5h_FRC_LUT_0_data_229;                               // [msb:lsb] = [7:0], val = 230, 	Absolute Address = 98E5h

extern volatile uint8_t xdata g_rw_frc_set_mode3_00E6h_FRC_LUT_0_data_230;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 98E6h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00E7h_FRC_LUT_0_data_231;                               // [msb:lsb] = [7:0], val = 129, 	Absolute Address = 98E7h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00E8h_FRC_LUT_0_data_232;                               // [msb:lsb] = [7:0], val = 112, 	Absolute Address = 98E8h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00E9h_FRC_LUT_0_data_233;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98E9h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00EAh_FRC_LUT_0_data_234;                               // [msb:lsb] = [7:0], val = 8, 	Absolute Address = 98EAh

extern volatile uint8_t xdata g_rw_frc_set_mode3_00EBh_FRC_LUT_0_data_235;                               // [msb:lsb] = [7:0], val = 230, 	Absolute Address = 98EBh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00ECh_FRC_LUT_0_data_236;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98ECh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00EDh_FRC_LUT_0_data_237;                               // [msb:lsb] = [7:0], val = 116, 	Absolute Address = 98EDh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00EEh_FRC_LUT_0_data_238;                               // [msb:lsb] = [7:0], val = 230, 	Absolute Address = 98EEh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00EFh_FRC_LUT_0_data_239;                               // [msb:lsb] = [7:0], val = 8, 	Absolute Address = 98EFh

extern volatile uint8_t xdata g_rw_frc_set_mode3_00F0h_FRC_LUT_0_data_240;                               // [msb:lsb] = [7:0], val = 148, 	Absolute Address = 98F0h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00F1h_FRC_LUT_0_data_241;                               // [msb:lsb] = [7:0], val = 72, 	Absolute Address = 98F1h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00F2h_FRC_LUT_0_data_242;                               // [msb:lsb] = [7:0], val = 129, 	Absolute Address = 98F2h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00F3h_FRC_LUT_0_data_243;                               // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 98F3h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00F4h_FRC_LUT_0_data_244;                               // [msb:lsb] = [7:0], val = 72, 	Absolute Address = 98F4h

extern volatile uint8_t xdata g_rw_frc_set_mode3_00F5h_FRC_LUT_0_data_245;                               // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 98F5h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00F6h_FRC_LUT_0_data_246;                               // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 98F6h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00F7h_FRC_LUT_0_data_247;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98F7h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00F8h_FRC_LUT_0_data_248;                               // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 98F8h
extern volatile uint8_t xdata g_rw_frc_set_mode3_00F9h_FRC_LUT_0_data_249;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98F9h

extern volatile uint8_t xdata g_rw_frc_set_mode3_00FAh_FRC_LUT_0_data_250;                               // [msb:lsb] = [7:0], val = 72, 	Absolute Address = 98FAh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00FBh_FRC_LUT_0_data_251;                               // [msb:lsb] = [7:0], val = 144, 	Absolute Address = 98FBh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00FCh_FRC_LUT_0_data_252;                               // [msb:lsb] = [7:0], val = 128, 	Absolute Address = 98FCh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00FDh_FRC_LUT_0_data_253;                               // [msb:lsb] = [7:0], val = 96, 	Absolute Address = 98FDh
extern volatile uint8_t xdata g_rw_frc_set_mode3_00FEh_FRC_LUT_0_data_254;                               // [msb:lsb] = [7:0], val = 148, 	Absolute Address = 98FEh

extern volatile uint8_t xdata g_rw_frc_set_mode3_00FFh_FRC_LUT_0_data_255;                               // [msb:lsb] = [7:0], val = 72, 	Absolute Address = 98FFh
extern volatile uint8_t xdata g_rw_frc_set_mode3_0100h_FRC_LUT_0_data_256;                               // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 9900h

#endif