#ifndef _AIP_REG_H_
#define _AIP_REG_H_
#include "reg_include.h"

union rw_aip_reg_0000h
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscgpll_sscg_p_normal                                                : 3;        // [msb:lsb] = [2:0], val = 2
        unsigned char r_sscgpll_sscg_q_normal                                                : 3;        // [msb:lsb] = [2:0], val = 4
        unsigned char r_sscgpll_sscg_r_normal                                                : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0000h xdata g_rw_aip_reg_0000h;    // Absolute Address = 1800h

union rw_aip_reg_0001h
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscgpll_sscg_p                                                       : 3;        // [msb:lsb] = [2:0], val = 1
        unsigned char r_sscgpll_sscg_q                                                       : 3;        // [msb:lsb] = [2:0], val = 4
        unsigned char r_sscgpll_sscg_r                                                       : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0001h xdata g_rw_aip_reg_0001h;    // Absolute Address = 1801h

union rw_aip_reg_0002h
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscgpll_twr                                                          : 1;        // val = 0
        unsigned char r_sscgpll_sel_rst_pfd0                                                 : 1;        // val = 1
        unsigned char r_sscgpll_sel_lock_div89                                               : 1;        // val = 0
        unsigned char r_sscg2_r_mash_reset_sel                                               : 1;        // val = 1
        unsigned char r_sscg2_r_mash_detcnt_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0002h xdata g_rw_aip_reg_0002h;    // Absolute Address = 1802h

union rw_aip_reg_0003h
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscg2_r_mash_detcnt_bits_7_4                                         : 4;        // [msb:lsb] = [7:4], val = 13
        unsigned char r_sscg2_mash_order_2                                                   : 1;        // val = 0
        unsigned char r_sscg2_r_mod_md_2x                                                    : 1;        // val = 0
        unsigned char r_r_q_div2_bits_1_0                                                    : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0003h xdata g_rw_aip_reg_0003h;    // Absolute Address = 1803h

union rw_aip_reg_0004h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r_q_div2_bits_2                                                      : 1;        // val = 1
        unsigned char r_sscg_ref_cnt_sel                                                     : 1;        // val = 0
        unsigned char r_sscg_frclkout_x2                                                     : 1;        // val = 0
        unsigned char r_sscg_8phase_sel                                                      : 1;        // val = 1
        unsigned char r_sscg_reset_offset_bits_3_0                                           : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0004h xdata g_rw_aip_reg_0004h;    // Absolute Address = 1804h

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_0005h
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscg_reset_offset_bits_9_4                                           : 6;        // [msb:lsb] = [9:4], val = 0
        unsigned char r_sscgphase_num_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_aip_reg_0005h xdata g_rw_aip_reg_0005h;    // Absolute Address = 1805h

union rw_aip_reg_0006h
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscgphase_num_bits_4_2                                               : 3;        // [msb:lsb] = [4:2], val = 1
        unsigned char r_sscgphase_step                                                       : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_sscgpll_en_sdm                                                       : 1;        // val = 0
        unsigned char r_sscg2_r_mod_fq_manual2_bits_1_0                                      : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0006h xdata g_rw_aip_reg_0006h;    // Absolute Address = 1806h

union rw_aip_reg_0009h
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscg2_mod_fq                                                         : 3;        // [msb:lsb] = [2:0], val = 4
        unsigned char r_sscg2_mod_md                                                         : 4;        // [msb:lsb] = [3:0], val = 8
        unsigned char r_sscgpll_ssc_pass                                                     : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0009h xdata g_rw_aip_reg_0009h;    // Absolute Address = 1809h

union rw_aip_reg_000Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscgpll_rd_skew                                                      : 3;        // [msb:lsb] = [2:0], val = 4
        unsigned char r_sscgpll_wr_skew                                                      : 3;        // [msb:lsb] = [2:0], val = 4
        unsigned char r_sscgpll_sel_fback_div2                                               : 1;        // val = 0
        unsigned char r_sscgpll_en_pll                                                       : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_000Ah xdata g_rw_aip_reg_000Ah;    // Absolute Address = 180Ah

union rw_aip_reg_000Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscgpll_sp_xtal                                                      : 1;        // val = 0
        unsigned char r_sscgpll_en_test_vct                                                  : 1;        // val = 0
        unsigned char r_sscgpll_pll_rsel                                                     : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_sscgpll_pll_rpole                                                    : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_sscgpll_en_pole                                                      : 1;        // val = 1
        unsigned char r_sscgpll_en_stb                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_000Bh xdata g_rw_aip_reg_000Bh;    // Absolute Address = 180Bh

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_000Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscgpll_en_cco_lek                                                   : 1;        // val = 0
        unsigned char r_sscgpll_en_cl                                                        : 1;        // val = 0
        unsigned char r_sscgpll_en_cco_psrr                                                  : 1;        // val = 1
        unsigned char r_sscgpll_sel_cco_ibn                                                  : 2;        // [msb:lsb] = [1:0], val = 2
        unsigned char r_sscgpll_sel_icco_bank                                                : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_sscgpll_isel_kvco_bits_0                                             : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_000Ch xdata g_rw_aip_reg_000Ch;    // Absolute Address = 180Ch

union rw_aip_reg_000Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscgpll_isel_kvco_bits_1                                             : 1;        // val = 0
        unsigned char r_sscgpll_rsel_kvco                                                    : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_sscgpll_isel_kvco_lek                                                : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_sscgpll_rsel_kvco_lek                                                : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_sscgpll_sel_cco_vref_bits_0                                          : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_000Dh xdata g_rw_aip_reg_000Dh;    // Absolute Address = 180Dh

union rw_aip_reg_000Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscgpll_sel_cco_vref_bits_1                                          : 1;        // val = 1
        unsigned char r_sscgpll_sel_cco_vlek                                                 : 2;        // [msb:lsb] = [1:0], val = 1
        unsigned char r_sscgpll_en_cco_bank                                                  : 1;        // val = 1
        unsigned char r_sscgpll_sel_op_inpath                                                : 1;        // val = 1
        unsigned char r_sscgpll_sel_vct_path                                                 : 1;        // val = 0
        unsigned char r_sscgpll_en_ivct_div2                                                 : 1;        // val = 0
        unsigned char r_sscgpll_rsel_vct_bits_0                                              : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_000Eh xdata g_rw_aip_reg_000Eh;    // Absolute Address = 180Eh

union rw_aip_reg_000Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscgpll_rsel_vct_bits_2_1                                            : 2;        // [msb:lsb] = [2:1], val = 2
        unsigned char r_sscgpll_en_vctstb                                                    : 1;        // val = 0
        unsigned char r_sscgpll_isel_vctstb                                                  : 2;        // [msb:lsb] = [1:0], val = 2
        unsigned char r_sscgpll_isel_vctop                                                   : 1;        // val = 1
        unsigned char r_sscgpll_isel_bankop                                                  : 1;        // val = 1
        unsigned char r_sscgpll_err_pll_bits_0                                               : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_000Fh xdata g_rw_aip_reg_000Fh;    // Absolute Address = 180Fh

union rw_aip_reg_0010h
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscgpll_err_pll_bits_3_1                                             : 3;        // [msb:lsb] = [3:1], val = 3
        unsigned char r_sscgpll_en_meas_ana_dc                                               : 1;        // val = 0
        unsigned char r_sscgpll_sel_ana_dc                                                   : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_sscgpll_en_meas_clk                                                  : 1;        // val = 0
        unsigned char r_sscg_mchip_slave_en                                                  : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0010h xdata g_rw_aip_reg_0010h;    // Absolute Address = 1810h

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_0011h
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_n_pre_normal                                                  : 6;        // [msb:lsb] = [5:0], val = 4
        unsigned char r_syspll_n_pre_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_aip_reg_0011h xdata g_rw_aip_reg_0011h;    // Absolute Address = 1811h

union rw_aip_reg_0012h
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_n_pre_bits_5_2                                                : 4;        // [msb:lsb] = [5:2], val = 0
        unsigned char r_syspll_en_ps1_div1                                                   : 1;        // val = 0
        unsigned char r_syspll_en_ps1_div2                                                   : 1;        // val = 1
        unsigned char r_syspll_n_ps1_normal_bits_1_0                                         : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_aip_reg_0012h xdata g_rw_aip_reg_0012h;    // Absolute Address = 1812h

union rw_aip_reg_0013h
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_n_ps1_normal_bits_5_2                                         : 4;        // [msb:lsb] = [5:2], val = 1
        unsigned char r_syspll_n_ps1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 5
    }bits;
};
extern volatile union rw_aip_reg_0013h xdata g_rw_aip_reg_0013h;    // Absolute Address = 1813h

union rw_aip_reg_0014h
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_n_ps1_bits_5_4                                                : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_syspll_en_ps2_div1                                                   : 1;        // val = 1
        unsigned char r_syspll_en_ps2_div2                                                   : 1;        // val = 1
        unsigned char r_syspll_n_ps2_normal_bits_3_0                                         : 4;        // [msb:lsb] = [3:0], val = 2
    }bits;
};
extern volatile union rw_aip_reg_0014h xdata g_rw_aip_reg_0014h;    // Absolute Address = 1814h

union rw_aip_reg_0015h
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_n_ps2_normal_bits_5_4                                         : 2;        // [msb:lsb] = [5:4], val = 0
        unsigned char r_syspll_n_ps2                                                         : 6;        // [msb:lsb] = [5:0], val = 2
    }bits;
};
extern volatile union rw_aip_reg_0015h xdata g_rw_aip_reg_0015h;    // Absolute Address = 1815h

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_0017h
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_p_normal_bits_8                                               : 1;        // val = 0
        unsigned char r_syspll_p_bits_6_0                                                    : 7;        // [msb:lsb] = [6:0], val = 29
    }bits;
};
extern volatile union rw_aip_reg_0017h xdata g_rw_aip_reg_0017h;    // Absolute Address = 1817h

union rw_aip_reg_0018h
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_p_bits_8_7                                                    : 2;        // [msb:lsb] = [8:7], val = 0
        unsigned char r_syspll_err_pll                                                       : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_syspll_sel_rst_pfd0                                                  : 1;        // val = 1
        unsigned char r_syspll_sel_icp_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0018h xdata g_rw_aip_reg_0018h;    // Absolute Address = 1818h

union rw_aip_reg_0019h
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_sel_icp_bits_4_1                                              : 4;        // [msb:lsb] = [4:1], val = 0
        unsigned char r_syspll_sel_w_psclk                                                   : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_syspll_sel_inv_sync_clk                                              : 1;        // val = 0
        unsigned char r_syspll_en_pll                                                        : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_0019h xdata g_rw_aip_reg_0019h;    // Absolute Address = 1819h

union rw_aip_reg_001Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_en_meas_clk                                                   : 1;        // val = 0
        unsigned char r_syspll_en_meas_ana_dc                                                : 1;        // val = 0
        unsigned char r_syspll_sel_ana_dc                                                    : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_syspll_sp_xtal                                                       : 1;        // val = 0
        unsigned char r_syspll_pll_rsel                                                      : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_syspll_pll_rpole_bits_0                                              : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_001Ah xdata g_rw_aip_reg_001Ah;    // Absolute Address = 181Ah

union rw_aip_reg_001Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_pll_rpole_bits_1                                              : 1;        // val = 1
        unsigned char r_syspll_en_pole                                                       : 1;        // val = 1
        unsigned char r_syspll_en_stb                                                        : 1;        // val = 0
        unsigned char r_syspll_twr                                                           : 1;        // val = 0
        unsigned char r_syspll_en_cco_lek                                                    : 1;        // val = 0
        unsigned char r_syspll_en_cl                                                         : 1;        // val = 0
        unsigned char r_syspll_en_cco_psrr                                                   : 1;        // val = 1
        unsigned char r_syspll_sel_cco_ibn_bits_0                                            : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_001Bh xdata g_rw_aip_reg_001Bh;    // Absolute Address = 181Bh

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_001Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_sel_cco_ibn_bits_1                                            : 1;        // val = 1
        unsigned char r_syspll_sel_icco_bank                                                 : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_syspll_isel_kvco                                                     : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_syspll_rsel_kvco                                                     : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_syspll_isel_kvco_lek_bits_0                                          : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_001Ch xdata g_rw_aip_reg_001Ch;    // Absolute Address = 181Ch

union rw_aip_reg_001Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_isel_kvco_lek_bits_1                                          : 1;        // val = 0
        unsigned char r_syspll_rsel_kvco_lek                                                 : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_syspll_sel_cco_vref                                                  : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_syspll_sel_cco_vlek                                                  : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_syspll_en_cco_bank                                                   : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_001Dh xdata g_rw_aip_reg_001Dh;    // Absolute Address = 181Dh

union rw_aip_reg_001Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_sel_op_inpath                                                 : 1;        // val = 1
        unsigned char r_syspll_en_psrr_vct                                                   : 1;        // val = 1
        unsigned char r_syspll_sel_vct_path                                                  : 1;        // val = 0
        unsigned char r_syspll_en_ivct_div2                                                  : 1;        // val = 0
        unsigned char r_syspll_rsel_vct                                                      : 3;        // [msb:lsb] = [2:0], val = 4
        unsigned char r_syspll_en_vctstb                                                     : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_001Eh xdata g_rw_aip_reg_001Eh;    // Absolute Address = 181Eh

union rw_aip_reg_001Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_isel_vctstb                                                   : 2;        // [msb:lsb] = [1:0], val = 2
        unsigned char r_syspll_isel_vctop                                                    : 1;        // val = 1
        unsigned char r_syspll_isel_bankop                                                   : 1;        // val = 1
        unsigned char r_syspll_sp_ext_ibcco                                                  : 1;        // val = 0
        unsigned char r_syspll_sel_fb_byp                                                    : 1;        // val = 0
        unsigned char r_syspll_sel_fb_sync                                                   : 1;        // val = 0
        unsigned char r_syspll_sel_rstref_fref                                               : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_001Fh xdata g_rw_aip_reg_001Fh;    // Absolute Address = 181Fh

union rw_aip_reg_0020h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_n_pre                                                             : 6;        // [msb:lsb] = [5:0], val = 8
        unsigned char r_tx_p_bits_1_0                                                        : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0020h xdata g_rw_aip_reg_0020h;    // Absolute Address = 1820h

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_0021h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_p_bits_8_2                                                        : 7;        // [msb:lsb] = [8:2], val = 16
        unsigned char r_tx_n_ps1_bits_0                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0021h xdata g_rw_aip_reg_0021h;    // Absolute Address = 1821h

union rw_aip_reg_0022h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_n_ps1_bits_5_1                                                    : 5;        // [msb:lsb] = [5:1], val = 4
        unsigned char r_tx_en_ps1_div1                                                       : 1;        // val = 1
        unsigned char r_tx_sel_sermd                                                         : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0022h xdata g_rw_aip_reg_0022h;    // Absolute Address = 1822h

union rw_aip_reg_0023h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_en_vcodiv                                                         : 1;        // val = 0
        unsigned char r_tx_sel_vcodiv                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_tx_en_mini                                                           : 1;        // val = 1
        unsigned char r_tx_sel_ldo_cp_cco                                                    : 3;        // [msb:lsb] = [2:0], val = 2
        unsigned char r_tx_sel_rst_pfd0                                                      : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_0023h xdata g_rw_aip_reg_0023h;    // Absolute Address = 1823h

union rw_aip_reg_0024h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_icp                                                           : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char r_tx_en_stb                                                            : 1;        // val = 0
        unsigned char r_tx_en_pole                                                           : 1;        // val = 0
        unsigned char r_tx_twr                                                               : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0024h xdata g_rw_aip_reg_0024h;    // Absolute Address = 1824h

union rw_aip_reg_0025h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_pll_rsel                                                          : 2;        // [msb:lsb] = [1:0], val = 3
        unsigned char r_tx_en_cco_bank                                                       : 1;        // val = 1
        unsigned char r_tx_rsel_vct                                                          : 3;        // [msb:lsb] = [2:0], val = 4
        unsigned char r_tx_sel_w_psclk                                                       : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_aip_reg_0025h xdata g_rw_aip_reg_0025h;    // Absolute Address = 1825h

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_0026h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_lock_div89                                                    : 1;        // val = 0
        unsigned char r_tx_err_pll                                                           : 4;        // [msb:lsb] = [3:0], val = 5
        unsigned char r_tx_frc_tx_hiz                                                        : 1;        // val = 1
        unsigned char r_tx_sel_swap_bits_1_0                                                 : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0026h xdata g_rw_aip_reg_0026h;    // Absolute Address = 1826h

union rw_aip_reg_0027h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_swap_bits_7_2                                                 : 6;        // [msb:lsb] = [7:2], val = 0
        unsigned char r_tx_sel_linkck_swap                                                   : 1;        // val = 0
        unsigned char r_tx_sel_rstref_fref_bits_0                                            : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_0027h xdata g_rw_aip_reg_0027h;    // Absolute Address = 1827h

union rw_aip_reg_0028h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_rstref_fref_bits_1                                            : 1;        // val = 1
        unsigned char r_tx_sel_rst_reflock                                                   : 1;        // val = 1
        unsigned char r_tx_sel_tx_reflock                                                    : 1;        // val = 1
        unsigned char r_tx_sel_di2c                                                          : 1;        // val = 0
        unsigned char r_tx_sp_inv_linkclk2x                                                  : 1;        // val = 0
        unsigned char r_tx_en_redcur                                                         : 1;        // val = 0
        unsigned char r_tx_cur_pecs                                                          : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_aip_reg_0028h xdata g_rw_aip_reg_0028h;    // Absolute Address = 1828h

union rw_aip_reg_0029h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_ldoclk                                                        : 3;        // [msb:lsb] = [2:0], val = 4
        unsigned char r_tx_sel_pmis                                                          : 2;        // [msb:lsb] = [1:0], val = 2
        unsigned char r_tx_inc_pera                                                          : 1;        // val = 1
        unsigned char r_tx_frc_en_phsw                                                       : 1;        // val = 0
        unsigned char r_tx_sel_vcmh                                                          : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0029h xdata g_rw_aip_reg_0029h;    // Absolute Address = 1829h

union rw_aip_reg_002Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_vcml                                                          : 1;        // val = 0
        unsigned char r_tx_sel_sh_0                                                          : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_tx_sel_sh_1_bits_0                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_002Ah xdata g_rw_aip_reg_002Ah;    // Absolute Address = 182Ah

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_002Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_sh_1_bits_5_1                                                 : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_tx_sel_sh_2_bits_2_0                                                 : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_002Bh xdata g_rw_aip_reg_002Bh;    // Absolute Address = 182Bh

union rw_aip_reg_002Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_sh_2_bits_5_3                                                 : 3;        // [msb:lsb] = [5:3], val = 0
        unsigned char r_tx_sel_sh_3_bits_4_0                                                 : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_002Ch xdata g_rw_aip_reg_002Ch;    // Absolute Address = 182Ch

union rw_aip_reg_002Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_sh_3_bits_5                                                   : 1;        // val = 0
        unsigned char r_tx_sel_sh_4                                                          : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_tx_sel_sh_5_bits_0                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_002Dh xdata g_rw_aip_reg_002Dh;    // Absolute Address = 182Dh

union rw_aip_reg_002Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_sh_5_bits_5_1                                                 : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_tx_sel_sh_6_bits_2_0                                                 : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_002Eh xdata g_rw_aip_reg_002Eh;    // Absolute Address = 182Eh

union rw_aip_reg_002Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_sh_6_bits_5_3                                                 : 3;        // [msb:lsb] = [5:3], val = 0
        unsigned char r_tx_sel_sh_7_bits_4_0                                                 : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_002Fh xdata g_rw_aip_reg_002Fh;    // Absolute Address = 182Fh

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_0030h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_sh_7_bits_5                                                   : 1;        // val = 0
        unsigned char r_tx_sel_mclkc_bits_6_0                                                : 7;        // [msb:lsb] = [6:0], val = 64
    }bits;
};
extern volatile union rw_aip_reg_0030h xdata g_rw_aip_reg_0030h;    // Absolute Address = 1830h

union rw_aip_reg_0031h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_mclkc_bits_7                                                  : 1;        // val = 1
        unsigned char r_tx_sph_mda                                                           : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char r_tx_sph_mck_a_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0031h xdata g_rw_aip_reg_0031h;    // Absolute Address = 1831h

union rw_aip_reg_0032h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sph_mck_a_bits_4_2                                                : 3;        // [msb:lsb] = [4:2], val = 2
        unsigned char r_tx_sph_mck_b                                                         : 5;        // [msb:lsb] = [4:0], val = 8
    }bits;
};
extern volatile union rw_aip_reg_0032h xdata g_rw_aip_reg_0032h;    // Absolute Address = 1832h

union rw_aip_reg_0033h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_en_meas_clk                                                       : 1;        // val = 0
        unsigned char r_tx_en_meas_bypint                                                    : 1;        // val = 0
        unsigned char r_tx_en_meas_bypclk                                                    : 1;        // val = 0
        unsigned char r_tx_sp_bypmeas                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_tx_sel_bypclk_ck_a                                                   : 1;        // val = 0
        unsigned char r_tx_sel_bypclk_ck_b                                                   : 1;        // val = 0
        unsigned char r_tx_en_meas_txplldc                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0033h xdata g_rw_aip_reg_0033h;    // Absolute Address = 1833h

union rw_aip_reg_0034h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_en_meas_txdc                                                      : 1;        // val = 0
        unsigned char r_tx_sel_txplldc                                                       : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_tx_sel_txdc                                                          : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_tx_en_test_vct                                                       : 1;        // val = 0
        unsigned char r_tx_set_rs_off                                                        : 1;        // val = 0
        unsigned char r_tx_swing_0_bits_0                                                    : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_0034h xdata g_rw_aip_reg_0034h;    // Absolute Address = 1834h

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_0035h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_swing_0_bits_3_1                                                  : 3;        // [msb:lsb] = [3:1], val = 1
        unsigned char r_tx_swing_1                                                           : 4;        // [msb:lsb] = [3:0], val = 3
        unsigned char r_tx_swing_2_bits_0                                                    : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_0035h xdata g_rw_aip_reg_0035h;    // Absolute Address = 1835h

union rw_aip_reg_0036h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_swing_2_bits_3_1                                                  : 3;        // [msb:lsb] = [3:1], val = 1
        unsigned char r_tx_swing_3                                                           : 4;        // [msb:lsb] = [3:0], val = 3
        unsigned char r_tx_swing_4_bits_0                                                    : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_0036h xdata g_rw_aip_reg_0036h;    // Absolute Address = 1836h

union rw_aip_reg_0037h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_swing_4_bits_3_1                                                  : 3;        // [msb:lsb] = [3:1], val = 1
        unsigned char r_tx_swing_5                                                           : 4;        // [msb:lsb] = [3:0], val = 3
        unsigned char r_tx_swing_6_bits_0                                                    : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_0037h xdata g_rw_aip_reg_0037h;    // Absolute Address = 1837h

union rw_aip_reg_0038h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_swing_6_bits_3_1                                                  : 3;        // [msb:lsb] = [3:1], val = 1
        unsigned char r_tx_swing_7                                                           : 4;        // [msb:lsb] = [3:0], val = 3
        unsigned char r_tx_en_pe_bits_0                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0038h xdata g_rw_aip_reg_0038h;    // Absolute Address = 1838h

union rw_aip_reg_0039h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_en_pe_bits_7_1                                                    : 7;        // [msb:lsb] = [7:1], val = 0
        unsigned char r_tx_pe_0_bits_0                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0039h xdata g_rw_aip_reg_0039h;    // Absolute Address = 1839h

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_003Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_pe_0_bits_1                                                       : 1;        // val = 0
        unsigned char r_tx_pe_1                                                              : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_tx_pe_2                                                              : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_tx_pe_3                                                              : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_tx_pe_4_bits_0                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_003Ah xdata g_rw_aip_reg_003Ah;    // Absolute Address = 183Ah

union rw_aip_reg_003Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_pe_4_bits_1                                                       : 1;        // val = 0
        unsigned char r_tx_pe_5                                                              : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_tx_pe_6                                                              : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_tx_pe_7                                                              : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_tx_en_pema_bits_0                                                    : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_003Bh xdata g_rw_aip_reg_003Bh;    // Absolute Address = 183Bh

union rw_aip_reg_003Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_en_pema_bits_7_1                                                  : 7;        // [msb:lsb] = [7:1], val = 127
        unsigned char r_tx_en_auto_ima                                                       : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_003Ch xdata g_rw_aip_reg_003Ch;    // Absolute Address = 183Ch

union rw_aip_reg_003Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_ipema                                                         : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char r_tx_sel_ipeps_0_bits_2_0                                              : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_003Dh xdata g_rw_aip_reg_003Dh;    // Absolute Address = 183Dh

union rw_aip_reg_003Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_ipeps_0_bits_5_3                                              : 3;        // [msb:lsb] = [5:3], val = 0
        unsigned char r_tx_sel_ipeps_1_bits_4_0                                              : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_003Eh xdata g_rw_aip_reg_003Eh;    // Absolute Address = 183Eh

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_003Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_ipeps_1_bits_5                                                : 1;        // val = 0
        unsigned char r_tx_sel_ipeps_2                                                       : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_tx_sel_ipeps_3_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_003Fh xdata g_rw_aip_reg_003Fh;    // Absolute Address = 183Fh

union rw_aip_reg_0040h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_ipeps_3_bits_5_1                                              : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_tx_sel_ipeps_4_bits_2_0                                              : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0040h xdata g_rw_aip_reg_0040h;    // Absolute Address = 1840h

union rw_aip_reg_0041h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_ipeps_4_bits_5_3                                              : 3;        // [msb:lsb] = [5:3], val = 0
        unsigned char r_tx_sel_ipeps_5_bits_4_0                                              : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0041h xdata g_rw_aip_reg_0041h;    // Absolute Address = 1841h

union rw_aip_reg_0042h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_ipeps_5_bits_5                                                : 1;        // val = 0
        unsigned char r_tx_sel_ipeps_6                                                       : 6;        // [msb:lsb] = [5:0], val = 0
        unsigned char r_tx_sel_ipeps_7_bits_0                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0042h xdata g_rw_aip_reg_0042h;    // Absolute Address = 1842h

union rw_aip_reg_0043h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_ipeps_7_bits_5_1                                              : 5;        // [msb:lsb] = [5:1], val = 0
        unsigned char r_tx_en_trimpe                                                         : 1;        // val = 0
        unsigned char r_tx_sel_rst                                                           : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0043h xdata g_rw_aip_reg_0043h;    // Absolute Address = 1843h

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_0044h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_auto_rst                                                          : 1;        // val = 0
        unsigned char r_tx_en_bist                                                           : 1;        // val = 0
        unsigned char r_tx_sp_xtal                                                           : 1;        // val = 0
        unsigned char r_tx_prbs_on                                                           : 1;        // val = 0
        unsigned char r_tx_pll_rpole                                                         : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_tx_en_phy                                                            : 1;        // val = 1
        unsigned char r_tx_pll_rstn                                                          : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_0044h xdata g_rw_aip_reg_0044h;    // Absolute Address = 1844h

union rw_aip_reg_0045h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_prechb                                                            : 1;        // val = 1
        unsigned char r_tx_rstn_lane                                                         : 1;        // val = 1
        unsigned char r_tx_en_txout_bits_5_0                                                 : 6;        // [msb:lsb] = [5:0], val = 63
    }bits;
};
extern volatile union rw_aip_reg_0045h xdata g_rw_aip_reg_0045h;    // Absolute Address = 1845h

union rw_aip_reg_0046h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_en_txout_bits_7_6                                                 : 2;        // [msb:lsb] = [7:6], val = 3
        unsigned char r_tx_en_lane_bits_5_0                                                  : 6;        // [msb:lsb] = [5:0], val = 63
    }bits;
};
extern volatile union rw_aip_reg_0046h xdata g_rw_aip_reg_0046h;    // Absolute Address = 1846h

union rw_aip_reg_0047h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_en_lane_bits_7_6                                                  : 2;        // [msb:lsb] = [7:6], val = 3
        unsigned char r_tx_sp_div4                                                           : 1;        // val = 1
        unsigned char r_tx_frc_mck_int                                                       : 1;        // val = 1
        unsigned char r_rx_en_sys_p1                                                         : 1;        // val = 1
        unsigned char r_rx_en_sys_p2                                                         : 1;        // val = 1
        unsigned char r_rx_en_ch_p1p2_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_aip_reg_0047h xdata g_rw_aip_reg_0047h;    // Absolute Address = 1847h

union rw_aip_reg_0048h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rx_en_ch_p1p2_bits_3_2                                               : 2;        // [msb:lsb] = [3:2], val = 3
        unsigned char r_rx_pnswap_p1                                                         : 1;        // val = 0
        unsigned char r_rx_pnswap_p2                                                         : 1;        // val = 0
        unsigned char r_rx_mirror_8bit_p1                                                    : 1;        // val = 0
        unsigned char r_rx_mirror_8bit_p2                                                    : 1;        // val = 0
        unsigned char r_rx_sp_lock                                                           : 1;        // val = 0
        unsigned char r_rx_sp_inv_dclk_p1p2                                                  : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0048h xdata g_rw_aip_reg_0048h;    // Absolute Address = 1848h

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_0049h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rx_cs_port                                                           : 2;        // [msb:lsb] = [1:0], val = 1
        unsigned char r_rx_eqa_p1p2                                                          : 2;        // [msb:lsb] = [1:0], val = 1
        unsigned char r_rx_eqp_p1                                                            : 4;        // [msb:lsb] = [3:0], val = 4
    }bits;
};
extern volatile union rw_aip_reg_0049h xdata g_rw_aip_reg_0049h;    // Absolute Address = 1849h

union rw_aip_reg_004Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_rx_eqp_p2                                                            : 4;        // [msb:lsb] = [3:0], val = 4
        unsigned char r_rx_eqr_p1                                                            : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_aip_reg_004Ah xdata g_rw_aip_reg_004Ah;    // Absolute Address = 184Ah

union rw_aip_reg_004Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_rx_eqr_p2                                                            : 4;        // [msb:lsb] = [3:0], val = 15
        unsigned char r_rx_eqdc_p1p2                                                         : 2;        // [msb:lsb] = [1:0], val = 1
        unsigned char r_rx_sel_icp                                                           : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_aip_reg_004Bh xdata g_rw_aip_reg_004Bh;    // Absolute Address = 184Bh

union rw_aip_reg_004Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_rx_sel_lpfc                                                          : 1;        // val = 0
        unsigned char r_rx_isel_kvco                                                         : 2;        // [msb:lsb] = [1:0], val = 1
        unsigned char r_rx_sp_en_dll                                                         : 1;        // val = 0
        unsigned char r_rx_en_dll_skew_p1p2                                                  : 1;        // val = 0
        unsigned char r_rx_skew_ck_p1_bits_2_0                                               : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_004Ch xdata g_rw_aip_reg_004Ch;    // Absolute Address = 184Ch

union rw_aip_reg_004Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_rx_skew_ck_p1_bits_3                                                 : 1;        // val = 0
        unsigned char r_rx_skew_ck_p2                                                        : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_rx_sel_iq_ldorx                                                      : 1;        // val = 0
        unsigned char r_rx_en_ldoclk                                                         : 1;        // val = 1
        unsigned char r_rx_en_pwrch_clk                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_004Dh xdata g_rw_aip_reg_004Dh;    // Absolute Address = 184Dh

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_004Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_rx_sp_vrefclk_vdd11                                                  : 1;        // val = 0
        unsigned char r_rx_frc_lock                                                          : 1;        // val = 0
        unsigned char r_rx_sp_prbs_rst                                                       : 1;        // val = 0
        unsigned char r_rx_en_meas_rxdc                                                      : 1;        // val = 0
        unsigned char r_rx_sel_rxdc                                                          : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_rx_en_phy                                                            : 1;        // val = 1
        unsigned char r_rx_rstn_bist                                                         : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_004Eh xdata g_rw_aip_reg_004Eh;    // Absolute Address = 184Eh

union rw_aip_reg_004Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_rx_en_prbschk                                                        : 1;        // val = 0
        unsigned char r_ldo_aip_sel_vref_ab                                                  : 3;        // [msb:lsb] = [2:0], val = 3
        unsigned char r_ldo_apr_en_capless                                                   : 1;        // val = 0
        unsigned char r_efuse_pwr_sel                                                        : 1;        // val = 1
        unsigned char r_osc_sel_mosc                                                         : 1;        // val = 0
        unsigned char r_sscgpll_manual_en                                                    : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_004Fh xdata g_rw_aip_reg_004Fh;    // Absolute Address = 184Fh

union rw_aip_reg_0050h
{
    unsigned char byte;
    struct
    {
        unsigned char r_central_en_esd_det_org                                               : 1;        // val = 0
        unsigned char r_central_esd_det_vot_1                                                : 1;        // val = 0
        unsigned char r_central_esd_det_vot_2                                                : 1;        // val = 0
        unsigned char r_central_esd_det_vot_3                                                : 1;        // val = 0
        unsigned char r_central_esd_det_vot_4                                                : 1;        // val = 0
        unsigned char r_iotest_en_test                                                       : 1;        // val = 0
        unsigned char r_iotest_sel_test_bits_1_0                                             : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0050h xdata g_rw_aip_reg_0050h;    // Absolute Address = 1850h

union rw_aip_reg_0051h
{
    unsigned char byte;
    struct
    {
        unsigned char r_iotest_sel_test_bits_3_2                                             : 2;        // [msb:lsb] = [3:2], val = 0
        unsigned char r_sscgpll_pll_lock_deb_sel                                             : 1;        // val = 0
        unsigned char r_sscgpll_pll_lock_deb_disable                                         : 1;        // val = 0
        unsigned char r_rx_act_p1_deb_sel                                                    : 1;        // val = 0
        unsigned char r_rx_act_p1_deb_disable                                                : 1;        // val = 0
        unsigned char r_rx_act_p2_deb_sel                                                    : 1;        // val = 0
        unsigned char r_rx_act_p2_deb_disable                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0051h xdata g_rw_aip_reg_0051h;    // Absolute Address = 1851h

union rw_aip_reg_0052h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rx_lock_p1_deb_sel                                                   : 1;        // val = 0
        unsigned char r_rx_lock_p1_deb_disable                                               : 1;        // val = 0
        unsigned char r_rx_lock_p2_deb_sel                                                   : 1;        // val = 0
        unsigned char r_rx_lock_p2_deb_disable                                               : 1;        // val = 0
        unsigned char r_spll_unlock_rst_en                                                   : 1;        // val = 1
        unsigned char r_sscgpll_unlock_rst_en                                                : 1;        // val = 1
        unsigned char r_tx_pll_unlock_rst_en                                                 : 1;        // val = 1
        unsigned char r_tx_unlock_flag_sscgpll_en                                            : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_0052h xdata g_rw_aip_reg_0052h;    // Absolute Address = 1852h

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_0053h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_unlock_flag_rx_act_p1_en                                          : 1;        // val = 1
        unsigned char r_tx_unlock_flag_rx_act_p2_en                                          : 1;        // val = 1
        unsigned char r_tx_unlock_flag_rx_lock_p1_en                                         : 1;        // val = 1
        unsigned char r_tx_unlock_flag_rx_lock_p2_en                                         : 1;        // val = 1
        unsigned char r_tx_sel_swap_xor80_en                                                 : 1;        // val = 0
        unsigned char r_tx_rst_sel                                                           : 1;        // val = 1
        unsigned char r_tx_reserve_0_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0053h xdata g_rw_aip_reg_0053h;    // Absolute Address = 1853h

union rw_aip_reg_0054h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_reserve_0_bits_7_2                                                : 6;        // [msb:lsb] = [7:2], val = 0
        unsigned char reserved1                                                              : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0054h xdata g_rw_aip_reg_0054h;    // Absolute Address = 1854h

union rw_aip_reg_0055h
{
    unsigned char byte;
    struct
    {
        unsigned char r_central_en_cap                                                       : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_central_sel_ldo_osc                                                  : 3;        // [msb:lsb] = [2:0], val = 4
        unsigned char r_central_reserve_bits_2_0                                             : 3;        // [msb:lsb] = [2:0], val = 7
    }bits;
};
extern volatile union rw_aip_reg_0055h xdata g_rw_aip_reg_0055h;    // Absolute Address = 1855h

union rw_aip_reg_0056h
{
    unsigned char byte;
    struct
    {
        unsigned char r_central_reserve_bits_7_3                                             : 5;        // [msb:lsb] = [7:3], val = 17
        unsigned char r_tx_en_bias                                                           : 1;        // val = 1
        unsigned char r_tx_en_ldo_cp                                                         : 1;        // val = 1
        unsigned char r_tx_en_pwrch_cp                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0056h xdata g_rw_aip_reg_0056h;    // Absolute Address = 1856h

union rw_aip_reg_0057h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_ldo_iq_cp                                                     : 1;        // val = 0
        unsigned char r_tx_en_ldo_cco                                                        : 1;        // val = 1
        unsigned char r_tx_en_pwrch_cco                                                      : 1;        // val = 0
        unsigned char r_tx_sel_ldo_iq_cco                                                    : 1;        // val = 0
        unsigned char r_tx_icp_upmis                                                         : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0057h xdata g_rw_aip_reg_0057h;    // Absolute Address = 1857h

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_0058h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_icp_dnmis                                                         : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_tx_en_lsw                                                            : 1;        // val = 1
        unsigned char r_tx_fra_sup                                                           : 1;        // val = 1
        unsigned char r_tx_en_psrr_vct                                                       : 1;        // val = 1
        unsigned char r_tx_en_divr                                                           : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0058h xdata g_rw_aip_reg_0058h;    // Absolute Address = 1858h

union rw_aip_reg_0059h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_sel_fb_byp                                                        : 1;        // val = 0
        unsigned char r_tx_sel_fb_sync                                                       : 1;        // val = 0
        unsigned char r_tx_sel_inv_sync_clk                                                  : 1;        // val = 0
        unsigned char r_tx_en_psdiv                                                          : 1;        // val = 1
        unsigned char r_tx_en_lkdet                                                          : 1;        // val = 1
        unsigned char r_tx_sel_wo_dly                                                        : 1;        // val = 0
        unsigned char r_tx_sel_ckout_reflock                                                 : 1;        // val = 0
        unsigned char r_tx_sel_wo_deglitch                                                   : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0059h xdata g_rw_aip_reg_0059h;    // Absolute Address = 1859h

union rw_aip_reg_005Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_di2c                                                              : 4;        // [msb:lsb] = [3:0], val = 5
        unsigned char r_tx_en_ldoclk                                                         : 1;        // val = 1
        unsigned char r_tx_en_pwrch_clk                                                      : 1;        // val = 0
        unsigned char r_tx_sel_iq_ldoclk                                                     : 1;        // val = 0
        unsigned char r_tx_en_extra_rd                                                       : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_005Dh xdata g_rw_aip_reg_005Dh;    // Absolute Address = 185Dh

union rw_aip_reg_005Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_frc_sel_link                                                      : 1;        // val = 1
        unsigned char r_tx_ext_sel_link                                                      : 3;        // [msb:lsb] = [2:0], val = 4
        unsigned char r_tx_reserve_1_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_005Eh xdata g_rw_aip_reg_005Eh;    // Absolute Address = 185Eh

union rw_aip_reg_005Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_reserve_1_bits_7_4                                                : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char r_tx_reserve_2_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_005Fh xdata g_rw_aip_reg_005Fh;    // Absolute Address = 185Fh

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_0060h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_reserve_2_bits_7_4                                                : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char r_tx_reserve_3_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0060h xdata g_rw_aip_reg_0060h;    // Absolute Address = 1860h

union rw_aip_reg_0061h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_reserve_3_bits_7_4                                                : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char r_tx_reserve_4_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0061h xdata g_rw_aip_reg_0061h;    // Absolute Address = 1861h

union rw_aip_reg_0062h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_reserve_4_bits_7_4                                                : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char r_tx_reserve_6_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 15
    }bits;
};
extern volatile union rw_aip_reg_0062h xdata g_rw_aip_reg_0062h;    // Absolute Address = 1862h

union rw_aip_reg_0063h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_reserve_6_bits_7_4                                                : 4;        // [msb:lsb] = [7:4], val = 15
        unsigned char r_tx_reserve_7_bits_3_0                                                : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0063h xdata g_rw_aip_reg_0063h;    // Absolute Address = 1863h

union rw_aip_reg_0064h
{
    unsigned char byte;
    struct
    {
        unsigned char r_tx_reserve_7_bits_7_4                                                : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char r_sscgpll_en_bias                                                      : 1;        // val = 1
        unsigned char r_sscgpll_en_ldo_cp                                                    : 1;        // val = 1
        unsigned char r_sscgpll_en_pwrch_cp                                                  : 1;        // val = 0
        unsigned char r_sscgpll_sel_ldo_iq_cp                                                : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0064h xdata g_rw_aip_reg_0064h;    // Absolute Address = 1864h

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_0065h
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscgpll_en_ldo_cco                                                   : 1;        // val = 0
        unsigned char r_sscgpll_en_pwrch_cco                                                 : 1;        // val = 0
        unsigned char r_sscgpll_sel_ldo_iq_cco                                               : 1;        // val = 0
        unsigned char r_sscgpll_sel_ckout_reflock                                            : 1;        // val = 0
        unsigned char r_sscgpll_sel_wo_dly                                                   : 1;        // val = 0
        unsigned char r_sscgpll_sel_wo_deglitch                                              : 1;        // val = 0
        unsigned char r_sscgpll_en_lsw                                                       : 1;        // val = 1
        unsigned char r_sscgpll_fra_sup                                                      : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_0065h xdata g_rw_aip_reg_0065h;    // Absolute Address = 1865h

union rw_aip_reg_0066h
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscgpll_sel_icp                                                      : 5;        // [msb:lsb] = [4:0], val = 0
        unsigned char r_sscgpll_icp_upmis_bits_2_0                                           : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0066h xdata g_rw_aip_reg_0066h;    // Absolute Address = 1866h

union rw_aip_reg_0067h
{
    unsigned char byte;
    struct
    {
        unsigned char r_sscgpll_icp_upmis_bits_3                                             : 1;        // val = 0
        unsigned char r_sscgpll_icp_dnmis                                                    : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_sscgpll_en_psrr_vct                                                  : 1;        // val = 1
        unsigned char r_sscgpll_en_divr                                                      : 1;        // val = 0
        unsigned char r_sscgpll_en_lkdet                                                     : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_0067h xdata g_rw_aip_reg_0067h;    // Absolute Address = 1867h

union rw_aip_reg_006Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_en_bias                                                       : 1;        // val = 1
        unsigned char r_syspll_en_txphy                                                      : 1;        // val = 0
        unsigned char r_syspll_en_mini                                                       : 1;        // val = 0
        unsigned char r_syspll_en_meas_bypint                                                : 1;        // val = 0
        unsigned char r_syspll_en_test_vct                                                   : 1;        // val = 0
        unsigned char r_syspll_en_ldo_cp                                                     : 1;        // val = 1
        unsigned char r_syspll_en_pwrch_cp                                                   : 1;        // val = 0
        unsigned char r_syspll_sel_ldo_iq_cp                                                 : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_006Bh xdata g_rw_aip_reg_006Bh;    // Absolute Address = 186Bh

union rw_aip_reg_006Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_en_ldo_cco                                                    : 1;        // val = 1
        unsigned char r_syspll_en_pwrch_cco                                                  : 1;        // val = 0
        unsigned char r_syspll_sel_ldo_iq_cco                                                : 1;        // val = 0
        unsigned char r_syspll_icp_upmis                                                     : 4;        // [msb:lsb] = [3:0], val = 0
        unsigned char r_syspll_icp_dnmis_bits_0                                              : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_006Ch xdata g_rw_aip_reg_006Ch;    // Absolute Address = 186Ch

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_006Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_icp_dnmis_bits_3_1                                            : 3;        // [msb:lsb] = [3:1], val = 0
        unsigned char r_syspll_en_lsw                                                        : 1;        // val = 1
        unsigned char r_syspll_fra_sup                                                       : 1;        // val = 1
        unsigned char r_syspll_sel_ckout_reflock                                             : 1;        // val = 0
        unsigned char r_syspll_sel_wo_deglitch                                               : 1;        // val = 0
        unsigned char r_syspll_sel_wo_dly                                                    : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_006Dh xdata g_rw_aip_reg_006Dh;    // Absolute Address = 186Dh

union rw_aip_reg_006Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_en_divr                                                       : 1;        // val = 0
        unsigned char r_syspll_en_lkdet                                                      : 1;        // val = 0
        unsigned char r_syspll_reserve_0_bits_5_0                                            : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_006Eh xdata g_rw_aip_reg_006Eh;    // Absolute Address = 186Eh

union rw_aip_reg_006Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_reserve_0_bits_7_6                                            : 2;        // [msb:lsb] = [7:6], val = 0
        unsigned char r_syspll_reserve_1_bits_5_0                                            : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_006Fh xdata g_rw_aip_reg_006Fh;    // Absolute Address = 186Fh

union rw_aip_reg_0070h
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_reserve_1_bits_7_6                                            : 2;        // [msb:lsb] = [7:6], val = 0
        unsigned char r_syspll_reserve_2_bits_5_0                                            : 6;        // [msb:lsb] = [5:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0070h xdata g_rw_aip_reg_0070h;    // Absolute Address = 1870h

union rw_aip_reg_0071h
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_reserve_2_bits_7_6                                            : 2;        // [msb:lsb] = [7:6], val = 0
        unsigned char r_ldo_aip_sel_ldo_iq_a                                                 : 1;        // val = 0
        unsigned char r_ldo_aip_sel_ldo_iq_b                                                 : 1;        // val = 0
        unsigned char r_ldo_apr_sel_ldo_iq                                                   : 1;        // val = 1
        unsigned char r_rx_sp_vreffs_ugb                                                     : 1;        // val = 0
        unsigned char r_rx_cs_fscmp                                                          : 1;        // val = 0
        unsigned char r_rx_sp_mux_bias                                                       : 1;        // val = 1
    }bits;
};
extern volatile union rw_aip_reg_0071h xdata g_rw_aip_reg_0071h;    // Absolute Address = 1871h

//------------------------------------------------------------------------------------------------------------------------
union rw_aip_reg_0072h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rx_sel_iq_ldoclk                                                     : 1;        // val = 0
        unsigned char r_rx_en_pre_3ma                                                        : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_rx_reserve_0_bits_4_0                                                : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0072h xdata g_rw_aip_reg_0072h;    // Absolute Address = 1872h

union rw_aip_reg_0073h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rx_reserve_0_bits_7_5                                                : 3;        // [msb:lsb] = [7:5], val = 0
        unsigned char r_rx_reserve_1_bits_4_0                                                : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_aip_reg_0073h xdata g_rw_aip_reg_0073h;    // Absolute Address = 1873h

union rw_aip_reg_0074h
{
    unsigned char byte;
    struct
    {
        unsigned char r_rx_reserve_1_bits_7_5                                                : 3;        // [msb:lsb] = [7:5], val = 0
        unsigned char r_osc_en_ldo                                                           : 1;        // val = 1
        unsigned char r_osc_en_pwrch                                                         : 1;        // val = 0
        unsigned char r_osc_sel_iq_ldo                                                       : 1;        // val = 0
        unsigned char r_syspll_reg_sw_en                                                     : 1;        // val = 0
        unsigned char r_syspll_mode_chg                                                      : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0074h xdata g_rw_aip_reg_0074h;    // Absolute Address = 1874h

union rw_aip_reg_0075h
{
    unsigned char byte;
    struct
    {
        unsigned char r_syspll_pll_rstn                                                      : 1;        // val = 0
        unsigned char r_syspll_prechb                                                        : 1;        // val = 0
        unsigned char r_sscgpll_reg_sw_en                                                    : 1;        // val = 0
        unsigned char r_sscgpll_mode_chg                                                     : 1;        // val = 0
        unsigned char r_sscgpll_pll_rstn                                                     : 1;        // val = 0
        unsigned char r_sscgpll_prechb                                                       : 1;        // val = 0
        unsigned char r_esd_mask_syspll_en                                                   : 1;        // val = 0
        unsigned char r_esd_mask_sscgpll_en                                                  : 1;        // val = 0
    }bits;
};
extern volatile union rw_aip_reg_0075h xdata g_rw_aip_reg_0075h;    // Absolute Address = 1875h

union rw_aip_reg_0076h
{
    unsigned char byte;
    struct
    {
        unsigned char r_esd_mask_tx_en                                                       : 1;        // val = 0
        unsigned char r_esd_mask_edge_sel                                                    : 1;        // val = 0
        unsigned char reserved_5_by_tool                                                     : 1;
        unsigned char reserved_4_by_tool                                                     : 1;
        unsigned char reserved_3_by_tool                                                     : 1;
        unsigned char reserved_2_by_tool                                                     : 1;
        unsigned char reserved_1_by_tool                                                     : 1;
        unsigned char reserved_0_by_tool                                                     : 1;
    }bits;
};
extern volatile union rw_aip_reg_0076h xdata g_rw_aip_reg_0076h;    // Absolute Address = 1876h

//------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_aip_reg_0007h_sscg2_r_mod_fq_manual2;                                 // [msb:lsb] = [9:2], val = 100, 	Absolute Address = 1807h
extern volatile uint8_t xdata g_rw_aip_reg_0008h_sscg2_r_mod_md_manual2;                                 // [msb:lsb] = [7:0], val = 26, 	Absolute Address = 1808h
extern volatile uint8_t xdata g_rw_aip_reg_0016h_syspll_p_normal;                                        // [msb:lsb] = [7:0], val = 29, 	Absolute Address = 1816h
extern volatile uint8_t xdata g_rw_aip_reg_005Ah_tx_pll_reserve_0;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 185Ah
extern volatile uint8_t xdata g_rw_aip_reg_005Bh_tx_pll_reserve_1;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 185Bh

extern volatile uint8_t xdata g_rw_aip_reg_005Ch_tx_pll_reserve_2;                                       // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 185Ch
extern volatile uint8_t xdata g_rw_aip_reg_0068h_sscgpll_reserve_0;                                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 1868h
extern volatile uint8_t xdata g_rw_aip_reg_0069h_sscgpll_reserve_1;                                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 1869h
extern volatile uint8_t xdata g_rw_aip_reg_006Ah_sscgpll_reserve_2;                                      // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 186Ah

#endif