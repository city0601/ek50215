#ifndef _LXLOD_H_
#define _LXLOD_H_
#include "reg_include.h"

union rw_lxlod_0000h
{
    unsigned char byte;
    struct
    {
        unsigned char r_cscd_en                                                              : 1;        // val = 0
        unsigned char r_cscd_last_pix                                                        : 1;        // val = 0
        unsigned char r_cscd_tx_edge_sel                                                     : 1;        // val = 0
        unsigned char r_cscd_sel                                                             : 5;        // [msb:lsb] = [4:0], val = 0
    }bits;
};
extern volatile union rw_lxlod_0000h xdata g_rw_lxlod_0000h;    // Absolute Address = 5000h

union rw_lxlod_0001h
{
    unsigned char byte;
    struct
    {
        unsigned char r_fst_line_en                                                          : 1;        // val = 0
        unsigned char r_fst_p_data_bits_6_0                                                  : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_lxlod_0001h xdata g_rw_lxlod_0001h;    // Absolute Address = 5001h

union rw_lxlod_0002h
{
    unsigned char byte;
    struct
    {
        unsigned char r_fst_p_data_bits_11_7                                                 : 5;        // [msb:lsb] = [11:7], val = 0
        unsigned char r_line_map_sel                                                         : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_lxlod_0002h xdata g_rw_lxlod_0002h;    // Absolute Address = 5002h

union rw_lxlod_0003h
{
    unsigned char byte;
    struct
    {
        unsigned char r_line_map_prog_en                                                     : 1;        // val = 0
        unsigned char r_line_map_prog_bits_6_0                                               : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_lxlod_0003h xdata g_rw_lxlod_0003h;    // Absolute Address = 5003h

union rw_lxlod_0004h
{
    unsigned char byte;
    struct
    {
        unsigned char r_line_map_prog_bits_7                                                 : 1;        // val = 0
        unsigned char r_def_gate_en                                                          : 1;        // val = 0
        unsigned char r_def_gate_val                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_line_cnt_rst_sel                                                     : 1;        // val = 0
        unsigned char r_r1_map0_pos_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_lxlod_0004h xdata g_rw_lxlod_0004h;    // Absolute Address = 5004h

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_0005h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map0_pos_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_g1_map0_pos_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 2
    }bits;
};
extern volatile union rw_lxlod_0005h xdata g_rw_lxlod_0005h;    // Absolute Address = 5005h

union rw_lxlod_0006h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map0_pos_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_b1_map0_pos_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 35
    }bits;
};
extern volatile union rw_lxlod_0006h xdata g_rw_lxlod_0006h;    // Absolute Address = 5006h

union rw_lxlod_0007h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map1_pos_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 33
        unsigned char r_g1_map1_pos_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_lxlod_0007h xdata g_rw_lxlod_0007h;    // Absolute Address = 5007h

union rw_lxlod_0008h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map1_pos_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_b1_map1_pos_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_0008h xdata g_rw_lxlod_0008h;    // Absolute Address = 5008h

union rw_lxlod_0009h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map1_pos_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_r1_map2_pos_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 33
    }bits;
};
extern volatile union rw_lxlod_0009h xdata g_rw_lxlod_0009h;    // Absolute Address = 5009h

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_000Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map2_pos_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 34
        unsigned char r_b1_map2_pos_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_000Ah xdata g_rw_lxlod_000Ah;    // Absolute Address = 500Ah

union rw_lxlod_000Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map2_pos_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_r1_map3_pos_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_lxlod_000Bh xdata g_rw_lxlod_000Bh;    // Absolute Address = 500Bh

union rw_lxlod_000Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map3_pos_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_g1_map3_pos_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 34
    }bits;
};
extern volatile union rw_lxlod_000Ch xdata g_rw_lxlod_000Ch;    // Absolute Address = 500Ch

union rw_lxlod_000Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map3_pos_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 35
        unsigned char r_r1_map4_pos_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_lxlod_000Dh xdata g_rw_lxlod_000Dh;    // Absolute Address = 500Dh

union rw_lxlod_000Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map4_pos_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_g1_map4_pos_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 2
    }bits;
};
extern volatile union rw_lxlod_000Eh xdata g_rw_lxlod_000Eh;    // Absolute Address = 500Eh

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_000Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map4_pos_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_b1_map4_pos_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 35
    }bits;
};
extern volatile union rw_lxlod_000Fh xdata g_rw_lxlod_000Fh;    // Absolute Address = 500Fh

union rw_lxlod_0010h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map5_pos_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 33
        unsigned char r_g1_map5_pos_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_lxlod_0010h xdata g_rw_lxlod_0010h;    // Absolute Address = 5010h

union rw_lxlod_0011h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map5_pos_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_b1_map5_pos_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_0011h xdata g_rw_lxlod_0011h;    // Absolute Address = 5011h

union rw_lxlod_0012h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map5_pos_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_r1_map6_pos_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 33
    }bits;
};
extern volatile union rw_lxlod_0012h xdata g_rw_lxlod_0012h;    // Absolute Address = 5012h

union rw_lxlod_0013h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map6_pos_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 34
        unsigned char r_b1_map6_pos_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_0013h xdata g_rw_lxlod_0013h;    // Absolute Address = 5013h

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_0014h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map6_pos_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_r1_map7_pos_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_lxlod_0014h xdata g_rw_lxlod_0014h;    // Absolute Address = 5014h

union rw_lxlod_0015h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map7_pos_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_g1_map7_pos_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 34
    }bits;
};
extern volatile union rw_lxlod_0015h xdata g_rw_lxlod_0015h;    // Absolute Address = 5015h

union rw_lxlod_0016h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map7_pos_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 35
        unsigned char r_r1_map0_neg_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_lxlod_0016h xdata g_rw_lxlod_0016h;    // Absolute Address = 5016h

union rw_lxlod_0017h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map0_neg_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_g1_map0_neg_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 2
    }bits;
};
extern volatile union rw_lxlod_0017h xdata g_rw_lxlod_0017h;    // Absolute Address = 5017h

union rw_lxlod_0018h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map0_neg_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_b1_map0_neg_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 35
    }bits;
};
extern volatile union rw_lxlod_0018h xdata g_rw_lxlod_0018h;    // Absolute Address = 5018h

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_0019h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map1_neg_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 33
        unsigned char r_g1_map1_neg_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_lxlod_0019h xdata g_rw_lxlod_0019h;    // Absolute Address = 5019h

union rw_lxlod_001Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map1_neg_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_b1_map1_neg_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_001Ah xdata g_rw_lxlod_001Ah;    // Absolute Address = 501Ah

union rw_lxlod_001Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map1_neg_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_r1_map2_neg_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 33
    }bits;
};
extern volatile union rw_lxlod_001Bh xdata g_rw_lxlod_001Bh;    // Absolute Address = 501Bh

union rw_lxlod_001Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map2_neg_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 34
        unsigned char r_b1_map2_neg_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_001Ch xdata g_rw_lxlod_001Ch;    // Absolute Address = 501Ch

union rw_lxlod_001Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map2_neg_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_r1_map3_neg_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_lxlod_001Dh xdata g_rw_lxlod_001Dh;    // Absolute Address = 501Dh

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_001Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map3_neg_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_g1_map3_neg_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 34
    }bits;
};
extern volatile union rw_lxlod_001Eh xdata g_rw_lxlod_001Eh;    // Absolute Address = 501Eh

union rw_lxlod_001Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map3_neg_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 35
        unsigned char r_r1_map4_neg_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_lxlod_001Fh xdata g_rw_lxlod_001Fh;    // Absolute Address = 501Fh

union rw_lxlod_0020h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map4_neg_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_g1_map4_neg_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 2
    }bits;
};
extern volatile union rw_lxlod_0020h xdata g_rw_lxlod_0020h;    // Absolute Address = 5020h

union rw_lxlod_0021h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map4_neg_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_b1_map4_neg_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 35
    }bits;
};
extern volatile union rw_lxlod_0021h xdata g_rw_lxlod_0021h;    // Absolute Address = 5021h

union rw_lxlod_0022h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map5_neg_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 33
        unsigned char r_g1_map5_neg_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_lxlod_0022h xdata g_rw_lxlod_0022h;    // Absolute Address = 5022h

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_0023h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map5_neg_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_b1_map5_neg_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_0023h xdata g_rw_lxlod_0023h;    // Absolute Address = 5023h

union rw_lxlod_0024h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map5_neg_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_r1_map6_neg_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 33
    }bits;
};
extern volatile union rw_lxlod_0024h xdata g_rw_lxlod_0024h;    // Absolute Address = 5024h

union rw_lxlod_0025h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map6_neg_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 34
        unsigned char r_b1_map6_neg_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_0025h xdata g_rw_lxlod_0025h;    // Absolute Address = 5025h

union rw_lxlod_0026h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map6_neg_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_r1_map7_neg_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_lxlod_0026h xdata g_rw_lxlod_0026h;    // Absolute Address = 5026h

union rw_lxlod_0027h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map7_neg_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_g1_map7_neg_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 34
    }bits;
};
extern volatile union rw_lxlod_0027h xdata g_rw_lxlod_0027h;    // Absolute Address = 5027h

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_0028h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map7_neg_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 35
        unsigned char r_r1_map0_thd_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_lxlod_0028h xdata g_rw_lxlod_0028h;    // Absolute Address = 5028h

union rw_lxlod_0029h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map0_thd_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_g1_map0_thd_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 2
    }bits;
};
extern volatile union rw_lxlod_0029h xdata g_rw_lxlod_0029h;    // Absolute Address = 5029h

union rw_lxlod_002Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map0_thd_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_b1_map0_thd_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 35
    }bits;
};
extern volatile union rw_lxlod_002Ah xdata g_rw_lxlod_002Ah;    // Absolute Address = 502Ah

union rw_lxlod_002Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map1_thd_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 33
        unsigned char r_g1_map1_thd_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_lxlod_002Bh xdata g_rw_lxlod_002Bh;    // Absolute Address = 502Bh

union rw_lxlod_002Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map1_thd_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_b1_map1_thd_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_002Ch xdata g_rw_lxlod_002Ch;    // Absolute Address = 502Ch

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_002Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map1_thd_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_r1_map2_thd_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 33
    }bits;
};
extern volatile union rw_lxlod_002Dh xdata g_rw_lxlod_002Dh;    // Absolute Address = 502Dh

union rw_lxlod_002Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map2_thd_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 34
        unsigned char r_b1_map2_thd_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_002Eh xdata g_rw_lxlod_002Eh;    // Absolute Address = 502Eh

union rw_lxlod_002Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map2_thd_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_r1_map3_thd_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_lxlod_002Fh xdata g_rw_lxlod_002Fh;    // Absolute Address = 502Fh

union rw_lxlod_0030h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map3_thd_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_g1_map3_thd_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 34
    }bits;
};
extern volatile union rw_lxlod_0030h xdata g_rw_lxlod_0030h;    // Absolute Address = 5030h

union rw_lxlod_0031h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map3_thd_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 35
        unsigned char r_r1_map4_thd_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_lxlod_0031h xdata g_rw_lxlod_0031h;    // Absolute Address = 5031h

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_0032h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map4_thd_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_g1_map4_thd_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 2
    }bits;
};
extern volatile union rw_lxlod_0032h xdata g_rw_lxlod_0032h;    // Absolute Address = 5032h

union rw_lxlod_0033h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map4_thd_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_b1_map4_thd_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 35
    }bits;
};
extern volatile union rw_lxlod_0033h xdata g_rw_lxlod_0033h;    // Absolute Address = 5033h

union rw_lxlod_0034h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map5_thd_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 33
        unsigned char r_g1_map5_thd_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_lxlod_0034h xdata g_rw_lxlod_0034h;    // Absolute Address = 5034h

union rw_lxlod_0035h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map5_thd_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_b1_map5_thd_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_0035h xdata g_rw_lxlod_0035h;    // Absolute Address = 5035h

union rw_lxlod_0036h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map5_thd_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_r1_map6_thd_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 33
    }bits;
};
extern volatile union rw_lxlod_0036h xdata g_rw_lxlod_0036h;    // Absolute Address = 5036h

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_0037h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map6_thd_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 34
        unsigned char r_b1_map6_thd_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_0037h xdata g_rw_lxlod_0037h;    // Absolute Address = 5037h

union rw_lxlod_0038h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map6_thd_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_r1_map7_thd_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_lxlod_0038h xdata g_rw_lxlod_0038h;    // Absolute Address = 5038h

union rw_lxlod_0039h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map7_thd_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_g1_map7_thd_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 34
    }bits;
};
extern volatile union rw_lxlod_0039h xdata g_rw_lxlod_0039h;    // Absolute Address = 5039h

union rw_lxlod_003Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map7_thd_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 35
        unsigned char r_r1_map0_fth_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_lxlod_003Ah xdata g_rw_lxlod_003Ah;    // Absolute Address = 503Ah

union rw_lxlod_003Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map0_fth_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_g1_map0_fth_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 2
    }bits;
};
extern volatile union rw_lxlod_003Bh xdata g_rw_lxlod_003Bh;    // Absolute Address = 503Bh

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_003Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map0_fth_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_b1_map0_fth_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 35
    }bits;
};
extern volatile union rw_lxlod_003Ch xdata g_rw_lxlod_003Ch;    // Absolute Address = 503Ch

union rw_lxlod_003Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map1_fth_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 33
        unsigned char r_g1_map1_fth_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_lxlod_003Dh xdata g_rw_lxlod_003Dh;    // Absolute Address = 503Dh

union rw_lxlod_003Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map1_fth_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_b1_map1_fth_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_003Eh xdata g_rw_lxlod_003Eh;    // Absolute Address = 503Eh

union rw_lxlod_003Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map1_fth_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_r1_map2_fth_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 33
    }bits;
};
extern volatile union rw_lxlod_003Fh xdata g_rw_lxlod_003Fh;    // Absolute Address = 503Fh

union rw_lxlod_0040h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map2_fth_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 34
        unsigned char r_b1_map2_fth_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_0040h xdata g_rw_lxlod_0040h;    // Absolute Address = 5040h

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_0041h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map2_fth_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_r1_map3_fth_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_lxlod_0041h xdata g_rw_lxlod_0041h;    // Absolute Address = 5041h

union rw_lxlod_0042h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map3_fth_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_g1_map3_fth_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 34
    }bits;
};
extern volatile union rw_lxlod_0042h xdata g_rw_lxlod_0042h;    // Absolute Address = 5042h

union rw_lxlod_0043h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map3_fth_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 35
        unsigned char r_r1_map4_fth_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_lxlod_0043h xdata g_rw_lxlod_0043h;    // Absolute Address = 5043h

union rw_lxlod_0044h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map4_fth_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_g1_map4_fth_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 2
    }bits;
};
extern volatile union rw_lxlod_0044h xdata g_rw_lxlod_0044h;    // Absolute Address = 5044h

union rw_lxlod_0045h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map4_fth_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_b1_map4_fth_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 35
    }bits;
};
extern volatile union rw_lxlod_0045h xdata g_rw_lxlod_0045h;    // Absolute Address = 5045h

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_0046h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map5_fth_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 33
        unsigned char r_g1_map5_fth_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 2
    }bits;
};
extern volatile union rw_lxlod_0046h xdata g_rw_lxlod_0046h;    // Absolute Address = 5046h

union rw_lxlod_0047h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map5_fth_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_b1_map5_fth_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_0047h xdata g_rw_lxlod_0047h;    // Absolute Address = 5047h

union rw_lxlod_0048h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map5_fth_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_r1_map6_fth_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 33
    }bits;
};
extern volatile union rw_lxlod_0048h xdata g_rw_lxlod_0048h;    // Absolute Address = 5048h

union rw_lxlod_0049h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_map6_fth_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 34
        unsigned char r_b1_map6_fth_ns_bits_1_0                                              : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_0049h xdata g_rw_lxlod_0049h;    // Absolute Address = 5049h

union rw_lxlod_004Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map6_fth_ns_bits_5_2                                              : 4;        // [msb:lsb] = [5:2], val = 8
        unsigned char r_r1_map7_fth_ns_bits_3_0                                              : 4;        // [msb:lsb] = [3:0], val = 1
    }bits;
};
extern volatile union rw_lxlod_004Ah xdata g_rw_lxlod_004Ah;    // Absolute Address = 504Ah

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_004Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_map7_fth_ns_bits_5_4                                              : 2;        // [msb:lsb] = [5:4], val = 2
        unsigned char r_g1_map7_fth_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 34
    }bits;
};
extern volatile union rw_lxlod_004Bh xdata g_rw_lxlod_004Bh;    // Absolute Address = 504Bh

union rw_lxlod_004Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_map7_fth_ns                                                       : 6;        // [msb:lsb] = [5:0], val = 35
        unsigned char r_lxl_linear_en                                                        : 1;        // val = 0
        unsigned char r_h_block_size_bits_0                                                  : 1;        // val = 0
    }bits;
};
extern volatile union rw_lxlod_004Ch xdata g_rw_lxlod_004Ch;    // Absolute Address = 504Ch

union rw_lxlod_004Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_h_block_size_bits_9                                                  : 1;        // val = 0
        unsigned char r_v_block_size_bits_6_0                                                : 7;        // [msb:lsb] = [6:0], val = 2
    }bits;
};
extern volatile union rw_lxlod_004Eh xdata g_rw_lxlod_004Eh;    // Absolute Address = 504Eh

union rw_lxlod_004Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_v_block_size_bits_8_7                                                : 2;        // [msb:lsb] = [8:7], val = 0
        unsigned char r_lxl_gain_en                                                          : 1;        // val = 1
        unsigned char r_lxl_offset_en                                                        : 1;        // val = 1
        unsigned char r_lxl_gain2_en                                                         : 1;        // val = 0
        unsigned char r_smooth_en                                                            : 1;        // val = 1
        unsigned char r_smooth_slop_bits_1_0                                                 : 2;        // [msb:lsb] = [1:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_004Fh xdata g_rw_lxlod_004Fh;    // Absolute Address = 504Fh

union rw_lxlod_0050h
{
    unsigned char byte;
    struct
    {
        unsigned char r_smooth_slop_bits_3_2                                                 : 2;        // [msb:lsb] = [3:2], val = 1
        unsigned char r_smooth_slop_div_bits_5_0                                             : 6;        // [msb:lsb] = [5:0], val = 4
    }bits;
};
extern volatile union rw_lxlod_0050h xdata g_rw_lxlod_0050h;    // Absolute Address = 5050h

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_0051h
{
    unsigned char byte;
    struct
    {
        unsigned char r_smooth_slop_div_bits_8_6                                             : 3;        // [msb:lsb] = [8:6], val = 0
        unsigned char r_zero_cor_en                                                          : 1;        // val = 1
        unsigned char r_x_div_bits_3_0                                                       : 4;        // [msb:lsb] = [3:0], val = 3
    }bits;
};
extern volatile union rw_lxlod_0051h xdata g_rw_lxlod_0051h;    // Absolute Address = 5051h

union rw_lxlod_0054h
{
    unsigned char byte;
    struct
    {
        unsigned char r_y_div_bits_11_8                                                      : 4;        // [msb:lsb] = [11:8], val = 15
        unsigned char r_r1_l1_cscd_en                                                        : 1;        // val = 0
        unsigned char r_r1_l2_cscd_en                                                        : 1;        // val = 0
        unsigned char r_r1_l3_cscd_en                                                        : 1;        // val = 0
        unsigned char r_r1_l4_cscd_en                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_lxlod_0054h xdata g_rw_lxlod_0054h;    // Absolute Address = 5054h

union rw_lxlod_0055h
{
    unsigned char byte;
    struct
    {
        unsigned char r_r1_l5_cscd_en                                                        : 1;        // val = 0
        unsigned char r_r1_l6_cscd_en                                                        : 1;        // val = 0
        unsigned char r_r1_l7_cscd_en                                                        : 1;        // val = 0
        unsigned char r_r1_l8_cscd_en                                                        : 1;        // val = 0
        unsigned char r_g1_l1_cscd_en                                                        : 1;        // val = 0
        unsigned char r_g1_l2_cscd_en                                                        : 1;        // val = 0
        unsigned char r_g1_l3_cscd_en                                                        : 1;        // val = 0
        unsigned char r_g1_l4_cscd_en                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_lxlod_0055h xdata g_rw_lxlod_0055h;    // Absolute Address = 5055h

union rw_lxlod_0056h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_l5_cscd_en                                                        : 1;        // val = 0
        unsigned char r_g1_l6_cscd_en                                                        : 1;        // val = 0
        unsigned char r_g1_l7_cscd_en                                                        : 1;        // val = 0
        unsigned char r_g1_l8_cscd_en                                                        : 1;        // val = 0
        unsigned char r_b1_l1_cscd_en                                                        : 1;        // val = 0
        unsigned char r_b1_l2_cscd_en                                                        : 1;        // val = 0
        unsigned char r_b1_l3_cscd_en                                                        : 1;        // val = 0
        unsigned char r_b1_l4_cscd_en                                                        : 1;        // val = 0
    }bits;
};
extern volatile union rw_lxlod_0056h xdata g_rw_lxlod_0056h;    // Absolute Address = 5056h

union rw_lxlod_0057h
{
    unsigned char byte;
    struct
    {
        unsigned char r_b1_l5_cscd_en                                                        : 1;        // val = 0
        unsigned char r_b1_l6_cscd_en                                                        : 1;        // val = 0
        unsigned char r_b1_l7_cscd_en                                                        : 1;        // val = 0
        unsigned char r_b1_l8_cscd_en                                                        : 1;        // val = 0
        unsigned char r_r1_gray_dbg_max                                                      : 1;        // val = 0
        unsigned char r_r1_gray_dbg_en                                                       : 1;        // val = 0
        unsigned char r_r1_gray_latch_sel                                                    : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_lxlod_0057h xdata g_rw_lxlod_0057h;    // Absolute Address = 5057h

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_0058h
{
    unsigned char byte;
    struct
    {
        unsigned char r_g1_gray_dbg_max                                                      : 1;        // val = 0
        unsigned char r_g1_gray_dbg_en                                                       : 1;        // val = 0
        unsigned char r_g1_gray_latch_sel                                                    : 2;        // [msb:lsb] = [1:0], val = 0
        unsigned char r_b1_gray_dbg_max                                                      : 1;        // val = 0
        unsigned char r_b1_gray_dbg_en                                                       : 1;        // val = 0
        unsigned char r_b1_gray_latch_sel                                                    : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_lxlod_0058h xdata g_rw_lxlod_0058h;    // Absolute Address = 5058h

union rw_lxlod_0059h
{
    unsigned char byte;
    struct
    {
        unsigned char r_lxlgray_17x17_en                                                     : 1;        // val = 1
        unsigned char r_lxlgray_mono_en                                                      : 1;        // val = 0
        unsigned char r_proc_bypass                                                          : 1;        // val = 0
        unsigned char r_col_point_adj_bits_4_0                                               : 5;        // [msb:lsb] = [4:0], val = 17
    }bits;
};
extern volatile union rw_lxlod_0059h xdata g_rw_lxlod_0059h;    // Absolute Address = 5059h

union rw_lxlod_005Ah
{
    unsigned char byte;
    struct
    {
        unsigned char r_col_point_adj_bits_7_5                                               : 3;        // [msb:lsb] = [7:5], val = 0
        unsigned char r_tri_gate_12cycle                                                     : 1;        // val = 0
        unsigned char r_lxlgray_19x19_en                                                     : 1;        // val = 0
        unsigned char r_lxl_gain_6b_en                                                       : 1;        // val = 0
        unsigned char r_h_block_num_bits_1_0                                                 : 2;        // [msb:lsb] = [1:0], val = 1
    }bits;
};
extern volatile union rw_lxlod_005Ah xdata g_rw_lxlod_005Ah;    // Absolute Address = 505Ah

union rw_lxlod_005Bh
{
    unsigned char byte;
    struct
    {
        unsigned char r_h_block_num_bits_4_2                                                 : 3;        // [msb:lsb] = [4:2], val = 4
        unsigned char r_v_block_num                                                          : 4;        // [msb:lsb] = [3:0], val = 10
        unsigned char r_idx_19x19_N1_bits_0                                                  : 1;        // val = 0
    }bits;
};
extern volatile union rw_lxlod_005Bh xdata g_rw_lxlod_005Bh;    // Absolute Address = 505Bh

union rw_lxlod_005Ch
{
    unsigned char byte;
    struct
    {
        unsigned char r_idx_19x19_N1_bits_2_1                                                : 2;        // [msb:lsb] = [2:1], val = 0
        unsigned char r_idx_19x19_N2                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_idx_19x19_N3                                                         : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_lxlod_005Ch xdata g_rw_lxlod_005Ch;    // Absolute Address = 505Ch

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_005Dh
{
    unsigned char byte;
    struct
    {
        unsigned char r_idx_19x19_N4                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_idx_19x19_N5                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_idx_19x19_N6_bits_1_0                                                : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_lxlod_005Dh xdata g_rw_lxlod_005Dh;    // Absolute Address = 505Dh

union rw_lxlod_005Eh
{
    unsigned char byte;
    struct
    {
        unsigned char r_idx_19x19_N6_bits_2                                                  : 1;        // val = 0
        unsigned char r_idx_19x19_N7                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_idx_19x19_N8                                                         : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_idx_19x19_N9_bits_0                                                  : 1;        // val = 0
    }bits;
};
extern volatile union rw_lxlod_005Eh xdata g_rw_lxlod_005Eh;    // Absolute Address = 505Eh

union rw_lxlod_005Fh
{
    unsigned char byte;
    struct
    {
        unsigned char r_idx_19x19_N9_bits_2_1                                                : 2;        // [msb:lsb] = [2:1], val = 0
        unsigned char r_idx_19x19_N10                                                        : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_idx_19x19_N11                                                        : 3;        // [msb:lsb] = [2:0], val = 0
    }bits;
};
extern volatile union rw_lxlod_005Fh xdata g_rw_lxlod_005Fh;    // Absolute Address = 505Fh

union rw_lxlod_0060h
{
    unsigned char byte;
    struct
    {
        unsigned char r_idx_19x19_N12                                                        : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_idx_19x19_N13                                                        : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_idx_19x19_N14_bits_1_0                                               : 2;        // [msb:lsb] = [1:0], val = 0
    }bits;
};
extern volatile union rw_lxlod_0060h xdata g_rw_lxlod_0060h;    // Absolute Address = 5060h

union rw_lxlod_0061h
{
    unsigned char byte;
    struct
    {
        unsigned char r_idx_19x19_N14_bits_2                                                 : 1;        // val = 0
        unsigned char r_idx_19x19_N15                                                        : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_idx_19x19_N16                                                        : 3;        // [msb:lsb] = [2:0], val = 0
        unsigned char r_idx_19x19_N17_bits_0                                                 : 1;        // val = 0
    }bits;
};
extern volatile union rw_lxlod_0061h xdata g_rw_lxlod_0061h;    // Absolute Address = 5061h

//------------------------------------------------------------------------------------------------------------------------
union rw_lxlod_0062h
{
    unsigned char byte;
    struct
    {
        unsigned char r_idx_19x19_N17_bits_2_1                                               : 2;        // [msb:lsb] = [2:1], val = 0
        unsigned char lxlgain_lut_rddt_mux                                                   : 1;        // val = 0
        unsigned char bypass_pre_rd_flag                                                     : 1;        // val = 0
        unsigned char r_vact_diff_bits_3_0                                                   : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_lxlod_0062h xdata g_rw_lxlod_0062h;    // Absolute Address = 5062h

union rw_lxlod_0063h
{
    unsigned char byte;
    struct
    {
        unsigned char r_vact_diff_bits_7_4                                                   : 4;        // [msb:lsb] = [7:4], val = 0
        unsigned char r_deb_num                                                              : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_lxlod_0063h xdata g_rw_lxlod_0063h;    // Absolute Address = 5063h

union rw_lxlod_0065h
{
    unsigned char byte;
    struct
    {
        unsigned char r_vrr_dbgen                                                            : 1;        // val = 0
        unsigned char r_dbg_period_bits_6_0                                                  : 7;        // [msb:lsb] = [6:0], val = 0
    }bits;
};
extern volatile union rw_lxlod_0065h xdata g_rw_lxlod_0065h;    // Absolute Address = 5065h

union rw_lxlod_0067h
{
    unsigned char byte;
    struct
    {
        unsigned char r_dbg_period_bits_15                                                   : 1;        // val = 0
        unsigned char r_rate_det_sel                                                         : 1;        // val = 0
        unsigned char r_gain_intpl_en                                                        : 1;        // val = 1
        unsigned char r_prog_rd_addr_rst_manual_en                                           : 1;        // val = 0
        unsigned char r_prog_rd_addr_rst_manual                                              : 4;        // [msb:lsb] = [3:0], val = 0
    }bits;
};
extern volatile union rw_lxlod_0067h xdata g_rw_lxlod_0067h;    // Absolute Address = 5067h


//------------------------------------------------------------------------------------------------------------------------
extern volatile uint8_t xdata g_rw_lxlod_004Dh_h_block_size;                                             // [msb:lsb] = [8:1], val = 80, 	Absolute Address = 504Dh
extern volatile uint8_t xdata g_rw_lxlod_0052h_x_div;                                                    // [msb:lsb] = [11:4], val = 51, 	Absolute Address = 5052h
extern volatile uint8_t xdata g_rw_lxlod_0053h_y_div;                                                    // [msb:lsb] = [7:0], val = 255, 	Absolute Address = 5053h
extern volatile uint8_t xdata g_rw_lxlod_0064h_div_cnt;                                                  // [msb:lsb] = [7:0], val = 0, 	Absolute Address = 5064h
extern volatile uint8_t xdata g_rw_lxlod_0066h_dbg_period;                                               // [msb:lsb] = [14:7], val = 0, 	Absolute Address = 5066h


#endif