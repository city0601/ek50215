#include "daf.h"
#include "hw_mem_map.h"

#ifdef DAF_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------
volatile union rw_daf_0000h xdata g_rw_daf_0000h  _at_  (DAF_FIELD_XMEM_BASE + 0x0000);
volatile union rw_daf_0001h xdata g_rw_daf_0001h  _at_  (DAF_FIELD_XMEM_BASE + 0x0001);
volatile union rw_daf_0002h xdata g_rw_daf_0002h  _at_  (DAF_FIELD_XMEM_BASE + 0x0002);
volatile union rw_daf_0003h xdata g_rw_daf_0003h  _at_  (DAF_FIELD_XMEM_BASE + 0x0003);
volatile union rw_daf_0004h xdata g_rw_daf_0004h  _at_  (DAF_FIELD_XMEM_BASE + 0x0004);

volatile union rw_daf_0005h xdata g_rw_daf_0005h  _at_  (DAF_FIELD_XMEM_BASE + 0x0005);
volatile union rw_daf_0006h xdata g_rw_daf_0006h  _at_  (DAF_FIELD_XMEM_BASE + 0x0006);
volatile union rw_daf_0008h xdata g_rw_daf_0008h  _at_  (DAF_FIELD_XMEM_BASE + 0x0008);
volatile union rw_daf_0009h xdata g_rw_daf_0009h  _at_  (DAF_FIELD_XMEM_BASE + 0x0009);
volatile union rw_daf_000Ah xdata g_rw_daf_000Ah  _at_  (DAF_FIELD_XMEM_BASE + 0x000A);

volatile union rw_daf_000Bh xdata g_rw_daf_000Bh  _at_  (DAF_FIELD_XMEM_BASE + 0x000B);
volatile union rw_daf_000Ch xdata g_rw_daf_000Ch  _at_  (DAF_FIELD_XMEM_BASE + 0x000C);
volatile union rw_daf_000Dh xdata g_rw_daf_000Dh  _at_  (DAF_FIELD_XMEM_BASE + 0x000D);
volatile union rw_daf_000Eh xdata g_rw_daf_000Eh  _at_  (DAF_FIELD_XMEM_BASE + 0x000E);
volatile union rw_daf_0011h xdata g_rw_daf_0011h  _at_  (DAF_FIELD_XMEM_BASE + 0x0011);

volatile union rw_daf_0012h xdata g_rw_daf_0012h  _at_  (DAF_FIELD_XMEM_BASE + 0x0012);
volatile union rw_daf_0013h xdata g_rw_daf_0013h  _at_  (DAF_FIELD_XMEM_BASE + 0x0013);
volatile union rw_daf_0016h xdata g_rw_daf_0016h  _at_  (DAF_FIELD_XMEM_BASE + 0x0016);
volatile union rw_daf_0017h xdata g_rw_daf_0017h  _at_  (DAF_FIELD_XMEM_BASE + 0x0017);
volatile union rw_daf_0019h xdata g_rw_daf_0019h  _at_  (DAF_FIELD_XMEM_BASE + 0x0019);

volatile union rw_daf_001Bh xdata g_rw_daf_001Bh  _at_  (DAF_FIELD_XMEM_BASE + 0x001B);
volatile union rw_daf_001Dh xdata g_rw_daf_001Dh  _at_  (DAF_FIELD_XMEM_BASE + 0x001D);
volatile union rw_daf_001Fh xdata g_rw_daf_001Fh  _at_  (DAF_FIELD_XMEM_BASE + 0x001F);
volatile union rw_daf_0021h xdata g_rw_daf_0021h  _at_  (DAF_FIELD_XMEM_BASE + 0x0021);
volatile union rw_daf_0023h xdata g_rw_daf_0023h  _at_  (DAF_FIELD_XMEM_BASE + 0x0023);

volatile union rw_daf_0026h xdata g_rw_daf_0026h  _at_  (DAF_FIELD_XMEM_BASE + 0x0026);
volatile union rw_daf_0029h xdata g_rw_daf_0029h  _at_  (DAF_FIELD_XMEM_BASE + 0x0029);
volatile union rw_daf_002Ah xdata g_rw_daf_002Ah  _at_  (DAF_FIELD_XMEM_BASE + 0x002A);
volatile union rw_daf_002Bh xdata g_rw_daf_002Bh  _at_  (DAF_FIELD_XMEM_BASE + 0x002B);
volatile union rw_daf_002Eh xdata g_rw_daf_002Eh  _at_  (DAF_FIELD_XMEM_BASE + 0x002E);

volatile union rw_daf_002Fh xdata g_rw_daf_002Fh  _at_  (DAF_FIELD_XMEM_BASE + 0x002F);
volatile union rw_daf_0030h xdata g_rw_daf_0030h  _at_  (DAF_FIELD_XMEM_BASE + 0x0030);
volatile union rw_daf_0032h xdata g_rw_daf_0032h  _at_  (DAF_FIELD_XMEM_BASE + 0x0032);
volatile union rw_daf_0035h xdata g_rw_daf_0035h  _at_  (DAF_FIELD_XMEM_BASE + 0x0035);
volatile union rw_daf_0036h xdata g_rw_daf_0036h  _at_  (DAF_FIELD_XMEM_BASE + 0x0036);

volatile union rw_daf_0037h xdata g_rw_daf_0037h  _at_  (DAF_FIELD_XMEM_BASE + 0x0037);
volatile union rw_daf_0038h xdata g_rw_daf_0038h  _at_  (DAF_FIELD_XMEM_BASE + 0x0038);
volatile union rw_daf_003Ch xdata g_rw_daf_003Ch  _at_  (DAF_FIELD_XMEM_BASE + 0x003C);
volatile union rw_daf_003Fh xdata g_rw_daf_003Fh  _at_  (DAF_FIELD_XMEM_BASE + 0x003F);
volatile union rw_daf_0041h xdata g_rw_daf_0041h  _at_  (DAF_FIELD_XMEM_BASE + 0x0041);

volatile union rw_daf_0043h xdata g_rw_daf_0043h  _at_  (DAF_FIELD_XMEM_BASE + 0x0043);

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_daf_0007h_period_h_thr                                             _at_  (DAF_FIELD_XMEM_BASE + 0x0007);
volatile uint8_t xdata g_rw_daf_000Fh_frc_frame_order                                          _at_  (DAF_FIELD_XMEM_BASE + 0x000F);
volatile uint8_t xdata g_rw_daf_0010h_frc_frame_order                                          _at_  (DAF_FIELD_XMEM_BASE + 0x0010);
volatile uint8_t xdata g_rw_daf_0014h_period_l_thr                                             _at_  (DAF_FIELD_XMEM_BASE + 0x0014);
volatile uint8_t xdata g_rw_daf_0015h_period_l_thr                                             _at_  (DAF_FIELD_XMEM_BASE + 0x0015);

volatile uint8_t xdata g_rw_daf_0018h_dbg_period                                               _at_  (DAF_FIELD_XMEM_BASE + 0x0018);
volatile uint8_t xdata g_rw_daf_001Ah_gam_vrr_ind_t1                                           _at_  (DAF_FIELD_XMEM_BASE + 0x001A);
volatile uint8_t xdata g_rw_daf_001Ch_gam_vrr_ind_t2                                           _at_  (DAF_FIELD_XMEM_BASE + 0x001C);
volatile uint8_t xdata g_rw_daf_001Eh_gam_vrr_ind_t3                                           _at_  (DAF_FIELD_XMEM_BASE + 0x001E);
volatile uint8_t xdata g_rw_daf_0020h_gam_vrr_ind_t4                                           _at_  (DAF_FIELD_XMEM_BASE + 0x0020);

volatile uint8_t xdata g_rw_daf_0022h_h_point                                                  _at_  (DAF_FIELD_XMEM_BASE + 0x0022);
volatile uint8_t xdata g_rw_daf_0024h_v_point                                                  _at_  (DAF_FIELD_XMEM_BASE + 0x0024);
volatile uint8_t xdata g_rw_daf_0025h_direct_r1                                                _at_  (DAF_FIELD_XMEM_BASE + 0x0025);
volatile uint8_t xdata g_rw_daf_0027h_direct_g1                                                _at_  (DAF_FIELD_XMEM_BASE + 0x0027);
volatile uint8_t xdata g_rw_daf_0028h_direct_b1                                                _at_  (DAF_FIELD_XMEM_BASE + 0x0028);

volatile uint8_t xdata g_rw_daf_002Ch_capture_line                                             _at_  (DAF_FIELD_XMEM_BASE + 0x002C);
volatile uint8_t xdata g_rw_daf_002Dh_capture_pixel                                            _at_  (DAF_FIELD_XMEM_BASE + 0x002D);
volatile uint8_t xdata g_rw_daf_0031h_frc_daul_lut_thd                                         _at_  (DAF_FIELD_XMEM_BASE + 0x0031);
volatile uint8_t xdata g_rw_daf_0033h_frc_frm_stack                                            _at_  (DAF_FIELD_XMEM_BASE + 0x0033);
volatile uint8_t xdata g_rw_daf_0034h_frc_frm_stack                                            _at_  (DAF_FIELD_XMEM_BASE + 0x0034);

volatile uint8_t xdata g_rw_daf_0039h_period_time                                              _at_  (DAF_FIELD_XMEM_BASE + 0x0039);
volatile uint8_t xdata g_rw_daf_003Ah_period_time                                              _at_  (DAF_FIELD_XMEM_BASE + 0x003A);
volatile uint8_t xdata g_rw_daf_003Bh_capture_r                                                _at_  (DAF_FIELD_XMEM_BASE + 0x003B);
volatile uint8_t xdata g_rw_daf_003Dh_capture_g                                                _at_  (DAF_FIELD_XMEM_BASE + 0x003D);
volatile uint8_t xdata g_rw_daf_003Eh_capture_b                                                _at_  (DAF_FIELD_XMEM_BASE + 0x003E);

volatile uint8_t xdata g_rw_daf_0040h_capture_htot_rec                                         _at_  (DAF_FIELD_XMEM_BASE + 0x0040);
volatile uint8_t xdata g_rw_daf_0042h_reserved10                                               _at_  (DAF_FIELD_XMEM_BASE + 0x0042);

#endif 
