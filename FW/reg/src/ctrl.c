#include "ctrl.h"
#include "hw_mem_map.h"

#ifdef CTRL_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------
volatile union rw_ctrl_0000h xdata g_rw_ctrl_0000h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0000);
volatile union rw_ctrl_0001h xdata g_rw_ctrl_0001h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0001);
volatile union rw_ctrl_0002h xdata g_rw_ctrl_0002h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0002);
volatile union rw_ctrl_0003h xdata g_rw_ctrl_0003h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0003);
volatile union rw_ctrl_0004h xdata g_rw_ctrl_0004h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0004);

volatile union rw_ctrl_0005h xdata g_rw_ctrl_0005h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0005);
volatile union rw_ctrl_0007h xdata g_rw_ctrl_0007h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0007);
volatile union rw_ctrl_0009h xdata g_rw_ctrl_0009h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0009);
volatile union rw_ctrl_000Ch xdata g_rw_ctrl_000Ch  _at_  (CTRL_FIELD_XMEM_BASE + 0x000C);
volatile union rw_ctrl_000Dh xdata g_rw_ctrl_000Dh  _at_  (CTRL_FIELD_XMEM_BASE + 0x000D);

volatile union rw_ctrl_000Eh xdata g_rw_ctrl_000Eh  _at_  (CTRL_FIELD_XMEM_BASE + 0x000E);
volatile union rw_ctrl_000Fh xdata g_rw_ctrl_000Fh  _at_  (CTRL_FIELD_XMEM_BASE + 0x000F);
volatile union rw_ctrl_0010h xdata g_rw_ctrl_0010h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0010);
volatile union rw_ctrl_0011h xdata g_rw_ctrl_0011h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0011);
volatile union rw_ctrl_0012h xdata g_rw_ctrl_0012h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0012);

volatile union rw_ctrl_0014h xdata g_rw_ctrl_0014h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0014);
volatile union rw_ctrl_0017h xdata g_rw_ctrl_0017h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0017);
volatile union rw_ctrl_0019h xdata g_rw_ctrl_0019h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0019);
volatile union rw_ctrl_001Ah xdata g_rw_ctrl_001Ah  _at_  (CTRL_FIELD_XMEM_BASE + 0x001A);
volatile union rw_ctrl_001Bh xdata g_rw_ctrl_001Bh  _at_  (CTRL_FIELD_XMEM_BASE + 0x001B);

volatile union rw_ctrl_001Ch xdata g_rw_ctrl_001Ch  _at_  (CTRL_FIELD_XMEM_BASE + 0x001C);
volatile union rw_ctrl_001Eh xdata g_rw_ctrl_001Eh  _at_  (CTRL_FIELD_XMEM_BASE + 0x001E);
volatile union rw_ctrl_001Fh xdata g_rw_ctrl_001Fh  _at_  (CTRL_FIELD_XMEM_BASE + 0x001F);
volatile union rw_ctrl_0020h xdata g_rw_ctrl_0020h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0020);
volatile union rw_ctrl_0022h xdata g_rw_ctrl_0022h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0022);

volatile union rw_ctrl_0024h xdata g_rw_ctrl_0024h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0024);
volatile union rw_ctrl_0025h xdata g_rw_ctrl_0025h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0025);
volatile union rw_ctrl_0026h xdata g_rw_ctrl_0026h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0026);
volatile union rw_ctrl_0027h xdata g_rw_ctrl_0027h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0027);
volatile union rw_ctrl_0028h xdata g_rw_ctrl_0028h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0028);

volatile union rw_ctrl_0029h xdata g_rw_ctrl_0029h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0029);
volatile union rw_ctrl_002Ah xdata g_rw_ctrl_002Ah  _at_  (CTRL_FIELD_XMEM_BASE + 0x002A);
volatile union rw_ctrl_002Bh xdata g_rw_ctrl_002Bh  _at_  (CTRL_FIELD_XMEM_BASE + 0x002B);
volatile union rw_ctrl_002Dh xdata g_rw_ctrl_002Dh  _at_  (CTRL_FIELD_XMEM_BASE + 0x002D);
volatile union rw_ctrl_002Fh xdata g_rw_ctrl_002Fh  _at_  (CTRL_FIELD_XMEM_BASE + 0x002F);

volatile union rw_ctrl_0030h xdata g_rw_ctrl_0030h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0030);
volatile union rw_ctrl_0032h xdata g_rw_ctrl_0032h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0032);
volatile union rw_ctrl_0033h xdata g_rw_ctrl_0033h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0033);
volatile union rw_ctrl_0034h xdata g_rw_ctrl_0034h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0034);
volatile union rw_ctrl_0035h xdata g_rw_ctrl_0035h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0035);

volatile union rw_ctrl_0036h xdata g_rw_ctrl_0036h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0036);
volatile union rw_ctrl_0037h xdata g_rw_ctrl_0037h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0037);
volatile union rw_ctrl_0038h xdata g_rw_ctrl_0038h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0038);
volatile union rw_ctrl_003Ah xdata g_rw_ctrl_003Ah  _at_  (CTRL_FIELD_XMEM_BASE + 0x003A);
volatile union rw_ctrl_003Bh xdata g_rw_ctrl_003Bh  _at_  (CTRL_FIELD_XMEM_BASE + 0x003B);

volatile union rw_ctrl_003Dh xdata g_rw_ctrl_003Dh  _at_  (CTRL_FIELD_XMEM_BASE + 0x003D);
volatile union rw_ctrl_003Fh xdata g_rw_ctrl_003Fh  _at_  (CTRL_FIELD_XMEM_BASE + 0x003F);
volatile union rw_ctrl_0040h xdata g_rw_ctrl_0040h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0040);
volatile union rw_ctrl_0041h xdata g_rw_ctrl_0041h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0041);
volatile union rw_ctrl_0042h xdata g_rw_ctrl_0042h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0042);

volatile union rw_ctrl_0046h xdata g_rw_ctrl_0046h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0046);
volatile union rw_ctrl_0048h xdata g_rw_ctrl_0048h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0048);
volatile union rw_ctrl_004Ah xdata g_rw_ctrl_004Ah  _at_  (CTRL_FIELD_XMEM_BASE + 0x004A);
volatile union rw_ctrl_004Bh xdata g_rw_ctrl_004Bh  _at_  (CTRL_FIELD_XMEM_BASE + 0x004B);
volatile union rw_ctrl_004Ch xdata g_rw_ctrl_004Ch  _at_  (CTRL_FIELD_XMEM_BASE + 0x004C);

volatile union rw_ctrl_004Dh xdata g_rw_ctrl_004Dh  _at_  (CTRL_FIELD_XMEM_BASE + 0x004D);
volatile union rw_ctrl_004Eh xdata g_rw_ctrl_004Eh  _at_  (CTRL_FIELD_XMEM_BASE + 0x004E);
volatile union rw_ctrl_004Fh xdata g_rw_ctrl_004Fh  _at_  (CTRL_FIELD_XMEM_BASE + 0x004F);
volatile union rw_ctrl_0050h xdata g_rw_ctrl_0050h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0050);
volatile union rw_ctrl_0051h xdata g_rw_ctrl_0051h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0051);

volatile union rw_ctrl_0053h xdata g_rw_ctrl_0053h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0053);
volatile union rw_ctrl_0055h xdata g_rw_ctrl_0055h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0055);
volatile union rw_ctrl_0056h xdata g_rw_ctrl_0056h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0056);
volatile union rw_ctrl_0058h xdata g_rw_ctrl_0058h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0058);
volatile union rw_ctrl_0059h xdata g_rw_ctrl_0059h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0059);

volatile union rw_ctrl_005Ah xdata g_rw_ctrl_005Ah  _at_  (CTRL_FIELD_XMEM_BASE + 0x005A);
volatile union rw_ctrl_005Bh xdata g_rw_ctrl_005Bh  _at_  (CTRL_FIELD_XMEM_BASE + 0x005B);
volatile union rw_ctrl_005Ch xdata g_rw_ctrl_005Ch  _at_  (CTRL_FIELD_XMEM_BASE + 0x005C);
volatile union rw_ctrl_005Dh xdata g_rw_ctrl_005Dh  _at_  (CTRL_FIELD_XMEM_BASE + 0x005D);
volatile union rw_ctrl_005Eh xdata g_rw_ctrl_005Eh  _at_  (CTRL_FIELD_XMEM_BASE + 0x005E);

volatile union rw_ctrl_0061h xdata g_rw_ctrl_0061h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0061);
volatile union rw_ctrl_0063h xdata g_rw_ctrl_0063h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0063);
volatile union rw_ctrl_0065h xdata g_rw_ctrl_0065h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0065);
volatile union rw_ctrl_0066h xdata g_rw_ctrl_0066h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0066);
volatile union rw_ctrl_0067h xdata g_rw_ctrl_0067h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0067);

volatile union rw_ctrl_0068h xdata g_rw_ctrl_0068h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0068);
volatile union rw_ctrl_0069h xdata g_rw_ctrl_0069h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0069);
volatile union rw_ctrl_006Ah xdata g_rw_ctrl_006Ah  _at_  (CTRL_FIELD_XMEM_BASE + 0x006A);
volatile union rw_ctrl_006Ch xdata g_rw_ctrl_006Ch  _at_  (CTRL_FIELD_XMEM_BASE + 0x006C);
volatile union rw_ctrl_006Eh xdata g_rw_ctrl_006Eh  _at_  (CTRL_FIELD_XMEM_BASE + 0x006E);

volatile union rw_ctrl_0071h xdata g_rw_ctrl_0071h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0071);
volatile union rw_ctrl_0072h xdata g_rw_ctrl_0072h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0072);
volatile union rw_ctrl_0073h xdata g_rw_ctrl_0073h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0073);
volatile union rw_ctrl_0074h xdata g_rw_ctrl_0074h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0074);
volatile union rw_ctrl_0075h xdata g_rw_ctrl_0075h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0075);

volatile union rw_ctrl_0076h xdata g_rw_ctrl_0076h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0076);
volatile union rw_ctrl_0077h xdata g_rw_ctrl_0077h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0077);
volatile union rw_ctrl_0079h xdata g_rw_ctrl_0079h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0079);
volatile union rw_ctrl_007Ch xdata g_rw_ctrl_007Ch  _at_  (CTRL_FIELD_XMEM_BASE + 0x007C);
volatile union rw_ctrl_007Eh xdata g_rw_ctrl_007Eh  _at_  (CTRL_FIELD_XMEM_BASE + 0x007E);

volatile union rw_ctrl_0080h xdata g_rw_ctrl_0080h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0080);
volatile union rw_ctrl_0081h xdata g_rw_ctrl_0081h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0081);
volatile union rw_ctrl_0082h xdata g_rw_ctrl_0082h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0082);
volatile union rw_ctrl_0083h xdata g_rw_ctrl_0083h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0083);
volatile union rw_ctrl_0084h xdata g_rw_ctrl_0084h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0084);

volatile union rw_ctrl_0086h xdata g_rw_ctrl_0086h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0086);
volatile union rw_ctrl_0087h xdata g_rw_ctrl_0087h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0087);
volatile union rw_ctrl_0088h xdata g_rw_ctrl_0088h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0088);
volatile union rw_ctrl_0089h xdata g_rw_ctrl_0089h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0089);
volatile union rw_ctrl_008Ah xdata g_rw_ctrl_008Ah  _at_  (CTRL_FIELD_XMEM_BASE + 0x008A);

volatile union rw_ctrl_008Bh xdata g_rw_ctrl_008Bh  _at_  (CTRL_FIELD_XMEM_BASE + 0x008B);
volatile union rw_ctrl_008Ch xdata g_rw_ctrl_008Ch  _at_  (CTRL_FIELD_XMEM_BASE + 0x008C);
volatile union rw_ctrl_008Dh xdata g_rw_ctrl_008Dh  _at_  (CTRL_FIELD_XMEM_BASE + 0x008D);
volatile union rw_ctrl_008Eh xdata g_rw_ctrl_008Eh  _at_  (CTRL_FIELD_XMEM_BASE + 0x008E);
volatile union rw_ctrl_008Fh xdata g_rw_ctrl_008Fh  _at_  (CTRL_FIELD_XMEM_BASE + 0x008F);

volatile union rw_ctrl_0090h xdata g_rw_ctrl_0090h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0090);
volatile union rw_ctrl_0091h xdata g_rw_ctrl_0091h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0091);
volatile union rw_ctrl_0092h xdata g_rw_ctrl_0092h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0092);
volatile union rw_ctrl_0093h xdata g_rw_ctrl_0093h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0093);
volatile union rw_ctrl_0094h xdata g_rw_ctrl_0094h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0094);

volatile union rw_ctrl_0095h xdata g_rw_ctrl_0095h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0095);
volatile union rw_ctrl_0096h xdata g_rw_ctrl_0096h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0096);
volatile union rw_ctrl_0097h xdata g_rw_ctrl_0097h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0097);
volatile union rw_ctrl_0098h xdata g_rw_ctrl_0098h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0098);
volatile union rw_ctrl_0099h xdata g_rw_ctrl_0099h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0099);

volatile union rw_ctrl_009Ah xdata g_rw_ctrl_009Ah  _at_  (CTRL_FIELD_XMEM_BASE + 0x009A);
volatile union rw_ctrl_009Bh xdata g_rw_ctrl_009Bh  _at_  (CTRL_FIELD_XMEM_BASE + 0x009B);
volatile union rw_ctrl_009Ch xdata g_rw_ctrl_009Ch  _at_  (CTRL_FIELD_XMEM_BASE + 0x009C);
volatile union rw_ctrl_009Dh xdata g_rw_ctrl_009Dh  _at_  (CTRL_FIELD_XMEM_BASE + 0x009D);
volatile union rw_ctrl_009Eh xdata g_rw_ctrl_009Eh  _at_  (CTRL_FIELD_XMEM_BASE + 0x009E);

volatile union rw_ctrl_00A0h xdata g_rw_ctrl_00A0h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00A0);
volatile union rw_ctrl_00A1h xdata g_rw_ctrl_00A1h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00A1);
volatile union rw_ctrl_00A3h xdata g_rw_ctrl_00A3h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00A3);
volatile union rw_ctrl_00A5h xdata g_rw_ctrl_00A5h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00A5);
volatile union rw_ctrl_00A6h xdata g_rw_ctrl_00A6h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00A6);

volatile union rw_ctrl_00A8h xdata g_rw_ctrl_00A8h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00A8);
volatile union rw_ctrl_00AAh xdata g_rw_ctrl_00AAh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00AA);
volatile union rw_ctrl_00ABh xdata g_rw_ctrl_00ABh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00AB);
volatile union rw_ctrl_00ADh xdata g_rw_ctrl_00ADh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00AD);
volatile union rw_ctrl_00B0h xdata g_rw_ctrl_00B0h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00B0);

volatile union rw_ctrl_00B2h xdata g_rw_ctrl_00B2h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00B2);
volatile union rw_ctrl_00B3h xdata g_rw_ctrl_00B3h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00B3);
volatile union rw_ctrl_00B4h xdata g_rw_ctrl_00B4h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00B4);
volatile union rw_ctrl_00B5h xdata g_rw_ctrl_00B5h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00B5);
volatile union rw_ctrl_00B6h xdata g_rw_ctrl_00B6h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00B6);

volatile union rw_ctrl_00B7h xdata g_rw_ctrl_00B7h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00B7);
volatile union rw_ctrl_00B8h xdata g_rw_ctrl_00B8h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00B8);
volatile union rw_ctrl_00B9h xdata g_rw_ctrl_00B9h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00B9);
volatile union rw_ctrl_00BBh xdata g_rw_ctrl_00BBh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00BB);
volatile union rw_ctrl_00BDh xdata g_rw_ctrl_00BDh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00BD);

volatile union rw_ctrl_00C0h xdata g_rw_ctrl_00C0h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00C0);
volatile union rw_ctrl_00C1h xdata g_rw_ctrl_00C1h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00C1);
volatile union rw_ctrl_00C2h xdata g_rw_ctrl_00C2h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00C2);
volatile union rw_ctrl_00C3h xdata g_rw_ctrl_00C3h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00C3);
volatile union rw_ctrl_00C4h xdata g_rw_ctrl_00C4h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00C4);

volatile union rw_ctrl_00C5h xdata g_rw_ctrl_00C5h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00C5);
volatile union rw_ctrl_00C6h xdata g_rw_ctrl_00C6h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00C6);
volatile union rw_ctrl_00C8h xdata g_rw_ctrl_00C8h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00C8);
volatile union rw_ctrl_00CBh xdata g_rw_ctrl_00CBh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00CB);
volatile union rw_ctrl_00CDh xdata g_rw_ctrl_00CDh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00CD);

volatile union rw_ctrl_00CEh xdata g_rw_ctrl_00CEh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00CE);
volatile union rw_ctrl_00CFh xdata g_rw_ctrl_00CFh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00CF);
volatile union rw_ctrl_00D0h xdata g_rw_ctrl_00D0h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00D0);
volatile union rw_ctrl_00D2h xdata g_rw_ctrl_00D2h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00D2);
volatile union rw_ctrl_00D3h xdata g_rw_ctrl_00D3h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00D3);

volatile union rw_ctrl_00D4h xdata g_rw_ctrl_00D4h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00D4);
volatile union rw_ctrl_00D6h xdata g_rw_ctrl_00D6h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00D6);
volatile union rw_ctrl_00D8h xdata g_rw_ctrl_00D8h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00D8);
volatile union rw_ctrl_00D9h xdata g_rw_ctrl_00D9h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00D9);
volatile union rw_ctrl_00DAh xdata g_rw_ctrl_00DAh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00DA);

volatile union rw_ctrl_00DBh xdata g_rw_ctrl_00DBh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00DB);
volatile union rw_ctrl_00DCh xdata g_rw_ctrl_00DCh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00DC);
volatile union rw_ctrl_00DDh xdata g_rw_ctrl_00DDh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00DD);
volatile union rw_ctrl_00DEh xdata g_rw_ctrl_00DEh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00DE);
volatile union rw_ctrl_00DFh xdata g_rw_ctrl_00DFh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00DF);

volatile union rw_ctrl_00E1h xdata g_rw_ctrl_00E1h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00E1);
volatile union rw_ctrl_00E3h xdata g_rw_ctrl_00E3h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00E3);
volatile union rw_ctrl_00E4h xdata g_rw_ctrl_00E4h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00E4);
volatile union rw_ctrl_00E6h xdata g_rw_ctrl_00E6h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00E6);
volatile union rw_ctrl_00E7h xdata g_rw_ctrl_00E7h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00E7);

volatile union rw_ctrl_00E8h xdata g_rw_ctrl_00E8h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00E8);
volatile union rw_ctrl_00E9h xdata g_rw_ctrl_00E9h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00E9);
volatile union rw_ctrl_00EAh xdata g_rw_ctrl_00EAh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00EA);
volatile union rw_ctrl_00EBh xdata g_rw_ctrl_00EBh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00EB);
volatile union rw_ctrl_00ECh xdata g_rw_ctrl_00ECh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00EC);

volatile union rw_ctrl_00EEh xdata g_rw_ctrl_00EEh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00EE);
volatile union rw_ctrl_00EFh xdata g_rw_ctrl_00EFh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00EF);
volatile union rw_ctrl_00F1h xdata g_rw_ctrl_00F1h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00F1);
volatile union rw_ctrl_00F3h xdata g_rw_ctrl_00F3h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00F3);
volatile union rw_ctrl_00F4h xdata g_rw_ctrl_00F4h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00F4);

volatile union rw_ctrl_00F5h xdata g_rw_ctrl_00F5h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00F5);
volatile union rw_ctrl_00F6h xdata g_rw_ctrl_00F6h  _at_  (CTRL_FIELD_XMEM_BASE + 0x00F6);
volatile union rw_ctrl_00FAh xdata g_rw_ctrl_00FAh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00FA);
volatile union rw_ctrl_00FCh xdata g_rw_ctrl_00FCh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00FC);
volatile union rw_ctrl_00FEh xdata g_rw_ctrl_00FEh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00FE);

volatile union rw_ctrl_00FFh xdata g_rw_ctrl_00FFh  _at_  (CTRL_FIELD_XMEM_BASE + 0x00FF);
volatile union rw_ctrl_0100h xdata g_rw_ctrl_0100h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0100);
volatile union rw_ctrl_0101h xdata g_rw_ctrl_0101h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0101);
volatile union rw_ctrl_0102h xdata g_rw_ctrl_0102h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0102);
volatile union rw_ctrl_0103h xdata g_rw_ctrl_0103h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0103);

volatile union rw_ctrl_0104h xdata g_rw_ctrl_0104h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0104);
volatile union rw_ctrl_0105h xdata g_rw_ctrl_0105h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0105);
volatile union rw_ctrl_0106h xdata g_rw_ctrl_0106h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0106);
volatile union rw_ctrl_0107h xdata g_rw_ctrl_0107h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0107);
volatile union rw_ctrl_0108h xdata g_rw_ctrl_0108h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0108);

volatile union rw_ctrl_0109h xdata g_rw_ctrl_0109h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0109);
volatile union rw_ctrl_010Ah xdata g_rw_ctrl_010Ah  _at_  (CTRL_FIELD_XMEM_BASE + 0x010A);
volatile union rw_ctrl_010Bh xdata g_rw_ctrl_010Bh  _at_  (CTRL_FIELD_XMEM_BASE + 0x010B);
volatile union rw_ctrl_010Ch xdata g_rw_ctrl_010Ch  _at_  (CTRL_FIELD_XMEM_BASE + 0x010C);
volatile union rw_ctrl_010Dh xdata g_rw_ctrl_010Dh  _at_  (CTRL_FIELD_XMEM_BASE + 0x010D);

volatile union rw_ctrl_010Eh xdata g_rw_ctrl_010Eh  _at_  (CTRL_FIELD_XMEM_BASE + 0x010E);
volatile union rw_ctrl_010Fh xdata g_rw_ctrl_010Fh  _at_  (CTRL_FIELD_XMEM_BASE + 0x010F);
volatile union rw_ctrl_0110h xdata g_rw_ctrl_0110h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0110);
volatile union rw_ctrl_0111h xdata g_rw_ctrl_0111h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0111);
volatile union rw_ctrl_0112h xdata g_rw_ctrl_0112h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0112);

volatile union rw_ctrl_0114h xdata g_rw_ctrl_0114h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0114);
volatile union rw_ctrl_0116h xdata g_rw_ctrl_0116h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0116);
volatile union rw_ctrl_0117h xdata g_rw_ctrl_0117h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0117);
volatile union rw_ctrl_0118h xdata g_rw_ctrl_0118h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0118);
volatile union rw_ctrl_0119h xdata g_rw_ctrl_0119h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0119);

volatile union rw_ctrl_011Ah xdata g_rw_ctrl_011Ah  _at_  (CTRL_FIELD_XMEM_BASE + 0x011A);
volatile union rw_ctrl_011Bh xdata g_rw_ctrl_011Bh  _at_  (CTRL_FIELD_XMEM_BASE + 0x011B);
volatile union rw_ctrl_011Ch xdata g_rw_ctrl_011Ch  _at_  (CTRL_FIELD_XMEM_BASE + 0x011C);
volatile union rw_ctrl_011Dh xdata g_rw_ctrl_011Dh  _at_  (CTRL_FIELD_XMEM_BASE + 0x011D);
volatile union rw_ctrl_011Eh xdata g_rw_ctrl_011Eh  _at_  (CTRL_FIELD_XMEM_BASE + 0x011E);

volatile union rw_ctrl_011Fh xdata g_rw_ctrl_011Fh  _at_  (CTRL_FIELD_XMEM_BASE + 0x011F);
volatile union rw_ctrl_0120h xdata g_rw_ctrl_0120h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0120);
volatile union rw_ctrl_0121h xdata g_rw_ctrl_0121h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0121);
volatile union rw_ctrl_0122h xdata g_rw_ctrl_0122h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0122);
volatile union rw_ctrl_0123h xdata g_rw_ctrl_0123h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0123);

volatile union rw_ctrl_0124h xdata g_rw_ctrl_0124h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0124);
volatile union rw_ctrl_0125h xdata g_rw_ctrl_0125h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0125);
volatile union rw_ctrl_0126h xdata g_rw_ctrl_0126h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0126);
volatile union rw_ctrl_0127h xdata g_rw_ctrl_0127h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0127);
volatile union rw_ctrl_0128h xdata g_rw_ctrl_0128h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0128);

volatile union rw_ctrl_0129h xdata g_rw_ctrl_0129h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0129);
volatile union rw_ctrl_012Ah xdata g_rw_ctrl_012Ah  _at_  (CTRL_FIELD_XMEM_BASE + 0x012A);
volatile union rw_ctrl_012Bh xdata g_rw_ctrl_012Bh  _at_  (CTRL_FIELD_XMEM_BASE + 0x012B);
volatile union rw_ctrl_012Ch xdata g_rw_ctrl_012Ch  _at_  (CTRL_FIELD_XMEM_BASE + 0x012C);
volatile union rw_ctrl_012Dh xdata g_rw_ctrl_012Dh  _at_  (CTRL_FIELD_XMEM_BASE + 0x012D);

volatile union rw_ctrl_012Eh xdata g_rw_ctrl_012Eh  _at_  (CTRL_FIELD_XMEM_BASE + 0x012E);
volatile union rw_ctrl_012Fh xdata g_rw_ctrl_012Fh  _at_  (CTRL_FIELD_XMEM_BASE + 0x012F);
volatile union rw_ctrl_0130h xdata g_rw_ctrl_0130h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0130);
volatile union rw_ctrl_0131h xdata g_rw_ctrl_0131h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0131);
volatile union rw_ctrl_0132h xdata g_rw_ctrl_0132h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0132);

volatile union rw_ctrl_0133h xdata g_rw_ctrl_0133h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0133);
volatile union rw_ctrl_0134h xdata g_rw_ctrl_0134h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0134);
volatile union rw_ctrl_0135h xdata g_rw_ctrl_0135h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0135);
volatile union rw_ctrl_0136h xdata g_rw_ctrl_0136h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0136);
volatile union rw_ctrl_0137h xdata g_rw_ctrl_0137h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0137);

volatile union rw_ctrl_0138h xdata g_rw_ctrl_0138h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0138);
volatile union rw_ctrl_0139h xdata g_rw_ctrl_0139h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0139);
volatile union rw_ctrl_013Ah xdata g_rw_ctrl_013Ah  _at_  (CTRL_FIELD_XMEM_BASE + 0x013A);
volatile union rw_ctrl_013Bh xdata g_rw_ctrl_013Bh  _at_  (CTRL_FIELD_XMEM_BASE + 0x013B);
volatile union rw_ctrl_013Ch xdata g_rw_ctrl_013Ch  _at_  (CTRL_FIELD_XMEM_BASE + 0x013C);

volatile union rw_ctrl_013Dh xdata g_rw_ctrl_013Dh  _at_  (CTRL_FIELD_XMEM_BASE + 0x013D);
volatile union rw_ctrl_013Fh xdata g_rw_ctrl_013Fh  _at_  (CTRL_FIELD_XMEM_BASE + 0x013F);
volatile union rw_ctrl_0141h xdata g_rw_ctrl_0141h  _at_  (CTRL_FIELD_XMEM_BASE + 0x0141);

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_ctrl_0006h_gck00_st_v_line                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x0006);
volatile uint8_t xdata g_rw_ctrl_0008h_gck00_end_v_line                                        _at_  (CTRL_FIELD_XMEM_BASE + 0x0008);
volatile uint8_t xdata g_rw_ctrl_000Ah_gck00_sh                                                _at_  (CTRL_FIELD_XMEM_BASE + 0x000A);
volatile uint8_t xdata g_rw_ctrl_000Bh_gck00_w                                                 _at_  (CTRL_FIELD_XMEM_BASE + 0x000B);
volatile uint8_t xdata g_rw_ctrl_0013h_gck01_st_v_line                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x0013);

volatile uint8_t xdata g_rw_ctrl_0015h_gck01_end_v_line                                        _at_  (CTRL_FIELD_XMEM_BASE + 0x0015);
volatile uint8_t xdata g_rw_ctrl_0016h_gck01_sh                                                _at_  (CTRL_FIELD_XMEM_BASE + 0x0016);
volatile uint8_t xdata g_rw_ctrl_0018h_gck01_w                                                 _at_  (CTRL_FIELD_XMEM_BASE + 0x0018);
volatile uint8_t xdata g_rw_ctrl_001Dh_gck02_frm_cycle                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x001D);
volatile uint8_t xdata g_rw_ctrl_0021h_gck02_end_v_line                                        _at_  (CTRL_FIELD_XMEM_BASE + 0x0021);

volatile uint8_t xdata g_rw_ctrl_0023h_gck02_sh                                                _at_  (CTRL_FIELD_XMEM_BASE + 0x0023);
volatile uint8_t xdata g_rw_ctrl_002Ch_gck03_st_v_line                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x002C);
volatile uint8_t xdata g_rw_ctrl_002Eh_gck03_end_v_line                                        _at_  (CTRL_FIELD_XMEM_BASE + 0x002E);
volatile uint8_t xdata g_rw_ctrl_0031h_gck03_w                                                 _at_  (CTRL_FIELD_XMEM_BASE + 0x0031);
volatile uint8_t xdata g_rw_ctrl_0039h_gck04_st_v_line                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x0039);

volatile uint8_t xdata g_rw_ctrl_003Ch_gck04_sh                                                _at_  (CTRL_FIELD_XMEM_BASE + 0x003C);
volatile uint8_t xdata g_rw_ctrl_003Eh_gck04_w                                                 _at_  (CTRL_FIELD_XMEM_BASE + 0x003E);
volatile uint8_t xdata g_rw_ctrl_0043h_gck05_frm_cycle                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x0043);
volatile uint8_t xdata g_rw_ctrl_0044h_gck05_init                                              _at_  (CTRL_FIELD_XMEM_BASE + 0x0044);
volatile uint8_t xdata g_rw_ctrl_0045h_gck05_st_v_line                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x0045);

volatile uint8_t xdata g_rw_ctrl_0047h_gck05_end_v_line                                        _at_  (CTRL_FIELD_XMEM_BASE + 0x0047);
volatile uint8_t xdata g_rw_ctrl_0049h_gck05_sh                                                _at_  (CTRL_FIELD_XMEM_BASE + 0x0049);
volatile uint8_t xdata g_rw_ctrl_0052h_gck06_st_v_line                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x0052);
volatile uint8_t xdata g_rw_ctrl_0054h_gck06_end_v_line                                        _at_  (CTRL_FIELD_XMEM_BASE + 0x0054);
volatile uint8_t xdata g_rw_ctrl_0057h_gck06_w                                                 _at_  (CTRL_FIELD_XMEM_BASE + 0x0057);

volatile uint8_t xdata g_rw_ctrl_005Fh_gck07_st_v_line                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x005F);
volatile uint8_t xdata g_rw_ctrl_0060h_gck07_end_v_line                                        _at_  (CTRL_FIELD_XMEM_BASE + 0x0060);
volatile uint8_t xdata g_rw_ctrl_0062h_gck07_sh                                                _at_  (CTRL_FIELD_XMEM_BASE + 0x0062);
volatile uint8_t xdata g_rw_ctrl_0064h_gck07_w                                                 _at_  (CTRL_FIELD_XMEM_BASE + 0x0064);
volatile uint8_t xdata g_rw_ctrl_006Bh_gck08_st_v_line                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x006B);

volatile uint8_t xdata g_rw_ctrl_006Dh_gck08_end_v_line                                        _at_  (CTRL_FIELD_XMEM_BASE + 0x006D);
volatile uint8_t xdata g_rw_ctrl_006Fh_gck08_sh                                                _at_  (CTRL_FIELD_XMEM_BASE + 0x006F);
volatile uint8_t xdata g_rw_ctrl_0070h_gck08_w                                                 _at_  (CTRL_FIELD_XMEM_BASE + 0x0070);
volatile uint8_t xdata g_rw_ctrl_0078h_gck09_st_v_line                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x0078);
volatile uint8_t xdata g_rw_ctrl_007Ah_gck09_end_v_line                                        _at_  (CTRL_FIELD_XMEM_BASE + 0x007A);

volatile uint8_t xdata g_rw_ctrl_007Bh_gck09_sh                                                _at_  (CTRL_FIELD_XMEM_BASE + 0x007B);
volatile uint8_t xdata g_rw_ctrl_007Dh_gck09_w                                                 _at_  (CTRL_FIELD_XMEM_BASE + 0x007D);
volatile uint8_t xdata g_rw_ctrl_007Fh_gck_w_cs                                                _at_  (CTRL_FIELD_XMEM_BASE + 0x007F);
volatile uint8_t xdata g_rw_ctrl_0085h_gck_vblk_end_v_line                                     _at_  (CTRL_FIELD_XMEM_BASE + 0x0085);
volatile uint8_t xdata g_rw_ctrl_009Fh_28s_gck_sh1                                             _at_  (CTRL_FIELD_XMEM_BASE + 0x009F);

volatile uint8_t xdata g_rw_ctrl_00A2h_28s_gck_sh3                                             _at_  (CTRL_FIELD_XMEM_BASE + 0x00A2);
volatile uint8_t xdata g_rw_ctrl_00A4h_28s_gck_w1                                              _at_  (CTRL_FIELD_XMEM_BASE + 0x00A4);
volatile uint8_t xdata g_rw_ctrl_00A7h_28s_gck_w3                                              _at_  (CTRL_FIELD_XMEM_BASE + 0x00A7);
volatile uint8_t xdata g_rw_ctrl_00A9h_28s_ld_sh1                                              _at_  (CTRL_FIELD_XMEM_BASE + 0x00A9);
volatile uint8_t xdata g_rw_ctrl_00ACh_28s_ld_sh3                                              _at_  (CTRL_FIELD_XMEM_BASE + 0x00AC);

volatile uint8_t xdata g_rw_ctrl_00AEh_28s_ld_w1                                               _at_  (CTRL_FIELD_XMEM_BASE + 0x00AE);
volatile uint8_t xdata g_rw_ctrl_00AFh_28s_ld_w2                                               _at_  (CTRL_FIELD_XMEM_BASE + 0x00AF);
volatile uint8_t xdata g_rw_ctrl_00B1h_28s_ld_w3                                               _at_  (CTRL_FIELD_XMEM_BASE + 0x00B1);
volatile uint8_t xdata g_rw_ctrl_00BAh_gck0a_st_v_line                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x00BA);
volatile uint8_t xdata g_rw_ctrl_00BCh_gck0a_end_v_line                                        _at_  (CTRL_FIELD_XMEM_BASE + 0x00BC);

volatile uint8_t xdata g_rw_ctrl_00BEh_gck0a_sh                                                _at_  (CTRL_FIELD_XMEM_BASE + 0x00BE);
volatile uint8_t xdata g_rw_ctrl_00BFh_gck0a_w                                                 _at_  (CTRL_FIELD_XMEM_BASE + 0x00BF);
volatile uint8_t xdata g_rw_ctrl_00C7h_gck0b_st_v_line                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x00C7);
volatile uint8_t xdata g_rw_ctrl_00C9h_gck0b_end_v_line                                        _at_  (CTRL_FIELD_XMEM_BASE + 0x00C9);
volatile uint8_t xdata g_rw_ctrl_00CAh_gck0b_sh                                                _at_  (CTRL_FIELD_XMEM_BASE + 0x00CA);

volatile uint8_t xdata g_rw_ctrl_00CCh_gck0b_w                                                 _at_  (CTRL_FIELD_XMEM_BASE + 0x00CC);
volatile uint8_t xdata g_rw_ctrl_00D1h_gck0c_frm_cycle                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x00D1);
volatile uint8_t xdata g_rw_ctrl_00D5h_gck0c_end_v_line                                        _at_  (CTRL_FIELD_XMEM_BASE + 0x00D5);
volatile uint8_t xdata g_rw_ctrl_00D7h_gck0c_sh                                                _at_  (CTRL_FIELD_XMEM_BASE + 0x00D7);
volatile uint8_t xdata g_rw_ctrl_00E0h_gck0d_st_v_line                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x00E0);

volatile uint8_t xdata g_rw_ctrl_00E2h_gck0d_end_v_line                                        _at_  (CTRL_FIELD_XMEM_BASE + 0x00E2);
volatile uint8_t xdata g_rw_ctrl_00E5h_gck0d_w                                                 _at_  (CTRL_FIELD_XMEM_BASE + 0x00E5);
volatile uint8_t xdata g_rw_ctrl_00EDh_gck0e_st_v_line                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x00ED);
volatile uint8_t xdata g_rw_ctrl_00F0h_gck0e_sh                                                _at_  (CTRL_FIELD_XMEM_BASE + 0x00F0);
volatile uint8_t xdata g_rw_ctrl_00F2h_gck0e_w                                                 _at_  (CTRL_FIELD_XMEM_BASE + 0x00F2);

volatile uint8_t xdata g_rw_ctrl_00F7h_gck0f_frm_cycle                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x00F7);
volatile uint8_t xdata g_rw_ctrl_00F8h_gck0f_init                                              _at_  (CTRL_FIELD_XMEM_BASE + 0x00F8);
volatile uint8_t xdata g_rw_ctrl_00F9h_gck0f_st_v_line                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x00F9);
volatile uint8_t xdata g_rw_ctrl_00FBh_gck0f_end_v_line                                        _at_  (CTRL_FIELD_XMEM_BASE + 0x00FB);
volatile uint8_t xdata g_rw_ctrl_00FDh_gck0f_sh                                                _at_  (CTRL_FIELD_XMEM_BASE + 0x00FD);

volatile uint8_t xdata g_rw_ctrl_0113h_28s_w1                                                  _at_  (CTRL_FIELD_XMEM_BASE + 0x0113);
volatile uint8_t xdata g_rw_ctrl_0115h_28s_w2                                                  _at_  (CTRL_FIELD_XMEM_BASE + 0x0115);
volatile uint8_t xdata g_rw_ctrl_013Eh_chksum                                                  _at_  (CTRL_FIELD_XMEM_BASE + 0x013E);
volatile uint8_t xdata g_rw_ctrl_0140h_touch_sync_hdly                                         _at_  (CTRL_FIELD_XMEM_BASE + 0x0140);

#endif 
