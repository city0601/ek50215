#include "pdf.h"
#include "hw_mem_map.h"

#ifdef PDF_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------
volatile union rw_pdf_0000h xdata g_rw_pdf_0000h  _at_  (PDF_FIELD_XMEM_BASE + 0x0000);
volatile union rw_pdf_0003h xdata g_rw_pdf_0003h  _at_  (PDF_FIELD_XMEM_BASE + 0x0003);
volatile union rw_pdf_0004h xdata g_rw_pdf_0004h  _at_  (PDF_FIELD_XMEM_BASE + 0x0004);
volatile union rw_pdf_0005h xdata g_rw_pdf_0005h  _at_  (PDF_FIELD_XMEM_BASE + 0x0005);
volatile union rw_pdf_0008h xdata g_rw_pdf_0008h  _at_  (PDF_FIELD_XMEM_BASE + 0x0008);

volatile union rw_pdf_0009h xdata g_rw_pdf_0009h  _at_  (PDF_FIELD_XMEM_BASE + 0x0009);
volatile union rw_pdf_000Ah xdata g_rw_pdf_000Ah  _at_  (PDF_FIELD_XMEM_BASE + 0x000A);
volatile union rw_pdf_000Dh xdata g_rw_pdf_000Dh  _at_  (PDF_FIELD_XMEM_BASE + 0x000D);
volatile union rw_pdf_000Eh xdata g_rw_pdf_000Eh  _at_  (PDF_FIELD_XMEM_BASE + 0x000E);
volatile union rw_pdf_000Fh xdata g_rw_pdf_000Fh  _at_  (PDF_FIELD_XMEM_BASE + 0x000F);

volatile union rw_pdf_0012h xdata g_rw_pdf_0012h  _at_  (PDF_FIELD_XMEM_BASE + 0x0012);
volatile union rw_pdf_0013h xdata g_rw_pdf_0013h  _at_  (PDF_FIELD_XMEM_BASE + 0x0013);
volatile union rw_pdf_0014h xdata g_rw_pdf_0014h  _at_  (PDF_FIELD_XMEM_BASE + 0x0014);
volatile union rw_pdf_0017h xdata g_rw_pdf_0017h  _at_  (PDF_FIELD_XMEM_BASE + 0x0017);
volatile union rw_pdf_0018h xdata g_rw_pdf_0018h  _at_  (PDF_FIELD_XMEM_BASE + 0x0018);

volatile union rw_pdf_0019h xdata g_rw_pdf_0019h  _at_  (PDF_FIELD_XMEM_BASE + 0x0019);
volatile union rw_pdf_001Ch xdata g_rw_pdf_001Ch  _at_  (PDF_FIELD_XMEM_BASE + 0x001C);
volatile union rw_pdf_001Dh xdata g_rw_pdf_001Dh  _at_  (PDF_FIELD_XMEM_BASE + 0x001D);
volatile union rw_pdf_001Eh xdata g_rw_pdf_001Eh  _at_  (PDF_FIELD_XMEM_BASE + 0x001E);
volatile union rw_pdf_0021h xdata g_rw_pdf_0021h  _at_  (PDF_FIELD_XMEM_BASE + 0x0021);

volatile union rw_pdf_0022h xdata g_rw_pdf_0022h  _at_  (PDF_FIELD_XMEM_BASE + 0x0022);
volatile union rw_pdf_0023h xdata g_rw_pdf_0023h  _at_  (PDF_FIELD_XMEM_BASE + 0x0023);
volatile union rw_pdf_0026h xdata g_rw_pdf_0026h  _at_  (PDF_FIELD_XMEM_BASE + 0x0026);
volatile union rw_pdf_0027h xdata g_rw_pdf_0027h  _at_  (PDF_FIELD_XMEM_BASE + 0x0027);
volatile union rw_pdf_0028h xdata g_rw_pdf_0028h  _at_  (PDF_FIELD_XMEM_BASE + 0x0028);

volatile union rw_pdf_002Bh xdata g_rw_pdf_002Bh  _at_  (PDF_FIELD_XMEM_BASE + 0x002B);
volatile union rw_pdf_002Ch xdata g_rw_pdf_002Ch  _at_  (PDF_FIELD_XMEM_BASE + 0x002C);
volatile union rw_pdf_002Dh xdata g_rw_pdf_002Dh  _at_  (PDF_FIELD_XMEM_BASE + 0x002D);
volatile union rw_pdf_0030h xdata g_rw_pdf_0030h  _at_  (PDF_FIELD_XMEM_BASE + 0x0030);
volatile union rw_pdf_0031h xdata g_rw_pdf_0031h  _at_  (PDF_FIELD_XMEM_BASE + 0x0031);

volatile union rw_pdf_0032h xdata g_rw_pdf_0032h  _at_  (PDF_FIELD_XMEM_BASE + 0x0032);
volatile union rw_pdf_0035h xdata g_rw_pdf_0035h  _at_  (PDF_FIELD_XMEM_BASE + 0x0035);
volatile union rw_pdf_0038h xdata g_rw_pdf_0038h  _at_  (PDF_FIELD_XMEM_BASE + 0x0038);
volatile union rw_pdf_0039h xdata g_rw_pdf_0039h  _at_  (PDF_FIELD_XMEM_BASE + 0x0039);
volatile union rw_pdf_003Ah xdata g_rw_pdf_003Ah  _at_  (PDF_FIELD_XMEM_BASE + 0x003A);

volatile union rw_pdf_003Dh xdata g_rw_pdf_003Dh  _at_  (PDF_FIELD_XMEM_BASE + 0x003D);
volatile union rw_pdf_0040h xdata g_rw_pdf_0040h  _at_  (PDF_FIELD_XMEM_BASE + 0x0040);
volatile union rw_pdf_0041h xdata g_rw_pdf_0041h  _at_  (PDF_FIELD_XMEM_BASE + 0x0041);
volatile union rw_pdf_0042h xdata g_rw_pdf_0042h  _at_  (PDF_FIELD_XMEM_BASE + 0x0042);
volatile union rw_pdf_0045h xdata g_rw_pdf_0045h  _at_  (PDF_FIELD_XMEM_BASE + 0x0045);

volatile union rw_pdf_0048h xdata g_rw_pdf_0048h  _at_  (PDF_FIELD_XMEM_BASE + 0x0048);
volatile union rw_pdf_004Bh xdata g_rw_pdf_004Bh  _at_  (PDF_FIELD_XMEM_BASE + 0x004B);
volatile union rw_pdf_004Eh xdata g_rw_pdf_004Eh  _at_  (PDF_FIELD_XMEM_BASE + 0x004E);
volatile union rw_pdf_004Fh xdata g_rw_pdf_004Fh  _at_  (PDF_FIELD_XMEM_BASE + 0x004F);
volatile union rw_pdf_0050h xdata g_rw_pdf_0050h  _at_  (PDF_FIELD_XMEM_BASE + 0x0050);

volatile union rw_pdf_0053h xdata g_rw_pdf_0053h  _at_  (PDF_FIELD_XMEM_BASE + 0x0053);
volatile union rw_pdf_0054h xdata g_rw_pdf_0054h  _at_  (PDF_FIELD_XMEM_BASE + 0x0054);
volatile union rw_pdf_0055h xdata g_rw_pdf_0055h  _at_  (PDF_FIELD_XMEM_BASE + 0x0055);
volatile union rw_pdf_0058h xdata g_rw_pdf_0058h  _at_  (PDF_FIELD_XMEM_BASE + 0x0058);
volatile union rw_pdf_0059h xdata g_rw_pdf_0059h  _at_  (PDF_FIELD_XMEM_BASE + 0x0059);

volatile union rw_pdf_005Ah xdata g_rw_pdf_005Ah  _at_  (PDF_FIELD_XMEM_BASE + 0x005A);
volatile union rw_pdf_005Dh xdata g_rw_pdf_005Dh  _at_  (PDF_FIELD_XMEM_BASE + 0x005D);
volatile union rw_pdf_005Eh xdata g_rw_pdf_005Eh  _at_  (PDF_FIELD_XMEM_BASE + 0x005E);
volatile union rw_pdf_005Fh xdata g_rw_pdf_005Fh  _at_  (PDF_FIELD_XMEM_BASE + 0x005F);
volatile union rw_pdf_0062h xdata g_rw_pdf_0062h  _at_  (PDF_FIELD_XMEM_BASE + 0x0062);

volatile union rw_pdf_0063h xdata g_rw_pdf_0063h  _at_  (PDF_FIELD_XMEM_BASE + 0x0063);
volatile union rw_pdf_0064h xdata g_rw_pdf_0064h  _at_  (PDF_FIELD_XMEM_BASE + 0x0064);
volatile union rw_pdf_0067h xdata g_rw_pdf_0067h  _at_  (PDF_FIELD_XMEM_BASE + 0x0067);
volatile union rw_pdf_0068h xdata g_rw_pdf_0068h  _at_  (PDF_FIELD_XMEM_BASE + 0x0068);
volatile union rw_pdf_0069h xdata g_rw_pdf_0069h  _at_  (PDF_FIELD_XMEM_BASE + 0x0069);

volatile union rw_pdf_006Ch xdata g_rw_pdf_006Ch  _at_  (PDF_FIELD_XMEM_BASE + 0x006C);
volatile union rw_pdf_006Dh xdata g_rw_pdf_006Dh  _at_  (PDF_FIELD_XMEM_BASE + 0x006D);
volatile union rw_pdf_006Eh xdata g_rw_pdf_006Eh  _at_  (PDF_FIELD_XMEM_BASE + 0x006E);
volatile union rw_pdf_0071h xdata g_rw_pdf_0071h  _at_  (PDF_FIELD_XMEM_BASE + 0x0071);
volatile union rw_pdf_0072h xdata g_rw_pdf_0072h  _at_  (PDF_FIELD_XMEM_BASE + 0x0072);

volatile union rw_pdf_0073h xdata g_rw_pdf_0073h  _at_  (PDF_FIELD_XMEM_BASE + 0x0073);
volatile union rw_pdf_0074h xdata g_rw_pdf_0074h  _at_  (PDF_FIELD_XMEM_BASE + 0x0074);
volatile union rw_pdf_0075h xdata g_rw_pdf_0075h  _at_  (PDF_FIELD_XMEM_BASE + 0x0075);
volatile union rw_pdf_0076h xdata g_rw_pdf_0076h  _at_  (PDF_FIELD_XMEM_BASE + 0x0076);
volatile union rw_pdf_0077h xdata g_rw_pdf_0077h  _at_  (PDF_FIELD_XMEM_BASE + 0x0077);

volatile union rw_pdf_0078h xdata g_rw_pdf_0078h  _at_  (PDF_FIELD_XMEM_BASE + 0x0078);
volatile union rw_pdf_0079h xdata g_rw_pdf_0079h  _at_  (PDF_FIELD_XMEM_BASE + 0x0079);
volatile union rw_pdf_007Ah xdata g_rw_pdf_007Ah  _at_  (PDF_FIELD_XMEM_BASE + 0x007A);
volatile union rw_pdf_007Bh xdata g_rw_pdf_007Bh  _at_  (PDF_FIELD_XMEM_BASE + 0x007B);
volatile union rw_pdf_007Ch xdata g_rw_pdf_007Ch  _at_  (PDF_FIELD_XMEM_BASE + 0x007C);

volatile union rw_pdf_007Dh xdata g_rw_pdf_007Dh  _at_  (PDF_FIELD_XMEM_BASE + 0x007D);
volatile union rw_pdf_007Eh xdata g_rw_pdf_007Eh  _at_  (PDF_FIELD_XMEM_BASE + 0x007E);
volatile union rw_pdf_007Fh xdata g_rw_pdf_007Fh  _at_  (PDF_FIELD_XMEM_BASE + 0x007F);
volatile union rw_pdf_0080h xdata g_rw_pdf_0080h  _at_  (PDF_FIELD_XMEM_BASE + 0x0080);
volatile union rw_pdf_0081h xdata g_rw_pdf_0081h  _at_  (PDF_FIELD_XMEM_BASE + 0x0081);

volatile union rw_pdf_0082h xdata g_rw_pdf_0082h  _at_  (PDF_FIELD_XMEM_BASE + 0x0082);
volatile union rw_pdf_0083h xdata g_rw_pdf_0083h  _at_  (PDF_FIELD_XMEM_BASE + 0x0083);
volatile union rw_pdf_0084h xdata g_rw_pdf_0084h  _at_  (PDF_FIELD_XMEM_BASE + 0x0084);
volatile union rw_pdf_0085h xdata g_rw_pdf_0085h  _at_  (PDF_FIELD_XMEM_BASE + 0x0085);
volatile union rw_pdf_0086h xdata g_rw_pdf_0086h  _at_  (PDF_FIELD_XMEM_BASE + 0x0086);

volatile union rw_pdf_0088h xdata g_rw_pdf_0088h  _at_  (PDF_FIELD_XMEM_BASE + 0x0088);
volatile union rw_pdf_008Bh xdata g_rw_pdf_008Bh  _at_  (PDF_FIELD_XMEM_BASE + 0x008B);
volatile union rw_pdf_008Eh xdata g_rw_pdf_008Eh  _at_  (PDF_FIELD_XMEM_BASE + 0x008E);
volatile union rw_pdf_0091h xdata g_rw_pdf_0091h  _at_  (PDF_FIELD_XMEM_BASE + 0x0091);
volatile union rw_pdf_0094h xdata g_rw_pdf_0094h  _at_  (PDF_FIELD_XMEM_BASE + 0x0094);

volatile union rw_pdf_0097h xdata g_rw_pdf_0097h  _at_  (PDF_FIELD_XMEM_BASE + 0x0097);
volatile union rw_pdf_009Ah xdata g_rw_pdf_009Ah  _at_  (PDF_FIELD_XMEM_BASE + 0x009A);
volatile union rw_pdf_009Dh xdata g_rw_pdf_009Dh  _at_  (PDF_FIELD_XMEM_BASE + 0x009D);
volatile union rw_pdf_00A0h xdata g_rw_pdf_00A0h  _at_  (PDF_FIELD_XMEM_BASE + 0x00A0);
volatile union rw_pdf_00A3h xdata g_rw_pdf_00A3h  _at_  (PDF_FIELD_XMEM_BASE + 0x00A3);

volatile union rw_pdf_00A6h xdata g_rw_pdf_00A6h  _at_  (PDF_FIELD_XMEM_BASE + 0x00A6);
volatile union rw_pdf_00A9h xdata g_rw_pdf_00A9h  _at_  (PDF_FIELD_XMEM_BASE + 0x00A9);
volatile union rw_pdf_00ACh xdata g_rw_pdf_00ACh  _at_  (PDF_FIELD_XMEM_BASE + 0x00AC);
volatile union rw_pdf_00AFh xdata g_rw_pdf_00AFh  _at_  (PDF_FIELD_XMEM_BASE + 0x00AF);
volatile union rw_pdf_00B2h xdata g_rw_pdf_00B2h  _at_  (PDF_FIELD_XMEM_BASE + 0x00B2);

volatile union rw_pdf_00B5h xdata g_rw_pdf_00B5h  _at_  (PDF_FIELD_XMEM_BASE + 0x00B5);
volatile union rw_pdf_00B8h xdata g_rw_pdf_00B8h  _at_  (PDF_FIELD_XMEM_BASE + 0x00B8);
volatile union rw_pdf_00BBh xdata g_rw_pdf_00BBh  _at_  (PDF_FIELD_XMEM_BASE + 0x00BB);
volatile union rw_pdf_00BEh xdata g_rw_pdf_00BEh  _at_  (PDF_FIELD_XMEM_BASE + 0x00BE);
volatile union rw_pdf_00C1h xdata g_rw_pdf_00C1h  _at_  (PDF_FIELD_XMEM_BASE + 0x00C1);

volatile union rw_pdf_00C4h xdata g_rw_pdf_00C4h  _at_  (PDF_FIELD_XMEM_BASE + 0x00C4);
volatile union rw_pdf_00C7h xdata g_rw_pdf_00C7h  _at_  (PDF_FIELD_XMEM_BASE + 0x00C7);
volatile union rw_pdf_00CAh xdata g_rw_pdf_00CAh  _at_  (PDF_FIELD_XMEM_BASE + 0x00CA);
volatile union rw_pdf_00CDh xdata g_rw_pdf_00CDh  _at_  (PDF_FIELD_XMEM_BASE + 0x00CD);
volatile union rw_pdf_00D0h xdata g_rw_pdf_00D0h  _at_  (PDF_FIELD_XMEM_BASE + 0x00D0);

volatile union rw_pdf_00D3h xdata g_rw_pdf_00D3h  _at_  (PDF_FIELD_XMEM_BASE + 0x00D3);
volatile union rw_pdf_00D6h xdata g_rw_pdf_00D6h  _at_  (PDF_FIELD_XMEM_BASE + 0x00D6);
volatile union rw_pdf_00D9h xdata g_rw_pdf_00D9h  _at_  (PDF_FIELD_XMEM_BASE + 0x00D9);
volatile union rw_pdf_00DCh xdata g_rw_pdf_00DCh  _at_  (PDF_FIELD_XMEM_BASE + 0x00DC);
volatile union rw_pdf_00DFh xdata g_rw_pdf_00DFh  _at_  (PDF_FIELD_XMEM_BASE + 0x00DF);

volatile union rw_pdf_00F1h xdata g_rw_pdf_00F1h  _at_  (PDF_FIELD_XMEM_BASE + 0x00F1);
volatile union rw_pdf_00F2h xdata g_rw_pdf_00F2h  _at_  (PDF_FIELD_XMEM_BASE + 0x00F2);
volatile union rw_pdf_00F3h xdata g_rw_pdf_00F3h  _at_  (PDF_FIELD_XMEM_BASE + 0x00F3);
volatile union rw_pdf_00F4h xdata g_rw_pdf_00F4h  _at_  (PDF_FIELD_XMEM_BASE + 0x00F4);
volatile union rw_pdf_00F5h xdata g_rw_pdf_00F5h  _at_  (PDF_FIELD_XMEM_BASE + 0x00F5);

volatile union rw_pdf_00F6h xdata g_rw_pdf_00F6h  _at_  (PDF_FIELD_XMEM_BASE + 0x00F6);
volatile union rw_pdf_00F7h xdata g_rw_pdf_00F7h  _at_  (PDF_FIELD_XMEM_BASE + 0x00F7);
volatile union rw_pdf_00F8h xdata g_rw_pdf_00F8h  _at_  (PDF_FIELD_XMEM_BASE + 0x00F8);
volatile union rw_pdf_00F9h xdata g_rw_pdf_00F9h  _at_  (PDF_FIELD_XMEM_BASE + 0x00F9);
volatile union rw_pdf_00FAh xdata g_rw_pdf_00FAh  _at_  (PDF_FIELD_XMEM_BASE + 0x00FA);

volatile union rw_pdf_00FBh xdata g_rw_pdf_00FBh  _at_  (PDF_FIELD_XMEM_BASE + 0x00FB);
volatile union rw_pdf_00FCh xdata g_rw_pdf_00FCh  _at_  (PDF_FIELD_XMEM_BASE + 0x00FC);
volatile union rw_pdf_00FDh xdata g_rw_pdf_00FDh  _at_  (PDF_FIELD_XMEM_BASE + 0x00FD);
volatile union rw_pdf_00FEh xdata g_rw_pdf_00FEh  _at_  (PDF_FIELD_XMEM_BASE + 0x00FE);
volatile union rw_pdf_00FFh xdata g_rw_pdf_00FFh  _at_  (PDF_FIELD_XMEM_BASE + 0x00FF);

volatile union rw_pdf_0100h xdata g_rw_pdf_0100h  _at_  (PDF_FIELD_XMEM_BASE + 0x0100);
volatile union rw_pdf_0101h xdata g_rw_pdf_0101h  _at_  (PDF_FIELD_XMEM_BASE + 0x0101);
volatile union rw_pdf_0102h xdata g_rw_pdf_0102h  _at_  (PDF_FIELD_XMEM_BASE + 0x0102);
volatile union rw_pdf_0103h xdata g_rw_pdf_0103h  _at_  (PDF_FIELD_XMEM_BASE + 0x0103);
volatile union rw_pdf_0104h xdata g_rw_pdf_0104h  _at_  (PDF_FIELD_XMEM_BASE + 0x0104);

volatile union rw_pdf_0105h xdata g_rw_pdf_0105h  _at_  (PDF_FIELD_XMEM_BASE + 0x0105);
volatile union rw_pdf_0106h xdata g_rw_pdf_0106h  _at_  (PDF_FIELD_XMEM_BASE + 0x0106);
volatile union rw_pdf_0107h xdata g_rw_pdf_0107h  _at_  (PDF_FIELD_XMEM_BASE + 0x0107);
volatile union rw_pdf_0108h xdata g_rw_pdf_0108h  _at_  (PDF_FIELD_XMEM_BASE + 0x0108);
volatile union rw_pdf_0109h xdata g_rw_pdf_0109h  _at_  (PDF_FIELD_XMEM_BASE + 0x0109);

volatile union rw_pdf_010Ah xdata g_rw_pdf_010Ah  _at_  (PDF_FIELD_XMEM_BASE + 0x010A);
volatile union rw_pdf_010Bh xdata g_rw_pdf_010Bh  _at_  (PDF_FIELD_XMEM_BASE + 0x010B);
volatile union rw_pdf_010Ch xdata g_rw_pdf_010Ch  _at_  (PDF_FIELD_XMEM_BASE + 0x010C);
volatile union rw_pdf_010Dh xdata g_rw_pdf_010Dh  _at_  (PDF_FIELD_XMEM_BASE + 0x010D);
volatile union rw_pdf_010Eh xdata g_rw_pdf_010Eh  _at_  (PDF_FIELD_XMEM_BASE + 0x010E);

volatile union rw_pdf_010Fh xdata g_rw_pdf_010Fh  _at_  (PDF_FIELD_XMEM_BASE + 0x010F);
volatile union rw_pdf_0110h xdata g_rw_pdf_0110h  _at_  (PDF_FIELD_XMEM_BASE + 0x0110);
volatile union rw_pdf_0111h xdata g_rw_pdf_0111h  _at_  (PDF_FIELD_XMEM_BASE + 0x0111);
volatile union rw_pdf_0112h xdata g_rw_pdf_0112h  _at_  (PDF_FIELD_XMEM_BASE + 0x0112);
volatile union rw_pdf_0113h xdata g_rw_pdf_0113h  _at_  (PDF_FIELD_XMEM_BASE + 0x0113);

volatile union rw_pdf_0114h xdata g_rw_pdf_0114h  _at_  (PDF_FIELD_XMEM_BASE + 0x0114);
volatile union rw_pdf_0115h xdata g_rw_pdf_0115h  _at_  (PDF_FIELD_XMEM_BASE + 0x0115);
volatile union rw_pdf_0116h xdata g_rw_pdf_0116h  _at_  (PDF_FIELD_XMEM_BASE + 0x0116);
volatile union rw_pdf_0117h xdata g_rw_pdf_0117h  _at_  (PDF_FIELD_XMEM_BASE + 0x0117);
volatile union rw_pdf_0118h xdata g_rw_pdf_0118h  _at_  (PDF_FIELD_XMEM_BASE + 0x0118);

volatile union rw_pdf_0119h xdata g_rw_pdf_0119h  _at_  (PDF_FIELD_XMEM_BASE + 0x0119);
volatile union rw_pdf_011Ah xdata g_rw_pdf_011Ah  _at_  (PDF_FIELD_XMEM_BASE + 0x011A);
volatile union rw_pdf_011Bh xdata g_rw_pdf_011Bh  _at_  (PDF_FIELD_XMEM_BASE + 0x011B);
volatile union rw_pdf_011Ch xdata g_rw_pdf_011Ch  _at_  (PDF_FIELD_XMEM_BASE + 0x011C);
volatile union rw_pdf_011Dh xdata g_rw_pdf_011Dh  _at_  (PDF_FIELD_XMEM_BASE + 0x011D);

volatile union rw_pdf_011Eh xdata g_rw_pdf_011Eh  _at_  (PDF_FIELD_XMEM_BASE + 0x011E);
volatile union rw_pdf_011Fh xdata g_rw_pdf_011Fh  _at_  (PDF_FIELD_XMEM_BASE + 0x011F);
volatile union rw_pdf_0120h xdata g_rw_pdf_0120h  _at_  (PDF_FIELD_XMEM_BASE + 0x0120);
volatile union rw_pdf_0121h xdata g_rw_pdf_0121h  _at_  (PDF_FIELD_XMEM_BASE + 0x0121);
volatile union rw_pdf_0122h xdata g_rw_pdf_0122h  _at_  (PDF_FIELD_XMEM_BASE + 0x0122);

volatile union rw_pdf_0123h xdata g_rw_pdf_0123h  _at_  (PDF_FIELD_XMEM_BASE + 0x0123);
volatile union rw_pdf_0124h xdata g_rw_pdf_0124h  _at_  (PDF_FIELD_XMEM_BASE + 0x0124);
volatile union rw_pdf_0125h xdata g_rw_pdf_0125h  _at_  (PDF_FIELD_XMEM_BASE + 0x0125);
volatile union rw_pdf_0126h xdata g_rw_pdf_0126h  _at_  (PDF_FIELD_XMEM_BASE + 0x0126);
volatile union rw_pdf_0127h xdata g_rw_pdf_0127h  _at_  (PDF_FIELD_XMEM_BASE + 0x0127);

volatile union rw_pdf_0128h xdata g_rw_pdf_0128h  _at_  (PDF_FIELD_XMEM_BASE + 0x0128);
volatile union rw_pdf_0129h xdata g_rw_pdf_0129h  _at_  (PDF_FIELD_XMEM_BASE + 0x0129);
volatile union rw_pdf_012Ah xdata g_rw_pdf_012Ah  _at_  (PDF_FIELD_XMEM_BASE + 0x012A);
volatile union rw_pdf_012Bh xdata g_rw_pdf_012Bh  _at_  (PDF_FIELD_XMEM_BASE + 0x012B);
volatile union rw_pdf_012Ch xdata g_rw_pdf_012Ch  _at_  (PDF_FIELD_XMEM_BASE + 0x012C);

volatile union rw_pdf_012Dh xdata g_rw_pdf_012Dh  _at_  (PDF_FIELD_XMEM_BASE + 0x012D);
volatile union rw_pdf_012Eh xdata g_rw_pdf_012Eh  _at_  (PDF_FIELD_XMEM_BASE + 0x012E);
volatile union rw_pdf_012Fh xdata g_rw_pdf_012Fh  _at_  (PDF_FIELD_XMEM_BASE + 0x012F);
volatile union rw_pdf_0130h xdata g_rw_pdf_0130h  _at_  (PDF_FIELD_XMEM_BASE + 0x0130);
volatile union rw_pdf_0131h xdata g_rw_pdf_0131h  _at_  (PDF_FIELD_XMEM_BASE + 0x0131);

volatile union rw_pdf_0132h xdata g_rw_pdf_0132h  _at_  (PDF_FIELD_XMEM_BASE + 0x0132);
volatile union rw_pdf_0133h xdata g_rw_pdf_0133h  _at_  (PDF_FIELD_XMEM_BASE + 0x0133);
volatile union rw_pdf_0134h xdata g_rw_pdf_0134h  _at_  (PDF_FIELD_XMEM_BASE + 0x0134);
volatile union rw_pdf_0135h xdata g_rw_pdf_0135h  _at_  (PDF_FIELD_XMEM_BASE + 0x0135);
volatile union rw_pdf_0136h xdata g_rw_pdf_0136h  _at_  (PDF_FIELD_XMEM_BASE + 0x0136);

volatile union rw_pdf_0137h xdata g_rw_pdf_0137h  _at_  (PDF_FIELD_XMEM_BASE + 0x0137);
volatile union rw_pdf_0138h xdata g_rw_pdf_0138h  _at_  (PDF_FIELD_XMEM_BASE + 0x0138);
volatile union rw_pdf_0139h xdata g_rw_pdf_0139h  _at_  (PDF_FIELD_XMEM_BASE + 0x0139);
volatile union rw_pdf_013Ah xdata g_rw_pdf_013Ah  _at_  (PDF_FIELD_XMEM_BASE + 0x013A);
volatile union rw_pdf_013Bh xdata g_rw_pdf_013Bh  _at_  (PDF_FIELD_XMEM_BASE + 0x013B);

volatile union rw_pdf_013Ch xdata g_rw_pdf_013Ch  _at_  (PDF_FIELD_XMEM_BASE + 0x013C);
volatile union rw_pdf_013Dh xdata g_rw_pdf_013Dh  _at_  (PDF_FIELD_XMEM_BASE + 0x013D);
volatile union rw_pdf_013Eh xdata g_rw_pdf_013Eh  _at_  (PDF_FIELD_XMEM_BASE + 0x013E);
volatile union rw_pdf_013Fh xdata g_rw_pdf_013Fh  _at_  (PDF_FIELD_XMEM_BASE + 0x013F);
volatile union rw_pdf_0140h xdata g_rw_pdf_0140h  _at_  (PDF_FIELD_XMEM_BASE + 0x0140);

volatile union rw_pdf_0141h xdata g_rw_pdf_0141h  _at_  (PDF_FIELD_XMEM_BASE + 0x0141);
volatile union rw_pdf_0142h xdata g_rw_pdf_0142h  _at_  (PDF_FIELD_XMEM_BASE + 0x0142);
volatile union rw_pdf_0143h xdata g_rw_pdf_0143h  _at_  (PDF_FIELD_XMEM_BASE + 0x0143);
volatile union rw_pdf_0144h xdata g_rw_pdf_0144h  _at_  (PDF_FIELD_XMEM_BASE + 0x0144);
volatile union rw_pdf_0145h xdata g_rw_pdf_0145h  _at_  (PDF_FIELD_XMEM_BASE + 0x0145);

volatile union rw_pdf_0146h xdata g_rw_pdf_0146h  _at_  (PDF_FIELD_XMEM_BASE + 0x0146);
volatile union rw_pdf_0147h xdata g_rw_pdf_0147h  _at_  (PDF_FIELD_XMEM_BASE + 0x0147);
volatile union rw_pdf_0148h xdata g_rw_pdf_0148h  _at_  (PDF_FIELD_XMEM_BASE + 0x0148);
volatile union rw_pdf_0149h xdata g_rw_pdf_0149h  _at_  (PDF_FIELD_XMEM_BASE + 0x0149);
volatile union rw_pdf_014Ah xdata g_rw_pdf_014Ah  _at_  (PDF_FIELD_XMEM_BASE + 0x014A);

volatile union rw_pdf_014Bh xdata g_rw_pdf_014Bh  _at_  (PDF_FIELD_XMEM_BASE + 0x014B);
volatile union rw_pdf_014Ch xdata g_rw_pdf_014Ch  _at_  (PDF_FIELD_XMEM_BASE + 0x014C);
volatile union rw_pdf_014Dh xdata g_rw_pdf_014Dh  _at_  (PDF_FIELD_XMEM_BASE + 0x014D);
volatile union rw_pdf_014Eh xdata g_rw_pdf_014Eh  _at_  (PDF_FIELD_XMEM_BASE + 0x014E);
volatile union rw_pdf_014Fh xdata g_rw_pdf_014Fh  _at_  (PDF_FIELD_XMEM_BASE + 0x014F);

volatile union rw_pdf_0150h xdata g_rw_pdf_0150h  _at_  (PDF_FIELD_XMEM_BASE + 0x0150);
volatile union rw_pdf_0151h xdata g_rw_pdf_0151h  _at_  (PDF_FIELD_XMEM_BASE + 0x0151);
volatile union rw_pdf_0152h xdata g_rw_pdf_0152h  _at_  (PDF_FIELD_XMEM_BASE + 0x0152);
volatile union rw_pdf_0153h xdata g_rw_pdf_0153h  _at_  (PDF_FIELD_XMEM_BASE + 0x0153);
volatile union rw_pdf_0154h xdata g_rw_pdf_0154h  _at_  (PDF_FIELD_XMEM_BASE + 0x0154);

volatile union rw_pdf_0155h xdata g_rw_pdf_0155h  _at_  (PDF_FIELD_XMEM_BASE + 0x0155);
volatile union rw_pdf_0156h xdata g_rw_pdf_0156h  _at_  (PDF_FIELD_XMEM_BASE + 0x0156);
volatile union rw_pdf_0157h xdata g_rw_pdf_0157h  _at_  (PDF_FIELD_XMEM_BASE + 0x0157);
volatile union rw_pdf_0158h xdata g_rw_pdf_0158h  _at_  (PDF_FIELD_XMEM_BASE + 0x0158);
volatile union rw_pdf_0159h xdata g_rw_pdf_0159h  _at_  (PDF_FIELD_XMEM_BASE + 0x0159);

volatile union rw_pdf_015Ah xdata g_rw_pdf_015Ah  _at_  (PDF_FIELD_XMEM_BASE + 0x015A);
volatile union rw_pdf_015Bh xdata g_rw_pdf_015Bh  _at_  (PDF_FIELD_XMEM_BASE + 0x015B);
volatile union rw_pdf_015Ch xdata g_rw_pdf_015Ch  _at_  (PDF_FIELD_XMEM_BASE + 0x015C);
volatile union rw_pdf_015Dh xdata g_rw_pdf_015Dh  _at_  (PDF_FIELD_XMEM_BASE + 0x015D);
volatile union rw_pdf_015Eh xdata g_rw_pdf_015Eh  _at_  (PDF_FIELD_XMEM_BASE + 0x015E);

volatile union rw_pdf_015Fh xdata g_rw_pdf_015Fh  _at_  (PDF_FIELD_XMEM_BASE + 0x015F);
volatile union rw_pdf_0160h xdata g_rw_pdf_0160h  _at_  (PDF_FIELD_XMEM_BASE + 0x0160);
volatile union rw_pdf_0161h xdata g_rw_pdf_0161h  _at_  (PDF_FIELD_XMEM_BASE + 0x0161);
volatile union rw_pdf_0162h xdata g_rw_pdf_0162h  _at_  (PDF_FIELD_XMEM_BASE + 0x0162);
volatile union rw_pdf_0163h xdata g_rw_pdf_0163h  _at_  (PDF_FIELD_XMEM_BASE + 0x0163);

volatile union rw_pdf_0164h xdata g_rw_pdf_0164h  _at_  (PDF_FIELD_XMEM_BASE + 0x0164);
volatile union rw_pdf_0165h xdata g_rw_pdf_0165h  _at_  (PDF_FIELD_XMEM_BASE + 0x0165);
volatile union rw_pdf_0166h xdata g_rw_pdf_0166h  _at_  (PDF_FIELD_XMEM_BASE + 0x0166);
volatile union rw_pdf_0167h xdata g_rw_pdf_0167h  _at_  (PDF_FIELD_XMEM_BASE + 0x0167);
volatile union rw_pdf_0168h xdata g_rw_pdf_0168h  _at_  (PDF_FIELD_XMEM_BASE + 0x0168);

volatile union rw_pdf_0169h xdata g_rw_pdf_0169h  _at_  (PDF_FIELD_XMEM_BASE + 0x0169);
volatile union rw_pdf_016Bh xdata g_rw_pdf_016Bh  _at_  (PDF_FIELD_XMEM_BASE + 0x016B);
volatile union rw_pdf_016Ch xdata g_rw_pdf_016Ch  _at_  (PDF_FIELD_XMEM_BASE + 0x016C);
volatile union rw_pdf_016Eh xdata g_rw_pdf_016Eh  _at_  (PDF_FIELD_XMEM_BASE + 0x016E);
volatile union rw_pdf_016Fh xdata g_rw_pdf_016Fh  _at_  (PDF_FIELD_XMEM_BASE + 0x016F);

volatile union rw_pdf_0171h xdata g_rw_pdf_0171h  _at_  (PDF_FIELD_XMEM_BASE + 0x0171);
volatile union rw_pdf_0172h xdata g_rw_pdf_0172h  _at_  (PDF_FIELD_XMEM_BASE + 0x0172);
volatile union rw_pdf_0174h xdata g_rw_pdf_0174h  _at_  (PDF_FIELD_XMEM_BASE + 0x0174);
volatile union rw_pdf_0175h xdata g_rw_pdf_0175h  _at_  (PDF_FIELD_XMEM_BASE + 0x0175);
volatile union rw_pdf_0176h xdata g_rw_pdf_0176h  _at_  (PDF_FIELD_XMEM_BASE + 0x0176);

volatile union rw_pdf_0177h xdata g_rw_pdf_0177h  _at_  (PDF_FIELD_XMEM_BASE + 0x0177);
volatile union rw_pdf_0178h xdata g_rw_pdf_0178h  _at_  (PDF_FIELD_XMEM_BASE + 0x0178);
volatile union rw_pdf_0179h xdata g_rw_pdf_0179h  _at_  (PDF_FIELD_XMEM_BASE + 0x0179);
volatile union rw_pdf_017Ah xdata g_rw_pdf_017Ah  _at_  (PDF_FIELD_XMEM_BASE + 0x017A);
volatile union rw_pdf_017Bh xdata g_rw_pdf_017Bh  _at_  (PDF_FIELD_XMEM_BASE + 0x017B);

volatile union rw_pdf_017Ch xdata g_rw_pdf_017Ch  _at_  (PDF_FIELD_XMEM_BASE + 0x017C);
volatile union rw_pdf_017Dh xdata g_rw_pdf_017Dh  _at_  (PDF_FIELD_XMEM_BASE + 0x017D);
volatile union rw_pdf_017Eh xdata g_rw_pdf_017Eh  _at_  (PDF_FIELD_XMEM_BASE + 0x017E);
volatile union rw_pdf_017Fh xdata g_rw_pdf_017Fh  _at_  (PDF_FIELD_XMEM_BASE + 0x017F);
volatile union rw_pdf_0180h xdata g_rw_pdf_0180h  _at_  (PDF_FIELD_XMEM_BASE + 0x0180);

volatile union rw_pdf_0181h xdata g_rw_pdf_0181h  _at_  (PDF_FIELD_XMEM_BASE + 0x0181);
volatile union rw_pdf_0182h xdata g_rw_pdf_0182h  _at_  (PDF_FIELD_XMEM_BASE + 0x0182);
volatile union rw_pdf_0183h xdata g_rw_pdf_0183h  _at_  (PDF_FIELD_XMEM_BASE + 0x0183);
volatile union rw_pdf_0184h xdata g_rw_pdf_0184h  _at_  (PDF_FIELD_XMEM_BASE + 0x0184);
volatile union rw_pdf_0185h xdata g_rw_pdf_0185h  _at_  (PDF_FIELD_XMEM_BASE + 0x0185);

volatile union rw_pdf_0186h xdata g_rw_pdf_0186h  _at_  (PDF_FIELD_XMEM_BASE + 0x0186);
volatile union rw_pdf_0187h xdata g_rw_pdf_0187h  _at_  (PDF_FIELD_XMEM_BASE + 0x0187);
volatile union rw_pdf_0188h xdata g_rw_pdf_0188h  _at_  (PDF_FIELD_XMEM_BASE + 0x0188);
volatile union rw_pdf_0189h xdata g_rw_pdf_0189h  _at_  (PDF_FIELD_XMEM_BASE + 0x0189);
volatile union rw_pdf_018Ah xdata g_rw_pdf_018Ah  _at_  (PDF_FIELD_XMEM_BASE + 0x018A);

volatile union rw_pdf_018Ch xdata g_rw_pdf_018Ch  _at_  (PDF_FIELD_XMEM_BASE + 0x018C);
volatile union rw_pdf_018Dh xdata g_rw_pdf_018Dh  _at_  (PDF_FIELD_XMEM_BASE + 0x018D);
volatile union rw_pdf_018Eh xdata g_rw_pdf_018Eh  _at_  (PDF_FIELD_XMEM_BASE + 0x018E);
volatile union rw_pdf_018Fh xdata g_rw_pdf_018Fh  _at_  (PDF_FIELD_XMEM_BASE + 0x018F);
volatile union rw_pdf_0190h xdata g_rw_pdf_0190h  _at_  (PDF_FIELD_XMEM_BASE + 0x0190);

volatile union rw_pdf_0191h xdata g_rw_pdf_0191h  _at_  (PDF_FIELD_XMEM_BASE + 0x0191);
volatile union rw_pdf_0192h xdata g_rw_pdf_0192h  _at_  (PDF_FIELD_XMEM_BASE + 0x0192);
volatile union rw_pdf_0195h xdata g_rw_pdf_0195h  _at_  (PDF_FIELD_XMEM_BASE + 0x0195);
volatile union rw_pdf_0196h xdata g_rw_pdf_0196h  _at_  (PDF_FIELD_XMEM_BASE + 0x0196);
volatile union rw_pdf_0197h xdata g_rw_pdf_0197h  _at_  (PDF_FIELD_XMEM_BASE + 0x0197);

volatile union rw_pdf_0198h xdata g_rw_pdf_0198h  _at_  (PDF_FIELD_XMEM_BASE + 0x0198);
volatile union rw_pdf_0199h xdata g_rw_pdf_0199h  _at_  (PDF_FIELD_XMEM_BASE + 0x0199);
volatile union rw_pdf_019Ah xdata g_rw_pdf_019Ah  _at_  (PDF_FIELD_XMEM_BASE + 0x019A);
volatile union rw_pdf_019Bh xdata g_rw_pdf_019Bh  _at_  (PDF_FIELD_XMEM_BASE + 0x019B);
volatile union rw_pdf_019Eh xdata g_rw_pdf_019Eh  _at_  (PDF_FIELD_XMEM_BASE + 0x019E);

volatile union rw_pdf_019Fh xdata g_rw_pdf_019Fh  _at_  (PDF_FIELD_XMEM_BASE + 0x019F);
volatile union rw_pdf_01A0h xdata g_rw_pdf_01A0h  _at_  (PDF_FIELD_XMEM_BASE + 0x01A0);
volatile union rw_pdf_01A1h xdata g_rw_pdf_01A1h  _at_  (PDF_FIELD_XMEM_BASE + 0x01A1);
volatile union rw_pdf_01A2h xdata g_rw_pdf_01A2h  _at_  (PDF_FIELD_XMEM_BASE + 0x01A2);
volatile union rw_pdf_01A3h xdata g_rw_pdf_01A3h  _at_  (PDF_FIELD_XMEM_BASE + 0x01A3);

volatile union rw_pdf_01A4h xdata g_rw_pdf_01A4h  _at_  (PDF_FIELD_XMEM_BASE + 0x01A4);
volatile union rw_pdf_01A5h xdata g_rw_pdf_01A5h  _at_  (PDF_FIELD_XMEM_BASE + 0x01A5);
volatile union rw_pdf_01A6h xdata g_rw_pdf_01A6h  _at_  (PDF_FIELD_XMEM_BASE + 0x01A6);
volatile union rw_pdf_01A7h xdata g_rw_pdf_01A7h  _at_  (PDF_FIELD_XMEM_BASE + 0x01A7);
volatile union rw_pdf_01A8h xdata g_rw_pdf_01A8h  _at_  (PDF_FIELD_XMEM_BASE + 0x01A8);

volatile union rw_pdf_01A9h xdata g_rw_pdf_01A9h  _at_  (PDF_FIELD_XMEM_BASE + 0x01A9);

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_pdf_0001h_gp00_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0001);
volatile uint8_t xdata g_rw_pdf_0002h_gp00_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0002);
volatile uint8_t xdata g_rw_pdf_0006h_gp01_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0006);
volatile uint8_t xdata g_rw_pdf_0007h_gp01_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0007);
volatile uint8_t xdata g_rw_pdf_000Bh_gp02_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x000B);

volatile uint8_t xdata g_rw_pdf_000Ch_gp02_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x000C);
volatile uint8_t xdata g_rw_pdf_0010h_gp03_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0010);
volatile uint8_t xdata g_rw_pdf_0011h_gp03_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0011);
volatile uint8_t xdata g_rw_pdf_0015h_gp04_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0015);
volatile uint8_t xdata g_rw_pdf_0016h_gp04_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0016);

volatile uint8_t xdata g_rw_pdf_001Ah_gp05_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x001A);
volatile uint8_t xdata g_rw_pdf_001Bh_gp05_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x001B);
volatile uint8_t xdata g_rw_pdf_001Fh_gp06_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x001F);
volatile uint8_t xdata g_rw_pdf_0020h_gp06_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0020);
volatile uint8_t xdata g_rw_pdf_0024h_gp07_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0024);

volatile uint8_t xdata g_rw_pdf_0025h_gp07_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0025);
volatile uint8_t xdata g_rw_pdf_0029h_gp08_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0029);
volatile uint8_t xdata g_rw_pdf_002Ah_gp08_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x002A);
volatile uint8_t xdata g_rw_pdf_002Eh_gp09_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x002E);
volatile uint8_t xdata g_rw_pdf_002Fh_gp09_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x002F);

volatile uint8_t xdata g_rw_pdf_0033h_gp10_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0033);
volatile uint8_t xdata g_rw_pdf_0034h_gp10_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0034);
volatile uint8_t xdata g_rw_pdf_0036h_gp10_pat_no3                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0036);
volatile uint8_t xdata g_rw_pdf_0037h_gp10_pat_no4                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0037);
volatile uint8_t xdata g_rw_pdf_003Bh_gp11_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x003B);

volatile uint8_t xdata g_rw_pdf_003Ch_gp11_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x003C);
volatile uint8_t xdata g_rw_pdf_003Eh_gp11_pat_no3                                             _at_  (PDF_FIELD_XMEM_BASE + 0x003E);
volatile uint8_t xdata g_rw_pdf_003Fh_gp11_pat_no4                                             _at_  (PDF_FIELD_XMEM_BASE + 0x003F);
volatile uint8_t xdata g_rw_pdf_0043h_gp12_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0043);
volatile uint8_t xdata g_rw_pdf_0044h_gp12_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0044);

volatile uint8_t xdata g_rw_pdf_0046h_gp12_pat_no3                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0046);
volatile uint8_t xdata g_rw_pdf_0047h_gp12_pat_no4                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0047);
volatile uint8_t xdata g_rw_pdf_0049h_gp12_pat_no5                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0049);
volatile uint8_t xdata g_rw_pdf_004Ah_gp12_pat_no6                                             _at_  (PDF_FIELD_XMEM_BASE + 0x004A);
volatile uint8_t xdata g_rw_pdf_004Ch_gp12_pat_no7                                             _at_  (PDF_FIELD_XMEM_BASE + 0x004C);

volatile uint8_t xdata g_rw_pdf_004Dh_gp12_pat_no8                                             _at_  (PDF_FIELD_XMEM_BASE + 0x004D);
volatile uint8_t xdata g_rw_pdf_0051h_gp13_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0051);
volatile uint8_t xdata g_rw_pdf_0052h_gp13_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0052);
volatile uint8_t xdata g_rw_pdf_0056h_gp14_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0056);
volatile uint8_t xdata g_rw_pdf_0057h_gp14_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0057);

volatile uint8_t xdata g_rw_pdf_005Bh_gp15_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x005B);
volatile uint8_t xdata g_rw_pdf_005Ch_gp15_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x005C);
volatile uint8_t xdata g_rw_pdf_0060h_gp16_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0060);
volatile uint8_t xdata g_rw_pdf_0061h_gp16_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0061);
volatile uint8_t xdata g_rw_pdf_0065h_gp17_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0065);

volatile uint8_t xdata g_rw_pdf_0066h_gp17_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0066);
volatile uint8_t xdata g_rw_pdf_006Ah_gp18_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x006A);
volatile uint8_t xdata g_rw_pdf_006Bh_gp18_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x006B);
volatile uint8_t xdata g_rw_pdf_006Fh_gp19_pat_no1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x006F);
volatile uint8_t xdata g_rw_pdf_0070h_gp19_pat_no2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x0070);

volatile uint8_t xdata g_rw_pdf_0087h_gp00_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x0087);
volatile uint8_t xdata g_rw_pdf_0089h_gp01_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x0089);
volatile uint8_t xdata g_rw_pdf_008Ah_gp02_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x008A);
volatile uint8_t xdata g_rw_pdf_008Ch_gp03_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x008C);
volatile uint8_t xdata g_rw_pdf_008Dh_gp04_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x008D);

volatile uint8_t xdata g_rw_pdf_008Fh_gp05_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x008F);
volatile uint8_t xdata g_rw_pdf_0090h_gp06_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x0090);
volatile uint8_t xdata g_rw_pdf_0092h_gp07_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x0092);
volatile uint8_t xdata g_rw_pdf_0093h_gp08_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x0093);
volatile uint8_t xdata g_rw_pdf_0095h_gp09_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x0095);

volatile uint8_t xdata g_rw_pdf_0096h_gp10_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x0096);
volatile uint8_t xdata g_rw_pdf_0098h_gp11_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x0098);
volatile uint8_t xdata g_rw_pdf_0099h_gp12_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x0099);
volatile uint8_t xdata g_rw_pdf_009Bh_gp13_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x009B);
volatile uint8_t xdata g_rw_pdf_009Ch_gp14_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x009C);

volatile uint8_t xdata g_rw_pdf_009Eh_gp15_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x009E);
volatile uint8_t xdata g_rw_pdf_009Fh_gp16_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x009F);
volatile uint8_t xdata g_rw_pdf_00A1h_gp17_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00A1);
volatile uint8_t xdata g_rw_pdf_00A2h_gp18_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00A2);
volatile uint8_t xdata g_rw_pdf_00A4h_gp19_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00A4);

volatile uint8_t xdata g_rw_pdf_00A5h_gp20_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00A5);
volatile uint8_t xdata g_rw_pdf_00A7h_gp21_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00A7);
volatile uint8_t xdata g_rw_pdf_00A8h_gp22_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00A8);
volatile uint8_t xdata g_rw_pdf_00AAh_gp23_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00AA);
volatile uint8_t xdata g_rw_pdf_00ABh_gp24_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00AB);

volatile uint8_t xdata g_rw_pdf_00ADh_gp25_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00AD);
volatile uint8_t xdata g_rw_pdf_00AEh_gp26_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00AE);
volatile uint8_t xdata g_rw_pdf_00B0h_gp27_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00B0);
volatile uint8_t xdata g_rw_pdf_00B1h_gp28_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00B1);
volatile uint8_t xdata g_rw_pdf_00B3h_gp29_acc_hgh_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00B3);

volatile uint8_t xdata g_rw_pdf_00B4h_gp00_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00B4);
volatile uint8_t xdata g_rw_pdf_00B6h_gp01_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00B6);
volatile uint8_t xdata g_rw_pdf_00B7h_gp02_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00B7);
volatile uint8_t xdata g_rw_pdf_00B9h_gp03_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00B9);
volatile uint8_t xdata g_rw_pdf_00BAh_gp04_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00BA);

volatile uint8_t xdata g_rw_pdf_00BCh_gp05_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00BC);
volatile uint8_t xdata g_rw_pdf_00BDh_gp06_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00BD);
volatile uint8_t xdata g_rw_pdf_00BFh_gp07_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00BF);
volatile uint8_t xdata g_rw_pdf_00C0h_gp08_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00C0);
volatile uint8_t xdata g_rw_pdf_00C2h_gp09_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00C2);

volatile uint8_t xdata g_rw_pdf_00C3h_gp10_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00C3);
volatile uint8_t xdata g_rw_pdf_00C5h_gp11_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00C5);
volatile uint8_t xdata g_rw_pdf_00C6h_gp12_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00C6);
volatile uint8_t xdata g_rw_pdf_00C8h_gp13_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00C8);
volatile uint8_t xdata g_rw_pdf_00C9h_gp14_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00C9);

volatile uint8_t xdata g_rw_pdf_00CBh_gp15_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00CB);
volatile uint8_t xdata g_rw_pdf_00CCh_gp16_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00CC);
volatile uint8_t xdata g_rw_pdf_00CEh_gp17_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00CE);
volatile uint8_t xdata g_rw_pdf_00CFh_gp18_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00CF);
volatile uint8_t xdata g_rw_pdf_00D1h_gp19_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00D1);

volatile uint8_t xdata g_rw_pdf_00D2h_gp20_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00D2);
volatile uint8_t xdata g_rw_pdf_00D4h_gp21_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00D4);
volatile uint8_t xdata g_rw_pdf_00D5h_gp22_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00D5);
volatile uint8_t xdata g_rw_pdf_00D7h_gp23_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00D7);
volatile uint8_t xdata g_rw_pdf_00D8h_gp24_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00D8);

volatile uint8_t xdata g_rw_pdf_00DAh_gp25_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00DA);
volatile uint8_t xdata g_rw_pdf_00DBh_gp26_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00DB);
volatile uint8_t xdata g_rw_pdf_00DDh_gp27_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00DD);
volatile uint8_t xdata g_rw_pdf_00DEh_gp28_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00DE);
volatile uint8_t xdata g_rw_pdf_00E0h_gp29_acc_low_th                                          _at_  (PDF_FIELD_XMEM_BASE + 0x00E0);

volatile uint8_t xdata g_rw_pdf_00E1h_hgh_gray_th0                                             _at_  (PDF_FIELD_XMEM_BASE + 0x00E1);
volatile uint8_t xdata g_rw_pdf_00E2h_hgh_gray_th1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x00E2);
volatile uint8_t xdata g_rw_pdf_00E3h_hgh_gray_th2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x00E3);
volatile uint8_t xdata g_rw_pdf_00E4h_hgh_gray_th3                                             _at_  (PDF_FIELD_XMEM_BASE + 0x00E4);
volatile uint8_t xdata g_rw_pdf_00E5h_hgh_gray_th4                                             _at_  (PDF_FIELD_XMEM_BASE + 0x00E5);

volatile uint8_t xdata g_rw_pdf_00E6h_hgh_gray_th5                                             _at_  (PDF_FIELD_XMEM_BASE + 0x00E6);
volatile uint8_t xdata g_rw_pdf_00E7h_hgh_gray_th6                                             _at_  (PDF_FIELD_XMEM_BASE + 0x00E7);
volatile uint8_t xdata g_rw_pdf_00E8h_hgh_gray_th7                                             _at_  (PDF_FIELD_XMEM_BASE + 0x00E8);
volatile uint8_t xdata g_rw_pdf_00E9h_low_gray_th0                                             _at_  (PDF_FIELD_XMEM_BASE + 0x00E9);
volatile uint8_t xdata g_rw_pdf_00EAh_low_gray_th1                                             _at_  (PDF_FIELD_XMEM_BASE + 0x00EA);

volatile uint8_t xdata g_rw_pdf_00EBh_low_gray_th2                                             _at_  (PDF_FIELD_XMEM_BASE + 0x00EB);
volatile uint8_t xdata g_rw_pdf_00ECh_low_gray_th3                                             _at_  (PDF_FIELD_XMEM_BASE + 0x00EC);
volatile uint8_t xdata g_rw_pdf_00EDh_low_gray_th4                                             _at_  (PDF_FIELD_XMEM_BASE + 0x00ED);
volatile uint8_t xdata g_rw_pdf_00EEh_low_gray_th5                                             _at_  (PDF_FIELD_XMEM_BASE + 0x00EE);
volatile uint8_t xdata g_rw_pdf_00EFh_low_gray_th6                                             _at_  (PDF_FIELD_XMEM_BASE + 0x00EF);

volatile uint8_t xdata g_rw_pdf_00F0h_low_gray_th7                                             _at_  (PDF_FIELD_XMEM_BASE + 0x00F0);
volatile uint8_t xdata g_rw_pdf_016Ah_gp12_pat_no9                                             _at_  (PDF_FIELD_XMEM_BASE + 0x016A);
volatile uint8_t xdata g_rw_pdf_016Dh_gp12_pat_no11                                            _at_  (PDF_FIELD_XMEM_BASE + 0x016D);
volatile uint8_t xdata g_rw_pdf_0170h_gp12_pat_no13                                            _at_  (PDF_FIELD_XMEM_BASE + 0x0170);
volatile uint8_t xdata g_rw_pdf_0173h_gp12_pat_no15                                            _at_  (PDF_FIELD_XMEM_BASE + 0x0173);

volatile uint8_t xdata g_rw_pdf_018Bh_region_width00                                           _at_  (PDF_FIELD_XMEM_BASE + 0x018B);
volatile uint8_t xdata g_rw_pdf_0193h_region_width07                                           _at_  (PDF_FIELD_XMEM_BASE + 0x0193);
volatile uint8_t xdata g_rw_pdf_0194h_region_width08                                           _at_  (PDF_FIELD_XMEM_BASE + 0x0194);
volatile uint8_t xdata g_rw_pdf_019Ch_region_width15                                           _at_  (PDF_FIELD_XMEM_BASE + 0x019C);
volatile uint8_t xdata g_rw_pdf_019Dh_region_width16                                           _at_  (PDF_FIELD_XMEM_BASE + 0x019D);


#endif 
