#include "gam_lut.h"
#include "hw_mem_map.h"

#ifdef GAM_LUT_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_gam_lut_0000h_GAM_LUT_0                                            _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0000);
volatile uint8_t xdata g_rw_gam_lut_0001h_GAM_LUT_1                                            _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0001);
volatile uint8_t xdata g_rw_gam_lut_0002h_GAM_LUT_2                                            _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0002);
volatile uint8_t xdata g_rw_gam_lut_0003h_GAM_LUT_3                                            _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0003);
volatile uint8_t xdata g_rw_gam_lut_0004h_GAM_LUT_4                                            _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0004);

volatile uint8_t xdata g_rw_gam_lut_0005h_GAM_LUT_5                                            _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0005);
volatile uint8_t xdata g_rw_gam_lut_0006h_GAM_LUT_6                                            _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0006);
volatile uint8_t xdata g_rw_gam_lut_0007h_GAM_LUT_7                                            _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0007);
volatile uint8_t xdata g_rw_gam_lut_0008h_GAM_LUT_8                                            _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0008);
volatile uint8_t xdata g_rw_gam_lut_0009h_GAM_LUT_9                                            _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0009);

volatile uint8_t xdata g_rw_gam_lut_000Ah_GAM_LUT_10                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x000A);
volatile uint8_t xdata g_rw_gam_lut_000Bh_GAM_LUT_11                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x000B);
volatile uint8_t xdata g_rw_gam_lut_000Ch_GAM_LUT_12                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x000C);
volatile uint8_t xdata g_rw_gam_lut_000Dh_GAM_LUT_13                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x000D);
volatile uint8_t xdata g_rw_gam_lut_000Eh_GAM_LUT_14                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x000E);

volatile uint8_t xdata g_rw_gam_lut_000Fh_GAM_LUT_15                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x000F);
volatile uint8_t xdata g_rw_gam_lut_0010h_GAM_LUT_16                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0010);
volatile uint8_t xdata g_rw_gam_lut_0011h_GAM_LUT_17                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0011);
volatile uint8_t xdata g_rw_gam_lut_0012h_GAM_LUT_18                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0012);
volatile uint8_t xdata g_rw_gam_lut_0013h_GAM_LUT_19                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0013);

volatile uint8_t xdata g_rw_gam_lut_0014h_GAM_LUT_20                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0014);
volatile uint8_t xdata g_rw_gam_lut_0015h_GAM_LUT_21                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0015);
volatile uint8_t xdata g_rw_gam_lut_0016h_GAM_LUT_22                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0016);
volatile uint8_t xdata g_rw_gam_lut_0017h_GAM_LUT_23                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0017);
volatile uint8_t xdata g_rw_gam_lut_0018h_GAM_LUT_24                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0018);

volatile uint8_t xdata g_rw_gam_lut_0019h_GAM_LUT_25                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0019);
volatile uint8_t xdata g_rw_gam_lut_001Ah_GAM_LUT_26                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x001A);
volatile uint8_t xdata g_rw_gam_lut_001Bh_GAM_LUT_27                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x001B);
volatile uint8_t xdata g_rw_gam_lut_001Ch_GAM_LUT_28                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x001C);
volatile uint8_t xdata g_rw_gam_lut_001Dh_GAM_LUT_29                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x001D);

volatile uint8_t xdata g_rw_gam_lut_001Eh_GAM_LUT_30                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x001E);
volatile uint8_t xdata g_rw_gam_lut_001Fh_GAM_LUT_31                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x001F);
volatile uint8_t xdata g_rw_gam_lut_0020h_GAM_LUT_32                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0020);
volatile uint8_t xdata g_rw_gam_lut_0021h_GAM_LUT_33                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0021);
volatile uint8_t xdata g_rw_gam_lut_0022h_GAM_LUT_34                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0022);

volatile uint8_t xdata g_rw_gam_lut_0023h_GAM_LUT_35                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0023);
volatile uint8_t xdata g_rw_gam_lut_0024h_GAM_LUT_36                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0024);
volatile uint8_t xdata g_rw_gam_lut_0025h_GAM_LUT_37                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0025);
volatile uint8_t xdata g_rw_gam_lut_0026h_GAM_LUT_38                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0026);
volatile uint8_t xdata g_rw_gam_lut_0027h_GAM_LUT_39                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0027);

volatile uint8_t xdata g_rw_gam_lut_0028h_GAM_LUT_40                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0028);
volatile uint8_t xdata g_rw_gam_lut_0029h_GAM_LUT_41                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0029);
volatile uint8_t xdata g_rw_gam_lut_002Ah_GAM_LUT_42                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x002A);
volatile uint8_t xdata g_rw_gam_lut_002Bh_GAM_LUT_43                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x002B);
volatile uint8_t xdata g_rw_gam_lut_002Ch_GAM_LUT_44                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x002C);

volatile uint8_t xdata g_rw_gam_lut_002Dh_GAM_LUT_45                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x002D);
volatile uint8_t xdata g_rw_gam_lut_002Eh_GAM_LUT_46                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x002E);
volatile uint8_t xdata g_rw_gam_lut_002Fh_GAM_LUT_47                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x002F);
volatile uint8_t xdata g_rw_gam_lut_0030h_GAM_LUT_48                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0030);
volatile uint8_t xdata g_rw_gam_lut_0031h_GAM_LUT_49                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0031);

volatile uint8_t xdata g_rw_gam_lut_0032h_GAM_LUT_50                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0032);
volatile uint8_t xdata g_rw_gam_lut_0033h_GAM_LUT_51                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0033);
volatile uint8_t xdata g_rw_gam_lut_0034h_GAM_LUT_52                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0034);
volatile uint8_t xdata g_rw_gam_lut_0035h_GAM_LUT_53                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0035);
volatile uint8_t xdata g_rw_gam_lut_0036h_GAM_LUT_54                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0036);

volatile uint8_t xdata g_rw_gam_lut_0037h_GAM_LUT_55                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0037);
volatile uint8_t xdata g_rw_gam_lut_0038h_GAM_LUT_56                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0038);
volatile uint8_t xdata g_rw_gam_lut_0039h_GAM_LUT_57                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0039);
volatile uint8_t xdata g_rw_gam_lut_003Ah_GAM_LUT_58                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x003A);
volatile uint8_t xdata g_rw_gam_lut_003Bh_GAM_LUT_59                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x003B);

volatile uint8_t xdata g_rw_gam_lut_003Ch_GAM_LUT_60                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x003C);
volatile uint8_t xdata g_rw_gam_lut_003Dh_GAM_LUT_61                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x003D);
volatile uint8_t xdata g_rw_gam_lut_003Eh_GAM_LUT_62                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x003E);
volatile uint8_t xdata g_rw_gam_lut_003Fh_GAM_LUT_63                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x003F);
volatile uint8_t xdata g_rw_gam_lut_0040h_GAM_LUT_64                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0040);

volatile uint8_t xdata g_rw_gam_lut_0041h_GAM_LUT_65                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0041);
volatile uint8_t xdata g_rw_gam_lut_0042h_GAM_LUT_66                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0042);
volatile uint8_t xdata g_rw_gam_lut_0043h_GAM_LUT_67                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0043);
volatile uint8_t xdata g_rw_gam_lut_0044h_GAM_LUT_68                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0044);
volatile uint8_t xdata g_rw_gam_lut_0045h_GAM_LUT_69                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0045);

volatile uint8_t xdata g_rw_gam_lut_0046h_GAM_LUT_70                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0046);
volatile uint8_t xdata g_rw_gam_lut_0047h_GAM_LUT_71                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0047);
volatile uint8_t xdata g_rw_gam_lut_0048h_GAM_LUT_72                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0048);
volatile uint8_t xdata g_rw_gam_lut_0049h_GAM_LUT_73                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0049);
volatile uint8_t xdata g_rw_gam_lut_004Ah_GAM_LUT_74                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x004A);

volatile uint8_t xdata g_rw_gam_lut_004Bh_GAM_LUT_75                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x004B);
volatile uint8_t xdata g_rw_gam_lut_004Ch_GAM_LUT_76                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x004C);
volatile uint8_t xdata g_rw_gam_lut_004Dh_GAM_LUT_77                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x004D);
volatile uint8_t xdata g_rw_gam_lut_004Eh_GAM_LUT_78                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x004E);
volatile uint8_t xdata g_rw_gam_lut_004Fh_GAM_LUT_79                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x004F);

volatile uint8_t xdata g_rw_gam_lut_0050h_GAM_LUT_80                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0050);
volatile uint8_t xdata g_rw_gam_lut_0051h_GAM_LUT_81                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0051);
volatile uint8_t xdata g_rw_gam_lut_0052h_GAM_LUT_82                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0052);
volatile uint8_t xdata g_rw_gam_lut_0053h_GAM_LUT_83                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0053);
volatile uint8_t xdata g_rw_gam_lut_0054h_GAM_LUT_84                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0054);

volatile uint8_t xdata g_rw_gam_lut_0055h_GAM_LUT_85                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0055);
volatile uint8_t xdata g_rw_gam_lut_0056h_GAM_LUT_86                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0056);
volatile uint8_t xdata g_rw_gam_lut_0057h_GAM_LUT_87                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0057);
volatile uint8_t xdata g_rw_gam_lut_0058h_GAM_LUT_88                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0058);
volatile uint8_t xdata g_rw_gam_lut_0059h_GAM_LUT_89                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0059);

volatile uint8_t xdata g_rw_gam_lut_005Ah_GAM_LUT_90                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x005A);
volatile uint8_t xdata g_rw_gam_lut_005Bh_GAM_LUT_91                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x005B);
volatile uint8_t xdata g_rw_gam_lut_005Ch_GAM_LUT_92                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x005C);
volatile uint8_t xdata g_rw_gam_lut_005Dh_GAM_LUT_93                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x005D);
volatile uint8_t xdata g_rw_gam_lut_005Eh_GAM_LUT_94                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x005E);

volatile uint8_t xdata g_rw_gam_lut_005Fh_GAM_LUT_95                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x005F);
volatile uint8_t xdata g_rw_gam_lut_0060h_GAM_LUT_96                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0060);
volatile uint8_t xdata g_rw_gam_lut_0061h_GAM_LUT_97                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0061);
volatile uint8_t xdata g_rw_gam_lut_0062h_GAM_LUT_98                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0062);
volatile uint8_t xdata g_rw_gam_lut_0063h_GAM_LUT_99                                           _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0063);

volatile uint8_t xdata g_rw_gam_lut_0064h_GAM_LUT_100                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0064);
volatile uint8_t xdata g_rw_gam_lut_0065h_GAM_LUT_101                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0065);
volatile uint8_t xdata g_rw_gam_lut_0066h_GAM_LUT_102                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0066);
volatile uint8_t xdata g_rw_gam_lut_0067h_GAM_LUT_103                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0067);
volatile uint8_t xdata g_rw_gam_lut_0068h_GAM_LUT_104                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0068);

volatile uint8_t xdata g_rw_gam_lut_0069h_GAM_LUT_105                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0069);
volatile uint8_t xdata g_rw_gam_lut_006Ah_GAM_LUT_106                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x006A);
volatile uint8_t xdata g_rw_gam_lut_006Bh_GAM_LUT_107                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x006B);
volatile uint8_t xdata g_rw_gam_lut_006Ch_GAM_LUT_108                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x006C);
volatile uint8_t xdata g_rw_gam_lut_006Dh_GAM_LUT_109                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x006D);

volatile uint8_t xdata g_rw_gam_lut_006Eh_GAM_LUT_110                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x006E);
volatile uint8_t xdata g_rw_gam_lut_006Fh_GAM_LUT_111                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x006F);
volatile uint8_t xdata g_rw_gam_lut_0070h_GAM_LUT_112                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0070);
volatile uint8_t xdata g_rw_gam_lut_0071h_GAM_LUT_113                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0071);
volatile uint8_t xdata g_rw_gam_lut_0072h_GAM_LUT_114                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0072);

volatile uint8_t xdata g_rw_gam_lut_0073h_GAM_LUT_115                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0073);
volatile uint8_t xdata g_rw_gam_lut_0074h_GAM_LUT_116                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0074);
volatile uint8_t xdata g_rw_gam_lut_0075h_GAM_LUT_117                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0075);
volatile uint8_t xdata g_rw_gam_lut_0076h_GAM_LUT_118                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0076);
volatile uint8_t xdata g_rw_gam_lut_0077h_GAM_LUT_119                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0077);

volatile uint8_t xdata g_rw_gam_lut_0078h_GAM_LUT_120                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0078);
volatile uint8_t xdata g_rw_gam_lut_0079h_GAM_LUT_121                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0079);
volatile uint8_t xdata g_rw_gam_lut_007Ah_GAM_LUT_122                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x007A);
volatile uint8_t xdata g_rw_gam_lut_007Bh_GAM_LUT_123                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x007B);
volatile uint8_t xdata g_rw_gam_lut_007Ch_GAM_LUT_124                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x007C);

volatile uint8_t xdata g_rw_gam_lut_007Dh_GAM_LUT_125                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x007D);
volatile uint8_t xdata g_rw_gam_lut_007Eh_GAM_LUT_126                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x007E);
volatile uint8_t xdata g_rw_gam_lut_007Fh_GAM_LUT_127                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x007F);
volatile uint8_t xdata g_rw_gam_lut_0080h_GAM_LUT_128                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0080);
volatile uint8_t xdata g_rw_gam_lut_0081h_GAM_LUT_129                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0081);

volatile uint8_t xdata g_rw_gam_lut_0082h_GAM_LUT_130                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0082);
volatile uint8_t xdata g_rw_gam_lut_0083h_GAM_LUT_131                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0083);
volatile uint8_t xdata g_rw_gam_lut_0084h_GAM_LUT_132                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0084);
volatile uint8_t xdata g_rw_gam_lut_0085h_GAM_LUT_133                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0085);
volatile uint8_t xdata g_rw_gam_lut_0086h_GAM_LUT_134                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0086);

volatile uint8_t xdata g_rw_gam_lut_0087h_GAM_LUT_135                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0087);
volatile uint8_t xdata g_rw_gam_lut_0088h_GAM_LUT_136                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0088);
volatile uint8_t xdata g_rw_gam_lut_0089h_GAM_LUT_137                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0089);
volatile uint8_t xdata g_rw_gam_lut_008Ah_GAM_LUT_138                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x008A);
volatile uint8_t xdata g_rw_gam_lut_008Bh_GAM_LUT_139                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x008B);

volatile uint8_t xdata g_rw_gam_lut_008Ch_GAM_LUT_140                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x008C);
volatile uint8_t xdata g_rw_gam_lut_008Dh_GAM_LUT_141                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x008D);
volatile uint8_t xdata g_rw_gam_lut_008Eh_GAM_LUT_142                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x008E);
volatile uint8_t xdata g_rw_gam_lut_008Fh_GAM_LUT_143                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x008F);
volatile uint8_t xdata g_rw_gam_lut_0090h_GAM_LUT_144                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0090);

volatile uint8_t xdata g_rw_gam_lut_0091h_GAM_LUT_145                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0091);
volatile uint8_t xdata g_rw_gam_lut_0092h_GAM_LUT_146                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0092);
volatile uint8_t xdata g_rw_gam_lut_0093h_GAM_LUT_147                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0093);
volatile uint8_t xdata g_rw_gam_lut_0094h_GAM_LUT_148                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0094);
volatile uint8_t xdata g_rw_gam_lut_0095h_GAM_LUT_149                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0095);

volatile uint8_t xdata g_rw_gam_lut_0096h_GAM_LUT_150                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0096);
volatile uint8_t xdata g_rw_gam_lut_0097h_GAM_LUT_151                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0097);
volatile uint8_t xdata g_rw_gam_lut_0098h_GAM_LUT_152                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0098);
volatile uint8_t xdata g_rw_gam_lut_0099h_GAM_LUT_153                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0099);
volatile uint8_t xdata g_rw_gam_lut_009Ah_GAM_LUT_154                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x009A);

volatile uint8_t xdata g_rw_gam_lut_009Bh_GAM_LUT_155                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x009B);
volatile uint8_t xdata g_rw_gam_lut_009Ch_GAM_LUT_156                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x009C);
volatile uint8_t xdata g_rw_gam_lut_009Dh_GAM_LUT_157                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x009D);
volatile uint8_t xdata g_rw_gam_lut_009Eh_GAM_LUT_158                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x009E);
volatile uint8_t xdata g_rw_gam_lut_009Fh_GAM_LUT_159                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x009F);

volatile uint8_t xdata g_rw_gam_lut_00A0h_GAM_LUT_160                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00A0);
volatile uint8_t xdata g_rw_gam_lut_00A1h_GAM_LUT_161                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00A1);
volatile uint8_t xdata g_rw_gam_lut_00A2h_GAM_LUT_162                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00A2);
volatile uint8_t xdata g_rw_gam_lut_00A3h_GAM_LUT_163                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00A3);
volatile uint8_t xdata g_rw_gam_lut_00A4h_GAM_LUT_164                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00A4);

volatile uint8_t xdata g_rw_gam_lut_00A5h_GAM_LUT_165                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00A5);
volatile uint8_t xdata g_rw_gam_lut_00A6h_GAM_LUT_166                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00A6);
volatile uint8_t xdata g_rw_gam_lut_00A7h_GAM_LUT_167                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00A7);
volatile uint8_t xdata g_rw_gam_lut_00A8h_GAM_LUT_168                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00A8);
volatile uint8_t xdata g_rw_gam_lut_00A9h_GAM_LUT_169                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00A9);

volatile uint8_t xdata g_rw_gam_lut_00AAh_GAM_LUT_170                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00AA);
volatile uint8_t xdata g_rw_gam_lut_00ABh_GAM_LUT_171                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00AB);
volatile uint8_t xdata g_rw_gam_lut_00ACh_GAM_LUT_172                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00AC);
volatile uint8_t xdata g_rw_gam_lut_00ADh_GAM_LUT_173                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00AD);
volatile uint8_t xdata g_rw_gam_lut_00AEh_GAM_LUT_174                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00AE);

volatile uint8_t xdata g_rw_gam_lut_00AFh_GAM_LUT_175                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00AF);
volatile uint8_t xdata g_rw_gam_lut_00B0h_GAM_LUT_176                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00B0);
volatile uint8_t xdata g_rw_gam_lut_00B1h_GAM_LUT_177                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00B1);
volatile uint8_t xdata g_rw_gam_lut_00B2h_GAM_LUT_178                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00B2);
volatile uint8_t xdata g_rw_gam_lut_00B3h_GAM_LUT_179                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00B3);

volatile uint8_t xdata g_rw_gam_lut_00B4h_GAM_LUT_180                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00B4);
volatile uint8_t xdata g_rw_gam_lut_00B5h_GAM_LUT_181                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00B5);
volatile uint8_t xdata g_rw_gam_lut_00B6h_GAM_LUT_182                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00B6);
volatile uint8_t xdata g_rw_gam_lut_00B7h_GAM_LUT_183                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00B7);
volatile uint8_t xdata g_rw_gam_lut_00B8h_GAM_LUT_184                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00B8);

volatile uint8_t xdata g_rw_gam_lut_00B9h_GAM_LUT_185                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00B9);
volatile uint8_t xdata g_rw_gam_lut_00BAh_GAM_LUT_186                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00BA);
volatile uint8_t xdata g_rw_gam_lut_00BBh_GAM_LUT_187                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00BB);
volatile uint8_t xdata g_rw_gam_lut_00BCh_GAM_LUT_188                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00BC);
volatile uint8_t xdata g_rw_gam_lut_00BDh_GAM_LUT_189                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00BD);

volatile uint8_t xdata g_rw_gam_lut_00BEh_GAM_LUT_190                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00BE);
volatile uint8_t xdata g_rw_gam_lut_00BFh_GAM_LUT_191                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00BF);
volatile uint8_t xdata g_rw_gam_lut_00C0h_GAM_LUT_192                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00C0);
volatile uint8_t xdata g_rw_gam_lut_00C1h_GAM_LUT_193                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00C1);
volatile uint8_t xdata g_rw_gam_lut_00C2h_GAM_LUT_194                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00C2);

volatile uint8_t xdata g_rw_gam_lut_00C3h_GAM_LUT_195                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00C3);
volatile uint8_t xdata g_rw_gam_lut_00C4h_GAM_LUT_196                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00C4);
volatile uint8_t xdata g_rw_gam_lut_00C5h_GAM_LUT_197                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00C5);
volatile uint8_t xdata g_rw_gam_lut_00C6h_GAM_LUT_198                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00C6);
volatile uint8_t xdata g_rw_gam_lut_00C7h_GAM_LUT_199                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00C7);

volatile uint8_t xdata g_rw_gam_lut_00C8h_GAM_LUT_200                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00C8);
volatile uint8_t xdata g_rw_gam_lut_00C9h_GAM_LUT_201                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00C9);
volatile uint8_t xdata g_rw_gam_lut_00CAh_GAM_LUT_202                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00CA);
volatile uint8_t xdata g_rw_gam_lut_00CBh_GAM_LUT_203                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00CB);
volatile uint8_t xdata g_rw_gam_lut_00CCh_GAM_LUT_204                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00CC);

volatile uint8_t xdata g_rw_gam_lut_00CDh_GAM_LUT_205                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00CD);
volatile uint8_t xdata g_rw_gam_lut_00CEh_GAM_LUT_206                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00CE);
volatile uint8_t xdata g_rw_gam_lut_00CFh_GAM_LUT_207                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00CF);
volatile uint8_t xdata g_rw_gam_lut_00D0h_GAM_LUT_208                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00D0);
volatile uint8_t xdata g_rw_gam_lut_00D1h_GAM_LUT_209                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00D1);

volatile uint8_t xdata g_rw_gam_lut_00D2h_GAM_LUT_210                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00D2);
volatile uint8_t xdata g_rw_gam_lut_00D3h_GAM_LUT_211                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00D3);
volatile uint8_t xdata g_rw_gam_lut_00D4h_GAM_LUT_212                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00D4);
volatile uint8_t xdata g_rw_gam_lut_00D5h_GAM_LUT_213                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00D5);
volatile uint8_t xdata g_rw_gam_lut_00D6h_GAM_LUT_214                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00D6);

volatile uint8_t xdata g_rw_gam_lut_00D7h_GAM_LUT_215                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00D7);
volatile uint8_t xdata g_rw_gam_lut_00D8h_GAM_LUT_216                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00D8);
volatile uint8_t xdata g_rw_gam_lut_00D9h_GAM_LUT_217                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00D9);
volatile uint8_t xdata g_rw_gam_lut_00DAh_GAM_LUT_218                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00DA);
volatile uint8_t xdata g_rw_gam_lut_00DBh_GAM_LUT_219                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00DB);

volatile uint8_t xdata g_rw_gam_lut_00DCh_GAM_LUT_220                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00DC);
volatile uint8_t xdata g_rw_gam_lut_00DDh_GAM_LUT_221                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00DD);
volatile uint8_t xdata g_rw_gam_lut_00DEh_GAM_LUT_222                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00DE);
volatile uint8_t xdata g_rw_gam_lut_00DFh_GAM_LUT_223                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00DF);
volatile uint8_t xdata g_rw_gam_lut_00E0h_GAM_LUT_224                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00E0);

volatile uint8_t xdata g_rw_gam_lut_00E1h_GAM_LUT_225                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00E1);
volatile uint8_t xdata g_rw_gam_lut_00E2h_GAM_LUT_226                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00E2);
volatile uint8_t xdata g_rw_gam_lut_00E3h_GAM_LUT_227                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00E3);
volatile uint8_t xdata g_rw_gam_lut_00E4h_GAM_LUT_228                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00E4);
volatile uint8_t xdata g_rw_gam_lut_00E5h_GAM_LUT_229                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00E5);

volatile uint8_t xdata g_rw_gam_lut_00E6h_GAM_LUT_230                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00E6);
volatile uint8_t xdata g_rw_gam_lut_00E7h_GAM_LUT_231                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00E7);
volatile uint8_t xdata g_rw_gam_lut_00E8h_GAM_LUT_232                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00E8);
volatile uint8_t xdata g_rw_gam_lut_00E9h_GAM_LUT_233                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00E9);
volatile uint8_t xdata g_rw_gam_lut_00EAh_GAM_LUT_234                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00EA);

volatile uint8_t xdata g_rw_gam_lut_00EBh_GAM_LUT_235                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00EB);
volatile uint8_t xdata g_rw_gam_lut_00ECh_GAM_LUT_236                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00EC);
volatile uint8_t xdata g_rw_gam_lut_00EDh_GAM_LUT_237                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00ED);
volatile uint8_t xdata g_rw_gam_lut_00EEh_GAM_LUT_238                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00EE);
volatile uint8_t xdata g_rw_gam_lut_00EFh_GAM_LUT_239                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00EF);

volatile uint8_t xdata g_rw_gam_lut_00F0h_GAM_LUT_240                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00F0);
volatile uint8_t xdata g_rw_gam_lut_00F1h_GAM_LUT_241                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00F1);
volatile uint8_t xdata g_rw_gam_lut_00F2h_GAM_LUT_242                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00F2);
volatile uint8_t xdata g_rw_gam_lut_00F3h_GAM_LUT_243                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00F3);
volatile uint8_t xdata g_rw_gam_lut_00F4h_GAM_LUT_244                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00F4);

volatile uint8_t xdata g_rw_gam_lut_00F5h_GAM_LUT_245                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00F5);
volatile uint8_t xdata g_rw_gam_lut_00F6h_GAM_LUT_246                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00F6);
volatile uint8_t xdata g_rw_gam_lut_00F7h_GAM_LUT_247                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00F7);
volatile uint8_t xdata g_rw_gam_lut_00F8h_GAM_LUT_248                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00F8);
volatile uint8_t xdata g_rw_gam_lut_00F9h_GAM_LUT_249                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00F9);

volatile uint8_t xdata g_rw_gam_lut_00FAh_GAM_LUT_250                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00FA);
volatile uint8_t xdata g_rw_gam_lut_00FBh_GAM_LUT_251                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00FB);
volatile uint8_t xdata g_rw_gam_lut_00FCh_GAM_LUT_252                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00FC);
volatile uint8_t xdata g_rw_gam_lut_00FDh_GAM_LUT_253                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00FD);
volatile uint8_t xdata g_rw_gam_lut_00FEh_GAM_LUT_254                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00FE);

volatile uint8_t xdata g_rw_gam_lut_00FFh_GAM_LUT_255                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x00FF);
volatile uint8_t xdata g_rw_gam_lut_0100h_GAM_LUT_256                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0100);
volatile uint8_t xdata g_rw_gam_lut_0101h_GAM_LUT_257                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0101);
volatile uint8_t xdata g_rw_gam_lut_0102h_GAM_LUT_258                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0102);
volatile uint8_t xdata g_rw_gam_lut_0103h_GAM_LUT_259                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0103);

volatile uint8_t xdata g_rw_gam_lut_0104h_GAM_LUT_260                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0104);
volatile uint8_t xdata g_rw_gam_lut_0105h_GAM_LUT_261                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0105);
volatile uint8_t xdata g_rw_gam_lut_0106h_GAM_LUT_262                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0106);
volatile uint8_t xdata g_rw_gam_lut_0107h_GAM_LUT_263                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0107);
volatile uint8_t xdata g_rw_gam_lut_0108h_GAM_LUT_264                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0108);

volatile uint8_t xdata g_rw_gam_lut_0109h_GAM_LUT_265                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0109);
volatile uint8_t xdata g_rw_gam_lut_010Ah_GAM_LUT_266                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x010A);
volatile uint8_t xdata g_rw_gam_lut_010Bh_GAM_LUT_267                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x010B);
volatile uint8_t xdata g_rw_gam_lut_010Ch_GAM_LUT_268                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x010C);
volatile uint8_t xdata g_rw_gam_lut_010Dh_GAM_LUT_269                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x010D);

volatile uint8_t xdata g_rw_gam_lut_010Eh_GAM_LUT_270                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x010E);
volatile uint8_t xdata g_rw_gam_lut_010Fh_GAM_LUT_271                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x010F);
volatile uint8_t xdata g_rw_gam_lut_0110h_GAM_LUT_272                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0110);
volatile uint8_t xdata g_rw_gam_lut_0111h_GAM_LUT_273                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0111);
volatile uint8_t xdata g_rw_gam_lut_0112h_GAM_LUT_274                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0112);

volatile uint8_t xdata g_rw_gam_lut_0113h_GAM_LUT_275                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0113);
volatile uint8_t xdata g_rw_gam_lut_0114h_GAM_LUT_276                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0114);
volatile uint8_t xdata g_rw_gam_lut_0115h_GAM_LUT_277                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0115);
volatile uint8_t xdata g_rw_gam_lut_0116h_GAM_LUT_278                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0116);
volatile uint8_t xdata g_rw_gam_lut_0117h_GAM_LUT_279                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0117);

volatile uint8_t xdata g_rw_gam_lut_0118h_GAM_LUT_280                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0118);
volatile uint8_t xdata g_rw_gam_lut_0119h_GAM_LUT_281                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0119);
volatile uint8_t xdata g_rw_gam_lut_011Ah_GAM_LUT_282                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x011A);
volatile uint8_t xdata g_rw_gam_lut_011Bh_GAM_LUT_283                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x011B);
volatile uint8_t xdata g_rw_gam_lut_011Ch_GAM_LUT_284                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x011C);

volatile uint8_t xdata g_rw_gam_lut_011Dh_GAM_LUT_285                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x011D);
volatile uint8_t xdata g_rw_gam_lut_011Eh_GAM_LUT_286                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x011E);
volatile uint8_t xdata g_rw_gam_lut_011Fh_GAM_LUT_287                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x011F);
volatile uint8_t xdata g_rw_gam_lut_0120h_GAM_LUT_288                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0120);
volatile uint8_t xdata g_rw_gam_lut_0121h_GAM_LUT_289                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0121);

volatile uint8_t xdata g_rw_gam_lut_0122h_GAM_LUT_290                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0122);
volatile uint8_t xdata g_rw_gam_lut_0123h_GAM_LUT_291                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0123);
volatile uint8_t xdata g_rw_gam_lut_0124h_GAM_LUT_292                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0124);
volatile uint8_t xdata g_rw_gam_lut_0125h_GAM_LUT_293                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0125);
volatile uint8_t xdata g_rw_gam_lut_0126h_GAM_LUT_294                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0126);

volatile uint8_t xdata g_rw_gam_lut_0127h_GAM_LUT_295                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0127);
volatile uint8_t xdata g_rw_gam_lut_0128h_GAM_LUT_296                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0128);
volatile uint8_t xdata g_rw_gam_lut_0129h_GAM_LUT_297                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0129);
volatile uint8_t xdata g_rw_gam_lut_012Ah_GAM_LUT_298                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x012A);
volatile uint8_t xdata g_rw_gam_lut_012Bh_GAM_LUT_299                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x012B);

volatile uint8_t xdata g_rw_gam_lut_012Ch_GAM_LUT_300                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x012C);
volatile uint8_t xdata g_rw_gam_lut_012Dh_GAM_LUT_301                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x012D);
volatile uint8_t xdata g_rw_gam_lut_012Eh_GAM_LUT_302                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x012E);
volatile uint8_t xdata g_rw_gam_lut_012Fh_GAM_LUT_303                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x012F);
volatile uint8_t xdata g_rw_gam_lut_0130h_GAM_LUT_304                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0130);

volatile uint8_t xdata g_rw_gam_lut_0131h_GAM_LUT_305                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0131);
volatile uint8_t xdata g_rw_gam_lut_0132h_GAM_LUT_306                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0132);
volatile uint8_t xdata g_rw_gam_lut_0133h_GAM_LUT_307                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0133);
volatile uint8_t xdata g_rw_gam_lut_0134h_GAM_LUT_308                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0134);
volatile uint8_t xdata g_rw_gam_lut_0135h_GAM_LUT_309                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0135);

volatile uint8_t xdata g_rw_gam_lut_0136h_GAM_LUT_310                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0136);
volatile uint8_t xdata g_rw_gam_lut_0137h_GAM_LUT_311                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0137);
volatile uint8_t xdata g_rw_gam_lut_0138h_GAM_LUT_312                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0138);
volatile uint8_t xdata g_rw_gam_lut_0139h_GAM_LUT_313                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0139);
volatile uint8_t xdata g_rw_gam_lut_013Ah_GAM_LUT_314                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x013A);

volatile uint8_t xdata g_rw_gam_lut_013Bh_GAM_LUT_315                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x013B);
volatile uint8_t xdata g_rw_gam_lut_013Ch_GAM_LUT_316                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x013C);
volatile uint8_t xdata g_rw_gam_lut_013Dh_GAM_LUT_317                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x013D);
volatile uint8_t xdata g_rw_gam_lut_013Eh_GAM_LUT_318                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x013E);
volatile uint8_t xdata g_rw_gam_lut_013Fh_GAM_LUT_319                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x013F);

volatile uint8_t xdata g_rw_gam_lut_0140h_GAM_LUT_320                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0140);
volatile uint8_t xdata g_rw_gam_lut_0141h_GAM_LUT_321                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0141);
volatile uint8_t xdata g_rw_gam_lut_0142h_GAM_LUT_322                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0142);
volatile uint8_t xdata g_rw_gam_lut_0143h_GAM_LUT_323                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0143);
volatile uint8_t xdata g_rw_gam_lut_0144h_GAM_LUT_324                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0144);

volatile uint8_t xdata g_rw_gam_lut_0145h_GAM_LUT_325                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0145);
volatile uint8_t xdata g_rw_gam_lut_0146h_GAM_LUT_326                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0146);
volatile uint8_t xdata g_rw_gam_lut_0147h_GAM_LUT_327                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0147);
volatile uint8_t xdata g_rw_gam_lut_0148h_GAM_LUT_328                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0148);
volatile uint8_t xdata g_rw_gam_lut_0149h_GAM_LUT_329                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0149);

volatile uint8_t xdata g_rw_gam_lut_014Ah_GAM_LUT_330                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x014A);
volatile uint8_t xdata g_rw_gam_lut_014Bh_GAM_LUT_331                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x014B);
volatile uint8_t xdata g_rw_gam_lut_014Ch_GAM_LUT_332                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x014C);
volatile uint8_t xdata g_rw_gam_lut_014Dh_GAM_LUT_333                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x014D);
volatile uint8_t xdata g_rw_gam_lut_014Eh_GAM_LUT_334                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x014E);

volatile uint8_t xdata g_rw_gam_lut_014Fh_GAM_LUT_335                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x014F);
volatile uint8_t xdata g_rw_gam_lut_0150h_GAM_LUT_336                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0150);
volatile uint8_t xdata g_rw_gam_lut_0151h_GAM_LUT_337                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0151);
volatile uint8_t xdata g_rw_gam_lut_0152h_GAM_LUT_338                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0152);
volatile uint8_t xdata g_rw_gam_lut_0153h_GAM_LUT_339                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0153);

volatile uint8_t xdata g_rw_gam_lut_0154h_GAM_LUT_340                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0154);
volatile uint8_t xdata g_rw_gam_lut_0155h_GAM_LUT_341                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0155);
volatile uint8_t xdata g_rw_gam_lut_0156h_GAM_LUT_342                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0156);
volatile uint8_t xdata g_rw_gam_lut_0157h_GAM_LUT_343                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0157);
volatile uint8_t xdata g_rw_gam_lut_0158h_GAM_LUT_344                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0158);

volatile uint8_t xdata g_rw_gam_lut_0159h_GAM_LUT_345                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0159);
volatile uint8_t xdata g_rw_gam_lut_015Ah_GAM_LUT_346                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x015A);
volatile uint8_t xdata g_rw_gam_lut_015Bh_GAM_LUT_347                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x015B);
volatile uint8_t xdata g_rw_gam_lut_015Ch_GAM_LUT_348                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x015C);
volatile uint8_t xdata g_rw_gam_lut_015Dh_GAM_LUT_349                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x015D);

volatile uint8_t xdata g_rw_gam_lut_015Eh_GAM_LUT_350                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x015E);
volatile uint8_t xdata g_rw_gam_lut_015Fh_GAM_LUT_351                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x015F);
volatile uint8_t xdata g_rw_gam_lut_0160h_GAM_LUT_352                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0160);
volatile uint8_t xdata g_rw_gam_lut_0161h_GAM_LUT_353                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0161);
volatile uint8_t xdata g_rw_gam_lut_0162h_GAM_LUT_354                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0162);

volatile uint8_t xdata g_rw_gam_lut_0163h_GAM_LUT_355                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0163);
volatile uint8_t xdata g_rw_gam_lut_0164h_GAM_LUT_356                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0164);
volatile uint8_t xdata g_rw_gam_lut_0165h_GAM_LUT_357                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0165);
volatile uint8_t xdata g_rw_gam_lut_0166h_GAM_LUT_358                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0166);
volatile uint8_t xdata g_rw_gam_lut_0167h_GAM_LUT_359                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0167);

volatile uint8_t xdata g_rw_gam_lut_0168h_GAM_LUT_360                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0168);
volatile uint8_t xdata g_rw_gam_lut_0169h_GAM_LUT_361                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0169);
volatile uint8_t xdata g_rw_gam_lut_016Ah_GAM_LUT_362                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x016A);
volatile uint8_t xdata g_rw_gam_lut_016Bh_GAM_LUT_363                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x016B);
volatile uint8_t xdata g_rw_gam_lut_016Ch_GAM_LUT_364                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x016C);

volatile uint8_t xdata g_rw_gam_lut_016Dh_GAM_LUT_365                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x016D);
volatile uint8_t xdata g_rw_gam_lut_016Eh_GAM_LUT_366                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x016E);
volatile uint8_t xdata g_rw_gam_lut_016Fh_GAM_LUT_367                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x016F);
volatile uint8_t xdata g_rw_gam_lut_0170h_GAM_LUT_368                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0170);
volatile uint8_t xdata g_rw_gam_lut_0171h_GAM_LUT_369                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0171);

volatile uint8_t xdata g_rw_gam_lut_0172h_GAM_LUT_370                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0172);
volatile uint8_t xdata g_rw_gam_lut_0173h_GAM_LUT_371                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0173);
volatile uint8_t xdata g_rw_gam_lut_0174h_GAM_LUT_372                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0174);
volatile uint8_t xdata g_rw_gam_lut_0175h_GAM_LUT_373                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0175);
volatile uint8_t xdata g_rw_gam_lut_0176h_GAM_LUT_374                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0176);

volatile uint8_t xdata g_rw_gam_lut_0177h_GAM_LUT_375                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0177);
volatile uint8_t xdata g_rw_gam_lut_0178h_GAM_LUT_376                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0178);
volatile uint8_t xdata g_rw_gam_lut_0179h_GAM_LUT_377                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0179);
volatile uint8_t xdata g_rw_gam_lut_017Ah_GAM_LUT_378                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x017A);
volatile uint8_t xdata g_rw_gam_lut_017Bh_GAM_LUT_379                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x017B);

volatile uint8_t xdata g_rw_gam_lut_017Ch_GAM_LUT_380                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x017C);
volatile uint8_t xdata g_rw_gam_lut_017Dh_GAM_LUT_381                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x017D);
volatile uint8_t xdata g_rw_gam_lut_017Eh_GAM_LUT_382                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x017E);
volatile uint8_t xdata g_rw_gam_lut_017Fh_GAM_LUT_383                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x017F);
volatile uint8_t xdata g_rw_gam_lut_0180h_GAM_LUT_384                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0180);

volatile uint8_t xdata g_rw_gam_lut_0181h_GAM_LUT_385                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0181);
volatile uint8_t xdata g_rw_gam_lut_0182h_GAM_LUT_386                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0182);
volatile uint8_t xdata g_rw_gam_lut_0183h_GAM_LUT_387                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0183);
volatile uint8_t xdata g_rw_gam_lut_0184h_GAM_LUT_388                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0184);
volatile uint8_t xdata g_rw_gam_lut_0185h_GAM_LUT_389                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0185);

volatile uint8_t xdata g_rw_gam_lut_0186h_GAM_LUT_390                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0186);
volatile uint8_t xdata g_rw_gam_lut_0187h_GAM_LUT_391                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0187);
volatile uint8_t xdata g_rw_gam_lut_0188h_GAM_LUT_392                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0188);
volatile uint8_t xdata g_rw_gam_lut_0189h_GAM_LUT_393                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0189);
volatile uint8_t xdata g_rw_gam_lut_018Ah_GAM_LUT_394                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x018A);

volatile uint8_t xdata g_rw_gam_lut_018Bh_GAM_LUT_395                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x018B);
volatile uint8_t xdata g_rw_gam_lut_018Ch_GAM_LUT_396                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x018C);
volatile uint8_t xdata g_rw_gam_lut_018Dh_GAM_LUT_397                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x018D);
volatile uint8_t xdata g_rw_gam_lut_018Eh_GAM_LUT_398                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x018E);
volatile uint8_t xdata g_rw_gam_lut_018Fh_GAM_LUT_399                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x018F);

volatile uint8_t xdata g_rw_gam_lut_0190h_GAM_LUT_400                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0190);
volatile uint8_t xdata g_rw_gam_lut_0191h_GAM_LUT_401                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0191);
volatile uint8_t xdata g_rw_gam_lut_0192h_GAM_LUT_402                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0192);
volatile uint8_t xdata g_rw_gam_lut_0193h_GAM_LUT_403                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0193);
volatile uint8_t xdata g_rw_gam_lut_0194h_GAM_LUT_404                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0194);

volatile uint8_t xdata g_rw_gam_lut_0195h_GAM_LUT_405                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0195);
volatile uint8_t xdata g_rw_gam_lut_0196h_GAM_LUT_406                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0196);
volatile uint8_t xdata g_rw_gam_lut_0197h_GAM_LUT_407                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0197);
volatile uint8_t xdata g_rw_gam_lut_0198h_GAM_LUT_408                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0198);
volatile uint8_t xdata g_rw_gam_lut_0199h_GAM_LUT_409                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0199);

volatile uint8_t xdata g_rw_gam_lut_019Ah_GAM_LUT_410                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x019A);
volatile uint8_t xdata g_rw_gam_lut_019Bh_GAM_LUT_411                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x019B);
volatile uint8_t xdata g_rw_gam_lut_019Ch_GAM_LUT_412                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x019C);
volatile uint8_t xdata g_rw_gam_lut_019Dh_GAM_LUT_413                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x019D);
volatile uint8_t xdata g_rw_gam_lut_019Eh_GAM_LUT_414                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x019E);

volatile uint8_t xdata g_rw_gam_lut_019Fh_GAM_LUT_415                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x019F);
volatile uint8_t xdata g_rw_gam_lut_01A0h_GAM_LUT_416                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01A0);
volatile uint8_t xdata g_rw_gam_lut_01A1h_GAM_LUT_417                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01A1);
volatile uint8_t xdata g_rw_gam_lut_01A2h_GAM_LUT_418                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01A2);
volatile uint8_t xdata g_rw_gam_lut_01A3h_GAM_LUT_419                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01A3);

volatile uint8_t xdata g_rw_gam_lut_01A4h_GAM_LUT_420                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01A4);
volatile uint8_t xdata g_rw_gam_lut_01A5h_GAM_LUT_421                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01A5);
volatile uint8_t xdata g_rw_gam_lut_01A6h_GAM_LUT_422                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01A6);
volatile uint8_t xdata g_rw_gam_lut_01A7h_GAM_LUT_423                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01A7);
volatile uint8_t xdata g_rw_gam_lut_01A8h_GAM_LUT_424                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01A8);

volatile uint8_t xdata g_rw_gam_lut_01A9h_GAM_LUT_425                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01A9);
volatile uint8_t xdata g_rw_gam_lut_01AAh_GAM_LUT_426                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01AA);
volatile uint8_t xdata g_rw_gam_lut_01ABh_GAM_LUT_427                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01AB);
volatile uint8_t xdata g_rw_gam_lut_01ACh_GAM_LUT_428                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01AC);
volatile uint8_t xdata g_rw_gam_lut_01ADh_GAM_LUT_429                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01AD);

volatile uint8_t xdata g_rw_gam_lut_01AEh_GAM_LUT_430                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01AE);
volatile uint8_t xdata g_rw_gam_lut_01AFh_GAM_LUT_431                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01AF);
volatile uint8_t xdata g_rw_gam_lut_01B0h_GAM_LUT_432                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01B0);
volatile uint8_t xdata g_rw_gam_lut_01B1h_GAM_LUT_433                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01B1);
volatile uint8_t xdata g_rw_gam_lut_01B2h_GAM_LUT_434                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01B2);

volatile uint8_t xdata g_rw_gam_lut_01B3h_GAM_LUT_435                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01B3);
volatile uint8_t xdata g_rw_gam_lut_01B4h_GAM_LUT_436                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01B4);
volatile uint8_t xdata g_rw_gam_lut_01B5h_GAM_LUT_437                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01B5);
volatile uint8_t xdata g_rw_gam_lut_01B6h_GAM_LUT_438                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01B6);
volatile uint8_t xdata g_rw_gam_lut_01B7h_GAM_LUT_439                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01B7);

volatile uint8_t xdata g_rw_gam_lut_01B8h_GAM_LUT_440                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01B8);
volatile uint8_t xdata g_rw_gam_lut_01B9h_GAM_LUT_441                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01B9);
volatile uint8_t xdata g_rw_gam_lut_01BAh_GAM_LUT_442                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01BA);
volatile uint8_t xdata g_rw_gam_lut_01BBh_GAM_LUT_443                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01BB);
volatile uint8_t xdata g_rw_gam_lut_01BCh_GAM_LUT_444                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01BC);

volatile uint8_t xdata g_rw_gam_lut_01BDh_GAM_LUT_445                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01BD);
volatile uint8_t xdata g_rw_gam_lut_01BEh_GAM_LUT_446                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01BE);
volatile uint8_t xdata g_rw_gam_lut_01BFh_GAM_LUT_447                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01BF);
volatile uint8_t xdata g_rw_gam_lut_01C0h_GAM_LUT_448                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01C0);
volatile uint8_t xdata g_rw_gam_lut_01C1h_GAM_LUT_449                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01C1);

volatile uint8_t xdata g_rw_gam_lut_01C2h_GAM_LUT_450                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01C2);
volatile uint8_t xdata g_rw_gam_lut_01C3h_GAM_LUT_451                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01C3);
volatile uint8_t xdata g_rw_gam_lut_01C4h_GAM_LUT_452                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01C4);
volatile uint8_t xdata g_rw_gam_lut_01C5h_GAM_LUT_453                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01C5);
volatile uint8_t xdata g_rw_gam_lut_01C6h_GAM_LUT_454                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01C6);

volatile uint8_t xdata g_rw_gam_lut_01C7h_GAM_LUT_455                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01C7);
volatile uint8_t xdata g_rw_gam_lut_01C8h_GAM_LUT_456                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01C8);
volatile uint8_t xdata g_rw_gam_lut_01C9h_GAM_LUT_457                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01C9);
volatile uint8_t xdata g_rw_gam_lut_01CAh_GAM_LUT_458                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01CA);
volatile uint8_t xdata g_rw_gam_lut_01CBh_GAM_LUT_459                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01CB);

volatile uint8_t xdata g_rw_gam_lut_01CCh_GAM_LUT_460                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01CC);
volatile uint8_t xdata g_rw_gam_lut_01CDh_GAM_LUT_461                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01CD);
volatile uint8_t xdata g_rw_gam_lut_01CEh_GAM_LUT_462                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01CE);
volatile uint8_t xdata g_rw_gam_lut_01CFh_GAM_LUT_463                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01CF);
volatile uint8_t xdata g_rw_gam_lut_01D0h_GAM_LUT_464                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01D0);

volatile uint8_t xdata g_rw_gam_lut_01D1h_GAM_LUT_465                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01D1);
volatile uint8_t xdata g_rw_gam_lut_01D2h_GAM_LUT_466                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01D2);
volatile uint8_t xdata g_rw_gam_lut_01D3h_GAM_LUT_467                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01D3);
volatile uint8_t xdata g_rw_gam_lut_01D4h_GAM_LUT_468                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01D4);
volatile uint8_t xdata g_rw_gam_lut_01D5h_GAM_LUT_469                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01D5);

volatile uint8_t xdata g_rw_gam_lut_01D6h_GAM_LUT_470                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01D6);
volatile uint8_t xdata g_rw_gam_lut_01D7h_GAM_LUT_471                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01D7);
volatile uint8_t xdata g_rw_gam_lut_01D8h_GAM_LUT_472                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01D8);
volatile uint8_t xdata g_rw_gam_lut_01D9h_GAM_LUT_473                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01D9);
volatile uint8_t xdata g_rw_gam_lut_01DAh_GAM_LUT_474                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01DA);

volatile uint8_t xdata g_rw_gam_lut_01DBh_GAM_LUT_475                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01DB);
volatile uint8_t xdata g_rw_gam_lut_01DCh_GAM_LUT_476                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01DC);
volatile uint8_t xdata g_rw_gam_lut_01DDh_GAM_LUT_477                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01DD);
volatile uint8_t xdata g_rw_gam_lut_01DEh_GAM_LUT_478                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01DE);
volatile uint8_t xdata g_rw_gam_lut_01DFh_GAM_LUT_479                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01DF);

volatile uint8_t xdata g_rw_gam_lut_01E0h_GAM_LUT_480                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01E0);
volatile uint8_t xdata g_rw_gam_lut_01E1h_GAM_LUT_481                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01E1);
volatile uint8_t xdata g_rw_gam_lut_01E2h_GAM_LUT_482                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01E2);
volatile uint8_t xdata g_rw_gam_lut_01E3h_GAM_LUT_483                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01E3);
volatile uint8_t xdata g_rw_gam_lut_01E4h_GAM_LUT_484                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01E4);

volatile uint8_t xdata g_rw_gam_lut_01E5h_GAM_LUT_485                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01E5);
volatile uint8_t xdata g_rw_gam_lut_01E6h_GAM_LUT_486                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01E6);
volatile uint8_t xdata g_rw_gam_lut_01E7h_GAM_LUT_487                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01E7);
volatile uint8_t xdata g_rw_gam_lut_01E8h_GAM_LUT_488                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01E8);
volatile uint8_t xdata g_rw_gam_lut_01E9h_GAM_LUT_489                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01E9);

volatile uint8_t xdata g_rw_gam_lut_01EAh_GAM_LUT_490                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01EA);
volatile uint8_t xdata g_rw_gam_lut_01EBh_GAM_LUT_491                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01EB);
volatile uint8_t xdata g_rw_gam_lut_01ECh_GAM_LUT_492                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01EC);
volatile uint8_t xdata g_rw_gam_lut_01EDh_GAM_LUT_493                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01ED);
volatile uint8_t xdata g_rw_gam_lut_01EEh_GAM_LUT_494                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01EE);

volatile uint8_t xdata g_rw_gam_lut_01EFh_GAM_LUT_495                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01EF);
volatile uint8_t xdata g_rw_gam_lut_01F0h_GAM_LUT_496                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01F0);
volatile uint8_t xdata g_rw_gam_lut_01F1h_GAM_LUT_497                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01F1);
volatile uint8_t xdata g_rw_gam_lut_01F2h_GAM_LUT_498                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01F2);
volatile uint8_t xdata g_rw_gam_lut_01F3h_GAM_LUT_499                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01F3);

volatile uint8_t xdata g_rw_gam_lut_01F4h_GAM_LUT_500                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01F4);
volatile uint8_t xdata g_rw_gam_lut_01F5h_GAM_LUT_501                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01F5);
volatile uint8_t xdata g_rw_gam_lut_01F6h_GAM_LUT_502                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01F6);
volatile uint8_t xdata g_rw_gam_lut_01F7h_GAM_LUT_503                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01F7);
volatile uint8_t xdata g_rw_gam_lut_01F8h_GAM_LUT_504                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01F8);

volatile uint8_t xdata g_rw_gam_lut_01F9h_GAM_LUT_505                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01F9);
volatile uint8_t xdata g_rw_gam_lut_01FAh_GAM_LUT_506                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01FA);
volatile uint8_t xdata g_rw_gam_lut_01FBh_GAM_LUT_507                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01FB);
volatile uint8_t xdata g_rw_gam_lut_01FCh_GAM_LUT_508                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01FC);
volatile uint8_t xdata g_rw_gam_lut_01FDh_GAM_LUT_509                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01FD);

volatile uint8_t xdata g_rw_gam_lut_01FEh_GAM_LUT_510                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01FE);
volatile uint8_t xdata g_rw_gam_lut_01FFh_GAM_LUT_511                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x01FF);
volatile uint8_t xdata g_rw_gam_lut_0200h_GAM_LUT_512                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0200);
volatile uint8_t xdata g_rw_gam_lut_0201h_GAM_LUT_513                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0201);
volatile uint8_t xdata g_rw_gam_lut_0202h_GAM_LUT_514                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0202);

volatile uint8_t xdata g_rw_gam_lut_0203h_GAM_LUT_515                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0203);
volatile uint8_t xdata g_rw_gam_lut_0204h_GAM_LUT_516                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0204);
volatile uint8_t xdata g_rw_gam_lut_0205h_GAM_LUT_517                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0205);
volatile uint8_t xdata g_rw_gam_lut_0206h_GAM_LUT_518                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0206);
volatile uint8_t xdata g_rw_gam_lut_0207h_GAM_LUT_519                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0207);

volatile uint8_t xdata g_rw_gam_lut_0208h_GAM_LUT_520                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0208);
volatile uint8_t xdata g_rw_gam_lut_0209h_GAM_LUT_521                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0209);
volatile uint8_t xdata g_rw_gam_lut_020Ah_GAM_LUT_522                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x020A);
volatile uint8_t xdata g_rw_gam_lut_020Bh_GAM_LUT_523                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x020B);
volatile uint8_t xdata g_rw_gam_lut_020Ch_GAM_LUT_524                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x020C);

volatile uint8_t xdata g_rw_gam_lut_020Dh_GAM_LUT_525                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x020D);
volatile uint8_t xdata g_rw_gam_lut_020Eh_GAM_LUT_526                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x020E);
volatile uint8_t xdata g_rw_gam_lut_020Fh_GAM_LUT_527                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x020F);
volatile uint8_t xdata g_rw_gam_lut_0210h_GAM_LUT_528                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0210);
volatile uint8_t xdata g_rw_gam_lut_0211h_GAM_LUT_529                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0211);

volatile uint8_t xdata g_rw_gam_lut_0212h_GAM_LUT_530                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0212);
volatile uint8_t xdata g_rw_gam_lut_0213h_GAM_LUT_531                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0213);
volatile uint8_t xdata g_rw_gam_lut_0214h_GAM_LUT_532                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0214);
volatile uint8_t xdata g_rw_gam_lut_0215h_GAM_LUT_533                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0215);
volatile uint8_t xdata g_rw_gam_lut_0216h_GAM_LUT_534                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0216);

volatile uint8_t xdata g_rw_gam_lut_0217h_GAM_LUT_535                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0217);
volatile uint8_t xdata g_rw_gam_lut_0218h_GAM_LUT_536                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0218);
volatile uint8_t xdata g_rw_gam_lut_0219h_GAM_LUT_537                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0219);
volatile uint8_t xdata g_rw_gam_lut_021Ah_GAM_LUT_538                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x021A);
volatile uint8_t xdata g_rw_gam_lut_021Bh_GAM_LUT_539                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x021B);

volatile uint8_t xdata g_rw_gam_lut_021Ch_GAM_LUT_540                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x021C);
volatile uint8_t xdata g_rw_gam_lut_021Dh_GAM_LUT_541                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x021D);
volatile uint8_t xdata g_rw_gam_lut_021Eh_GAM_LUT_542                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x021E);
volatile uint8_t xdata g_rw_gam_lut_021Fh_GAM_LUT_543                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x021F);
volatile uint8_t xdata g_rw_gam_lut_0220h_GAM_LUT_544                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0220);

volatile uint8_t xdata g_rw_gam_lut_0221h_GAM_LUT_545                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0221);
volatile uint8_t xdata g_rw_gam_lut_0222h_GAM_LUT_546                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0222);
volatile uint8_t xdata g_rw_gam_lut_0223h_GAM_LUT_547                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0223);
volatile uint8_t xdata g_rw_gam_lut_0224h_GAM_LUT_548                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0224);
volatile uint8_t xdata g_rw_gam_lut_0225h_GAM_LUT_549                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0225);

volatile uint8_t xdata g_rw_gam_lut_0226h_GAM_LUT_550                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0226);
volatile uint8_t xdata g_rw_gam_lut_0227h_GAM_LUT_551                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0227);
volatile uint8_t xdata g_rw_gam_lut_0228h_GAM_LUT_552                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0228);
volatile uint8_t xdata g_rw_gam_lut_0229h_GAM_LUT_553                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0229);
volatile uint8_t xdata g_rw_gam_lut_022Ah_GAM_LUT_554                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x022A);

volatile uint8_t xdata g_rw_gam_lut_022Bh_GAM_LUT_555                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x022B);
volatile uint8_t xdata g_rw_gam_lut_022Ch_GAM_LUT_556                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x022C);
volatile uint8_t xdata g_rw_gam_lut_022Dh_GAM_LUT_557                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x022D);
volatile uint8_t xdata g_rw_gam_lut_022Eh_GAM_LUT_558                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x022E);
volatile uint8_t xdata g_rw_gam_lut_022Fh_GAM_LUT_559                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x022F);

volatile uint8_t xdata g_rw_gam_lut_0230h_GAM_LUT_560                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0230);
volatile uint8_t xdata g_rw_gam_lut_0231h_GAM_LUT_561                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0231);
volatile uint8_t xdata g_rw_gam_lut_0232h_GAM_LUT_562                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0232);
volatile uint8_t xdata g_rw_gam_lut_0233h_GAM_LUT_563                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0233);
volatile uint8_t xdata g_rw_gam_lut_0234h_GAM_LUT_564                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0234);

volatile uint8_t xdata g_rw_gam_lut_0235h_GAM_LUT_565                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0235);
volatile uint8_t xdata g_rw_gam_lut_0236h_GAM_LUT_566                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0236);
volatile uint8_t xdata g_rw_gam_lut_0237h_GAM_LUT_567                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0237);
volatile uint8_t xdata g_rw_gam_lut_0238h_GAM_LUT_568                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0238);
volatile uint8_t xdata g_rw_gam_lut_0239h_GAM_LUT_569                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0239);

volatile uint8_t xdata g_rw_gam_lut_023Ah_GAM_LUT_570                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x023A);
volatile uint8_t xdata g_rw_gam_lut_023Bh_GAM_LUT_571                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x023B);
volatile uint8_t xdata g_rw_gam_lut_023Ch_GAM_LUT_572                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x023C);
volatile uint8_t xdata g_rw_gam_lut_023Dh_GAM_LUT_573                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x023D);
volatile uint8_t xdata g_rw_gam_lut_023Eh_GAM_LUT_574                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x023E);

volatile uint8_t xdata g_rw_gam_lut_023Fh_GAM_LUT_575                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x023F);
volatile uint8_t xdata g_rw_gam_lut_0240h_GAM_LUT_576                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0240);
volatile uint8_t xdata g_rw_gam_lut_0241h_GAM_LUT_577                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0241);
volatile uint8_t xdata g_rw_gam_lut_0242h_GAM_LUT_578                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0242);
volatile uint8_t xdata g_rw_gam_lut_0243h_GAM_LUT_579                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0243);

volatile uint8_t xdata g_rw_gam_lut_0244h_GAM_LUT_580                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0244);
volatile uint8_t xdata g_rw_gam_lut_0245h_GAM_LUT_581                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0245);
volatile uint8_t xdata g_rw_gam_lut_0246h_GAM_LUT_582                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0246);
volatile uint8_t xdata g_rw_gam_lut_0247h_GAM_LUT_583                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0247);
volatile uint8_t xdata g_rw_gam_lut_0248h_GAM_LUT_584                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0248);

volatile uint8_t xdata g_rw_gam_lut_0249h_GAM_LUT_585                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0249);
volatile uint8_t xdata g_rw_gam_lut_024Ah_GAM_LUT_586                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x024A);
volatile uint8_t xdata g_rw_gam_lut_024Bh_GAM_LUT_587                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x024B);
volatile uint8_t xdata g_rw_gam_lut_024Ch_GAM_LUT_588                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x024C);
volatile uint8_t xdata g_rw_gam_lut_024Dh_GAM_LUT_589                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x024D);

volatile uint8_t xdata g_rw_gam_lut_024Eh_GAM_LUT_590                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x024E);
volatile uint8_t xdata g_rw_gam_lut_024Fh_GAM_LUT_591                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x024F);
volatile uint8_t xdata g_rw_gam_lut_0250h_GAM_LUT_592                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0250);
volatile uint8_t xdata g_rw_gam_lut_0251h_GAM_LUT_593                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0251);
volatile uint8_t xdata g_rw_gam_lut_0252h_GAM_LUT_594                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0252);

volatile uint8_t xdata g_rw_gam_lut_0253h_GAM_LUT_595                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0253);
volatile uint8_t xdata g_rw_gam_lut_0254h_GAM_LUT_596                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0254);
volatile uint8_t xdata g_rw_gam_lut_0255h_GAM_LUT_597                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0255);
volatile uint8_t xdata g_rw_gam_lut_0256h_GAM_LUT_598                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0256);
volatile uint8_t xdata g_rw_gam_lut_0257h_GAM_LUT_599                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0257);

volatile uint8_t xdata g_rw_gam_lut_0258h_GAM_LUT_600                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0258);
volatile uint8_t xdata g_rw_gam_lut_0259h_GAM_LUT_601                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0259);
volatile uint8_t xdata g_rw_gam_lut_025Ah_GAM_LUT_602                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x025A);
volatile uint8_t xdata g_rw_gam_lut_025Bh_GAM_LUT_603                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x025B);
volatile uint8_t xdata g_rw_gam_lut_025Ch_GAM_LUT_604                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x025C);

volatile uint8_t xdata g_rw_gam_lut_025Dh_GAM_LUT_605                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x025D);
volatile uint8_t xdata g_rw_gam_lut_025Eh_GAM_LUT_606                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x025E);
volatile uint8_t xdata g_rw_gam_lut_025Fh_GAM_LUT_607                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x025F);
volatile uint8_t xdata g_rw_gam_lut_0260h_GAM_LUT_608                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0260);
volatile uint8_t xdata g_rw_gam_lut_0261h_GAM_LUT_609                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0261);

volatile uint8_t xdata g_rw_gam_lut_0262h_GAM_LUT_610                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0262);
volatile uint8_t xdata g_rw_gam_lut_0263h_GAM_LUT_611                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0263);
volatile uint8_t xdata g_rw_gam_lut_0264h_GAM_LUT_612                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0264);
volatile uint8_t xdata g_rw_gam_lut_0265h_GAM_LUT_613                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0265);
volatile uint8_t xdata g_rw_gam_lut_0266h_GAM_LUT_614                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0266);

volatile uint8_t xdata g_rw_gam_lut_0267h_GAM_LUT_615                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0267);
volatile uint8_t xdata g_rw_gam_lut_0268h_GAM_LUT_616                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0268);
volatile uint8_t xdata g_rw_gam_lut_0269h_GAM_LUT_617                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0269);
volatile uint8_t xdata g_rw_gam_lut_026Ah_GAM_LUT_618                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x026A);
volatile uint8_t xdata g_rw_gam_lut_026Bh_GAM_LUT_619                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x026B);

volatile uint8_t xdata g_rw_gam_lut_026Ch_GAM_LUT_620                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x026C);
volatile uint8_t xdata g_rw_gam_lut_026Dh_GAM_LUT_621                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x026D);
volatile uint8_t xdata g_rw_gam_lut_026Eh_GAM_LUT_622                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x026E);
volatile uint8_t xdata g_rw_gam_lut_026Fh_GAM_LUT_623                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x026F);
volatile uint8_t xdata g_rw_gam_lut_0270h_GAM_LUT_624                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0270);

volatile uint8_t xdata g_rw_gam_lut_0271h_GAM_LUT_625                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0271);
volatile uint8_t xdata g_rw_gam_lut_0272h_GAM_LUT_626                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0272);
volatile uint8_t xdata g_rw_gam_lut_0273h_GAM_LUT_627                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0273);
volatile uint8_t xdata g_rw_gam_lut_0274h_GAM_LUT_628                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0274);
volatile uint8_t xdata g_rw_gam_lut_0275h_GAM_LUT_629                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0275);

volatile uint8_t xdata g_rw_gam_lut_0276h_GAM_LUT_630                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0276);
volatile uint8_t xdata g_rw_gam_lut_0277h_GAM_LUT_631                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0277);
volatile uint8_t xdata g_rw_gam_lut_0278h_GAM_LUT_632                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0278);
volatile uint8_t xdata g_rw_gam_lut_0279h_GAM_LUT_633                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0279);
volatile uint8_t xdata g_rw_gam_lut_027Ah_GAM_LUT_634                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x027A);

volatile uint8_t xdata g_rw_gam_lut_027Bh_GAM_LUT_635                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x027B);
volatile uint8_t xdata g_rw_gam_lut_027Ch_GAM_LUT_636                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x027C);
volatile uint8_t xdata g_rw_gam_lut_027Dh_GAM_LUT_637                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x027D);
volatile uint8_t xdata g_rw_gam_lut_027Eh_GAM_LUT_638                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x027E);
volatile uint8_t xdata g_rw_gam_lut_027Fh_GAM_LUT_639                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x027F);

volatile uint8_t xdata g_rw_gam_lut_0280h_GAM_LUT_640                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0280);
volatile uint8_t xdata g_rw_gam_lut_0281h_GAM_LUT_641                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0281);
volatile uint8_t xdata g_rw_gam_lut_0282h_GAM_LUT_642                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0282);
volatile uint8_t xdata g_rw_gam_lut_0283h_GAM_LUT_643                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0283);
volatile uint8_t xdata g_rw_gam_lut_0284h_GAM_LUT_644                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0284);

volatile uint8_t xdata g_rw_gam_lut_0285h_GAM_LUT_645                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0285);
volatile uint8_t xdata g_rw_gam_lut_0286h_GAM_LUT_646                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0286);
volatile uint8_t xdata g_rw_gam_lut_0287h_GAM_LUT_647                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0287);
volatile uint8_t xdata g_rw_gam_lut_0288h_GAM_LUT_648                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0288);
volatile uint8_t xdata g_rw_gam_lut_0289h_GAM_LUT_649                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0289);

volatile uint8_t xdata g_rw_gam_lut_028Ah_GAM_LUT_650                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x028A);
volatile uint8_t xdata g_rw_gam_lut_028Bh_GAM_LUT_651                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x028B);
volatile uint8_t xdata g_rw_gam_lut_028Ch_GAM_LUT_652                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x028C);
volatile uint8_t xdata g_rw_gam_lut_028Dh_GAM_LUT_653                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x028D);
volatile uint8_t xdata g_rw_gam_lut_028Eh_GAM_LUT_654                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x028E);

volatile uint8_t xdata g_rw_gam_lut_028Fh_GAM_LUT_655                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x028F);
volatile uint8_t xdata g_rw_gam_lut_0290h_GAM_LUT_656                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0290);
volatile uint8_t xdata g_rw_gam_lut_0291h_GAM_LUT_657                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0291);
volatile uint8_t xdata g_rw_gam_lut_0292h_GAM_LUT_658                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0292);
volatile uint8_t xdata g_rw_gam_lut_0293h_GAM_LUT_659                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0293);

volatile uint8_t xdata g_rw_gam_lut_0294h_GAM_LUT_660                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0294);
volatile uint8_t xdata g_rw_gam_lut_0295h_GAM_LUT_661                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0295);
volatile uint8_t xdata g_rw_gam_lut_0296h_GAM_LUT_662                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0296);
volatile uint8_t xdata g_rw_gam_lut_0297h_GAM_LUT_663                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0297);
volatile uint8_t xdata g_rw_gam_lut_0298h_GAM_LUT_664                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0298);

volatile uint8_t xdata g_rw_gam_lut_0299h_GAM_LUT_665                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0299);
volatile uint8_t xdata g_rw_gam_lut_029Ah_GAM_LUT_666                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x029A);
volatile uint8_t xdata g_rw_gam_lut_029Bh_GAM_LUT_667                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x029B);
volatile uint8_t xdata g_rw_gam_lut_029Ch_GAM_LUT_668                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x029C);
volatile uint8_t xdata g_rw_gam_lut_029Dh_GAM_LUT_669                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x029D);

volatile uint8_t xdata g_rw_gam_lut_029Eh_GAM_LUT_670                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x029E);
volatile uint8_t xdata g_rw_gam_lut_029Fh_GAM_LUT_671                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x029F);
volatile uint8_t xdata g_rw_gam_lut_02A0h_GAM_LUT_672                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02A0);
volatile uint8_t xdata g_rw_gam_lut_02A1h_GAM_LUT_673                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02A1);
volatile uint8_t xdata g_rw_gam_lut_02A2h_GAM_LUT_674                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02A2);

volatile uint8_t xdata g_rw_gam_lut_02A3h_GAM_LUT_675                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02A3);
volatile uint8_t xdata g_rw_gam_lut_02A4h_GAM_LUT_676                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02A4);
volatile uint8_t xdata g_rw_gam_lut_02A5h_GAM_LUT_677                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02A5);
volatile uint8_t xdata g_rw_gam_lut_02A6h_GAM_LUT_678                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02A6);
volatile uint8_t xdata g_rw_gam_lut_02A7h_GAM_LUT_679                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02A7);

volatile uint8_t xdata g_rw_gam_lut_02A8h_GAM_LUT_680                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02A8);
volatile uint8_t xdata g_rw_gam_lut_02A9h_GAM_LUT_681                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02A9);
volatile uint8_t xdata g_rw_gam_lut_02AAh_GAM_LUT_682                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02AA);
volatile uint8_t xdata g_rw_gam_lut_02ABh_GAM_LUT_683                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02AB);
volatile uint8_t xdata g_rw_gam_lut_02ACh_GAM_LUT_684                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02AC);

volatile uint8_t xdata g_rw_gam_lut_02ADh_GAM_LUT_685                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02AD);
volatile uint8_t xdata g_rw_gam_lut_02AEh_GAM_LUT_686                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02AE);
volatile uint8_t xdata g_rw_gam_lut_02AFh_GAM_LUT_687                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02AF);
volatile uint8_t xdata g_rw_gam_lut_02B0h_GAM_LUT_688                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02B0);
volatile uint8_t xdata g_rw_gam_lut_02B1h_GAM_LUT_689                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02B1);

volatile uint8_t xdata g_rw_gam_lut_02B2h_GAM_LUT_690                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02B2);
volatile uint8_t xdata g_rw_gam_lut_02B3h_GAM_LUT_691                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02B3);
volatile uint8_t xdata g_rw_gam_lut_02B4h_GAM_LUT_692                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02B4);
volatile uint8_t xdata g_rw_gam_lut_02B5h_GAM_LUT_693                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02B5);
volatile uint8_t xdata g_rw_gam_lut_02B6h_GAM_LUT_694                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02B6);

volatile uint8_t xdata g_rw_gam_lut_02B7h_GAM_LUT_695                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02B7);
volatile uint8_t xdata g_rw_gam_lut_02B8h_GAM_LUT_696                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02B8);
volatile uint8_t xdata g_rw_gam_lut_02B9h_GAM_LUT_697                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02B9);
volatile uint8_t xdata g_rw_gam_lut_02BAh_GAM_LUT_698                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02BA);
volatile uint8_t xdata g_rw_gam_lut_02BBh_GAM_LUT_699                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02BB);

volatile uint8_t xdata g_rw_gam_lut_02BCh_GAM_LUT_700                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02BC);
volatile uint8_t xdata g_rw_gam_lut_02BDh_GAM_LUT_701                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02BD);
volatile uint8_t xdata g_rw_gam_lut_02BEh_GAM_LUT_702                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02BE);
volatile uint8_t xdata g_rw_gam_lut_02BFh_GAM_LUT_703                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02BF);
volatile uint8_t xdata g_rw_gam_lut_02C0h_GAM_LUT_704                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02C0);

volatile uint8_t xdata g_rw_gam_lut_02C1h_GAM_LUT_705                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02C1);
volatile uint8_t xdata g_rw_gam_lut_02C2h_GAM_LUT_706                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02C2);
volatile uint8_t xdata g_rw_gam_lut_02C3h_GAM_LUT_707                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02C3);
volatile uint8_t xdata g_rw_gam_lut_02C4h_GAM_LUT_708                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02C4);
volatile uint8_t xdata g_rw_gam_lut_02C5h_GAM_LUT_709                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02C5);

volatile uint8_t xdata g_rw_gam_lut_02C6h_GAM_LUT_710                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02C6);
volatile uint8_t xdata g_rw_gam_lut_02C7h_GAM_LUT_711                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02C7);
volatile uint8_t xdata g_rw_gam_lut_02C8h_GAM_LUT_712                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02C8);
volatile uint8_t xdata g_rw_gam_lut_02C9h_GAM_LUT_713                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02C9);
volatile uint8_t xdata g_rw_gam_lut_02CAh_GAM_LUT_714                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02CA);

volatile uint8_t xdata g_rw_gam_lut_02CBh_GAM_LUT_715                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02CB);
volatile uint8_t xdata g_rw_gam_lut_02CCh_GAM_LUT_716                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02CC);
volatile uint8_t xdata g_rw_gam_lut_02CDh_GAM_LUT_717                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02CD);
volatile uint8_t xdata g_rw_gam_lut_02CEh_GAM_LUT_718                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02CE);
volatile uint8_t xdata g_rw_gam_lut_02CFh_GAM_LUT_719                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02CF);

volatile uint8_t xdata g_rw_gam_lut_02D0h_GAM_LUT_720                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02D0);
volatile uint8_t xdata g_rw_gam_lut_02D1h_GAM_LUT_721                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02D1);
volatile uint8_t xdata g_rw_gam_lut_02D2h_GAM_LUT_722                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02D2);
volatile uint8_t xdata g_rw_gam_lut_02D3h_GAM_LUT_723                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02D3);
volatile uint8_t xdata g_rw_gam_lut_02D4h_GAM_LUT_724                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02D4);

volatile uint8_t xdata g_rw_gam_lut_02D5h_GAM_LUT_725                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02D5);
volatile uint8_t xdata g_rw_gam_lut_02D6h_GAM_LUT_726                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02D6);
volatile uint8_t xdata g_rw_gam_lut_02D7h_GAM_LUT_727                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02D7);
volatile uint8_t xdata g_rw_gam_lut_02D8h_GAM_LUT_728                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02D8);
volatile uint8_t xdata g_rw_gam_lut_02D9h_GAM_LUT_729                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02D9);

volatile uint8_t xdata g_rw_gam_lut_02DAh_GAM_LUT_730                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02DA);
volatile uint8_t xdata g_rw_gam_lut_02DBh_GAM_LUT_731                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02DB);
volatile uint8_t xdata g_rw_gam_lut_02DCh_GAM_LUT_732                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02DC);
volatile uint8_t xdata g_rw_gam_lut_02DDh_GAM_LUT_733                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02DD);
volatile uint8_t xdata g_rw_gam_lut_02DEh_GAM_LUT_734                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02DE);

volatile uint8_t xdata g_rw_gam_lut_02DFh_GAM_LUT_735                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02DF);
volatile uint8_t xdata g_rw_gam_lut_02E0h_GAM_LUT_736                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02E0);
volatile uint8_t xdata g_rw_gam_lut_02E1h_GAM_LUT_737                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02E1);
volatile uint8_t xdata g_rw_gam_lut_02E2h_GAM_LUT_738                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02E2);
volatile uint8_t xdata g_rw_gam_lut_02E3h_GAM_LUT_739                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02E3);

volatile uint8_t xdata g_rw_gam_lut_02E4h_GAM_LUT_740                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02E4);
volatile uint8_t xdata g_rw_gam_lut_02E5h_GAM_LUT_741                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02E5);
volatile uint8_t xdata g_rw_gam_lut_02E6h_GAM_LUT_742                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02E6);
volatile uint8_t xdata g_rw_gam_lut_02E7h_GAM_LUT_743                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02E7);
volatile uint8_t xdata g_rw_gam_lut_02E8h_GAM_LUT_744                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02E8);

volatile uint8_t xdata g_rw_gam_lut_02E9h_GAM_LUT_745                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02E9);
volatile uint8_t xdata g_rw_gam_lut_02EAh_GAM_LUT_746                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02EA);
volatile uint8_t xdata g_rw_gam_lut_02EBh_GAM_LUT_747                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02EB);
volatile uint8_t xdata g_rw_gam_lut_02ECh_GAM_LUT_748                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02EC);
volatile uint8_t xdata g_rw_gam_lut_02EDh_GAM_LUT_749                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02ED);

volatile uint8_t xdata g_rw_gam_lut_02EEh_GAM_LUT_750                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02EE);
volatile uint8_t xdata g_rw_gam_lut_02EFh_GAM_LUT_751                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02EF);
volatile uint8_t xdata g_rw_gam_lut_02F0h_GAM_LUT_752                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02F0);
volatile uint8_t xdata g_rw_gam_lut_02F1h_GAM_LUT_753                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02F1);
volatile uint8_t xdata g_rw_gam_lut_02F2h_GAM_LUT_754                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02F2);

volatile uint8_t xdata g_rw_gam_lut_02F3h_GAM_LUT_755                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02F3);
volatile uint8_t xdata g_rw_gam_lut_02F4h_GAM_LUT_756                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02F4);
volatile uint8_t xdata g_rw_gam_lut_02F5h_GAM_LUT_757                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02F5);
volatile uint8_t xdata g_rw_gam_lut_02F6h_GAM_LUT_758                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02F6);
volatile uint8_t xdata g_rw_gam_lut_02F7h_GAM_LUT_759                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02F7);

volatile uint8_t xdata g_rw_gam_lut_02F8h_GAM_LUT_760                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02F8);
volatile uint8_t xdata g_rw_gam_lut_02F9h_GAM_LUT_761                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02F9);
volatile uint8_t xdata g_rw_gam_lut_02FAh_GAM_LUT_762                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02FA);
volatile uint8_t xdata g_rw_gam_lut_02FBh_GAM_LUT_763                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02FB);
volatile uint8_t xdata g_rw_gam_lut_02FCh_GAM_LUT_764                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02FC);

volatile uint8_t xdata g_rw_gam_lut_02FDh_GAM_LUT_765                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02FD);
volatile uint8_t xdata g_rw_gam_lut_02FEh_GAM_LUT_766                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02FE);
volatile uint8_t xdata g_rw_gam_lut_02FFh_GAM_LUT_767                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x02FF);
volatile uint8_t xdata g_rw_gam_lut_0300h_GAM_LUT_768                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0300);
volatile uint8_t xdata g_rw_gam_lut_0301h_GAM_LUT_769                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0301);

volatile uint8_t xdata g_rw_gam_lut_0302h_GAM_LUT_770                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0302);
volatile uint8_t xdata g_rw_gam_lut_0303h_GAM_LUT_771                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0303);
volatile uint8_t xdata g_rw_gam_lut_0304h_GAM_LUT_772                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0304);
volatile uint8_t xdata g_rw_gam_lut_0305h_GAM_LUT_773                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0305);
volatile uint8_t xdata g_rw_gam_lut_0306h_GAM_LUT_774                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0306);

volatile uint8_t xdata g_rw_gam_lut_0307h_GAM_LUT_775                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0307);
volatile uint8_t xdata g_rw_gam_lut_0308h_GAM_LUT_776                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0308);
volatile uint8_t xdata g_rw_gam_lut_0309h_GAM_LUT_777                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0309);
volatile uint8_t xdata g_rw_gam_lut_030Ah_GAM_LUT_778                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x030A);
volatile uint8_t xdata g_rw_gam_lut_030Bh_GAM_LUT_779                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x030B);

volatile uint8_t xdata g_rw_gam_lut_030Ch_GAM_LUT_780                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x030C);
volatile uint8_t xdata g_rw_gam_lut_030Dh_GAM_LUT_781                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x030D);
volatile uint8_t xdata g_rw_gam_lut_030Eh_GAM_LUT_782                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x030E);
volatile uint8_t xdata g_rw_gam_lut_030Fh_GAM_LUT_783                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x030F);
volatile uint8_t xdata g_rw_gam_lut_0310h_GAM_LUT_784                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0310);

volatile uint8_t xdata g_rw_gam_lut_0311h_GAM_LUT_785                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0311);
volatile uint8_t xdata g_rw_gam_lut_0312h_GAM_LUT_786                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0312);
volatile uint8_t xdata g_rw_gam_lut_0313h_GAM_LUT_787                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0313);
volatile uint8_t xdata g_rw_gam_lut_0314h_GAM_LUT_788                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0314);
volatile uint8_t xdata g_rw_gam_lut_0315h_GAM_LUT_789                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0315);

volatile uint8_t xdata g_rw_gam_lut_0316h_GAM_LUT_790                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0316);
volatile uint8_t xdata g_rw_gam_lut_0317h_GAM_LUT_791                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0317);
volatile uint8_t xdata g_rw_gam_lut_0318h_GAM_LUT_792                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0318);
volatile uint8_t xdata g_rw_gam_lut_0319h_GAM_LUT_793                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0319);
volatile uint8_t xdata g_rw_gam_lut_031Ah_GAM_LUT_794                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x031A);

volatile uint8_t xdata g_rw_gam_lut_031Bh_GAM_LUT_795                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x031B);
volatile uint8_t xdata g_rw_gam_lut_031Ch_GAM_LUT_796                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x031C);
volatile uint8_t xdata g_rw_gam_lut_031Dh_GAM_LUT_797                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x031D);
volatile uint8_t xdata g_rw_gam_lut_031Eh_GAM_LUT_798                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x031E);
volatile uint8_t xdata g_rw_gam_lut_031Fh_GAM_LUT_799                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x031F);

volatile uint8_t xdata g_rw_gam_lut_0320h_GAM_LUT_800                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0320);
volatile uint8_t xdata g_rw_gam_lut_0321h_GAM_LUT_801                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0321);
volatile uint8_t xdata g_rw_gam_lut_0322h_GAM_LUT_802                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0322);
volatile uint8_t xdata g_rw_gam_lut_0323h_GAM_LUT_803                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0323);
volatile uint8_t xdata g_rw_gam_lut_0324h_GAM_LUT_804                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0324);

volatile uint8_t xdata g_rw_gam_lut_0325h_GAM_LUT_805                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0325);
volatile uint8_t xdata g_rw_gam_lut_0326h_GAM_LUT_806                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0326);
volatile uint8_t xdata g_rw_gam_lut_0327h_GAM_LUT_807                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0327);
volatile uint8_t xdata g_rw_gam_lut_0328h_GAM_LUT_808                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0328);
volatile uint8_t xdata g_rw_gam_lut_0329h_GAM_LUT_809                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0329);

volatile uint8_t xdata g_rw_gam_lut_032Ah_GAM_LUT_810                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x032A);
volatile uint8_t xdata g_rw_gam_lut_032Bh_GAM_LUT_811                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x032B);
volatile uint8_t xdata g_rw_gam_lut_032Ch_GAM_LUT_812                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x032C);
volatile uint8_t xdata g_rw_gam_lut_032Dh_GAM_LUT_813                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x032D);
volatile uint8_t xdata g_rw_gam_lut_032Eh_GAM_LUT_814                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x032E);

volatile uint8_t xdata g_rw_gam_lut_032Fh_GAM_LUT_815                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x032F);
volatile uint8_t xdata g_rw_gam_lut_0330h_GAM_LUT_816                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0330);
volatile uint8_t xdata g_rw_gam_lut_0331h_GAM_LUT_817                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0331);
volatile uint8_t xdata g_rw_gam_lut_0332h_GAM_LUT_818                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0332);
volatile uint8_t xdata g_rw_gam_lut_0333h_GAM_LUT_819                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0333);

volatile uint8_t xdata g_rw_gam_lut_0334h_GAM_LUT_820                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0334);
volatile uint8_t xdata g_rw_gam_lut_0335h_GAM_LUT_821                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0335);
volatile uint8_t xdata g_rw_gam_lut_0336h_GAM_LUT_822                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0336);
volatile uint8_t xdata g_rw_gam_lut_0337h_GAM_LUT_823                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0337);
volatile uint8_t xdata g_rw_gam_lut_0338h_GAM_LUT_824                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0338);

volatile uint8_t xdata g_rw_gam_lut_0339h_GAM_LUT_825                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0339);
volatile uint8_t xdata g_rw_gam_lut_033Ah_GAM_LUT_826                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x033A);
volatile uint8_t xdata g_rw_gam_lut_033Bh_GAM_LUT_827                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x033B);
volatile uint8_t xdata g_rw_gam_lut_033Ch_GAM_LUT_828                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x033C);
volatile uint8_t xdata g_rw_gam_lut_033Dh_GAM_LUT_829                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x033D);

volatile uint8_t xdata g_rw_gam_lut_033Eh_GAM_LUT_830                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x033E);
volatile uint8_t xdata g_rw_gam_lut_033Fh_GAM_LUT_831                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x033F);
volatile uint8_t xdata g_rw_gam_lut_0340h_GAM_LUT_832                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0340);
volatile uint8_t xdata g_rw_gam_lut_0341h_GAM_LUT_833                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0341);
volatile uint8_t xdata g_rw_gam_lut_0342h_GAM_LUT_834                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0342);

volatile uint8_t xdata g_rw_gam_lut_0343h_GAM_LUT_835                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0343);
volatile uint8_t xdata g_rw_gam_lut_0344h_GAM_LUT_836                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0344);
volatile uint8_t xdata g_rw_gam_lut_0345h_GAM_LUT_837                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0345);
volatile uint8_t xdata g_rw_gam_lut_0346h_GAM_LUT_838                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0346);
volatile uint8_t xdata g_rw_gam_lut_0347h_GAM_LUT_839                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0347);

volatile uint8_t xdata g_rw_gam_lut_0348h_GAM_LUT_840                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0348);
volatile uint8_t xdata g_rw_gam_lut_0349h_GAM_LUT_841                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0349);
volatile uint8_t xdata g_rw_gam_lut_034Ah_GAM_LUT_842                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x034A);
volatile uint8_t xdata g_rw_gam_lut_034Bh_GAM_LUT_843                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x034B);
volatile uint8_t xdata g_rw_gam_lut_034Ch_GAM_LUT_844                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x034C);

volatile uint8_t xdata g_rw_gam_lut_034Dh_GAM_LUT_845                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x034D);
volatile uint8_t xdata g_rw_gam_lut_034Eh_GAM_LUT_846                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x034E);
volatile uint8_t xdata g_rw_gam_lut_034Fh_GAM_LUT_847                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x034F);
volatile uint8_t xdata g_rw_gam_lut_0350h_GAM_LUT_848                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0350);
volatile uint8_t xdata g_rw_gam_lut_0351h_GAM_LUT_849                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0351);

volatile uint8_t xdata g_rw_gam_lut_0352h_GAM_LUT_850                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0352);
volatile uint8_t xdata g_rw_gam_lut_0353h_GAM_LUT_851                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0353);
volatile uint8_t xdata g_rw_gam_lut_0354h_GAM_LUT_852                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0354);
volatile uint8_t xdata g_rw_gam_lut_0355h_GAM_LUT_853                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0355);
volatile uint8_t xdata g_rw_gam_lut_0356h_GAM_LUT_854                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0356);

volatile uint8_t xdata g_rw_gam_lut_0357h_GAM_LUT_855                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0357);
volatile uint8_t xdata g_rw_gam_lut_0358h_GAM_LUT_856                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0358);
volatile uint8_t xdata g_rw_gam_lut_0359h_GAM_LUT_857                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0359);
volatile uint8_t xdata g_rw_gam_lut_035Ah_GAM_LUT_858                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x035A);
volatile uint8_t xdata g_rw_gam_lut_035Bh_GAM_LUT_859                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x035B);

volatile uint8_t xdata g_rw_gam_lut_035Ch_GAM_LUT_860                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x035C);
volatile uint8_t xdata g_rw_gam_lut_035Dh_GAM_LUT_861                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x035D);
volatile uint8_t xdata g_rw_gam_lut_035Eh_GAM_LUT_862                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x035E);
volatile uint8_t xdata g_rw_gam_lut_035Fh_GAM_LUT_863                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x035F);
volatile uint8_t xdata g_rw_gam_lut_0360h_GAM_LUT_864                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0360);

volatile uint8_t xdata g_rw_gam_lut_0361h_GAM_LUT_865                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0361);
volatile uint8_t xdata g_rw_gam_lut_0362h_GAM_LUT_866                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0362);
volatile uint8_t xdata g_rw_gam_lut_0363h_GAM_LUT_867                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0363);
volatile uint8_t xdata g_rw_gam_lut_0364h_GAM_LUT_868                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0364);
volatile uint8_t xdata g_rw_gam_lut_0365h_GAM_LUT_869                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0365);

volatile uint8_t xdata g_rw_gam_lut_0366h_GAM_LUT_870                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0366);
volatile uint8_t xdata g_rw_gam_lut_0367h_GAM_LUT_871                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0367);
volatile uint8_t xdata g_rw_gam_lut_0368h_GAM_LUT_872                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0368);
volatile uint8_t xdata g_rw_gam_lut_0369h_GAM_LUT_873                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0369);
volatile uint8_t xdata g_rw_gam_lut_036Ah_GAM_LUT_874                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x036A);

volatile uint8_t xdata g_rw_gam_lut_036Bh_GAM_LUT_875                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x036B);
volatile uint8_t xdata g_rw_gam_lut_036Ch_GAM_LUT_876                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x036C);
volatile uint8_t xdata g_rw_gam_lut_036Dh_GAM_LUT_877                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x036D);
volatile uint8_t xdata g_rw_gam_lut_036Eh_GAM_LUT_878                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x036E);
volatile uint8_t xdata g_rw_gam_lut_036Fh_GAM_LUT_879                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x036F);

volatile uint8_t xdata g_rw_gam_lut_0370h_GAM_LUT_880                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0370);
volatile uint8_t xdata g_rw_gam_lut_0371h_GAM_LUT_881                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0371);
volatile uint8_t xdata g_rw_gam_lut_0372h_GAM_LUT_882                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0372);
volatile uint8_t xdata g_rw_gam_lut_0373h_GAM_LUT_883                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0373);
volatile uint8_t xdata g_rw_gam_lut_0374h_GAM_LUT_884                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0374);

volatile uint8_t xdata g_rw_gam_lut_0375h_GAM_LUT_885                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0375);
volatile uint8_t xdata g_rw_gam_lut_0376h_GAM_LUT_886                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0376);
volatile uint8_t xdata g_rw_gam_lut_0377h_GAM_LUT_887                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0377);
volatile uint8_t xdata g_rw_gam_lut_0378h_GAM_LUT_888                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0378);
volatile uint8_t xdata g_rw_gam_lut_0379h_GAM_LUT_889                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0379);

volatile uint8_t xdata g_rw_gam_lut_037Ah_GAM_LUT_890                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x037A);
volatile uint8_t xdata g_rw_gam_lut_037Bh_GAM_LUT_891                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x037B);
volatile uint8_t xdata g_rw_gam_lut_037Ch_GAM_LUT_892                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x037C);
volatile uint8_t xdata g_rw_gam_lut_037Dh_GAM_LUT_893                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x037D);
volatile uint8_t xdata g_rw_gam_lut_037Eh_GAM_LUT_894                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x037E);

volatile uint8_t xdata g_rw_gam_lut_037Fh_GAM_LUT_895                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x037F);
volatile uint8_t xdata g_rw_gam_lut_0380h_GAM_LUT_896                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0380);
volatile uint8_t xdata g_rw_gam_lut_0381h_GAM_LUT_897                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0381);
volatile uint8_t xdata g_rw_gam_lut_0382h_GAM_LUT_898                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0382);
volatile uint8_t xdata g_rw_gam_lut_0383h_GAM_LUT_899                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0383);

volatile uint8_t xdata g_rw_gam_lut_0384h_GAM_LUT_900                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0384);
volatile uint8_t xdata g_rw_gam_lut_0385h_GAM_LUT_901                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0385);
volatile uint8_t xdata g_rw_gam_lut_0386h_GAM_LUT_902                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0386);
volatile uint8_t xdata g_rw_gam_lut_0387h_GAM_LUT_903                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0387);
volatile uint8_t xdata g_rw_gam_lut_0388h_GAM_LUT_904                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0388);

volatile uint8_t xdata g_rw_gam_lut_0389h_GAM_LUT_905                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0389);
volatile uint8_t xdata g_rw_gam_lut_038Ah_GAM_LUT_906                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x038A);
volatile uint8_t xdata g_rw_gam_lut_038Bh_GAM_LUT_907                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x038B);
volatile uint8_t xdata g_rw_gam_lut_038Ch_GAM_LUT_908                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x038C);
volatile uint8_t xdata g_rw_gam_lut_038Dh_GAM_LUT_909                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x038D);

volatile uint8_t xdata g_rw_gam_lut_038Eh_GAM_LUT_910                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x038E);
volatile uint8_t xdata g_rw_gam_lut_038Fh_GAM_LUT_911                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x038F);
volatile uint8_t xdata g_rw_gam_lut_0390h_GAM_LUT_912                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0390);
volatile uint8_t xdata g_rw_gam_lut_0391h_GAM_LUT_913                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0391);
volatile uint8_t xdata g_rw_gam_lut_0392h_GAM_LUT_914                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0392);

volatile uint8_t xdata g_rw_gam_lut_0393h_GAM_LUT_915                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0393);
volatile uint8_t xdata g_rw_gam_lut_0394h_GAM_LUT_916                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0394);
volatile uint8_t xdata g_rw_gam_lut_0395h_GAM_LUT_917                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0395);
volatile uint8_t xdata g_rw_gam_lut_0396h_GAM_LUT_918                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0396);
volatile uint8_t xdata g_rw_gam_lut_0397h_GAM_LUT_919                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0397);

volatile uint8_t xdata g_rw_gam_lut_0398h_GAM_LUT_920                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0398);
volatile uint8_t xdata g_rw_gam_lut_0399h_GAM_LUT_921                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0399);
volatile uint8_t xdata g_rw_gam_lut_039Ah_GAM_LUT_922                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x039A);
volatile uint8_t xdata g_rw_gam_lut_039Bh_GAM_LUT_923                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x039B);
volatile uint8_t xdata g_rw_gam_lut_039Ch_GAM_LUT_924                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x039C);

volatile uint8_t xdata g_rw_gam_lut_039Dh_GAM_LUT_925                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x039D);
volatile uint8_t xdata g_rw_gam_lut_039Eh_GAM_LUT_926                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x039E);
volatile uint8_t xdata g_rw_gam_lut_039Fh_GAM_LUT_927                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x039F);
volatile uint8_t xdata g_rw_gam_lut_03A0h_GAM_LUT_928                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03A0);
volatile uint8_t xdata g_rw_gam_lut_03A1h_GAM_LUT_929                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03A1);

volatile uint8_t xdata g_rw_gam_lut_03A2h_GAM_LUT_930                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03A2);
volatile uint8_t xdata g_rw_gam_lut_03A3h_GAM_LUT_931                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03A3);
volatile uint8_t xdata g_rw_gam_lut_03A4h_GAM_LUT_932                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03A4);
volatile uint8_t xdata g_rw_gam_lut_03A5h_GAM_LUT_933                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03A5);
volatile uint8_t xdata g_rw_gam_lut_03A6h_GAM_LUT_934                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03A6);

volatile uint8_t xdata g_rw_gam_lut_03A7h_GAM_LUT_935                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03A7);
volatile uint8_t xdata g_rw_gam_lut_03A8h_GAM_LUT_936                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03A8);
volatile uint8_t xdata g_rw_gam_lut_03A9h_GAM_LUT_937                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03A9);
volatile uint8_t xdata g_rw_gam_lut_03AAh_GAM_LUT_938                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03AA);
volatile uint8_t xdata g_rw_gam_lut_03ABh_GAM_LUT_939                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03AB);

volatile uint8_t xdata g_rw_gam_lut_03ACh_GAM_LUT_940                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03AC);
volatile uint8_t xdata g_rw_gam_lut_03ADh_GAM_LUT_941                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03AD);
volatile uint8_t xdata g_rw_gam_lut_03AEh_GAM_LUT_942                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03AE);
volatile uint8_t xdata g_rw_gam_lut_03AFh_GAM_LUT_943                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03AF);
volatile uint8_t xdata g_rw_gam_lut_03B0h_GAM_LUT_944                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03B0);

volatile uint8_t xdata g_rw_gam_lut_03B1h_GAM_LUT_945                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03B1);
volatile uint8_t xdata g_rw_gam_lut_03B2h_GAM_LUT_946                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03B2);
volatile uint8_t xdata g_rw_gam_lut_03B3h_GAM_LUT_947                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03B3);
volatile uint8_t xdata g_rw_gam_lut_03B4h_GAM_LUT_948                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03B4);
volatile uint8_t xdata g_rw_gam_lut_03B5h_GAM_LUT_949                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03B5);

volatile uint8_t xdata g_rw_gam_lut_03B6h_GAM_LUT_950                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03B6);
volatile uint8_t xdata g_rw_gam_lut_03B7h_GAM_LUT_951                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03B7);
volatile uint8_t xdata g_rw_gam_lut_03B8h_GAM_LUT_952                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03B8);
volatile uint8_t xdata g_rw_gam_lut_03B9h_GAM_LUT_953                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03B9);
volatile uint8_t xdata g_rw_gam_lut_03BAh_GAM_LUT_954                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03BA);

volatile uint8_t xdata g_rw_gam_lut_03BBh_GAM_LUT_955                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03BB);
volatile uint8_t xdata g_rw_gam_lut_03BCh_GAM_LUT_956                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03BC);
volatile uint8_t xdata g_rw_gam_lut_03BDh_GAM_LUT_957                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03BD);
volatile uint8_t xdata g_rw_gam_lut_03BEh_GAM_LUT_958                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03BE);
volatile uint8_t xdata g_rw_gam_lut_03BFh_GAM_LUT_959                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03BF);

volatile uint8_t xdata g_rw_gam_lut_03C0h_GAM_LUT_960                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03C0);
volatile uint8_t xdata g_rw_gam_lut_03C1h_GAM_LUT_961                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03C1);
volatile uint8_t xdata g_rw_gam_lut_03C2h_GAM_LUT_962                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03C2);
volatile uint8_t xdata g_rw_gam_lut_03C3h_GAM_LUT_963                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03C3);
volatile uint8_t xdata g_rw_gam_lut_03C4h_GAM_LUT_964                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03C4);

volatile uint8_t xdata g_rw_gam_lut_03C5h_GAM_LUT_965                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03C5);
volatile uint8_t xdata g_rw_gam_lut_03C6h_GAM_LUT_966                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03C6);
volatile uint8_t xdata g_rw_gam_lut_03C7h_GAM_LUT_967                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03C7);
volatile uint8_t xdata g_rw_gam_lut_03C8h_GAM_LUT_968                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03C8);
volatile uint8_t xdata g_rw_gam_lut_03C9h_GAM_LUT_969                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03C9);

volatile uint8_t xdata g_rw_gam_lut_03CAh_GAM_LUT_970                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03CA);
volatile uint8_t xdata g_rw_gam_lut_03CBh_GAM_LUT_971                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03CB);
volatile uint8_t xdata g_rw_gam_lut_03CCh_GAM_LUT_972                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03CC);
volatile uint8_t xdata g_rw_gam_lut_03CDh_GAM_LUT_973                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03CD);
volatile uint8_t xdata g_rw_gam_lut_03CEh_GAM_LUT_974                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03CE);

volatile uint8_t xdata g_rw_gam_lut_03CFh_GAM_LUT_975                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03CF);
volatile uint8_t xdata g_rw_gam_lut_03D0h_GAM_LUT_976                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03D0);
volatile uint8_t xdata g_rw_gam_lut_03D1h_GAM_LUT_977                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03D1);
volatile uint8_t xdata g_rw_gam_lut_03D2h_GAM_LUT_978                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03D2);
volatile uint8_t xdata g_rw_gam_lut_03D3h_GAM_LUT_979                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03D3);

volatile uint8_t xdata g_rw_gam_lut_03D4h_GAM_LUT_980                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03D4);
volatile uint8_t xdata g_rw_gam_lut_03D5h_GAM_LUT_981                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03D5);
volatile uint8_t xdata g_rw_gam_lut_03D6h_GAM_LUT_982                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03D6);
volatile uint8_t xdata g_rw_gam_lut_03D7h_GAM_LUT_983                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03D7);
volatile uint8_t xdata g_rw_gam_lut_03D8h_GAM_LUT_984                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03D8);

volatile uint8_t xdata g_rw_gam_lut_03D9h_GAM_LUT_985                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03D9);
volatile uint8_t xdata g_rw_gam_lut_03DAh_GAM_LUT_986                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03DA);
volatile uint8_t xdata g_rw_gam_lut_03DBh_GAM_LUT_987                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03DB);
volatile uint8_t xdata g_rw_gam_lut_03DCh_GAM_LUT_988                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03DC);
volatile uint8_t xdata g_rw_gam_lut_03DDh_GAM_LUT_989                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03DD);

volatile uint8_t xdata g_rw_gam_lut_03DEh_GAM_LUT_990                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03DE);
volatile uint8_t xdata g_rw_gam_lut_03DFh_GAM_LUT_991                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03DF);
volatile uint8_t xdata g_rw_gam_lut_03E0h_GAM_LUT_992                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03E0);
volatile uint8_t xdata g_rw_gam_lut_03E1h_GAM_LUT_993                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03E1);
volatile uint8_t xdata g_rw_gam_lut_03E2h_GAM_LUT_994                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03E2);

volatile uint8_t xdata g_rw_gam_lut_03E3h_GAM_LUT_995                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03E3);
volatile uint8_t xdata g_rw_gam_lut_03E4h_GAM_LUT_996                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03E4);
volatile uint8_t xdata g_rw_gam_lut_03E5h_GAM_LUT_997                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03E5);
volatile uint8_t xdata g_rw_gam_lut_03E6h_GAM_LUT_998                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03E6);
volatile uint8_t xdata g_rw_gam_lut_03E7h_GAM_LUT_999                                          _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03E7);

volatile uint8_t xdata g_rw_gam_lut_03E8h_GAM_LUT_1000                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03E8);
volatile uint8_t xdata g_rw_gam_lut_03E9h_GAM_LUT_1001                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03E9);
volatile uint8_t xdata g_rw_gam_lut_03EAh_GAM_LUT_1002                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03EA);
volatile uint8_t xdata g_rw_gam_lut_03EBh_GAM_LUT_1003                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03EB);
volatile uint8_t xdata g_rw_gam_lut_03ECh_GAM_LUT_1004                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03EC);

volatile uint8_t xdata g_rw_gam_lut_03EDh_GAM_LUT_1005                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03ED);
volatile uint8_t xdata g_rw_gam_lut_03EEh_GAM_LUT_1006                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03EE);
volatile uint8_t xdata g_rw_gam_lut_03EFh_GAM_LUT_1007                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03EF);
volatile uint8_t xdata g_rw_gam_lut_03F0h_GAM_LUT_1008                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03F0);
volatile uint8_t xdata g_rw_gam_lut_03F1h_GAM_LUT_1009                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03F1);

volatile uint8_t xdata g_rw_gam_lut_03F2h_GAM_LUT_1010                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03F2);
volatile uint8_t xdata g_rw_gam_lut_03F3h_GAM_LUT_1011                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03F3);
volatile uint8_t xdata g_rw_gam_lut_03F4h_GAM_LUT_1012                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03F4);
volatile uint8_t xdata g_rw_gam_lut_03F5h_GAM_LUT_1013                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03F5);
volatile uint8_t xdata g_rw_gam_lut_03F6h_GAM_LUT_1014                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03F6);

volatile uint8_t xdata g_rw_gam_lut_03F7h_GAM_LUT_1015                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03F7);
volatile uint8_t xdata g_rw_gam_lut_03F8h_GAM_LUT_1016                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03F8);
volatile uint8_t xdata g_rw_gam_lut_03F9h_GAM_LUT_1017                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03F9);
volatile uint8_t xdata g_rw_gam_lut_03FAh_GAM_LUT_1018                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03FA);
volatile uint8_t xdata g_rw_gam_lut_03FBh_GAM_LUT_1019                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03FB);

volatile uint8_t xdata g_rw_gam_lut_03FCh_GAM_LUT_1020                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03FC);
volatile uint8_t xdata g_rw_gam_lut_03FDh_GAM_LUT_1021                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03FD);
volatile uint8_t xdata g_rw_gam_lut_03FEh_GAM_LUT_1022                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03FE);
volatile uint8_t xdata g_rw_gam_lut_03FFh_GAM_LUT_1023                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x03FF);
volatile uint8_t xdata g_rw_gam_lut_0400h_GAM_LUT_1024                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0400);

volatile uint8_t xdata g_rw_gam_lut_0401h_GAM_LUT_1025                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0401);
volatile uint8_t xdata g_rw_gam_lut_0402h_GAM_LUT_1026                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0402);
volatile uint8_t xdata g_rw_gam_lut_0403h_GAM_LUT_1027                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0403);
volatile uint8_t xdata g_rw_gam_lut_0404h_GAM_LUT_1028                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0404);
volatile uint8_t xdata g_rw_gam_lut_0405h_GAM_LUT_1029                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0405);

volatile uint8_t xdata g_rw_gam_lut_0406h_GAM_LUT_1030                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0406);
volatile uint8_t xdata g_rw_gam_lut_0407h_GAM_LUT_1031                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0407);
volatile uint8_t xdata g_rw_gam_lut_0408h_GAM_LUT_1032                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0408);
volatile uint8_t xdata g_rw_gam_lut_0409h_GAM_LUT_1033                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0409);
volatile uint8_t xdata g_rw_gam_lut_040Ah_GAM_LUT_1034                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x040A);

volatile uint8_t xdata g_rw_gam_lut_040Bh_GAM_LUT_1035                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x040B);
volatile uint8_t xdata g_rw_gam_lut_040Ch_GAM_LUT_1036                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x040C);
volatile uint8_t xdata g_rw_gam_lut_040Dh_GAM_LUT_1037                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x040D);
volatile uint8_t xdata g_rw_gam_lut_040Eh_GAM_LUT_1038                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x040E);
volatile uint8_t xdata g_rw_gam_lut_040Fh_GAM_LUT_1039                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x040F);

volatile uint8_t xdata g_rw_gam_lut_0410h_GAM_LUT_1040                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0410);
volatile uint8_t xdata g_rw_gam_lut_0411h_GAM_LUT_1041                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0411);
volatile uint8_t xdata g_rw_gam_lut_0412h_GAM_LUT_1042                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0412);
volatile uint8_t xdata g_rw_gam_lut_0413h_GAM_LUT_1043                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0413);
volatile uint8_t xdata g_rw_gam_lut_0414h_GAM_LUT_1044                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0414);

volatile uint8_t xdata g_rw_gam_lut_0415h_GAM_LUT_1045                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0415);
volatile uint8_t xdata g_rw_gam_lut_0416h_GAM_LUT_1046                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0416);
volatile uint8_t xdata g_rw_gam_lut_0417h_GAM_LUT_1047                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0417);
volatile uint8_t xdata g_rw_gam_lut_0418h_GAM_LUT_1048                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0418);
volatile uint8_t xdata g_rw_gam_lut_0419h_GAM_LUT_1049                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0419);

volatile uint8_t xdata g_rw_gam_lut_041Ah_GAM_LUT_1050                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x041A);
volatile uint8_t xdata g_rw_gam_lut_041Bh_GAM_LUT_1051                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x041B);
volatile uint8_t xdata g_rw_gam_lut_041Ch_GAM_LUT_1052                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x041C);
volatile uint8_t xdata g_rw_gam_lut_041Dh_GAM_LUT_1053                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x041D);
volatile uint8_t xdata g_rw_gam_lut_041Eh_GAM_LUT_1054                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x041E);

volatile uint8_t xdata g_rw_gam_lut_041Fh_GAM_LUT_1055                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x041F);
volatile uint8_t xdata g_rw_gam_lut_0420h_GAM_LUT_1056                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0420);
volatile uint8_t xdata g_rw_gam_lut_0421h_GAM_LUT_1057                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0421);
volatile uint8_t xdata g_rw_gam_lut_0422h_GAM_LUT_1058                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0422);
volatile uint8_t xdata g_rw_gam_lut_0423h_GAM_LUT_1059                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0423);

volatile uint8_t xdata g_rw_gam_lut_0424h_GAM_LUT_1060                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0424);
volatile uint8_t xdata g_rw_gam_lut_0425h_GAM_LUT_1061                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0425);
volatile uint8_t xdata g_rw_gam_lut_0426h_GAM_LUT_1062                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0426);
volatile uint8_t xdata g_rw_gam_lut_0427h_GAM_LUT_1063                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0427);
volatile uint8_t xdata g_rw_gam_lut_0428h_GAM_LUT_1064                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0428);

volatile uint8_t xdata g_rw_gam_lut_0429h_GAM_LUT_1065                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0429);
volatile uint8_t xdata g_rw_gam_lut_042Ah_GAM_LUT_1066                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x042A);
volatile uint8_t xdata g_rw_gam_lut_042Bh_GAM_LUT_1067                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x042B);
volatile uint8_t xdata g_rw_gam_lut_042Ch_GAM_LUT_1068                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x042C);
volatile uint8_t xdata g_rw_gam_lut_042Dh_GAM_LUT_1069                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x042D);

volatile uint8_t xdata g_rw_gam_lut_042Eh_GAM_LUT_1070                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x042E);
volatile uint8_t xdata g_rw_gam_lut_042Fh_GAM_LUT_1071                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x042F);
volatile uint8_t xdata g_rw_gam_lut_0430h_GAM_LUT_1072                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0430);
volatile uint8_t xdata g_rw_gam_lut_0431h_GAM_LUT_1073                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0431);
volatile uint8_t xdata g_rw_gam_lut_0432h_GAM_LUT_1074                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0432);

volatile uint8_t xdata g_rw_gam_lut_0433h_GAM_LUT_1075                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0433);
volatile uint8_t xdata g_rw_gam_lut_0434h_GAM_LUT_1076                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0434);
volatile uint8_t xdata g_rw_gam_lut_0435h_GAM_LUT_1077                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0435);
volatile uint8_t xdata g_rw_gam_lut_0436h_GAM_LUT_1078                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0436);
volatile uint8_t xdata g_rw_gam_lut_0437h_GAM_LUT_1079                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0437);

volatile uint8_t xdata g_rw_gam_lut_0438h_GAM_LUT_1080                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0438);
volatile uint8_t xdata g_rw_gam_lut_0439h_GAM_LUT_1081                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0439);
volatile uint8_t xdata g_rw_gam_lut_043Ah_GAM_LUT_1082                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x043A);
volatile uint8_t xdata g_rw_gam_lut_043Bh_GAM_LUT_1083                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x043B);
volatile uint8_t xdata g_rw_gam_lut_043Ch_GAM_LUT_1084                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x043C);

volatile uint8_t xdata g_rw_gam_lut_043Dh_GAM_LUT_1085                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x043D);
volatile uint8_t xdata g_rw_gam_lut_043Eh_GAM_LUT_1086                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x043E);
volatile uint8_t xdata g_rw_gam_lut_043Fh_GAM_LUT_1087                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x043F);
volatile uint8_t xdata g_rw_gam_lut_0440h_GAM_LUT_1088                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0440);
volatile uint8_t xdata g_rw_gam_lut_0441h_GAM_LUT_1089                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0441);

volatile uint8_t xdata g_rw_gam_lut_0442h_GAM_LUT_1090                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0442);
volatile uint8_t xdata g_rw_gam_lut_0443h_GAM_LUT_1091                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0443);
volatile uint8_t xdata g_rw_gam_lut_0444h_GAM_LUT_1092                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0444);
volatile uint8_t xdata g_rw_gam_lut_0445h_GAM_LUT_1093                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0445);
volatile uint8_t xdata g_rw_gam_lut_0446h_GAM_LUT_1094                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0446);

volatile uint8_t xdata g_rw_gam_lut_0447h_GAM_LUT_1095                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0447);
volatile uint8_t xdata g_rw_gam_lut_0448h_GAM_LUT_1096                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0448);
volatile uint8_t xdata g_rw_gam_lut_0449h_GAM_LUT_1097                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0449);
volatile uint8_t xdata g_rw_gam_lut_044Ah_GAM_LUT_1098                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x044A);
volatile uint8_t xdata g_rw_gam_lut_044Bh_GAM_LUT_1099                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x044B);

volatile uint8_t xdata g_rw_gam_lut_044Ch_GAM_LUT_1100                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x044C);
volatile uint8_t xdata g_rw_gam_lut_044Dh_GAM_LUT_1101                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x044D);
volatile uint8_t xdata g_rw_gam_lut_044Eh_GAM_LUT_1102                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x044E);
volatile uint8_t xdata g_rw_gam_lut_044Fh_GAM_LUT_1103                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x044F);
volatile uint8_t xdata g_rw_gam_lut_0450h_GAM_LUT_1104                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0450);

volatile uint8_t xdata g_rw_gam_lut_0451h_GAM_LUT_1105                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0451);
volatile uint8_t xdata g_rw_gam_lut_0452h_GAM_LUT_1106                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0452);
volatile uint8_t xdata g_rw_gam_lut_0453h_GAM_LUT_1107                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0453);
volatile uint8_t xdata g_rw_gam_lut_0454h_GAM_LUT_1108                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0454);
volatile uint8_t xdata g_rw_gam_lut_0455h_GAM_LUT_1109                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0455);

volatile uint8_t xdata g_rw_gam_lut_0456h_GAM_LUT_1110                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0456);
volatile uint8_t xdata g_rw_gam_lut_0457h_GAM_LUT_1111                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0457);
volatile uint8_t xdata g_rw_gam_lut_0458h_GAM_LUT_1112                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0458);
volatile uint8_t xdata g_rw_gam_lut_0459h_GAM_LUT_1113                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0459);
volatile uint8_t xdata g_rw_gam_lut_045Ah_GAM_LUT_1114                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x045A);

volatile uint8_t xdata g_rw_gam_lut_045Bh_GAM_LUT_1115                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x045B);
volatile uint8_t xdata g_rw_gam_lut_045Ch_GAM_LUT_1116                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x045C);
volatile uint8_t xdata g_rw_gam_lut_045Dh_GAM_LUT_1117                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x045D);
volatile uint8_t xdata g_rw_gam_lut_045Eh_GAM_LUT_1118                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x045E);
volatile uint8_t xdata g_rw_gam_lut_045Fh_GAM_LUT_1119                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x045F);

volatile uint8_t xdata g_rw_gam_lut_0460h_GAM_LUT_1120                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0460);
volatile uint8_t xdata g_rw_gam_lut_0461h_GAM_LUT_1121                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0461);
volatile uint8_t xdata g_rw_gam_lut_0462h_GAM_LUT_1122                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0462);
volatile uint8_t xdata g_rw_gam_lut_0463h_GAM_LUT_1123                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0463);
volatile uint8_t xdata g_rw_gam_lut_0464h_GAM_LUT_1124                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0464);

volatile uint8_t xdata g_rw_gam_lut_0465h_GAM_LUT_1125                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0465);
volatile uint8_t xdata g_rw_gam_lut_0466h_GAM_LUT_1126                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0466);
volatile uint8_t xdata g_rw_gam_lut_0467h_GAM_LUT_1127                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0467);
volatile uint8_t xdata g_rw_gam_lut_0468h_GAM_LUT_1128                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0468);
volatile uint8_t xdata g_rw_gam_lut_0469h_GAM_LUT_1129                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0469);

volatile uint8_t xdata g_rw_gam_lut_046Ah_GAM_LUT_1130                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x046A);
volatile uint8_t xdata g_rw_gam_lut_046Bh_GAM_LUT_1131                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x046B);
volatile uint8_t xdata g_rw_gam_lut_046Ch_GAM_LUT_1132                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x046C);
volatile uint8_t xdata g_rw_gam_lut_046Dh_GAM_LUT_1133                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x046D);
volatile uint8_t xdata g_rw_gam_lut_046Eh_GAM_LUT_1134                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x046E);

volatile uint8_t xdata g_rw_gam_lut_046Fh_GAM_LUT_1135                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x046F);
volatile uint8_t xdata g_rw_gam_lut_0470h_GAM_LUT_1136                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0470);
volatile uint8_t xdata g_rw_gam_lut_0471h_GAM_LUT_1137                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0471);
volatile uint8_t xdata g_rw_gam_lut_0472h_GAM_LUT_1138                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0472);
volatile uint8_t xdata g_rw_gam_lut_0473h_GAM_LUT_1139                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0473);

volatile uint8_t xdata g_rw_gam_lut_0474h_GAM_LUT_1140                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0474);
volatile uint8_t xdata g_rw_gam_lut_0475h_GAM_LUT_1141                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0475);
volatile uint8_t xdata g_rw_gam_lut_0476h_GAM_LUT_1142                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0476);
volatile uint8_t xdata g_rw_gam_lut_0477h_GAM_LUT_1143                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0477);
volatile uint8_t xdata g_rw_gam_lut_0478h_GAM_LUT_1144                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0478);

volatile uint8_t xdata g_rw_gam_lut_0479h_GAM_LUT_1145                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0479);
volatile uint8_t xdata g_rw_gam_lut_047Ah_GAM_LUT_1146                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x047A);
volatile uint8_t xdata g_rw_gam_lut_047Bh_GAM_LUT_1147                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x047B);
volatile uint8_t xdata g_rw_gam_lut_047Ch_GAM_LUT_1148                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x047C);
volatile uint8_t xdata g_rw_gam_lut_047Dh_GAM_LUT_1149                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x047D);

volatile uint8_t xdata g_rw_gam_lut_047Eh_GAM_LUT_1150                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x047E);
volatile uint8_t xdata g_rw_gam_lut_047Fh_GAM_LUT_1151                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x047F);
volatile uint8_t xdata g_rw_gam_lut_0480h_GAM_LUT_1152                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0480);
volatile uint8_t xdata g_rw_gam_lut_0481h_GAM_LUT_1153                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0481);
volatile uint8_t xdata g_rw_gam_lut_0482h_GAM_LUT_1154                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0482);

volatile uint8_t xdata g_rw_gam_lut_0483h_GAM_LUT_1155                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0483);
volatile uint8_t xdata g_rw_gam_lut_0484h_GAM_LUT_1156                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0484);
volatile uint8_t xdata g_rw_gam_lut_0485h_GAM_LUT_1157                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0485);
volatile uint8_t xdata g_rw_gam_lut_0486h_GAM_LUT_1158                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0486);
volatile uint8_t xdata g_rw_gam_lut_0487h_GAM_LUT_1159                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0487);

volatile uint8_t xdata g_rw_gam_lut_0488h_GAM_LUT_1160                                         _at_  (GAM_LUT_FIELD_XMEM_BASE + 0x0488);

#endif 
