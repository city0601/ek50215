#include "inproc.h"
#include "hw_mem_map.h"

#ifdef INPROC_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------
volatile union rw_inproc_0000h xdata g_rw_inproc_0000h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0000);
volatile union rw_inproc_0001h xdata g_rw_inproc_0001h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0001);
volatile union rw_inproc_0002h xdata g_rw_inproc_0002h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0002);
volatile union rw_inproc_0003h xdata g_rw_inproc_0003h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0003);
volatile union rw_inproc_0004h xdata g_rw_inproc_0004h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0004);

volatile union rw_inproc_0005h xdata g_rw_inproc_0005h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0005);
volatile union rw_inproc_0006h xdata g_rw_inproc_0006h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0006);
volatile union rw_inproc_0007h xdata g_rw_inproc_0007h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0007);
volatile union rw_inproc_0008h xdata g_rw_inproc_0008h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0008);
volatile union rw_inproc_0009h xdata g_rw_inproc_0009h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0009);

volatile union rw_inproc_000Ah xdata g_rw_inproc_000Ah  _at_  (INPROC_FIELD_XMEM_BASE + 0x000A);
volatile union rw_inproc_000Bh xdata g_rw_inproc_000Bh  _at_  (INPROC_FIELD_XMEM_BASE + 0x000B);
volatile union rw_inproc_000Dh xdata g_rw_inproc_000Dh  _at_  (INPROC_FIELD_XMEM_BASE + 0x000D);
volatile union rw_inproc_000Eh xdata g_rw_inproc_000Eh  _at_  (INPROC_FIELD_XMEM_BASE + 0x000E);
volatile union rw_inproc_0010h xdata g_rw_inproc_0010h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0010);

volatile union rw_inproc_0013h xdata g_rw_inproc_0013h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0013);
volatile union rw_inproc_0015h xdata g_rw_inproc_0015h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0015);
volatile union rw_inproc_0016h xdata g_rw_inproc_0016h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0016);
volatile union rw_inproc_0018h xdata g_rw_inproc_0018h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0018);
volatile union rw_inproc_0019h xdata g_rw_inproc_0019h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0019);

volatile union rw_inproc_001Bh xdata g_rw_inproc_001Bh  _at_  (INPROC_FIELD_XMEM_BASE + 0x001B);
volatile union rw_inproc_001Ch xdata g_rw_inproc_001Ch  _at_  (INPROC_FIELD_XMEM_BASE + 0x001C);
volatile union rw_inproc_001Dh xdata g_rw_inproc_001Dh  _at_  (INPROC_FIELD_XMEM_BASE + 0x001D);
volatile union rw_inproc_001Eh xdata g_rw_inproc_001Eh  _at_  (INPROC_FIELD_XMEM_BASE + 0x001E);
volatile union rw_inproc_001Fh xdata g_rw_inproc_001Fh  _at_  (INPROC_FIELD_XMEM_BASE + 0x001F);

volatile union rw_inproc_0020h xdata g_rw_inproc_0020h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0020);
volatile union rw_inproc_0022h xdata g_rw_inproc_0022h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0022);
volatile union rw_inproc_0023h xdata g_rw_inproc_0023h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0023);
volatile union rw_inproc_0025h xdata g_rw_inproc_0025h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0025);
volatile union rw_inproc_0027h xdata g_rw_inproc_0027h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0027);

volatile union rw_inproc_0029h xdata g_rw_inproc_0029h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0029);
volatile union rw_inproc_002Ah xdata g_rw_inproc_002Ah  _at_  (INPROC_FIELD_XMEM_BASE + 0x002A);
volatile union rw_inproc_002Bh xdata g_rw_inproc_002Bh  _at_  (INPROC_FIELD_XMEM_BASE + 0x002B);
volatile union rw_inproc_002Ch xdata g_rw_inproc_002Ch  _at_  (INPROC_FIELD_XMEM_BASE + 0x002C);
volatile union rw_inproc_002Dh xdata g_rw_inproc_002Dh  _at_  (INPROC_FIELD_XMEM_BASE + 0x002D);

volatile union rw_inproc_002Eh xdata g_rw_inproc_002Eh  _at_  (INPROC_FIELD_XMEM_BASE + 0x002E);
volatile union rw_inproc_0030h xdata g_rw_inproc_0030h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0030);
volatile union rw_inproc_0031h xdata g_rw_inproc_0031h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0031);
volatile union rw_inproc_0032h xdata g_rw_inproc_0032h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0032);
volatile union rw_inproc_0033h xdata g_rw_inproc_0033h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0033);

volatile union rw_inproc_0034h xdata g_rw_inproc_0034h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0034);
volatile union rw_inproc_0035h xdata g_rw_inproc_0035h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0035);
volatile union rw_inproc_0036h xdata g_rw_inproc_0036h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0036);
volatile union rw_inproc_0037h xdata g_rw_inproc_0037h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0037);
volatile union rw_inproc_003Ah xdata g_rw_inproc_003Ah  _at_  (INPROC_FIELD_XMEM_BASE + 0x003A);

volatile union rw_inproc_003Ch xdata g_rw_inproc_003Ch  _at_  (INPROC_FIELD_XMEM_BASE + 0x003C);
volatile union rw_inproc_003Eh xdata g_rw_inproc_003Eh  _at_  (INPROC_FIELD_XMEM_BASE + 0x003E);
volatile union rw_inproc_003Fh xdata g_rw_inproc_003Fh  _at_  (INPROC_FIELD_XMEM_BASE + 0x003F);
volatile union rw_inproc_0041h xdata g_rw_inproc_0041h  _at_  (INPROC_FIELD_XMEM_BASE + 0x0041);

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_inproc_000Ch_pstate_detect_region                                  _at_  (INPROC_FIELD_XMEM_BASE + 0x000C);
volatile uint8_t xdata g_rw_inproc_000Fh_hdisp_pg                                              _at_  (INPROC_FIELD_XMEM_BASE + 0x000F);
volatile uint8_t xdata g_rw_inproc_0011h_vdisp_pg                                              _at_  (INPROC_FIELD_XMEM_BASE + 0x0011);
volatile uint8_t xdata g_rw_inproc_0012h_disp_htot_0                                           _at_  (INPROC_FIELD_XMEM_BASE + 0x0012);
volatile uint8_t xdata g_rw_inproc_0014h_disp_vtot_0                                           _at_  (INPROC_FIELD_XMEM_BASE + 0x0014);

volatile uint8_t xdata g_rw_inproc_0017h_disp_vtot_1                                           _at_  (INPROC_FIELD_XMEM_BASE + 0x0017);
volatile uint8_t xdata g_rw_inproc_001Ah_disp_vtot_bus_en                                      _at_  (INPROC_FIELD_XMEM_BASE + 0x001A);
volatile uint8_t xdata g_rw_inproc_0021h_htotal_min_lvclk                                      _at_  (INPROC_FIELD_XMEM_BASE + 0x0021);
volatile uint8_t xdata g_rw_inproc_0024h_vdisp                                                 _at_  (INPROC_FIELD_XMEM_BASE + 0x0024);
volatile uint8_t xdata g_rw_inproc_0026h_vtotal_min                                            _at_  (INPROC_FIELD_XMEM_BASE + 0x0026);

volatile uint8_t xdata g_rw_inproc_0028h_vtotal_max                                            _at_  (INPROC_FIELD_XMEM_BASE + 0x0028);
volatile uint8_t xdata g_rw_inproc_002Fh_front_end_vact_min                                    _at_  (INPROC_FIELD_XMEM_BASE + 0x002F);
volatile uint8_t xdata g_rw_inproc_0038h_cap_line_cnt                                          _at_  (INPROC_FIELD_XMEM_BASE + 0x0038);
volatile uint8_t xdata g_rw_inproc_0039h_cap_pixel_cnt                                         _at_  (INPROC_FIELD_XMEM_BASE + 0x0039);
volatile uint8_t xdata g_rw_inproc_003Bh_htot_cmp_reg_min                                      _at_  (INPROC_FIELD_XMEM_BASE + 0x003B);

volatile uint8_t xdata g_rw_inproc_003Dh_htot_rec                                              _at_  (INPROC_FIELD_XMEM_BASE + 0x003D);
volatile uint8_t xdata g_rw_inproc_0040h_hs_pin_width                                          _at_  (INPROC_FIELD_XMEM_BASE + 0x0040);
volatile uint8_t xdata g_rw_inproc_0042h_frun_dly                                              _at_  (INPROC_FIELD_XMEM_BASE + 0x0042);

#endif 
