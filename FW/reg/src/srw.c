#include "srw.h"
#include "hw_mem_map.h"

#ifdef SRW_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------
volatile union rw_srw_000Dh xdata g_rw_srw_000Dh  _at_  (SRW_FIELD_XMEM_BASE + 0x000D);
volatile union rw_srw_000Eh xdata g_rw_srw_000Eh  _at_  (SRW_FIELD_XMEM_BASE + 0x000E);

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_srw_0000h_reserved1                                                _at_  (SRW_FIELD_XMEM_BASE + 0x0000);
volatile uint8_t xdata g_rw_srw_0001h_lut_ip_sel                                               _at_  (SRW_FIELD_XMEM_BASE + 0x0001);
volatile uint8_t xdata g_rw_srw_0002h_lut_sram_sel                                             _at_  (SRW_FIELD_XMEM_BASE + 0x0002);
volatile uint8_t xdata g_rw_srw_0003h_lut_addr                                                 _at_  (SRW_FIELD_XMEM_BASE + 0x0003);
volatile uint8_t xdata g_rw_srw_0004h_lut_addr                                                 _at_  (SRW_FIELD_XMEM_BASE + 0x0004);

volatile uint8_t xdata g_rw_srw_0005h_lut_wrdt                                                 _at_  (SRW_FIELD_XMEM_BASE + 0x0005);
volatile uint8_t xdata g_rw_srw_0006h_lut_wrdt                                                 _at_  (SRW_FIELD_XMEM_BASE + 0x0006);
volatile uint8_t xdata g_rw_srw_0007h_lut_wrdt                                                 _at_  (SRW_FIELD_XMEM_BASE + 0x0007);
volatile uint8_t xdata g_rw_srw_0008h_lut_wrdt                                                 _at_  (SRW_FIELD_XMEM_BASE + 0x0008);
volatile uint8_t xdata g_rw_srw_0009h_lut_rddt_lth                                             _at_  (SRW_FIELD_XMEM_BASE + 0x0009);

volatile uint8_t xdata g_rw_srw_000Ah_lut_rddt_lth                                             _at_  (SRW_FIELD_XMEM_BASE + 0x000A);
volatile uint8_t xdata g_rw_srw_000Bh_lut_rddt_lth                                             _at_  (SRW_FIELD_XMEM_BASE + 0x000B);
volatile uint8_t xdata g_rw_srw_000Ch_lut_rddt_lth                                             _at_  (SRW_FIELD_XMEM_BASE + 0x000C);

#endif 
