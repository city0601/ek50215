#include "p2p.h"
#include "hw_mem_map.h"

#ifdef P2P_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------
volatile union rw_p2p_0000h xdata g_rw_p2p_0000h  _at_  (P2P_FIELD_XMEM_BASE + 0x0000);
volatile union rw_p2p_0001h xdata g_rw_p2p_0001h  _at_  (P2P_FIELD_XMEM_BASE + 0x0001);
volatile union rw_p2p_0002h xdata g_rw_p2p_0002h  _at_  (P2P_FIELD_XMEM_BASE + 0x0002);
volatile union rw_p2p_0004h xdata g_rw_p2p_0004h  _at_  (P2P_FIELD_XMEM_BASE + 0x0004);
volatile union rw_p2p_0006h xdata g_rw_p2p_0006h  _at_  (P2P_FIELD_XMEM_BASE + 0x0006);

volatile union rw_p2p_0009h xdata g_rw_p2p_0009h  _at_  (P2P_FIELD_XMEM_BASE + 0x0009);
volatile union rw_p2p_000Eh xdata g_rw_p2p_000Eh  _at_  (P2P_FIELD_XMEM_BASE + 0x000E);
volatile union rw_p2p_0024h xdata g_rw_p2p_0024h  _at_  (P2P_FIELD_XMEM_BASE + 0x0024);
volatile union rw_p2p_0025h xdata g_rw_p2p_0025h  _at_  (P2P_FIELD_XMEM_BASE + 0x0025);
volatile union rw_p2p_0026h xdata g_rw_p2p_0026h  _at_  (P2P_FIELD_XMEM_BASE + 0x0026);

volatile union rw_p2p_0027h xdata g_rw_p2p_0027h  _at_  (P2P_FIELD_XMEM_BASE + 0x0027);
volatile union rw_p2p_0028h xdata g_rw_p2p_0028h  _at_  (P2P_FIELD_XMEM_BASE + 0x0028);
volatile union rw_p2p_0029h xdata g_rw_p2p_0029h  _at_  (P2P_FIELD_XMEM_BASE + 0x0029);
volatile union rw_p2p_002Ah xdata g_rw_p2p_002Ah  _at_  (P2P_FIELD_XMEM_BASE + 0x002A);
volatile union rw_p2p_002Bh xdata g_rw_p2p_002Bh  _at_  (P2P_FIELD_XMEM_BASE + 0x002B);

volatile union rw_p2p_002Ch xdata g_rw_p2p_002Ch  _at_  (P2P_FIELD_XMEM_BASE + 0x002C);
volatile union rw_p2p_002Dh xdata g_rw_p2p_002Dh  _at_  (P2P_FIELD_XMEM_BASE + 0x002D);
volatile union rw_p2p_002Eh xdata g_rw_p2p_002Eh  _at_  (P2P_FIELD_XMEM_BASE + 0x002E);
volatile union rw_p2p_0031h xdata g_rw_p2p_0031h  _at_  (P2P_FIELD_XMEM_BASE + 0x0031);
volatile union rw_p2p_0033h xdata g_rw_p2p_0033h  _at_  (P2P_FIELD_XMEM_BASE + 0x0033);

volatile union rw_p2p_0035h xdata g_rw_p2p_0035h  _at_  (P2P_FIELD_XMEM_BASE + 0x0035);
volatile union rw_p2p_0036h xdata g_rw_p2p_0036h  _at_  (P2P_FIELD_XMEM_BASE + 0x0036);

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_p2p_0003h_vact_num                                                 _at_  (P2P_FIELD_XMEM_BASE + 0x0003);
volatile uint8_t xdata g_rw_p2p_0005h_enddmy_num                                               _at_  (P2P_FIELD_XMEM_BASE + 0x0005);
volatile uint8_t xdata g_rw_p2p_0007h_mrst_stdt                                                _at_  (P2P_FIELD_XMEM_BASE + 0x0007);
volatile uint8_t xdata g_rw_p2p_0008h_mrst_enddt                                               _at_  (P2P_FIELD_XMEM_BASE + 0x0008);
volatile uint8_t xdata g_rw_p2p_000Ah_pol_posdt                                                _at_  (P2P_FIELD_XMEM_BASE + 0x000A);

volatile uint8_t xdata g_rw_p2p_000Bh_pol_negdt                                                _at_  (P2P_FIELD_XMEM_BASE + 0x000B);
volatile uint8_t xdata g_rw_p2p_000Ch_lcmd1                                                    _at_  (P2P_FIELD_XMEM_BASE + 0x000C);
volatile uint8_t xdata g_rw_p2p_000Dh_lcmd2                                                    _at_  (P2P_FIELD_XMEM_BASE + 0x000D);
volatile uint8_t xdata g_rw_p2p_000Fh_key_code                                                 _at_  (P2P_FIELD_XMEM_BASE + 0x000F);
volatile uint8_t xdata g_rw_p2p_0010h_key_code                                                 _at_  (P2P_FIELD_XMEM_BASE + 0x0010);

volatile uint8_t xdata g_rw_p2p_0011h_gam1                                                     _at_  (P2P_FIELD_XMEM_BASE + 0x0011);
volatile uint8_t xdata g_rw_p2p_0012h_gam2                                                     _at_  (P2P_FIELD_XMEM_BASE + 0x0012);
volatile uint8_t xdata g_rw_p2p_0013h_gam3                                                     _at_  (P2P_FIELD_XMEM_BASE + 0x0013);
volatile uint8_t xdata g_rw_p2p_0014h_gam4                                                     _at_  (P2P_FIELD_XMEM_BASE + 0x0014);
volatile uint8_t xdata g_rw_p2p_0015h_gam5                                                     _at_  (P2P_FIELD_XMEM_BASE + 0x0015);

volatile uint8_t xdata g_rw_p2p_0016h_gam6                                                     _at_  (P2P_FIELD_XMEM_BASE + 0x0016);
volatile uint8_t xdata g_rw_p2p_0017h_gam7                                                     _at_  (P2P_FIELD_XMEM_BASE + 0x0017);
volatile uint8_t xdata g_rw_p2p_0018h_gam8                                                     _at_  (P2P_FIELD_XMEM_BASE + 0x0018);
volatile uint8_t xdata g_rw_p2p_0019h_gam9                                                     _at_  (P2P_FIELD_XMEM_BASE + 0x0019);
volatile uint8_t xdata g_rw_p2p_001Ah_gam10                                                    _at_  (P2P_FIELD_XMEM_BASE + 0x001A);

volatile uint8_t xdata g_rw_p2p_001Bh_gam11                                                    _at_  (P2P_FIELD_XMEM_BASE + 0x001B);
volatile uint8_t xdata g_rw_p2p_001Ch_gam12                                                    _at_  (P2P_FIELD_XMEM_BASE + 0x001C);
volatile uint8_t xdata g_rw_p2p_001Dh_gam13                                                    _at_  (P2P_FIELD_XMEM_BASE + 0x001D);
volatile uint8_t xdata g_rw_p2p_001Eh_gam14                                                    _at_  (P2P_FIELD_XMEM_BASE + 0x001E);
volatile uint8_t xdata g_rw_p2p_001Fh_byte15                                                   _at_  (P2P_FIELD_XMEM_BASE + 0x001F);

volatile uint8_t xdata g_rw_p2p_0020h_reserve1                                                 _at_  (P2P_FIELD_XMEM_BASE + 0x0020);
volatile uint8_t xdata g_rw_p2p_0021h_reserve2                                                 _at_  (P2P_FIELD_XMEM_BASE + 0x0021);
volatile uint8_t xdata g_rw_p2p_0022h_reserve3                                                 _at_  (P2P_FIELD_XMEM_BASE + 0x0022);
volatile uint8_t xdata g_rw_p2p_0023h_reserve4                                                 _at_  (P2P_FIELD_XMEM_BASE + 0x0023);
volatile uint8_t xdata g_rw_p2p_002Fh_dc_test_dt                                               _at_  (P2P_FIELD_XMEM_BASE + 0x002F);

volatile uint8_t xdata g_rw_p2p_0030h_dc_test_dt                                               _at_  (P2P_FIELD_XMEM_BASE + 0x0030);
volatile uint8_t xdata g_rw_p2p_0032h_prbs_ck_en                                               _at_  (P2P_FIELD_XMEM_BASE + 0x0032);
volatile uint8_t xdata g_rw_p2p_0034h_mil_chksum                                               _at_  (P2P_FIELD_XMEM_BASE + 0x0034);

#endif 
