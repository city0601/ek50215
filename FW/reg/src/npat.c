#include "npat.h"
#include "hw_mem_map.h"

#ifdef NPAT_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------
volatile union rw_npat_0000h xdata g_rw_npat_0000h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0000);
volatile union rw_npat_0001h xdata g_rw_npat_0001h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0001);
volatile union rw_npat_0002h xdata g_rw_npat_0002h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0002);
volatile union rw_npat_0003h xdata g_rw_npat_0003h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0003);
volatile union rw_npat_0007h xdata g_rw_npat_0007h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0007);

volatile union rw_npat_0008h xdata g_rw_npat_0008h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0008);
volatile union rw_npat_0009h xdata g_rw_npat_0009h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0009);
volatile union rw_npat_000Ah xdata g_rw_npat_000Ah  _at_  (NPAT_FIELD_XMEM_BASE + 0x000A);
volatile union rw_npat_000Ch xdata g_rw_npat_000Ch  _at_  (NPAT_FIELD_XMEM_BASE + 0x000C);
volatile union rw_npat_000Dh xdata g_rw_npat_000Dh  _at_  (NPAT_FIELD_XMEM_BASE + 0x000D);

volatile union rw_npat_000Eh xdata g_rw_npat_000Eh  _at_  (NPAT_FIELD_XMEM_BASE + 0x000E);
volatile union rw_npat_000Fh xdata g_rw_npat_000Fh  _at_  (NPAT_FIELD_XMEM_BASE + 0x000F);
volatile union rw_npat_0010h xdata g_rw_npat_0010h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0010);
volatile union rw_npat_0011h xdata g_rw_npat_0011h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0011);
volatile union rw_npat_0012h xdata g_rw_npat_0012h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0012);

volatile union rw_npat_0013h xdata g_rw_npat_0013h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0013);
volatile union rw_npat_0014h xdata g_rw_npat_0014h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0014);
volatile union rw_npat_0015h xdata g_rw_npat_0015h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0015);
volatile union rw_npat_0016h xdata g_rw_npat_0016h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0016);
volatile union rw_npat_0017h xdata g_rw_npat_0017h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0017);

volatile union rw_npat_0019h xdata g_rw_npat_0019h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0019);
volatile union rw_npat_001Ah xdata g_rw_npat_001Ah  _at_  (NPAT_FIELD_XMEM_BASE + 0x001A);
volatile union rw_npat_001Bh xdata g_rw_npat_001Bh  _at_  (NPAT_FIELD_XMEM_BASE + 0x001B);
volatile union rw_npat_001Ch xdata g_rw_npat_001Ch  _at_  (NPAT_FIELD_XMEM_BASE + 0x001C);
volatile union rw_npat_001Dh xdata g_rw_npat_001Dh  _at_  (NPAT_FIELD_XMEM_BASE + 0x001D);

volatile union rw_npat_001Eh xdata g_rw_npat_001Eh  _at_  (NPAT_FIELD_XMEM_BASE + 0x001E);
volatile union rw_npat_001Fh xdata g_rw_npat_001Fh  _at_  (NPAT_FIELD_XMEM_BASE + 0x001F);
volatile union rw_npat_0020h xdata g_rw_npat_0020h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0020);
volatile union rw_npat_0021h xdata g_rw_npat_0021h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0021);
volatile union rw_npat_0022h xdata g_rw_npat_0022h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0022);

volatile union rw_npat_0023h xdata g_rw_npat_0023h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0023);
volatile union rw_npat_0024h xdata g_rw_npat_0024h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0024);
volatile union rw_npat_0025h xdata g_rw_npat_0025h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0025);
volatile union rw_npat_0026h xdata g_rw_npat_0026h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0026);
volatile union rw_npat_0027h xdata g_rw_npat_0027h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0027);

volatile union rw_npat_0028h xdata g_rw_npat_0028h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0028);
volatile union rw_npat_0029h xdata g_rw_npat_0029h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0029);
volatile union rw_npat_002Ah xdata g_rw_npat_002Ah  _at_  (NPAT_FIELD_XMEM_BASE + 0x002A);
volatile union rw_npat_002Bh xdata g_rw_npat_002Bh  _at_  (NPAT_FIELD_XMEM_BASE + 0x002B);
volatile union rw_npat_002Ch xdata g_rw_npat_002Ch  _at_  (NPAT_FIELD_XMEM_BASE + 0x002C);

volatile union rw_npat_002Dh xdata g_rw_npat_002Dh  _at_  (NPAT_FIELD_XMEM_BASE + 0x002D);
volatile union rw_npat_002Eh xdata g_rw_npat_002Eh  _at_  (NPAT_FIELD_XMEM_BASE + 0x002E);
volatile union rw_npat_002Fh xdata g_rw_npat_002Fh  _at_  (NPAT_FIELD_XMEM_BASE + 0x002F);
volatile union rw_npat_0030h xdata g_rw_npat_0030h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0030);
volatile union rw_npat_0031h xdata g_rw_npat_0031h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0031);

volatile union rw_npat_0032h xdata g_rw_npat_0032h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0032);
volatile union rw_npat_0033h xdata g_rw_npat_0033h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0033);
volatile union rw_npat_0036h xdata g_rw_npat_0036h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0036);
volatile union rw_npat_0037h xdata g_rw_npat_0037h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0037);
volatile union rw_npat_0038h xdata g_rw_npat_0038h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0038);

volatile union rw_npat_003Bh xdata g_rw_npat_003Bh  _at_  (NPAT_FIELD_XMEM_BASE + 0x003B);
volatile union rw_npat_003Ch xdata g_rw_npat_003Ch  _at_  (NPAT_FIELD_XMEM_BASE + 0x003C);
volatile union rw_npat_003Dh xdata g_rw_npat_003Dh  _at_  (NPAT_FIELD_XMEM_BASE + 0x003D);
volatile union rw_npat_0040h xdata g_rw_npat_0040h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0040);
volatile union rw_npat_0041h xdata g_rw_npat_0041h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0041);

volatile union rw_npat_0042h xdata g_rw_npat_0042h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0042);
volatile union rw_npat_0045h xdata g_rw_npat_0045h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0045);
volatile union rw_npat_0046h xdata g_rw_npat_0046h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0046);
volatile union rw_npat_0047h xdata g_rw_npat_0047h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0047);
volatile union rw_npat_004Ah xdata g_rw_npat_004Ah  _at_  (NPAT_FIELD_XMEM_BASE + 0x004A);

volatile union rw_npat_004Bh xdata g_rw_npat_004Bh  _at_  (NPAT_FIELD_XMEM_BASE + 0x004B);
volatile union rw_npat_004Ch xdata g_rw_npat_004Ch  _at_  (NPAT_FIELD_XMEM_BASE + 0x004C);
volatile union rw_npat_004Fh xdata g_rw_npat_004Fh  _at_  (NPAT_FIELD_XMEM_BASE + 0x004F);
volatile union rw_npat_0050h xdata g_rw_npat_0050h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0050);
volatile union rw_npat_0051h xdata g_rw_npat_0051h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0051);

volatile union rw_npat_0054h xdata g_rw_npat_0054h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0054);
volatile union rw_npat_0055h xdata g_rw_npat_0055h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0055);
volatile union rw_npat_0056h xdata g_rw_npat_0056h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0056);
volatile union rw_npat_0059h xdata g_rw_npat_0059h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0059);
volatile union rw_npat_005Ah xdata g_rw_npat_005Ah  _at_  (NPAT_FIELD_XMEM_BASE + 0x005A);

volatile union rw_npat_005Bh xdata g_rw_npat_005Bh  _at_  (NPAT_FIELD_XMEM_BASE + 0x005B);
volatile union rw_npat_005Eh xdata g_rw_npat_005Eh  _at_  (NPAT_FIELD_XMEM_BASE + 0x005E);
volatile union rw_npat_005Fh xdata g_rw_npat_005Fh  _at_  (NPAT_FIELD_XMEM_BASE + 0x005F);
volatile union rw_npat_0060h xdata g_rw_npat_0060h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0060);
volatile union rw_npat_0063h xdata g_rw_npat_0063h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0063);

volatile union rw_npat_0064h xdata g_rw_npat_0064h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0064);
volatile union rw_npat_0065h xdata g_rw_npat_0065h  _at_  (NPAT_FIELD_XMEM_BASE + 0x0065);

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_npat_0004h_delta_vgray                                             _at_  (NPAT_FIELD_XMEM_BASE + 0x0004);
volatile uint8_t xdata g_rw_npat_0005h_delta_hgray                                             _at_  (NPAT_FIELD_XMEM_BASE + 0x0005);
volatile uint8_t xdata g_rw_npat_0006h_patx_slope                                              _at_  (NPAT_FIELD_XMEM_BASE + 0x0006);
volatile uint8_t xdata g_rw_npat_000Bh_bar_g                                                   _at_  (NPAT_FIELD_XMEM_BASE + 0x000B);
volatile uint8_t xdata g_rw_npat_0018h_htot_rec                                                _at_  (NPAT_FIELD_XMEM_BASE + 0x0018);

volatile uint8_t xdata g_rw_npat_0034h_gc_a_g                                                  _at_  (NPAT_FIELD_XMEM_BASE + 0x0034);
volatile uint8_t xdata g_rw_npat_0035h_gc_a_b                                                  _at_  (NPAT_FIELD_XMEM_BASE + 0x0035);
volatile uint8_t xdata g_rw_npat_0039h_gc_b_b                                                  _at_  (NPAT_FIELD_XMEM_BASE + 0x0039);
volatile uint8_t xdata g_rw_npat_003Ah_gc_c_r                                                  _at_  (NPAT_FIELD_XMEM_BASE + 0x003A);
volatile uint8_t xdata g_rw_npat_003Eh_gc_d_r                                                  _at_  (NPAT_FIELD_XMEM_BASE + 0x003E);

volatile uint8_t xdata g_rw_npat_003Fh_gc_d_g                                                  _at_  (NPAT_FIELD_XMEM_BASE + 0x003F);
volatile uint8_t xdata g_rw_npat_0043h_gc_e_g                                                  _at_  (NPAT_FIELD_XMEM_BASE + 0x0043);
volatile uint8_t xdata g_rw_npat_0044h_gc_e_b                                                  _at_  (NPAT_FIELD_XMEM_BASE + 0x0044);
volatile uint8_t xdata g_rw_npat_0048h_gc_f_b                                                  _at_  (NPAT_FIELD_XMEM_BASE + 0x0048);
volatile uint8_t xdata g_rw_npat_0049h_gc_l_r                                                  _at_  (NPAT_FIELD_XMEM_BASE + 0x0049);

volatile uint8_t xdata g_rw_npat_004Dh_gc_bg_r                                                 _at_  (NPAT_FIELD_XMEM_BASE + 0x004D);
volatile uint8_t xdata g_rw_npat_004Eh_gc_bg_g                                                 _at_  (NPAT_FIELD_XMEM_BASE + 0x004E);
volatile uint8_t xdata g_rw_npat_0052h_arb_a_g                                                 _at_  (NPAT_FIELD_XMEM_BASE + 0x0052);
volatile uint8_t xdata g_rw_npat_0053h_arb_a_b                                                 _at_  (NPAT_FIELD_XMEM_BASE + 0x0053);
volatile uint8_t xdata g_rw_npat_0057h_arb_b_b                                                 _at_  (NPAT_FIELD_XMEM_BASE + 0x0057);

volatile uint8_t xdata g_rw_npat_0058h_arb_c_r                                                 _at_  (NPAT_FIELD_XMEM_BASE + 0x0058);
volatile uint8_t xdata g_rw_npat_005Ch_arb_d_r                                                 _at_  (NPAT_FIELD_XMEM_BASE + 0x005C);
volatile uint8_t xdata g_rw_npat_005Dh_arb_d_g                                                 _at_  (NPAT_FIELD_XMEM_BASE + 0x005D);
volatile uint8_t xdata g_rw_npat_0061h_arb_e_g                                                 _at_  (NPAT_FIELD_XMEM_BASE + 0x0061);
volatile uint8_t xdata g_rw_npat_0062h_arb_e_b                                                 _at_  (NPAT_FIELD_XMEM_BASE + 0x0062);

volatile uint8_t xdata g_rw_npat_0066h_arb_f_b                                                 _at_  (NPAT_FIELD_XMEM_BASE + 0x0066);
volatile uint8_t xdata g_rw_npat_0067h_freerun_r0                                              _at_  (NPAT_FIELD_XMEM_BASE + 0x0067);
volatile uint8_t xdata g_rw_npat_0068h_freerun_g0                                              _at_  (NPAT_FIELD_XMEM_BASE + 0x0068);
volatile uint8_t xdata g_rw_npat_0069h_freerun_b0                                              _at_  (NPAT_FIELD_XMEM_BASE + 0x0069);
volatile uint8_t xdata g_rw_npat_006Ah_freerun_r1                                              _at_  (NPAT_FIELD_XMEM_BASE + 0x006A);

volatile uint8_t xdata g_rw_npat_006Bh_freerun_g1                                              _at_  (NPAT_FIELD_XMEM_BASE + 0x006B);
volatile uint8_t xdata g_rw_npat_006Ch_freerun_b1                                              _at_  (NPAT_FIELD_XMEM_BASE + 0x006C);

#endif 
