#include "lxlod.h"
#include "hw_mem_map.h"

#ifdef LXLOD_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------
volatile union rw_lxlod_0000h xdata g_rw_lxlod_0000h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0000);
volatile union rw_lxlod_0001h xdata g_rw_lxlod_0001h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0001);
volatile union rw_lxlod_0002h xdata g_rw_lxlod_0002h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0002);
volatile union rw_lxlod_0003h xdata g_rw_lxlod_0003h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0003);
volatile union rw_lxlod_0004h xdata g_rw_lxlod_0004h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0004);

volatile union rw_lxlod_0005h xdata g_rw_lxlod_0005h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0005);
volatile union rw_lxlod_0006h xdata g_rw_lxlod_0006h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0006);
volatile union rw_lxlod_0007h xdata g_rw_lxlod_0007h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0007);
volatile union rw_lxlod_0008h xdata g_rw_lxlod_0008h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0008);
volatile union rw_lxlod_0009h xdata g_rw_lxlod_0009h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0009);

volatile union rw_lxlod_000Ah xdata g_rw_lxlod_000Ah  _at_  (LXLOD_FIELD_XMEM_BASE + 0x000A);
volatile union rw_lxlod_000Bh xdata g_rw_lxlod_000Bh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x000B);
volatile union rw_lxlod_000Ch xdata g_rw_lxlod_000Ch  _at_  (LXLOD_FIELD_XMEM_BASE + 0x000C);
volatile union rw_lxlod_000Dh xdata g_rw_lxlod_000Dh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x000D);
volatile union rw_lxlod_000Eh xdata g_rw_lxlod_000Eh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x000E);

volatile union rw_lxlod_000Fh xdata g_rw_lxlod_000Fh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x000F);
volatile union rw_lxlod_0010h xdata g_rw_lxlod_0010h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0010);
volatile union rw_lxlod_0011h xdata g_rw_lxlod_0011h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0011);
volatile union rw_lxlod_0012h xdata g_rw_lxlod_0012h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0012);
volatile union rw_lxlod_0013h xdata g_rw_lxlod_0013h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0013);

volatile union rw_lxlod_0014h xdata g_rw_lxlod_0014h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0014);
volatile union rw_lxlod_0015h xdata g_rw_lxlod_0015h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0015);
volatile union rw_lxlod_0016h xdata g_rw_lxlod_0016h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0016);
volatile union rw_lxlod_0017h xdata g_rw_lxlod_0017h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0017);
volatile union rw_lxlod_0018h xdata g_rw_lxlod_0018h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0018);

volatile union rw_lxlod_0019h xdata g_rw_lxlod_0019h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0019);
volatile union rw_lxlod_001Ah xdata g_rw_lxlod_001Ah  _at_  (LXLOD_FIELD_XMEM_BASE + 0x001A);
volatile union rw_lxlod_001Bh xdata g_rw_lxlod_001Bh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x001B);
volatile union rw_lxlod_001Ch xdata g_rw_lxlod_001Ch  _at_  (LXLOD_FIELD_XMEM_BASE + 0x001C);
volatile union rw_lxlod_001Dh xdata g_rw_lxlod_001Dh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x001D);

volatile union rw_lxlod_001Eh xdata g_rw_lxlod_001Eh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x001E);
volatile union rw_lxlod_001Fh xdata g_rw_lxlod_001Fh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x001F);
volatile union rw_lxlod_0020h xdata g_rw_lxlod_0020h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0020);
volatile union rw_lxlod_0021h xdata g_rw_lxlod_0021h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0021);
volatile union rw_lxlod_0022h xdata g_rw_lxlod_0022h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0022);

volatile union rw_lxlod_0023h xdata g_rw_lxlod_0023h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0023);
volatile union rw_lxlod_0024h xdata g_rw_lxlod_0024h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0024);
volatile union rw_lxlod_0025h xdata g_rw_lxlod_0025h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0025);
volatile union rw_lxlod_0026h xdata g_rw_lxlod_0026h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0026);
volatile union rw_lxlod_0027h xdata g_rw_lxlod_0027h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0027);

volatile union rw_lxlod_0028h xdata g_rw_lxlod_0028h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0028);
volatile union rw_lxlod_0029h xdata g_rw_lxlod_0029h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0029);
volatile union rw_lxlod_002Ah xdata g_rw_lxlod_002Ah  _at_  (LXLOD_FIELD_XMEM_BASE + 0x002A);
volatile union rw_lxlod_002Bh xdata g_rw_lxlod_002Bh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x002B);
volatile union rw_lxlod_002Ch xdata g_rw_lxlod_002Ch  _at_  (LXLOD_FIELD_XMEM_BASE + 0x002C);

volatile union rw_lxlod_002Dh xdata g_rw_lxlod_002Dh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x002D);
volatile union rw_lxlod_002Eh xdata g_rw_lxlod_002Eh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x002E);
volatile union rw_lxlod_002Fh xdata g_rw_lxlod_002Fh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x002F);
volatile union rw_lxlod_0030h xdata g_rw_lxlod_0030h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0030);
volatile union rw_lxlod_0031h xdata g_rw_lxlod_0031h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0031);

volatile union rw_lxlod_0032h xdata g_rw_lxlod_0032h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0032);
volatile union rw_lxlod_0033h xdata g_rw_lxlod_0033h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0033);
volatile union rw_lxlod_0034h xdata g_rw_lxlod_0034h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0034);
volatile union rw_lxlod_0035h xdata g_rw_lxlod_0035h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0035);
volatile union rw_lxlod_0036h xdata g_rw_lxlod_0036h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0036);

volatile union rw_lxlod_0037h xdata g_rw_lxlod_0037h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0037);
volatile union rw_lxlod_0038h xdata g_rw_lxlod_0038h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0038);
volatile union rw_lxlod_0039h xdata g_rw_lxlod_0039h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0039);
volatile union rw_lxlod_003Ah xdata g_rw_lxlod_003Ah  _at_  (LXLOD_FIELD_XMEM_BASE + 0x003A);
volatile union rw_lxlod_003Bh xdata g_rw_lxlod_003Bh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x003B);

volatile union rw_lxlod_003Ch xdata g_rw_lxlod_003Ch  _at_  (LXLOD_FIELD_XMEM_BASE + 0x003C);
volatile union rw_lxlod_003Dh xdata g_rw_lxlod_003Dh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x003D);
volatile union rw_lxlod_003Eh xdata g_rw_lxlod_003Eh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x003E);
volatile union rw_lxlod_003Fh xdata g_rw_lxlod_003Fh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x003F);
volatile union rw_lxlod_0040h xdata g_rw_lxlod_0040h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0040);

volatile union rw_lxlod_0041h xdata g_rw_lxlod_0041h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0041);
volatile union rw_lxlod_0042h xdata g_rw_lxlod_0042h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0042);
volatile union rw_lxlod_0043h xdata g_rw_lxlod_0043h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0043);
volatile union rw_lxlod_0044h xdata g_rw_lxlod_0044h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0044);
volatile union rw_lxlod_0045h xdata g_rw_lxlod_0045h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0045);

volatile union rw_lxlod_0046h xdata g_rw_lxlod_0046h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0046);
volatile union rw_lxlod_0047h xdata g_rw_lxlod_0047h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0047);
volatile union rw_lxlod_0048h xdata g_rw_lxlod_0048h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0048);
volatile union rw_lxlod_0049h xdata g_rw_lxlod_0049h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0049);
volatile union rw_lxlod_004Ah xdata g_rw_lxlod_004Ah  _at_  (LXLOD_FIELD_XMEM_BASE + 0x004A);

volatile union rw_lxlod_004Bh xdata g_rw_lxlod_004Bh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x004B);
volatile union rw_lxlod_004Ch xdata g_rw_lxlod_004Ch  _at_  (LXLOD_FIELD_XMEM_BASE + 0x004C);
volatile union rw_lxlod_004Eh xdata g_rw_lxlod_004Eh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x004E);
volatile union rw_lxlod_004Fh xdata g_rw_lxlod_004Fh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x004F);
volatile union rw_lxlod_0050h xdata g_rw_lxlod_0050h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0050);

volatile union rw_lxlod_0051h xdata g_rw_lxlod_0051h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0051);
volatile union rw_lxlod_0054h xdata g_rw_lxlod_0054h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0054);
volatile union rw_lxlod_0055h xdata g_rw_lxlod_0055h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0055);
volatile union rw_lxlod_0056h xdata g_rw_lxlod_0056h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0056);
volatile union rw_lxlod_0057h xdata g_rw_lxlod_0057h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0057);

volatile union rw_lxlod_0058h xdata g_rw_lxlod_0058h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0058);
volatile union rw_lxlod_0059h xdata g_rw_lxlod_0059h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0059);
volatile union rw_lxlod_005Ah xdata g_rw_lxlod_005Ah  _at_  (LXLOD_FIELD_XMEM_BASE + 0x005A);
volatile union rw_lxlod_005Bh xdata g_rw_lxlod_005Bh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x005B);
volatile union rw_lxlod_005Ch xdata g_rw_lxlod_005Ch  _at_  (LXLOD_FIELD_XMEM_BASE + 0x005C);

volatile union rw_lxlod_005Dh xdata g_rw_lxlod_005Dh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x005D);
volatile union rw_lxlod_005Eh xdata g_rw_lxlod_005Eh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x005E);
volatile union rw_lxlod_005Fh xdata g_rw_lxlod_005Fh  _at_  (LXLOD_FIELD_XMEM_BASE + 0x005F);
volatile union rw_lxlod_0060h xdata g_rw_lxlod_0060h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0060);
volatile union rw_lxlod_0061h xdata g_rw_lxlod_0061h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0061);

volatile union rw_lxlod_0062h xdata g_rw_lxlod_0062h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0062);
volatile union rw_lxlod_0063h xdata g_rw_lxlod_0063h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0063);
volatile union rw_lxlod_0065h xdata g_rw_lxlod_0065h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0065);
volatile union rw_lxlod_0067h xdata g_rw_lxlod_0067h  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0067);

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_lxlod_004Dh_h_block_size                                           _at_  (LXLOD_FIELD_XMEM_BASE + 0x004D);
volatile uint8_t xdata g_rw_lxlod_0052h_x_div                                                  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0052);
volatile uint8_t xdata g_rw_lxlod_0053h_y_div                                                  _at_  (LXLOD_FIELD_XMEM_BASE + 0x0053);
volatile uint8_t xdata g_rw_lxlod_0064h_div_cnt                                                _at_  (LXLOD_FIELD_XMEM_BASE + 0x0064);
volatile uint8_t xdata g_rw_lxlod_0066h_dbg_period                                             _at_  (LXLOD_FIELD_XMEM_BASE + 0x0066);


#endif 
