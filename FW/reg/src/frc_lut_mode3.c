#include "frc_lut_mode3.h"
#include "hw_mem_map.h"

#ifdef FRC_LUT_MODE3_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_frc_lut_mode3_0000h_FRC_LUT_1_data_0                               _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0000);
volatile uint8_t xdata g_rw_frc_lut_mode3_0001h_FRC_LUT_1_data_1                               _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0001);
volatile uint8_t xdata g_rw_frc_lut_mode3_0002h_FRC_LUT_1_data_2                               _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0002);
volatile uint8_t xdata g_rw_frc_lut_mode3_0003h_FRC_LUT_1_data_3                               _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0003);
volatile uint8_t xdata g_rw_frc_lut_mode3_0004h_FRC_LUT_1_data_4                               _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0004);

volatile uint8_t xdata g_rw_frc_lut_mode3_0005h_FRC_LUT_1_data_5                               _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0005);
volatile uint8_t xdata g_rw_frc_lut_mode3_0006h_FRC_LUT_1_data_6                               _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0006);
volatile uint8_t xdata g_rw_frc_lut_mode3_0007h_FRC_LUT_1_data_7                               _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0007);
volatile uint8_t xdata g_rw_frc_lut_mode3_0008h_FRC_LUT_1_data_8                               _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0008);
volatile uint8_t xdata g_rw_frc_lut_mode3_0009h_FRC_LUT_1_data_9                               _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0009);

volatile uint8_t xdata g_rw_frc_lut_mode3_000Ah_FRC_LUT_1_data_10                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x000A);
volatile uint8_t xdata g_rw_frc_lut_mode3_000Bh_FRC_LUT_1_data_11                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x000B);
volatile uint8_t xdata g_rw_frc_lut_mode3_000Ch_FRC_LUT_1_data_12                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x000C);
volatile uint8_t xdata g_rw_frc_lut_mode3_000Dh_FRC_LUT_1_data_13                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x000D);
volatile uint8_t xdata g_rw_frc_lut_mode3_000Eh_FRC_LUT_1_data_14                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x000E);

volatile uint8_t xdata g_rw_frc_lut_mode3_000Fh_FRC_LUT_1_data_15                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x000F);
volatile uint8_t xdata g_rw_frc_lut_mode3_0010h_FRC_LUT_1_data_16                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0010);
volatile uint8_t xdata g_rw_frc_lut_mode3_0011h_FRC_LUT_1_data_17                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0011);
volatile uint8_t xdata g_rw_frc_lut_mode3_0012h_FRC_LUT_1_data_18                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0012);
volatile uint8_t xdata g_rw_frc_lut_mode3_0013h_FRC_LUT_1_data_19                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0013);

volatile uint8_t xdata g_rw_frc_lut_mode3_0014h_FRC_LUT_1_data_20                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0014);
volatile uint8_t xdata g_rw_frc_lut_mode3_0015h_FRC_LUT_1_data_21                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0015);
volatile uint8_t xdata g_rw_frc_lut_mode3_0016h_FRC_LUT_1_data_22                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0016);
volatile uint8_t xdata g_rw_frc_lut_mode3_0017h_FRC_LUT_1_data_23                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0017);
volatile uint8_t xdata g_rw_frc_lut_mode3_0018h_FRC_LUT_1_data_24                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0018);

volatile uint8_t xdata g_rw_frc_lut_mode3_0019h_FRC_LUT_1_data_25                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0019);
volatile uint8_t xdata g_rw_frc_lut_mode3_001Ah_FRC_LUT_1_data_26                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x001A);
volatile uint8_t xdata g_rw_frc_lut_mode3_001Bh_FRC_LUT_1_data_27                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x001B);
volatile uint8_t xdata g_rw_frc_lut_mode3_001Ch_FRC_LUT_1_data_28                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x001C);
volatile uint8_t xdata g_rw_frc_lut_mode3_001Dh_FRC_LUT_1_data_29                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x001D);

volatile uint8_t xdata g_rw_frc_lut_mode3_001Eh_FRC_LUT_1_data_30                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x001E);
volatile uint8_t xdata g_rw_frc_lut_mode3_001Fh_FRC_LUT_1_data_31                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x001F);
volatile uint8_t xdata g_rw_frc_lut_mode3_0020h_FRC_LUT_1_data_32                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0020);
volatile uint8_t xdata g_rw_frc_lut_mode3_0021h_FRC_LUT_1_data_33                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0021);
volatile uint8_t xdata g_rw_frc_lut_mode3_0022h_FRC_LUT_1_data_34                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0022);

volatile uint8_t xdata g_rw_frc_lut_mode3_0023h_FRC_LUT_1_data_35                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0023);
volatile uint8_t xdata g_rw_frc_lut_mode3_0024h_FRC_LUT_1_data_36                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0024);
volatile uint8_t xdata g_rw_frc_lut_mode3_0025h_FRC_LUT_1_data_37                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0025);
volatile uint8_t xdata g_rw_frc_lut_mode3_0026h_FRC_LUT_1_data_38                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0026);
volatile uint8_t xdata g_rw_frc_lut_mode3_0027h_FRC_LUT_1_data_39                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0027);

volatile uint8_t xdata g_rw_frc_lut_mode3_0028h_FRC_LUT_1_data_40                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0028);
volatile uint8_t xdata g_rw_frc_lut_mode3_0029h_FRC_LUT_1_data_41                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0029);
volatile uint8_t xdata g_rw_frc_lut_mode3_002Ah_FRC_LUT_1_data_42                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x002A);
volatile uint8_t xdata g_rw_frc_lut_mode3_002Bh_FRC_LUT_1_data_43                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x002B);
volatile uint8_t xdata g_rw_frc_lut_mode3_002Ch_FRC_LUT_1_data_44                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x002C);

volatile uint8_t xdata g_rw_frc_lut_mode3_002Dh_FRC_LUT_1_data_45                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x002D);
volatile uint8_t xdata g_rw_frc_lut_mode3_002Eh_FRC_LUT_1_data_46                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x002E);
volatile uint8_t xdata g_rw_frc_lut_mode3_002Fh_FRC_LUT_1_data_47                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x002F);
volatile uint8_t xdata g_rw_frc_lut_mode3_0030h_FRC_LUT_1_data_48                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0030);
volatile uint8_t xdata g_rw_frc_lut_mode3_0031h_FRC_LUT_1_data_49                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0031);

volatile uint8_t xdata g_rw_frc_lut_mode3_0032h_FRC_LUT_1_data_50                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0032);
volatile uint8_t xdata g_rw_frc_lut_mode3_0033h_FRC_LUT_1_data_51                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0033);
volatile uint8_t xdata g_rw_frc_lut_mode3_0034h_FRC_LUT_1_data_52                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0034);
volatile uint8_t xdata g_rw_frc_lut_mode3_0035h_FRC_LUT_1_data_53                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0035);
volatile uint8_t xdata g_rw_frc_lut_mode3_0036h_FRC_LUT_1_data_54                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0036);

volatile uint8_t xdata g_rw_frc_lut_mode3_0037h_FRC_LUT_1_data_55                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0037);
volatile uint8_t xdata g_rw_frc_lut_mode3_0038h_FRC_LUT_1_data_56                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0038);
volatile uint8_t xdata g_rw_frc_lut_mode3_0039h_FRC_LUT_1_data_57                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0039);
volatile uint8_t xdata g_rw_frc_lut_mode3_003Ah_FRC_LUT_1_data_58                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x003A);
volatile uint8_t xdata g_rw_frc_lut_mode3_003Bh_FRC_LUT_1_data_59                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x003B);

volatile uint8_t xdata g_rw_frc_lut_mode3_003Ch_FRC_LUT_1_data_60                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x003C);
volatile uint8_t xdata g_rw_frc_lut_mode3_003Dh_FRC_LUT_1_data_61                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x003D);
volatile uint8_t xdata g_rw_frc_lut_mode3_003Eh_FRC_LUT_1_data_62                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x003E);
volatile uint8_t xdata g_rw_frc_lut_mode3_003Fh_FRC_LUT_1_data_63                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x003F);
volatile uint8_t xdata g_rw_frc_lut_mode3_0040h_FRC_LUT_1_data_64                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0040);

volatile uint8_t xdata g_rw_frc_lut_mode3_0041h_FRC_LUT_1_data_65                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0041);
volatile uint8_t xdata g_rw_frc_lut_mode3_0042h_FRC_LUT_1_data_66                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0042);
volatile uint8_t xdata g_rw_frc_lut_mode3_0043h_FRC_LUT_1_data_67                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0043);
volatile uint8_t xdata g_rw_frc_lut_mode3_0044h_FRC_LUT_1_data_68                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0044);
volatile uint8_t xdata g_rw_frc_lut_mode3_0045h_FRC_LUT_1_data_69                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0045);

volatile uint8_t xdata g_rw_frc_lut_mode3_0046h_FRC_LUT_1_data_70                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0046);
volatile uint8_t xdata g_rw_frc_lut_mode3_0047h_FRC_LUT_1_data_71                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0047);
volatile uint8_t xdata g_rw_frc_lut_mode3_0048h_FRC_LUT_1_data_72                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0048);
volatile uint8_t xdata g_rw_frc_lut_mode3_0049h_FRC_LUT_1_data_73                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0049);
volatile uint8_t xdata g_rw_frc_lut_mode3_004Ah_FRC_LUT_1_data_74                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x004A);

volatile uint8_t xdata g_rw_frc_lut_mode3_004Bh_FRC_LUT_1_data_75                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x004B);
volatile uint8_t xdata g_rw_frc_lut_mode3_004Ch_FRC_LUT_1_data_76                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x004C);
volatile uint8_t xdata g_rw_frc_lut_mode3_004Dh_FRC_LUT_1_data_77                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x004D);
volatile uint8_t xdata g_rw_frc_lut_mode3_004Eh_FRC_LUT_1_data_78                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x004E);
volatile uint8_t xdata g_rw_frc_lut_mode3_004Fh_FRC_LUT_1_data_79                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x004F);

volatile uint8_t xdata g_rw_frc_lut_mode3_0050h_FRC_LUT_1_data_80                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0050);
volatile uint8_t xdata g_rw_frc_lut_mode3_0051h_FRC_LUT_1_data_81                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0051);
volatile uint8_t xdata g_rw_frc_lut_mode3_0052h_FRC_LUT_1_data_82                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0052);
volatile uint8_t xdata g_rw_frc_lut_mode3_0053h_FRC_LUT_1_data_83                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0053);
volatile uint8_t xdata g_rw_frc_lut_mode3_0054h_FRC_LUT_1_data_84                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0054);

volatile uint8_t xdata g_rw_frc_lut_mode3_0055h_FRC_LUT_1_data_85                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0055);
volatile uint8_t xdata g_rw_frc_lut_mode3_0056h_FRC_LUT_1_data_86                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0056);
volatile uint8_t xdata g_rw_frc_lut_mode3_0057h_FRC_LUT_1_data_87                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0057);
volatile uint8_t xdata g_rw_frc_lut_mode3_0058h_FRC_LUT_1_data_88                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0058);
volatile uint8_t xdata g_rw_frc_lut_mode3_0059h_FRC_LUT_1_data_89                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0059);

volatile uint8_t xdata g_rw_frc_lut_mode3_005Ah_FRC_LUT_1_data_90                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x005A);
volatile uint8_t xdata g_rw_frc_lut_mode3_005Bh_FRC_LUT_1_data_91                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x005B);
volatile uint8_t xdata g_rw_frc_lut_mode3_005Ch_FRC_LUT_1_data_92                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x005C);
volatile uint8_t xdata g_rw_frc_lut_mode3_005Dh_FRC_LUT_1_data_93                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x005D);
volatile uint8_t xdata g_rw_frc_lut_mode3_005Eh_FRC_LUT_1_data_94                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x005E);

volatile uint8_t xdata g_rw_frc_lut_mode3_005Fh_FRC_LUT_1_data_95                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x005F);
volatile uint8_t xdata g_rw_frc_lut_mode3_0060h_FRC_LUT_1_data_96                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0060);
volatile uint8_t xdata g_rw_frc_lut_mode3_0061h_FRC_LUT_1_data_97                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0061);
volatile uint8_t xdata g_rw_frc_lut_mode3_0062h_FRC_LUT_1_data_98                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0062);
volatile uint8_t xdata g_rw_frc_lut_mode3_0063h_FRC_LUT_1_data_99                              _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0063);

volatile uint8_t xdata g_rw_frc_lut_mode3_0064h_FRC_LUT_1_data_100                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0064);
volatile uint8_t xdata g_rw_frc_lut_mode3_0065h_FRC_LUT_1_data_101                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0065);
volatile uint8_t xdata g_rw_frc_lut_mode3_0066h_FRC_LUT_1_data_102                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0066);
volatile uint8_t xdata g_rw_frc_lut_mode3_0067h_FRC_LUT_1_data_103                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0067);
volatile uint8_t xdata g_rw_frc_lut_mode3_0068h_FRC_LUT_1_data_104                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0068);

volatile uint8_t xdata g_rw_frc_lut_mode3_0069h_FRC_LUT_1_data_105                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0069);
volatile uint8_t xdata g_rw_frc_lut_mode3_006Ah_FRC_LUT_1_data_106                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x006A);
volatile uint8_t xdata g_rw_frc_lut_mode3_006Bh_FRC_LUT_1_data_107                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x006B);
volatile uint8_t xdata g_rw_frc_lut_mode3_006Ch_FRC_LUT_1_data_108                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x006C);
volatile uint8_t xdata g_rw_frc_lut_mode3_006Dh_FRC_LUT_1_data_109                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x006D);

volatile uint8_t xdata g_rw_frc_lut_mode3_006Eh_FRC_LUT_1_data_110                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x006E);
volatile uint8_t xdata g_rw_frc_lut_mode3_006Fh_FRC_LUT_1_data_111                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x006F);
volatile uint8_t xdata g_rw_frc_lut_mode3_0070h_FRC_LUT_1_data_112                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0070);
volatile uint8_t xdata g_rw_frc_lut_mode3_0071h_FRC_LUT_1_data_113                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0071);
volatile uint8_t xdata g_rw_frc_lut_mode3_0072h_FRC_LUT_1_data_114                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0072);

volatile uint8_t xdata g_rw_frc_lut_mode3_0073h_FRC_LUT_1_data_115                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0073);
volatile uint8_t xdata g_rw_frc_lut_mode3_0074h_FRC_LUT_1_data_116                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0074);
volatile uint8_t xdata g_rw_frc_lut_mode3_0075h_FRC_LUT_1_data_117                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0075);
volatile uint8_t xdata g_rw_frc_lut_mode3_0076h_FRC_LUT_1_data_118                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0076);
volatile uint8_t xdata g_rw_frc_lut_mode3_0077h_FRC_LUT_1_data_119                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0077);

volatile uint8_t xdata g_rw_frc_lut_mode3_0078h_FRC_LUT_1_data_120                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0078);
volatile uint8_t xdata g_rw_frc_lut_mode3_0079h_FRC_LUT_1_data_121                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0079);
volatile uint8_t xdata g_rw_frc_lut_mode3_007Ah_FRC_LUT_1_data_122                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x007A);
volatile uint8_t xdata g_rw_frc_lut_mode3_007Bh_FRC_LUT_1_data_123                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x007B);
volatile uint8_t xdata g_rw_frc_lut_mode3_007Ch_FRC_LUT_1_data_124                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x007C);

volatile uint8_t xdata g_rw_frc_lut_mode3_007Dh_FRC_LUT_1_data_125                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x007D);
volatile uint8_t xdata g_rw_frc_lut_mode3_007Eh_FRC_LUT_1_data_126                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x007E);
volatile uint8_t xdata g_rw_frc_lut_mode3_007Fh_FRC_LUT_1_data_127                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x007F);
volatile uint8_t xdata g_rw_frc_lut_mode3_0080h_FRC_LUT_1_data_128                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0080);
volatile uint8_t xdata g_rw_frc_lut_mode3_0081h_FRC_LUT_1_data_129                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0081);

volatile uint8_t xdata g_rw_frc_lut_mode3_0082h_FRC_LUT_1_data_130                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0082);
volatile uint8_t xdata g_rw_frc_lut_mode3_0083h_FRC_LUT_1_data_131                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0083);
volatile uint8_t xdata g_rw_frc_lut_mode3_0084h_FRC_LUT_1_data_132                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0084);
volatile uint8_t xdata g_rw_frc_lut_mode3_0085h_FRC_LUT_1_data_133                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0085);
volatile uint8_t xdata g_rw_frc_lut_mode3_0086h_FRC_LUT_1_data_134                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0086);

volatile uint8_t xdata g_rw_frc_lut_mode3_0087h_FRC_LUT_1_data_135                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0087);
volatile uint8_t xdata g_rw_frc_lut_mode3_0088h_FRC_LUT_1_data_136                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0088);
volatile uint8_t xdata g_rw_frc_lut_mode3_0089h_FRC_LUT_1_data_137                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0089);
volatile uint8_t xdata g_rw_frc_lut_mode3_008Ah_FRC_LUT_1_data_138                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x008A);
volatile uint8_t xdata g_rw_frc_lut_mode3_008Bh_FRC_LUT_1_data_139                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x008B);

volatile uint8_t xdata g_rw_frc_lut_mode3_008Ch_FRC_LUT_1_data_140                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x008C);
volatile uint8_t xdata g_rw_frc_lut_mode3_008Dh_FRC_LUT_1_data_141                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x008D);
volatile uint8_t xdata g_rw_frc_lut_mode3_008Eh_FRC_LUT_1_data_142                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x008E);
volatile uint8_t xdata g_rw_frc_lut_mode3_008Fh_FRC_LUT_1_data_143                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x008F);
volatile uint8_t xdata g_rw_frc_lut_mode3_0090h_FRC_LUT_1_data_144                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0090);

volatile uint8_t xdata g_rw_frc_lut_mode3_0091h_FRC_LUT_1_data_145                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0091);
volatile uint8_t xdata g_rw_frc_lut_mode3_0092h_FRC_LUT_1_data_146                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0092);
volatile uint8_t xdata g_rw_frc_lut_mode3_0093h_FRC_LUT_1_data_147                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0093);
volatile uint8_t xdata g_rw_frc_lut_mode3_0094h_FRC_LUT_1_data_148                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0094);
volatile uint8_t xdata g_rw_frc_lut_mode3_0095h_FRC_LUT_1_data_149                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0095);

volatile uint8_t xdata g_rw_frc_lut_mode3_0096h_FRC_LUT_1_data_150                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0096);
volatile uint8_t xdata g_rw_frc_lut_mode3_0097h_FRC_LUT_1_data_151                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0097);
volatile uint8_t xdata g_rw_frc_lut_mode3_0098h_FRC_LUT_1_data_152                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0098);
volatile uint8_t xdata g_rw_frc_lut_mode3_0099h_FRC_LUT_1_data_153                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0099);
volatile uint8_t xdata g_rw_frc_lut_mode3_009Ah_FRC_LUT_1_data_154                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x009A);

volatile uint8_t xdata g_rw_frc_lut_mode3_009Bh_FRC_LUT_1_data_155                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x009B);
volatile uint8_t xdata g_rw_frc_lut_mode3_009Ch_FRC_LUT_1_data_156                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x009C);
volatile uint8_t xdata g_rw_frc_lut_mode3_009Dh_FRC_LUT_1_data_157                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x009D);
volatile uint8_t xdata g_rw_frc_lut_mode3_009Eh_FRC_LUT_1_data_158                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x009E);
volatile uint8_t xdata g_rw_frc_lut_mode3_009Fh_FRC_LUT_1_data_159                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x009F);

volatile uint8_t xdata g_rw_frc_lut_mode3_00A0h_FRC_LUT_1_data_160                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00A0);
volatile uint8_t xdata g_rw_frc_lut_mode3_00A1h_FRC_LUT_1_data_161                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00A1);
volatile uint8_t xdata g_rw_frc_lut_mode3_00A2h_FRC_LUT_1_data_162                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00A2);
volatile uint8_t xdata g_rw_frc_lut_mode3_00A3h_FRC_LUT_1_data_163                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00A3);
volatile uint8_t xdata g_rw_frc_lut_mode3_00A4h_FRC_LUT_1_data_164                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00A4);

volatile uint8_t xdata g_rw_frc_lut_mode3_00A5h_FRC_LUT_1_data_165                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00A5);
volatile uint8_t xdata g_rw_frc_lut_mode3_00A6h_FRC_LUT_1_data_166                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00A6);
volatile uint8_t xdata g_rw_frc_lut_mode3_00A7h_FRC_LUT_1_data_167                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00A7);
volatile uint8_t xdata g_rw_frc_lut_mode3_00A8h_FRC_LUT_1_data_168                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00A8);
volatile uint8_t xdata g_rw_frc_lut_mode3_00A9h_FRC_LUT_1_data_169                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00A9);

volatile uint8_t xdata g_rw_frc_lut_mode3_00AAh_FRC_LUT_1_data_170                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00AA);
volatile uint8_t xdata g_rw_frc_lut_mode3_00ABh_FRC_LUT_1_data_171                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00AB);
volatile uint8_t xdata g_rw_frc_lut_mode3_00ACh_FRC_LUT_1_data_172                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00AC);
volatile uint8_t xdata g_rw_frc_lut_mode3_00ADh_FRC_LUT_1_data_173                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00AD);
volatile uint8_t xdata g_rw_frc_lut_mode3_00AEh_FRC_LUT_1_data_174                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00AE);

volatile uint8_t xdata g_rw_frc_lut_mode3_00AFh_FRC_LUT_1_data_175                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00AF);
volatile uint8_t xdata g_rw_frc_lut_mode3_00B0h_FRC_LUT_1_data_176                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00B0);
volatile uint8_t xdata g_rw_frc_lut_mode3_00B1h_FRC_LUT_1_data_177                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00B1);
volatile uint8_t xdata g_rw_frc_lut_mode3_00B2h_FRC_LUT_1_data_178                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00B2);
volatile uint8_t xdata g_rw_frc_lut_mode3_00B3h_FRC_LUT_1_data_179                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00B3);

volatile uint8_t xdata g_rw_frc_lut_mode3_00B4h_FRC_LUT_1_data_180                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00B4);
volatile uint8_t xdata g_rw_frc_lut_mode3_00B5h_FRC_LUT_1_data_181                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00B5);
volatile uint8_t xdata g_rw_frc_lut_mode3_00B6h_FRC_LUT_1_data_182                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00B6);
volatile uint8_t xdata g_rw_frc_lut_mode3_00B7h_FRC_LUT_1_data_183                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00B7);
volatile uint8_t xdata g_rw_frc_lut_mode3_00B8h_FRC_LUT_1_data_184                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00B8);

volatile uint8_t xdata g_rw_frc_lut_mode3_00B9h_FRC_LUT_1_data_185                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00B9);
volatile uint8_t xdata g_rw_frc_lut_mode3_00BAh_FRC_LUT_1_data_186                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00BA);
volatile uint8_t xdata g_rw_frc_lut_mode3_00BBh_FRC_LUT_1_data_187                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00BB);
volatile uint8_t xdata g_rw_frc_lut_mode3_00BCh_FRC_LUT_1_data_188                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00BC);
volatile uint8_t xdata g_rw_frc_lut_mode3_00BDh_FRC_LUT_1_data_189                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00BD);

volatile uint8_t xdata g_rw_frc_lut_mode3_00BEh_FRC_LUT_1_data_190                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00BE);
volatile uint8_t xdata g_rw_frc_lut_mode3_00BFh_FRC_LUT_1_data_191                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00BF);
volatile uint8_t xdata g_rw_frc_lut_mode3_00C0h_FRC_LUT_1_data_192                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00C0);
volatile uint8_t xdata g_rw_frc_lut_mode3_00C1h_FRC_LUT_1_data_193                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00C1);
volatile uint8_t xdata g_rw_frc_lut_mode3_00C2h_FRC_LUT_1_data_194                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00C2);

volatile uint8_t xdata g_rw_frc_lut_mode3_00C3h_FRC_LUT_1_data_195                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00C3);
volatile uint8_t xdata g_rw_frc_lut_mode3_00C4h_FRC_LUT_1_data_196                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00C4);
volatile uint8_t xdata g_rw_frc_lut_mode3_00C5h_FRC_LUT_1_data_197                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00C5);
volatile uint8_t xdata g_rw_frc_lut_mode3_00C6h_FRC_LUT_1_data_198                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00C6);
volatile uint8_t xdata g_rw_frc_lut_mode3_00C7h_FRC_LUT_1_data_199                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00C7);

volatile uint8_t xdata g_rw_frc_lut_mode3_00C8h_FRC_LUT_1_data_200                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00C8);
volatile uint8_t xdata g_rw_frc_lut_mode3_00C9h_FRC_LUT_1_data_201                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00C9);
volatile uint8_t xdata g_rw_frc_lut_mode3_00CAh_FRC_LUT_1_data_202                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00CA);
volatile uint8_t xdata g_rw_frc_lut_mode3_00CBh_FRC_LUT_1_data_203                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00CB);
volatile uint8_t xdata g_rw_frc_lut_mode3_00CCh_FRC_LUT_1_data_204                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00CC);

volatile uint8_t xdata g_rw_frc_lut_mode3_00CDh_FRC_LUT_1_data_205                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00CD);
volatile uint8_t xdata g_rw_frc_lut_mode3_00CEh_FRC_LUT_1_data_206                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00CE);
volatile uint8_t xdata g_rw_frc_lut_mode3_00CFh_FRC_LUT_1_data_207                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00CF);
volatile uint8_t xdata g_rw_frc_lut_mode3_00D0h_FRC_LUT_1_data_208                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00D0);
volatile uint8_t xdata g_rw_frc_lut_mode3_00D1h_FRC_LUT_1_data_209                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00D1);

volatile uint8_t xdata g_rw_frc_lut_mode3_00D2h_FRC_LUT_1_data_210                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00D2);
volatile uint8_t xdata g_rw_frc_lut_mode3_00D3h_FRC_LUT_1_data_211                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00D3);
volatile uint8_t xdata g_rw_frc_lut_mode3_00D4h_FRC_LUT_1_data_212                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00D4);
volatile uint8_t xdata g_rw_frc_lut_mode3_00D5h_FRC_LUT_1_data_213                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00D5);
volatile uint8_t xdata g_rw_frc_lut_mode3_00D6h_FRC_LUT_1_data_214                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00D6);

volatile uint8_t xdata g_rw_frc_lut_mode3_00D7h_FRC_LUT_1_data_215                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00D7);
volatile uint8_t xdata g_rw_frc_lut_mode3_00D8h_FRC_LUT_1_data_216                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00D8);
volatile uint8_t xdata g_rw_frc_lut_mode3_00D9h_FRC_LUT_1_data_217                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00D9);
volatile uint8_t xdata g_rw_frc_lut_mode3_00DAh_FRC_LUT_1_data_218                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00DA);
volatile uint8_t xdata g_rw_frc_lut_mode3_00DBh_FRC_LUT_1_data_219                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00DB);

volatile uint8_t xdata g_rw_frc_lut_mode3_00DCh_FRC_LUT_1_data_220                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00DC);
volatile uint8_t xdata g_rw_frc_lut_mode3_00DDh_FRC_LUT_1_data_221                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00DD);
volatile uint8_t xdata g_rw_frc_lut_mode3_00DEh_FRC_LUT_1_data_222                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00DE);
volatile uint8_t xdata g_rw_frc_lut_mode3_00DFh_FRC_LUT_1_data_223                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00DF);
volatile uint8_t xdata g_rw_frc_lut_mode3_00E0h_FRC_LUT_1_data_224                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00E0);

volatile uint8_t xdata g_rw_frc_lut_mode3_00E1h_FRC_LUT_1_data_225                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00E1);
volatile uint8_t xdata g_rw_frc_lut_mode3_00E2h_FRC_LUT_1_data_226                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00E2);
volatile uint8_t xdata g_rw_frc_lut_mode3_00E3h_FRC_LUT_1_data_227                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00E3);
volatile uint8_t xdata g_rw_frc_lut_mode3_00E4h_FRC_LUT_1_data_228                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00E4);
volatile uint8_t xdata g_rw_frc_lut_mode3_00E5h_FRC_LUT_1_data_229                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00E5);

volatile uint8_t xdata g_rw_frc_lut_mode3_00E6h_FRC_LUT_1_data_230                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00E6);
volatile uint8_t xdata g_rw_frc_lut_mode3_00E7h_FRC_LUT_1_data_231                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00E7);
volatile uint8_t xdata g_rw_frc_lut_mode3_00E8h_FRC_LUT_1_data_232                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00E8);
volatile uint8_t xdata g_rw_frc_lut_mode3_00E9h_FRC_LUT_1_data_233                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00E9);
volatile uint8_t xdata g_rw_frc_lut_mode3_00EAh_FRC_LUT_1_data_234                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00EA);

volatile uint8_t xdata g_rw_frc_lut_mode3_00EBh_FRC_LUT_1_data_235                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00EB);
volatile uint8_t xdata g_rw_frc_lut_mode3_00ECh_FRC_LUT_1_data_236                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00EC);
volatile uint8_t xdata g_rw_frc_lut_mode3_00EDh_FRC_LUT_1_data_237                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00ED);
volatile uint8_t xdata g_rw_frc_lut_mode3_00EEh_FRC_LUT_1_data_238                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00EE);
volatile uint8_t xdata g_rw_frc_lut_mode3_00EFh_FRC_LUT_1_data_239                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00EF);

volatile uint8_t xdata g_rw_frc_lut_mode3_00F0h_FRC_LUT_1_data_240                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00F0);
volatile uint8_t xdata g_rw_frc_lut_mode3_00F1h_FRC_LUT_1_data_241                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00F1);
volatile uint8_t xdata g_rw_frc_lut_mode3_00F2h_FRC_LUT_1_data_242                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00F2);
volatile uint8_t xdata g_rw_frc_lut_mode3_00F3h_FRC_LUT_1_data_243                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00F3);
volatile uint8_t xdata g_rw_frc_lut_mode3_00F4h_FRC_LUT_1_data_244                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00F4);

volatile uint8_t xdata g_rw_frc_lut_mode3_00F5h_FRC_LUT_1_data_245                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00F5);
volatile uint8_t xdata g_rw_frc_lut_mode3_00F6h_FRC_LUT_1_data_246                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00F6);
volatile uint8_t xdata g_rw_frc_lut_mode3_00F7h_FRC_LUT_1_data_247                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00F7);
volatile uint8_t xdata g_rw_frc_lut_mode3_00F8h_FRC_LUT_1_data_248                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00F8);
volatile uint8_t xdata g_rw_frc_lut_mode3_00F9h_FRC_LUT_1_data_249                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00F9);

volatile uint8_t xdata g_rw_frc_lut_mode3_00FAh_FRC_LUT_1_data_250                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00FA);
volatile uint8_t xdata g_rw_frc_lut_mode3_00FBh_FRC_LUT_1_data_251                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00FB);
volatile uint8_t xdata g_rw_frc_lut_mode3_00FCh_FRC_LUT_1_data_252                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00FC);
volatile uint8_t xdata g_rw_frc_lut_mode3_00FDh_FRC_LUT_1_data_253                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00FD);
volatile uint8_t xdata g_rw_frc_lut_mode3_00FEh_FRC_LUT_1_data_254                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00FE);

volatile uint8_t xdata g_rw_frc_lut_mode3_00FFh_FRC_LUT_1_data_255                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x00FF);
//volatile uint8_t xdata g_rw_frc_lut_mode3_0100h_FRC_LUT_1_data_256                             _at_  (FRC_LUT_MODE3_FIELD_XMEM_BASE + 0x0100);

#endif 
