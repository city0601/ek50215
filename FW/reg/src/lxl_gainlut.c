#include "lxl_gainlut.h"
#include "hw_mem_map.h"

#ifdef LXL_GAINLUT_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_lxl_gainlut_0000h_LXL_LUT1_data_0                                  _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0000);
volatile uint8_t xdata g_rw_lxl_gainlut_0001h_LXL_LUT1_data_1                                  _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0001);
volatile uint8_t xdata g_rw_lxl_gainlut_0002h_LXL_LUT1_data_2                                  _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0002);
volatile uint8_t xdata g_rw_lxl_gainlut_0003h_LXL_LUT1_data_3                                  _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0003);
volatile uint8_t xdata g_rw_lxl_gainlut_0004h_LXL_LUT1_data_4                                  _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0004);

volatile uint8_t xdata g_rw_lxl_gainlut_0005h_LXL_LUT1_data_5                                  _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0005);
volatile uint8_t xdata g_rw_lxl_gainlut_0006h_LXL_LUT1_data_6                                  _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0006);
volatile uint8_t xdata g_rw_lxl_gainlut_0007h_LXL_LUT1_data_7                                  _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0007);
volatile uint8_t xdata g_rw_lxl_gainlut_0008h_LXL_LUT1_data_8                                  _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0008);
volatile uint8_t xdata g_rw_lxl_gainlut_0009h_LXL_LUT1_data_9                                  _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0009);

volatile uint8_t xdata g_rw_lxl_gainlut_000Ah_LXL_LUT1_data_10                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x000A);
volatile uint8_t xdata g_rw_lxl_gainlut_000Bh_LXL_LUT1_data_11                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x000B);
volatile uint8_t xdata g_rw_lxl_gainlut_000Ch_LXL_LUT1_data_12                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x000C);
volatile uint8_t xdata g_rw_lxl_gainlut_000Dh_LXL_LUT1_data_13                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x000D);
volatile uint8_t xdata g_rw_lxl_gainlut_000Eh_LXL_LUT1_data_14                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x000E);

volatile uint8_t xdata g_rw_lxl_gainlut_000Fh_LXL_LUT1_data_15                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x000F);
volatile uint8_t xdata g_rw_lxl_gainlut_0010h_LXL_LUT1_data_16                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0010);
volatile uint8_t xdata g_rw_lxl_gainlut_0011h_LXL_LUT1_data_17                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0011);
volatile uint8_t xdata g_rw_lxl_gainlut_0012h_LXL_LUT1_data_18                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0012);
volatile uint8_t xdata g_rw_lxl_gainlut_0013h_LXL_LUT1_data_19                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0013);

volatile uint8_t xdata g_rw_lxl_gainlut_0014h_LXL_LUT1_data_20                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0014);
volatile uint8_t xdata g_rw_lxl_gainlut_0015h_LXL_LUT1_data_21                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0015);
volatile uint8_t xdata g_rw_lxl_gainlut_0016h_LXL_LUT1_data_22                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0016);
volatile uint8_t xdata g_rw_lxl_gainlut_0017h_LXL_LUT1_data_23                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0017);
volatile uint8_t xdata g_rw_lxl_gainlut_0018h_LXL_LUT1_data_24                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0018);

volatile uint8_t xdata g_rw_lxl_gainlut_0019h_LXL_LUT1_data_25                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0019);
volatile uint8_t xdata g_rw_lxl_gainlut_001Ah_LXL_LUT1_data_26                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x001A);
volatile uint8_t xdata g_rw_lxl_gainlut_001Bh_LXL_LUT1_data_27                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x001B);
volatile uint8_t xdata g_rw_lxl_gainlut_001Ch_LXL_LUT1_data_28                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x001C);
volatile uint8_t xdata g_rw_lxl_gainlut_001Dh_LXL_LUT1_data_29                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x001D);

volatile uint8_t xdata g_rw_lxl_gainlut_001Eh_LXL_LUT1_data_30                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x001E);
volatile uint8_t xdata g_rw_lxl_gainlut_001Fh_LXL_LUT1_data_31                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x001F);
volatile uint8_t xdata g_rw_lxl_gainlut_0020h_LXL_LUT1_data_32                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0020);
volatile uint8_t xdata g_rw_lxl_gainlut_0021h_LXL_LUT1_data_33                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0021);
volatile uint8_t xdata g_rw_lxl_gainlut_0022h_LXL_LUT1_data_34                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0022);

volatile uint8_t xdata g_rw_lxl_gainlut_0023h_LXL_LUT1_data_35                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0023);
volatile uint8_t xdata g_rw_lxl_gainlut_0024h_LXL_LUT1_data_36                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0024);
volatile uint8_t xdata g_rw_lxl_gainlut_0025h_LXL_LUT1_data_37                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0025);
volatile uint8_t xdata g_rw_lxl_gainlut_0026h_LXL_LUT1_data_38                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0026);
volatile uint8_t xdata g_rw_lxl_gainlut_0027h_LXL_LUT1_data_39                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0027);

volatile uint8_t xdata g_rw_lxl_gainlut_0028h_LXL_LUT1_data_40                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0028);
volatile uint8_t xdata g_rw_lxl_gainlut_0029h_LXL_LUT1_data_41                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0029);
volatile uint8_t xdata g_rw_lxl_gainlut_002Ah_LXL_LUT1_data_42                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x002A);
volatile uint8_t xdata g_rw_lxl_gainlut_002Bh_LXL_LUT1_data_43                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x002B);
volatile uint8_t xdata g_rw_lxl_gainlut_002Ch_LXL_LUT1_data_44                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x002C);

volatile uint8_t xdata g_rw_lxl_gainlut_002Dh_LXL_LUT1_data_45                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x002D);
volatile uint8_t xdata g_rw_lxl_gainlut_002Eh_LXL_LUT1_data_46                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x002E);
volatile uint8_t xdata g_rw_lxl_gainlut_002Fh_LXL_LUT1_data_47                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x002F);
volatile uint8_t xdata g_rw_lxl_gainlut_0030h_LXL_LUT1_data_48                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0030);
volatile uint8_t xdata g_rw_lxl_gainlut_0031h_LXL_LUT1_data_49                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0031);

volatile uint8_t xdata g_rw_lxl_gainlut_0032h_LXL_LUT1_data_50                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0032);
volatile uint8_t xdata g_rw_lxl_gainlut_0033h_LXL_LUT1_data_51                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0033);
volatile uint8_t xdata g_rw_lxl_gainlut_0034h_LXL_LUT1_data_52                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0034);
volatile uint8_t xdata g_rw_lxl_gainlut_0035h_LXL_LUT1_data_53                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0035);
volatile uint8_t xdata g_rw_lxl_gainlut_0036h_LXL_LUT1_data_54                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0036);

volatile uint8_t xdata g_rw_lxl_gainlut_0037h_LXL_LUT1_data_55                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0037);
volatile uint8_t xdata g_rw_lxl_gainlut_0038h_LXL_LUT1_data_56                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0038);
volatile uint8_t xdata g_rw_lxl_gainlut_0039h_LXL_LUT1_data_57                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0039);
volatile uint8_t xdata g_rw_lxl_gainlut_003Ah_LXL_LUT1_data_58                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x003A);
volatile uint8_t xdata g_rw_lxl_gainlut_003Bh_LXL_LUT1_data_59                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x003B);

volatile uint8_t xdata g_rw_lxl_gainlut_003Ch_LXL_LUT1_data_60                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x003C);
volatile uint8_t xdata g_rw_lxl_gainlut_003Dh_LXL_LUT1_data_61                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x003D);
volatile uint8_t xdata g_rw_lxl_gainlut_003Eh_LXL_LUT1_data_62                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x003E);
volatile uint8_t xdata g_rw_lxl_gainlut_003Fh_LXL_LUT1_data_63                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x003F);
volatile uint8_t xdata g_rw_lxl_gainlut_0040h_LXL_LUT1_data_64                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0040);

volatile uint8_t xdata g_rw_lxl_gainlut_0041h_LXL_LUT1_data_65                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0041);
volatile uint8_t xdata g_rw_lxl_gainlut_0042h_LXL_LUT1_data_66                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0042);
volatile uint8_t xdata g_rw_lxl_gainlut_0043h_LXL_LUT1_data_67                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0043);
volatile uint8_t xdata g_rw_lxl_gainlut_0044h_LXL_LUT1_data_68                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0044);
volatile uint8_t xdata g_rw_lxl_gainlut_0045h_LXL_LUT1_data_69                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0045);

volatile uint8_t xdata g_rw_lxl_gainlut_0046h_LXL_LUT1_data_70                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0046);
volatile uint8_t xdata g_rw_lxl_gainlut_0047h_LXL_LUT1_data_71                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0047);
volatile uint8_t xdata g_rw_lxl_gainlut_0048h_LXL_LUT1_data_72                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0048);
volatile uint8_t xdata g_rw_lxl_gainlut_0049h_LXL_LUT1_data_73                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0049);
volatile uint8_t xdata g_rw_lxl_gainlut_004Ah_LXL_LUT1_data_74                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x004A);

volatile uint8_t xdata g_rw_lxl_gainlut_004Bh_LXL_LUT1_data_75                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x004B);
volatile uint8_t xdata g_rw_lxl_gainlut_004Ch_LXL_LUT1_data_76                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x004C);
volatile uint8_t xdata g_rw_lxl_gainlut_004Dh_LXL_LUT1_data_77                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x004D);
volatile uint8_t xdata g_rw_lxl_gainlut_004Eh_LXL_LUT1_data_78                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x004E);
volatile uint8_t xdata g_rw_lxl_gainlut_004Fh_LXL_LUT1_data_79                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x004F);

volatile uint8_t xdata g_rw_lxl_gainlut_0050h_LXL_LUT1_data_80                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0050);
volatile uint8_t xdata g_rw_lxl_gainlut_0051h_LXL_LUT1_data_81                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0051);
volatile uint8_t xdata g_rw_lxl_gainlut_0052h_LXL_LUT1_data_82                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0052);
volatile uint8_t xdata g_rw_lxl_gainlut_0053h_LXL_LUT1_data_83                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0053);
volatile uint8_t xdata g_rw_lxl_gainlut_0054h_LXL_LUT1_data_84                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0054);

volatile uint8_t xdata g_rw_lxl_gainlut_0055h_LXL_LUT1_data_85                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0055);
volatile uint8_t xdata g_rw_lxl_gainlut_0056h_LXL_LUT1_data_86                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0056);
volatile uint8_t xdata g_rw_lxl_gainlut_0057h_LXL_LUT1_data_87                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0057);
volatile uint8_t xdata g_rw_lxl_gainlut_0058h_LXL_LUT1_data_88                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0058);
volatile uint8_t xdata g_rw_lxl_gainlut_0059h_LXL_LUT1_data_89                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0059);

volatile uint8_t xdata g_rw_lxl_gainlut_005Ah_LXL_LUT1_data_90                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x005A);
volatile uint8_t xdata g_rw_lxl_gainlut_005Bh_LXL_LUT1_data_91                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x005B);
volatile uint8_t xdata g_rw_lxl_gainlut_005Ch_LXL_LUT1_data_92                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x005C);
volatile uint8_t xdata g_rw_lxl_gainlut_005Dh_LXL_LUT1_data_93                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x005D);
volatile uint8_t xdata g_rw_lxl_gainlut_005Eh_LXL_LUT1_data_94                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x005E);

volatile uint8_t xdata g_rw_lxl_gainlut_005Fh_LXL_LUT1_data_95                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x005F);
volatile uint8_t xdata g_rw_lxl_gainlut_0060h_LXL_LUT1_data_96                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0060);
volatile uint8_t xdata g_rw_lxl_gainlut_0061h_LXL_LUT1_data_97                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0061);
volatile uint8_t xdata g_rw_lxl_gainlut_0062h_LXL_LUT1_data_98                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0062);
volatile uint8_t xdata g_rw_lxl_gainlut_0063h_LXL_LUT1_data_99                                 _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0063);

volatile uint8_t xdata g_rw_lxl_gainlut_0064h_LXL_LUT1_data_100                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0064);
volatile uint8_t xdata g_rw_lxl_gainlut_0065h_LXL_LUT1_data_101                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0065);
volatile uint8_t xdata g_rw_lxl_gainlut_0066h_LXL_LUT1_data_102                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0066);
volatile uint8_t xdata g_rw_lxl_gainlut_0067h_LXL_LUT1_data_103                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0067);
volatile uint8_t xdata g_rw_lxl_gainlut_0068h_LXL_LUT1_data_104                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0068);

volatile uint8_t xdata g_rw_lxl_gainlut_0069h_LXL_LUT1_data_105                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0069);
volatile uint8_t xdata g_rw_lxl_gainlut_006Ah_LXL_LUT1_data_106                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x006A);
volatile uint8_t xdata g_rw_lxl_gainlut_006Bh_LXL_LUT1_data_107                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x006B);
volatile uint8_t xdata g_rw_lxl_gainlut_006Ch_LXL_LUT1_data_108                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x006C);
volatile uint8_t xdata g_rw_lxl_gainlut_006Dh_LXL_LUT1_data_109                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x006D);

volatile uint8_t xdata g_rw_lxl_gainlut_006Eh_LXL_LUT1_data_110                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x006E);
volatile uint8_t xdata g_rw_lxl_gainlut_006Fh_LXL_LUT1_data_111                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x006F);
volatile uint8_t xdata g_rw_lxl_gainlut_0070h_LXL_LUT1_data_112                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0070);
volatile uint8_t xdata g_rw_lxl_gainlut_0071h_LXL_LUT1_data_113                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0071);
volatile uint8_t xdata g_rw_lxl_gainlut_0072h_LXL_LUT1_data_114                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0072);

volatile uint8_t xdata g_rw_lxl_gainlut_0073h_LXL_LUT1_data_115                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0073);
volatile uint8_t xdata g_rw_lxl_gainlut_0074h_LXL_LUT1_data_116                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0074);
volatile uint8_t xdata g_rw_lxl_gainlut_0075h_LXL_LUT1_data_117                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0075);
volatile uint8_t xdata g_rw_lxl_gainlut_0076h_LXL_LUT1_data_118                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0076);
volatile uint8_t xdata g_rw_lxl_gainlut_0077h_LXL_LUT1_data_119                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0077);

volatile uint8_t xdata g_rw_lxl_gainlut_0078h_LXL_LUT1_data_120                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0078);
volatile uint8_t xdata g_rw_lxl_gainlut_0079h_LXL_LUT1_data_121                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0079);
volatile uint8_t xdata g_rw_lxl_gainlut_007Ah_LXL_LUT1_data_122                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x007A);
volatile uint8_t xdata g_rw_lxl_gainlut_007Bh_LXL_LUT1_data_123                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x007B);
volatile uint8_t xdata g_rw_lxl_gainlut_007Ch_LXL_LUT1_data_124                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x007C);

volatile uint8_t xdata g_rw_lxl_gainlut_007Dh_LXL_LUT1_data_125                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x007D);
volatile uint8_t xdata g_rw_lxl_gainlut_007Eh_LXL_LUT1_data_126                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x007E);
volatile uint8_t xdata g_rw_lxl_gainlut_007Fh_LXL_LUT1_data_127                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x007F);
volatile uint8_t xdata g_rw_lxl_gainlut_0080h_LXL_LUT1_data_128                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0080);
volatile uint8_t xdata g_rw_lxl_gainlut_0081h_LXL_LUT1_data_129                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0081);

volatile uint8_t xdata g_rw_lxl_gainlut_0082h_LXL_LUT1_data_130                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0082);
volatile uint8_t xdata g_rw_lxl_gainlut_0083h_LXL_LUT1_data_131                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0083);
volatile uint8_t xdata g_rw_lxl_gainlut_0084h_LXL_LUT1_data_132                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0084);
volatile uint8_t xdata g_rw_lxl_gainlut_0085h_LXL_LUT1_data_133                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0085);
volatile uint8_t xdata g_rw_lxl_gainlut_0086h_LXL_LUT1_data_134                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0086);

volatile uint8_t xdata g_rw_lxl_gainlut_0087h_LXL_LUT1_data_135                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0087);
volatile uint8_t xdata g_rw_lxl_gainlut_0088h_LXL_LUT1_data_136                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0088);
volatile uint8_t xdata g_rw_lxl_gainlut_0089h_LXL_LUT1_data_137                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0089);
volatile uint8_t xdata g_rw_lxl_gainlut_008Ah_LXL_LUT1_data_138                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x008A);
volatile uint8_t xdata g_rw_lxl_gainlut_008Bh_LXL_LUT1_data_139                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x008B);

volatile uint8_t xdata g_rw_lxl_gainlut_008Ch_LXL_LUT1_data_140                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x008C);
volatile uint8_t xdata g_rw_lxl_gainlut_008Dh_LXL_LUT1_data_141                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x008D);
volatile uint8_t xdata g_rw_lxl_gainlut_008Eh_LXL_LUT1_data_142                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x008E);
volatile uint8_t xdata g_rw_lxl_gainlut_008Fh_LXL_LUT1_data_143                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x008F);
volatile uint8_t xdata g_rw_lxl_gainlut_0090h_LXL_LUT1_data_144                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0090);

volatile uint8_t xdata g_rw_lxl_gainlut_0091h_LXL_LUT1_data_145                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0091);
volatile uint8_t xdata g_rw_lxl_gainlut_0092h_LXL_LUT1_data_146                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0092);
volatile uint8_t xdata g_rw_lxl_gainlut_0093h_LXL_LUT1_data_147                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0093);
volatile uint8_t xdata g_rw_lxl_gainlut_0094h_LXL_LUT1_data_148                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0094);
volatile uint8_t xdata g_rw_lxl_gainlut_0095h_LXL_LUT1_data_149                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0095);

volatile uint8_t xdata g_rw_lxl_gainlut_0096h_LXL_LUT1_data_150                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0096);
volatile uint8_t xdata g_rw_lxl_gainlut_0097h_LXL_LUT1_data_151                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0097);
volatile uint8_t xdata g_rw_lxl_gainlut_0098h_LXL_LUT1_data_152                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0098);
volatile uint8_t xdata g_rw_lxl_gainlut_0099h_LXL_LUT1_data_153                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0099);
volatile uint8_t xdata g_rw_lxl_gainlut_009Ah_LXL_LUT1_data_154                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x009A);

volatile uint8_t xdata g_rw_lxl_gainlut_009Bh_LXL_LUT1_data_155                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x009B);
volatile uint8_t xdata g_rw_lxl_gainlut_009Ch_LXL_LUT1_data_156                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x009C);
volatile uint8_t xdata g_rw_lxl_gainlut_009Dh_LXL_LUT1_data_157                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x009D);
volatile uint8_t xdata g_rw_lxl_gainlut_009Eh_LXL_LUT1_data_158                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x009E);
volatile uint8_t xdata g_rw_lxl_gainlut_009Fh_LXL_LUT1_data_159                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x009F);

volatile uint8_t xdata g_rw_lxl_gainlut_00A0h_LXL_LUT1_data_160                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00A0);
volatile uint8_t xdata g_rw_lxl_gainlut_00A1h_LXL_LUT1_data_161                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00A1);
volatile uint8_t xdata g_rw_lxl_gainlut_00A2h_LXL_LUT1_data_162                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00A2);
volatile uint8_t xdata g_rw_lxl_gainlut_00A3h_LXL_LUT1_data_163                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00A3);
volatile uint8_t xdata g_rw_lxl_gainlut_00A4h_LXL_LUT1_data_164                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00A4);

volatile uint8_t xdata g_rw_lxl_gainlut_00A5h_LXL_LUT1_data_165                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00A5);
volatile uint8_t xdata g_rw_lxl_gainlut_00A6h_LXL_LUT1_data_166                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00A6);
volatile uint8_t xdata g_rw_lxl_gainlut_00A7h_LXL_LUT1_data_167                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00A7);
volatile uint8_t xdata g_rw_lxl_gainlut_00A8h_LXL_LUT1_data_168                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00A8);
volatile uint8_t xdata g_rw_lxl_gainlut_00A9h_LXL_LUT1_data_169                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00A9);

volatile uint8_t xdata g_rw_lxl_gainlut_00AAh_LXL_LUT1_data_170                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00AA);
volatile uint8_t xdata g_rw_lxl_gainlut_00ABh_LXL_LUT1_data_171                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00AB);
volatile uint8_t xdata g_rw_lxl_gainlut_00ACh_LXL_LUT1_data_172                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00AC);
volatile uint8_t xdata g_rw_lxl_gainlut_00ADh_LXL_LUT1_data_173                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00AD);
volatile uint8_t xdata g_rw_lxl_gainlut_00AEh_LXL_LUT1_data_174                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00AE);

volatile uint8_t xdata g_rw_lxl_gainlut_00AFh_LXL_LUT1_data_175                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00AF);
volatile uint8_t xdata g_rw_lxl_gainlut_00B0h_LXL_LUT1_data_176                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00B0);
volatile uint8_t xdata g_rw_lxl_gainlut_00B1h_LXL_LUT1_data_177                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00B1);
volatile uint8_t xdata g_rw_lxl_gainlut_00B2h_LXL_LUT1_data_178                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00B2);
volatile uint8_t xdata g_rw_lxl_gainlut_00B3h_LXL_LUT1_data_179                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00B3);

volatile uint8_t xdata g_rw_lxl_gainlut_00B4h_LXL_LUT1_data_180                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00B4);
volatile uint8_t xdata g_rw_lxl_gainlut_00B5h_LXL_LUT1_data_181                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00B5);
volatile uint8_t xdata g_rw_lxl_gainlut_00B6h_LXL_LUT1_data_182                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00B6);
volatile uint8_t xdata g_rw_lxl_gainlut_00B7h_LXL_LUT1_data_183                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00B7);
volatile uint8_t xdata g_rw_lxl_gainlut_00B8h_LXL_LUT1_data_184                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00B8);

volatile uint8_t xdata g_rw_lxl_gainlut_00B9h_LXL_LUT1_data_185                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00B9);
volatile uint8_t xdata g_rw_lxl_gainlut_00BAh_LXL_LUT1_data_186                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00BA);
volatile uint8_t xdata g_rw_lxl_gainlut_00BBh_LXL_LUT1_data_187                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00BB);
volatile uint8_t xdata g_rw_lxl_gainlut_00BCh_LXL_LUT1_data_188                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00BC);
volatile uint8_t xdata g_rw_lxl_gainlut_00BDh_LXL_LUT1_data_189                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00BD);

volatile uint8_t xdata g_rw_lxl_gainlut_00BEh_LXL_LUT1_data_190                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00BE);
volatile uint8_t xdata g_rw_lxl_gainlut_00BFh_LXL_LUT1_data_191                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00BF);
volatile uint8_t xdata g_rw_lxl_gainlut_00C0h_LXL_LUT1_data_192                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00C0);
volatile uint8_t xdata g_rw_lxl_gainlut_00C1h_LXL_LUT1_data_193                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00C1);
volatile uint8_t xdata g_rw_lxl_gainlut_00C2h_LXL_LUT1_data_194                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00C2);

volatile uint8_t xdata g_rw_lxl_gainlut_00C3h_LXL_LUT1_data_195                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00C3);
volatile uint8_t xdata g_rw_lxl_gainlut_00C4h_LXL_LUT1_data_196                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00C4);
volatile uint8_t xdata g_rw_lxl_gainlut_00C5h_LXL_LUT1_data_197                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00C5);
volatile uint8_t xdata g_rw_lxl_gainlut_00C6h_LXL_LUT1_data_198                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00C6);
volatile uint8_t xdata g_rw_lxl_gainlut_00C7h_LXL_LUT1_data_199                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00C7);

volatile uint8_t xdata g_rw_lxl_gainlut_00C8h_LXL_LUT1_data_200                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00C8);
volatile uint8_t xdata g_rw_lxl_gainlut_00C9h_LXL_LUT1_data_201                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00C9);
volatile uint8_t xdata g_rw_lxl_gainlut_00CAh_LXL_LUT1_data_202                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00CA);
volatile uint8_t xdata g_rw_lxl_gainlut_00CBh_LXL_LUT1_data_203                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00CB);
volatile uint8_t xdata g_rw_lxl_gainlut_00CCh_LXL_LUT1_data_204                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00CC);

volatile uint8_t xdata g_rw_lxl_gainlut_00CDh_LXL_LUT1_data_205                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00CD);
volatile uint8_t xdata g_rw_lxl_gainlut_00CEh_LXL_LUT1_data_206                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00CE);
volatile uint8_t xdata g_rw_lxl_gainlut_00CFh_LXL_LUT1_data_207                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00CF);
volatile uint8_t xdata g_rw_lxl_gainlut_00D0h_LXL_LUT1_data_208                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00D0);
volatile uint8_t xdata g_rw_lxl_gainlut_00D1h_LXL_LUT1_data_209                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00D1);

volatile uint8_t xdata g_rw_lxl_gainlut_00D2h_LXL_LUT1_data_210                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00D2);
volatile uint8_t xdata g_rw_lxl_gainlut_00D3h_LXL_LUT1_data_211                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00D3);
volatile uint8_t xdata g_rw_lxl_gainlut_00D4h_LXL_LUT1_data_212                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00D4);
volatile uint8_t xdata g_rw_lxl_gainlut_00D5h_LXL_LUT1_data_213                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00D5);
volatile uint8_t xdata g_rw_lxl_gainlut_00D6h_LXL_LUT1_data_214                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00D6);

volatile uint8_t xdata g_rw_lxl_gainlut_00D7h_LXL_LUT1_data_215                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00D7);
volatile uint8_t xdata g_rw_lxl_gainlut_00D8h_LXL_LUT1_data_216                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00D8);
volatile uint8_t xdata g_rw_lxl_gainlut_00D9h_LXL_LUT1_data_217                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00D9);
volatile uint8_t xdata g_rw_lxl_gainlut_00DAh_LXL_LUT1_data_218                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00DA);
volatile uint8_t xdata g_rw_lxl_gainlut_00DBh_LXL_LUT1_data_219                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00DB);

volatile uint8_t xdata g_rw_lxl_gainlut_00DCh_LXL_LUT1_data_220                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00DC);
volatile uint8_t xdata g_rw_lxl_gainlut_00DDh_LXL_LUT1_data_221                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00DD);
volatile uint8_t xdata g_rw_lxl_gainlut_00DEh_LXL_LUT1_data_222                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00DE);
volatile uint8_t xdata g_rw_lxl_gainlut_00DFh_LXL_LUT1_data_223                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00DF);
volatile uint8_t xdata g_rw_lxl_gainlut_00E0h_LXL_LUT1_data_224                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00E0);

volatile uint8_t xdata g_rw_lxl_gainlut_00E1h_LXL_LUT1_data_225                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00E1);
volatile uint8_t xdata g_rw_lxl_gainlut_00E2h_LXL_LUT1_data_226                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00E2);
volatile uint8_t xdata g_rw_lxl_gainlut_00E3h_LXL_LUT1_data_227                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00E3);
volatile uint8_t xdata g_rw_lxl_gainlut_00E4h_LXL_LUT1_data_228                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00E4);
volatile uint8_t xdata g_rw_lxl_gainlut_00E5h_LXL_LUT1_data_229                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00E5);

volatile uint8_t xdata g_rw_lxl_gainlut_00E6h_LXL_LUT1_data_230                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00E6);
volatile uint8_t xdata g_rw_lxl_gainlut_00E7h_LXL_LUT1_data_231                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00E7);
volatile uint8_t xdata g_rw_lxl_gainlut_00E8h_LXL_LUT1_data_232                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00E8);
volatile uint8_t xdata g_rw_lxl_gainlut_00E9h_LXL_LUT1_data_233                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00E9);
volatile uint8_t xdata g_rw_lxl_gainlut_00EAh_LXL_LUT1_data_234                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00EA);

volatile uint8_t xdata g_rw_lxl_gainlut_00EBh_LXL_LUT1_data_235                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00EB);
volatile uint8_t xdata g_rw_lxl_gainlut_00ECh_LXL_LUT1_data_236                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00EC);
volatile uint8_t xdata g_rw_lxl_gainlut_00EDh_LXL_LUT1_data_237                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00ED);
volatile uint8_t xdata g_rw_lxl_gainlut_00EEh_LXL_LUT1_data_238                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00EE);
volatile uint8_t xdata g_rw_lxl_gainlut_00EFh_LXL_LUT1_data_239                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00EF);

volatile uint8_t xdata g_rw_lxl_gainlut_00F0h_LXL_LUT1_data_240                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00F0);
volatile uint8_t xdata g_rw_lxl_gainlut_00F1h_LXL_LUT1_data_241                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00F1);
volatile uint8_t xdata g_rw_lxl_gainlut_00F2h_LXL_LUT1_data_242                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00F2);
volatile uint8_t xdata g_rw_lxl_gainlut_00F3h_LXL_LUT1_data_243                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00F3);
volatile uint8_t xdata g_rw_lxl_gainlut_00F4h_LXL_LUT1_data_244                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00F4);

volatile uint8_t xdata g_rw_lxl_gainlut_00F5h_LXL_LUT1_data_245                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00F5);
volatile uint8_t xdata g_rw_lxl_gainlut_00F6h_LXL_LUT1_data_246                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00F6);
volatile uint8_t xdata g_rw_lxl_gainlut_00F7h_LXL_LUT1_data_247                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00F7);
volatile uint8_t xdata g_rw_lxl_gainlut_00F8h_LXL_LUT1_data_248                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00F8);
volatile uint8_t xdata g_rw_lxl_gainlut_00F9h_LXL_LUT1_data_249                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00F9);

volatile uint8_t xdata g_rw_lxl_gainlut_00FAh_LXL_LUT1_data_250                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00FA);
volatile uint8_t xdata g_rw_lxl_gainlut_00FBh_LXL_LUT1_data_251                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00FB);
volatile uint8_t xdata g_rw_lxl_gainlut_00FCh_LXL_LUT1_data_252                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00FC);
volatile uint8_t xdata g_rw_lxl_gainlut_00FDh_LXL_LUT1_data_253                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00FD);
volatile uint8_t xdata g_rw_lxl_gainlut_00FEh_LXL_LUT1_data_254                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00FE);

volatile uint8_t xdata g_rw_lxl_gainlut_00FFh_LXL_LUT1_data_255                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x00FF);
volatile uint8_t xdata g_rw_lxl_gainlut_0100h_LXL_LUT1_data_256                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0100);
volatile uint8_t xdata g_rw_lxl_gainlut_0101h_LXL_LUT1_data_257                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0101);
volatile uint8_t xdata g_rw_lxl_gainlut_0102h_LXL_LUT1_data_258                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0102);
volatile uint8_t xdata g_rw_lxl_gainlut_0103h_LXL_LUT1_data_259                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0103);

volatile uint8_t xdata g_rw_lxl_gainlut_0104h_LXL_LUT1_data_260                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0104);
volatile uint8_t xdata g_rw_lxl_gainlut_0105h_LXL_LUT1_data_261                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0105);
volatile uint8_t xdata g_rw_lxl_gainlut_0106h_LXL_LUT1_data_262                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0106);
volatile uint8_t xdata g_rw_lxl_gainlut_0107h_LXL_LUT1_data_263                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0107);
volatile uint8_t xdata g_rw_lxl_gainlut_0108h_LXL_LUT1_data_264                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0108);

volatile uint8_t xdata g_rw_lxl_gainlut_0109h_LXL_LUT1_data_265                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0109);
volatile uint8_t xdata g_rw_lxl_gainlut_010Ah_LXL_LUT1_data_266                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x010A);
volatile uint8_t xdata g_rw_lxl_gainlut_010Bh_LXL_LUT1_data_267                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x010B);
volatile uint8_t xdata g_rw_lxl_gainlut_010Ch_LXL_LUT1_data_268                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x010C);
volatile uint8_t xdata g_rw_lxl_gainlut_010Dh_LXL_LUT1_data_269                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x010D);

volatile uint8_t xdata g_rw_lxl_gainlut_010Eh_LXL_LUT1_data_270                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x010E);
volatile uint8_t xdata g_rw_lxl_gainlut_010Fh_LXL_LUT1_data_271                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x010F);
volatile uint8_t xdata g_rw_lxl_gainlut_0110h_LXL_LUT1_data_272                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0110);
volatile uint8_t xdata g_rw_lxl_gainlut_0111h_LXL_LUT1_data_273                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0111);
volatile uint8_t xdata g_rw_lxl_gainlut_0112h_LXL_LUT1_data_274                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0112);

volatile uint8_t xdata g_rw_lxl_gainlut_0113h_LXL_LUT1_data_275                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0113);
volatile uint8_t xdata g_rw_lxl_gainlut_0114h_LXL_LUT1_data_276                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0114);
volatile uint8_t xdata g_rw_lxl_gainlut_0115h_LXL_LUT1_data_277                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0115);
volatile uint8_t xdata g_rw_lxl_gainlut_0116h_LXL_LUT1_data_278                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0116);
volatile uint8_t xdata g_rw_lxl_gainlut_0117h_LXL_LUT1_data_279                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0117);

volatile uint8_t xdata g_rw_lxl_gainlut_0118h_LXL_LUT1_data_280                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0118);
volatile uint8_t xdata g_rw_lxl_gainlut_0119h_LXL_LUT1_data_281                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0119);
volatile uint8_t xdata g_rw_lxl_gainlut_011Ah_LXL_LUT1_data_282                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x011A);
volatile uint8_t xdata g_rw_lxl_gainlut_011Bh_LXL_LUT1_data_283                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x011B);
volatile uint8_t xdata g_rw_lxl_gainlut_011Ch_LXL_LUT1_data_284                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x011C);

volatile uint8_t xdata g_rw_lxl_gainlut_011Dh_LXL_LUT1_data_285                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x011D);
volatile uint8_t xdata g_rw_lxl_gainlut_011Eh_LXL_LUT1_data_286                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x011E);
volatile uint8_t xdata g_rw_lxl_gainlut_011Fh_LXL_LUT1_data_287                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x011F);
volatile uint8_t xdata g_rw_lxl_gainlut_0120h_LXL_LUT1_data_288                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0120);
volatile uint8_t xdata g_rw_lxl_gainlut_0121h_LXL_LUT1_data_289                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0121);

volatile uint8_t xdata g_rw_lxl_gainlut_0122h_LXL_LUT1_data_290                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0122);
volatile uint8_t xdata g_rw_lxl_gainlut_0123h_LXL_LUT1_data_291                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0123);
volatile uint8_t xdata g_rw_lxl_gainlut_0124h_LXL_LUT1_data_292                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0124);
volatile uint8_t xdata g_rw_lxl_gainlut_0125h_LXL_LUT1_data_293                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0125);
volatile uint8_t xdata g_rw_lxl_gainlut_0126h_LXL_LUT1_data_294                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0126);

volatile uint8_t xdata g_rw_lxl_gainlut_0127h_LXL_LUT1_data_295                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0127);
volatile uint8_t xdata g_rw_lxl_gainlut_0128h_LXL_LUT1_data_296                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0128);
volatile uint8_t xdata g_rw_lxl_gainlut_0129h_LXL_LUT1_data_297                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0129);
volatile uint8_t xdata g_rw_lxl_gainlut_012Ah_LXL_LUT1_data_298                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x012A);
volatile uint8_t xdata g_rw_lxl_gainlut_012Bh_LXL_LUT1_data_299                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x012B);

volatile uint8_t xdata g_rw_lxl_gainlut_012Ch_LXL_LUT1_data_300                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x012C);
volatile uint8_t xdata g_rw_lxl_gainlut_012Dh_LXL_LUT1_data_301                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x012D);
volatile uint8_t xdata g_rw_lxl_gainlut_012Eh_LXL_LUT1_data_302                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x012E);
volatile uint8_t xdata g_rw_lxl_gainlut_012Fh_LXL_LUT1_data_303                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x012F);
volatile uint8_t xdata g_rw_lxl_gainlut_0130h_LXL_LUT1_data_304                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0130);

volatile uint8_t xdata g_rw_lxl_gainlut_0131h_LXL_LUT1_data_305                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0131);
volatile uint8_t xdata g_rw_lxl_gainlut_0132h_LXL_LUT1_data_306                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0132);
volatile uint8_t xdata g_rw_lxl_gainlut_0133h_LXL_LUT1_data_307                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0133);
volatile uint8_t xdata g_rw_lxl_gainlut_0134h_LXL_LUT1_data_308                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0134);
volatile uint8_t xdata g_rw_lxl_gainlut_0135h_LXL_LUT1_data_309                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0135);

volatile uint8_t xdata g_rw_lxl_gainlut_0136h_LXL_LUT1_data_310                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0136);
volatile uint8_t xdata g_rw_lxl_gainlut_0137h_LXL_LUT1_data_311                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0137);
volatile uint8_t xdata g_rw_lxl_gainlut_0138h_LXL_LUT1_data_312                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0138);
volatile uint8_t xdata g_rw_lxl_gainlut_0139h_LXL_LUT1_data_313                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0139);
volatile uint8_t xdata g_rw_lxl_gainlut_013Ah_LXL_LUT1_data_314                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x013A);

volatile uint8_t xdata g_rw_lxl_gainlut_013Bh_LXL_LUT1_data_315                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x013B);
volatile uint8_t xdata g_rw_lxl_gainlut_013Ch_LXL_LUT1_data_316                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x013C);
volatile uint8_t xdata g_rw_lxl_gainlut_013Dh_LXL_LUT1_data_317                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x013D);
volatile uint8_t xdata g_rw_lxl_gainlut_013Eh_LXL_LUT1_data_318                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x013E);
volatile uint8_t xdata g_rw_lxl_gainlut_013Fh_LXL_LUT1_data_319                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x013F);

volatile uint8_t xdata g_rw_lxl_gainlut_0140h_LXL_LUT1_data_320                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0140);
volatile uint8_t xdata g_rw_lxl_gainlut_0141h_LXL_LUT1_data_321                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0141);
volatile uint8_t xdata g_rw_lxl_gainlut_0142h_LXL_LUT1_data_322                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0142);
volatile uint8_t xdata g_rw_lxl_gainlut_0143h_LXL_LUT1_data_323                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0143);
volatile uint8_t xdata g_rw_lxl_gainlut_0144h_LXL_LUT1_data_324                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0144);

volatile uint8_t xdata g_rw_lxl_gainlut_0145h_LXL_LUT1_data_325                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0145);
volatile uint8_t xdata g_rw_lxl_gainlut_0146h_LXL_LUT1_data_326                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0146);
volatile uint8_t xdata g_rw_lxl_gainlut_0147h_LXL_LUT1_data_327                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0147);
volatile uint8_t xdata g_rw_lxl_gainlut_0148h_LXL_LUT1_data_328                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0148);
volatile uint8_t xdata g_rw_lxl_gainlut_0149h_LXL_LUT1_data_329                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0149);

volatile uint8_t xdata g_rw_lxl_gainlut_014Ah_LXL_LUT1_data_330                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x014A);
volatile uint8_t xdata g_rw_lxl_gainlut_014Bh_LXL_LUT1_data_331                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x014B);
volatile uint8_t xdata g_rw_lxl_gainlut_014Ch_LXL_LUT1_data_332                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x014C);
volatile uint8_t xdata g_rw_lxl_gainlut_014Dh_LXL_LUT1_data_333                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x014D);
volatile uint8_t xdata g_rw_lxl_gainlut_014Eh_LXL_LUT1_data_334                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x014E);

volatile uint8_t xdata g_rw_lxl_gainlut_014Fh_LXL_LUT1_data_335                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x014F);
volatile uint8_t xdata g_rw_lxl_gainlut_0150h_LXL_LUT1_data_336                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0150);
volatile uint8_t xdata g_rw_lxl_gainlut_0151h_LXL_LUT1_data_337                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0151);
volatile uint8_t xdata g_rw_lxl_gainlut_0152h_LXL_LUT1_data_338                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0152);
volatile uint8_t xdata g_rw_lxl_gainlut_0153h_LXL_LUT1_data_339                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0153);

volatile uint8_t xdata g_rw_lxl_gainlut_0154h_LXL_LUT1_data_340                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0154);
volatile uint8_t xdata g_rw_lxl_gainlut_0155h_LXL_LUT1_data_341                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0155);
volatile uint8_t xdata g_rw_lxl_gainlut_0156h_LXL_LUT1_data_342                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0156);
volatile uint8_t xdata g_rw_lxl_gainlut_0157h_LXL_LUT1_data_343                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0157);
volatile uint8_t xdata g_rw_lxl_gainlut_0158h_LXL_LUT1_data_344                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0158);

volatile uint8_t xdata g_rw_lxl_gainlut_0159h_LXL_LUT1_data_345                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0159);
volatile uint8_t xdata g_rw_lxl_gainlut_015Ah_LXL_LUT1_data_346                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x015A);
volatile uint8_t xdata g_rw_lxl_gainlut_015Bh_LXL_LUT1_data_347                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x015B);
volatile uint8_t xdata g_rw_lxl_gainlut_015Ch_LXL_LUT1_data_348                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x015C);
volatile uint8_t xdata g_rw_lxl_gainlut_015Dh_LXL_LUT1_data_349                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x015D);

volatile uint8_t xdata g_rw_lxl_gainlut_015Eh_LXL_LUT1_data_350                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x015E);
volatile uint8_t xdata g_rw_lxl_gainlut_015Fh_LXL_LUT1_data_351                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x015F);
volatile uint8_t xdata g_rw_lxl_gainlut_0160h_LXL_LUT1_data_352                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0160);
volatile uint8_t xdata g_rw_lxl_gainlut_0161h_LXL_LUT1_data_353                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0161);
volatile uint8_t xdata g_rw_lxl_gainlut_0162h_LXL_LUT1_data_354                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0162);

volatile uint8_t xdata g_rw_lxl_gainlut_0163h_LXL_LUT1_data_355                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0163);
volatile uint8_t xdata g_rw_lxl_gainlut_0164h_LXL_LUT1_data_356                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0164);
volatile uint8_t xdata g_rw_lxl_gainlut_0165h_LXL_LUT1_data_357                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0165);
volatile uint8_t xdata g_rw_lxl_gainlut_0166h_LXL_LUT1_data_358                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0166);
volatile uint8_t xdata g_rw_lxl_gainlut_0167h_LXL_LUT1_data_359                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0167);

volatile uint8_t xdata g_rw_lxl_gainlut_0168h_LXL_LUT1_data_360                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0168);
volatile uint8_t xdata g_rw_lxl_gainlut_0169h_LXL_LUT1_data_361                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0169);
volatile uint8_t xdata g_rw_lxl_gainlut_016Ah_LXL_LUT1_data_362                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x016A);
volatile uint8_t xdata g_rw_lxl_gainlut_016Bh_LXL_LUT1_data_363                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x016B);
volatile uint8_t xdata g_rw_lxl_gainlut_016Ch_LXL_LUT1_data_364                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x016C);

volatile uint8_t xdata g_rw_lxl_gainlut_016Dh_LXL_LUT1_data_365                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x016D);
volatile uint8_t xdata g_rw_lxl_gainlut_016Eh_LXL_LUT1_data_366                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x016E);
volatile uint8_t xdata g_rw_lxl_gainlut_016Fh_LXL_LUT1_data_367                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x016F);
volatile uint8_t xdata g_rw_lxl_gainlut_0170h_LXL_LUT1_data_368                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0170);
volatile uint8_t xdata g_rw_lxl_gainlut_0171h_LXL_LUT1_data_369                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0171);

volatile uint8_t xdata g_rw_lxl_gainlut_0172h_LXL_LUT1_data_370                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0172);
volatile uint8_t xdata g_rw_lxl_gainlut_0173h_LXL_LUT1_data_371                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0173);
volatile uint8_t xdata g_rw_lxl_gainlut_0174h_LXL_LUT1_data_372                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0174);
volatile uint8_t xdata g_rw_lxl_gainlut_0175h_LXL_LUT1_data_373                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0175);
volatile uint8_t xdata g_rw_lxl_gainlut_0176h_LXL_LUT1_data_374                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0176);

volatile uint8_t xdata g_rw_lxl_gainlut_0177h_LXL_LUT1_data_375                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0177);
volatile uint8_t xdata g_rw_lxl_gainlut_0178h_LXL_LUT1_data_376                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0178);
volatile uint8_t xdata g_rw_lxl_gainlut_0179h_LXL_LUT1_data_377                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0179);
volatile uint8_t xdata g_rw_lxl_gainlut_017Ah_LXL_LUT1_data_378                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x017A);
volatile uint8_t xdata g_rw_lxl_gainlut_017Bh_LXL_LUT1_data_379                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x017B);

volatile uint8_t xdata g_rw_lxl_gainlut_017Ch_LXL_LUT1_data_380                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x017C);
volatile uint8_t xdata g_rw_lxl_gainlut_017Dh_LXL_LUT1_data_381                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x017D);
volatile uint8_t xdata g_rw_lxl_gainlut_017Eh_LXL_LUT1_data_382                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x017E);
volatile uint8_t xdata g_rw_lxl_gainlut_017Fh_LXL_LUT1_data_383                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x017F);
volatile uint8_t xdata g_rw_lxl_gainlut_0180h_LXL_LUT1_data_384                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0180);

volatile uint8_t xdata g_rw_lxl_gainlut_0181h_LXL_LUT1_data_385                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0181);
volatile uint8_t xdata g_rw_lxl_gainlut_0182h_LXL_LUT1_data_386                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0182);
volatile uint8_t xdata g_rw_lxl_gainlut_0183h_LXL_LUT1_data_387                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0183);
volatile uint8_t xdata g_rw_lxl_gainlut_0184h_LXL_LUT1_data_388                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0184);
volatile uint8_t xdata g_rw_lxl_gainlut_0185h_LXL_LUT1_data_389                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0185);

volatile uint8_t xdata g_rw_lxl_gainlut_0186h_LXL_LUT1_data_390                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0186);
volatile uint8_t xdata g_rw_lxl_gainlut_0187h_LXL_LUT1_data_391                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0187);
volatile uint8_t xdata g_rw_lxl_gainlut_0188h_LXL_LUT1_data_392                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0188);
volatile uint8_t xdata g_rw_lxl_gainlut_0189h_LXL_LUT1_data_393                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0189);
volatile uint8_t xdata g_rw_lxl_gainlut_018Ah_LXL_LUT1_data_394                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x018A);

volatile uint8_t xdata g_rw_lxl_gainlut_018Bh_LXL_LUT1_data_395                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x018B);
volatile uint8_t xdata g_rw_lxl_gainlut_018Ch_LXL_LUT1_data_396                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x018C);
volatile uint8_t xdata g_rw_lxl_gainlut_018Dh_LXL_LUT1_data_397                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x018D);
volatile uint8_t xdata g_rw_lxl_gainlut_018Eh_LXL_LUT1_data_398                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x018E);
volatile uint8_t xdata g_rw_lxl_gainlut_018Fh_LXL_LUT1_data_399                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x018F);

volatile uint8_t xdata g_rw_lxl_gainlut_0190h_LXL_LUT1_data_400                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0190);
volatile uint8_t xdata g_rw_lxl_gainlut_0191h_LXL_LUT1_data_401                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0191);
volatile uint8_t xdata g_rw_lxl_gainlut_0192h_LXL_LUT1_data_402                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0192);
volatile uint8_t xdata g_rw_lxl_gainlut_0193h_LXL_LUT1_data_403                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0193);
volatile uint8_t xdata g_rw_lxl_gainlut_0194h_LXL_LUT1_data_404                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0194);

volatile uint8_t xdata g_rw_lxl_gainlut_0195h_LXL_LUT1_data_405                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0195);
volatile uint8_t xdata g_rw_lxl_gainlut_0196h_LXL_LUT1_data_406                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0196);
volatile uint8_t xdata g_rw_lxl_gainlut_0197h_LXL_LUT1_data_407                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0197);
volatile uint8_t xdata g_rw_lxl_gainlut_0198h_LXL_LUT1_data_408                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0198);
volatile uint8_t xdata g_rw_lxl_gainlut_0199h_LXL_LUT1_data_409                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0199);

volatile uint8_t xdata g_rw_lxl_gainlut_019Ah_LXL_LUT1_data_410                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x019A);
volatile uint8_t xdata g_rw_lxl_gainlut_019Bh_LXL_LUT1_data_411                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x019B);
volatile uint8_t xdata g_rw_lxl_gainlut_019Ch_LXL_LUT1_data_412                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x019C);
volatile uint8_t xdata g_rw_lxl_gainlut_019Dh_LXL_LUT1_data_413                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x019D);
volatile uint8_t xdata g_rw_lxl_gainlut_019Eh_LXL_LUT1_data_414                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x019E);

volatile uint8_t xdata g_rw_lxl_gainlut_019Fh_LXL_LUT1_data_415                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x019F);
volatile uint8_t xdata g_rw_lxl_gainlut_01A0h_LXL_LUT1_data_416                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01A0);
volatile uint8_t xdata g_rw_lxl_gainlut_01A1h_LXL_LUT1_data_417                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01A1);
volatile uint8_t xdata g_rw_lxl_gainlut_01A2h_LXL_LUT1_data_418                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01A2);
volatile uint8_t xdata g_rw_lxl_gainlut_01A3h_LXL_LUT1_data_419                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01A3);

volatile uint8_t xdata g_rw_lxl_gainlut_01A4h_LXL_LUT1_data_420                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01A4);
volatile uint8_t xdata g_rw_lxl_gainlut_01A5h_LXL_LUT1_data_421                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01A5);
volatile uint8_t xdata g_rw_lxl_gainlut_01A6h_LXL_LUT1_data_422                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01A6);
volatile uint8_t xdata g_rw_lxl_gainlut_01A7h_LXL_LUT1_data_423                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01A7);
volatile uint8_t xdata g_rw_lxl_gainlut_01A8h_LXL_LUT1_data_424                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01A8);

volatile uint8_t xdata g_rw_lxl_gainlut_01A9h_LXL_LUT1_data_425                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01A9);
volatile uint8_t xdata g_rw_lxl_gainlut_01AAh_LXL_LUT1_data_426                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01AA);
volatile uint8_t xdata g_rw_lxl_gainlut_01ABh_LXL_LUT1_data_427                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01AB);
volatile uint8_t xdata g_rw_lxl_gainlut_01ACh_LXL_LUT1_data_428                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01AC);
volatile uint8_t xdata g_rw_lxl_gainlut_01ADh_LXL_LUT1_data_429                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01AD);

volatile uint8_t xdata g_rw_lxl_gainlut_01AEh_LXL_LUT1_data_430                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01AE);
volatile uint8_t xdata g_rw_lxl_gainlut_01AFh_LXL_LUT1_data_431                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01AF);
volatile uint8_t xdata g_rw_lxl_gainlut_01B0h_LXL_LUT1_data_432                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01B0);
volatile uint8_t xdata g_rw_lxl_gainlut_01B1h_LXL_LUT1_data_433                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01B1);
volatile uint8_t xdata g_rw_lxl_gainlut_01B2h_LXL_LUT1_data_434                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01B2);

volatile uint8_t xdata g_rw_lxl_gainlut_01B3h_LXL_LUT1_data_435                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01B3);
volatile uint8_t xdata g_rw_lxl_gainlut_01B4h_LXL_LUT1_data_436                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01B4);
volatile uint8_t xdata g_rw_lxl_gainlut_01B5h_LXL_LUT1_data_437                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01B5);
volatile uint8_t xdata g_rw_lxl_gainlut_01B6h_LXL_LUT1_data_438                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01B6);
volatile uint8_t xdata g_rw_lxl_gainlut_01B7h_LXL_LUT1_data_439                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01B7);

volatile uint8_t xdata g_rw_lxl_gainlut_01B8h_LXL_LUT1_data_440                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01B8);
volatile uint8_t xdata g_rw_lxl_gainlut_01B9h_LXL_LUT1_data_441                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01B9);
volatile uint8_t xdata g_rw_lxl_gainlut_01BAh_LXL_LUT1_data_442                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01BA);
volatile uint8_t xdata g_rw_lxl_gainlut_01BBh_LXL_LUT1_data_443                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01BB);
volatile uint8_t xdata g_rw_lxl_gainlut_01BCh_LXL_LUT1_data_444                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01BC);

volatile uint8_t xdata g_rw_lxl_gainlut_01BDh_LXL_LUT1_data_445                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01BD);
volatile uint8_t xdata g_rw_lxl_gainlut_01BEh_LXL_LUT1_data_446                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01BE);
volatile uint8_t xdata g_rw_lxl_gainlut_01BFh_LXL_LUT1_data_447                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01BF);
volatile uint8_t xdata g_rw_lxl_gainlut_01C0h_LXL_LUT1_data_448                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01C0);
volatile uint8_t xdata g_rw_lxl_gainlut_01C1h_LXL_LUT1_data_449                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01C1);

volatile uint8_t xdata g_rw_lxl_gainlut_01C2h_LXL_LUT1_data_450                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01C2);
volatile uint8_t xdata g_rw_lxl_gainlut_01C3h_LXL_LUT1_data_451                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01C3);
volatile uint8_t xdata g_rw_lxl_gainlut_01C4h_LXL_LUT1_data_452                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01C4);
volatile uint8_t xdata g_rw_lxl_gainlut_01C5h_LXL_LUT1_data_453                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01C5);
volatile uint8_t xdata g_rw_lxl_gainlut_01C6h_LXL_LUT1_data_454                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01C6);

volatile uint8_t xdata g_rw_lxl_gainlut_01C7h_LXL_LUT1_data_455                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01C7);
volatile uint8_t xdata g_rw_lxl_gainlut_01C8h_LXL_LUT1_data_456                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01C8);
volatile uint8_t xdata g_rw_lxl_gainlut_01C9h_LXL_LUT1_data_457                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01C9);
volatile uint8_t xdata g_rw_lxl_gainlut_01CAh_LXL_LUT1_data_458                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01CA);
volatile uint8_t xdata g_rw_lxl_gainlut_01CBh_LXL_LUT1_data_459                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01CB);

volatile uint8_t xdata g_rw_lxl_gainlut_01CCh_LXL_LUT1_data_460                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01CC);
volatile uint8_t xdata g_rw_lxl_gainlut_01CDh_LXL_LUT1_data_461                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01CD);
volatile uint8_t xdata g_rw_lxl_gainlut_01CEh_LXL_LUT1_data_462                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01CE);
volatile uint8_t xdata g_rw_lxl_gainlut_01CFh_LXL_LUT1_data_463                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01CF);
volatile uint8_t xdata g_rw_lxl_gainlut_01D0h_LXL_LUT1_data_464                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01D0);

volatile uint8_t xdata g_rw_lxl_gainlut_01D1h_LXL_LUT1_data_465                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01D1);
volatile uint8_t xdata g_rw_lxl_gainlut_01D2h_LXL_LUT1_data_466                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01D2);
volatile uint8_t xdata g_rw_lxl_gainlut_01D3h_LXL_LUT1_data_467                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01D3);
volatile uint8_t xdata g_rw_lxl_gainlut_01D4h_LXL_LUT1_data_468                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01D4);
volatile uint8_t xdata g_rw_lxl_gainlut_01D5h_LXL_LUT1_data_469                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01D5);

volatile uint8_t xdata g_rw_lxl_gainlut_01D6h_LXL_LUT1_data_470                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01D6);
volatile uint8_t xdata g_rw_lxl_gainlut_01D7h_LXL_LUT1_data_471                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01D7);
volatile uint8_t xdata g_rw_lxl_gainlut_01D8h_LXL_LUT1_data_472                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01D8);
volatile uint8_t xdata g_rw_lxl_gainlut_01D9h_LXL_LUT1_data_473                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01D9);
volatile uint8_t xdata g_rw_lxl_gainlut_01DAh_LXL_LUT1_data_474                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01DA);

volatile uint8_t xdata g_rw_lxl_gainlut_01DBh_LXL_LUT1_data_475                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01DB);
volatile uint8_t xdata g_rw_lxl_gainlut_01DCh_LXL_LUT1_data_476                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01DC);
volatile uint8_t xdata g_rw_lxl_gainlut_01DDh_LXL_LUT1_data_477                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01DD);
volatile uint8_t xdata g_rw_lxl_gainlut_01DEh_LXL_LUT1_data_478                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01DE);
volatile uint8_t xdata g_rw_lxl_gainlut_01DFh_LXL_LUT1_data_479                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01DF);

volatile uint8_t xdata g_rw_lxl_gainlut_01E0h_LXL_LUT1_data_480                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01E0);
volatile uint8_t xdata g_rw_lxl_gainlut_01E1h_LXL_LUT1_data_481                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01E1);
volatile uint8_t xdata g_rw_lxl_gainlut_01E2h_LXL_LUT1_data_482                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01E2);
volatile uint8_t xdata g_rw_lxl_gainlut_01E3h_LXL_LUT1_data_483                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01E3);
volatile uint8_t xdata g_rw_lxl_gainlut_01E4h_LXL_LUT1_data_484                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01E4);

volatile uint8_t xdata g_rw_lxl_gainlut_01E5h_LXL_LUT1_data_485                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01E5);
volatile uint8_t xdata g_rw_lxl_gainlut_01E6h_LXL_LUT1_data_486                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01E6);
volatile uint8_t xdata g_rw_lxl_gainlut_01E7h_LXL_LUT1_data_487                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01E7);
volatile uint8_t xdata g_rw_lxl_gainlut_01E8h_LXL_LUT1_data_488                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01E8);
volatile uint8_t xdata g_rw_lxl_gainlut_01E9h_LXL_LUT1_data_489                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01E9);

volatile uint8_t xdata g_rw_lxl_gainlut_01EAh_LXL_LUT1_data_490                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01EA);
volatile uint8_t xdata g_rw_lxl_gainlut_01EBh_LXL_LUT1_data_491                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01EB);
volatile uint8_t xdata g_rw_lxl_gainlut_01ECh_LXL_LUT1_data_492                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01EC);
volatile uint8_t xdata g_rw_lxl_gainlut_01EDh_LXL_LUT1_data_493                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01ED);
volatile uint8_t xdata g_rw_lxl_gainlut_01EEh_LXL_LUT1_data_494                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01EE);

volatile uint8_t xdata g_rw_lxl_gainlut_01EFh_LXL_LUT1_data_495                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01EF);
volatile uint8_t xdata g_rw_lxl_gainlut_01F0h_LXL_LUT1_data_496                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01F0);
volatile uint8_t xdata g_rw_lxl_gainlut_01F1h_LXL_LUT1_data_497                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01F1);
volatile uint8_t xdata g_rw_lxl_gainlut_01F2h_LXL_LUT1_data_498                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01F2);
volatile uint8_t xdata g_rw_lxl_gainlut_01F3h_LXL_LUT1_data_499                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01F3);

volatile uint8_t xdata g_rw_lxl_gainlut_01F4h_LXL_LUT1_data_500                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01F4);
volatile uint8_t xdata g_rw_lxl_gainlut_01F5h_LXL_LUT1_data_501                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01F5);
volatile uint8_t xdata g_rw_lxl_gainlut_01F6h_LXL_LUT1_data_502                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01F6);
volatile uint8_t xdata g_rw_lxl_gainlut_01F7h_LXL_LUT1_data_503                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01F7);
volatile uint8_t xdata g_rw_lxl_gainlut_01F8h_LXL_LUT1_data_504                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01F8);

volatile uint8_t xdata g_rw_lxl_gainlut_01F9h_LXL_LUT1_data_505                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01F9);
volatile uint8_t xdata g_rw_lxl_gainlut_01FAh_LXL_LUT1_data_506                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01FA);
volatile uint8_t xdata g_rw_lxl_gainlut_01FBh_LXL_LUT1_data_507                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01FB);
volatile uint8_t xdata g_rw_lxl_gainlut_01FCh_LXL_LUT1_data_508                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01FC);
volatile uint8_t xdata g_rw_lxl_gainlut_01FDh_LXL_LUT1_data_509                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01FD);

volatile uint8_t xdata g_rw_lxl_gainlut_01FEh_LXL_LUT1_data_510                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01FE);
volatile uint8_t xdata g_rw_lxl_gainlut_01FFh_LXL_LUT1_data_511                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x01FF);
volatile uint8_t xdata g_rw_lxl_gainlut_0200h_LXL_LUT1_data_512                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0200);
volatile uint8_t xdata g_rw_lxl_gainlut_0201h_LXL_LUT1_data_513                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0201);
volatile uint8_t xdata g_rw_lxl_gainlut_0202h_LXL_LUT1_data_514                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0202);

volatile uint8_t xdata g_rw_lxl_gainlut_0203h_LXL_LUT1_data_515                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0203);
volatile uint8_t xdata g_rw_lxl_gainlut_0204h_LXL_LUT1_data_516                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0204);
volatile uint8_t xdata g_rw_lxl_gainlut_0205h_LXL_LUT1_data_517                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0205);
volatile uint8_t xdata g_rw_lxl_gainlut_0206h_LXL_LUT1_data_518                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0206);
volatile uint8_t xdata g_rw_lxl_gainlut_0207h_LXL_LUT1_data_519                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0207);

volatile uint8_t xdata g_rw_lxl_gainlut_0208h_LXL_LUT1_data_520                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0208);
volatile uint8_t xdata g_rw_lxl_gainlut_0209h_LXL_LUT1_data_521                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0209);
volatile uint8_t xdata g_rw_lxl_gainlut_020Ah_LXL_LUT1_data_522                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x020A);
volatile uint8_t xdata g_rw_lxl_gainlut_020Bh_LXL_LUT1_data_523                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x020B);
volatile uint8_t xdata g_rw_lxl_gainlut_020Ch_LXL_LUT1_data_524                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x020C);

volatile uint8_t xdata g_rw_lxl_gainlut_020Dh_LXL_LUT1_data_525                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x020D);
volatile uint8_t xdata g_rw_lxl_gainlut_020Eh_LXL_LUT1_data_526                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x020E);
volatile uint8_t xdata g_rw_lxl_gainlut_020Fh_LXL_LUT1_data_527                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x020F);
volatile uint8_t xdata g_rw_lxl_gainlut_0210h_LXL_LUT1_data_528                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0210);
volatile uint8_t xdata g_rw_lxl_gainlut_0211h_LXL_LUT1_data_529                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0211);

volatile uint8_t xdata g_rw_lxl_gainlut_0212h_LXL_LUT1_data_530                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0212);
volatile uint8_t xdata g_rw_lxl_gainlut_0213h_LXL_LUT1_data_531                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0213);
volatile uint8_t xdata g_rw_lxl_gainlut_0214h_LXL_LUT1_data_532                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0214);
volatile uint8_t xdata g_rw_lxl_gainlut_0215h_LXL_LUT1_data_533                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0215);
volatile uint8_t xdata g_rw_lxl_gainlut_0216h_LXL_LUT1_data_534                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0216);

volatile uint8_t xdata g_rw_lxl_gainlut_0217h_LXL_LUT1_data_535                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0217);
volatile uint8_t xdata g_rw_lxl_gainlut_0218h_LXL_LUT1_data_536                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0218);
volatile uint8_t xdata g_rw_lxl_gainlut_0219h_LXL_LUT1_data_537                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0219);
volatile uint8_t xdata g_rw_lxl_gainlut_021Ah_LXL_LUT1_data_538                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x021A);
volatile uint8_t xdata g_rw_lxl_gainlut_021Bh_LXL_LUT1_data_539                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x021B);

volatile uint8_t xdata g_rw_lxl_gainlut_021Ch_LXL_LUT1_data_540                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x021C);
volatile uint8_t xdata g_rw_lxl_gainlut_021Dh_LXL_LUT1_data_541                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x021D);
volatile uint8_t xdata g_rw_lxl_gainlut_021Eh_LXL_LUT1_data_542                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x021E);
volatile uint8_t xdata g_rw_lxl_gainlut_021Fh_LXL_LUT1_data_543                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x021F);
volatile uint8_t xdata g_rw_lxl_gainlut_0220h_LXL_LUT1_data_544                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0220);

volatile uint8_t xdata g_rw_lxl_gainlut_0221h_LXL_LUT1_data_545                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0221);
volatile uint8_t xdata g_rw_lxl_gainlut_0222h_LXL_LUT1_data_546                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0222);
volatile uint8_t xdata g_rw_lxl_gainlut_0223h_LXL_LUT1_data_547                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0223);
volatile uint8_t xdata g_rw_lxl_gainlut_0224h_LXL_LUT1_data_548                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0224);
volatile uint8_t xdata g_rw_lxl_gainlut_0225h_LXL_LUT1_data_549                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0225);

volatile uint8_t xdata g_rw_lxl_gainlut_0226h_LXL_LUT1_data_550                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0226);
volatile uint8_t xdata g_rw_lxl_gainlut_0227h_LXL_LUT1_data_551                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0227);
volatile uint8_t xdata g_rw_lxl_gainlut_0228h_LXL_LUT1_data_552                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0228);
volatile uint8_t xdata g_rw_lxl_gainlut_0229h_LXL_LUT1_data_553                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0229);
volatile uint8_t xdata g_rw_lxl_gainlut_022Ah_LXL_LUT1_data_554                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x022A);

volatile uint8_t xdata g_rw_lxl_gainlut_022Bh_LXL_LUT1_data_555                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x022B);
volatile uint8_t xdata g_rw_lxl_gainlut_022Ch_LXL_LUT1_data_556                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x022C);
volatile uint8_t xdata g_rw_lxl_gainlut_022Dh_LXL_LUT1_data_557                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x022D);
volatile uint8_t xdata g_rw_lxl_gainlut_022Eh_LXL_LUT1_data_558                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x022E);
volatile uint8_t xdata g_rw_lxl_gainlut_022Fh_LXL_LUT1_data_559                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x022F);

volatile uint8_t xdata g_rw_lxl_gainlut_0230h_LXL_LUT1_data_560                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0230);
volatile uint8_t xdata g_rw_lxl_gainlut_0231h_LXL_LUT1_data_561                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0231);
volatile uint8_t xdata g_rw_lxl_gainlut_0232h_LXL_LUT1_data_562                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0232);
volatile uint8_t xdata g_rw_lxl_gainlut_0233h_LXL_LUT1_data_563                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0233);
volatile uint8_t xdata g_rw_lxl_gainlut_0234h_LXL_LUT1_data_564                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0234);

volatile uint8_t xdata g_rw_lxl_gainlut_0235h_LXL_LUT1_data_565                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0235);
volatile uint8_t xdata g_rw_lxl_gainlut_0236h_LXL_LUT1_data_566                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0236);
volatile uint8_t xdata g_rw_lxl_gainlut_0237h_LXL_LUT1_data_567                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0237);
volatile uint8_t xdata g_rw_lxl_gainlut_0238h_LXL_LUT1_data_568                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0238);
volatile uint8_t xdata g_rw_lxl_gainlut_0239h_LXL_LUT1_data_569                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0239);

volatile uint8_t xdata g_rw_lxl_gainlut_023Ah_LXL_LUT1_data_570                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x023A);
volatile uint8_t xdata g_rw_lxl_gainlut_023Bh_LXL_LUT1_data_571                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x023B);
volatile uint8_t xdata g_rw_lxl_gainlut_023Ch_LXL_LUT1_data_572                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x023C);
volatile uint8_t xdata g_rw_lxl_gainlut_023Dh_LXL_LUT1_data_573                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x023D);
volatile uint8_t xdata g_rw_lxl_gainlut_023Eh_LXL_LUT1_data_574                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x023E);

volatile uint8_t xdata g_rw_lxl_gainlut_023Fh_LXL_LUT1_data_575                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x023F);
volatile uint8_t xdata g_rw_lxl_gainlut_0240h_LXL_LUT1_data_576                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0240);
volatile uint8_t xdata g_rw_lxl_gainlut_0241h_LXL_LUT1_data_577                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0241);
volatile uint8_t xdata g_rw_lxl_gainlut_0242h_LXL_LUT1_data_578                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0242);
volatile uint8_t xdata g_rw_lxl_gainlut_0243h_LXL_LUT1_data_579                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0243);

volatile uint8_t xdata g_rw_lxl_gainlut_0244h_LXL_LUT1_data_580                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0244);
volatile uint8_t xdata g_rw_lxl_gainlut_0245h_LXL_LUT1_data_581                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0245);
volatile uint8_t xdata g_rw_lxl_gainlut_0246h_LXL_LUT1_data_582                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0246);
volatile uint8_t xdata g_rw_lxl_gainlut_0247h_LXL_LUT1_data_583                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0247);
volatile uint8_t xdata g_rw_lxl_gainlut_0248h_LXL_LUT1_data_584                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0248);

volatile uint8_t xdata g_rw_lxl_gainlut_0249h_LXL_LUT1_data_585                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0249);
volatile uint8_t xdata g_rw_lxl_gainlut_024Ah_LXL_LUT1_data_586                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x024A);
volatile uint8_t xdata g_rw_lxl_gainlut_024Bh_LXL_LUT1_data_587                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x024B);
volatile uint8_t xdata g_rw_lxl_gainlut_024Ch_LXL_LUT1_data_588                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x024C);
volatile uint8_t xdata g_rw_lxl_gainlut_024Dh_LXL_LUT1_data_589                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x024D);

volatile uint8_t xdata g_rw_lxl_gainlut_024Eh_LXL_LUT1_data_590                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x024E);
volatile uint8_t xdata g_rw_lxl_gainlut_024Fh_LXL_LUT1_data_591                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x024F);
volatile uint8_t xdata g_rw_lxl_gainlut_0250h_LXL_LUT1_data_592                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0250);
volatile uint8_t xdata g_rw_lxl_gainlut_0251h_LXL_LUT1_data_593                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0251);
volatile uint8_t xdata g_rw_lxl_gainlut_0252h_LXL_LUT1_data_594                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0252);

volatile uint8_t xdata g_rw_lxl_gainlut_0253h_LXL_LUT1_data_595                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0253);
volatile uint8_t xdata g_rw_lxl_gainlut_0254h_LXL_LUT1_data_596                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0254);
volatile uint8_t xdata g_rw_lxl_gainlut_0255h_LXL_LUT1_data_597                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0255);
volatile uint8_t xdata g_rw_lxl_gainlut_0256h_LXL_LUT1_data_598                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0256);
volatile uint8_t xdata g_rw_lxl_gainlut_0257h_LXL_LUT1_data_599                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0257);

volatile uint8_t xdata g_rw_lxl_gainlut_0258h_LXL_LUT1_data_600                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0258);
volatile uint8_t xdata g_rw_lxl_gainlut_0259h_LXL_LUT1_data_601                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0259);
volatile uint8_t xdata g_rw_lxl_gainlut_025Ah_LXL_LUT1_data_602                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x025A);
volatile uint8_t xdata g_rw_lxl_gainlut_025Bh_LXL_LUT1_data_603                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x025B);
volatile uint8_t xdata g_rw_lxl_gainlut_025Ch_LXL_LUT1_data_604                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x025C);

volatile uint8_t xdata g_rw_lxl_gainlut_025Dh_LXL_LUT1_data_605                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x025D);
volatile uint8_t xdata g_rw_lxl_gainlut_025Eh_LXL_LUT1_data_606                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x025E);
volatile uint8_t xdata g_rw_lxl_gainlut_025Fh_LXL_LUT1_data_607                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x025F);
volatile uint8_t xdata g_rw_lxl_gainlut_0260h_LXL_LUT1_data_608                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0260);
volatile uint8_t xdata g_rw_lxl_gainlut_0261h_LXL_LUT1_data_609                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0261);

volatile uint8_t xdata g_rw_lxl_gainlut_0262h_LXL_LUT1_data_610                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0262);
volatile uint8_t xdata g_rw_lxl_gainlut_0263h_LXL_LUT1_data_611                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0263);
volatile uint8_t xdata g_rw_lxl_gainlut_0264h_LXL_LUT1_data_612                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0264);
volatile uint8_t xdata g_rw_lxl_gainlut_0265h_LXL_LUT1_data_613                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0265);
volatile uint8_t xdata g_rw_lxl_gainlut_0266h_LXL_LUT1_data_614                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0266);

volatile uint8_t xdata g_rw_lxl_gainlut_0267h_LXL_LUT1_data_615                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0267);
volatile uint8_t xdata g_rw_lxl_gainlut_0268h_LXL_LUT1_data_616                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0268);
volatile uint8_t xdata g_rw_lxl_gainlut_0269h_LXL_LUT1_data_617                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0269);
volatile uint8_t xdata g_rw_lxl_gainlut_026Ah_LXL_LUT1_data_618                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x026A);
volatile uint8_t xdata g_rw_lxl_gainlut_026Bh_LXL_LUT1_data_619                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x026B);

volatile uint8_t xdata g_rw_lxl_gainlut_026Ch_LXL_LUT1_data_620                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x026C);
volatile uint8_t xdata g_rw_lxl_gainlut_026Dh_LXL_LUT1_data_621                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x026D);
volatile uint8_t xdata g_rw_lxl_gainlut_026Eh_LXL_LUT1_data_622                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x026E);
volatile uint8_t xdata g_rw_lxl_gainlut_026Fh_LXL_LUT1_data_623                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x026F);
volatile uint8_t xdata g_rw_lxl_gainlut_0270h_LXL_LUT1_data_624                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0270);

volatile uint8_t xdata g_rw_lxl_gainlut_0271h_LXL_LUT1_data_625                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0271);
volatile uint8_t xdata g_rw_lxl_gainlut_0272h_LXL_LUT1_data_626                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0272);
volatile uint8_t xdata g_rw_lxl_gainlut_0273h_LXL_LUT1_data_627                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0273);
volatile uint8_t xdata g_rw_lxl_gainlut_0274h_LXL_LUT1_data_628                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0274);
volatile uint8_t xdata g_rw_lxl_gainlut_0275h_LXL_LUT1_data_629                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0275);

volatile uint8_t xdata g_rw_lxl_gainlut_0276h_LXL_LUT1_data_630                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0276);
volatile uint8_t xdata g_rw_lxl_gainlut_0277h_LXL_LUT1_data_631                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0277);
volatile uint8_t xdata g_rw_lxl_gainlut_0278h_LXL_LUT1_data_632                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0278);
volatile uint8_t xdata g_rw_lxl_gainlut_0279h_LXL_LUT1_data_633                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0279);
volatile uint8_t xdata g_rw_lxl_gainlut_027Ah_LXL_LUT1_data_634                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x027A);

volatile uint8_t xdata g_rw_lxl_gainlut_027Bh_LXL_LUT1_data_635                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x027B);
volatile uint8_t xdata g_rw_lxl_gainlut_027Ch_LXL_LUT1_data_636                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x027C);
volatile uint8_t xdata g_rw_lxl_gainlut_027Dh_LXL_LUT1_data_637                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x027D);
volatile uint8_t xdata g_rw_lxl_gainlut_027Eh_LXL_LUT1_data_638                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x027E);
volatile uint8_t xdata g_rw_lxl_gainlut_027Fh_LXL_LUT1_data_639                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x027F);

volatile uint8_t xdata g_rw_lxl_gainlut_0280h_LXL_LUT1_data_640                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0280);
volatile uint8_t xdata g_rw_lxl_gainlut_0281h_LXL_LUT1_data_641                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0281);
volatile uint8_t xdata g_rw_lxl_gainlut_0282h_LXL_LUT1_data_642                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0282);
volatile uint8_t xdata g_rw_lxl_gainlut_0283h_LXL_LUT1_data_643                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0283);
volatile uint8_t xdata g_rw_lxl_gainlut_0284h_LXL_LUT1_data_644                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0284);

volatile uint8_t xdata g_rw_lxl_gainlut_0285h_LXL_LUT1_data_645                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0285);
volatile uint8_t xdata g_rw_lxl_gainlut_0286h_LXL_LUT1_data_646                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0286);
volatile uint8_t xdata g_rw_lxl_gainlut_0287h_LXL_LUT1_data_647                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0287);
volatile uint8_t xdata g_rw_lxl_gainlut_0288h_LXL_LUT1_data_648                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0288);
volatile uint8_t xdata g_rw_lxl_gainlut_0289h_LXL_LUT1_data_649                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0289);

volatile uint8_t xdata g_rw_lxl_gainlut_028Ah_LXL_LUT1_data_650                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x028A);
volatile uint8_t xdata g_rw_lxl_gainlut_028Bh_LXL_LUT1_data_651                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x028B);
volatile uint8_t xdata g_rw_lxl_gainlut_028Ch_LXL_LUT1_data_652                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x028C);
volatile uint8_t xdata g_rw_lxl_gainlut_028Dh_LXL_LUT1_data_653                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x028D);
volatile uint8_t xdata g_rw_lxl_gainlut_028Eh_LXL_LUT1_data_654                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x028E);

volatile uint8_t xdata g_rw_lxl_gainlut_028Fh_LXL_LUT1_data_655                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x028F);
volatile uint8_t xdata g_rw_lxl_gainlut_0290h_LXL_LUT1_data_656                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0290);
volatile uint8_t xdata g_rw_lxl_gainlut_0291h_LXL_LUT1_data_657                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0291);
volatile uint8_t xdata g_rw_lxl_gainlut_0292h_LXL_LUT1_data_658                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0292);
volatile uint8_t xdata g_rw_lxl_gainlut_0293h_LXL_LUT1_data_659                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0293);

volatile uint8_t xdata g_rw_lxl_gainlut_0294h_LXL_LUT1_data_660                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0294);
volatile uint8_t xdata g_rw_lxl_gainlut_0295h_LXL_LUT1_data_661                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0295);
volatile uint8_t xdata g_rw_lxl_gainlut_0296h_LXL_LUT1_data_662                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0296);
volatile uint8_t xdata g_rw_lxl_gainlut_0297h_LXL_LUT1_data_663                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0297);
volatile uint8_t xdata g_rw_lxl_gainlut_0298h_LXL_LUT1_data_664                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0298);

volatile uint8_t xdata g_rw_lxl_gainlut_0299h_LXL_LUT1_data_665                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x0299);
volatile uint8_t xdata g_rw_lxl_gainlut_029Ah_LXL_LUT1_data_666                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x029A);
volatile uint8_t xdata g_rw_lxl_gainlut_029Bh_LXL_LUT1_data_667                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x029B);
volatile uint8_t xdata g_rw_lxl_gainlut_029Ch_LXL_LUT1_data_668                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x029C);
volatile uint8_t xdata g_rw_lxl_gainlut_029Dh_LXL_LUT1_data_669                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x029D);

volatile uint8_t xdata g_rw_lxl_gainlut_029Eh_LXL_LUT1_data_670                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x029E);
volatile uint8_t xdata g_rw_lxl_gainlut_029Fh_LXL_LUT1_data_671                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x029F);
volatile uint8_t xdata g_rw_lxl_gainlut_02A0h_LXL_LUT1_data_672                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x02A0);
volatile uint8_t xdata g_rw_lxl_gainlut_02A1h_LXL_LUT1_data_673                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x02A1);
volatile uint8_t xdata g_rw_lxl_gainlut_02A2h_LXL_LUT1_data_674                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x02A2);

volatile uint8_t xdata g_rw_lxl_gainlut_02A3h_LXL_LUT1_data_675                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x02A3);
volatile uint8_t xdata g_rw_lxl_gainlut_02A4h_LXL_LUT1_data_676                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x02A4);
volatile uint8_t xdata g_rw_lxl_gainlut_02A5h_LXL_LUT1_data_677                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x02A5);
volatile uint8_t xdata g_rw_lxl_gainlut_02A6h_LXL_LUT1_data_678                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x02A6);
volatile uint8_t xdata g_rw_lxl_gainlut_02A7h_LXL_LUT1_data_679                                _at_  (LXL_GAINLUT_FIELD_XMEM_BASE + 0x02A7);


#endif 
