#include "sys.h"
#include "hw_mem_map.h"

#ifdef SYS_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------
volatile union rw_sys_0000h xdata g_rw_sys_0000h  _at_  (SYS_FIELD_XMEM_BASE + 0x0000);
volatile union rw_sys_0001h xdata g_rw_sys_0001h  _at_  (SYS_FIELD_XMEM_BASE + 0x0001);
volatile union rw_sys_0002h xdata g_rw_sys_0002h  _at_  (SYS_FIELD_XMEM_BASE + 0x0002);
volatile union rw_sys_0003h xdata g_rw_sys_0003h  _at_  (SYS_FIELD_XMEM_BASE + 0x0003);
volatile union rw_sys_0006h xdata g_rw_sys_0006h  _at_  (SYS_FIELD_XMEM_BASE + 0x0006);

volatile union rw_sys_0007h xdata g_rw_sys_0007h  _at_  (SYS_FIELD_XMEM_BASE + 0x0007);
volatile union rw_sys_0008h xdata g_rw_sys_0008h  _at_  (SYS_FIELD_XMEM_BASE + 0x0008);
volatile union rw_sys_0009h xdata g_rw_sys_0009h  _at_  (SYS_FIELD_XMEM_BASE + 0x0009);
volatile union rw_sys_000Ah xdata g_rw_sys_000Ah  _at_  (SYS_FIELD_XMEM_BASE + 0x000A);
volatile union rw_sys_000Bh xdata g_rw_sys_000Bh  _at_  (SYS_FIELD_XMEM_BASE + 0x000B);

volatile union rw_sys_000Ch xdata g_rw_sys_000Ch  _at_  (SYS_FIELD_XMEM_BASE + 0x000C);
volatile union rw_sys_000Dh xdata g_rw_sys_000Dh  _at_  (SYS_FIELD_XMEM_BASE + 0x000D);
volatile union rw_sys_000Eh xdata g_rw_sys_000Eh  _at_  (SYS_FIELD_XMEM_BASE + 0x000E);
volatile union rw_sys_000Fh xdata g_rw_sys_000Fh  _at_  (SYS_FIELD_XMEM_BASE + 0x000F);
volatile union rw_sys_0010h xdata g_rw_sys_0010h  _at_  (SYS_FIELD_XMEM_BASE + 0x0010);

volatile union rw_sys_0011h xdata g_rw_sys_0011h  _at_  (SYS_FIELD_XMEM_BASE + 0x0011);
volatile union rw_sys_0012h xdata g_rw_sys_0012h  _at_  (SYS_FIELD_XMEM_BASE + 0x0012);
volatile union rw_sys_0013h xdata g_rw_sys_0013h  _at_  (SYS_FIELD_XMEM_BASE + 0x0013);
volatile union rw_sys_0014h xdata g_rw_sys_0014h  _at_  (SYS_FIELD_XMEM_BASE + 0x0014);
volatile union rw_sys_0015h xdata g_rw_sys_0015h  _at_  (SYS_FIELD_XMEM_BASE + 0x0015);

volatile union rw_sys_0016h xdata g_rw_sys_0016h  _at_  (SYS_FIELD_XMEM_BASE + 0x0016);
volatile union rw_sys_0017h xdata g_rw_sys_0017h  _at_  (SYS_FIELD_XMEM_BASE + 0x0017);
volatile union rw_sys_001Eh xdata g_rw_sys_001Eh  _at_  (SYS_FIELD_XMEM_BASE + 0x001E);
volatile union rw_sys_001Fh xdata g_rw_sys_001Fh  _at_  (SYS_FIELD_XMEM_BASE + 0x001F);
volatile union rw_sys_0020h xdata g_rw_sys_0020h  _at_  (SYS_FIELD_XMEM_BASE + 0x0020);

volatile union rw_sys_0021h xdata g_rw_sys_0021h  _at_  (SYS_FIELD_XMEM_BASE + 0x0021);
volatile union rw_sys_0022h xdata g_rw_sys_0022h  _at_  (SYS_FIELD_XMEM_BASE + 0x0022);
volatile union rw_sys_0023h xdata g_rw_sys_0023h  _at_  (SYS_FIELD_XMEM_BASE + 0x0023);
volatile union rw_sys_0024h xdata g_rw_sys_0024h  _at_  (SYS_FIELD_XMEM_BASE + 0x0024);
volatile union rw_sys_0025h xdata g_rw_sys_0025h  _at_  (SYS_FIELD_XMEM_BASE + 0x0025);

volatile union rw_sys_0026h xdata g_rw_sys_0026h  _at_  (SYS_FIELD_XMEM_BASE + 0x0026);
volatile union rw_sys_0027h xdata g_rw_sys_0027h  _at_  (SYS_FIELD_XMEM_BASE + 0x0027);
volatile union rw_sys_0028h xdata g_rw_sys_0028h  _at_  (SYS_FIELD_XMEM_BASE + 0x0028);
volatile union rw_sys_0029h xdata g_rw_sys_0029h  _at_  (SYS_FIELD_XMEM_BASE + 0x0029);
volatile union rw_sys_002Ah xdata g_rw_sys_002Ah  _at_  (SYS_FIELD_XMEM_BASE + 0x002A);

volatile union rw_sys_002Bh xdata g_rw_sys_002Bh  _at_  (SYS_FIELD_XMEM_BASE + 0x002B);
volatile union rw_sys_002Ch xdata g_rw_sys_002Ch  _at_  (SYS_FIELD_XMEM_BASE + 0x002C);
volatile union rw_sys_002Dh xdata g_rw_sys_002Dh  _at_  (SYS_FIELD_XMEM_BASE + 0x002D);
volatile union rw_sys_002Eh xdata g_rw_sys_002Eh  _at_  (SYS_FIELD_XMEM_BASE + 0x002E);
volatile union rw_sys_002Fh xdata g_rw_sys_002Fh  _at_  (SYS_FIELD_XMEM_BASE + 0x002F);

volatile union rw_sys_0030h xdata g_rw_sys_0030h  _at_  (SYS_FIELD_XMEM_BASE + 0x0030);
volatile union rw_sys_0031h xdata g_rw_sys_0031h  _at_  (SYS_FIELD_XMEM_BASE + 0x0031);
volatile union rw_sys_0032h xdata g_rw_sys_0032h  _at_  (SYS_FIELD_XMEM_BASE + 0x0032);
volatile union rw_sys_0033h xdata g_rw_sys_0033h  _at_  (SYS_FIELD_XMEM_BASE + 0x0033);
volatile union rw_sys_0038h xdata g_rw_sys_0038h  _at_  (SYS_FIELD_XMEM_BASE + 0x0038);

volatile union rw_sys_0039h xdata g_rw_sys_0039h  _at_  (SYS_FIELD_XMEM_BASE + 0x0039);
volatile union rw_sys_003Ah xdata g_rw_sys_003Ah  _at_  (SYS_FIELD_XMEM_BASE + 0x003A);
volatile union rw_sys_003Bh xdata g_rw_sys_003Bh  _at_  (SYS_FIELD_XMEM_BASE + 0x003B);
volatile union rw_sys_003Ch xdata g_rw_sys_003Ch  _at_  (SYS_FIELD_XMEM_BASE + 0x003C);
volatile union rw_sys_003Dh xdata g_rw_sys_003Dh  _at_  (SYS_FIELD_XMEM_BASE + 0x003D);

volatile union rw_sys_003Eh xdata g_rw_sys_003Eh  _at_  (SYS_FIELD_XMEM_BASE + 0x003E);
volatile union rw_sys_003Fh xdata g_rw_sys_003Fh  _at_  (SYS_FIELD_XMEM_BASE + 0x003F);
volatile union rw_sys_0040h xdata g_rw_sys_0040h  _at_  (SYS_FIELD_XMEM_BASE + 0x0040);
volatile union rw_sys_0041h xdata g_rw_sys_0041h  _at_  (SYS_FIELD_XMEM_BASE + 0x0041);
volatile union rw_sys_0042h xdata g_rw_sys_0042h  _at_  (SYS_FIELD_XMEM_BASE + 0x0042);

volatile union rw_sys_0043h xdata g_rw_sys_0043h  _at_  (SYS_FIELD_XMEM_BASE + 0x0043);
volatile union rw_sys_0044h xdata g_rw_sys_0044h  _at_  (SYS_FIELD_XMEM_BASE + 0x0044);
volatile union rw_sys_0045h xdata g_rw_sys_0045h  _at_  (SYS_FIELD_XMEM_BASE + 0x0045);
volatile union rw_sys_0046h xdata g_rw_sys_0046h  _at_  (SYS_FIELD_XMEM_BASE + 0x0046);
volatile union rw_sys_0047h xdata g_rw_sys_0047h  _at_  (SYS_FIELD_XMEM_BASE + 0x0047);

volatile union rw_sys_0048h xdata g_rw_sys_0048h  _at_  (SYS_FIELD_XMEM_BASE + 0x0048);
volatile union rw_sys_0049h xdata g_rw_sys_0049h  _at_  (SYS_FIELD_XMEM_BASE + 0x0049);
volatile union rw_sys_004Ah xdata g_rw_sys_004Ah  _at_  (SYS_FIELD_XMEM_BASE + 0x004A);
volatile union rw_sys_004Bh xdata g_rw_sys_004Bh  _at_  (SYS_FIELD_XMEM_BASE + 0x004B);
volatile union rw_sys_004Ch xdata g_rw_sys_004Ch  _at_  (SYS_FIELD_XMEM_BASE + 0x004C);

volatile union rw_sys_004Dh xdata g_rw_sys_004Dh  _at_  (SYS_FIELD_XMEM_BASE + 0x004D);
volatile union rw_sys_004Eh xdata g_rw_sys_004Eh  _at_  (SYS_FIELD_XMEM_BASE + 0x004E);
volatile union rw_sys_004Fh xdata g_rw_sys_004Fh  _at_  (SYS_FIELD_XMEM_BASE + 0x004F);
volatile union rw_sys_0050h xdata g_rw_sys_0050h  _at_  (SYS_FIELD_XMEM_BASE + 0x0050);
volatile union rw_sys_0051h xdata g_rw_sys_0051h  _at_  (SYS_FIELD_XMEM_BASE + 0x0051);

volatile union rw_sys_0052h xdata g_rw_sys_0052h  _at_  (SYS_FIELD_XMEM_BASE + 0x0052);
volatile union rw_sys_0053h xdata g_rw_sys_0053h  _at_  (SYS_FIELD_XMEM_BASE + 0x0053);
volatile union rw_sys_0054h xdata g_rw_sys_0054h  _at_  (SYS_FIELD_XMEM_BASE + 0x0054);
volatile union rw_sys_0055h xdata g_rw_sys_0055h  _at_  (SYS_FIELD_XMEM_BASE + 0x0055);
volatile union rw_sys_0056h xdata g_rw_sys_0056h  _at_  (SYS_FIELD_XMEM_BASE + 0x0056);

volatile union rw_sys_0057h xdata g_rw_sys_0057h  _at_  (SYS_FIELD_XMEM_BASE + 0x0057);
volatile union rw_sys_0058h xdata g_rw_sys_0058h  _at_  (SYS_FIELD_XMEM_BASE + 0x0058);
volatile union rw_sys_0059h xdata g_rw_sys_0059h  _at_  (SYS_FIELD_XMEM_BASE + 0x0059);
volatile union rw_sys_005Ah xdata g_rw_sys_005Ah  _at_  (SYS_FIELD_XMEM_BASE + 0x005A);
volatile union rw_sys_005Bh xdata g_rw_sys_005Bh  _at_  (SYS_FIELD_XMEM_BASE + 0x005B);

volatile union rw_sys_005Ch xdata g_rw_sys_005Ch  _at_  (SYS_FIELD_XMEM_BASE + 0x005C);
volatile union rw_sys_005Dh xdata g_rw_sys_005Dh  _at_  (SYS_FIELD_XMEM_BASE + 0x005D);
volatile union rw_sys_005Eh xdata g_rw_sys_005Eh  _at_  (SYS_FIELD_XMEM_BASE + 0x005E);
volatile union rw_sys_005Fh xdata g_rw_sys_005Fh  _at_  (SYS_FIELD_XMEM_BASE + 0x005F);
volatile union rw_sys_0060h xdata g_rw_sys_0060h  _at_  (SYS_FIELD_XMEM_BASE + 0x0060);

volatile union rw_sys_0061h xdata g_rw_sys_0061h  _at_  (SYS_FIELD_XMEM_BASE + 0x0061);
volatile union rw_sys_0062h xdata g_rw_sys_0062h  _at_  (SYS_FIELD_XMEM_BASE + 0x0062);
volatile union rw_sys_0063h xdata g_rw_sys_0063h  _at_  (SYS_FIELD_XMEM_BASE + 0x0063);
volatile union rw_sys_0064h xdata g_rw_sys_0064h  _at_  (SYS_FIELD_XMEM_BASE + 0x0064);
volatile union rw_sys_0065h xdata g_rw_sys_0065h  _at_  (SYS_FIELD_XMEM_BASE + 0x0065);

volatile union rw_sys_0066h xdata g_rw_sys_0066h  _at_  (SYS_FIELD_XMEM_BASE + 0x0066);
volatile union rw_sys_0067h xdata g_rw_sys_0067h  _at_  (SYS_FIELD_XMEM_BASE + 0x0067);
volatile union rw_sys_0068h xdata g_rw_sys_0068h  _at_  (SYS_FIELD_XMEM_BASE + 0x0068);
volatile union rw_sys_0069h xdata g_rw_sys_0069h  _at_  (SYS_FIELD_XMEM_BASE + 0x0069);
volatile union rw_sys_006Ah xdata g_rw_sys_006Ah  _at_  (SYS_FIELD_XMEM_BASE + 0x006A);

volatile union rw_sys_006Bh xdata g_rw_sys_006Bh  _at_  (SYS_FIELD_XMEM_BASE + 0x006B);
volatile union rw_sys_006Ch xdata g_rw_sys_006Ch  _at_  (SYS_FIELD_XMEM_BASE + 0x006C);
volatile union rw_sys_006Dh xdata g_rw_sys_006Dh  _at_  (SYS_FIELD_XMEM_BASE + 0x006D);
volatile union rw_sys_006Eh xdata g_rw_sys_006Eh  _at_  (SYS_FIELD_XMEM_BASE + 0x006E);
volatile union rw_sys_006Fh xdata g_rw_sys_006Fh  _at_  (SYS_FIELD_XMEM_BASE + 0x006F);

volatile union rw_sys_0070h xdata g_rw_sys_0070h  _at_  (SYS_FIELD_XMEM_BASE + 0x0070);
volatile union rw_sys_0071h xdata g_rw_sys_0071h  _at_  (SYS_FIELD_XMEM_BASE + 0x0071);
volatile union rw_sys_0074h xdata g_rw_sys_0074h  _at_  (SYS_FIELD_XMEM_BASE + 0x0074);

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_sys_0004h_reserved6                                                _at_  (SYS_FIELD_XMEM_BASE + 0x0004);
volatile uint8_t xdata g_rw_sys_0005h_reserved7                                                _at_  (SYS_FIELD_XMEM_BASE + 0x0005);
volatile uint8_t xdata g_rw_sys_0018h_reserved9                                                _at_  (SYS_FIELD_XMEM_BASE + 0x0018);
volatile uint8_t xdata g_rw_sys_0019h_reserved10                                               _at_  (SYS_FIELD_XMEM_BASE + 0x0019);
volatile uint8_t xdata g_rw_sys_001Ah_reserved11                                               _at_  (SYS_FIELD_XMEM_BASE + 0x001A);

volatile uint8_t xdata g_rw_sys_001Bh_reserved12                                               _at_  (SYS_FIELD_XMEM_BASE + 0x001B);
volatile uint8_t xdata g_rw_sys_001Ch_reserved13                                               _at_  (SYS_FIELD_XMEM_BASE + 0x001C);
volatile uint8_t xdata g_rw_sys_001Dh_reserved14                                               _at_  (SYS_FIELD_XMEM_BASE + 0x001D);
volatile uint8_t xdata g_rw_sys_0034h_xmem_clk_en                                              _at_  (SYS_FIELD_XMEM_BASE + 0x0034);
volatile uint8_t xdata g_rw_sys_0035h_xmem_clk_en                                              _at_  (SYS_FIELD_XMEM_BASE + 0x0035);

volatile uint8_t xdata g_rw_sys_0036h_xmem_clk_en                                              _at_  (SYS_FIELD_XMEM_BASE + 0x0036);
volatile uint8_t xdata g_rw_sys_0037h_xmem_clk_en                                              _at_  (SYS_FIELD_XMEM_BASE + 0x0037);
volatile uint8_t xdata g_rw_sys_0072h_rx_chksum                                                _at_  (SYS_FIELD_XMEM_BASE + 0x0072);
volatile uint8_t xdata g_rw_sys_0073h_rx_chksum                                                _at_  (SYS_FIELD_XMEM_BASE + 0x0073);
volatile uint8_t xdata g_rw_sys_0075h_reserved56                                               _at_  (SYS_FIELD_XMEM_BASE + 0x0075);

volatile uint8_t xdata g_rw_sys_0076h_reserved57                                               _at_  (SYS_FIELD_XMEM_BASE + 0x0076);
volatile uint8_t xdata g_rw_sys_0077h_reserved58                                               _at_  (SYS_FIELD_XMEM_BASE + 0x0077);
volatile uint8_t xdata g_rw_sys_0078h_reserved59                                               _at_  (SYS_FIELD_XMEM_BASE + 0x0078);

#endif 
