#include "irq_read.h"
#include "hw_mem_map.h"

#ifdef IRQ_READ_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------
volatile union rw_irq_read_0000h xdata g_rw_irq_read_0000h  _at_  (IRQ_READ_FIELD_XMEM_BASE + 0x0000);
volatile union rw_irq_read_0001h xdata g_rw_irq_read_0001h  _at_  (IRQ_READ_FIELD_XMEM_BASE + 0x0001);
volatile union rw_irq_read_0002h xdata g_rw_irq_read_0002h  _at_  (IRQ_READ_FIELD_XMEM_BASE + 0x0002);
volatile union rw_irq_read_0003h xdata g_rw_irq_read_0003h  _at_  (IRQ_READ_FIELD_XMEM_BASE + 0x0003);
volatile union rw_irq_read_000Ch xdata g_rw_irq_read_000Ch  _at_  (IRQ_READ_FIELD_XMEM_BASE + 0x000C);

volatile union rw_irq_read_000Dh xdata g_rw_irq_read_000Dh  _at_  (IRQ_READ_FIELD_XMEM_BASE + 0x000D);

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_irq_read_0004h_reserved2                                           _at_  (IRQ_READ_FIELD_XMEM_BASE + 0x0004);
volatile uint8_t xdata g_rw_irq_read_0005h_reserved2                                           _at_  (IRQ_READ_FIELD_XMEM_BASE + 0x0005);
volatile uint8_t xdata g_rw_irq_read_0006h_reserved2                                           _at_  (IRQ_READ_FIELD_XMEM_BASE + 0x0006);
volatile uint8_t xdata g_rw_irq_read_0007h_reserved2                                           _at_  (IRQ_READ_FIELD_XMEM_BASE + 0x0007);
volatile uint8_t xdata g_rw_irq_read_0008h_reserved2                                           _at_  (IRQ_READ_FIELD_XMEM_BASE + 0x0008);

volatile uint8_t xdata g_rw_irq_read_0009h_irq_patdet_sdpol                                    _at_  (IRQ_READ_FIELD_XMEM_BASE + 0x0009);
volatile uint8_t xdata g_rw_irq_read_000Ah_irq_patdet_sdpol                                    _at_  (IRQ_READ_FIELD_XMEM_BASE + 0x000A);
volatile uint8_t xdata g_rw_irq_read_000Bh_irq_patdet_sdpol                                    _at_  (IRQ_READ_FIELD_XMEM_BASE + 0x000B);

#endif 
