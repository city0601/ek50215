#include "dbg_mux.h"
#include "hw_mem_map.h"

#ifdef DBG_MUX_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------
volatile union rw_dbg_mux_000Ah xdata g_rw_dbg_mux_000Ah  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x000A);
volatile union rw_dbg_mux_000Bh xdata g_rw_dbg_mux_000Bh  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x000B);
volatile union rw_dbg_mux_000Ch xdata g_rw_dbg_mux_000Ch  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x000C);
volatile union rw_dbg_mux_000Dh xdata g_rw_dbg_mux_000Dh  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x000D);
volatile union rw_dbg_mux_000Eh xdata g_rw_dbg_mux_000Eh  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x000E);

volatile union rw_dbg_mux_000Fh xdata g_rw_dbg_mux_000Fh  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x000F);
volatile union rw_dbg_mux_0010h xdata g_rw_dbg_mux_0010h  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0010);
volatile union rw_dbg_mux_0011h xdata g_rw_dbg_mux_0011h  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0011);
volatile union rw_dbg_mux_0012h xdata g_rw_dbg_mux_0012h  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0012);
volatile union rw_dbg_mux_0013h xdata g_rw_dbg_mux_0013h  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0013);

volatile union rw_dbg_mux_0014h xdata g_rw_dbg_mux_0014h  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0014);
volatile union rw_dbg_mux_0015h xdata g_rw_dbg_mux_0015h  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0015);
volatile union rw_dbg_mux_0016h xdata g_rw_dbg_mux_0016h  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0016);
volatile union rw_dbg_mux_0017h xdata g_rw_dbg_mux_0017h  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0017);
volatile union rw_dbg_mux_0018h xdata g_rw_dbg_mux_0018h  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0018);

volatile union rw_dbg_mux_001Ch xdata g_rw_dbg_mux_001Ch  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x001C);
volatile union rw_dbg_mux_001Fh xdata g_rw_dbg_mux_001Fh  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x001F);
volatile union rw_dbg_mux_0020h xdata g_rw_dbg_mux_0020h  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0020);
volatile union rw_dbg_mux_0021h xdata g_rw_dbg_mux_0021h  _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0021);

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_dbg_mux_0000h_sw_rst                                               _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0000);
volatile uint8_t xdata g_rw_dbg_mux_0001h_manual_rstn                                          _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0001);
volatile uint8_t xdata g_rw_dbg_mux_0002h_manual_mcu_rstn                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0002);
volatile uint8_t xdata g_rw_dbg_mux_0003h_manual_rx_rstn                                       _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0003);
volatile uint8_t xdata g_rw_dbg_mux_0004h_manual_dclk_rstn                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0004);

volatile uint8_t xdata g_rw_dbg_mux_0005h_manual_ctrl_rstn                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0005);
volatile uint8_t xdata g_rw_dbg_mux_0006h_manual_rdclk_rstn                                    _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0006);
volatile uint8_t xdata g_rw_dbg_mux_0007h_manual_mclk_rstn                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0007);
volatile uint8_t xdata g_rw_dbg_mux_0008h_test_mode                                            _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0008);
volatile uint8_t xdata g_rw_dbg_mux_0009h_mbist_start                                          _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0009);

volatile uint8_t xdata g_rw_dbg_mux_0019h_dbg_reserve0                                         _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0019);
volatile uint8_t xdata g_rw_dbg_mux_001Ah_dbg_reserve1                                         _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x001A);
volatile uint8_t xdata g_rw_dbg_mux_001Bh_dbg_reserve2                                         _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x001B);
volatile uint8_t xdata g_rw_dbg_mux_001Dh_rx_chksum_dt_i                                       _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x001D);
volatile uint8_t xdata g_rw_dbg_mux_001Eh_rx_chksum_dt_i                                       _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x001E);

volatile uint8_t xdata g_rw_dbg_mux_0022h_rom_misr_dataout                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0022);
volatile uint8_t xdata g_rw_dbg_mux_0023h_rom_misr_dataout                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0023);
volatile uint8_t xdata g_rw_dbg_mux_0024h_rom_misr_dataout                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0024);
volatile uint8_t xdata g_rw_dbg_mux_0025h_rom_misr_dataout                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0025);
volatile uint8_t xdata g_rw_dbg_mux_0026h_rom_misr_dataout                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0026);

volatile uint8_t xdata g_rw_dbg_mux_0027h_rom_misr_dataout                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0027);
volatile uint8_t xdata g_rw_dbg_mux_0028h_rom_misr_dataout                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0028);
volatile uint8_t xdata g_rw_dbg_mux_0029h_rom_misr_dataout                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0029);
volatile uint8_t xdata g_rw_dbg_mux_002Ah_mcu_top_dbgr_1                                       _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x002A);
volatile uint8_t xdata g_rw_dbg_mux_002Bh_mcu_top_dbgr_2                                       _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x002B);

volatile uint8_t xdata g_rw_dbg_mux_002Ch_mcu_top_dbgr_3                                       _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x002C);
volatile uint8_t xdata g_rw_dbg_mux_002Dh_mcu_top_dbgr_4                                       _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x002D);
volatile uint8_t xdata g_rw_dbg_mux_002Eh_mcu_top_dbgr_5                                       _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x002E);
volatile uint8_t xdata g_rw_dbg_mux_002Fh_mcu_top_dbgr_6                                       _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x002F);
volatile uint8_t xdata g_rw_dbg_mux_0030h_mcu_top_dbgr_7                                       _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0030);

volatile uint8_t xdata g_rw_dbg_mux_0031h_mcu_top_dbgr_8                                       _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0031);
volatile uint8_t xdata g_rw_dbg_mux_0032h_mcu_top_dbgr_9                                       _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0032);
volatile uint8_t xdata g_rw_dbg_mux_0033h_mcu_top_dbgr_10                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0033);
volatile uint8_t xdata g_rw_dbg_mux_0034h_mcu_top_dbgr_11                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0034);
volatile uint8_t xdata g_rw_dbg_mux_0035h_mcu_top_dbgr_12                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0035);

volatile uint8_t xdata g_rw_dbg_mux_0036h_mcu_top_dbgr_13                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0036);
volatile uint8_t xdata g_rw_dbg_mux_0037h_mcu_top_dbgr_14                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0037);
volatile uint8_t xdata g_rw_dbg_mux_0038h_mcu_top_dbgr_15                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0038);
volatile uint8_t xdata g_rw_dbg_mux_0039h_mcu_top_dbgr_16                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0039);
volatile uint8_t xdata g_rw_dbg_mux_003Ah_mcu_top_dbgr_17                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x003A);

volatile uint8_t xdata g_rw_dbg_mux_003Bh_mcu_top_dbgr_18                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x003B);
volatile uint8_t xdata g_rw_dbg_mux_003Ch_mcu_top_dbgr_19                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x003C);
volatile uint8_t xdata g_rw_dbg_mux_003Dh_mcu_top_dbgr_20                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x003D);
volatile uint8_t xdata g_rw_dbg_mux_003Eh_mcu_top_dbgr_21                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x003E);
volatile uint8_t xdata g_rw_dbg_mux_003Fh_mcu_top_dbgr_22                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x003F);

volatile uint8_t xdata g_rw_dbg_mux_0040h_mcu_top_dbgr_23                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0040);
volatile uint8_t xdata g_rw_dbg_mux_0041h_mcu_top_dbgr_24                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0041);
volatile uint8_t xdata g_rw_dbg_mux_0042h_indbgr_0                                             _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0042);
volatile uint8_t xdata g_rw_dbg_mux_0043h_indbgr_1                                             _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0043);
volatile uint8_t xdata g_rw_dbg_mux_0044h_indbgr_2                                             _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0044);

volatile uint8_t xdata g_rw_dbg_mux_0045h_indbgr_3                                             _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0045);
volatile uint8_t xdata g_rw_dbg_mux_0046h_indbgr_4                                             _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0046);
volatile uint8_t xdata g_rw_dbg_mux_0047h_indbgr_5                                             _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0047);
volatile uint8_t xdata g_rw_dbg_mux_0048h_indbgr_6                                             _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0048);
volatile uint8_t xdata g_rw_dbg_mux_0049h_indbgr_7                                             _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0049);

volatile uint8_t xdata g_rw_dbg_mux_004Ah_indbgr_8                                             _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x004A);
volatile uint8_t xdata g_rw_dbg_mux_004Bh_indbgr_9                                             _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x004B);
volatile uint8_t xdata g_rw_dbg_mux_004Ch_indbgr_10                                            _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x004C);
volatile uint8_t xdata g_rw_dbg_mux_004Dh_indbgr_11                                            _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x004D);
volatile uint8_t xdata g_rw_dbg_mux_004Eh_indbgr_12                                            _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x004E);

volatile uint8_t xdata g_rw_dbg_mux_004Fh_indbgr_13                                            _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x004F);
volatile uint8_t xdata g_rw_dbg_mux_0050h_indbgr_14                                            _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0050);
volatile uint8_t xdata g_rw_dbg_mux_0051h_indbgr_15                                            _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0051);
volatile uint8_t xdata g_rw_dbg_mux_0052h_indbgr_16                                            _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0052);
volatile uint8_t xdata g_rw_dbg_mux_0053h_indbgr_17                                            _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0053);

volatile uint8_t xdata g_rw_dbg_mux_0054h_aging_dbgr_0                                         _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0054);
volatile uint8_t xdata g_rw_dbg_mux_0055h_aging_dbgr_1                                         _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0055);
volatile uint8_t xdata g_rw_dbg_mux_0056h_aging_dbgr_2                                         _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0056);
volatile uint8_t xdata g_rw_dbg_mux_0057h_aging_dbgr_3                                         _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0057);
volatile uint8_t xdata g_rw_dbg_mux_0058h_aging_dbgr_4                                         _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0058);

volatile uint8_t xdata g_rw_dbg_mux_0059h_aging_dbgr_5                                         _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0059);
volatile uint8_t xdata g_rw_dbg_mux_005Ah_aging_dbgr_6                                         _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x005A);
volatile uint8_t xdata g_rw_dbg_mux_005Bh_aging_dbgr_7                                         _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x005B);
volatile uint8_t xdata g_rw_dbg_mux_005Ch_patdet_dbgr_0                                        _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x005C);
volatile uint8_t xdata g_rw_dbg_mux_005Dh_dataflow_dbgr_0                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x005D);

volatile uint8_t xdata g_rw_dbg_mux_005Eh_dataflow_dbgr_1                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x005E);
volatile uint8_t xdata g_rw_dbg_mux_005Fh_dataflow_dbgr_2                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x005F);
volatile uint8_t xdata g_rw_dbg_mux_0060h_dataflow_dbgr_3                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0060);
volatile uint8_t xdata g_rw_dbg_mux_0061h_dataflow_dbgr_4                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0061);
volatile uint8_t xdata g_rw_dbg_mux_0062h_dataflow_dbgr_5                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0062);

volatile uint8_t xdata g_rw_dbg_mux_0063h_dataflow_dbgr_6                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0063);
volatile uint8_t xdata g_rw_dbg_mux_0064h_dataflow_dbgr_7                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0064);
volatile uint8_t xdata g_rw_dbg_mux_0065h_dataflow_dbgr_8                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0065);
volatile uint8_t xdata g_rw_dbg_mux_0066h_dataflow_dbgr_9                                      _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0066);
volatile uint8_t xdata g_rw_dbg_mux_0067h_dataflow_dbgr_10                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0067);

volatile uint8_t xdata g_rw_dbg_mux_0068h_dataflow_dbgr_11                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0068);
volatile uint8_t xdata g_rw_dbg_mux_0069h_dataflow_dbgr_12                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0069);
volatile uint8_t xdata g_rw_dbg_mux_006Ah_dataflow_dbgr_13                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x006A);
volatile uint8_t xdata g_rw_dbg_mux_006Bh_dataflow_dbgr_14                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x006B);
volatile uint8_t xdata g_rw_dbg_mux_006Ch_dataflow_dbgr_15                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x006C);

volatile uint8_t xdata g_rw_dbg_mux_006Dh_dataflow_dbgr_16                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x006D);
volatile uint8_t xdata g_rw_dbg_mux_006Eh_outdbgr_0                                            _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x006E);
volatile uint8_t xdata g_rw_dbg_mux_006Fh_outdbgr_1                                            _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x006F);
volatile uint8_t xdata g_rw_dbg_mux_0070h_outdbgr_2                                            _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0070);
volatile uint8_t xdata g_rw_dbg_mux_0071h_outdbgr_3                                            _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0071);

volatile uint8_t xdata g_rw_dbg_mux_0072h_outdbgr_4                                            _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0072);
volatile uint8_t xdata g_rw_dbg_mux_0073h_outdbgr_5                                            _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0073);
volatile uint8_t xdata g_rw_dbg_mux_0074h_outdbgr_6                                            _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0074);
volatile uint8_t xdata g_rw_dbg_mux_0075h_outdbgr_7                                            _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0075);
volatile uint8_t xdata g_rw_dbg_mux_0076h_p2p_dbgr_0                                           _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0076);

volatile uint8_t xdata g_rw_dbg_mux_0077h_p2p_dbgr_1                                           _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0077);
volatile uint8_t xdata g_rw_dbg_mux_0078h_p2p_dbgr_2                                           _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0078);
volatile uint8_t xdata g_rw_dbg_mux_0079h_p2p_dbgr_3                                           _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0079);
volatile uint8_t xdata g_rw_dbg_mux_007Ah_p2p_dbgr_4                                           _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x007A);
volatile uint8_t xdata g_rw_dbg_mux_007Bh_p2p_dbgr_5                                           _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x007B);

volatile uint8_t xdata g_rw_dbg_mux_007Ch_p2p_dbgr_6                                           _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x007C);
volatile uint8_t xdata g_rw_dbg_mux_007Dh_p2p_dbgr_7                                           _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x007D);
volatile uint8_t xdata g_rw_dbg_mux_007Eh_ctrl_signal_dbgr_0                                   _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x007E);
volatile uint8_t xdata g_rw_dbg_mux_007Fh_ctrl_signal_dbgr_1                                   _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x007F);
volatile uint8_t xdata g_rw_dbg_mux_0080h_ctrl_signal_dbgr_2                                   _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0080);

volatile uint8_t xdata g_rw_dbg_mux_0081h_ctrl_signal_dbgr_3                                   _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0081);
volatile uint8_t xdata g_rw_dbg_mux_0082h_ctrl_signal_dbgr_4                                   _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0082);
volatile uint8_t xdata g_rw_dbg_mux_0083h_ctrl_signal_dbgr_5                                   _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0083);
volatile uint8_t xdata g_rw_dbg_mux_0084h_ctrl_signal_dbgr_6                                   _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0084);
volatile uint8_t xdata g_rw_dbg_mux_0085h_ctrl_signal_dbgr_7                                   _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0085);

volatile uint8_t xdata g_rw_dbg_mux_0086h_aip_dbgr_0                                           _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0086);
volatile uint8_t xdata g_rw_dbg_mux_0087h_aip_dbgr_1                                           _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0087);
volatile uint8_t xdata g_rw_dbg_mux_0088h_aip_dbgr_2                                           _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0088);
volatile uint8_t xdata g_rw_dbg_mux_0089h_aip_dbgr_3                                           _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x0089);
volatile uint8_t xdata g_rw_dbg_mux_008Ah_aip_dbgr_4                                           _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x008A);

volatile uint8_t xdata g_rw_dbg_mux_008Bh_ro_dbgr_reserve0                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x008B);
volatile uint8_t xdata g_rw_dbg_mux_008Ch_ro_dbgr_reserve1                                     _at_  (DBG_MUX_FIELD_XMEM_BASE + 0x008C);

#endif 
