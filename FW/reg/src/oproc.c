#include "oproc.h"
#include "hw_mem_map.h"

#ifdef OPROC_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------
volatile union rw_oproc_0000h xdata g_rw_oproc_0000h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0000);
volatile union rw_oproc_0001h xdata g_rw_oproc_0001h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0001);
volatile union rw_oproc_0002h xdata g_rw_oproc_0002h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0002);
volatile union rw_oproc_0003h xdata g_rw_oproc_0003h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0003);
volatile union rw_oproc_0004h xdata g_rw_oproc_0004h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0004);

volatile union rw_oproc_0005h xdata g_rw_oproc_0005h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0005);
volatile union rw_oproc_0006h xdata g_rw_oproc_0006h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0006);
volatile union rw_oproc_0008h xdata g_rw_oproc_0008h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0008);
volatile union rw_oproc_0009h xdata g_rw_oproc_0009h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0009);
volatile union rw_oproc_000Ah xdata g_rw_oproc_000Ah  _at_  (OPROC_FIELD_XMEM_BASE + 0x000A);

volatile union rw_oproc_000Bh xdata g_rw_oproc_000Bh  _at_  (OPROC_FIELD_XMEM_BASE + 0x000B);
volatile union rw_oproc_000Dh xdata g_rw_oproc_000Dh  _at_  (OPROC_FIELD_XMEM_BASE + 0x000D);
volatile union rw_oproc_000Eh xdata g_rw_oproc_000Eh  _at_  (OPROC_FIELD_XMEM_BASE + 0x000E);
volatile union rw_oproc_0010h xdata g_rw_oproc_0010h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0010);
volatile union rw_oproc_0011h xdata g_rw_oproc_0011h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0011);

volatile union rw_oproc_0012h xdata g_rw_oproc_0012h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0012);
volatile union rw_oproc_0013h xdata g_rw_oproc_0013h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0013);
volatile union rw_oproc_0014h xdata g_rw_oproc_0014h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0014);
volatile union rw_oproc_0015h xdata g_rw_oproc_0015h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0015);
volatile union rw_oproc_0016h xdata g_rw_oproc_0016h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0016);

volatile union rw_oproc_0017h xdata g_rw_oproc_0017h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0017);
volatile union rw_oproc_0018h xdata g_rw_oproc_0018h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0018);
volatile union rw_oproc_0019h xdata g_rw_oproc_0019h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0019);
volatile union rw_oproc_001Ah xdata g_rw_oproc_001Ah  _at_  (OPROC_FIELD_XMEM_BASE + 0x001A);
volatile union rw_oproc_001Bh xdata g_rw_oproc_001Bh  _at_  (OPROC_FIELD_XMEM_BASE + 0x001B);

volatile union rw_oproc_001Ch xdata g_rw_oproc_001Ch  _at_  (OPROC_FIELD_XMEM_BASE + 0x001C);
volatile union rw_oproc_001Dh xdata g_rw_oproc_001Dh  _at_  (OPROC_FIELD_XMEM_BASE + 0x001D);
volatile union rw_oproc_001Eh xdata g_rw_oproc_001Eh  _at_  (OPROC_FIELD_XMEM_BASE + 0x001E);
volatile union rw_oproc_001Fh xdata g_rw_oproc_001Fh  _at_  (OPROC_FIELD_XMEM_BASE + 0x001F);
volatile union rw_oproc_0020h xdata g_rw_oproc_0020h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0020);

volatile union rw_oproc_0021h xdata g_rw_oproc_0021h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0021);
volatile union rw_oproc_0022h xdata g_rw_oproc_0022h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0022);
volatile union rw_oproc_0023h xdata g_rw_oproc_0023h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0023);
volatile union rw_oproc_0024h xdata g_rw_oproc_0024h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0024);
volatile union rw_oproc_0025h xdata g_rw_oproc_0025h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0025);

volatile union rw_oproc_0026h xdata g_rw_oproc_0026h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0026);
volatile union rw_oproc_0027h xdata g_rw_oproc_0027h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0027);
volatile union rw_oproc_0028h xdata g_rw_oproc_0028h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0028);
volatile union rw_oproc_0029h xdata g_rw_oproc_0029h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0029);
volatile union rw_oproc_002Ah xdata g_rw_oproc_002Ah  _at_  (OPROC_FIELD_XMEM_BASE + 0x002A);

volatile union rw_oproc_002Bh xdata g_rw_oproc_002Bh  _at_  (OPROC_FIELD_XMEM_BASE + 0x002B);
volatile union rw_oproc_002Ch xdata g_rw_oproc_002Ch  _at_  (OPROC_FIELD_XMEM_BASE + 0x002C);
volatile union rw_oproc_002Dh xdata g_rw_oproc_002Dh  _at_  (OPROC_FIELD_XMEM_BASE + 0x002D);
volatile union rw_oproc_002Eh xdata g_rw_oproc_002Eh  _at_  (OPROC_FIELD_XMEM_BASE + 0x002E);
volatile union rw_oproc_002Fh xdata g_rw_oproc_002Fh  _at_  (OPROC_FIELD_XMEM_BASE + 0x002F);

volatile union rw_oproc_0030h xdata g_rw_oproc_0030h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0030);
volatile union rw_oproc_0032h xdata g_rw_oproc_0032h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0032);
volatile union rw_oproc_0033h xdata g_rw_oproc_0033h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0033);
volatile union rw_oproc_0034h xdata g_rw_oproc_0034h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0034);
volatile union rw_oproc_0035h xdata g_rw_oproc_0035h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0035);

volatile union rw_oproc_0036h xdata g_rw_oproc_0036h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0036);
volatile union rw_oproc_0037h xdata g_rw_oproc_0037h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0037);
volatile union rw_oproc_0038h xdata g_rw_oproc_0038h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0038);
volatile union rw_oproc_0039h xdata g_rw_oproc_0039h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0039);
volatile union rw_oproc_003Bh xdata g_rw_oproc_003Bh  _at_  (OPROC_FIELD_XMEM_BASE + 0x003B);

volatile union rw_oproc_003Dh xdata g_rw_oproc_003Dh  _at_  (OPROC_FIELD_XMEM_BASE + 0x003D);
volatile union rw_oproc_003Eh xdata g_rw_oproc_003Eh  _at_  (OPROC_FIELD_XMEM_BASE + 0x003E);
volatile union rw_oproc_0040h xdata g_rw_oproc_0040h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0040);
volatile union rw_oproc_0041h xdata g_rw_oproc_0041h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0041);
volatile union rw_oproc_0042h xdata g_rw_oproc_0042h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0042);

volatile union rw_oproc_0044h xdata g_rw_oproc_0044h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0044);
volatile union rw_oproc_0047h xdata g_rw_oproc_0047h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0047);
volatile union rw_oproc_004Ah xdata g_rw_oproc_004Ah  _at_  (OPROC_FIELD_XMEM_BASE + 0x004A);
volatile union rw_oproc_004Ch xdata g_rw_oproc_004Ch  _at_  (OPROC_FIELD_XMEM_BASE + 0x004C);
volatile union rw_oproc_004Dh xdata g_rw_oproc_004Dh  _at_  (OPROC_FIELD_XMEM_BASE + 0x004D);

volatile union rw_oproc_004Eh xdata g_rw_oproc_004Eh  _at_  (OPROC_FIELD_XMEM_BASE + 0x004E);
volatile union rw_oproc_004Fh xdata g_rw_oproc_004Fh  _at_  (OPROC_FIELD_XMEM_BASE + 0x004F);
volatile union rw_oproc_0050h xdata g_rw_oproc_0050h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0050);
volatile union rw_oproc_0051h xdata g_rw_oproc_0051h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0051);
volatile union rw_oproc_0052h xdata g_rw_oproc_0052h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0052);

volatile union rw_oproc_0054h xdata g_rw_oproc_0054h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0054);
volatile union rw_oproc_0055h xdata g_rw_oproc_0055h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0055);
volatile union rw_oproc_0056h xdata g_rw_oproc_0056h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0056);
volatile union rw_oproc_0057h xdata g_rw_oproc_0057h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0057);
volatile union rw_oproc_0059h xdata g_rw_oproc_0059h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0059);

volatile union rw_oproc_005Ah xdata g_rw_oproc_005Ah  _at_  (OPROC_FIELD_XMEM_BASE + 0x005A);
volatile union rw_oproc_005Bh xdata g_rw_oproc_005Bh  _at_  (OPROC_FIELD_XMEM_BASE + 0x005B);
volatile union rw_oproc_005Eh xdata g_rw_oproc_005Eh  _at_  (OPROC_FIELD_XMEM_BASE + 0x005E);
volatile union rw_oproc_005Fh xdata g_rw_oproc_005Fh  _at_  (OPROC_FIELD_XMEM_BASE + 0x005F);
volatile union rw_oproc_0060h xdata g_rw_oproc_0060h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0060);

volatile union rw_oproc_0063h xdata g_rw_oproc_0063h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0063);
volatile union rw_oproc_0065h xdata g_rw_oproc_0065h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0065);
volatile union rw_oproc_0067h xdata g_rw_oproc_0067h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0067);
volatile union rw_oproc_0069h xdata g_rw_oproc_0069h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0069);
volatile union rw_oproc_006Ch xdata g_rw_oproc_006Ch  _at_  (OPROC_FIELD_XMEM_BASE + 0x006C);

volatile union rw_oproc_006Fh xdata g_rw_oproc_006Fh  _at_  (OPROC_FIELD_XMEM_BASE + 0x006F);
volatile union rw_oproc_0072h xdata g_rw_oproc_0072h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0072);
volatile union rw_oproc_0075h xdata g_rw_oproc_0075h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0075);
volatile union rw_oproc_0078h xdata g_rw_oproc_0078h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0078);
volatile union rw_oproc_007Bh xdata g_rw_oproc_007Bh  _at_  (OPROC_FIELD_XMEM_BASE + 0x007B);

volatile union rw_oproc_007Ch xdata g_rw_oproc_007Ch  _at_  (OPROC_FIELD_XMEM_BASE + 0x007C);
volatile union rw_oproc_007Dh xdata g_rw_oproc_007Dh  _at_  (OPROC_FIELD_XMEM_BASE + 0x007D);
volatile union rw_oproc_007Eh xdata g_rw_oproc_007Eh  _at_  (OPROC_FIELD_XMEM_BASE + 0x007E);
volatile union rw_oproc_007Fh xdata g_rw_oproc_007Fh  _at_  (OPROC_FIELD_XMEM_BASE + 0x007F);
volatile union rw_oproc_0080h xdata g_rw_oproc_0080h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0080);

volatile union rw_oproc_0081h xdata g_rw_oproc_0081h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0081);
volatile union rw_oproc_0082h xdata g_rw_oproc_0082h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0082);
volatile union rw_oproc_0083h xdata g_rw_oproc_0083h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0083);
volatile union rw_oproc_0084h xdata g_rw_oproc_0084h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0084);
volatile union rw_oproc_0085h xdata g_rw_oproc_0085h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0085);

volatile union rw_oproc_0086h xdata g_rw_oproc_0086h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0086);
volatile union rw_oproc_0087h xdata g_rw_oproc_0087h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0087);
volatile union rw_oproc_0088h xdata g_rw_oproc_0088h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0088);
volatile union rw_oproc_0089h xdata g_rw_oproc_0089h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0089);
volatile union rw_oproc_008Ah xdata g_rw_oproc_008Ah  _at_  (OPROC_FIELD_XMEM_BASE + 0x008A);

volatile union rw_oproc_008Bh xdata g_rw_oproc_008Bh  _at_  (OPROC_FIELD_XMEM_BASE + 0x008B);
volatile union rw_oproc_008Ch xdata g_rw_oproc_008Ch  _at_  (OPROC_FIELD_XMEM_BASE + 0x008C);
volatile union rw_oproc_008Dh xdata g_rw_oproc_008Dh  _at_  (OPROC_FIELD_XMEM_BASE + 0x008D);
volatile union rw_oproc_008Eh xdata g_rw_oproc_008Eh  _at_  (OPROC_FIELD_XMEM_BASE + 0x008E);
volatile union rw_oproc_008Fh xdata g_rw_oproc_008Fh  _at_  (OPROC_FIELD_XMEM_BASE + 0x008F);

volatile union rw_oproc_0090h xdata g_rw_oproc_0090h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0090);
volatile union rw_oproc_0091h xdata g_rw_oproc_0091h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0091);
volatile union rw_oproc_0092h xdata g_rw_oproc_0092h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0092);
volatile union rw_oproc_0093h xdata g_rw_oproc_0093h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0093);
volatile union rw_oproc_0094h xdata g_rw_oproc_0094h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0094);

volatile union rw_oproc_0095h xdata g_rw_oproc_0095h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0095);
volatile union rw_oproc_0096h xdata g_rw_oproc_0096h  _at_  (OPROC_FIELD_XMEM_BASE + 0x0096);

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_oproc_0007h_lbuf_rd_hdisp                                          _at_  (OPROC_FIELD_XMEM_BASE + 0x0007);
volatile uint8_t xdata g_rw_oproc_000Ch_a_start_pix                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x000C);
volatile uint8_t xdata g_rw_oproc_000Fh_lbuf_addr_max                                          _at_  (OPROC_FIELD_XMEM_BASE + 0x000F);
volatile uint8_t xdata g_rw_oproc_0031h_htot_rec                                               _at_  (OPROC_FIELD_XMEM_BASE + 0x0031);
volatile uint8_t xdata g_rw_oproc_003Ah_frame_inv_num2                                         _at_  (OPROC_FIELD_XMEM_BASE + 0x003A);

volatile uint8_t xdata g_rw_oproc_003Ch_htot_cmp_reg_min                                       _at_  (OPROC_FIELD_XMEM_BASE + 0x003C);
volatile uint8_t xdata g_rw_oproc_003Fh_dym_sft_num                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x003F);
volatile uint8_t xdata g_rw_oproc_0043h_c_start_pix                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x0043);
volatile uint8_t xdata g_rw_oproc_0045h_d_start_pix                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x0045);
volatile uint8_t xdata g_rw_oproc_0046h_e_start_pix                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x0046);

volatile uint8_t xdata g_rw_oproc_0048h_f_start_pix                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x0048);
volatile uint8_t xdata g_rw_oproc_0049h_g_start_pix                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x0049);
volatile uint8_t xdata g_rw_oproc_004Bh_h_start_pix                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x004B);
volatile uint8_t xdata g_rw_oproc_0053h_measure_dt_2                                           _at_  (OPROC_FIELD_XMEM_BASE + 0x0053);
volatile uint8_t xdata g_rw_oproc_0058h_measure_dt_6                                           _at_  (OPROC_FIELD_XMEM_BASE + 0x0058);

volatile uint8_t xdata g_rw_oproc_005Ch_test_data_g                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x005C);
volatile uint8_t xdata g_rw_oproc_005Dh_test_data_b                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x005D);
volatile uint8_t xdata g_rw_oproc_0061h_tpl_data_st                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x0061);
volatile uint8_t xdata g_rw_oproc_0062h_tph_data_st1                                           _at_  (OPROC_FIELD_XMEM_BASE + 0x0062);
volatile uint8_t xdata g_rw_oproc_0064h_tph_data_st2                                           _at_  (OPROC_FIELD_XMEM_BASE + 0x0064);

volatile uint8_t xdata g_rw_oproc_0066h_htot_ctrl_rec                                          _at_  (OPROC_FIELD_XMEM_BASE + 0x0066);
volatile uint8_t xdata g_rw_oproc_0068h_driv1_point                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x0068);
volatile uint8_t xdata g_rw_oproc_006Ah_driv2_point                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x006A);
volatile uint8_t xdata g_rw_oproc_006Bh_driv3_point                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x006B);
volatile uint8_t xdata g_rw_oproc_006Dh_driv4_point                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x006D);

volatile uint8_t xdata g_rw_oproc_006Eh_driv5_point                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x006E);
volatile uint8_t xdata g_rw_oproc_0070h_driv6_point                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x0070);
volatile uint8_t xdata g_rw_oproc_0071h_driv7_point                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x0071);
volatile uint8_t xdata g_rw_oproc_0073h_driv8_point                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x0073);
volatile uint8_t xdata g_rw_oproc_0074h_driv9_point                                            _at_  (OPROC_FIELD_XMEM_BASE + 0x0074);

volatile uint8_t xdata g_rw_oproc_0076h_driv10_point                                           _at_  (OPROC_FIELD_XMEM_BASE + 0x0076);
volatile uint8_t xdata g_rw_oproc_0077h_driv11_point                                           _at_  (OPROC_FIELD_XMEM_BASE + 0x0077);
volatile uint8_t xdata g_rw_oproc_0079h_driv12_point                                           _at_  (OPROC_FIELD_XMEM_BASE + 0x0079);
volatile uint8_t xdata g_rw_oproc_007Ah_end_point                                              _at_  (OPROC_FIELD_XMEM_BASE + 0x007A);

#endif 
