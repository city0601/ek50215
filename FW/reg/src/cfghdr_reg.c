#include "cfghdr_reg.h"
#include "hw_mem_map.h"

#ifdef CFGHDR_REG_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_cfghdr_reg_0000h_cfg_reserved0                                     _at_  (CFGHDR_REG_FIELD_XMEM_BASE + 0x0000);
volatile uint8_t xdata g_rw_cfghdr_reg_0001h_cfg_reserved1                                     _at_  (CFGHDR_REG_FIELD_XMEM_BASE + 0x0001);

#endif 
