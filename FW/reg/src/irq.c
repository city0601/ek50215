#include "irq.h"
#include "hw_mem_map.h"

#ifdef IRQ_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------
volatile union rw_irq_0001h xdata g_rw_irq_0001h  _at_  (IRQ_FIELD_XMEM_BASE + 0x0001);
volatile union rw_irq_0002h xdata g_rw_irq_0002h  _at_  (IRQ_FIELD_XMEM_BASE + 0x0002);
volatile union rw_irq_0005h xdata g_rw_irq_0005h  _at_  (IRQ_FIELD_XMEM_BASE + 0x0005);
volatile union rw_irq_0008h xdata g_rw_irq_0008h  _at_  (IRQ_FIELD_XMEM_BASE + 0x0008);
volatile union rw_irq_000Bh xdata g_rw_irq_000Bh  _at_  (IRQ_FIELD_XMEM_BASE + 0x000B);

volatile union rw_irq_000Eh xdata g_rw_irq_000Eh  _at_  (IRQ_FIELD_XMEM_BASE + 0x000E);
volatile union rw_irq_0011h xdata g_rw_irq_0011h  _at_  (IRQ_FIELD_XMEM_BASE + 0x0011);
volatile union rw_irq_0012h xdata g_rw_irq_0012h  _at_  (IRQ_FIELD_XMEM_BASE + 0x0012);
volatile union rw_irq_0013h xdata g_rw_irq_0013h  _at_  (IRQ_FIELD_XMEM_BASE + 0x0013);
volatile union rw_irq_0014h xdata g_rw_irq_0014h  _at_  (IRQ_FIELD_XMEM_BASE + 0x0014);


//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_irq_0000h_vx1_sym_thr                                              _at_  (IRQ_FIELD_XMEM_BASE + 0x0000);
volatile uint8_t xdata g_rw_irq_0003h_inde_cnt1                                                _at_  (IRQ_FIELD_XMEM_BASE + 0x0003);
volatile uint8_t xdata g_rw_irq_0004h_inpix_cnt1                                               _at_  (IRQ_FIELD_XMEM_BASE + 0x0004);
volatile uint8_t xdata g_rw_irq_0006h_inde_cnt2                                                _at_  (IRQ_FIELD_XMEM_BASE + 0x0006);
volatile uint8_t xdata g_rw_irq_0007h_inpix_cnt2                                               _at_  (IRQ_FIELD_XMEM_BASE + 0x0007);

volatile uint8_t xdata g_rw_irq_0009h_outde_cnt1                                               _at_  (IRQ_FIELD_XMEM_BASE + 0x0009);
volatile uint8_t xdata g_rw_irq_000Ah_outpix_cnt1                                              _at_  (IRQ_FIELD_XMEM_BASE + 0x000A);
volatile uint8_t xdata g_rw_irq_000Ch_outde_cnt2                                               _at_  (IRQ_FIELD_XMEM_BASE + 0x000C);
volatile uint8_t xdata g_rw_irq_000Dh_outpix_cnt2                                              _at_  (IRQ_FIELD_XMEM_BASE + 0x000D);
volatile uint8_t xdata g_rw_irq_000Fh_frm_cnt1                                                 _at_  (IRQ_FIELD_XMEM_BASE + 0x000F);

volatile uint8_t xdata g_rw_irq_0010h_frm_cnt2                                                 _at_  (IRQ_FIELD_XMEM_BASE + 0x0010);

#endif 
