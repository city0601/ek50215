#include "gamvrr0.h"
#include "hw_mem_map.h"

#ifdef GAMVRR0_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_gamvrr0_0000h_GAM_VRR_LUT_OFST12_0_new_data_0                      _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0000);
volatile uint8_t xdata g_rw_gamvrr0_0001h_GAM_VRR_LUT_OFST12_0_new_data_1                      _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0001);
volatile uint8_t xdata g_rw_gamvrr0_0002h_GAM_VRR_LUT_OFST12_0_new_data_2                      _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0002);
volatile uint8_t xdata g_rw_gamvrr0_0003h_GAM_VRR_LUT_OFST12_0_new_data_3                      _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0003);
volatile uint8_t xdata g_rw_gamvrr0_0004h_GAM_VRR_LUT_OFST12_0_new_data_4                      _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0004);

volatile uint8_t xdata g_rw_gamvrr0_0005h_GAM_VRR_LUT_OFST12_0_new_data_5                      _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0005);
volatile uint8_t xdata g_rw_gamvrr0_0006h_GAM_VRR_LUT_OFST12_0_new_data_6                      _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0006);
volatile uint8_t xdata g_rw_gamvrr0_0007h_GAM_VRR_LUT_OFST12_0_new_data_7                      _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0007);
volatile uint8_t xdata g_rw_gamvrr0_0008h_GAM_VRR_LUT_OFST12_0_new_data_8                      _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0008);
volatile uint8_t xdata g_rw_gamvrr0_0009h_GAM_VRR_LUT_OFST12_0_new_data_9                      _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0009);

volatile uint8_t xdata g_rw_gamvrr0_000Ah_GAM_VRR_LUT_OFST12_0_new_data_10                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x000A);
volatile uint8_t xdata g_rw_gamvrr0_000Bh_GAM_VRR_LUT_OFST12_0_new_data_11                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x000B);
volatile uint8_t xdata g_rw_gamvrr0_000Ch_GAM_VRR_LUT_OFST12_0_new_data_12                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x000C);
volatile uint8_t xdata g_rw_gamvrr0_000Dh_GAM_VRR_LUT_OFST12_0_new_data_13                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x000D);
volatile uint8_t xdata g_rw_gamvrr0_000Eh_GAM_VRR_LUT_OFST12_0_new_data_14                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x000E);

volatile uint8_t xdata g_rw_gamvrr0_000Fh_GAM_VRR_LUT_OFST12_0_new_data_15                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x000F);
volatile uint8_t xdata g_rw_gamvrr0_0010h_GAM_VRR_LUT_OFST12_0_new_data_16                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0010);
volatile uint8_t xdata g_rw_gamvrr0_0011h_GAM_VRR_LUT_OFST12_0_new_data_17                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0011);
volatile uint8_t xdata g_rw_gamvrr0_0012h_GAM_VRR_LUT_OFST12_0_new_data_18                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0012);
volatile uint8_t xdata g_rw_gamvrr0_0013h_GAM_VRR_LUT_OFST12_0_new_data_19                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0013);

volatile uint8_t xdata g_rw_gamvrr0_0014h_GAM_VRR_LUT_OFST12_0_new_data_20                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0014);
volatile uint8_t xdata g_rw_gamvrr0_0015h_GAM_VRR_LUT_OFST12_0_new_data_21                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0015);
volatile uint8_t xdata g_rw_gamvrr0_0016h_GAM_VRR_LUT_OFST12_0_new_data_22                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0016);
volatile uint8_t xdata g_rw_gamvrr0_0017h_GAM_VRR_LUT_OFST12_0_new_data_23                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0017);
volatile uint8_t xdata g_rw_gamvrr0_0018h_GAM_VRR_LUT_OFST12_0_new_data_24                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0018);

volatile uint8_t xdata g_rw_gamvrr0_0019h_GAM_VRR_LUT_OFST12_0_new_data_25                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0019);
volatile uint8_t xdata g_rw_gamvrr0_001Ah_GAM_VRR_LUT_OFST12_0_new_data_26                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x001A);
volatile uint8_t xdata g_rw_gamvrr0_001Bh_GAM_VRR_LUT_OFST12_0_new_data_27                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x001B);
volatile uint8_t xdata g_rw_gamvrr0_001Ch_GAM_VRR_LUT_OFST12_0_new_data_28                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x001C);
volatile uint8_t xdata g_rw_gamvrr0_001Dh_GAM_VRR_LUT_OFST12_0_new_data_29                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x001D);

volatile uint8_t xdata g_rw_gamvrr0_001Eh_GAM_VRR_LUT_OFST12_0_new_data_30                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x001E);
volatile uint8_t xdata g_rw_gamvrr0_001Fh_GAM_VRR_LUT_OFST12_0_new_data_31                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x001F);
volatile uint8_t xdata g_rw_gamvrr0_0020h_GAM_VRR_LUT_OFST12_0_new_data_32                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0020);
volatile uint8_t xdata g_rw_gamvrr0_0021h_GAM_VRR_LUT_OFST12_0_new_data_33                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0021);
volatile uint8_t xdata g_rw_gamvrr0_0022h_GAM_VRR_LUT_OFST12_0_new_data_34                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0022);

volatile uint8_t xdata g_rw_gamvrr0_0023h_GAM_VRR_LUT_OFST12_0_new_data_35                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0023);
volatile uint8_t xdata g_rw_gamvrr0_0024h_GAM_VRR_LUT_OFST12_0_new_data_36                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0024);
volatile uint8_t xdata g_rw_gamvrr0_0025h_GAM_VRR_LUT_OFST12_0_new_data_37                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0025);
volatile uint8_t xdata g_rw_gamvrr0_0026h_GAM_VRR_LUT_OFST12_0_new_data_38                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0026);
volatile uint8_t xdata g_rw_gamvrr0_0027h_GAM_VRR_LUT_OFST12_0_new_data_39                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0027);

volatile uint8_t xdata g_rw_gamvrr0_0028h_GAM_VRR_LUT_OFST12_0_new_data_40                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0028);
volatile uint8_t xdata g_rw_gamvrr0_0029h_GAM_VRR_LUT_OFST12_0_new_data_41                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0029);
volatile uint8_t xdata g_rw_gamvrr0_002Ah_GAM_VRR_LUT_OFST12_0_new_data_42                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x002A);
volatile uint8_t xdata g_rw_gamvrr0_002Bh_GAM_VRR_LUT_OFST12_0_new_data_43                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x002B);
volatile uint8_t xdata g_rw_gamvrr0_002Ch_GAM_VRR_LUT_OFST12_0_new_data_44                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x002C);

volatile uint8_t xdata g_rw_gamvrr0_002Dh_GAM_VRR_LUT_OFST12_0_new_data_45                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x002D);
volatile uint8_t xdata g_rw_gamvrr0_002Eh_GAM_VRR_LUT_OFST12_0_new_data_46                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x002E);
volatile uint8_t xdata g_rw_gamvrr0_002Fh_GAM_VRR_LUT_OFST12_0_new_data_47                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x002F);
volatile uint8_t xdata g_rw_gamvrr0_0030h_GAM_VRR_LUT_OFST12_0_new_data_48                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0030);
volatile uint8_t xdata g_rw_gamvrr0_0031h_GAM_VRR_LUT_OFST12_0_new_data_49                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0031);

volatile uint8_t xdata g_rw_gamvrr0_0032h_GAM_VRR_LUT_OFST12_0_new_data_50                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0032);
volatile uint8_t xdata g_rw_gamvrr0_0033h_GAM_VRR_LUT_OFST12_0_new_data_51                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0033);
volatile uint8_t xdata g_rw_gamvrr0_0034h_GAM_VRR_LUT_OFST12_0_new_data_52                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0034);
volatile uint8_t xdata g_rw_gamvrr0_0035h_GAM_VRR_LUT_OFST12_0_new_data_53                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0035);
volatile uint8_t xdata g_rw_gamvrr0_0036h_GAM_VRR_LUT_OFST12_0_new_data_54                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0036);

volatile uint8_t xdata g_rw_gamvrr0_0037h_GAM_VRR_LUT_OFST12_0_new_data_55                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0037);
volatile uint8_t xdata g_rw_gamvrr0_0038h_GAM_VRR_LUT_OFST12_0_new_data_56                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0038);
volatile uint8_t xdata g_rw_gamvrr0_0039h_GAM_VRR_LUT_OFST12_0_new_data_57                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0039);
volatile uint8_t xdata g_rw_gamvrr0_003Ah_GAM_VRR_LUT_OFST12_0_new_data_58                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x003A);
volatile uint8_t xdata g_rw_gamvrr0_003Bh_GAM_VRR_LUT_OFST12_0_new_data_59                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x003B);

volatile uint8_t xdata g_rw_gamvrr0_003Ch_GAM_VRR_LUT_OFST12_0_new_data_60                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x003C);
volatile uint8_t xdata g_rw_gamvrr0_003Dh_GAM_VRR_LUT_OFST12_0_new_data_61                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x003D);
volatile uint8_t xdata g_rw_gamvrr0_003Eh_GAM_VRR_LUT_OFST12_0_new_data_62                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x003E);
volatile uint8_t xdata g_rw_gamvrr0_003Fh_GAM_VRR_LUT_OFST12_0_new_data_63                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x003F);
volatile uint8_t xdata g_rw_gamvrr0_0040h_GAM_VRR_LUT_OFST12_0_new_data_64                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0040);

volatile uint8_t xdata g_rw_gamvrr0_0041h_GAM_VRR_LUT_OFST12_0_new_data_65                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0041);
volatile uint8_t xdata g_rw_gamvrr0_0042h_GAM_VRR_LUT_OFST12_0_new_data_66                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0042);
volatile uint8_t xdata g_rw_gamvrr0_0043h_GAM_VRR_LUT_OFST12_0_new_data_67                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0043);
volatile uint8_t xdata g_rw_gamvrr0_0044h_GAM_VRR_LUT_OFST12_0_new_data_68                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0044);
volatile uint8_t xdata g_rw_gamvrr0_0045h_GAM_VRR_LUT_OFST12_0_new_data_69                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0045);

volatile uint8_t xdata g_rw_gamvrr0_0046h_GAM_VRR_LUT_OFST12_0_new_data_70                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0046);
volatile uint8_t xdata g_rw_gamvrr0_0047h_GAM_VRR_LUT_OFST12_0_new_data_71                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0047);
volatile uint8_t xdata g_rw_gamvrr0_0048h_GAM_VRR_LUT_OFST12_0_new_data_72                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0048);
volatile uint8_t xdata g_rw_gamvrr0_0049h_GAM_VRR_LUT_OFST12_0_new_data_73                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0049);
volatile uint8_t xdata g_rw_gamvrr0_004Ah_GAM_VRR_LUT_OFST12_0_new_data_74                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x004A);

volatile uint8_t xdata g_rw_gamvrr0_004Bh_GAM_VRR_LUT_OFST12_0_new_data_75                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x004B);
volatile uint8_t xdata g_rw_gamvrr0_004Ch_GAM_VRR_LUT_OFST12_0_new_data_76                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x004C);
volatile uint8_t xdata g_rw_gamvrr0_004Dh_GAM_VRR_LUT_OFST12_0_new_data_77                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x004D);
volatile uint8_t xdata g_rw_gamvrr0_004Eh_GAM_VRR_LUT_OFST12_0_new_data_78                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x004E);
volatile uint8_t xdata g_rw_gamvrr0_004Fh_GAM_VRR_LUT_OFST12_0_new_data_79                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x004F);

volatile uint8_t xdata g_rw_gamvrr0_0050h_GAM_VRR_LUT_OFST12_0_new_data_80                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0050);
volatile uint8_t xdata g_rw_gamvrr0_0051h_GAM_VRR_LUT_OFST12_0_new_data_81                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0051);
volatile uint8_t xdata g_rw_gamvrr0_0052h_GAM_VRR_LUT_OFST12_0_new_data_82                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0052);
volatile uint8_t xdata g_rw_gamvrr0_0053h_GAM_VRR_LUT_OFST12_0_new_data_83                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0053);
volatile uint8_t xdata g_rw_gamvrr0_0054h_GAM_VRR_LUT_OFST12_0_new_data_84                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0054);

volatile uint8_t xdata g_rw_gamvrr0_0055h_GAM_VRR_LUT_OFST12_0_new_data_85                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0055);
volatile uint8_t xdata g_rw_gamvrr0_0056h_GAM_VRR_LUT_OFST12_0_new_data_86                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0056);
volatile uint8_t xdata g_rw_gamvrr0_0057h_GAM_VRR_LUT_OFST12_0_new_data_87                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0057);
volatile uint8_t xdata g_rw_gamvrr0_0058h_GAM_VRR_LUT_OFST12_0_new_data_88                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0058);
volatile uint8_t xdata g_rw_gamvrr0_0059h_GAM_VRR_LUT_OFST12_0_new_data_89                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0059);

volatile uint8_t xdata g_rw_gamvrr0_005Ah_GAM_VRR_LUT_OFST12_0_new_data_90                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x005A);
volatile uint8_t xdata g_rw_gamvrr0_005Bh_GAM_VRR_LUT_OFST12_0_new_data_91                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x005B);
volatile uint8_t xdata g_rw_gamvrr0_005Ch_GAM_VRR_LUT_OFST12_0_new_data_92                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x005C);
volatile uint8_t xdata g_rw_gamvrr0_005Dh_GAM_VRR_LUT_OFST12_0_new_data_93                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x005D);
volatile uint8_t xdata g_rw_gamvrr0_005Eh_GAM_VRR_LUT_OFST12_0_new_data_94                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x005E);

volatile uint8_t xdata g_rw_gamvrr0_005Fh_GAM_VRR_LUT_OFST12_0_new_data_95                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x005F);
volatile uint8_t xdata g_rw_gamvrr0_0060h_GAM_VRR_LUT_OFST12_0_new_data_96                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0060);
volatile uint8_t xdata g_rw_gamvrr0_0061h_GAM_VRR_LUT_OFST12_0_new_data_97                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0061);
volatile uint8_t xdata g_rw_gamvrr0_0062h_GAM_VRR_LUT_OFST12_0_new_data_98                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0062);
volatile uint8_t xdata g_rw_gamvrr0_0063h_GAM_VRR_LUT_OFST12_0_new_data_99                     _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0063);

volatile uint8_t xdata g_rw_gamvrr0_0064h_GAM_VRR_LUT_OFST12_0_new_data_100                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0064);
volatile uint8_t xdata g_rw_gamvrr0_0065h_GAM_VRR_LUT_OFST12_0_new_data_101                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0065);
volatile uint8_t xdata g_rw_gamvrr0_0066h_GAM_VRR_LUT_OFST12_0_new_data_102                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0066);
volatile uint8_t xdata g_rw_gamvrr0_0067h_GAM_VRR_LUT_OFST12_0_new_data_103                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0067);
volatile uint8_t xdata g_rw_gamvrr0_0068h_GAM_VRR_LUT_OFST12_0_new_data_104                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0068);

volatile uint8_t xdata g_rw_gamvrr0_0069h_GAM_VRR_LUT_OFST12_0_new_data_105                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0069);
volatile uint8_t xdata g_rw_gamvrr0_006Ah_GAM_VRR_LUT_OFST12_0_new_data_106                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x006A);
volatile uint8_t xdata g_rw_gamvrr0_006Bh_GAM_VRR_LUT_OFST12_0_new_data_107                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x006B);
volatile uint8_t xdata g_rw_gamvrr0_006Ch_GAM_VRR_LUT_OFST12_0_new_data_108                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x006C);
volatile uint8_t xdata g_rw_gamvrr0_006Dh_GAM_VRR_LUT_OFST12_0_new_data_109                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x006D);

volatile uint8_t xdata g_rw_gamvrr0_006Eh_GAM_VRR_LUT_OFST12_0_new_data_110                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x006E);
volatile uint8_t xdata g_rw_gamvrr0_006Fh_GAM_VRR_LUT_OFST12_0_new_data_111                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x006F);
volatile uint8_t xdata g_rw_gamvrr0_0070h_GAM_VRR_LUT_OFST12_0_new_data_112                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0070);
volatile uint8_t xdata g_rw_gamvrr0_0071h_GAM_VRR_LUT_OFST12_0_new_data_113                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0071);
volatile uint8_t xdata g_rw_gamvrr0_0072h_GAM_VRR_LUT_OFST12_0_new_data_114                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0072);

volatile uint8_t xdata g_rw_gamvrr0_0073h_GAM_VRR_LUT_OFST12_0_new_data_115                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0073);
volatile uint8_t xdata g_rw_gamvrr0_0074h_GAM_VRR_LUT_OFST12_0_new_data_116                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0074);
volatile uint8_t xdata g_rw_gamvrr0_0075h_GAM_VRR_LUT_OFST12_0_new_data_117                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0075);
volatile uint8_t xdata g_rw_gamvrr0_0076h_GAM_VRR_LUT_OFST12_0_new_data_118                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0076);
volatile uint8_t xdata g_rw_gamvrr0_0077h_GAM_VRR_LUT_OFST12_0_new_data_119                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0077);

volatile uint8_t xdata g_rw_gamvrr0_0078h_GAM_VRR_LUT_OFST12_0_new_data_120                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0078);
volatile uint8_t xdata g_rw_gamvrr0_0079h_GAM_VRR_LUT_OFST12_0_new_data_121                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0079);
volatile uint8_t xdata g_rw_gamvrr0_007Ah_GAM_VRR_LUT_OFST12_0_new_data_122                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x007A);
volatile uint8_t xdata g_rw_gamvrr0_007Bh_GAM_VRR_LUT_OFST12_0_new_data_123                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x007B);
volatile uint8_t xdata g_rw_gamvrr0_007Ch_GAM_VRR_LUT_OFST12_0_new_data_124                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x007C);

volatile uint8_t xdata g_rw_gamvrr0_007Dh_GAM_VRR_LUT_OFST12_0_new_data_125                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x007D);
volatile uint8_t xdata g_rw_gamvrr0_007Eh_GAM_VRR_LUT_OFST12_0_new_data_126                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x007E);
volatile uint8_t xdata g_rw_gamvrr0_007Fh_GAM_VRR_LUT_OFST12_0_new_data_127                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x007F);
volatile uint8_t xdata g_rw_gamvrr0_0080h_GAM_VRR_LUT_OFST12_0_new_data_128                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0080);
volatile uint8_t xdata g_rw_gamvrr0_0081h_GAM_VRR_LUT_OFST12_0_new_data_129                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0081);

volatile uint8_t xdata g_rw_gamvrr0_0082h_GAM_VRR_LUT_OFST12_0_new_data_130                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0082);
volatile uint8_t xdata g_rw_gamvrr0_0083h_GAM_VRR_LUT_OFST12_0_new_data_131                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0083);
volatile uint8_t xdata g_rw_gamvrr0_0084h_GAM_VRR_LUT_OFST12_0_new_data_132                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0084);
volatile uint8_t xdata g_rw_gamvrr0_0085h_GAM_VRR_LUT_OFST12_0_new_data_133                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0085);
volatile uint8_t xdata g_rw_gamvrr0_0086h_GAM_VRR_LUT_OFST12_0_new_data_134                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0086);

volatile uint8_t xdata g_rw_gamvrr0_0087h_GAM_VRR_LUT_OFST12_0_new_data_135                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0087);
volatile uint8_t xdata g_rw_gamvrr0_0088h_GAM_VRR_LUT_OFST12_0_new_data_136                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0088);
volatile uint8_t xdata g_rw_gamvrr0_0089h_GAM_VRR_LUT_OFST12_0_new_data_137                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0089);
volatile uint8_t xdata g_rw_gamvrr0_008Ah_GAM_VRR_LUT_OFST12_0_new_data_138                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x008A);
volatile uint8_t xdata g_rw_gamvrr0_008Bh_GAM_VRR_LUT_OFST12_0_new_data_139                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x008B);

volatile uint8_t xdata g_rw_gamvrr0_008Ch_GAM_VRR_LUT_OFST12_0_new_data_140                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x008C);
volatile uint8_t xdata g_rw_gamvrr0_008Dh_GAM_VRR_LUT_OFST12_0_new_data_141                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x008D);
volatile uint8_t xdata g_rw_gamvrr0_008Eh_GAM_VRR_LUT_OFST12_0_new_data_142                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x008E);
volatile uint8_t xdata g_rw_gamvrr0_008Fh_GAM_VRR_LUT_OFST12_0_new_data_143                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x008F);
volatile uint8_t xdata g_rw_gamvrr0_0090h_GAM_VRR_LUT_OFST12_0_new_data_144                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0090);

volatile uint8_t xdata g_rw_gamvrr0_0091h_GAM_VRR_LUT_OFST12_0_new_data_145                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0091);
volatile uint8_t xdata g_rw_gamvrr0_0092h_GAM_VRR_LUT_OFST12_0_new_data_146                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0092);
volatile uint8_t xdata g_rw_gamvrr0_0093h_GAM_VRR_LUT_OFST12_0_new_data_147                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0093);
volatile uint8_t xdata g_rw_gamvrr0_0094h_GAM_VRR_LUT_OFST12_0_new_data_148                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0094);
volatile uint8_t xdata g_rw_gamvrr0_0095h_GAM_VRR_LUT_OFST12_0_new_data_149                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0095);

volatile uint8_t xdata g_rw_gamvrr0_0096h_GAM_VRR_LUT_OFST12_0_new_data_150                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0096);
volatile uint8_t xdata g_rw_gamvrr0_0097h_GAM_VRR_LUT_OFST12_0_new_data_151                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0097);
volatile uint8_t xdata g_rw_gamvrr0_0098h_GAM_VRR_LUT_OFST12_0_new_data_152                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0098);
volatile uint8_t xdata g_rw_gamvrr0_0099h_GAM_VRR_LUT_OFST12_0_new_data_153                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0099);
volatile uint8_t xdata g_rw_gamvrr0_009Ah_GAM_VRR_LUT_OFST12_0_new_data_154                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x009A);

volatile uint8_t xdata g_rw_gamvrr0_009Bh_GAM_VRR_LUT_OFST12_0_new_data_155                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x009B);
volatile uint8_t xdata g_rw_gamvrr0_009Ch_GAM_VRR_LUT_OFST12_0_new_data_156                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x009C);
volatile uint8_t xdata g_rw_gamvrr0_009Dh_GAM_VRR_LUT_OFST12_0_new_data_157                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x009D);
volatile uint8_t xdata g_rw_gamvrr0_009Eh_GAM_VRR_LUT_OFST12_0_new_data_158                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x009E);
volatile uint8_t xdata g_rw_gamvrr0_009Fh_GAM_VRR_LUT_OFST12_0_new_data_159                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x009F);

volatile uint8_t xdata g_rw_gamvrr0_00A0h_GAM_VRR_LUT_OFST12_0_new_data_160                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00A0);
volatile uint8_t xdata g_rw_gamvrr0_00A1h_GAM_VRR_LUT_OFST12_0_new_data_161                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00A1);
volatile uint8_t xdata g_rw_gamvrr0_00A2h_GAM_VRR_LUT_OFST12_0_new_data_162                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00A2);
volatile uint8_t xdata g_rw_gamvrr0_00A3h_GAM_VRR_LUT_OFST12_0_new_data_163                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00A3);
volatile uint8_t xdata g_rw_gamvrr0_00A4h_GAM_VRR_LUT_OFST12_0_new_data_164                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00A4);

volatile uint8_t xdata g_rw_gamvrr0_00A5h_GAM_VRR_LUT_OFST12_0_new_data_165                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00A5);
volatile uint8_t xdata g_rw_gamvrr0_00A6h_GAM_VRR_LUT_OFST12_0_new_data_166                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00A6);
volatile uint8_t xdata g_rw_gamvrr0_00A7h_GAM_VRR_LUT_OFST12_0_new_data_167                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00A7);
volatile uint8_t xdata g_rw_gamvrr0_00A8h_GAM_VRR_LUT_OFST12_0_new_data_168                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00A8);
volatile uint8_t xdata g_rw_gamvrr0_00A9h_GAM_VRR_LUT_OFST12_0_new_data_169                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00A9);

volatile uint8_t xdata g_rw_gamvrr0_00AAh_GAM_VRR_LUT_OFST12_0_new_data_170                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00AA);
volatile uint8_t xdata g_rw_gamvrr0_00ABh_GAM_VRR_LUT_OFST12_0_new_data_171                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00AB);
volatile uint8_t xdata g_rw_gamvrr0_00ACh_GAM_VRR_LUT_OFST12_0_new_data_172                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00AC);
volatile uint8_t xdata g_rw_gamvrr0_00ADh_GAM_VRR_LUT_OFST12_0_new_data_173                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00AD);
volatile uint8_t xdata g_rw_gamvrr0_00AEh_GAM_VRR_LUT_OFST12_0_new_data_174                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00AE);

volatile uint8_t xdata g_rw_gamvrr0_00AFh_GAM_VRR_LUT_OFST12_0_new_data_175                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00AF);
volatile uint8_t xdata g_rw_gamvrr0_00B0h_GAM_VRR_LUT_OFST12_0_new_data_176                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00B0);
volatile uint8_t xdata g_rw_gamvrr0_00B1h_GAM_VRR_LUT_OFST12_0_new_data_177                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00B1);
volatile uint8_t xdata g_rw_gamvrr0_00B2h_GAM_VRR_LUT_OFST12_0_new_data_178                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00B2);
volatile uint8_t xdata g_rw_gamvrr0_00B3h_GAM_VRR_LUT_OFST12_0_new_data_179                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00B3);

volatile uint8_t xdata g_rw_gamvrr0_00B4h_GAM_VRR_LUT_OFST12_0_new_data_180                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00B4);
volatile uint8_t xdata g_rw_gamvrr0_00B5h_GAM_VRR_LUT_OFST12_0_new_data_181                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00B5);
volatile uint8_t xdata g_rw_gamvrr0_00B6h_GAM_VRR_LUT_OFST12_0_new_data_182                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00B6);
volatile uint8_t xdata g_rw_gamvrr0_00B7h_GAM_VRR_LUT_OFST12_0_new_data_183                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00B7);
volatile uint8_t xdata g_rw_gamvrr0_00B8h_GAM_VRR_LUT_OFST12_0_new_data_184                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00B8);

volatile uint8_t xdata g_rw_gamvrr0_00B9h_GAM_VRR_LUT_OFST12_0_new_data_185                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00B9);
volatile uint8_t xdata g_rw_gamvrr0_00BAh_GAM_VRR_LUT_OFST12_0_new_data_186                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00BA);
volatile uint8_t xdata g_rw_gamvrr0_00BBh_GAM_VRR_LUT_OFST12_0_new_data_187                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00BB);
volatile uint8_t xdata g_rw_gamvrr0_00BCh_GAM_VRR_LUT_OFST12_0_new_data_188                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00BC);
volatile uint8_t xdata g_rw_gamvrr0_00BDh_GAM_VRR_LUT_OFST12_0_new_data_189                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00BD);

volatile uint8_t xdata g_rw_gamvrr0_00BEh_GAM_VRR_LUT_OFST12_0_new_data_190                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00BE);
volatile uint8_t xdata g_rw_gamvrr0_00BFh_GAM_VRR_LUT_OFST12_0_new_data_191                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00BF);
volatile uint8_t xdata g_rw_gamvrr0_00C0h_GAM_VRR_LUT_OFST12_0_new_data_192                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00C0);
volatile uint8_t xdata g_rw_gamvrr0_00C1h_GAM_VRR_LUT_OFST12_0_new_data_193                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00C1);
volatile uint8_t xdata g_rw_gamvrr0_00C2h_GAM_VRR_LUT_OFST12_0_new_data_194                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00C2);

volatile uint8_t xdata g_rw_gamvrr0_00C3h_GAM_VRR_LUT_OFST12_0_new_data_195                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00C3);
volatile uint8_t xdata g_rw_gamvrr0_00C4h_GAM_VRR_LUT_OFST12_0_new_data_196                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00C4);
volatile uint8_t xdata g_rw_gamvrr0_00C5h_GAM_VRR_LUT_OFST12_0_new_data_197                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00C5);
volatile uint8_t xdata g_rw_gamvrr0_00C6h_GAM_VRR_LUT_OFST12_0_new_data_198                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00C6);
volatile uint8_t xdata g_rw_gamvrr0_00C7h_GAM_VRR_LUT_OFST12_0_new_data_199                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00C7);

volatile uint8_t xdata g_rw_gamvrr0_00C8h_GAM_VRR_LUT_OFST12_0_new_data_200                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00C8);
volatile uint8_t xdata g_rw_gamvrr0_00C9h_GAM_VRR_LUT_OFST12_0_new_data_201                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00C9);
volatile uint8_t xdata g_rw_gamvrr0_00CAh_GAM_VRR_LUT_OFST12_0_new_data_202                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00CA);
volatile uint8_t xdata g_rw_gamvrr0_00CBh_GAM_VRR_LUT_OFST12_0_new_data_203                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00CB);
volatile uint8_t xdata g_rw_gamvrr0_00CCh_GAM_VRR_LUT_OFST12_0_new_data_204                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00CC);

volatile uint8_t xdata g_rw_gamvrr0_00CDh_GAM_VRR_LUT_OFST12_0_new_data_205                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00CD);
volatile uint8_t xdata g_rw_gamvrr0_00CEh_GAM_VRR_LUT_OFST12_0_new_data_206                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00CE);
volatile uint8_t xdata g_rw_gamvrr0_00CFh_GAM_VRR_LUT_OFST12_0_new_data_207                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00CF);
volatile uint8_t xdata g_rw_gamvrr0_00D0h_GAM_VRR_LUT_OFST12_0_new_data_208                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00D0);
volatile uint8_t xdata g_rw_gamvrr0_00D1h_GAM_VRR_LUT_OFST12_0_new_data_209                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00D1);

volatile uint8_t xdata g_rw_gamvrr0_00D2h_GAM_VRR_LUT_OFST12_0_new_data_210                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00D2);
volatile uint8_t xdata g_rw_gamvrr0_00D3h_GAM_VRR_LUT_OFST12_0_new_data_211                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00D3);
volatile uint8_t xdata g_rw_gamvrr0_00D4h_GAM_VRR_LUT_OFST12_0_new_data_212                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00D4);
volatile uint8_t xdata g_rw_gamvrr0_00D5h_GAM_VRR_LUT_OFST12_0_new_data_213                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00D5);
volatile uint8_t xdata g_rw_gamvrr0_00D6h_GAM_VRR_LUT_OFST12_0_new_data_214                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00D6);

volatile uint8_t xdata g_rw_gamvrr0_00D7h_GAM_VRR_LUT_OFST12_0_new_data_215                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00D7);
volatile uint8_t xdata g_rw_gamvrr0_00D8h_GAM_VRR_LUT_OFST12_0_new_data_216                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00D8);
volatile uint8_t xdata g_rw_gamvrr0_00D9h_GAM_VRR_LUT_OFST12_0_new_data_217                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00D9);
volatile uint8_t xdata g_rw_gamvrr0_00DAh_GAM_VRR_LUT_OFST12_0_new_data_218                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00DA);
volatile uint8_t xdata g_rw_gamvrr0_00DBh_GAM_VRR_LUT_OFST12_0_new_data_219                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00DB);

volatile uint8_t xdata g_rw_gamvrr0_00DCh_GAM_VRR_LUT_OFST12_0_new_data_220                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00DC);
volatile uint8_t xdata g_rw_gamvrr0_00DDh_GAM_VRR_LUT_OFST12_0_new_data_221                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00DD);
volatile uint8_t xdata g_rw_gamvrr0_00DEh_GAM_VRR_LUT_OFST12_0_new_data_222                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00DE);
volatile uint8_t xdata g_rw_gamvrr0_00DFh_GAM_VRR_LUT_OFST12_0_new_data_223                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00DF);
volatile uint8_t xdata g_rw_gamvrr0_00E0h_GAM_VRR_LUT_OFST12_0_new_data_224                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00E0);

volatile uint8_t xdata g_rw_gamvrr0_00E1h_GAM_VRR_LUT_OFST12_0_new_data_225                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00E1);
volatile uint8_t xdata g_rw_gamvrr0_00E2h_GAM_VRR_LUT_OFST12_0_new_data_226                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00E2);
volatile uint8_t xdata g_rw_gamvrr0_00E3h_GAM_VRR_LUT_OFST12_0_new_data_227                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00E3);
volatile uint8_t xdata g_rw_gamvrr0_00E4h_GAM_VRR_LUT_OFST12_0_new_data_228                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00E4);
volatile uint8_t xdata g_rw_gamvrr0_00E5h_GAM_VRR_LUT_OFST12_0_new_data_229                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00E5);

volatile uint8_t xdata g_rw_gamvrr0_00E6h_GAM_VRR_LUT_OFST12_0_new_data_230                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00E6);
volatile uint8_t xdata g_rw_gamvrr0_00E7h_GAM_VRR_LUT_OFST12_0_new_data_231                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00E7);
volatile uint8_t xdata g_rw_gamvrr0_00E8h_GAM_VRR_LUT_OFST12_0_new_data_232                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00E8);
volatile uint8_t xdata g_rw_gamvrr0_00E9h_GAM_VRR_LUT_OFST12_0_new_data_233                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00E9);
volatile uint8_t xdata g_rw_gamvrr0_00EAh_GAM_VRR_LUT_OFST12_0_new_data_234                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00EA);

volatile uint8_t xdata g_rw_gamvrr0_00EBh_GAM_VRR_LUT_OFST12_0_new_data_235                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00EB);
volatile uint8_t xdata g_rw_gamvrr0_00ECh_GAM_VRR_LUT_OFST12_0_new_data_236                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00EC);
volatile uint8_t xdata g_rw_gamvrr0_00EDh_GAM_VRR_LUT_OFST12_0_new_data_237                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00ED);
volatile uint8_t xdata g_rw_gamvrr0_00EEh_GAM_VRR_LUT_OFST12_0_new_data_238                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00EE);
volatile uint8_t xdata g_rw_gamvrr0_00EFh_GAM_VRR_LUT_OFST12_0_new_data_239                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00EF);

volatile uint8_t xdata g_rw_gamvrr0_00F0h_GAM_VRR_LUT_OFST12_0_new_data_240                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00F0);
volatile uint8_t xdata g_rw_gamvrr0_00F1h_GAM_VRR_LUT_OFST12_0_new_data_241                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00F1);
volatile uint8_t xdata g_rw_gamvrr0_00F2h_GAM_VRR_LUT_OFST12_0_new_data_242                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00F2);
volatile uint8_t xdata g_rw_gamvrr0_00F3h_GAM_VRR_LUT_OFST12_0_new_data_243                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00F3);
volatile uint8_t xdata g_rw_gamvrr0_00F4h_GAM_VRR_LUT_OFST12_0_new_data_244                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00F4);

volatile uint8_t xdata g_rw_gamvrr0_00F5h_GAM_VRR_LUT_OFST12_0_new_data_245                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00F5);
volatile uint8_t xdata g_rw_gamvrr0_00F6h_GAM_VRR_LUT_OFST12_0_new_data_246                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00F6);
volatile uint8_t xdata g_rw_gamvrr0_00F7h_GAM_VRR_LUT_OFST12_0_new_data_247                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00F7);
volatile uint8_t xdata g_rw_gamvrr0_00F8h_GAM_VRR_LUT_OFST12_0_new_data_248                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00F8);
volatile uint8_t xdata g_rw_gamvrr0_00F9h_GAM_VRR_LUT_OFST12_0_new_data_249                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00F9);

volatile uint8_t xdata g_rw_gamvrr0_00FAh_GAM_VRR_LUT_OFST12_0_new_data_250                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00FA);
volatile uint8_t xdata g_rw_gamvrr0_00FBh_GAM_VRR_LUT_OFST12_0_new_data_251                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00FB);
volatile uint8_t xdata g_rw_gamvrr0_00FCh_GAM_VRR_LUT_OFST12_0_new_data_252                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00FC);
volatile uint8_t xdata g_rw_gamvrr0_00FDh_GAM_VRR_LUT_OFST12_0_new_data_253                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00FD);
volatile uint8_t xdata g_rw_gamvrr0_00FEh_GAM_VRR_LUT_OFST12_0_new_data_254                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00FE);

volatile uint8_t xdata g_rw_gamvrr0_00FFh_GAM_VRR_LUT_OFST12_0_new_data_255                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x00FF);
volatile uint8_t xdata g_rw_gamvrr0_0100h_GAM_VRR_LUT_OFST12_0_new_data_256                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0100);
volatile uint8_t xdata g_rw_gamvrr0_0101h_GAM_VRR_LUT_OFST12_0_new_data_257                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0101);
volatile uint8_t xdata g_rw_gamvrr0_0102h_GAM_VRR_LUT_OFST12_0_new_data_258                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0102);
volatile uint8_t xdata g_rw_gamvrr0_0103h_GAM_VRR_LUT_OFST12_0_new_data_259                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0103);

volatile uint8_t xdata g_rw_gamvrr0_0104h_GAM_VRR_LUT_OFST12_0_new_data_260                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0104);
volatile uint8_t xdata g_rw_gamvrr0_0105h_GAM_VRR_LUT_OFST12_0_new_data_261                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0105);
volatile uint8_t xdata g_rw_gamvrr0_0106h_GAM_VRR_LUT_OFST12_0_new_data_262                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0106);
volatile uint8_t xdata g_rw_gamvrr0_0107h_GAM_VRR_LUT_OFST12_0_new_data_263                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0107);
volatile uint8_t xdata g_rw_gamvrr0_0108h_GAM_VRR_LUT_OFST12_0_new_data_264                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0108);

volatile uint8_t xdata g_rw_gamvrr0_0109h_GAM_VRR_LUT_OFST12_0_new_data_265                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0109);
volatile uint8_t xdata g_rw_gamvrr0_010Ah_GAM_VRR_LUT_OFST12_0_new_data_266                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x010A);
volatile uint8_t xdata g_rw_gamvrr0_010Bh_GAM_VRR_LUT_OFST12_0_new_data_267                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x010B);
volatile uint8_t xdata g_rw_gamvrr0_010Ch_GAM_VRR_LUT_OFST12_0_new_data_268                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x010C);
volatile uint8_t xdata g_rw_gamvrr0_010Dh_GAM_VRR_LUT_OFST12_0_new_data_269                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x010D);

volatile uint8_t xdata g_rw_gamvrr0_010Eh_GAM_VRR_LUT_OFST12_0_new_data_270                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x010E);
volatile uint8_t xdata g_rw_gamvrr0_010Fh_GAM_VRR_LUT_OFST12_0_new_data_271                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x010F);
volatile uint8_t xdata g_rw_gamvrr0_0110h_GAM_VRR_LUT_OFST12_0_new_data_272                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0110);
volatile uint8_t xdata g_rw_gamvrr0_0111h_GAM_VRR_LUT_OFST12_0_new_data_273                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0111);
volatile uint8_t xdata g_rw_gamvrr0_0112h_GAM_VRR_LUT_OFST12_0_new_data_274                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0112);

volatile uint8_t xdata g_rw_gamvrr0_0113h_GAM_VRR_LUT_OFST12_0_new_data_275                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0113);
volatile uint8_t xdata g_rw_gamvrr0_0114h_GAM_VRR_LUT_OFST12_0_new_data_276                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0114);
volatile uint8_t xdata g_rw_gamvrr0_0115h_GAM_VRR_LUT_OFST12_0_new_data_277                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0115);
volatile uint8_t xdata g_rw_gamvrr0_0116h_GAM_VRR_LUT_OFST12_0_new_data_278                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0116);
volatile uint8_t xdata g_rw_gamvrr0_0117h_GAM_VRR_LUT_OFST12_0_new_data_279                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0117);

volatile uint8_t xdata g_rw_gamvrr0_0118h_GAM_VRR_LUT_OFST12_0_new_data_280                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0118);
volatile uint8_t xdata g_rw_gamvrr0_0119h_GAM_VRR_LUT_OFST12_0_new_data_281                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0119);
volatile uint8_t xdata g_rw_gamvrr0_011Ah_GAM_VRR_LUT_OFST12_0_new_data_282                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x011A);
volatile uint8_t xdata g_rw_gamvrr0_011Bh_GAM_VRR_LUT_OFST12_0_new_data_283                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x011B);
volatile uint8_t xdata g_rw_gamvrr0_011Ch_GAM_VRR_LUT_OFST12_0_new_data_284                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x011C);

volatile uint8_t xdata g_rw_gamvrr0_011Dh_GAM_VRR_LUT_OFST12_0_new_data_285                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x011D);
volatile uint8_t xdata g_rw_gamvrr0_011Eh_GAM_VRR_LUT_OFST12_0_new_data_286                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x011E);
volatile uint8_t xdata g_rw_gamvrr0_011Fh_GAM_VRR_LUT_OFST12_0_new_data_287                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x011F);
volatile uint8_t xdata g_rw_gamvrr0_0120h_GAM_VRR_LUT_OFST12_0_new_data_288                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0120);
volatile uint8_t xdata g_rw_gamvrr0_0121h_GAM_VRR_LUT_OFST12_0_new_data_289                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0121);

volatile uint8_t xdata g_rw_gamvrr0_0122h_GAM_VRR_LUT_OFST12_0_new_data_290                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0122);
volatile uint8_t xdata g_rw_gamvrr0_0123h_GAM_VRR_LUT_OFST12_0_new_data_291                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0123);
volatile uint8_t xdata g_rw_gamvrr0_0124h_GAM_VRR_LUT_OFST12_0_new_data_292                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0124);
volatile uint8_t xdata g_rw_gamvrr0_0125h_GAM_VRR_LUT_OFST12_0_new_data_293                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0125);
volatile uint8_t xdata g_rw_gamvrr0_0126h_GAM_VRR_LUT_OFST12_0_new_data_294                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0126);

volatile uint8_t xdata g_rw_gamvrr0_0127h_GAM_VRR_LUT_OFST12_0_new_data_295                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0127);
volatile uint8_t xdata g_rw_gamvrr0_0128h_GAM_VRR_LUT_OFST12_0_new_data_296                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0128);
volatile uint8_t xdata g_rw_gamvrr0_0129h_GAM_VRR_LUT_OFST12_0_new_data_297                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0129);
volatile uint8_t xdata g_rw_gamvrr0_012Ah_GAM_VRR_LUT_OFST12_0_new_data_298                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x012A);
volatile uint8_t xdata g_rw_gamvrr0_012Bh_GAM_VRR_LUT_OFST12_0_new_data_299                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x012B);

volatile uint8_t xdata g_rw_gamvrr0_012Ch_GAM_VRR_LUT_OFST12_0_new_data_300                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x012C);
volatile uint8_t xdata g_rw_gamvrr0_012Dh_GAM_VRR_LUT_OFST12_0_new_data_301                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x012D);
volatile uint8_t xdata g_rw_gamvrr0_012Eh_GAM_VRR_LUT_OFST12_0_new_data_302                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x012E);
volatile uint8_t xdata g_rw_gamvrr0_012Fh_GAM_VRR_LUT_OFST12_0_new_data_303                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x012F);
volatile uint8_t xdata g_rw_gamvrr0_0130h_GAM_VRR_LUT_OFST12_0_new_data_304                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0130);

volatile uint8_t xdata g_rw_gamvrr0_0131h_GAM_VRR_LUT_OFST12_0_new_data_305                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0131);
volatile uint8_t xdata g_rw_gamvrr0_0132h_GAM_VRR_LUT_OFST12_0_new_data_306                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0132);
volatile uint8_t xdata g_rw_gamvrr0_0133h_GAM_VRR_LUT_OFST12_0_new_data_307                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0133);
volatile uint8_t xdata g_rw_gamvrr0_0134h_GAM_VRR_LUT_OFST12_0_new_data_308                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0134);
volatile uint8_t xdata g_rw_gamvrr0_0135h_GAM_VRR_LUT_OFST12_0_new_data_309                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0135);

volatile uint8_t xdata g_rw_gamvrr0_0136h_GAM_VRR_LUT_OFST12_0_new_data_310                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0136);
volatile uint8_t xdata g_rw_gamvrr0_0137h_GAM_VRR_LUT_OFST12_0_new_data_311                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0137);
volatile uint8_t xdata g_rw_gamvrr0_0138h_GAM_VRR_LUT_OFST12_0_new_data_312                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0138);
volatile uint8_t xdata g_rw_gamvrr0_0139h_GAM_VRR_LUT_OFST12_0_new_data_313                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0139);
volatile uint8_t xdata g_rw_gamvrr0_013Ah_GAM_VRR_LUT_OFST12_0_new_data_314                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x013A);

volatile uint8_t xdata g_rw_gamvrr0_013Bh_GAM_VRR_LUT_OFST12_0_new_data_315                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x013B);
volatile uint8_t xdata g_rw_gamvrr0_013Ch_GAM_VRR_LUT_OFST12_0_new_data_316                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x013C);
volatile uint8_t xdata g_rw_gamvrr0_013Dh_GAM_VRR_LUT_OFST12_0_new_data_317                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x013D);
volatile uint8_t xdata g_rw_gamvrr0_013Eh_GAM_VRR_LUT_OFST12_0_new_data_318                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x013E);
volatile uint8_t xdata g_rw_gamvrr0_013Fh_GAM_VRR_LUT_OFST12_0_new_data_319                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x013F);

volatile uint8_t xdata g_rw_gamvrr0_0140h_GAM_VRR_LUT_OFST12_0_new_data_320                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0140);
volatile uint8_t xdata g_rw_gamvrr0_0141h_GAM_VRR_LUT_OFST12_0_new_data_321                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0141);
volatile uint8_t xdata g_rw_gamvrr0_0142h_GAM_VRR_LUT_OFST12_0_new_data_322                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0142);
volatile uint8_t xdata g_rw_gamvrr0_0143h_GAM_VRR_LUT_OFST12_0_new_data_323                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0143);
volatile uint8_t xdata g_rw_gamvrr0_0144h_GAM_VRR_LUT_OFST12_0_new_data_324                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0144);

volatile uint8_t xdata g_rw_gamvrr0_0145h_GAM_VRR_LUT_OFST12_0_new_data_325                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0145);
volatile uint8_t xdata g_rw_gamvrr0_0146h_GAM_VRR_LUT_OFST12_0_new_data_326                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0146);
volatile uint8_t xdata g_rw_gamvrr0_0147h_GAM_VRR_LUT_OFST12_0_new_data_327                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0147);
volatile uint8_t xdata g_rw_gamvrr0_0148h_GAM_VRR_LUT_OFST12_0_new_data_328                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0148);
volatile uint8_t xdata g_rw_gamvrr0_0149h_GAM_VRR_LUT_OFST12_0_new_data_329                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0149);

volatile uint8_t xdata g_rw_gamvrr0_014Ah_GAM_VRR_LUT_OFST12_0_new_data_330                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x014A);
volatile uint8_t xdata g_rw_gamvrr0_014Bh_GAM_VRR_LUT_OFST12_0_new_data_331                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x014B);
volatile uint8_t xdata g_rw_gamvrr0_014Ch_GAM_VRR_LUT_OFST12_0_new_data_332                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x014C);
volatile uint8_t xdata g_rw_gamvrr0_014Dh_GAM_VRR_LUT_OFST12_0_new_data_333                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x014D);
volatile uint8_t xdata g_rw_gamvrr0_014Eh_GAM_VRR_LUT_OFST12_0_new_data_334                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x014E);

volatile uint8_t xdata g_rw_gamvrr0_014Fh_GAM_VRR_LUT_OFST12_0_new_data_335                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x014F);
volatile uint8_t xdata g_rw_gamvrr0_0150h_GAM_VRR_LUT_OFST12_0_new_data_336                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0150);
volatile uint8_t xdata g_rw_gamvrr0_0151h_GAM_VRR_LUT_OFST12_0_new_data_337                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0151);
volatile uint8_t xdata g_rw_gamvrr0_0152h_GAM_VRR_LUT_OFST12_0_new_data_338                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0152);
volatile uint8_t xdata g_rw_gamvrr0_0153h_GAM_VRR_LUT_OFST12_0_new_data_339                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0153);

volatile uint8_t xdata g_rw_gamvrr0_0154h_GAM_VRR_LUT_OFST12_0_new_data_340                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0154);
volatile uint8_t xdata g_rw_gamvrr0_0155h_GAM_VRR_LUT_OFST12_0_new_data_341                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0155);
volatile uint8_t xdata g_rw_gamvrr0_0156h_GAM_VRR_LUT_OFST12_0_new_data_342                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0156);
volatile uint8_t xdata g_rw_gamvrr0_0157h_GAM_VRR_LUT_OFST12_0_new_data_343                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0157);
volatile uint8_t xdata g_rw_gamvrr0_0158h_GAM_VRR_LUT_OFST12_0_new_data_344                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0158);

volatile uint8_t xdata g_rw_gamvrr0_0159h_GAM_VRR_LUT_OFST12_0_new_data_345                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0159);
volatile uint8_t xdata g_rw_gamvrr0_015Ah_GAM_VRR_LUT_OFST12_0_new_data_346                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x015A);
volatile uint8_t xdata g_rw_gamvrr0_015Bh_GAM_VRR_LUT_OFST12_0_new_data_347                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x015B);
volatile uint8_t xdata g_rw_gamvrr0_015Ch_GAM_VRR_LUT_OFST12_0_new_data_348                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x015C);
volatile uint8_t xdata g_rw_gamvrr0_015Dh_GAM_VRR_LUT_OFST12_0_new_data_349                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x015D);

volatile uint8_t xdata g_rw_gamvrr0_015Eh_GAM_VRR_LUT_OFST12_0_new_data_350                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x015E);
volatile uint8_t xdata g_rw_gamvrr0_015Fh_GAM_VRR_LUT_OFST12_0_new_data_351                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x015F);
volatile uint8_t xdata g_rw_gamvrr0_0160h_GAM_VRR_LUT_OFST12_0_new_data_352                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0160);
volatile uint8_t xdata g_rw_gamvrr0_0161h_GAM_VRR_LUT_OFST12_0_new_data_353                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0161);
volatile uint8_t xdata g_rw_gamvrr0_0162h_GAM_VRR_LUT_OFST12_0_new_data_354                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0162);

volatile uint8_t xdata g_rw_gamvrr0_0163h_GAM_VRR_LUT_OFST12_0_new_data_355                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0163);
volatile uint8_t xdata g_rw_gamvrr0_0164h_GAM_VRR_LUT_OFST12_0_new_data_356                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0164);
volatile uint8_t xdata g_rw_gamvrr0_0165h_GAM_VRR_LUT_OFST12_0_new_data_357                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0165);
volatile uint8_t xdata g_rw_gamvrr0_0166h_GAM_VRR_LUT_OFST12_0_new_data_358                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0166);
volatile uint8_t xdata g_rw_gamvrr0_0167h_GAM_VRR_LUT_OFST12_0_new_data_359                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0167);

volatile uint8_t xdata g_rw_gamvrr0_0168h_GAM_VRR_LUT_OFST12_0_new_data_360                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0168);
volatile uint8_t xdata g_rw_gamvrr0_0169h_GAM_VRR_LUT_OFST12_0_new_data_361                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0169);
volatile uint8_t xdata g_rw_gamvrr0_016Ah_GAM_VRR_LUT_OFST12_0_new_data_362                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x016A);
volatile uint8_t xdata g_rw_gamvrr0_016Bh_GAM_VRR_LUT_OFST12_0_new_data_363                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x016B);
volatile uint8_t xdata g_rw_gamvrr0_016Ch_GAM_VRR_LUT_OFST12_0_new_data_364                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x016C);

volatile uint8_t xdata g_rw_gamvrr0_016Dh_GAM_VRR_LUT_OFST12_0_new_data_365                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x016D);
volatile uint8_t xdata g_rw_gamvrr0_016Eh_GAM_VRR_LUT_OFST12_0_new_data_366                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x016E);
volatile uint8_t xdata g_rw_gamvrr0_016Fh_GAM_VRR_LUT_OFST12_0_new_data_367                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x016F);
volatile uint8_t xdata g_rw_gamvrr0_0170h_GAM_VRR_LUT_OFST12_0_new_data_368                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0170);
volatile uint8_t xdata g_rw_gamvrr0_0171h_GAM_VRR_LUT_OFST12_0_new_data_369                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0171);

volatile uint8_t xdata g_rw_gamvrr0_0172h_GAM_VRR_LUT_OFST12_0_new_data_370                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0172);
volatile uint8_t xdata g_rw_gamvrr0_0173h_GAM_VRR_LUT_OFST12_0_new_data_371                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0173);
volatile uint8_t xdata g_rw_gamvrr0_0174h_GAM_VRR_LUT_OFST12_0_new_data_372                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0174);
volatile uint8_t xdata g_rw_gamvrr0_0175h_GAM_VRR_LUT_OFST12_0_new_data_373                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0175);
volatile uint8_t xdata g_rw_gamvrr0_0176h_GAM_VRR_LUT_OFST12_0_new_data_374                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0176);

volatile uint8_t xdata g_rw_gamvrr0_0177h_GAM_VRR_LUT_OFST12_0_new_data_375                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0177);
volatile uint8_t xdata g_rw_gamvrr0_0178h_GAM_VRR_LUT_OFST12_0_new_data_376                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0178);
volatile uint8_t xdata g_rw_gamvrr0_0179h_GAM_VRR_LUT_OFST12_0_new_data_377                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0179);
volatile uint8_t xdata g_rw_gamvrr0_017Ah_GAM_VRR_LUT_OFST12_0_new_data_378                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x017A);
volatile uint8_t xdata g_rw_gamvrr0_017Bh_GAM_VRR_LUT_OFST12_0_new_data_379                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x017B);

volatile uint8_t xdata g_rw_gamvrr0_017Ch_GAM_VRR_LUT_OFST12_0_new_data_380                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x017C);
volatile uint8_t xdata g_rw_gamvrr0_017Dh_GAM_VRR_LUT_OFST12_0_new_data_381                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x017D);
volatile uint8_t xdata g_rw_gamvrr0_017Eh_GAM_VRR_LUT_OFST12_0_new_data_382                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x017E);
volatile uint8_t xdata g_rw_gamvrr0_017Fh_GAM_VRR_LUT_OFST12_0_new_data_383                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x017F);
volatile uint8_t xdata g_rw_gamvrr0_0180h_GAM_VRR_LUT_OFST12_0_new_data_384                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0180);

volatile uint8_t xdata g_rw_gamvrr0_0181h_GAM_VRR_LUT_OFST12_0_new_data_385                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0181);
volatile uint8_t xdata g_rw_gamvrr0_0182h_GAM_VRR_LUT_OFST12_0_new_data_386                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0182);
volatile uint8_t xdata g_rw_gamvrr0_0183h_GAM_VRR_LUT_OFST12_0_new_data_387                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0183);
volatile uint8_t xdata g_rw_gamvrr0_0184h_GAM_VRR_LUT_OFST12_0_new_data_388                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0184);
volatile uint8_t xdata g_rw_gamvrr0_0185h_GAM_VRR_LUT_OFST12_0_new_data_389                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0185);

volatile uint8_t xdata g_rw_gamvrr0_0186h_GAM_VRR_LUT_OFST12_0_new_data_390                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0186);
volatile uint8_t xdata g_rw_gamvrr0_0187h_GAM_VRR_LUT_OFST12_0_new_data_391                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0187);
volatile uint8_t xdata g_rw_gamvrr0_0188h_GAM_VRR_LUT_OFST12_0_new_data_392                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0188);
volatile uint8_t xdata g_rw_gamvrr0_0189h_GAM_VRR_LUT_OFST12_0_new_data_393                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0189);
volatile uint8_t xdata g_rw_gamvrr0_018Ah_GAM_VRR_LUT_OFST12_0_new_data_394                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x018A);

volatile uint8_t xdata g_rw_gamvrr0_018Bh_GAM_VRR_LUT_OFST12_0_new_data_395                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x018B);
volatile uint8_t xdata g_rw_gamvrr0_018Ch_GAM_VRR_LUT_OFST12_0_new_data_396                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x018C);
volatile uint8_t xdata g_rw_gamvrr0_018Dh_GAM_VRR_LUT_OFST12_0_new_data_397                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x018D);
volatile uint8_t xdata g_rw_gamvrr0_018Eh_GAM_VRR_LUT_OFST12_0_new_data_398                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x018E);
volatile uint8_t xdata g_rw_gamvrr0_018Fh_GAM_VRR_LUT_OFST12_0_new_data_399                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x018F);

volatile uint8_t xdata g_rw_gamvrr0_0190h_GAM_VRR_LUT_OFST12_0_new_data_400                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0190);
volatile uint8_t xdata g_rw_gamvrr0_0191h_GAM_VRR_LUT_OFST12_0_new_data_401                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0191);
volatile uint8_t xdata g_rw_gamvrr0_0192h_GAM_VRR_LUT_OFST12_0_new_data_402                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0192);
volatile uint8_t xdata g_rw_gamvrr0_0193h_GAM_VRR_LUT_OFST12_0_new_data_403                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0193);
volatile uint8_t xdata g_rw_gamvrr0_0194h_GAM_VRR_LUT_OFST12_0_new_data_404                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0194);

volatile uint8_t xdata g_rw_gamvrr0_0195h_GAM_VRR_LUT_OFST12_0_new_data_405                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0195);
volatile uint8_t xdata g_rw_gamvrr0_0196h_GAM_VRR_LUT_OFST12_0_new_data_406                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0196);
volatile uint8_t xdata g_rw_gamvrr0_0197h_GAM_VRR_LUT_OFST12_0_new_data_407                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0197);
volatile uint8_t xdata g_rw_gamvrr0_0198h_GAM_VRR_LUT_OFST12_0_new_data_408                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0198);
volatile uint8_t xdata g_rw_gamvrr0_0199h_GAM_VRR_LUT_OFST12_0_new_data_409                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0199);

volatile uint8_t xdata g_rw_gamvrr0_019Ah_GAM_VRR_LUT_OFST12_0_new_data_410                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x019A);
volatile uint8_t xdata g_rw_gamvrr0_019Bh_GAM_VRR_LUT_OFST12_0_new_data_411                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x019B);
volatile uint8_t xdata g_rw_gamvrr0_019Ch_GAM_VRR_LUT_OFST12_0_new_data_412                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x019C);
volatile uint8_t xdata g_rw_gamvrr0_019Dh_GAM_VRR_LUT_OFST12_0_new_data_413                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x019D);
volatile uint8_t xdata g_rw_gamvrr0_019Eh_GAM_VRR_LUT_OFST12_0_new_data_414                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x019E);

volatile uint8_t xdata g_rw_gamvrr0_019Fh_GAM_VRR_LUT_OFST12_0_new_data_415                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x019F);
volatile uint8_t xdata g_rw_gamvrr0_01A0h_GAM_VRR_LUT_OFST12_0_new_data_416                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01A0);
volatile uint8_t xdata g_rw_gamvrr0_01A1h_GAM_VRR_LUT_OFST12_0_new_data_417                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01A1);
volatile uint8_t xdata g_rw_gamvrr0_01A2h_GAM_VRR_LUT_OFST12_0_new_data_418                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01A2);
volatile uint8_t xdata g_rw_gamvrr0_01A3h_GAM_VRR_LUT_OFST12_0_new_data_419                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01A3);

volatile uint8_t xdata g_rw_gamvrr0_01A4h_GAM_VRR_LUT_OFST12_0_new_data_420                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01A4);
volatile uint8_t xdata g_rw_gamvrr0_01A5h_GAM_VRR_LUT_OFST12_0_new_data_421                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01A5);
volatile uint8_t xdata g_rw_gamvrr0_01A6h_GAM_VRR_LUT_OFST12_0_new_data_422                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01A6);
volatile uint8_t xdata g_rw_gamvrr0_01A7h_GAM_VRR_LUT_OFST12_0_new_data_423                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01A7);
volatile uint8_t xdata g_rw_gamvrr0_01A8h_GAM_VRR_LUT_OFST12_0_new_data_424                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01A8);

volatile uint8_t xdata g_rw_gamvrr0_01A9h_GAM_VRR_LUT_OFST12_0_new_data_425                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01A9);
volatile uint8_t xdata g_rw_gamvrr0_01AAh_GAM_VRR_LUT_OFST12_0_new_data_426                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01AA);
volatile uint8_t xdata g_rw_gamvrr0_01ABh_GAM_VRR_LUT_OFST12_0_new_data_427                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01AB);
volatile uint8_t xdata g_rw_gamvrr0_01ACh_GAM_VRR_LUT_OFST12_0_new_data_428                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01AC);
volatile uint8_t xdata g_rw_gamvrr0_01ADh_GAM_VRR_LUT_OFST12_0_new_data_429                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01AD);

volatile uint8_t xdata g_rw_gamvrr0_01AEh_GAM_VRR_LUT_OFST12_0_new_data_430                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01AE);
volatile uint8_t xdata g_rw_gamvrr0_01AFh_GAM_VRR_LUT_OFST12_0_new_data_431                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01AF);
volatile uint8_t xdata g_rw_gamvrr0_01B0h_GAM_VRR_LUT_OFST12_0_new_data_432                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01B0);
volatile uint8_t xdata g_rw_gamvrr0_01B1h_GAM_VRR_LUT_OFST12_0_new_data_433                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01B1);
volatile uint8_t xdata g_rw_gamvrr0_01B2h_GAM_VRR_LUT_OFST12_0_new_data_434                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01B2);

volatile uint8_t xdata g_rw_gamvrr0_01B3h_GAM_VRR_LUT_OFST12_0_new_data_435                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01B3);
volatile uint8_t xdata g_rw_gamvrr0_01B4h_GAM_VRR_LUT_OFST12_0_new_data_436                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01B4);
volatile uint8_t xdata g_rw_gamvrr0_01B5h_GAM_VRR_LUT_OFST12_0_new_data_437                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01B5);
volatile uint8_t xdata g_rw_gamvrr0_01B6h_GAM_VRR_LUT_OFST12_0_new_data_438                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01B6);
volatile uint8_t xdata g_rw_gamvrr0_01B7h_GAM_VRR_LUT_OFST12_0_new_data_439                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01B7);

volatile uint8_t xdata g_rw_gamvrr0_01B8h_GAM_VRR_LUT_OFST12_0_new_data_440                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01B8);
volatile uint8_t xdata g_rw_gamvrr0_01B9h_GAM_VRR_LUT_OFST12_0_new_data_441                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01B9);
volatile uint8_t xdata g_rw_gamvrr0_01BAh_GAM_VRR_LUT_OFST12_0_new_data_442                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01BA);
volatile uint8_t xdata g_rw_gamvrr0_01BBh_GAM_VRR_LUT_OFST12_0_new_data_443                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01BB);
volatile uint8_t xdata g_rw_gamvrr0_01BCh_GAM_VRR_LUT_OFST12_0_new_data_444                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01BC);

volatile uint8_t xdata g_rw_gamvrr0_01BDh_GAM_VRR_LUT_OFST12_0_new_data_445                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01BD);
volatile uint8_t xdata g_rw_gamvrr0_01BEh_GAM_VRR_LUT_OFST12_0_new_data_446                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01BE);
volatile uint8_t xdata g_rw_gamvrr0_01BFh_GAM_VRR_LUT_OFST12_0_new_data_447                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01BF);
volatile uint8_t xdata g_rw_gamvrr0_01C0h_GAM_VRR_LUT_OFST12_0_new_data_448                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01C0);
volatile uint8_t xdata g_rw_gamvrr0_01C1h_GAM_VRR_LUT_OFST12_0_new_data_449                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01C1);

volatile uint8_t xdata g_rw_gamvrr0_01C2h_GAM_VRR_LUT_OFST12_0_new_data_450                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01C2);
volatile uint8_t xdata g_rw_gamvrr0_01C3h_GAM_VRR_LUT_OFST12_0_new_data_451                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01C3);
volatile uint8_t xdata g_rw_gamvrr0_01C4h_GAM_VRR_LUT_OFST12_0_new_data_452                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01C4);
volatile uint8_t xdata g_rw_gamvrr0_01C5h_GAM_VRR_LUT_OFST12_0_new_data_453                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01C5);
volatile uint8_t xdata g_rw_gamvrr0_01C6h_GAM_VRR_LUT_OFST12_0_new_data_454                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01C6);

volatile uint8_t xdata g_rw_gamvrr0_01C7h_GAM_VRR_LUT_OFST12_0_new_data_455                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01C7);
volatile uint8_t xdata g_rw_gamvrr0_01C8h_GAM_VRR_LUT_OFST12_0_new_data_456                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01C8);
volatile uint8_t xdata g_rw_gamvrr0_01C9h_GAM_VRR_LUT_OFST12_0_new_data_457                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01C9);
volatile uint8_t xdata g_rw_gamvrr0_01CAh_GAM_VRR_LUT_OFST12_0_new_data_458                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01CA);
volatile uint8_t xdata g_rw_gamvrr0_01CBh_GAM_VRR_LUT_OFST12_0_new_data_459                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01CB);

volatile uint8_t xdata g_rw_gamvrr0_01CCh_GAM_VRR_LUT_OFST12_0_new_data_460                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01CC);
volatile uint8_t xdata g_rw_gamvrr0_01CDh_GAM_VRR_LUT_OFST12_0_new_data_461                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01CD);
volatile uint8_t xdata g_rw_gamvrr0_01CEh_GAM_VRR_LUT_OFST12_0_new_data_462                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01CE);
volatile uint8_t xdata g_rw_gamvrr0_01CFh_GAM_VRR_LUT_OFST12_0_new_data_463                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01CF);
volatile uint8_t xdata g_rw_gamvrr0_01D0h_GAM_VRR_LUT_OFST12_0_new_data_464                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01D0);

volatile uint8_t xdata g_rw_gamvrr0_01D1h_GAM_VRR_LUT_OFST12_0_new_data_465                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01D1);
volatile uint8_t xdata g_rw_gamvrr0_01D2h_GAM_VRR_LUT_OFST12_0_new_data_466                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01D2);
volatile uint8_t xdata g_rw_gamvrr0_01D3h_GAM_VRR_LUT_OFST12_0_new_data_467                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01D3);
volatile uint8_t xdata g_rw_gamvrr0_01D4h_GAM_VRR_LUT_OFST12_0_new_data_468                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01D4);
volatile uint8_t xdata g_rw_gamvrr0_01D5h_GAM_VRR_LUT_OFST12_0_new_data_469                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01D5);

volatile uint8_t xdata g_rw_gamvrr0_01D6h_GAM_VRR_LUT_OFST12_0_new_data_470                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01D6);
volatile uint8_t xdata g_rw_gamvrr0_01D7h_GAM_VRR_LUT_OFST12_0_new_data_471                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01D7);
volatile uint8_t xdata g_rw_gamvrr0_01D8h_GAM_VRR_LUT_OFST12_0_new_data_472                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01D8);
volatile uint8_t xdata g_rw_gamvrr0_01D9h_GAM_VRR_LUT_OFST12_0_new_data_473                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01D9);
volatile uint8_t xdata g_rw_gamvrr0_01DAh_GAM_VRR_LUT_OFST12_0_new_data_474                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01DA);

volatile uint8_t xdata g_rw_gamvrr0_01DBh_GAM_VRR_LUT_OFST12_0_new_data_475                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01DB);
volatile uint8_t xdata g_rw_gamvrr0_01DCh_GAM_VRR_LUT_OFST12_0_new_data_476                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01DC);
volatile uint8_t xdata g_rw_gamvrr0_01DDh_GAM_VRR_LUT_OFST12_0_new_data_477                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01DD);
volatile uint8_t xdata g_rw_gamvrr0_01DEh_GAM_VRR_LUT_OFST12_0_new_data_478                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01DE);
volatile uint8_t xdata g_rw_gamvrr0_01DFh_GAM_VRR_LUT_OFST12_0_new_data_479                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01DF);

volatile uint8_t xdata g_rw_gamvrr0_01E0h_GAM_VRR_LUT_OFST12_0_new_data_480                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01E0);
volatile uint8_t xdata g_rw_gamvrr0_01E1h_GAM_VRR_LUT_OFST12_0_new_data_481                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01E1);
volatile uint8_t xdata g_rw_gamvrr0_01E2h_GAM_VRR_LUT_OFST12_0_new_data_482                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01E2);
volatile uint8_t xdata g_rw_gamvrr0_01E3h_GAM_VRR_LUT_OFST12_0_new_data_483                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01E3);
volatile uint8_t xdata g_rw_gamvrr0_01E4h_GAM_VRR_LUT_OFST12_0_new_data_484                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01E4);

volatile uint8_t xdata g_rw_gamvrr0_01E5h_GAM_VRR_LUT_OFST12_0_new_data_485                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01E5);
volatile uint8_t xdata g_rw_gamvrr0_01E6h_GAM_VRR_LUT_OFST12_0_new_data_486                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01E6);
volatile uint8_t xdata g_rw_gamvrr0_01E7h_GAM_VRR_LUT_OFST12_0_new_data_487                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01E7);
volatile uint8_t xdata g_rw_gamvrr0_01E8h_GAM_VRR_LUT_OFST12_0_new_data_488                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01E8);
volatile uint8_t xdata g_rw_gamvrr0_01E9h_GAM_VRR_LUT_OFST12_0_new_data_489                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01E9);

volatile uint8_t xdata g_rw_gamvrr0_01EAh_GAM_VRR_LUT_OFST12_0_new_data_490                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01EA);
volatile uint8_t xdata g_rw_gamvrr0_01EBh_GAM_VRR_LUT_OFST12_0_new_data_491                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01EB);
volatile uint8_t xdata g_rw_gamvrr0_01ECh_GAM_VRR_LUT_OFST12_0_new_data_492                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01EC);
volatile uint8_t xdata g_rw_gamvrr0_01EDh_GAM_VRR_LUT_OFST12_0_new_data_493                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01ED);
volatile uint8_t xdata g_rw_gamvrr0_01EEh_GAM_VRR_LUT_OFST12_0_new_data_494                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01EE);

volatile uint8_t xdata g_rw_gamvrr0_01EFh_GAM_VRR_LUT_OFST12_0_new_data_495                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01EF);
volatile uint8_t xdata g_rw_gamvrr0_01F0h_GAM_VRR_LUT_OFST12_0_new_data_496                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01F0);
volatile uint8_t xdata g_rw_gamvrr0_01F1h_GAM_VRR_LUT_OFST12_0_new_data_497                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01F1);
volatile uint8_t xdata g_rw_gamvrr0_01F2h_GAM_VRR_LUT_OFST12_0_new_data_498                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01F2);
volatile uint8_t xdata g_rw_gamvrr0_01F3h_GAM_VRR_LUT_OFST12_0_new_data_499                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01F3);

volatile uint8_t xdata g_rw_gamvrr0_01F4h_GAM_VRR_LUT_OFST12_0_new_data_500                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01F4);
volatile uint8_t xdata g_rw_gamvrr0_01F5h_GAM_VRR_LUT_OFST12_0_new_data_501                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01F5);
volatile uint8_t xdata g_rw_gamvrr0_01F6h_GAM_VRR_LUT_OFST12_0_new_data_502                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01F6);
volatile uint8_t xdata g_rw_gamvrr0_01F7h_GAM_VRR_LUT_OFST12_0_new_data_503                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01F7);
volatile uint8_t xdata g_rw_gamvrr0_01F8h_GAM_VRR_LUT_OFST12_0_new_data_504                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01F8);

volatile uint8_t xdata g_rw_gamvrr0_01F9h_GAM_VRR_LUT_OFST12_0_new_data_505                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01F9);
volatile uint8_t xdata g_rw_gamvrr0_01FAh_GAM_VRR_LUT_OFST12_0_new_data_506                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01FA);
volatile uint8_t xdata g_rw_gamvrr0_01FBh_GAM_VRR_LUT_OFST12_0_new_data_507                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01FB);
volatile uint8_t xdata g_rw_gamvrr0_01FCh_GAM_VRR_LUT_OFST12_0_new_data_508                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01FC);
volatile uint8_t xdata g_rw_gamvrr0_01FDh_GAM_VRR_LUT_OFST12_0_new_data_509                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01FD);

volatile uint8_t xdata g_rw_gamvrr0_01FEh_GAM_VRR_LUT_OFST12_0_new_data_510                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01FE);
volatile uint8_t xdata g_rw_gamvrr0_01FFh_GAM_VRR_LUT_OFST12_0_new_data_511                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x01FF);
volatile uint8_t xdata g_rw_gamvrr0_0200h_GAM_VRR_LUT_OFST12_0_new_data_512                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0200);
volatile uint8_t xdata g_rw_gamvrr0_0201h_GAM_VRR_LUT_OFST12_0_new_data_513                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0201);
volatile uint8_t xdata g_rw_gamvrr0_0202h_GAM_VRR_LUT_OFST12_0_new_data_514                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0202);

volatile uint8_t xdata g_rw_gamvrr0_0203h_GAM_VRR_LUT_OFST12_0_new_data_515                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0203);
volatile uint8_t xdata g_rw_gamvrr0_0204h_GAM_VRR_LUT_OFST12_0_new_data_516                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0204);
volatile uint8_t xdata g_rw_gamvrr0_0205h_GAM_VRR_LUT_OFST12_0_new_data_517                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0205);
volatile uint8_t xdata g_rw_gamvrr0_0206h_GAM_VRR_LUT_OFST12_0_new_data_518                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0206);
volatile uint8_t xdata g_rw_gamvrr0_0207h_GAM_VRR_LUT_OFST12_0_new_data_519                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0207);

volatile uint8_t xdata g_rw_gamvrr0_0208h_GAM_VRR_LUT_OFST12_0_new_data_520                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0208);
volatile uint8_t xdata g_rw_gamvrr0_0209h_GAM_VRR_LUT_OFST12_0_new_data_521                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0209);
volatile uint8_t xdata g_rw_gamvrr0_020Ah_GAM_VRR_LUT_OFST12_0_new_data_522                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x020A);
volatile uint8_t xdata g_rw_gamvrr0_020Bh_GAM_VRR_LUT_OFST12_0_new_data_523                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x020B);
volatile uint8_t xdata g_rw_gamvrr0_020Ch_GAM_VRR_LUT_OFST12_0_new_data_524                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x020C);

volatile uint8_t xdata g_rw_gamvrr0_020Dh_GAM_VRR_LUT_OFST12_0_new_data_525                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x020D);
volatile uint8_t xdata g_rw_gamvrr0_020Eh_GAM_VRR_LUT_OFST12_0_new_data_526                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x020E);
volatile uint8_t xdata g_rw_gamvrr0_020Fh_GAM_VRR_LUT_OFST12_0_new_data_527                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x020F);
volatile uint8_t xdata g_rw_gamvrr0_0210h_GAM_VRR_LUT_OFST12_0_new_data_528                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0210);
volatile uint8_t xdata g_rw_gamvrr0_0211h_GAM_VRR_LUT_OFST12_0_new_data_529                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0211);

volatile uint8_t xdata g_rw_gamvrr0_0212h_GAM_VRR_LUT_OFST12_0_new_data_530                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0212);
volatile uint8_t xdata g_rw_gamvrr0_0213h_GAM_VRR_LUT_OFST12_0_new_data_531                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0213);
volatile uint8_t xdata g_rw_gamvrr0_0214h_GAM_VRR_LUT_OFST12_0_new_data_532                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0214);
volatile uint8_t xdata g_rw_gamvrr0_0215h_GAM_VRR_LUT_OFST12_0_new_data_533                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0215);
volatile uint8_t xdata g_rw_gamvrr0_0216h_GAM_VRR_LUT_OFST12_0_new_data_534                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0216);

volatile uint8_t xdata g_rw_gamvrr0_0217h_GAM_VRR_LUT_OFST12_0_new_data_535                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0217);
volatile uint8_t xdata g_rw_gamvrr0_0218h_GAM_VRR_LUT_OFST12_0_new_data_536                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0218);
volatile uint8_t xdata g_rw_gamvrr0_0219h_GAM_VRR_LUT_OFST12_0_new_data_537                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0219);
volatile uint8_t xdata g_rw_gamvrr0_021Ah_GAM_VRR_LUT_OFST12_0_new_data_538                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x021A);
volatile uint8_t xdata g_rw_gamvrr0_021Bh_GAM_VRR_LUT_OFST12_0_new_data_539                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x021B);

volatile uint8_t xdata g_rw_gamvrr0_021Ch_GAM_VRR_LUT_OFST12_0_new_data_540                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x021C);
volatile uint8_t xdata g_rw_gamvrr0_021Dh_GAM_VRR_LUT_OFST12_0_new_data_541                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x021D);
volatile uint8_t xdata g_rw_gamvrr0_021Eh_GAM_VRR_LUT_OFST12_0_new_data_542                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x021E);
volatile uint8_t xdata g_rw_gamvrr0_021Fh_GAM_VRR_LUT_OFST12_0_new_data_543                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x021F);
volatile uint8_t xdata g_rw_gamvrr0_0220h_GAM_VRR_LUT_OFST12_0_new_data_544                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0220);

volatile uint8_t xdata g_rw_gamvrr0_0221h_GAM_VRR_LUT_OFST12_0_new_data_545                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0221);
volatile uint8_t xdata g_rw_gamvrr0_0222h_GAM_VRR_LUT_OFST12_0_new_data_546                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0222);
volatile uint8_t xdata g_rw_gamvrr0_0223h_GAM_VRR_LUT_OFST12_0_new_data_547                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0223);
volatile uint8_t xdata g_rw_gamvrr0_0224h_GAM_VRR_LUT_OFST12_0_new_data_548                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0224);
volatile uint8_t xdata g_rw_gamvrr0_0225h_GAM_VRR_LUT_OFST12_0_new_data_549                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0225);

volatile uint8_t xdata g_rw_gamvrr0_0226h_GAM_VRR_LUT_OFST12_0_new_data_550                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0226);
volatile uint8_t xdata g_rw_gamvrr0_0227h_GAM_VRR_LUT_OFST12_0_new_data_551                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0227);
volatile uint8_t xdata g_rw_gamvrr0_0228h_GAM_VRR_LUT_OFST12_0_new_data_552                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0228);
volatile uint8_t xdata g_rw_gamvrr0_0229h_GAM_VRR_LUT_OFST12_0_new_data_553                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0229);
volatile uint8_t xdata g_rw_gamvrr0_022Ah_GAM_VRR_LUT_OFST12_0_new_data_554                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x022A);

volatile uint8_t xdata g_rw_gamvrr0_022Bh_GAM_VRR_LUT_OFST12_0_new_data_555                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x022B);
volatile uint8_t xdata g_rw_gamvrr0_022Ch_GAM_VRR_LUT_OFST12_0_new_data_556                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x022C);
volatile uint8_t xdata g_rw_gamvrr0_022Dh_GAM_VRR_LUT_OFST12_0_new_data_557                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x022D);
volatile uint8_t xdata g_rw_gamvrr0_022Eh_GAM_VRR_LUT_OFST12_0_new_data_558                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x022E);
volatile uint8_t xdata g_rw_gamvrr0_022Fh_GAM_VRR_LUT_OFST12_0_new_data_559                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x022F);

volatile uint8_t xdata g_rw_gamvrr0_0230h_GAM_VRR_LUT_OFST12_0_new_data_560                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0230);
volatile uint8_t xdata g_rw_gamvrr0_0231h_GAM_VRR_LUT_OFST12_0_new_data_561                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0231);
volatile uint8_t xdata g_rw_gamvrr0_0232h_GAM_VRR_LUT_OFST12_0_new_data_562                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0232);
volatile uint8_t xdata g_rw_gamvrr0_0233h_GAM_VRR_LUT_OFST12_0_new_data_563                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0233);
volatile uint8_t xdata g_rw_gamvrr0_0234h_GAM_VRR_LUT_OFST12_0_new_data_564                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0234);

volatile uint8_t xdata g_rw_gamvrr0_0235h_GAM_VRR_LUT_OFST12_0_new_data_565                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0235);
volatile uint8_t xdata g_rw_gamvrr0_0236h_GAM_VRR_LUT_OFST12_0_new_data_566                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0236);
volatile uint8_t xdata g_rw_gamvrr0_0237h_GAM_VRR_LUT_OFST12_0_new_data_567                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0237);
volatile uint8_t xdata g_rw_gamvrr0_0238h_GAM_VRR_LUT_OFST12_0_new_data_568                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0238);
volatile uint8_t xdata g_rw_gamvrr0_0239h_GAM_VRR_LUT_OFST12_0_new_data_569                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0239);

volatile uint8_t xdata g_rw_gamvrr0_023Ah_GAM_VRR_LUT_OFST12_0_new_data_570                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x023A);
volatile uint8_t xdata g_rw_gamvrr0_023Bh_GAM_VRR_LUT_OFST12_0_new_data_571                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x023B);
volatile uint8_t xdata g_rw_gamvrr0_023Ch_GAM_VRR_LUT_OFST12_0_new_data_572                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x023C);
volatile uint8_t xdata g_rw_gamvrr0_023Dh_GAM_VRR_LUT_OFST12_0_new_data_573                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x023D);
volatile uint8_t xdata g_rw_gamvrr0_023Eh_GAM_VRR_LUT_OFST12_0_new_data_574                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x023E);

volatile uint8_t xdata g_rw_gamvrr0_023Fh_GAM_VRR_LUT_OFST12_0_new_data_575                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x023F);
volatile uint8_t xdata g_rw_gamvrr0_0240h_GAM_VRR_LUT_OFST12_0_new_data_576                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0240);
volatile uint8_t xdata g_rw_gamvrr0_0241h_GAM_VRR_LUT_OFST12_0_new_data_577                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0241);
volatile uint8_t xdata g_rw_gamvrr0_0242h_GAM_VRR_LUT_OFST12_0_new_data_578                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0242);
volatile uint8_t xdata g_rw_gamvrr0_0243h_GAM_VRR_LUT_OFST12_0_new_data_579                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0243);

volatile uint8_t xdata g_rw_gamvrr0_0244h_GAM_VRR_LUT_OFST12_0_new_data_580                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0244);
volatile uint8_t xdata g_rw_gamvrr0_0245h_GAM_VRR_LUT_OFST12_0_new_data_581                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0245);
volatile uint8_t xdata g_rw_gamvrr0_0246h_GAM_VRR_LUT_OFST12_0_new_data_582                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0246);
volatile uint8_t xdata g_rw_gamvrr0_0247h_GAM_VRR_LUT_OFST12_0_new_data_583                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0247);
volatile uint8_t xdata g_rw_gamvrr0_0248h_GAM_VRR_LUT_OFST12_0_new_data_584                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0248);

volatile uint8_t xdata g_rw_gamvrr0_0249h_GAM_VRR_LUT_OFST12_0_new_data_585                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0249);
volatile uint8_t xdata g_rw_gamvrr0_024Ah_GAM_VRR_LUT_OFST12_0_new_data_586                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x024A);
volatile uint8_t xdata g_rw_gamvrr0_024Bh_GAM_VRR_LUT_OFST12_0_new_data_587                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x024B);
volatile uint8_t xdata g_rw_gamvrr0_024Ch_GAM_VRR_LUT_OFST12_0_new_data_588                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x024C);
volatile uint8_t xdata g_rw_gamvrr0_024Dh_GAM_VRR_LUT_OFST12_0_new_data_589                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x024D);

volatile uint8_t xdata g_rw_gamvrr0_024Eh_GAM_VRR_LUT_OFST12_0_new_data_590                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x024E);
volatile uint8_t xdata g_rw_gamvrr0_024Fh_GAM_VRR_LUT_OFST12_0_new_data_591                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x024F);
volatile uint8_t xdata g_rw_gamvrr0_0250h_GAM_VRR_LUT_OFST12_0_new_data_592                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0250);
volatile uint8_t xdata g_rw_gamvrr0_0251h_GAM_VRR_LUT_OFST12_0_new_data_593                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0251);
volatile uint8_t xdata g_rw_gamvrr0_0252h_GAM_VRR_LUT_OFST12_0_new_data_594                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0252);

volatile uint8_t xdata g_rw_gamvrr0_0253h_GAM_VRR_LUT_OFST12_0_new_data_595                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0253);
volatile uint8_t xdata g_rw_gamvrr0_0254h_GAM_VRR_LUT_OFST12_0_new_data_596                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0254);
volatile uint8_t xdata g_rw_gamvrr0_0255h_GAM_VRR_LUT_OFST12_0_new_data_597                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0255);
volatile uint8_t xdata g_rw_gamvrr0_0256h_GAM_VRR_LUT_OFST12_0_new_data_598                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0256);
volatile uint8_t xdata g_rw_gamvrr0_0257h_GAM_VRR_LUT_OFST12_0_new_data_599                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0257);

volatile uint8_t xdata g_rw_gamvrr0_0258h_GAM_VRR_LUT_OFST12_0_new_data_600                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0258);
volatile uint8_t xdata g_rw_gamvrr0_0259h_GAM_VRR_LUT_OFST12_0_new_data_601                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0259);
volatile uint8_t xdata g_rw_gamvrr0_025Ah_GAM_VRR_LUT_OFST12_0_new_data_602                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x025A);
volatile uint8_t xdata g_rw_gamvrr0_025Bh_GAM_VRR_LUT_OFST12_0_new_data_603                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x025B);
volatile uint8_t xdata g_rw_gamvrr0_025Ch_GAM_VRR_LUT_OFST12_0_new_data_604                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x025C);

volatile uint8_t xdata g_rw_gamvrr0_025Dh_GAM_VRR_LUT_OFST12_0_new_data_605                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x025D);
volatile uint8_t xdata g_rw_gamvrr0_025Eh_GAM_VRR_LUT_OFST12_0_new_data_606                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x025E);
volatile uint8_t xdata g_rw_gamvrr0_025Fh_GAM_VRR_LUT_OFST12_0_new_data_607                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x025F);
volatile uint8_t xdata g_rw_gamvrr0_0260h_GAM_VRR_LUT_OFST12_0_new_data_608                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0260);
volatile uint8_t xdata g_rw_gamvrr0_0261h_GAM_VRR_LUT_OFST12_0_new_data_609                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0261);

volatile uint8_t xdata g_rw_gamvrr0_0262h_GAM_VRR_LUT_OFST12_0_new_data_610                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0262);
volatile uint8_t xdata g_rw_gamvrr0_0263h_GAM_VRR_LUT_OFST12_0_new_data_611                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0263);
volatile uint8_t xdata g_rw_gamvrr0_0264h_GAM_VRR_LUT_OFST12_0_new_data_612                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0264);
volatile uint8_t xdata g_rw_gamvrr0_0265h_GAM_VRR_LUT_OFST12_0_new_data_613                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0265);
volatile uint8_t xdata g_rw_gamvrr0_0266h_GAM_VRR_LUT_OFST12_0_new_data_614                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0266);

volatile uint8_t xdata g_rw_gamvrr0_0267h_GAM_VRR_LUT_OFST12_0_new_data_615                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0267);
volatile uint8_t xdata g_rw_gamvrr0_0268h_GAM_VRR_LUT_OFST12_0_new_data_616                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0268);
volatile uint8_t xdata g_rw_gamvrr0_0269h_GAM_VRR_LUT_OFST12_0_new_data_617                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0269);
volatile uint8_t xdata g_rw_gamvrr0_026Ah_GAM_VRR_LUT_OFST12_0_new_data_618                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x026A);
volatile uint8_t xdata g_rw_gamvrr0_026Bh_GAM_VRR_LUT_OFST12_0_new_data_619                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x026B);

volatile uint8_t xdata g_rw_gamvrr0_026Ch_GAM_VRR_LUT_OFST12_0_new_data_620                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x026C);
volatile uint8_t xdata g_rw_gamvrr0_026Dh_GAM_VRR_LUT_OFST12_0_new_data_621                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x026D);
volatile uint8_t xdata g_rw_gamvrr0_026Eh_GAM_VRR_LUT_OFST12_0_new_data_622                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x026E);
volatile uint8_t xdata g_rw_gamvrr0_026Fh_GAM_VRR_LUT_OFST12_0_new_data_623                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x026F);
volatile uint8_t xdata g_rw_gamvrr0_0270h_GAM_VRR_LUT_OFST12_0_new_data_624                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0270);

volatile uint8_t xdata g_rw_gamvrr0_0271h_GAM_VRR_LUT_OFST12_0_new_data_625                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0271);
volatile uint8_t xdata g_rw_gamvrr0_0272h_GAM_VRR_LUT_OFST12_0_new_data_626                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0272);
volatile uint8_t xdata g_rw_gamvrr0_0273h_GAM_VRR_LUT_OFST12_0_new_data_627                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0273);
volatile uint8_t xdata g_rw_gamvrr0_0274h_GAM_VRR_LUT_OFST12_0_new_data_628                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0274);
volatile uint8_t xdata g_rw_gamvrr0_0275h_GAM_VRR_LUT_OFST12_0_new_data_629                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0275);

volatile uint8_t xdata g_rw_gamvrr0_0276h_GAM_VRR_LUT_OFST12_0_new_data_630                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0276);
volatile uint8_t xdata g_rw_gamvrr0_0277h_GAM_VRR_LUT_OFST12_0_new_data_631                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0277);
volatile uint8_t xdata g_rw_gamvrr0_0278h_GAM_VRR_LUT_OFST12_0_new_data_632                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0278);
volatile uint8_t xdata g_rw_gamvrr0_0279h_GAM_VRR_LUT_OFST12_0_new_data_633                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0279);
volatile uint8_t xdata g_rw_gamvrr0_027Ah_GAM_VRR_LUT_OFST12_0_new_data_634                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x027A);

volatile uint8_t xdata g_rw_gamvrr0_027Bh_GAM_VRR_LUT_OFST12_0_new_data_635                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x027B);
volatile uint8_t xdata g_rw_gamvrr0_027Ch_GAM_VRR_LUT_OFST12_0_new_data_636                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x027C);
volatile uint8_t xdata g_rw_gamvrr0_027Dh_GAM_VRR_LUT_OFST12_0_new_data_637                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x027D);
volatile uint8_t xdata g_rw_gamvrr0_027Eh_GAM_VRR_LUT_OFST12_0_new_data_638                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x027E);
volatile uint8_t xdata g_rw_gamvrr0_027Fh_GAM_VRR_LUT_OFST12_0_new_data_639                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x027F);

volatile uint8_t xdata g_rw_gamvrr0_0280h_GAM_VRR_LUT_OFST12_0_new_data_640                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0280);
volatile uint8_t xdata g_rw_gamvrr0_0281h_GAM_VRR_LUT_OFST12_0_new_data_641                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0281);
volatile uint8_t xdata g_rw_gamvrr0_0282h_GAM_VRR_LUT_OFST12_0_new_data_642                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0282);
volatile uint8_t xdata g_rw_gamvrr0_0283h_GAM_VRR_LUT_OFST12_0_new_data_643                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0283);
volatile uint8_t xdata g_rw_gamvrr0_0284h_GAM_VRR_LUT_OFST12_0_new_data_644                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0284);

volatile uint8_t xdata g_rw_gamvrr0_0285h_GAM_VRR_LUT_OFST12_0_new_data_645                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0285);
volatile uint8_t xdata g_rw_gamvrr0_0286h_GAM_VRR_LUT_OFST12_0_new_data_646                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0286);
volatile uint8_t xdata g_rw_gamvrr0_0287h_GAM_VRR_LUT_OFST12_0_new_data_647                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0287);
volatile uint8_t xdata g_rw_gamvrr0_0288h_GAM_VRR_LUT_OFST12_0_new_data_648                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0288);
volatile uint8_t xdata g_rw_gamvrr0_0289h_GAM_VRR_LUT_OFST12_0_new_data_649                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0289);

volatile uint8_t xdata g_rw_gamvrr0_028Ah_GAM_VRR_LUT_OFST12_0_new_data_650                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x028A);
volatile uint8_t xdata g_rw_gamvrr0_028Bh_GAM_VRR_LUT_OFST12_0_new_data_651                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x028B);
volatile uint8_t xdata g_rw_gamvrr0_028Ch_GAM_VRR_LUT_OFST12_0_new_data_652                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x028C);
volatile uint8_t xdata g_rw_gamvrr0_028Dh_GAM_VRR_LUT_OFST12_0_new_data_653                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x028D);
volatile uint8_t xdata g_rw_gamvrr0_028Eh_GAM_VRR_LUT_OFST12_0_new_data_654                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x028E);

volatile uint8_t xdata g_rw_gamvrr0_028Fh_GAM_VRR_LUT_OFST12_0_new_data_655                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x028F);
volatile uint8_t xdata g_rw_gamvrr0_0290h_GAM_VRR_LUT_OFST12_0_new_data_656                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0290);
volatile uint8_t xdata g_rw_gamvrr0_0291h_GAM_VRR_LUT_OFST12_0_new_data_657                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0291);
volatile uint8_t xdata g_rw_gamvrr0_0292h_GAM_VRR_LUT_OFST12_0_new_data_658                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0292);
volatile uint8_t xdata g_rw_gamvrr0_0293h_GAM_VRR_LUT_OFST12_0_new_data_659                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0293);

volatile uint8_t xdata g_rw_gamvrr0_0294h_GAM_VRR_LUT_OFST12_0_new_data_660                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0294);
volatile uint8_t xdata g_rw_gamvrr0_0295h_GAM_VRR_LUT_OFST12_0_new_data_661                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0295);
volatile uint8_t xdata g_rw_gamvrr0_0296h_GAM_VRR_LUT_OFST12_0_new_data_662                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0296);
volatile uint8_t xdata g_rw_gamvrr0_0297h_GAM_VRR_LUT_OFST12_0_new_data_663                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0297);
volatile uint8_t xdata g_rw_gamvrr0_0298h_GAM_VRR_LUT_OFST12_0_new_data_664                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0298);

volatile uint8_t xdata g_rw_gamvrr0_0299h_GAM_VRR_LUT_OFST12_0_new_data_665                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0299);
volatile uint8_t xdata g_rw_gamvrr0_029Ah_GAM_VRR_LUT_OFST12_0_new_data_666                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x029A);
volatile uint8_t xdata g_rw_gamvrr0_029Bh_GAM_VRR_LUT_OFST12_0_new_data_667                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x029B);
volatile uint8_t xdata g_rw_gamvrr0_029Ch_GAM_VRR_LUT_OFST12_0_new_data_668                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x029C);
volatile uint8_t xdata g_rw_gamvrr0_029Dh_GAM_VRR_LUT_OFST12_0_new_data_669                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x029D);

volatile uint8_t xdata g_rw_gamvrr0_029Eh_GAM_VRR_LUT_OFST12_0_new_data_670                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x029E);
volatile uint8_t xdata g_rw_gamvrr0_029Fh_GAM_VRR_LUT_OFST12_0_new_data_671                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x029F);
volatile uint8_t xdata g_rw_gamvrr0_02A0h_GAM_VRR_LUT_OFST12_0_new_data_672                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02A0);
volatile uint8_t xdata g_rw_gamvrr0_02A1h_GAM_VRR_LUT_OFST12_0_new_data_673                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02A1);
volatile uint8_t xdata g_rw_gamvrr0_02A2h_GAM_VRR_LUT_OFST12_0_new_data_674                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02A2);

volatile uint8_t xdata g_rw_gamvrr0_02A3h_GAM_VRR_LUT_OFST12_0_new_data_675                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02A3);
volatile uint8_t xdata g_rw_gamvrr0_02A4h_GAM_VRR_LUT_OFST12_0_new_data_676                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02A4);
volatile uint8_t xdata g_rw_gamvrr0_02A5h_GAM_VRR_LUT_OFST12_0_new_data_677                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02A5);
volatile uint8_t xdata g_rw_gamvrr0_02A6h_GAM_VRR_LUT_OFST12_0_new_data_678                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02A6);
volatile uint8_t xdata g_rw_gamvrr0_02A7h_GAM_VRR_LUT_OFST12_0_new_data_679                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02A7);

volatile uint8_t xdata g_rw_gamvrr0_02A8h_GAM_VRR_LUT_OFST12_0_new_data_680                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02A8);
volatile uint8_t xdata g_rw_gamvrr0_02A9h_GAM_VRR_LUT_OFST12_0_new_data_681                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02A9);
volatile uint8_t xdata g_rw_gamvrr0_02AAh_GAM_VRR_LUT_OFST12_0_new_data_682                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02AA);
volatile uint8_t xdata g_rw_gamvrr0_02ABh_GAM_VRR_LUT_OFST12_0_new_data_683                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02AB);
volatile uint8_t xdata g_rw_gamvrr0_02ACh_GAM_VRR_LUT_OFST12_0_new_data_684                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02AC);

volatile uint8_t xdata g_rw_gamvrr0_02ADh_GAM_VRR_LUT_OFST12_0_new_data_685                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02AD);
volatile uint8_t xdata g_rw_gamvrr0_02AEh_GAM_VRR_LUT_OFST12_0_new_data_686                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02AE);
volatile uint8_t xdata g_rw_gamvrr0_02AFh_GAM_VRR_LUT_OFST12_0_new_data_687                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02AF);
volatile uint8_t xdata g_rw_gamvrr0_02B0h_GAM_VRR_LUT_OFST12_0_new_data_688                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02B0);
volatile uint8_t xdata g_rw_gamvrr0_02B1h_GAM_VRR_LUT_OFST12_0_new_data_689                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02B1);

volatile uint8_t xdata g_rw_gamvrr0_02B2h_GAM_VRR_LUT_OFST12_0_new_data_690                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02B2);
volatile uint8_t xdata g_rw_gamvrr0_02B3h_GAM_VRR_LUT_OFST12_0_new_data_691                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02B3);
volatile uint8_t xdata g_rw_gamvrr0_02B4h_GAM_VRR_LUT_OFST12_0_new_data_692                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02B4);
volatile uint8_t xdata g_rw_gamvrr0_02B5h_GAM_VRR_LUT_OFST12_0_new_data_693                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02B5);
volatile uint8_t xdata g_rw_gamvrr0_02B6h_GAM_VRR_LUT_OFST12_0_new_data_694                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02B6);

volatile uint8_t xdata g_rw_gamvrr0_02B7h_GAM_VRR_LUT_OFST12_0_new_data_695                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02B7);
volatile uint8_t xdata g_rw_gamvrr0_02B8h_GAM_VRR_LUT_OFST12_0_new_data_696                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02B8);
volatile uint8_t xdata g_rw_gamvrr0_02B9h_GAM_VRR_LUT_OFST12_0_new_data_697                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02B9);
volatile uint8_t xdata g_rw_gamvrr0_02BAh_GAM_VRR_LUT_OFST12_0_new_data_698                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02BA);
volatile uint8_t xdata g_rw_gamvrr0_02BBh_GAM_VRR_LUT_OFST12_0_new_data_699                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02BB);

volatile uint8_t xdata g_rw_gamvrr0_02BCh_GAM_VRR_LUT_OFST12_0_new_data_700                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02BC);
volatile uint8_t xdata g_rw_gamvrr0_02BDh_GAM_VRR_LUT_OFST12_0_new_data_701                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02BD);
volatile uint8_t xdata g_rw_gamvrr0_02BEh_GAM_VRR_LUT_OFST12_0_new_data_702                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02BE);
volatile uint8_t xdata g_rw_gamvrr0_02BFh_GAM_VRR_LUT_OFST12_0_new_data_703                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02BF);
volatile uint8_t xdata g_rw_gamvrr0_02C0h_GAM_VRR_LUT_OFST12_0_new_data_704                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02C0);

volatile uint8_t xdata g_rw_gamvrr0_02C1h_GAM_VRR_LUT_OFST12_0_new_data_705                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02C1);
volatile uint8_t xdata g_rw_gamvrr0_02C2h_GAM_VRR_LUT_OFST12_0_new_data_706                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02C2);
volatile uint8_t xdata g_rw_gamvrr0_02C3h_GAM_VRR_LUT_OFST12_0_new_data_707                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02C3);
volatile uint8_t xdata g_rw_gamvrr0_02C4h_GAM_VRR_LUT_OFST12_0_new_data_708                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02C4);
volatile uint8_t xdata g_rw_gamvrr0_02C5h_GAM_VRR_LUT_OFST12_0_new_data_709                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02C5);

volatile uint8_t xdata g_rw_gamvrr0_02C6h_GAM_VRR_LUT_OFST12_0_new_data_710                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02C6);
volatile uint8_t xdata g_rw_gamvrr0_02C7h_GAM_VRR_LUT_OFST12_0_new_data_711                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02C7);
volatile uint8_t xdata g_rw_gamvrr0_02C8h_GAM_VRR_LUT_OFST12_0_new_data_712                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02C8);
volatile uint8_t xdata g_rw_gamvrr0_02C9h_GAM_VRR_LUT_OFST12_0_new_data_713                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02C9);
volatile uint8_t xdata g_rw_gamvrr0_02CAh_GAM_VRR_LUT_OFST12_0_new_data_714                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02CA);

volatile uint8_t xdata g_rw_gamvrr0_02CBh_GAM_VRR_LUT_OFST12_0_new_data_715                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02CB);
volatile uint8_t xdata g_rw_gamvrr0_02CCh_GAM_VRR_LUT_OFST12_0_new_data_716                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02CC);
volatile uint8_t xdata g_rw_gamvrr0_02CDh_GAM_VRR_LUT_OFST12_0_new_data_717                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02CD);
volatile uint8_t xdata g_rw_gamvrr0_02CEh_GAM_VRR_LUT_OFST12_0_new_data_718                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02CE);
volatile uint8_t xdata g_rw_gamvrr0_02CFh_GAM_VRR_LUT_OFST12_0_new_data_719                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02CF);

volatile uint8_t xdata g_rw_gamvrr0_02D0h_GAM_VRR_LUT_OFST12_0_new_data_720                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02D0);
volatile uint8_t xdata g_rw_gamvrr0_02D1h_GAM_VRR_LUT_OFST12_0_new_data_721                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02D1);
volatile uint8_t xdata g_rw_gamvrr0_02D2h_GAM_VRR_LUT_OFST12_0_new_data_722                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02D2);
volatile uint8_t xdata g_rw_gamvrr0_02D3h_GAM_VRR_LUT_OFST12_0_new_data_723                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02D3);
volatile uint8_t xdata g_rw_gamvrr0_02D4h_GAM_VRR_LUT_OFST12_0_new_data_724                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02D4);

volatile uint8_t xdata g_rw_gamvrr0_02D5h_GAM_VRR_LUT_OFST12_0_new_data_725                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02D5);
volatile uint8_t xdata g_rw_gamvrr0_02D6h_GAM_VRR_LUT_OFST12_0_new_data_726                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02D6);
volatile uint8_t xdata g_rw_gamvrr0_02D7h_GAM_VRR_LUT_OFST12_0_new_data_727                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02D7);
volatile uint8_t xdata g_rw_gamvrr0_02D8h_GAM_VRR_LUT_OFST12_0_new_data_728                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02D8);
volatile uint8_t xdata g_rw_gamvrr0_02D9h_GAM_VRR_LUT_OFST12_0_new_data_729                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02D9);

volatile uint8_t xdata g_rw_gamvrr0_02DAh_GAM_VRR_LUT_OFST12_0_new_data_730                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02DA);
volatile uint8_t xdata g_rw_gamvrr0_02DBh_GAM_VRR_LUT_OFST12_0_new_data_731                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02DB);
volatile uint8_t xdata g_rw_gamvrr0_02DCh_GAM_VRR_LUT_OFST12_0_new_data_732                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02DC);
volatile uint8_t xdata g_rw_gamvrr0_02DDh_GAM_VRR_LUT_OFST12_0_new_data_733                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02DD);
volatile uint8_t xdata g_rw_gamvrr0_02DEh_GAM_VRR_LUT_OFST12_0_new_data_734                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02DE);

volatile uint8_t xdata g_rw_gamvrr0_02DFh_GAM_VRR_LUT_OFST12_0_new_data_735                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02DF);
volatile uint8_t xdata g_rw_gamvrr0_02E0h_GAM_VRR_LUT_OFST12_0_new_data_736                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02E0);
volatile uint8_t xdata g_rw_gamvrr0_02E1h_GAM_VRR_LUT_OFST12_0_new_data_737                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02E1);
volatile uint8_t xdata g_rw_gamvrr0_02E2h_GAM_VRR_LUT_OFST12_0_new_data_738                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02E2);
volatile uint8_t xdata g_rw_gamvrr0_02E3h_GAM_VRR_LUT_OFST12_0_new_data_739                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02E3);

volatile uint8_t xdata g_rw_gamvrr0_02E4h_GAM_VRR_LUT_OFST12_0_new_data_740                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02E4);
volatile uint8_t xdata g_rw_gamvrr0_02E5h_GAM_VRR_LUT_OFST12_0_new_data_741                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02E5);
volatile uint8_t xdata g_rw_gamvrr0_02E6h_GAM_VRR_LUT_OFST12_0_new_data_742                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02E6);
volatile uint8_t xdata g_rw_gamvrr0_02E7h_GAM_VRR_LUT_OFST12_0_new_data_743                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02E7);
volatile uint8_t xdata g_rw_gamvrr0_02E8h_GAM_VRR_LUT_OFST12_0_new_data_744                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02E8);

volatile uint8_t xdata g_rw_gamvrr0_02E9h_GAM_VRR_LUT_OFST12_0_new_data_745                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02E9);
volatile uint8_t xdata g_rw_gamvrr0_02EAh_GAM_VRR_LUT_OFST12_0_new_data_746                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02EA);
volatile uint8_t xdata g_rw_gamvrr0_02EBh_GAM_VRR_LUT_OFST12_0_new_data_747                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02EB);
volatile uint8_t xdata g_rw_gamvrr0_02ECh_GAM_VRR_LUT_OFST12_0_new_data_748                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02EC);
volatile uint8_t xdata g_rw_gamvrr0_02EDh_GAM_VRR_LUT_OFST12_0_new_data_749                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02ED);

volatile uint8_t xdata g_rw_gamvrr0_02EEh_GAM_VRR_LUT_OFST12_0_new_data_750                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02EE);
volatile uint8_t xdata g_rw_gamvrr0_02EFh_GAM_VRR_LUT_OFST12_0_new_data_751                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02EF);
volatile uint8_t xdata g_rw_gamvrr0_02F0h_GAM_VRR_LUT_OFST12_0_new_data_752                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02F0);
volatile uint8_t xdata g_rw_gamvrr0_02F1h_GAM_VRR_LUT_OFST12_0_new_data_753                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02F1);
volatile uint8_t xdata g_rw_gamvrr0_02F2h_GAM_VRR_LUT_OFST12_0_new_data_754                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02F2);

volatile uint8_t xdata g_rw_gamvrr0_02F3h_GAM_VRR_LUT_OFST12_0_new_data_755                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02F3);
volatile uint8_t xdata g_rw_gamvrr0_02F4h_GAM_VRR_LUT_OFST12_0_new_data_756                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02F4);
volatile uint8_t xdata g_rw_gamvrr0_02F5h_GAM_VRR_LUT_OFST12_0_new_data_757                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02F5);
volatile uint8_t xdata g_rw_gamvrr0_02F6h_GAM_VRR_LUT_OFST12_0_new_data_758                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02F6);
volatile uint8_t xdata g_rw_gamvrr0_02F7h_GAM_VRR_LUT_OFST12_0_new_data_759                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02F7);

volatile uint8_t xdata g_rw_gamvrr0_02F8h_GAM_VRR_LUT_OFST12_0_new_data_760                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02F8);
volatile uint8_t xdata g_rw_gamvrr0_02F9h_GAM_VRR_LUT_OFST12_0_new_data_761                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02F9);
volatile uint8_t xdata g_rw_gamvrr0_02FAh_GAM_VRR_LUT_OFST12_0_new_data_762                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02FA);
volatile uint8_t xdata g_rw_gamvrr0_02FBh_GAM_VRR_LUT_OFST12_0_new_data_763                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02FB);
volatile uint8_t xdata g_rw_gamvrr0_02FCh_GAM_VRR_LUT_OFST12_0_new_data_764                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02FC);

volatile uint8_t xdata g_rw_gamvrr0_02FDh_GAM_VRR_LUT_OFST12_0_new_data_765                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02FD);
volatile uint8_t xdata g_rw_gamvrr0_02FEh_GAM_VRR_LUT_OFST12_0_new_data_766                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02FE);
volatile uint8_t xdata g_rw_gamvrr0_02FFh_GAM_VRR_LUT_OFST12_0_new_data_767                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x02FF);
volatile uint8_t xdata g_rw_gamvrr0_0300h_GAM_VRR_LUT_OFST12_0_new_data_768                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0300);
volatile uint8_t xdata g_rw_gamvrr0_0301h_GAM_VRR_LUT_OFST12_0_new_data_769                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0301);

volatile uint8_t xdata g_rw_gamvrr0_0302h_GAM_VRR_LUT_OFST12_0_new_data_770                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0302);
volatile uint8_t xdata g_rw_gamvrr0_0303h_GAM_VRR_LUT_OFST12_0_new_data_771                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0303);
volatile uint8_t xdata g_rw_gamvrr0_0304h_GAM_VRR_LUT_OFST12_0_new_data_772                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0304);
volatile uint8_t xdata g_rw_gamvrr0_0305h_GAM_VRR_LUT_OFST12_0_new_data_773                    _at_  (GAMVRR0_FIELD_XMEM_BASE + 0x0305);

#endif 
