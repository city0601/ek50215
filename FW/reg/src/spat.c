#include "spat.h"
#include "hw_mem_map.h"

#ifdef SPAT_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------
volatile union rw_spat_0018h xdata g_rw_spat_0018h  _at_  (SPAT_FIELD_XMEM_BASE + 0x0018);
volatile union rw_spat_001Ah xdata g_rw_spat_001Ah  _at_  (SPAT_FIELD_XMEM_BASE + 0x001A);
volatile union rw_spat_001Dh xdata g_rw_spat_001Dh  _at_  (SPAT_FIELD_XMEM_BASE + 0x001D);
volatile union rw_spat_0020h xdata g_rw_spat_0020h  _at_  (SPAT_FIELD_XMEM_BASE + 0x0020);
volatile union rw_spat_0030h xdata g_rw_spat_0030h  _at_  (SPAT_FIELD_XMEM_BASE + 0x0030);

volatile union rw_spat_0031h xdata g_rw_spat_0031h  _at_  (SPAT_FIELD_XMEM_BASE + 0x0031);
volatile union rw_spat_0035h xdata g_rw_spat_0035h  _at_  (SPAT_FIELD_XMEM_BASE + 0x0035);

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_spat_0000h_aging0                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0000);
volatile uint8_t xdata g_rw_spat_0001h_aging0                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0001);
volatile uint8_t xdata g_rw_spat_0002h_aging0                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0002);
volatile uint8_t xdata g_rw_spat_0003h_aging0                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0003);
volatile uint8_t xdata g_rw_spat_0004h_aging1                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0004);

volatile uint8_t xdata g_rw_spat_0005h_aging1                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0005);
volatile uint8_t xdata g_rw_spat_0006h_aging1                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0006);
volatile uint8_t xdata g_rw_spat_0007h_aging1                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0007);
volatile uint8_t xdata g_rw_spat_0008h_aging2                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0008);
volatile uint8_t xdata g_rw_spat_0009h_aging2                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0009);

volatile uint8_t xdata g_rw_spat_000Ah_aging2                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x000A);
volatile uint8_t xdata g_rw_spat_000Bh_aging2                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x000B);
volatile uint8_t xdata g_rw_spat_000Ch_aging3                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x000C);
volatile uint8_t xdata g_rw_spat_000Dh_aging3                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x000D);
volatile uint8_t xdata g_rw_spat_000Eh_aging3                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x000E);

volatile uint8_t xdata g_rw_spat_000Fh_aging3                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x000F);
volatile uint8_t xdata g_rw_spat_0010h_aging4                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0010);
volatile uint8_t xdata g_rw_spat_0011h_aging4                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0011);
volatile uint8_t xdata g_rw_spat_0012h_aging4                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0012);
volatile uint8_t xdata g_rw_spat_0013h_aging4                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0013);

volatile uint8_t xdata g_rw_spat_0014h_aging5                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0014);
volatile uint8_t xdata g_rw_spat_0015h_aging5                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0015);
volatile uint8_t xdata g_rw_spat_0016h_aging5                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0016);
volatile uint8_t xdata g_rw_spat_0017h_aging5                                                  _at_  (SPAT_FIELD_XMEM_BASE + 0x0017);
volatile uint8_t xdata g_rw_spat_0019h_box_w                                                   _at_  (SPAT_FIELD_XMEM_BASE + 0x0019);

volatile uint8_t xdata g_rw_spat_001Bh_box_h                                                   _at_  (SPAT_FIELD_XMEM_BASE + 0x001B);
volatile uint8_t xdata g_rw_spat_001Ch_vp1                                                     _at_  (SPAT_FIELD_XMEM_BASE + 0x001C);
volatile uint8_t xdata g_rw_spat_001Eh_vp2                                                     _at_  (SPAT_FIELD_XMEM_BASE + 0x001E);
volatile uint8_t xdata g_rw_spat_001Fh_hp1                                                     _at_  (SPAT_FIELD_XMEM_BASE + 0x001F);
volatile uint8_t xdata g_rw_spat_0021h_hp2                                                     _at_  (SPAT_FIELD_XMEM_BASE + 0x0021);

volatile uint8_t xdata g_rw_spat_0022h_flicker_gray1                                           _at_  (SPAT_FIELD_XMEM_BASE + 0x0022);
volatile uint8_t xdata g_rw_spat_0023h_aging_w                                                 _at_  (SPAT_FIELD_XMEM_BASE + 0x0023);
volatile uint8_t xdata g_rw_spat_0024h_flicker_gray2                                           _at_  (SPAT_FIELD_XMEM_BASE + 0x0024);
volatile uint8_t xdata g_rw_spat_0025h_flicker_r                                               _at_  (SPAT_FIELD_XMEM_BASE + 0x0025);
volatile uint8_t xdata g_rw_spat_0026h_flicker_g                                               _at_  (SPAT_FIELD_XMEM_BASE + 0x0026);

volatile uint8_t xdata g_rw_spat_0027h_flicker_b                                               _at_  (SPAT_FIELD_XMEM_BASE + 0x0027);
volatile uint8_t xdata g_rw_spat_0028h_flicker_pat_num                                         _at_  (SPAT_FIELD_XMEM_BASE + 0x0028);
volatile uint8_t xdata g_rw_spat_0029h_flicker_gray1_for_pin                                   _at_  (SPAT_FIELD_XMEM_BASE + 0x0029);
volatile uint8_t xdata g_rw_spat_002Ah_flicker_gray2_for_pin                                   _at_  (SPAT_FIELD_XMEM_BASE + 0x002A);
volatile uint8_t xdata g_rw_spat_002Bh_fcnt_0_1sec                                             _at_  (SPAT_FIELD_XMEM_BASE + 0x002B);

volatile uint8_t xdata g_rw_spat_002Ch_fcnt_1_1sec                                             _at_  (SPAT_FIELD_XMEM_BASE + 0x002C);
volatile uint8_t xdata g_rw_spat_002Dh_fcnt_2_1sec                                             _at_  (SPAT_FIELD_XMEM_BASE + 0x002D);
volatile uint8_t xdata g_rw_spat_002Eh_fcnt_3_1sec                                             _at_  (SPAT_FIELD_XMEM_BASE + 0x002E);
volatile uint8_t xdata g_rw_spat_002Fh_fcnt_sub_1sec                                           _at_  (SPAT_FIELD_XMEM_BASE + 0x002F);
volatile uint8_t xdata g_rw_spat_0032h_flicker_loc_r                                           _at_  (SPAT_FIELD_XMEM_BASE + 0x0032);

volatile uint8_t xdata g_rw_spat_0033h_flicker_loc_g                                           _at_  (SPAT_FIELD_XMEM_BASE + 0x0033);
volatile uint8_t xdata g_rw_spat_0034h_flicker_loc_b                                           _at_  (SPAT_FIELD_XMEM_BASE + 0x0034);

#endif 
