#include "efuse.h"
#include "hw_mem_map.h"

#ifdef EFUSE_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------
volatile union rw_efuse_0002h xdata g_rw_efuse_0002h  _at_  (EFUSE_FIELD_XMEM_BASE + 0x0002);
volatile union rw_efuse_0003h xdata g_rw_efuse_0003h  _at_  (EFUSE_FIELD_XMEM_BASE + 0x0003);
volatile union rw_efuse_0006h xdata g_rw_efuse_0006h  _at_  (EFUSE_FIELD_XMEM_BASE + 0x0006);
volatile union rw_efuse_0007h xdata g_rw_efuse_0007h  _at_  (EFUSE_FIELD_XMEM_BASE + 0x0007);
volatile union rw_efuse_0008h xdata g_rw_efuse_0008h  _at_  (EFUSE_FIELD_XMEM_BASE + 0x0008);

volatile union rw_efuse_000Ah xdata g_rw_efuse_000Ah  _at_  (EFUSE_FIELD_XMEM_BASE + 0x000A);
volatile union rw_efuse_000Bh xdata g_rw_efuse_000Bh  _at_  (EFUSE_FIELD_XMEM_BASE + 0x000B);
volatile union rw_efuse_000Dh xdata g_rw_efuse_000Dh  _at_  (EFUSE_FIELD_XMEM_BASE + 0x000D);
volatile union rw_efuse_000Eh xdata g_rw_efuse_000Eh  _at_  (EFUSE_FIELD_XMEM_BASE + 0x000E);
volatile union rw_efuse_0010h xdata g_rw_efuse_0010h  _at_  (EFUSE_FIELD_XMEM_BASE + 0x0010);

volatile union rw_efuse_0011h xdata g_rw_efuse_0011h  _at_  (EFUSE_FIELD_XMEM_BASE + 0x0011);
volatile union rw_efuse_0012h xdata g_rw_efuse_0012h  _at_  (EFUSE_FIELD_XMEM_BASE + 0x0012);
volatile union rw_efuse_0013h xdata g_rw_efuse_0013h  _at_  (EFUSE_FIELD_XMEM_BASE + 0x0013);
volatile union rw_efuse_0014h xdata g_rw_efuse_0014h  _at_  (EFUSE_FIELD_XMEM_BASE + 0x0014);
volatile union rw_efuse_0015h xdata g_rw_efuse_0015h  _at_  (EFUSE_FIELD_XMEM_BASE + 0x0015);

volatile union rw_efuse_0016h xdata g_rw_efuse_0016h  _at_  (EFUSE_FIELD_XMEM_BASE + 0x0016);
volatile union rw_efuse_0017h xdata g_rw_efuse_0017h  _at_  (EFUSE_FIELD_XMEM_BASE + 0x0017);
volatile union rw_efuse_0018h xdata g_rw_efuse_0018h  _at_  (EFUSE_FIELD_XMEM_BASE + 0x0018);
volatile union rw_efuse_0019h xdata g_rw_efuse_0019h  _at_  (EFUSE_FIELD_XMEM_BASE + 0x0019);
volatile union rw_efuse_001Ah xdata g_rw_efuse_001Ah  _at_  (EFUSE_FIELD_XMEM_BASE + 0x001A);

volatile union rw_efuse_001Bh xdata g_rw_efuse_001Bh  _at_  (EFUSE_FIELD_XMEM_BASE + 0x001B);

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_efuse_0000h_Project_id_LSB                                         _at_  (EFUSE_FIELD_XMEM_BASE + 0x0000);
volatile uint8_t xdata g_rw_efuse_0001h_Project_id_MSB                                         _at_  (EFUSE_FIELD_XMEM_BASE + 0x0001);
volatile uint8_t xdata g_rw_efuse_0004h_osc_cs_trim                                            _at_  (EFUSE_FIELD_XMEM_BASE + 0x0004);
volatile uint8_t xdata g_rw_efuse_0005h_osc_reserve                                            _at_  (EFUSE_FIELD_XMEM_BASE + 0x0005);
volatile uint8_t xdata g_rw_efuse_0009h_tx_reserve_5                                           _at_  (EFUSE_FIELD_XMEM_BASE + 0x0009);

volatile uint8_t xdata g_rw_efuse_000Ch_rx_reserve_2                                           _at_  (EFUSE_FIELD_XMEM_BASE + 0x000C);
volatile uint8_t xdata g_rw_efuse_000Fh_mcu_o                                                  _at_  (EFUSE_FIELD_XMEM_BASE + 0x000F);

#endif 
