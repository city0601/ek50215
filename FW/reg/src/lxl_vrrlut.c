#include "lxl_vrrlut.h"
#include "hw_mem_map.h"

#ifdef LXL_VRRLUT_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_lxl_vrrlut_0000h_LXL_LUT0_data_0                                   _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0000);
volatile uint8_t xdata g_rw_lxl_vrrlut_0001h_LXL_LUT0_data_1                                   _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0001);
volatile uint8_t xdata g_rw_lxl_vrrlut_0002h_LXL_LUT0_data_2                                   _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0002);
volatile uint8_t xdata g_rw_lxl_vrrlut_0003h_LXL_LUT0_data_3                                   _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0003);
volatile uint8_t xdata g_rw_lxl_vrrlut_0004h_LXL_LUT0_data_4                                   _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0004);

volatile uint8_t xdata g_rw_lxl_vrrlut_0005h_LXL_LUT0_data_5                                   _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0005);
volatile uint8_t xdata g_rw_lxl_vrrlut_0006h_LXL_LUT0_data_6                                   _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0006);
volatile uint8_t xdata g_rw_lxl_vrrlut_0007h_LXL_LUT0_data_7                                   _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0007);
volatile uint8_t xdata g_rw_lxl_vrrlut_0008h_LXL_LUT0_data_8                                   _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0008);
volatile uint8_t xdata g_rw_lxl_vrrlut_0009h_LXL_LUT0_data_9                                   _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0009);

volatile uint8_t xdata g_rw_lxl_vrrlut_000Ah_LXL_LUT0_data_10                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x000A);
volatile uint8_t xdata g_rw_lxl_vrrlut_000Bh_LXL_LUT0_data_11                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x000B);
volatile uint8_t xdata g_rw_lxl_vrrlut_000Ch_LXL_LUT0_data_12                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x000C);
volatile uint8_t xdata g_rw_lxl_vrrlut_000Dh_LXL_LUT0_data_13                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x000D);
volatile uint8_t xdata g_rw_lxl_vrrlut_000Eh_LXL_LUT0_data_14                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x000E);

volatile uint8_t xdata g_rw_lxl_vrrlut_000Fh_LXL_LUT0_data_15                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x000F);
volatile uint8_t xdata g_rw_lxl_vrrlut_0010h_LXL_LUT0_data_16                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0010);
volatile uint8_t xdata g_rw_lxl_vrrlut_0011h_LXL_LUT0_data_17                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0011);
volatile uint8_t xdata g_rw_lxl_vrrlut_0012h_LXL_LUT0_data_18                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0012);
volatile uint8_t xdata g_rw_lxl_vrrlut_0013h_LXL_LUT0_data_19                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0013);

volatile uint8_t xdata g_rw_lxl_vrrlut_0014h_LXL_LUT0_data_20                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0014);
volatile uint8_t xdata g_rw_lxl_vrrlut_0015h_LXL_LUT0_data_21                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0015);
volatile uint8_t xdata g_rw_lxl_vrrlut_0016h_LXL_LUT0_data_22                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0016);
volatile uint8_t xdata g_rw_lxl_vrrlut_0017h_LXL_LUT0_data_23                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0017);
volatile uint8_t xdata g_rw_lxl_vrrlut_0018h_LXL_LUT0_data_24                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0018);

volatile uint8_t xdata g_rw_lxl_vrrlut_0019h_LXL_LUT0_data_25                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0019);
volatile uint8_t xdata g_rw_lxl_vrrlut_001Ah_LXL_LUT0_data_26                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x001A);
volatile uint8_t xdata g_rw_lxl_vrrlut_001Bh_LXL_LUT0_data_27                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x001B);
volatile uint8_t xdata g_rw_lxl_vrrlut_001Ch_LXL_LUT0_data_28                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x001C);
volatile uint8_t xdata g_rw_lxl_vrrlut_001Dh_LXL_LUT0_data_29                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x001D);

volatile uint8_t xdata g_rw_lxl_vrrlut_001Eh_LXL_LUT0_data_30                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x001E);
volatile uint8_t xdata g_rw_lxl_vrrlut_001Fh_LXL_LUT0_data_31                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x001F);
volatile uint8_t xdata g_rw_lxl_vrrlut_0020h_LXL_LUT0_data_32                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0020);
volatile uint8_t xdata g_rw_lxl_vrrlut_0021h_LXL_LUT0_data_33                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0021);
volatile uint8_t xdata g_rw_lxl_vrrlut_0022h_LXL_LUT0_data_34                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0022);

volatile uint8_t xdata g_rw_lxl_vrrlut_0023h_LXL_LUT0_data_35                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0023);
volatile uint8_t xdata g_rw_lxl_vrrlut_0024h_LXL_LUT0_data_36                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0024);
volatile uint8_t xdata g_rw_lxl_vrrlut_0025h_LXL_LUT0_data_37                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0025);
volatile uint8_t xdata g_rw_lxl_vrrlut_0026h_LXL_LUT0_data_38                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0026);
volatile uint8_t xdata g_rw_lxl_vrrlut_0027h_LXL_LUT0_data_39                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0027);

volatile uint8_t xdata g_rw_lxl_vrrlut_0028h_LXL_LUT0_data_40                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0028);
volatile uint8_t xdata g_rw_lxl_vrrlut_0029h_LXL_LUT0_data_41                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0029);
volatile uint8_t xdata g_rw_lxl_vrrlut_002Ah_LXL_LUT0_data_42                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x002A);
volatile uint8_t xdata g_rw_lxl_vrrlut_002Bh_LXL_LUT0_data_43                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x002B);
volatile uint8_t xdata g_rw_lxl_vrrlut_002Ch_LXL_LUT0_data_44                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x002C);

volatile uint8_t xdata g_rw_lxl_vrrlut_002Dh_LXL_LUT0_data_45                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x002D);
volatile uint8_t xdata g_rw_lxl_vrrlut_002Eh_LXL_LUT0_data_46                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x002E);
volatile uint8_t xdata g_rw_lxl_vrrlut_002Fh_LXL_LUT0_data_47                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x002F);
volatile uint8_t xdata g_rw_lxl_vrrlut_0030h_LXL_LUT0_data_48                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0030);
volatile uint8_t xdata g_rw_lxl_vrrlut_0031h_LXL_LUT0_data_49                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0031);

volatile uint8_t xdata g_rw_lxl_vrrlut_0032h_LXL_LUT0_data_50                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0032);
volatile uint8_t xdata g_rw_lxl_vrrlut_0033h_LXL_LUT0_data_51                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0033);
volatile uint8_t xdata g_rw_lxl_vrrlut_0034h_LXL_LUT0_data_52                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0034);
volatile uint8_t xdata g_rw_lxl_vrrlut_0035h_LXL_LUT0_data_53                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0035);
volatile uint8_t xdata g_rw_lxl_vrrlut_0036h_LXL_LUT0_data_54                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0036);

volatile uint8_t xdata g_rw_lxl_vrrlut_0037h_LXL_LUT0_data_55                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0037);
volatile uint8_t xdata g_rw_lxl_vrrlut_0038h_LXL_LUT0_data_56                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0038);
volatile uint8_t xdata g_rw_lxl_vrrlut_0039h_LXL_LUT0_data_57                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0039);
volatile uint8_t xdata g_rw_lxl_vrrlut_003Ah_LXL_LUT0_data_58                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x003A);
volatile uint8_t xdata g_rw_lxl_vrrlut_003Bh_LXL_LUT0_data_59                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x003B);

volatile uint8_t xdata g_rw_lxl_vrrlut_003Ch_LXL_LUT0_data_60                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x003C);
volatile uint8_t xdata g_rw_lxl_vrrlut_003Dh_LXL_LUT0_data_61                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x003D);
volatile uint8_t xdata g_rw_lxl_vrrlut_003Eh_LXL_LUT0_data_62                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x003E);
volatile uint8_t xdata g_rw_lxl_vrrlut_003Fh_LXL_LUT0_data_63                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x003F);
volatile uint8_t xdata g_rw_lxl_vrrlut_0040h_LXL_LUT0_data_64                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0040);

volatile uint8_t xdata g_rw_lxl_vrrlut_0041h_LXL_LUT0_data_65                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0041);
volatile uint8_t xdata g_rw_lxl_vrrlut_0042h_LXL_LUT0_data_66                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0042);
volatile uint8_t xdata g_rw_lxl_vrrlut_0043h_LXL_LUT0_data_67                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0043);
volatile uint8_t xdata g_rw_lxl_vrrlut_0044h_LXL_LUT0_data_68                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0044);
volatile uint8_t xdata g_rw_lxl_vrrlut_0045h_LXL_LUT0_data_69                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0045);

volatile uint8_t xdata g_rw_lxl_vrrlut_0046h_LXL_LUT0_data_70                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0046);
volatile uint8_t xdata g_rw_lxl_vrrlut_0047h_LXL_LUT0_data_71                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0047);
volatile uint8_t xdata g_rw_lxl_vrrlut_0048h_LXL_LUT0_data_72                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0048);
volatile uint8_t xdata g_rw_lxl_vrrlut_0049h_LXL_LUT0_data_73                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0049);
volatile uint8_t xdata g_rw_lxl_vrrlut_004Ah_LXL_LUT0_data_74                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x004A);

volatile uint8_t xdata g_rw_lxl_vrrlut_004Bh_LXL_LUT0_data_75                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x004B);
volatile uint8_t xdata g_rw_lxl_vrrlut_004Ch_LXL_LUT0_data_76                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x004C);
volatile uint8_t xdata g_rw_lxl_vrrlut_004Dh_LXL_LUT0_data_77                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x004D);
volatile uint8_t xdata g_rw_lxl_vrrlut_004Eh_LXL_LUT0_data_78                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x004E);
volatile uint8_t xdata g_rw_lxl_vrrlut_004Fh_LXL_LUT0_data_79                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x004F);

volatile uint8_t xdata g_rw_lxl_vrrlut_0050h_LXL_LUT0_data_80                                  _at_  (LXL_VRRLUT_FIELD_XMEM_BASE + 0x0050);

#endif 
