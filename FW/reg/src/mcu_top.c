#include "mcu_top.h"
#include "hw_mem_map.h"

#ifdef MCU_TOP_FIELD_XMEM_BASE
//------------------------------------------------------------------------------------------------------------------------
volatile union rw_mcu_top_0000h xdata g_rw_mcu_top_0000h  _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0000);
volatile union rw_mcu_top_0001h xdata g_rw_mcu_top_0001h  _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0001);
volatile union rw_mcu_top_0002h xdata g_rw_mcu_top_0002h  _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0002);
volatile union rw_mcu_top_0003h xdata g_rw_mcu_top_0003h  _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0003);
volatile union rw_mcu_top_0004h xdata g_rw_mcu_top_0004h  _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0004);

volatile union rw_mcu_top_0006h xdata g_rw_mcu_top_0006h  _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0006);
volatile union rw_mcu_top_0007h xdata g_rw_mcu_top_0007h  _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0007);
volatile union rw_mcu_top_0009h xdata g_rw_mcu_top_0009h  _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0009);
volatile union rw_mcu_top_000Ah xdata g_rw_mcu_top_000Ah  _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x000A);
volatile union rw_mcu_top_0011h xdata g_rw_mcu_top_0011h  _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0011);

volatile union rw_mcu_top_0013h xdata g_rw_mcu_top_0013h  _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0013);
volatile union rw_mcu_top_0020h xdata g_rw_mcu_top_0020h  _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0020);
volatile union rw_mcu_top_0027h xdata g_rw_mcu_top_0027h  _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0027);
volatile union rw_mcu_top_0035h xdata g_rw_mcu_top_0035h  _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0035);
volatile union rw_mcu_top_0036h xdata g_rw_mcu_top_0036h  _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0036);

volatile union rw_mcu_top_0037h xdata g_rw_mcu_top_0037h  _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0037);

//------------------------------------------------------------------------------------------------------------------------
volatile uint8_t xdata g_rw_mcu_top_0005h_pgm_t_unit                                           _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0005);
volatile uint8_t xdata g_rw_mcu_top_0008h_init_ready_cnt                                       _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0008);
volatile uint8_t xdata g_rw_mcu_top_000Bh_pmic_banka_start_adr                                 _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x000B);
volatile uint8_t xdata g_rw_mcu_top_000Ch_pmic_bankb_start_adr                                 _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x000C);
volatile uint8_t xdata g_rw_mcu_top_000Dh_pmic_banka_len                                       _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x000D);

volatile uint8_t xdata g_rw_mcu_top_000Eh_pmic_bankb_len                                       _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x000E);
volatile uint8_t xdata g_rw_mcu_top_000Fh_pmic_header_len                                      _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x000F);
volatile uint8_t xdata g_rw_mcu_top_0010h_pmic_gma_len                                         _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0010);
volatile uint8_t xdata g_rw_mcu_top_0012h_pmic_status_adr                                      _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0012);
volatile uint8_t xdata g_rw_mcu_top_0014h_x_board_pmic_hdr_adr1                                _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0014);

volatile uint8_t xdata g_rw_mcu_top_0015h_x_board_pmic_hdr_adr2                                _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0015);
volatile uint8_t xdata g_rw_mcu_top_0016h_x_board_pmic_hdr_adr3                                _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0016);
volatile uint8_t xdata g_rw_mcu_top_0017h_x_board_pmic_gma_adr1                                _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0017);
volatile uint8_t xdata g_rw_mcu_top_0018h_x_board_pmic_gma_adr2                                _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0018);
volatile uint8_t xdata g_rw_mcu_top_0019h_x_board_pmic_gma_adr3                                _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0019);

volatile uint8_t xdata g_rw_mcu_top_001Ah_x_board_pmic_bankA_adr1                              _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x001A);
volatile uint8_t xdata g_rw_mcu_top_001Bh_x_board_pmic_bankA_adr2                              _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x001B);
volatile uint8_t xdata g_rw_mcu_top_001Ch_x_board_pmic_bankA_adr3                              _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x001C);
volatile uint8_t xdata g_rw_mcu_top_001Dh_x_board_pmic_bankB_adr1                              _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x001D);
volatile uint8_t xdata g_rw_mcu_top_001Eh_x_board_pmic_bankB_adr2                              _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x001E);

volatile uint8_t xdata g_rw_mcu_top_001Fh_x_board_pmic_bankB_adr3                              _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x001F);
volatile uint8_t xdata g_rw_mcu_top_0021h_x_board_DGC_adr1                                     _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0021);
volatile uint8_t xdata g_rw_mcu_top_0022h_x_board_DGC_adr2                                     _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0022);
volatile uint8_t xdata g_rw_mcu_top_0023h_x_board_DGC_adr3                                     _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0023);
volatile uint8_t xdata g_rw_mcu_top_0024h_x_board_VCOM_adr1                                    _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0024);

volatile uint8_t xdata g_rw_mcu_top_0025h_x_board_VCOM_adr2                                    _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0025);
volatile uint8_t xdata g_rw_mcu_top_0026h_x_board_VCOM_adr3                                    _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0026);
volatile uint8_t xdata g_rw_mcu_top_0028h_i2c_over_fw_addr1                                    _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0028);
volatile uint8_t xdata g_rw_mcu_top_0029h_i2c_over_fw_addr2                                    _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0029);
volatile uint8_t xdata g_rw_mcu_top_002Ah_i2c_target_num                                       _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x002A);

volatile uint8_t xdata g_rw_mcu_top_002Bh_i2c_over_fw_dev                                      _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x002B);
volatile uint8_t xdata g_rw_mcu_top_002Ch_i2c_over_data1                                       _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x002C);
volatile uint8_t xdata g_rw_mcu_top_002Dh_i2c_over_data2                                       _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x002D);
volatile uint8_t xdata g_rw_mcu_top_002Eh_i2c_over_data3                                       _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x002E);
volatile uint8_t xdata g_rw_mcu_top_002Fh_i2c_over_data4                                       _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x002F);

volatile uint8_t xdata g_rw_mcu_top_0030h_i2c_over_data5                                       _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0030);
volatile uint8_t xdata g_rw_mcu_top_0031h_i2c_over_data6                                       _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0031);
volatile uint8_t xdata g_rw_mcu_top_0032h_i2c_over_data7                                       _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0032);
volatile uint8_t xdata g_rw_mcu_top_0033h_i2c_over_data8                                       _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0033);
volatile uint8_t xdata g_rw_mcu_top_0034h_i2c_over_cnt                                         _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0034);

volatile uint8_t xdata g_rw_mcu_top_0038h_vendor_id                                            _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0038);
volatile uint8_t xdata g_rw_mcu_top_0039h_tcon_id1                                             _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x0039);
volatile uint8_t xdata g_rw_mcu_top_003Ah_tcon_id2                                             _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x003A);
volatile uint8_t xdata g_rw_mcu_top_003Bh_mid                                                  _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x003B);
volatile uint8_t xdata g_rw_mcu_top_003Ch_did1                                                 _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x003C);

volatile uint8_t xdata g_rw_mcu_top_003Dh_did2                                                 _at_  (MCU_TOP_FIELD_XMEM_BASE + 0x003D);

#endif 
