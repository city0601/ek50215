@echo off & setlocal enabledelayedexpansion
set "target=ek50215_app.dig"
set /a "num=8192"
set /a "line=0"
set /a "file_index=1"
for %%a in (%target%) do (
  set file=%%~na
  set ext=%%~xa
)
:_loop
set "tempfile={{%random%}}.{{%random%}}.~tmp"
if exist "tempfile" goto _loop
break>%tempfile%
for /f "tokens=*" %%a in (%target%) do (
  set /a "line+=1"
  echo %%a>>%tempfile%
  if "!line!"=="%num%" (
    copy /y "%tempfile%" "%file%_!line!_!file_index!%ext%" >nul 2>&1
    break>%tempfile%
    set /a "file_index+=1"
    set /a "line=0"
  )
)
copy /y "%tempfile%" "%file%_!line!_!file_index!%ext%" >nul 2>&1
:end
del /f /q "%tempfile%" >nul 2>&1
endlocal & @echo on