import os
import re

class Module:
    def __init__(self, name):
        self.name = name
        self.bit   = 0
        self.data  = 0
        self.idata = 0
        self.pdata = 0
        self.xdata = 0
        self.code  = 0
        self.const = 0
##END: class

def getMapFileName():
    fName = None
    files = os.listdir(".")
    for file in files:
        if file.endswith(".map"):
            return file
    print("No map file found (.map)")
    exit(1)

def parse(file):
    type = None
    data = {}
    fh = open(file)
    for line in fh.readlines():
        """
        M51 value examples:
        000015H - 0x15 bytes
        00001FH.7 - 0x1F bytes and 7 bits

        Groups:
        1. Number of bytes hex
        2. Number of bits in decimal (0-7)
        """
        valueFormat = "([0-9A-F]+)H(?:\.(\d))?\s+"

        """
        M51 specifier examples:
        BYTE
        UNIT
        DATA

        Groups:
        1. Specifier string
        """
        specifierFormat = "([A-Z]+)\s+"

        """
        M51 segment examples:
        ?DT?SPI_0
        ?PR?SPI0_ISR?SPI_0
        ?PR?GETTICKCOUNT?TICK

        Groups:
        1. Segment type (DT, PR, CO, etc)
        2. Function name
        3. Module name
        """
        segmentFormat = "\??([A-Z0-9_]+)?\??([A-Z0-9_]+)?\?([A-Z0-9_]+)"

        format = "\s*" + \
                 valueFormat + \
                 valueFormat + \
                 valueFormat + \
                 specifierFormat + \
                 specifierFormat + \
                 specifierFormat + \
                 segmentFormat

        m = re.match(format, line)
        if m:
            type     = m.group(9)
            length   = int(m.group(5), 16)
            bitLength = int(m.group(6)) if m.group(6) != None else 0
            function = m.group(11)
            module   = m.group(12)
            
            if module not in data:
                data[module] = Module(module)

            if type == "BIT":
                data[module].bit += length * 8 + bitLength
            elif type == "DATA":
                data[module].data += length
            elif type == "IDATA":
                data[module].idata += length
            elif type == "PDATA":
                data[module].pdata += length
            elif type == "XDATA":
                data[module].xdata += length
            elif type == "CODE":
                data[module].code += length
            elif type == "CONST":
                data[module].const += length
            else:
                print("Unknown memory type {} from {}.{}".format(type, module, function))

            ##DGB
            ##print("?{}?{}?{} = {}".format(object, function, module, data[module]))
        ##END: if match
    ##END: for line in file
    fh.close()
    return data


def printSummary():
    fName = getMapFileName()
    print("Processing {}".format(fName))

    data = parse(fName)

    fmt = "{0:20} {1:5} {2:5} {3:5} {4:5} {5:5} {6:5} {7:5}"
    print(fmt.format("", "BIT", "DATA", "IDATA", "PDATA", "XDATA", "CODE", "CONST"))
    tBit = 0
    tData = 0
    tIdata = 0
    tPdata = 0
    tXdata = 0
    tCode = 0
    tConst = 0
    for module in sorted(data.values(), key=lambda x: x.name):
        print(fmt.format(module.name, module.bit, module.data, module.idata, module.pdata, module.xdata, module.code, module.const))
        tBit += module.bit
        tData += module.data
        tIdata += module.idata
        tPdata += module.pdata
        tXdata += module.xdata
        tCode += module.code
        tConst += module.const

    print("--------------------------------------------------------------")
    print(fmt.format("TOTAL", tBit, tData, tIdata, tPdata, tXdata, tCode, tConst))

if __name__ == "__main__":
    printSummary()
