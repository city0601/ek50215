
copy .\Objects\ek50215_app.hex .

hex2bin -s 0000 -l 3000 -p 00 -k 5 -r 0000 2FFF -f 2FFE ek50215_app.hex

bin2txt ek50215_app.bin ek50215_app.txt

bin2dig ek50215_app.bin ek50215_app.dig


srec_cat.exe ek50215_app.hex -Intel -crop 0x0000 0x1FFF -o ek50215_app_8k.hex -Intel

hex2bin -s 0000 -l 2000 -p 00 ek50215_app_8k.hex

bin2txt ek50215_app_8k.bin ek50215_app_8k.txt

bin2dig ek50215_app_8k.bin ek50215_app_8k.dig


srec_cat.exe ek50215_app.hex -Intel -crop 0x002000 0x002FFF -offset -0x002000 -o ek50215_app_2k.hex -Intel

hex2bin -s 0000 -l 0800 -p 00 ek50215_app_2k.hex

bin2txt ek50215_app_2k.bin ek50215_app_2k.txt

bin2dig ek50215_app_2k.bin ek50215_app_2k.dig
