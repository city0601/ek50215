1. 複製程式編譯檔(ex. ek50508_app.txt)至CodeSplit資料夾。

2. 輸入Information.txt內容的Filename, AverageSplitSize, RomSize, RamSize。

3. 執行CodeSplit，輸入Y/y後依AverageSplitSize數值平均分割編譯檔，輸入N/n依Rom/Ram Size數值分割編譯檔。

※注意:
a. 請勿修改檔名、欄目名稱與雙引號。
b. AverageSplitSize需小於程式編譯檔長度。
c. Rom/Ram Size和需等於程式編譯檔長度。