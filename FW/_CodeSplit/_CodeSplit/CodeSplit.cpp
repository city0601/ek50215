#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <iomanip>
#include <algorithm>

std::string g_filename = "";
bool g_averageSplit = false;
bool g_recalChks = false;

uint32_t g_avrSplt = 0;  // Average Split Size
uint32_t g_RomSize = 0;  // Rom Size
uint32_t g_RamSize = 0;  // Ram Size
uint32_t g_cmpSize = 0;  // Compiled Text Size

std::string g_chks_msb_str;
std::string g_chks_lsb_str;

static void askSplitMode(void);
static void askRecalChks(void);

static void chkInfo(std::string &str);
static bool readInfo(void);

static std::vector<std::string> readText(void);

static void generateSpitCode(std::string &filename, std::vector<std::string> &tmp);
static void averageSpitCode(std::vector<std::string> &str);
static void hardwareSpitCode(std::vector<std::string> &str);
static bool infoErrorHandle(void);


int main(void)
{
    askSplitMode();
    
    std::cout << "[Status] Reading Information...\n";
    if(!readInfo())
    {
        system("pause");
        return 0;
    }
    
    std::vector<std::string> str_total;
    std::cout << "[Status] Reading Compiled Text File...\n";
    str_total = readText();

    if(!infoErrorHandle())
    {
        system("pause");
        return 0;
    }

    if(g_averageSplit)
    {
        averageSpitCode(str_total);
    }else 
    {
        askRecalChks();
        hardwareSpitCode(str_total);
    }

    std::cout << "\n[Status] Code spliting is finished.\n";
    system("pause");
    return 0;
}

/*************************************************************
 * Reading Information & Complied Text 
 * ***********************************************************/

static void askSplitMode(void)
{
    char ans;
    std::cout << "Average split or not?(y/n)";
    std::cin >> ans;
    if(ans == 'Y' || ans == 'y')
        g_averageSplit = true;
}

static void chkInfo(std::string &str)
{
    std::string str_tmp, str_item;
    int str_num, end1_num;
    str_num = str.find_first_of('"');
    end1_num = str.find_last_of('"');

    str_item = str.substr(0, str_num - 1);
    str_tmp = str.substr(str_num + 1, end1_num - str_num - 1);

    if(str_item == "Filename")
        g_filename = str_tmp;
    else if(str_item == "AverageSplitSize")
        g_avrSplt = stoi(str_tmp, 0, 0);
    else if(str_item == "RomSize")
        g_RomSize = (str_tmp[0] - '0')*1024;
    else if(str_item == "RamSize")
        g_RamSize = (str_tmp[0] - '0')*1024;
}

static bool readInfo(void)
{
    // Init Information file
    std::ifstream ifs("Information.txt", std::ios::in);

    // Open & Read Information file 
    if (!ifs.is_open()) {
        std::cout << "<<<<< [Error] Failed to open information file!!!!! >>>>>\n";
        return false;
    } else {
        std::string str;
        while (std::getline(ifs, str)) {
            chkInfo(str);
        }
    }
    ifs.close();

    // Print Information
    std::cout << std::endl << "Filename:\t\t" << g_filename << std::endl;
    if(g_averageSplit)
    {
        std::cout << "Average Split Size is:\t" << g_avrSplt << std::endl << std::endl;
    }else
    {
        std::cout << "Rom Size is:\t\t" << g_RomSize << std::endl;
        std::cout << "Ram Size is:\t\t" << g_RamSize << std::endl << std::endl;
    }
    return true;
}

static std::vector<std::string> readText(void)
{
    // Init Compiled Text file
    std::vector<std::string> text;
    std::ifstream ifs(g_filename, std::ios::in);

    // Open & Read Compiled Text file 
    if (!ifs.is_open()) {
        std::cout << "<<<<< [Error] Failed to open compiled file!!!!! >>>>>\n";
        system("pause");
    } else {
        std::string str;
        while (std::getline(ifs, str)) {
            g_cmpSize++;
            text.push_back(str);     
        }
    }
    ifs.close();
    return text;
}

static bool infoErrorHandle(void)
{
    if((!g_averageSplit) && ((g_RomSize + g_RamSize) != g_cmpSize))
    {
        std::cout << "\n<<<<< [Error] Rom and Ram size is not equal to compiled code size!!!!! >>>>>\n";
        return false;
    }
    if(g_averageSplit && (g_avrSplt > g_cmpSize))
    {
        std::cout << "\n<<<<< [Error] Split average size is Larger than compiled code size!!!!! >>>>>\n";
        return false;
    }
    return true;
}

/*************************************************************
 * Average Spit Code
 * ***********************************************************/

static void generateSpitCode(std::string &filename, std::vector<std::string> &tmp)
{
    std::ofstream ofs;

    ofs.open(filename);
    for (auto &s : tmp) {
        ofs << s << std::endl;
    }
    ofs.close();    
}

static void averageSpitCode(std::vector<std::string> &str)
{
    
    uint32_t file_quo, file_rem, dot_num, i;
    std::string l_cmp_filename = "";
    std::string l_spit_filename = "";
    std::vector<std::string> tmp;

    // Calculate split file number
    file_quo = g_cmpSize / g_avrSplt;
    file_rem = g_cmpSize % g_avrSplt; 

    // Store the compiled file name
    dot_num = g_filename.find_first_of('.');
    l_cmp_filename = g_filename.substr(0, dot_num);

    for (i = 0; i < file_quo; ++i)
    {
        l_spit_filename = "";
        tmp.clear();

        for (int j = 0; j < g_avrSplt; ++j)
        {
            tmp.push_back(str[i * g_avrSplt + j]);
        }

        l_spit_filename = l_cmp_filename + "_" + std::to_string(g_avrSplt) + "_" + std::to_string(i+1) + ".txt";
        generateSpitCode(l_spit_filename, tmp);
        std::cout << l_spit_filename << " is generated..."<< std::endl;
    }

    if(file_rem != 0)
    {
        std::cout << "\n<<<<<[Warning] The complied text is not exactly divided by average split size!!!!!>>>>>\n";
        l_spit_filename = "";
        tmp.clear();
        for (int k = 0; k < file_rem; ++k)
        {
            tmp.push_back(str[i * g_avrSplt + k]);  
        }

        l_spit_filename = l_cmp_filename + "_" + std::to_string(g_avrSplt) + "_" + std::to_string(i+1) + ".txt";
        generateSpitCode(l_spit_filename, tmp);
        std::cout << l_spit_filename << " is generated..."<< std::endl;
    }
}

/*************************************************************
 * Spit Code based on Rom/Ram size
 * ***********************************************************/

static void askRecalChks(void)
{
    char ans;
    std::cout << "Re-Calculate checksum or not?(y/n)";
    std::cin >> ans;
    if(ans == 'Y' || ans == 'y')
        g_recalChks = true;
}

static void hexChksTwoBytes(uint16_t val)
{
    uint16_t val_msb = 0;
    uint16_t val_lsb = 0;
    std::stringstream stream_msb, stream_lsb;
    g_chks_msb_str = "";
    g_chks_lsb_str = "";

    val_msb = val / 256;
    val_lsb = val % 256;
    // std::cout << "checksum(dec): "<< val_msb << " " << val_lsb << std::endl;

    stream_msb << std::hex << val_msb;
    std::string res_msb (stream_msb.str());
    if(res_msb.size() == 1)
        res_msb.insert(0, "0");

    stream_lsb << std::hex << val_lsb;
    std::string res_lsb (stream_lsb.str());
    if(res_lsb.size() == 1)
        res_lsb.insert(0, "0");

    transform(res_msb.begin(), res_msb.end(), res_msb.begin(), ::toupper);
    transform(res_lsb.begin(), res_lsb.end(), res_lsb.begin(), ::toupper);

    g_chks_msb_str = res_msb;
    g_chks_lsb_str = res_lsb;

    std::cout << "\nChecksum(hex): 0x"<< g_chks_msb_str << g_chks_lsb_str << std::endl;
}

static void hardwareSpitCode(std::vector<std::string> &str)
{
    uint32_t dot_num;
    std::string l_cmp_filename = "";
    std::string l_spit_filename = "";
    std::vector<std::string> tmp;

    uint16_t chks_rom_val = 0;
    uint16_t chks_ram_val = 0;
    std::string chks_rom_str;
    std::string chks_ram_str;

    // Store the compiled file name
    dot_num = g_filename.find_first_of('.');
    l_cmp_filename = g_filename.substr(0, dot_num);
    
    // Rom file generate
    l_spit_filename = "";
    tmp.clear();

    for (int i = 0; i < g_RomSize; ++i)
    {
        if(g_recalChks)
        {
            chks_rom_val += stoi(str[i], 0, 16);
        }  
        tmp.push_back(str[i]);
    }

    if(g_recalChks)
    {
        hexChksTwoBytes(chks_rom_val);
        tmp[g_RomSize - 2] = g_chks_lsb_str;
        tmp[g_RomSize - 1] = g_chks_msb_str;
    }

    l_spit_filename = l_cmp_filename + "_" + std::to_string(g_RomSize/1024) + "k.txt";
    generateSpitCode(l_spit_filename, tmp);
    std::cout << l_spit_filename << " is generated..."<< std::endl;
    
    // Ram file generate
    l_spit_filename = "";
    tmp.clear();

    if(g_recalChks)
    {
        str[(g_RomSize + g_RamSize) - 2] = "00";
        str[(g_RomSize + g_RamSize) - 1] = "00";
    }

    for (int i = g_RomSize; i < (g_RomSize + g_RamSize); ++i)
    {
        if(g_recalChks)
        {
            chks_ram_val += stoi(str[i], 0, 16);
        }
        tmp.push_back(str[i]);
    }

    if(g_recalChks)
    {    
        hexChksTwoBytes(chks_ram_val);
        tmp[(g_RamSize) - 2] = g_chks_lsb_str;
        tmp[(g_RamSize) - 1] = g_chks_msb_str;
    }

    l_spit_filename = l_cmp_filename + "_" + std::to_string(g_RamSize/1024) + "k.txt";
    generateSpitCode(l_spit_filename, tmp);
    std::cout << l_spit_filename << " is generated..."<< std::endl;
}
