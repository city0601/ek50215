/******************************************************** 
   Description: EK58212AA reg.v
     TCON Name: EK58212AA
       Company: FitiPower
    Department: SOC1
   Create Date: 2023-12-25 09:44:30
  Tool version: AutoGen_v1.3.0
  Source Excel: mcu_reg
********************************************************/ 
`timescale 1ns/1ps
module mcu_reg#(
parameter REG_ADDR = 16,
parameter REG_DATA = 8,
parameter MCU_OFFSET = 16'h0000
)(
input              scan_mode, 
input              clk, 
input              rst_n, 
input [23:0]       Slv_MEMADR_i , 
input              Slv_MEMWR_i  , 
input              Slv_MEMRD_i  , 
input [7:0]        Slv_MEMDATAI_i , 
input              reg_en_i , 
output wire [REG_DATA-1:0] reg_rdata_o, 
output wire        reg_ack_o, 

output wire [6:0]  r_slv_dev,
output wire        r_slv_grm,
output wire [6:0]  r_ext_dev,
output wire        r_mtr_grm,
output wire [2:0]  r_vcom_delay_sel,
output wire [1:0]  r_i2c_wr_dly_tm,
output wire [2:0]  r_mcu_i2c_mst_clk,
output wire        r_i2cs_rdata_sel,
output wire        mcu_i2c_ctrl,
output wire        fw_i2c_access_vcom,
output wire        vcm_i2cen,
output wire        through_mode,
output wire        r_i2c_mon_byte_mode,
output wire        reserved1,
output wire        reserved2,
output wire        r_clear,
output wire        r_wp_n,
output wire        r_hold_n,
output wire        r_sck_inv,
output wire        r_si_lth_inv,
output wire [2:0]  r_pgm_t_set_ppg,
output wire [7:0]  r_pgm_t_unit,
output wire [4:0]  r_pgm_t_set_wr,
output wire        r_nack_off,
output wire        r_ready_det2_en,
output wire        r_ready_det1_en,
output wire [1:0]  r_ready_mode,
output wire [1:0]  r_init_ready_sel,
output wire [3:0]  reserved3,
output wire [7:0]  r_init_ready_cnt,
output wire [6:0]  r_exedid_dev_addr,
output wire        reserved4,
output wire [6:0]  r_pmic_dev_adr,
output wire        r_pmic_bus,
output wire [7:0]  r_pmic_banka_start_adr,
output wire [7:0]  r_pmic_bankb_start_adr,
output wire [7:0]  r_pmic_banka_len,
output wire [7:0]  r_pmic_bankb_len,
output wire [7:0]  r_pmic_header_len,
output wire [7:0]  r_pmic_gma_len,
output wire        r_crc_en,
output wire        r_hdr_dl_en,
output wire        r_pmic_fun_en,
output wire        r_pmic_gma_en,
output wire [3:0]  r_pmic_type,
output wire [7:0]  r_pmic_status_adr,
output wire [1:0]  r_pmic_i2c_dly_rim,
output wire [5:0]  reserved5,
output wire [7:0]  r_x_board_pmic_hdr_adr1,
output wire [7:0]  r_x_board_pmic_hdr_adr2,
output wire [7:0]  r_x_board_pmic_hdr_adr3,
output wire [7:0]  r_x_board_pmic_gma_adr1,
output wire [7:0]  r_x_board_pmic_gma_adr2,
output wire [7:0]  r_x_board_pmic_gma_adr3,
output wire [7:0]  r_x_board_pmic_banka_adr1,
output wire [7:0]  r_x_board_pmic_banka_adr2,
output wire [7:0]  r_x_board_pmic_banka_adr3,
output wire [7:0]  r_x_board_pmic_bankb_adr1,
output wire [7:0]  r_x_board_pmic_bankb_adr2,
output wire [7:0]  r_x_board_pmic_bankb_adr3,
output wire [3:0]  r_c_board_spi_freq_sel,
output wire [3:0]  r_x_board_spi_freq_sel,
output wire [7:0]  r_x_board_dgc_adr1,
output wire [7:0]  r_x_board_dgc_adr2,
output wire [7:0]  r_x_board_dgc_adr3,
output wire [7:0]  r_x_board_vcom_adr1,
output wire [7:0]  r_x_board_vcom_adr2,
output wire [7:0]  r_x_board_vcom_adr3,
output wire        ioa_i2c_path_ctrl,
output wire        r_pmic_i2c_path,
output wire        r_mcu_idle_en,
output wire [2:0]  r_mcu_spi_clk,
output wire        r_auto_cal_flash_size_en,
output wire        reserved6,
output wire [7:0]  r_i2c_over_fw_addr1,
output wire [7:0]  r_i2c_over_fw_addr2,
output wire [7:0]  r_i2c_target_num,
output wire [7:0]  r_i2c_over_fw_dev,
output wire [7:0]  r_i2c_over_data1,
output wire [7:0]  r_i2c_over_data2,
output wire [7:0]  r_i2c_over_data3,
output wire [7:0]  r_i2c_over_data4,
output wire [7:0]  r_i2c_over_data5,
output wire [7:0]  r_i2c_over_data6,
output wire [7:0]  r_i2c_over_data7,
output wire [7:0]  r_i2c_over_data8,
output wire [7:0]  r_i2c_over_cnt,
output wire        r_init_restart,
output wire [6:0]  reserved7,
input [3:0]        i2c_slave_fsm_st,
input              r_hdfailbypass,
input              r_datafailbypass,
input              restart_dl_flag,
input [7:7]        r_read_only_reserved
);
///////////////////////////////////////////////////////////////
wire [REG_ADDR-1:0] reg_wraddr_i = Slv_MEMADR_i[REG_ADDR-1:0]   & {REG_ADDR{reg_en_i}} ;
wire [REG_DATA-1:0] reg_wdata_i  = Slv_MEMDATAI_i[REG_DATA-1:0] & {REG_DATA{reg_en_i}} ;
wire [REG_ADDR-1:0] reg_rdaddr_i = Slv_MEMADR_i[REG_ADDR-1:0]   & {REG_ADDR{reg_en_i}} ;
wire                reg_nRdWr_i  = Slv_MEMWR_i                  & reg_en_i             ;
wire                reg_rden_i   = Slv_MEMRD_i                  & reg_en_i             ;
///////////////////////////////////////////////////////////////
reg [2:0] ran_cnt;
reg       MEMACK_pre;
assign reg_ack_o = MEMACK_pre;
wire   MEMACK_pre1 = (ran_cnt == 3'd1);

always@(posedge clk or negedge rst_n )begin
if(!rst_n) MEMACK_pre <= 1'd0;
else if(!reg_en_i) MEMACK_pre <= 1'd0;
else MEMACK_pre <= MEMACK_pre1;
end

always@(posedge clk or negedge rst_n )begin
if(!rst_n) ran_cnt <= 3'd0;
else if(reg_en_i) begin 
    if(MEMACK_pre1)
        ran_cnt <= ran_cnt; 
    else if(ran_cnt != 3'd0)
        ran_cnt <= ran_cnt + 3'd1; 
    else if(reg_nRdWr_i | reg_rden_i) 
        ran_cnt <= 3'd1; 
end else  
    ran_cnt <= 3'd0; 
end
reg [7:0] reg_000;
reg [7:0] reg_001;
reg [7:0] reg_002;
reg [7:0] reg_003;
reg [7:0] reg_004;
reg [7:0] reg_005;
reg [7:0] reg_006;
reg [7:0] reg_007;
reg [7:0] reg_008;
reg [7:0] reg_009;
reg [7:0] reg_00a;
reg [7:0] reg_00b;
reg [7:0] reg_00c;
reg [7:0] reg_00d;
reg [7:0] reg_00e;
reg [7:0] reg_00f;
reg [7:0] reg_010;
reg [7:0] reg_011;
reg [7:0] reg_012;
reg [7:0] reg_013;
reg [7:0] reg_014;
reg [7:0] reg_015;
reg [7:0] reg_016;
reg [7:0] reg_017;
reg [7:0] reg_018;
reg [7:0] reg_019;
reg [7:0] reg_01a;
reg [7:0] reg_01b;
reg [7:0] reg_01c;
reg [7:0] reg_01d;
reg [7:0] reg_01e;
reg [7:0] reg_01f;
reg [7:0] reg_020;
reg [7:0] reg_021;
reg [7:0] reg_022;
reg [7:0] reg_023;
reg [7:0] reg_024;
reg [7:0] reg_025;
reg [7:0] reg_026;
reg [7:0] reg_027;
reg [7:0] reg_028;
reg [7:0] reg_029;
reg [7:0] reg_02a;
reg [7:0] reg_02b;
reg [7:0] reg_02c;
reg [7:0] reg_02d;
reg [7:0] reg_02e;
reg [7:0] reg_02f;
reg [7:0] reg_030;
reg [7:0] reg_031;
reg [7:0] reg_032;
reg [7:0] reg_033;
reg [7:0] reg_034;
reg [7:0] reg_035;
reg [7:0] reg_036;

always@(posedge clk or negedge rst_n )begin
    if(!rst_n)begin
           reg_000 <= 8'hDA;
           reg_001 <= 8'h80;
           reg_002 <= 8'h20;
           reg_003 <= 8'h20;
           reg_004 <= 8'h00;
           reg_005 <= 8'h00;
           reg_006 <= 8'h00;
           reg_007 <= 8'h00;
           reg_008 <= 8'h00;
           reg_009 <= 8'h00;
           reg_00a <= 8'h00;
           reg_00b <= 8'h00;
           reg_00c <= 8'h00;
           reg_00d <= 8'h00;
           reg_00e <= 8'h00;
           reg_00f <= 8'h00;
           reg_010 <= 8'h00;
           reg_011 <= 8'h00;
           reg_012 <= 8'h00;
           reg_013 <= 8'h00;
           reg_014 <= 8'h00;
           reg_015 <= 8'h00;
           reg_016 <= 8'h00;
           reg_017 <= 8'h00;
           reg_018 <= 8'h00;
           reg_019 <= 8'h00;
           reg_01a <= 8'h00;
           reg_01b <= 8'h00;
           reg_01c <= 8'h00;
           reg_01d <= 8'h00;
           reg_01e <= 8'h00;
           reg_01f <= 8'h00;
           reg_020 <= 8'h21;
           reg_021 <= 8'h00;
           reg_022 <= 8'h00;
           reg_023 <= 8'h00;
           reg_024 <= 8'h00;
           reg_025 <= 8'h00;
           reg_026 <= 8'h00;
           reg_027 <= 8'h04;
           reg_028 <= 8'h00;
           reg_029 <= 8'h00;
           reg_02a <= 8'h00;
           reg_02b <= 8'h00;
           reg_02c <= 8'h00;
           reg_02d <= 8'h00;
           reg_02e <= 8'h00;
           reg_02f <= 8'h00;
           reg_030 <= 8'h00;
           reg_031 <= 8'h00;
           reg_032 <= 8'h00;
           reg_033 <= 8'h00;
           reg_034 <= 8'h00;
           reg_035 <= 8'h00;
           //reg_036 <= 8'h00;
    end else if (reg_nRdWr_i) begin
        case (reg_wraddr_i) 
            MCU_OFFSET + 12'h000 : reg_000 <= reg_wdata_i;
            MCU_OFFSET + 12'h001 : reg_001 <= reg_wdata_i;
            MCU_OFFSET + 12'h002 : reg_002 <= reg_wdata_i;
            MCU_OFFSET + 12'h003 : reg_003 <= reg_wdata_i;
            MCU_OFFSET + 12'h004 : reg_004 <= reg_wdata_i;
            MCU_OFFSET + 12'h005 : reg_005 <= reg_wdata_i;
            MCU_OFFSET + 12'h006 : reg_006 <= reg_wdata_i;
            MCU_OFFSET + 12'h007 : reg_007 <= reg_wdata_i;
            MCU_OFFSET + 12'h008 : reg_008 <= reg_wdata_i;
            MCU_OFFSET + 12'h009 : reg_009 <= reg_wdata_i;
            MCU_OFFSET + 12'h00a : reg_00a <= reg_wdata_i;
            MCU_OFFSET + 12'h00b : reg_00b <= reg_wdata_i;
            MCU_OFFSET + 12'h00c : reg_00c <= reg_wdata_i;
            MCU_OFFSET + 12'h00d : reg_00d <= reg_wdata_i;
            MCU_OFFSET + 12'h00e : reg_00e <= reg_wdata_i;
            MCU_OFFSET + 12'h00f : reg_00f <= reg_wdata_i;
            MCU_OFFSET + 12'h010 : reg_010 <= reg_wdata_i;
            MCU_OFFSET + 12'h011 : reg_011 <= reg_wdata_i;
            MCU_OFFSET + 12'h012 : reg_012 <= reg_wdata_i;
            MCU_OFFSET + 12'h013 : reg_013 <= reg_wdata_i;
            MCU_OFFSET + 12'h014 : reg_014 <= reg_wdata_i;
            MCU_OFFSET + 12'h015 : reg_015 <= reg_wdata_i;
            MCU_OFFSET + 12'h016 : reg_016 <= reg_wdata_i;
            MCU_OFFSET + 12'h017 : reg_017 <= reg_wdata_i;
            MCU_OFFSET + 12'h018 : reg_018 <= reg_wdata_i;
            MCU_OFFSET + 12'h019 : reg_019 <= reg_wdata_i;
            MCU_OFFSET + 12'h01a : reg_01a <= reg_wdata_i;
            MCU_OFFSET + 12'h01b : reg_01b <= reg_wdata_i;
            MCU_OFFSET + 12'h01c : reg_01c <= reg_wdata_i;
            MCU_OFFSET + 12'h01d : reg_01d <= reg_wdata_i;
            MCU_OFFSET + 12'h01e : reg_01e <= reg_wdata_i;
            MCU_OFFSET + 12'h01f : reg_01f <= reg_wdata_i;
            MCU_OFFSET + 12'h020 : reg_020 <= reg_wdata_i;
            MCU_OFFSET + 12'h021 : reg_021 <= reg_wdata_i;
            MCU_OFFSET + 12'h022 : reg_022 <= reg_wdata_i;
            MCU_OFFSET + 12'h023 : reg_023 <= reg_wdata_i;
            MCU_OFFSET + 12'h024 : reg_024 <= reg_wdata_i;
            MCU_OFFSET + 12'h025 : reg_025 <= reg_wdata_i;
            MCU_OFFSET + 12'h026 : reg_026 <= reg_wdata_i;
            MCU_OFFSET + 12'h027 : reg_027 <= reg_wdata_i;
            MCU_OFFSET + 12'h028 : reg_028 <= reg_wdata_i;
            MCU_OFFSET + 12'h029 : reg_029 <= reg_wdata_i;
            MCU_OFFSET + 12'h02a : reg_02a <= reg_wdata_i;
            MCU_OFFSET + 12'h02b : reg_02b <= reg_wdata_i;
            MCU_OFFSET + 12'h02c : reg_02c <= reg_wdata_i;
            MCU_OFFSET + 12'h02d : reg_02d <= reg_wdata_i;
            MCU_OFFSET + 12'h02e : reg_02e <= reg_wdata_i;
            MCU_OFFSET + 12'h02f : reg_02f <= reg_wdata_i;
            MCU_OFFSET + 12'h030 : reg_030 <= reg_wdata_i;
            MCU_OFFSET + 12'h031 : reg_031 <= reg_wdata_i;
            MCU_OFFSET + 12'h032 : reg_032 <= reg_wdata_i;
            MCU_OFFSET + 12'h033 : reg_033 <= reg_wdata_i;
            MCU_OFFSET + 12'h034 : reg_034 <= reg_wdata_i;
            MCU_OFFSET + 12'h035 : reg_035 <= reg_wdata_i;
            //MCU_OFFSET + 12'h036 : reg_036 <= reg_wdata;
            default: begin
            reg_000 <= reg_000;
            reg_001 <= reg_001;
            reg_002 <= reg_002;
            reg_003 <= reg_003;
            reg_004 <= reg_004;
            reg_005 <= reg_005;
            reg_006 <= reg_006;
            reg_007 <= reg_007;
            reg_008 <= reg_008;
            reg_009 <= reg_009;
            reg_00a <= reg_00a;
            reg_00b <= reg_00b;
            reg_00c <= reg_00c;
            reg_00d <= reg_00d;
            reg_00e <= reg_00e;
            reg_00f <= reg_00f;
            reg_010 <= reg_010;
            reg_011 <= reg_011;
            reg_012 <= reg_012;
            reg_013 <= reg_013;
            reg_014 <= reg_014;
            reg_015 <= reg_015;
            reg_016 <= reg_016;
            reg_017 <= reg_017;
            reg_018 <= reg_018;
            reg_019 <= reg_019;
            reg_01a <= reg_01a;
            reg_01b <= reg_01b;
            reg_01c <= reg_01c;
            reg_01d <= reg_01d;
            reg_01e <= reg_01e;
            reg_01f <= reg_01f;
            reg_020 <= reg_020;
            reg_021 <= reg_021;
            reg_022 <= reg_022;
            reg_023 <= reg_023;
            reg_024 <= reg_024;
            reg_025 <= reg_025;
            reg_026 <= reg_026;
            reg_027 <= reg_027;
            reg_028 <= reg_028;
            reg_029 <= reg_029;
            reg_02a <= reg_02a;
            reg_02b <= reg_02b;
            reg_02c <= reg_02c;
            reg_02d <= reg_02d;
            reg_02e <= reg_02e;
            reg_02f <= reg_02f;
            reg_030 <= reg_030;
            reg_031 <= reg_031;
            reg_032 <= reg_032;
            reg_033 <= reg_033;
            reg_034 <= reg_034;
            reg_035 <= reg_035;
            //reg_036 <= reg_036;
            end
        endcase
    end
end

reg [7:0] reg_rdata_o_net;
always@(*)begin
        case(reg_rdaddr_i)
            MCU_OFFSET + 12'h000 : reg_rdata_o_net = reg_000;
            MCU_OFFSET + 12'h001 : reg_rdata_o_net = reg_001;
            MCU_OFFSET + 12'h002 : reg_rdata_o_net = reg_002;
            MCU_OFFSET + 12'h003 : reg_rdata_o_net = reg_003;
            MCU_OFFSET + 12'h004 : reg_rdata_o_net = reg_004;
            MCU_OFFSET + 12'h005 : reg_rdata_o_net = reg_005;
            MCU_OFFSET + 12'h006 : reg_rdata_o_net = reg_006;
            MCU_OFFSET + 12'h007 : reg_rdata_o_net = reg_007;
            MCU_OFFSET + 12'h008 : reg_rdata_o_net = reg_008;
            MCU_OFFSET + 12'h009 : reg_rdata_o_net = reg_009;
            MCU_OFFSET + 12'h00a : reg_rdata_o_net = reg_00a;
            MCU_OFFSET + 12'h00b : reg_rdata_o_net = reg_00b;
            MCU_OFFSET + 12'h00c : reg_rdata_o_net = reg_00c;
            MCU_OFFSET + 12'h00d : reg_rdata_o_net = reg_00d;
            MCU_OFFSET + 12'h00e : reg_rdata_o_net = reg_00e;
            MCU_OFFSET + 12'h00f : reg_rdata_o_net = reg_00f;
            MCU_OFFSET + 12'h010 : reg_rdata_o_net = reg_010;
            MCU_OFFSET + 12'h011 : reg_rdata_o_net = reg_011;
            MCU_OFFSET + 12'h012 : reg_rdata_o_net = reg_012;
            MCU_OFFSET + 12'h013 : reg_rdata_o_net = reg_013;
            MCU_OFFSET + 12'h014 : reg_rdata_o_net = reg_014;
            MCU_OFFSET + 12'h015 : reg_rdata_o_net = reg_015;
            MCU_OFFSET + 12'h016 : reg_rdata_o_net = reg_016;
            MCU_OFFSET + 12'h017 : reg_rdata_o_net = reg_017;
            MCU_OFFSET + 12'h018 : reg_rdata_o_net = reg_018;
            MCU_OFFSET + 12'h019 : reg_rdata_o_net = reg_019;
            MCU_OFFSET + 12'h01a : reg_rdata_o_net = reg_01a;
            MCU_OFFSET + 12'h01b : reg_rdata_o_net = reg_01b;
            MCU_OFFSET + 12'h01c : reg_rdata_o_net = reg_01c;
            MCU_OFFSET + 12'h01d : reg_rdata_o_net = reg_01d;
            MCU_OFFSET + 12'h01e : reg_rdata_o_net = reg_01e;
            MCU_OFFSET + 12'h01f : reg_rdata_o_net = reg_01f;
            MCU_OFFSET + 12'h020 : reg_rdata_o_net = reg_020;
            MCU_OFFSET + 12'h021 : reg_rdata_o_net = reg_021;
            MCU_OFFSET + 12'h022 : reg_rdata_o_net = reg_022;
            MCU_OFFSET + 12'h023 : reg_rdata_o_net = reg_023;
            MCU_OFFSET + 12'h024 : reg_rdata_o_net = reg_024;
            MCU_OFFSET + 12'h025 : reg_rdata_o_net = reg_025;
            MCU_OFFSET + 12'h026 : reg_rdata_o_net = reg_026;
            MCU_OFFSET + 12'h027 : reg_rdata_o_net = reg_027;
            MCU_OFFSET + 12'h028 : reg_rdata_o_net = reg_028;
            MCU_OFFSET + 12'h029 : reg_rdata_o_net = reg_029;
            MCU_OFFSET + 12'h02a : reg_rdata_o_net = reg_02a;
            MCU_OFFSET + 12'h02b : reg_rdata_o_net = reg_02b;
            MCU_OFFSET + 12'h02c : reg_rdata_o_net = reg_02c;
            MCU_OFFSET + 12'h02d : reg_rdata_o_net = reg_02d;
            MCU_OFFSET + 12'h02e : reg_rdata_o_net = reg_02e;
            MCU_OFFSET + 12'h02f : reg_rdata_o_net = reg_02f;
            MCU_OFFSET + 12'h030 : reg_rdata_o_net = reg_030;
            MCU_OFFSET + 12'h031 : reg_rdata_o_net = reg_031;
            MCU_OFFSET + 12'h032 : reg_rdata_o_net = reg_032;
            MCU_OFFSET + 12'h033 : reg_rdata_o_net = reg_033;
            MCU_OFFSET + 12'h034 : reg_rdata_o_net = reg_034;
            MCU_OFFSET + 12'h035 : reg_rdata_o_net = reg_035;
            MCU_OFFSET + 12'h036 : reg_rdata_o_net = reg_036;
            default:reg_rdata_o_net = 8'd0;
        endcase
end

reg [7:0] reg_rdata_o_net_d1;
always@(posedge clk or negedge rst_n )begin
    if(!rst_n) reg_rdata_o_net_d1 <= 0;
    else reg_rdata_o_net_d1 <= reg_rdata_o_net;
end
assign reg_rdata_o = scan_mode? reg_rdata_o_net_d1 : reg_rdata_o_net;

assign r_slv_dev[6:0] = reg_000[6:0];
assign r_slv_grm = reg_000[7];
assign r_ext_dev[6:0] = reg_001[6:0];
assign r_mtr_grm = reg_001[7];
assign r_vcom_delay_sel[2:0] = reg_002[2:0];
assign r_i2c_wr_dly_tm[1:0] = reg_002[4:3];
assign r_mcu_i2c_mst_clk[2:0] = reg_002[7:5];
assign r_i2cs_rdata_sel = reg_003[0];
assign mcu_i2c_ctrl = reg_003[1];
assign fw_i2c_access_vcom = reg_003[2];
assign vcm_i2cen = reg_003[3];
assign through_mode = reg_003[4];
assign r_i2c_mon_byte_mode = reg_003[5];
assign reserved1 = reg_003[6];
assign reserved2 = reg_003[7];
assign r_clear = reg_004[0];
assign r_wp_n = reg_004[1];
assign r_hold_n = reg_004[2];
assign r_sck_inv = reg_004[3];
assign r_si_lth_inv = reg_004[4];
assign r_pgm_t_set_ppg[2:0] = reg_004[7:5];
assign r_pgm_t_unit[7:0] = reg_005[7:0];
assign r_pgm_t_set_wr[4:0] = reg_006[4:0];
assign r_nack_off = reg_006[5];
assign r_ready_det2_en = reg_006[6];
assign r_ready_det1_en = reg_006[7];
assign r_ready_mode[1:0] = reg_007[1:0];
assign r_init_ready_sel[1:0] = reg_007[3:2];
assign reserved3[3:0] = reg_007[7:4];
assign r_init_ready_cnt[7:0] = reg_008[7:0];
assign r_exedid_dev_addr[6:0] = reg_009[6:0];
assign reserved4 = reg_009[7];
assign r_pmic_dev_adr[6:0] = reg_00a[6:0];
assign r_pmic_bus = reg_00a[7];
assign r_pmic_banka_start_adr[7:0] = reg_00b[7:0];
assign r_pmic_bankb_start_adr[7:0] = reg_00c[7:0];
assign r_pmic_banka_len[7:0] = reg_00d[7:0];
assign r_pmic_bankb_len[7:0] = reg_00e[7:0];
assign r_pmic_header_len[7:0] = reg_00f[7:0];
assign r_pmic_gma_len[7:0] = reg_010[7:0];
assign r_crc_en = reg_011[0];
assign r_hdr_dl_en = reg_011[1];
assign r_pmic_fun_en = reg_011[2];
assign r_pmic_gma_en = reg_011[3];
assign r_pmic_type[3:0] = reg_011[7:4];
assign r_pmic_status_adr[7:0] = reg_012[7:0];
assign r_pmic_i2c_dly_rim[1:0] = reg_013[1:0];
assign reserved5[5:0] = reg_013[7:2];
assign r_x_board_pmic_hdr_adr1[7:0] = reg_014[7:0];
assign r_x_board_pmic_hdr_adr2[7:0] = reg_015[7:0];
assign r_x_board_pmic_hdr_adr3[7:0] = reg_016[7:0];
assign r_x_board_pmic_gma_adr1[7:0] = reg_017[7:0];
assign r_x_board_pmic_gma_adr2[7:0] = reg_018[7:0];
assign r_x_board_pmic_gma_adr3[7:0] = reg_019[7:0];
assign r_x_board_pmic_banka_adr1[7:0] = reg_01a[7:0];
assign r_x_board_pmic_banka_adr2[7:0] = reg_01b[7:0];
assign r_x_board_pmic_banka_adr3[7:0] = reg_01c[7:0];
assign r_x_board_pmic_bankb_adr1[7:0] = reg_01d[7:0];
assign r_x_board_pmic_bankb_adr2[7:0] = reg_01e[7:0];
assign r_x_board_pmic_bankb_adr3[7:0] = reg_01f[7:0];
assign r_c_board_spi_freq_sel[3:0] = reg_020[3:0];
assign r_x_board_spi_freq_sel[3:0] = reg_020[7:4];
assign r_x_board_dgc_adr1[7:0] = reg_021[7:0];
assign r_x_board_dgc_adr2[7:0] = reg_022[7:0];
assign r_x_board_dgc_adr3[7:0] = reg_023[7:0];
assign r_x_board_vcom_adr1[7:0] = reg_024[7:0];
assign r_x_board_vcom_adr2[7:0] = reg_025[7:0];
assign r_x_board_vcom_adr3[7:0] = reg_026[7:0];
assign ioa_i2c_path_ctrl = reg_027[0];
assign r_pmic_i2c_path = reg_027[1];
assign r_mcu_idle_en = reg_027[2];
assign r_mcu_spi_clk[2:0] = reg_027[5:3];
assign r_auto_cal_flash_size_en = reg_027[6];
assign reserved6 = reg_027[7];
assign r_i2c_over_fw_addr1[7:0] = reg_028[7:0];
assign r_i2c_over_fw_addr2[7:0] = reg_029[7:0];
assign r_i2c_target_num[7:0] = reg_02a[7:0];
assign r_i2c_over_fw_dev[7:0] = reg_02b[7:0];
assign r_i2c_over_data1[7:0] = reg_02c[7:0];
assign r_i2c_over_data2[7:0] = reg_02d[7:0];
assign r_i2c_over_data3[7:0] = reg_02e[7:0];
assign r_i2c_over_data4[7:0] = reg_02f[7:0];
assign r_i2c_over_data5[7:0] = reg_030[7:0];
assign r_i2c_over_data6[7:0] = reg_031[7:0];
assign r_i2c_over_data7[7:0] = reg_032[7:0];
assign r_i2c_over_data8[7:0] = reg_033[7:0];
assign r_i2c_over_cnt[7:0] = reg_034[7:0];
assign r_init_restart = reg_035[0];
assign reserved7[6:0] = reg_035[7:1];
always@(*)begin
       reg_036[3:0] = i2c_slave_fsm_st[3:0];
       reg_036[4] = r_hdfailbypass;
       reg_036[5] = r_datafailbypass;
       reg_036[6] = restart_dl_flag;
       reg_036[7:7] = r_read_only_reserved;
end

endmodule